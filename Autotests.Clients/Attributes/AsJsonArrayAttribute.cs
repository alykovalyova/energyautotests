﻿using System;

namespace Autotests.Clients.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AsJsonArrayAttribute : Attribute
    {
    }
}