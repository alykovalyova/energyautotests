﻿using System;

namespace Autotests.Clients.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GetTokenAttribute : Attribute
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}