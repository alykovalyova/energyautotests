﻿using System;
using System.Net.Http;

namespace Autotests.Clients
{
    public class BaseHttpClient : System.Net.Http.HttpClient
    {

        public BaseHttpClient(string url)
        {
            this.BaseAddress = new Uri(url);
        }
    }
}
