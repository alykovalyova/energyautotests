using System;
using System.Net;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using Nuts.ProdMan.Model.Contract.Transport;
using Proposition = Nuts.ProdMan.Model.Contract.PropositionsToSave.Proposition;
using PropositionDetails = Nuts.ProdMan.Model.Contract.Propositions.Proposition;

namespace Autotests.Clients.Clients
{
    public class ProdmanServiceClient
    {
        private readonly ServiceClient<IProdManService> _prodManClient;

        public ProdmanServiceClient()
        {
            _prodManClient =
                new ServiceClient<IProdManService>(ConfigHandler.Instance.ServicesEndpoints.ProdManService);
        }

        public PropositionDetails CreateProposition(Proposition proposition)
        {
            var response = new AddPropositionRequest()
            {
                PropositionInfo = proposition
            }.CallWith(_prodManClient.Proxy.AddProposition);
            Assert.That(response.Header.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));

            var propositionDetails = GetPropositionDetails(response.PropositionId);
            return propositionDetails;
        }

        private void ActivateProposition(Guid propositionId)
        {
            new UpdatePropositionStatusRequest
            {
                PropositionId = propositionId,
                NewStatus = Status.Verifiable
            }.CallWith(_prodManClient.Proxy.UpdatePropositionStatus);

            new UpdatePropositionStatusRequest
            {
                PropositionId = propositionId,
                NewStatus = Status.Approved
            }.CallWith(_prodManClient.Proxy.UpdatePropositionStatus);

            new UpdatePropositionStatusRequest
            {
                PropositionId = propositionId,
                NewStatus = Status.Active
            }.CallWith(_prodManClient.Proxy.UpdatePropositionStatus);

            Waiter.Wait(() => new GetPropositionInfoRequest
                {PropositionId = propositionId}
                .CallWith(_prodManClient.Proxy.GetPropositionInfo).PropositionInfo.PropositionStatus == Status.Active);
        }

        public PropositionDetails GetPropositionDetails(Guid propositionId)
        {
            var response = new GetPropositionDetailsRequest()
            {
                PropositionId = propositionId
            }.CallWith(_prodManClient.Proxy.GetProposition);
            
            Assert.That(response.Header.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));

            return response.PropositionDetails;
        }

        public PropositionDetails CreateActiveProposition(Proposition propositionBase)
        {
            var proposition = CreateProposition(propositionBase);
            ActivateProposition(proposition.Id);
            return GetPropositionDetails(proposition.Id);
        }
    }
}