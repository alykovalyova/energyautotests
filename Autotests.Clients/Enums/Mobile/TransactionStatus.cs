﻿namespace Autotests.Clients.Enums.Mobile
{
    public enum TransactionStatus
    {
        Open,
        Expired,
        Cancelled,
        Success,
        Failure
    }
}