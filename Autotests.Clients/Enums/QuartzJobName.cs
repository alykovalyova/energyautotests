﻿namespace Autotests.Clients.Enums
{
    public enum QuartzJobName
    {
        #region Switches
        Energy_Switches_SwitchesTestJobA,
        Energy_Switches_SwitchesTestJobB,
        Energy_EdsnSwitch_GetGainsFake,
        Energy_EdsnSwitch_GetLossesFake,
        #endregion
        #region RenewalCampaign
        Energy_RenewalCampaign_ExecutedReactiveCampaignEvents,
        Energy_RenewalCampaign_CheckEventsOfferPropositions,
        Energy_RenewalCampaign_CreateBnaCampaigns,
        Energy_RenewalCampaign_AddRenewableContractsToCampaign,
        Energy_RenewalCampaign_ExecuteProactiveAndBnaCampaignsEvents,
        Energy_RenewalCampaign_ExecuteSearchOutdatedContracts,
        Energy_RenewalCampaign_ExecuteStartCampaignsTask,
        Energy_RenewalCampaign_ExecuteStopCampaignByEndDate,
        Energy_RenewalCampaign_ExecuteUpdateUsageCheck,
        Energy_RenewalCampaign_ExpiredCustomOffer,
        Energy_RenewalCampaign_ExportPhoneEventCsvFiles,
        #endregion
        #region RegisterSynchronization
        Energy_RegisterSynchronization_NotifyCustomerInformationAboutExtractionJob,
        Energy_RegisterSynchronization_NotifyIcarAboutExtractionJob,
        Energy_RegisterSynchronization_TryToCreateMismatchReportJob,
        #endregion
        #region Prospect
        Energy_Prospect_DailyCheckJob,
        #endregion
        #region ProdMan
        Energy_ProdMan_DailyPropositionCheckJob,
        #endregion
        #region MMVoucher
        Energy_MMVoucher_CancelVoucherReservationJob,
        Energy_MMVoucher_SftpListenerJob,
        Energy_MMVoucher_RequestRestValueJob,
        #endregion
        #region Messaging
        Energy_Messaging_MoveInJob,
        Energy_Messaging_MoveOutJob,
        Energy_Messaging_ChangeOfSupplierJob,
        Energy_Messaging_EndOfSupplyJob,
        #endregion
        #region MasterData
        Energy_MasterData_ExecuteMasterDataBatchTask,
        Energy_MasterData_FutureProcessingMduJob,
        #endregion
        #region ICAR
        Energy_ICAR_ProcessFutureUpdates,
        Energy_ICAR_ReprocessMessageJob,
        #endregion
        #region EdsnGateway
        Energy_EdsnGateway_DownloadFilesJob,
        Energy_EdsnGateway_GetAcceptedMeterReadings,
        Energy_EdsnGateway_GetAcceptedMeterReadingsFake,
        Energy_EdsnGateway_GetCapacityTariffsFakeJob,
        Energy_EdsnGateway_GetCapacityTariffsJob,
        Energy_EdsnGateway_GetContractMoveOuts,
        Energy_EdsnGateway_GetContractMoveOutsFake,
        Energy_EdsnGateway_GetContractRegisteries,
        Energy_EdsnGateway_GetContractRegisteriesFake,
        Energy_EdsnGateway_GetDisputeResults,
        Energy_EdsnGateway_GetDisputeResultsFake,
        Energy_EdsnGateway_GetDisputes,
        Energy_EdsnGateway_GetDisputesFake,
        Energy_EdsnGateway_GetFileExchangeResult,
        Energy_EdsnGateway_GetFileExchangeResultFake,
        Energy_EdsnGateway_GetLosses,
        Energy_EdsnGateway_GetLossesFake,
        Energy_EdsnGateway_GetMasterDataUpdates,
        Energy_EdsnGateway_GetMasterDataUpdatesFake,
        Energy_EdsnGateway_GetMeterReadingRejections,
        Energy_EdsnGateway_GetMeterReadingRejectionsFake,
        #endregion
        #region Consumption
        /// <summary>
        /// Domain Name => DailyJob
        /// </summary>
        Energy_Consumption_DailyStatusCheckJob,
        Energy_Consumption_SendDisputeMeterReadingJob,
        Energy_Consumption_SendMeterReadingJob,
        #endregion
        #region CommercialCharacteristic
        Energy_CommercialCharacteristic_ScheduledRetryProcessingCcuJob,
        Energy_CommercialCharacteristic_FutureProcessingCcuJob,
        #endregion
        #region P4DailyReadings
        Energy_P4Daily_DailyCommunicationHistoryJob,
        Energy_P4Daily_RequestDailyReadingsJob,
        Energy_P4Daily_HistoricalDataJob,
        #endregion
        #region P4EventReadings
        Energy_P4Event_RequestingMeterReadingsJob,
        Energy_P4Event_MarkingMeteringPointStatusJob,
        Energy_P4Event_RetrySendP4DataJob,
        Energy_P4Event_SendingRequestsJob
        #endregion
    }
}