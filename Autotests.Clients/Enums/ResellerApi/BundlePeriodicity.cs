﻿namespace Autotests.Clients.Enums.ResellerApi
{
    public enum BundlePeriodicity
    {
        Monthly,
        OneTime
    }
}