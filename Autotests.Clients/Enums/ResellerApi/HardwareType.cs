﻿namespace Autotests.Clients.Enums.ResellerApi
{
    public enum HardwareType
    {
        AdditionalStb,
        MainStb,
        Modem,
        NotHardware
    }
}