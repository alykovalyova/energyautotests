﻿namespace Autotests.Clients.Enums.ResellerApi
{
    public enum LineType
    {
        Fiber,
        NotLine,
        Copper
    }
}