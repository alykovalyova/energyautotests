﻿namespace Autotests.Clients.Enums.ResellerApi
{
    public enum Name
    {
        ExpectedDownMbps,
        ExpectedUpMbps,
        FiberAmount,
        CopperAmount
    }
}