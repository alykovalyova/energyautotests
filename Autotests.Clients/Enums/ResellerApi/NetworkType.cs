﻿using System.Runtime.Serialization;

namespace Autotests.Clients.Enums.ResellerApi
{
    public enum NetworkType
    {
        [EnumMember(Value = " ")] Empty,
        [EnumMember(Value = "Sni-F")] SniF
    }
}