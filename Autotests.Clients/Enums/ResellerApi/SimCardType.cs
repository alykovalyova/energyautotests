﻿namespace Autotests.Clients.Enums.ResellerApi
{
    public enum SimCardType
    {
        Unknown,
        USIM,
        USIM_Nano,
        USIM_NormalMicroNano
    }
}