﻿using System.Runtime.Serialization;

namespace Autotests.Clients.Enums.ResellerApi
{
    public enum Technology
    {
        [EnumMember(Value = " ")] Empty,
        [EnumMember(Value = "GoF")] GoF,
        [EnumMember(Value = "Adsl2_Pots")] Adsl2Pots
    }
}