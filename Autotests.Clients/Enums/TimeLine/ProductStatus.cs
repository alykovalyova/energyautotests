﻿namespace Autotests.Clients.Enums.TimeLine
{
    public enum ProductStatus
    {
        CrossSell = 1,
        Active,
        Passive
    }
}