﻿using System;

namespace Autotests.Clients.Enums
{
    [Serializable]
    public enum ValidationStatus
    {
        Valid,
        InvalidFunctionality,
        InvalidTechnical,
        SwappedReadings
    }
}