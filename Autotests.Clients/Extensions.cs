﻿using System.Net;
using Autotests.Clients.Rest;
using NUnit.Framework;

namespace Autotests.Clients
{
    public static class Extensions
    {
        public static void StatusCodeShouldBe(this ResponseHeader header, HttpStatusCode expectation)
        {
            if (header.StatusCode != expectation)
                throw new AssertionException($"Expected: {expectation}, but was: {header.StatusCode}.\n{header.Message}");
        }
    }
}