﻿using System;
using System.Collections.Generic;
using Allure.NUnit.Attributes;
using Autotests.Core.Handlers;
using Nuts.FakeEdsn.Contracts;
using Nuts.FakeEdsn.Contracts.Enums;
using Nuts.FakeEdsn.Contracts.NoticeContract;
using static Autotests.Core.Helpers.AllureHelpers;

namespace Autotests.Clients
{
    public class FakeEdsnAdminClient : IFakeEdsnAdministrativeService
    {
        private const string ClearAllowedKey = "F876B430-F3B1-4E7F-9ABF-1FC4DFD2CD80";
        private readonly ServiceClient<IFakeEdsnAdministrativeService> _client;

        public FakeEdsnAdminClient()
        {
            _client = new ServiceClient<IFakeEdsnAdministrativeService>(ConfigHandler.Instance.ServicesEndpoints.FakeEdsnAdministrativeService);
        }

        [AllureStep("ProcessMeterReadingByExternalReference")]
        public void ProcessMeterReadingByExternalReference(string externalReference, MeterReadingStatus status)
        {
            _client.Proxy.ProcessMeterReadingByExternalReference(externalReference, status);
            AddAttachments(externalReference, status);
        }

        [AllureStep("ProcessDisputeByExternalReference")]
        public void ProcessDisputeByExternalReference(string externalReference, MeterReadingDisputeStatus status)
        {
            _client.Proxy.ProcessDisputeByExternalReference(externalReference, status);
            AddAttachments(externalReference, status);
        }

        [AllureStep("ProcessMasterDataUpdateByExternalReference")]
        public void ProcessMasterDataUpdateByExternalReference(string externalReference, MasterDataUpdateStatus status)
        {
            _client.Proxy.ProcessMasterDataUpdateByExternalReference(externalReference, status);
            AddAttachments(externalReference, status);
        }

        [AllureStep("ProcessP4RequestsByExternalReference")]
        public void ProcessP4RequestsByExternalReference(string externalReference, List<object> p4MpItems)
        {
            _client.Proxy.ProcessP4RequestsByExternalReference(externalReference, p4MpItems);
            AddAttachments(externalReference, p4MpItems);
        }

        [AllureStep("SendInitialCommercialCharacteristic")]
        public void SendInitialCommercialCharacteristic(CommercialCharacteristic cc)
        {
            _client.Proxy.SendInitialCommercialCharacteristic(cc);
            AddAttachments(cc);
        }

        [AllureStep("SendMasterDataUpdate")]
        public void SendMasterDataUpdate(List<MasterDataUpdateResponseEnvelope_PC_PMP> mdus)
        {
            _client.Proxy.SendMasterDataUpdate(mdus);
            AddAttachments(mdus);
        }

        [AllureStep("SaveHistoricalMeterReadings")]
        public void SaveHistoricalMeterReadingSeries(List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope> series)
        {
            _client.Proxy.SaveHistoricalMeterReadingSeries(series);
            AddAttachments(series);
        }

        [AllureStep("SaveSupplierPhaseMeteringPoint")]
        public void SaveSupplierPhaseMeteringPoint(List<GetMeteringPointResponseEnvelope> responses)
        {
            _client.Proxy.SaveSupplierPhaseMeteringPoint(responses);
            AddAttachments(responses);
        }

        [AllureStep("SaveOfferPhaseMeteringPoint")]
        public void SaveOfferPhaseMeteringPoint(List<GetMPInformationResponseEnvelope> responses)
        {
            _client.Proxy.SaveOfferPhaseMeteringPoint(responses);
            AddAttachments(responses);
        }

        [AllureStep("SavePreSwitchPhaseMeteringPoint")]
        public void SavePreSwitchPhaseMeteringPoint(List<GetSCMPInformationResponseEnvelope> responses)
        {
            _client.Proxy.SavePreSwitchPhaseMeteringPoint(responses);
            AddAttachments(responses);
        }

        [AllureStep("SaveSearchPhaseMeteringPoint")]
        public void SaveSearchPhaseMeteringPoint(List<SearchMeteringPointsResponseRequestEnvelope> responses)
        {
            _client.Proxy.SaveSearchPhaseMeteringPoint(responses);
            AddAttachments(responses);
        }

        [AllureStep("SaveCustomerApprovalKeysMPs")]
        public void SaveCustomerApprovalKeysMPs(string externalReference, CreateCKResponseEnvelope response)
        {
            _client.Proxy.SaveCustomerApprovalKeysMPs(externalReference, response);
            AddAttachments(externalReference, response);
        }

        [AllureStep("SavePublishCustomerContractAcknowledgementMPs")]
        public void SavePublishCustomerContractAcknowledgementMPs(string externalReference, NoticeContractAcknowledgementEnvelope response)
        {
            _client.Proxy.SavePublishCustomerContractAcknowledgementMPs(externalReference, response);
            AddAttachments(externalReference, response);
        }

        [AllureStep("SaveCerContractData")]
        public void SaveCerContractData(List<ContractDataResponseEnvelope> contractDataResponses)
        {
            _client.Proxy.SaveCerContractData(contractDataResponses);
            AddAttachments(contractDataResponses);
        }

        [AllureStep("ClearAllPublishCustomerInfoMps")]
        public void ClearAllPublishCustomerInfoMps(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllPublishCustomerInfoMps(clearAllowedKey);
        }

        [AllureStep("ClearAllReadings")]
        public void ClearAllReadings(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllReadings(clearAllowedKey);
        }

        [AllureStep("ClearAllMdus")]
        public void ClearAllMdus(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllMdus(clearAllowedKey);
        }

        [AllureStep("ClearAllCommercialCharacteristics")]
        public void ClearAllCommercialCharacteristics(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllCommercialCharacteristics(clearAllowedKey);
        }

        [AllureStep("ClearAllHistoricalMeterReadings")]
        public void ClearAllHistoricalMeterReadingsSeries(string clearAllowedKey)
        {
            _client.Proxy.ClearAllHistoricalMeterReadingsSeries(clearAllowedKey);
        }

        [AllureStep("ClearAllRequests")]
        public void ClearAllRequests(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllRequests(clearAllowedKey);
        }

        [AllureStep("ClearAllMpsOfferPhase")]
        public void ClearAllMpsOfferPhase(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllMpsOfferPhase(clearAllowedKey);
        }

        [AllureStep("ClearAllMpsPreSwitchPhase")]
        public void ClearAllMpsPreSwitchPhase(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllMpsPreSwitchPhase(clearAllowedKey);
        }

        [AllureStep("ClearAllMpsSearch")]
        public void ClearAllMpsSearch(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllMpsSearch(clearAllowedKey);
        }

        [AllureStep("ClearAllMpsSupplierPhase")]
        public void ClearAllMpsSupplierPhase(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllMpsSupplierPhase(clearAllowedKey);
        }

        [AllureStep("ClearAllMps")]
        public void ClearAllMps(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllMps(clearAllowedKey);
        }

        [AllureStep("ClearAllCerData")]
        public void ClearAllCerData(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllCerData(clearAllowedKey);
        }

        [AllureStep("ClearAll")]
        public void ClearAll(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAll(clearAllowedKey);
        }

        [AllureStep("AddTmrReadingSupplierPhase")]
        public void AddTmrReadingSupplierPhase(antwoord_aansluiting_type reading)
        {
            _client.Proxy.AddTmrReadingSupplierPhase(reading);
            AddAttachments(reading);
        }

        [AllureStep("AddTmrReadingPreContractPhase")]
        public void AddTmrReadingPreContractPhase(antwoord_aansluiting reading)
        {
            _client.Proxy.AddTmrReadingPreContractPhase(reading);
            AddAttachments(reading);
        }

        [AllureStep("SetSleepValue")]
        public void SetSleepValue(uint? sleepValue, string serviceName)
        {
            _client.Proxy.SetSleepValue(sleepValue, serviceName);
            AddAttachments(sleepValue, serviceName);
        }

        [AllureStep("GetStorageFiles")]
        public ICollection<string> GetStorageFiles()
        {
            var files = _client.Proxy.GetStorageFiles();
            AddAttachments(files);
            return files;
        }

        [AllureStep("ReadStorageFile")]
        public string ReadStorageFile(string fileName)
        {
            var file = _client.Proxy.ReadStorageFile(fileName);
            AddAttachments(file);
            return file;
        }

        [AllureStep("UploadFile")]
        public void UploadFile(string name, string content)
        {
            _client.Proxy.UploadFile(name, content);
            AddAttachments(name, content);
        }

        [AllureStep("SaveFileExchangeResult")]
        public void SaveFileExchangeResult(List<FileExchangeResultResponseEnvelope> fileExchangeResultResponse)
        {
            _client.Proxy.SaveFileExchangeResult(fileExchangeResultResponse);
            AddAttachments(fileExchangeResultResponse);
        }

        [AllureStep("SaveFileExchangeNotification")]
        public void SaveFileExchangeNotification(string externalReference, FileExchangeAcknowledgementEnvelope_PC fileExchangeNotificationResponse)
        {
            _client.Proxy.SaveFileExchangeNotification(externalReference, fileExchangeNotificationResponse);
            AddAttachments(externalReference, fileExchangeNotificationResponse);
        }

        [AllureStep("ClearAllFileExchangeNotifications")]
        public void ClearAllFileExchangeNotifications(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllFileExchangeNotifications(clearAllowedKey);
        }

        [AllureStep("SaveCapacityTariffs")]
        public void SaveCapacityTariffs(List<TariffsResponseEnvelope_Portaal_Content_Query> tariffs)
        {
            _client.Proxy.SaveCapacityTariffs(tariffs);
        }

        [AllureStep("ClearAllCapacityTariffs")]
        public void ClearAllCapacityTariffs(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllCapacityTariffs(clearAllowedKey);
        }

        public void SaveBatchRejection(BatchType type, BatchRejectionBase response)
        {
            throw new NotImplementedException();
        }

        [AllureStep("ClearAllBatchResponses")]
        public void ClearAllBatchResponses(string clearAllowedKey = ClearAllowedKey)
        {
            _client.Proxy.ClearAllBatchResponses(clearAllowedKey);
        }

        [AllureStep("SaveExternalLoss")]
        public void SaveExternalLoss(LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint loss)
        {
            _client.Proxy.SaveExternalLoss(loss);
        }
    }
}
