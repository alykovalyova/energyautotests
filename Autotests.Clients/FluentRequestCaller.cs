﻿using System;
using System.Threading.Tasks;
using Allure.NUnit.Attributes;
using Autotests.Clients.Rest;
using Nuts.ApiClient;
using static Autotests.Core.Helpers.AllureHelpers;

namespace Autotests.Clients
{
    public static class FluentRequestCaller
    {
        public static ApiResponse<TResponse> CallWith<TRequest, TResponse>(this TRequest request, NutsHttpClient client)
            where TRequest : ApiRequestBase
        {
            return client.MakeRequest<TRequest, TResponse>(request);
        }

        public static ApiResponse CallWith<TRequest>(this TRequest request, NutsHttpClient client)
            where TRequest : ApiRequestBase
        {
            return client.MakeRequest(request);
        }

        public static RestResponseBase<TResponse> CallWith<TResponse>(this RestRequestBase request,
            RestSharpClient client, string token = null, bool skipNullProps = false) where TResponse : new()
        {
            return client.ExecuteCall<TResponse>(request, token, skipNullProps);
        }

        [AllureStep("Call: &request&")]
        public static TResponse CallWith<TRequest, TResponse>(this TRequest request, Func<TRequest, TResponse> func)
        {
            var response = func.Invoke(request);
            AddAttachments(request, response);
            return response;
        }

        [AllureStep("Call: &request&")]
        public static TResponse CallWith<TRequest, TResponse>(this TRequest request,
            Func<TRequest, Task<TResponse>> asyncFunc)
        {
            var response = asyncFunc.Invoke(request).GetAwaiter().GetResult();
            AddAttachments(request, response);
            return response;
        }
    }
}