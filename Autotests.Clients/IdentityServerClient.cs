﻿using Autotests.Clients.Rest;
using Autotests.Clients.Rest.Contracts.IdentityServer;
using Autotests.Core.Handlers;
using IdentityModel.Client;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;

namespace Autotests.Clients
{
    public class IdentityServerClient
    {
        private static readonly ConfigHandler _config = ConfigHandler.Instance;
        private readonly HttpClient _client;
        private readonly RestSharpClient _restSharpClient;
        private readonly DiscoveryDocumentResponse _discoveryDocument;
        private readonly string _baseUrl = _config.ApiUrls.IdentityService;
        private readonly string _clientId = "qaAuto";
        private readonly string _clientSecret = _config.IdentityServerSettings.ClientSecrete;
        private readonly string _scope = "mobileApi openid idsService";

        public IdentityServerClient()
        {
            _client = new BaseHttpClient(_baseUrl);
            _restSharpClient = new RestSharpClient(new Uri(_baseUrl));
            _discoveryDocument = _client.GetDiscoveryDocumentAsync(_baseUrl).Result;
            if (_discoveryDocument.IsError)
            {
                throw new AssertionException(_discoveryDocument.Error);
            }
        }

        public TokenResponse GetAccessToken(string userName, string password)
        {
            TokenResponse response;
            try
            {
                response = _client.RequestPasswordTokenAsync(new PasswordTokenRequest
                {
                    Address = _discoveryDocument.TokenEndpoint,
                    UserName = userName,
                    Password = password,
                    ClientId = _clientId,
                    ClientSecret = _clientSecret,
                    Scope = _scope
                }).GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                throw new AssertionException(e.Message);
            }

            return response;
        }

        public string GetChangeEmailToken(string oldEmail, string newEmail, string accessToken, string label = "BudgetEnergie")
        {
            var response = new GetChangeEmailTokenRequest
            {
                Label = label,
                NewEmail = newEmail,
                OldEmail = oldEmail
            }.CallWith<GetChangeEmailTokenResponse>(_restSharpClient, accessToken);

            if (response.Header.StatusCode != HttpStatusCode.OK)
            {
                throw new AssertionException(response.Header.Message);
            }

            return response.Data.Token;
        }

        public RestResponseBase<ChangeEmailResponse> ChangeEmail(string oldEmail, string newEmail, string token, string label = "BudgetEnergie")
        {
            var response = new ChangeEmailRequest
            {
                Label = label,
                NewEmail = newEmail,
                OldEmail = oldEmail,
                Token = token
            }.CallWith<ChangeEmailResponse>(_restSharpClient);

            return response;
        }

        public RestResponseBase<GetTokenByStrategyResponse> GetTokenByStrategy(string token, string email, string strategy, string label = "BudgetEnergie")
        {
            var response = new GetTokenByStrategyRequest
            {
                Label = label,
                Email = email,
                Strategy = strategy
            }.CallWith<GetTokenByStrategyResponse>(_restSharpClient, token);

            if (response.Header.StatusCode != HttpStatusCode.OK)
            {
                throw new AssertionException(response.Header.Message);
            }

            return response;
        }
    }
}