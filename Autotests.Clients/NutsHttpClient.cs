﻿using System.Threading.Tasks;
using Allure.NUnit.Attributes;
using Autotests.Core.Helpers;
using Nuts.ApiClient;
using System.Net.Http;

namespace Autotests.Clients
{
    public class NutsHttpClient : ApiClientBase
    {

        public NutsHttpClient(System.Net.Http.HttpClient httpClient) : base(httpClient)
        {
        }

        [AllureStep("Call:[&request&]_With:NutsHttpClient")]
        public ApiResponse<TResponse> MakeRequest<TRequest, TResponse>(TRequest request) where TRequest : ApiRequestBase
        {
            var response = Call<TResponse>(request).GetAwaiter().GetResult();
            AllureHelpers.AddAttachments(request, response);
            return response;
        }

        [AllureStep("Call:[&request&]_With:NutsHttpClient")]
        public async Task<ApiResponse<TResponse>> MakeRequestAsync<TRequest, TResponse>(TRequest request) where TRequest : ApiRequestBase
        {
            var response = Call<TResponse>(request);
            AllureHelpers.AddAttachments(request, response);
            return await response;
        }

        [AllureStep("Call:[&request&]_With:NutsHttpClient")]
        public ApiResponse MakeRequest<TRequest>(TRequest request) where TRequest : ApiRequestBase
        {
            var response = Call(request).GetAwaiter().GetResult();
            AllureHelpers.AddAttachments(request, response);
            return response;
        }
    }
}