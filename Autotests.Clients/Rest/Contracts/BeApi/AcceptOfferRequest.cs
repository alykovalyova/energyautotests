﻿using System;
using Nuts.Prospect.Model.Core.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class AcceptOfferRequest : RestRequestBase
    {
        private const string Route = "/renewal/offer/accept";
        private const Method Method = RestSharp.Method.POST;

        [DefaultValueValidation] public Guid PropositionId { get; set; }
        [DefaultValueValidation] public Guid ExternalReference { get; set; }
        public bool IsCustomOffer { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}