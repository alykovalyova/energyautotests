﻿using System;
using Autotests.Clients.Rest.Models.BeApi.AcceptOffer;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class AcceptOfferResponse
    {
        public AcceptOfferData Data { get; set; }
    }
}