﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class ApproveOfferRequest : RestRequestBase
    {
        private const string Route = "/Prospect/ApproveOffer";
        private const Method Method = RestSharp.Method.GET;

        public string ExRef { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}