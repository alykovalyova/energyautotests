﻿using Nuts.Questionnaire.Contract.Rest;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class CreateLeadSurveyRequest : RestRequestBase
    {
        private const string Route = "/lead/survey";
        private const Method Method = RestSharp.Method.POST;
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<QuestionSurveySubmit> SurveyQuestions { get; set; }
        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}