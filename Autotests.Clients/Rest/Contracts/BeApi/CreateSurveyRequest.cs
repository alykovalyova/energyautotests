﻿using Nuts.Questionnaire.Contract.Rest;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class CreateSurveyRequest : RestRequestBase
    {
        private const string Route = "/crossell/survey";
        private const Method Method = RestSharp.Method.POST;

        public int RelationId { get; set; }
        public List<QuestionSurveySubmit> SurveyQuestions { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}