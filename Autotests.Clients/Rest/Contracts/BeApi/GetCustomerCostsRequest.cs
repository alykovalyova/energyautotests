﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetCustomerCostsRequest : RestRequestBase
    {
        private const string Route = "/contract/cost";
        private const Method Method = RestSharp.Method.POST;
        public int ContractId { get; set; }
        public string Period { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
