﻿using Autotests.Clients.Rest.Models.BeApi.GetCustomerCosts;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetCustomerCostsResponse
    {
        public long MonthlyAmount { get; set; }
        public List<EnergyConnection> EnergyConnections { get; set; }
    }
}
