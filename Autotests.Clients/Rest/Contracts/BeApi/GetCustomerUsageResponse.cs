﻿using Autotests.Clients.Rest.Models.BeApi.GetCustomerUsage;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetCustomerUsageResponse
    {
        public List<UsageEnergyConnection> EnergyConnections { get; set; }
    }

}
