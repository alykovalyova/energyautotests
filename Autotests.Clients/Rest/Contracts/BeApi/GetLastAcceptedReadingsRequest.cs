﻿using RestSharp;
using System.Collections.Generic;
using Autotests.Clients.Attributes;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [AsJsonArray]
    public class GetLastAcceptedReadingsRequest : RestRequestBase
    {
        private const string Route = "/Consumption/GetLastAcceptedReadings";
        private const Method Method = RestSharp.Method.POST;

        public List<string> Eans { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}