﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Nuts.Consumption.Model.Contract;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetLastAcceptedReadingsResponse
    {
        [JsonProperty(PropertyName = "Results")]
        public List<MeterReading> MeterReadings { get; set; }
    }
}