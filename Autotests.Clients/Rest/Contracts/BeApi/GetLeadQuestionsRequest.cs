﻿using Autotests.Clients.Rest.Models.BeApi.GetLeadQuestions;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetLeadQuestionsRequest : RestRequestBase
    {
        private const string Route = "/lead/questions";
        private const Method Method = RestSharp.Method.GET;

        public LeadQuestions Data { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}