﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetMandateRequest : RestRequestBase
    {
        private const string Route = " /dailyReading/mandate/{ContractId}";
        private const Method Method = RestSharp.Method.GET;

        public long ContractId { get; set; }

        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
