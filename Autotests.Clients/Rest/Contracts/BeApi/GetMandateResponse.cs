﻿using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetMandateResponse
    {
        public bool Mandate { get; set; }
    }
}
