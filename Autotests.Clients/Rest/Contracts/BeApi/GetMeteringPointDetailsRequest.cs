﻿using System.Collections.Generic;
using Nuts.ApiClient;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetMeteringPointDetailsRequest : ApiRequestBase
    {
        public List<string> EanIds { get; set; }

        public override string GetRouteUri()
        {
            return "/meteringpoint/details";
        }

        public override string GetRouteTemplate()
        {
            return "/meteringpoint/details";
        }

        public override ApiHttpMethod GetHttpMethod()
        {
            return ApiHttpMethod.Post;
        }
    }
}