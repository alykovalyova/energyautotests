﻿using Autotests.Clients.Rest.Models.BeApi.GetMeteringPointDetails;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetMeteringPointDetailsResponse
    {
        public MeteringPointDetailsData Data { get; set; }
    }
}