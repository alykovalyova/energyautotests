﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetOfferByContractIdRequest : RestRequestBase
    {
        private const string Route = "/renewal/offer/contract/{ContractId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public int ContractId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}