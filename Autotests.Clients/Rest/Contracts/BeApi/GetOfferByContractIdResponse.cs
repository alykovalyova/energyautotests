﻿using Autotests.Clients.Rest.Models.BeApi.GetOfferByContractId;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetOfferByContractIdResponse
    {
        public RenewalData Data { get; set; }
    }
}