﻿using Autotests.Clients.Attributes;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetOfferByRelationIdRequest : RestRequestBase
    {
        private const string Route = "/renewal/offer/isAvailable/{RelationId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long RelationId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}