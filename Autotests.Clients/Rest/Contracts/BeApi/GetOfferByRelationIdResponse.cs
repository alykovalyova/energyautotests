﻿using Autotests.Clients.Rest.Models.BeApi.GetOfferByRelationId;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetOfferByRelationIdResponse
    {
        public OfferByRelationData Data { get; set; }
    }
}