﻿using Newtonsoft.Json;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetOfferDataRequest : RestRequestBase
    {
        private const string Route = "/Prospect/GetOfferData";
        private const Method Method = RestSharp.Method.GET;

        [JsonProperty(PropertyName = "exRef")] public string ExRef { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}