﻿using Autotests.Clients.Rest.Models.BeApi.GetOfferData;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetOfferDataResponse
    {
        public GetOfferData Data { get; set; }
    }
}