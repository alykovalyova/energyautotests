﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetProdmanPropositionRequest : RestRequestBase
    {
        private const string Route = "/ProductDomain/GetProposition";
        private const Method Method = RestSharp.Method.GET;

        public Guid PropositionId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}