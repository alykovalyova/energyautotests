﻿using Autotests.Clients.Attributes;
using RestSharp;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [AsJsonArray]
    public class GetProdmanPropositionsRequest : RestRequestBase
    {
        private const string Route = "/ProductDomain/GetPropositions";
        private const Method Method = RestSharp.Method.POST;

        public List<string> Ids { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}