﻿using Autotests.Clients.Rest.Models.BeApi.GetProdmanPropositions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetProdmanPropositionsResponse
    {
        [JsonProperty(PropertyName = "Results")]
        public List<ProdmanProposition> ProdmanPropositions { get; set; }
    }
}