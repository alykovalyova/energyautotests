﻿using Newtonsoft.Json;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetProspectDataRequest : RestRequestBase
    {
        private const string Route = "/Prospect/GetProspectData";
        private const Method Method = RestSharp.Method.GET;

        [JsonProperty(PropertyName = "exRef")] public string ExRef { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}