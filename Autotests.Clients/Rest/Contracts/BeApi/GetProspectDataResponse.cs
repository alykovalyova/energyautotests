﻿using Autotests.Clients.Rest.Models.BeApi.GetProspectData;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetProspectDataResponse
    {
        public ProspectData Data { get; set; }
    }
}