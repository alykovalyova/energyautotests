﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetQuestionsRequest : RestRequestBase
    {
        private const string Route = "/crossell/questions";
        private const Method Method = RestSharp.Method.GET;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}