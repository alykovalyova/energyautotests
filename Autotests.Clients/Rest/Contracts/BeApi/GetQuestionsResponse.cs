﻿using System;
using System.Collections.Generic;
using Nuts.Questionnaire.Contract.Rest;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class GetQuestionsResponse
    {
        public List<Question> Questions { get; set; }
    }
}