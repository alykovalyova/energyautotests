﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GetSurveyByRelationIdRequest : RestRequestBase
    {
        private const string Route = "/crossell/survey/{RelationId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string RelationId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}