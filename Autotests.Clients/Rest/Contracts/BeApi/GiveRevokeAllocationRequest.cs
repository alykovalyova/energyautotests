﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GiveRevokeAllocationRequest : RestRequestBase
    {
        private const string Route = "/dailyReading/allocation";
        private const Method Method = RestSharp.Method.POST;

        public int ContractId { get; set; }
        public bool Permission { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
