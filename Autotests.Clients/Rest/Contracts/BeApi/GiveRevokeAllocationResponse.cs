﻿namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class GiveRevokeAllocationResponse
    {
        public bool Mandate { get; set; }
        public bool Allocation { get; set; }
    }
}
