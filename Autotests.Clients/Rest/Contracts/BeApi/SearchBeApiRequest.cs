﻿using Autotests.Clients.Attributes;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class SearchBeApiAddressRequest : RestRequestBase
    {
        private const string Route = "/address/search/{ZipCode}/{BuildingNr}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string ZipCode { get; set; }
        [AsUrlSegment] public string BuildingNr { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}