﻿using Autotests.Clients.Rest.Models.BeApi.SearchBeApiAddress;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class SearchBeApiAddressResponse
    {
        public Data Data { get; set; }
    }
}