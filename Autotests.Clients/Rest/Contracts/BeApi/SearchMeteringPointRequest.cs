﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class SearchMeteringPointRequest : RestRequestBase
    {
        private const string Route = "/meteringpoint/search/{ZipCode}/{BuildingNr}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string ZipCode { get; set; }
        [AsUrlSegment] public string BuildingNr { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}