﻿using Autotests.Clients.Rest.Models.BeApi.SearchMeteringPoint;
using System;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class SearchMeteringPointResponse
    {
        public SearchMpData Data { get; set; }
    }
}