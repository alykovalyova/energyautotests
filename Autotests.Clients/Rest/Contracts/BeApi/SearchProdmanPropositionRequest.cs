﻿using Nuts.ProdMan.Core.Enums;
using RestSharp;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class SearchProdmanPropositionRequest : RestRequestBase
    {
        private const string Route = "/ProductDomain/SearchPropositions";
        private const Method Method = RestSharp.Method.POST;

        public string SearchText { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public TypeOfClient ClientType { get; set; }
        public List<int> Durations { get; set; }
        public Status Status { get; set; }
        public bool IsEligibleAsRenewal { get; set; }
        public bool IsSalesEligible { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}