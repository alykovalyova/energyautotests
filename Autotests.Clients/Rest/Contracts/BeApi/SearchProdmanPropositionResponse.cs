﻿using Autotests.Clients.Rest.Models.BeApi.SearchProdmanProposition;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class SearchProdmanPropositionResponse
    {
        [JsonProperty(PropertyName = "Results")]
        public List<ProdmanProposition> ProdmanPropositions { get; set; }
    }
}