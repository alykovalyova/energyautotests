﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    public class UpdateMandateForContractIdRequest : RestRequestBase
    {
        private const string Route = "/dailyReading/mandate";
        private const Method Method = RestSharp.Method.POST;

        public int ContractId { get; set; }
        public bool Mandate { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
