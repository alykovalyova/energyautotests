﻿using Autotests.Clients.Attributes;
using Nuts.Consumption.Model.Contract;
using RestSharp;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [AsJsonArray]
    public class ValidateMeterReadingRequest : RestRequestBase
    {
        private const string Route = "/Consumption/ValidateMeterReading";
        private const Method Method = RestSharp.Method.POST;

        public List<ReadingsSetInfo> ReadingsSet { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}