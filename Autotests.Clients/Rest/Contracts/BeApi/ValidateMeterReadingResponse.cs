﻿using Autotests.Clients.Rest.Models.BeApi.ValidateMeterReading;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.BeApi
{
    [Serializable]
    public class ValidateMeterReadingResponse
    {
        [JsonProperty(PropertyName = "Results")]
        public List<ReadingsSetValidationResult> ValidationResults { get; set; }
    }
}