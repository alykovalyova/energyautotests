﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class CleanFakeTimeLineRequest : RestRequestBase
    {
        private const string Route = "/fake/v1/timeline";
        private const Method Method = RestSharp.Method.DELETE;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}