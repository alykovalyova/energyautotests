﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class GetFakeTimeLineRequest : RestRequestBase
    {
        private const string Route = "/fake/v1/timeline/{ContactPersonNumber}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public int ContactPersonNumber { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool OnlyUnread { get; set; }
        public bool OnlyWithIncompleteAction { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}