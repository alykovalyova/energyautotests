﻿using Autotests.Clients.Rest.Models.FakeTimeLine.GetFakeTimeLine;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class GetFakeTimeLineResponse
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public CustomerEvent[] CustomerEvents { get; set; }
    }
}