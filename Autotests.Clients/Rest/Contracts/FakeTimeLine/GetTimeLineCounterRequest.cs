﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class GetTimeLineCounterRequest : RestRequestBase
    {
        private const string Route = "/fake/v1/timeline/{ContactPersonNumber}/counter";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long ContactPersonNumber { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}