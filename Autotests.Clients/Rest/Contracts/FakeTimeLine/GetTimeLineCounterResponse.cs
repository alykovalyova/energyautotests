﻿using Autotests.Clients.Rest.Models.FakeTimeLine.GetTimeLineCounter;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class GetTimeLineCounterResponse
    {
        public Counter Counter { get; set; }
    }
}