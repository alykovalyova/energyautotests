﻿using Autotests.Clients.Rest.Models.FakeTimeLine.PutFakeEntity;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class PutFakeEntityRequest : RestRequestBase
    {
        private const string Route = "/fake/v1/timeline";
        private const Method Method = RestSharp.Method.PUT;

        public long ContactPersonNumber { get; set; }
        public Timeline[] TimeLines { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}