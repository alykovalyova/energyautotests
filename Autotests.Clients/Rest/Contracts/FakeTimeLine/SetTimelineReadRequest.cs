﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.FakeTimeLine
{
    public class SetTimelineReadRequest : RestRequestBase
    {
        private const string Route = "/fake/v1/timeline";
        private const Method Method = RestSharp.Method.PATCH;

        public string[] Ids { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}