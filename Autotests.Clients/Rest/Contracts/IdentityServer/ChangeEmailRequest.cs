﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.IdentityServer
{
    public class ChangeEmailRequest : RestRequestBase
    {
        private const string Route = "/v1/email";
        private const Method Method = RestSharp.Method.POST;

        public string Label { get; set; }
        public string NewEmail { get; set; }
        public string OldEmail { get; set; }
        public string Token { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
