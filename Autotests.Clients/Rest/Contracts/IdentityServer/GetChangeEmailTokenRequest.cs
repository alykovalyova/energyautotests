﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.IdentityServer
{
    public class GetChangeEmailTokenRequest : RestRequestBase
    {
        private const string Route = "/internal/v1/changeEmailToken";
        private const Method Method = RestSharp.Method.GET;

        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
        public string Label { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
