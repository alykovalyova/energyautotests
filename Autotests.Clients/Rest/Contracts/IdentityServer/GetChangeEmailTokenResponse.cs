﻿namespace Autotests.Clients.Rest.Contracts.IdentityServer
{
    public class GetChangeEmailTokenResponse
    {
        public string Token { get; set; }
    }
}
