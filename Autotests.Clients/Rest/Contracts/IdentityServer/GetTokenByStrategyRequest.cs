﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.IdentityServer
{
    public class GetTokenByStrategyRequest : RestRequestBase
    {
        private const string Route = "internal/v1/token";
        private const Method Method = RestSharp.Method.GET;

        public string Email { get; set; }
        public string Label { get; set; }
        public string Strategy { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
