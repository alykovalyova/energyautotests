﻿namespace Autotests.Clients.Rest.Contracts.IdentityServer
{
    public class GetTokenByStrategyResponse
    {
        public string Token { get; set; }
    }
}
