﻿using Autotests.Clients.Rest.Models.Insurance.CreateCustomer;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class CreateCustomerRequest : RestRequestBase
    {
        private const string Route = "/v1/customer";
        private const Method Method = RestSharp.Method.POST;

        public long LeadId { get; set; }
        public long RelationId { get; set; }
        public Person Person { get; set; }
        public Contact Contact { get; set; }
        public Address Address { get; set; }
        public PaymentDetails PaymentDetails { get; set; }
        public Package Package { get; set; }
        public string FamilySize { get; set; }
        public long MonthlyIncome { get; set; }

        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
