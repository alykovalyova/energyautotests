﻿using System;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class CreateCustomerResponse 
    {
        public Guid ResponseGuid { get; set; }
    }
}
