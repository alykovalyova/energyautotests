﻿using System.Collections.Generic;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class CreateOfferRequest : RestRequestBase
    {
        private const string Route = "/v1/offer";
        private const Method Method = RestSharp.Method.POST;
        public long BatchId { get; set; }
        public List<Models.Insurance.CreateOffer.Offer> Offers { get; set; }
        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
