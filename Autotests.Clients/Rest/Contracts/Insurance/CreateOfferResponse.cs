﻿using System;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class CreateOfferResponse 
    {
        public Guid ResponseGuid { get; set; }
    }
}
