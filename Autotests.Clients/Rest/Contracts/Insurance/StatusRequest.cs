﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class StatusRequest : RestRequestBase
    {
        private const string Route = "/status";
        private const Method Method = RestSharp.Method.GET;
        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
