﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class StatusResponse
    {
        public string StartOn { get; set; }
    }
}
