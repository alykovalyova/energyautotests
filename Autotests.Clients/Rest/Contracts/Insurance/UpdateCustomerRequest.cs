﻿using Autotests.Clients.Rest.Models.Insurance.UpdateCustomer;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class UpdateCustomerRequest : RestRequestBase
    {
        private const string Route = "/v1/customer/update";
        private const Method Method = RestSharp.Method.POST;

        public long RelationId { get; set; }
        public Package Package { get; set; }

        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
