﻿using System;

namespace Autotests.Clients.Rest.Contracts.Insurance
{
    public class UpdateCustomerResponse
    {
        public Guid ResponseGuid { get; set; }
    }
}
