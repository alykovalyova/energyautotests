﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class AddTvPackageRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/tvSubscription/package";
        private const Method Method = RestSharp.Method.POST;

        public string BundleId { get; set; }
        public string CustomerAccountId { get; set; }
        public string ServiceAccountId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}