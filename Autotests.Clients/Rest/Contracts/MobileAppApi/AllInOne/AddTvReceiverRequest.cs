﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class AddTvReceiverRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/tvSubscription/receiver";
        private const Method Method = RestSharp.Method.POST;

        public string BundleId { get; set; }
        public string CustomerAccountId { get; set; }
        public string ServiceAccountId { get; set; }
        public string DiscountId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}