﻿using Autotests.Clients.Rest.Models.MobileAppApi.DownloadContractById;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class DownloadContractByIdResponse
    {
        public PdfFile PdfFile { get; set; }
    }
}