﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class DownloadInvoiceByIdRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/serviceAccount/{ServiceAccountId}/invoice/{InvoiceId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long ServiceAccountId { get; set; }
        [AsUrlSegment] public long InvoiceId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}