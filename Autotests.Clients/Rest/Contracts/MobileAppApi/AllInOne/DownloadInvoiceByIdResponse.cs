﻿using Autotests.Clients.Rest.Models.MobileAppApi.DownloadInvoiceById;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class DownloadInvoiceByIdResponse
    {
        public PdfFile PdfFile { get; set; }
    }
}