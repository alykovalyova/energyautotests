﻿using System;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class DropVoipPackageRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/voip/packages";
        private const Method Method = RestSharp.Method.DELETE;

        public string BundleId { get; set; }
        public string CustomerAccountId { get; set; }
        public string ServiceAccountId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}