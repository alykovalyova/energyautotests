﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class GetContractsRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/serviceAccount/{ServiceAccountId}/{CustomerAccountId}/contract";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long ServiceAccountId { get; set; }
        [AsUrlSegment] public long CustomerAccountId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}