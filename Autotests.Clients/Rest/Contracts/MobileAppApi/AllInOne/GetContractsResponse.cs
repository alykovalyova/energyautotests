﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetContracts;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class GetContractsResponse
    {
        public Contract[] Contracts { get; set; }
    }
}