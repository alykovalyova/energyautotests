﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetDslSubscription;
using System;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class GetDslSubscriptionResponse
    {
        public DslInformation DslInformation { get; set; }
    }
}