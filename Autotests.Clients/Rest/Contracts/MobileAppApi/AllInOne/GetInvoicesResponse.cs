﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetInvoices;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class GetInvoicesResponse
    {
        public Invoice[] Invoices { get; set; }
    }
}