﻿using System;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class GetServiceAccountRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/serviceAccount";
        private const Method Method = RestSharp.Method.POST;

        public long[] CustomerAccountIds { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}