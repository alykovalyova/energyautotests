﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetServiceAccount;
using System;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class GetServiceAccountResponse
    {
        public ServiceAccountInformation[] ServiceAccountInformation { get; set; }
    }
}