﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetTvSubscription;
using System;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class GetTvSubscriptionResponse
    {
        public TvSubscriptionInfo TvSubscriptionInfo { get; set; }
    }
}