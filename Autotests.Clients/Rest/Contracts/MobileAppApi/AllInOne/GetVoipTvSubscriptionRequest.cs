﻿using System;
using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class GetVoipTvSubscriptionRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/voip/{ServiceAccountId}/{CustomerAccountId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string ServiceAccountId { get; set; }
        [AsUrlSegment] public string CustomerAccountId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}