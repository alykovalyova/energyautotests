﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetVoipTvSubscription;
using System;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    [Serializable]
    public class GetVoipTvSubscriptionResponse
    {
        public VoipSubscriptionInfo VoipSubscriptionInfo { get; set; }
    }
}