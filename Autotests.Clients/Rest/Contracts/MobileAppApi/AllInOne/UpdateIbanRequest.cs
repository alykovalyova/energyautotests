﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.AllInOne
{
    public class UpdateIbanRequest : RestRequestBase
    {
        private const string Route = "/allinone/v1/serviceAccount/iban";
        private const Method Method = RestSharp.Method.POST;

        public long? CustomerAccountId { get; set; }
        public long? ServiceAccountId { get; set; }
        public string Iban { get; set; }
        public string LastName { get; set; }
        public string PaymentMethod { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }

    public enum PaymentMethod
    {
        Unknown, DirectDebit, CustomerPays
    }
}