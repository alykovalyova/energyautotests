﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class DownloadEnergyVoucherRequest : RestRequestBase
    {
        private const string Route = "energy/v1/contract/{ContractId}/voucher/{VoucherId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public int ContractId { get; set; }
        [AsUrlSegment] public int VoucherId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}