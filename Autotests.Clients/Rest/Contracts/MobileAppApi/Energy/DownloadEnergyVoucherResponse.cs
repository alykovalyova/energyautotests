﻿using Autotests.Clients.Rest.Models.MobileAppApi.DownloadEnergyVoucher;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class DownloadEnergyVoucherResponse
    {
        public PdfFile PdfFile { get; set; }
    }
}