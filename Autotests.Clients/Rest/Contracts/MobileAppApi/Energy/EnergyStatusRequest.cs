﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class EnergyStatusRequest : RestRequestBase
    {
        private const string Route = "/energy/status";
        private const Method Method = RestSharp.Method.GET;
        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
