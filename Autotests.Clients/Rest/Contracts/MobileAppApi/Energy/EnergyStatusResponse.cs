﻿using System;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class EnergyStatusResponse
    {
        public DateTimeOffset StartOn { get; set; }
    }
}
