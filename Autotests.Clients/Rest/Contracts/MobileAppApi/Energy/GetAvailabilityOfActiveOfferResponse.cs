﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetAvailabilityOfActiveOffer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetAvailabilityOfActiveOfferResponse
    {
        public OfferDetails Offer { get; set; }
    }
}
