﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetContractInfoByRelationIds;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetContractInfoByRelationIdsResponse
    {
        public ContractsInfo[] ContractsInfo { get; set; }
    }
}