﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetContractInvoicePdfRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/contract/{ContractId}/invoice/{InvoiceId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string ContractId { get; set; }
        [AsUrlSegment] public string InvoiceId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}