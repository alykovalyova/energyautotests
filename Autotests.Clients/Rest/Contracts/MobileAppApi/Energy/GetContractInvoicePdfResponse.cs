﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicePdf;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetContractInvoicePdfResponse
    {
        public PdfFile PdfFile { get; set; }
    }
}