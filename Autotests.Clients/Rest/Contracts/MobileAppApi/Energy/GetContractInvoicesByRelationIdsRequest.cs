﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetContractInvoicesByRelationIdsRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/customer/invoice";
        private const Method Method = RestSharp.Method.POST;

        public long[] RelationIds { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}