﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicesByRelationIds;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetContractInvoicesByRelationIdsResponse
    {
        public AddressInvoice[] AddressInvoices { get; set; }
    }
}