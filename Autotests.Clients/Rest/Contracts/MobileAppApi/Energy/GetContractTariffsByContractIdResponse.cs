﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetContractTariffsByContractId;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetContractTariffsByContractIdResponse
    {
        public string ContractName { get; set; }
        public List<Tariff> Tariffs { get; set; }
    }
}