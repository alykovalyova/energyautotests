﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetCustomerBalanceByContractIdRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/contract/{ContractId}/balance";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string ContractId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}