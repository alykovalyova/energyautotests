﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetCustomerBalanceByContractIdResponse
    {
        public double MonthlyAmount { get; set; }
        public double? Balance { get; set; }
        public double Production { get; set; }
        public bool IsSmartMeter { get; set; }
        public bool ChangeMonthlyAmountInProgress { get; set; }
    }
}