﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetCustomerTariffsPdfRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/contract/{ContractId}/tariff/{TariffId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public int ContractId { get; set; }
        [AsUrlSegment] public int TariffId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}