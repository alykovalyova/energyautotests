﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerTariffsPdf;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetCustomerTariffsPdfResponse
    {
        public PdfFile PdfFile { get; set; }
    }
}