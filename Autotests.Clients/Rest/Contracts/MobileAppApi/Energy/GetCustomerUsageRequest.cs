﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetCustomerUsageRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/contract/{ContractId}/usage";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public int ContractId { get; set; }
        public string Period { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}