﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerUsage;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetCustomerUsageResponse
    {
        public EnergyConnection[] EnergyConnections { get; set; }
    }
}