﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetIsCompensationEligibleResponse
    {
        public bool Eligible { get; set; }
    }
}