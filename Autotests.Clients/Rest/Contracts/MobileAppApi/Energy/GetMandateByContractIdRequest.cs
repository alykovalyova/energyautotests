﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetMandateByContractIdRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/dailyReading/mandate/{ContractId}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long ContractId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}