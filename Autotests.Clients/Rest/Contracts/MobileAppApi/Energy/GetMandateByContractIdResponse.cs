﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetMandateByContractIdResponse
    {
        public bool Mandate { get; set; }
    }
}