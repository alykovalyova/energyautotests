﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetMonthlyAmountResponse
    {
        public double minAmount { get; set; }
        public double maxAmount { get; set; }
        public double currentAmount { get; set; }
        public double? suggestedAmount { get; set; }
        public bool changeMonthlyAmountInProgress { get; set; }
    }
}
