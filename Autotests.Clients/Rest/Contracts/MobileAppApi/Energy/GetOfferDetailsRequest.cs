﻿using Autotests.Clients.Attributes;
using Autotests.Clients.Rest;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetOfferDetailsRequest : RestRequestBase
    {
        private const string Route = "energy/v1/contract/{ContractId}/offer-details";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public int ContractId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
