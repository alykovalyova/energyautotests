﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetOfferDetailsByContractId;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetOfferDetailsResponse
    {
        public Renewal Renewal { get; set; }
        public List<Offer> Offers { get; set; }
    }
}
