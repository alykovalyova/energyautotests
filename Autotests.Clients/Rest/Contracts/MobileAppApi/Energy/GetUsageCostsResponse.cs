﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetUsageCosts;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetUsageCostsResponse
    {
        public decimal MonthlyAmount { get; set; }

        public List<EnergyCostConnection> EnergyConnections { get; set; }
    }
}