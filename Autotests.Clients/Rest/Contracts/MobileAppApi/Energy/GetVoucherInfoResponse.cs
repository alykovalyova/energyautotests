﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GetVoucherInfoResponse
    {
        public int Id { get; set; }
        public string VoucherName { get; set; }
    }
}