﻿using Autotests.Clients.Rest;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GiveOrRevokeAllocationRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/dailyReading/allocation";
        private const Method Method = RestSharp.Method.POST;
    
        public int? ContractId { get; set; }
        public bool? Permission { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}