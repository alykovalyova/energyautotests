﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class GiveOrRevokeMandateRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/dailyReading/mandate";
        private const Method Method = RestSharp.Method.POST;

        public int? ContractId { get; set; }
        public bool? Mandate { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}