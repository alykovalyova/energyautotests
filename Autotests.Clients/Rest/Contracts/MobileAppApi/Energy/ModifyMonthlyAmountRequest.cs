﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class ModifyMonthlyAmountRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/contract/monthlyAmount";
        private const Method Method = RestSharp.Method.POST;

        public int ContractId { get; set; }
        public double CurrentAmount { get; set; }
        public double NewAmount { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
