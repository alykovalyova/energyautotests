﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Energy
{
    public class UpdateCustomerPaymentDetailsRequest : RestRequestBase
    {
        private const string Route = "/energy/v1/customer/iban";
        private const Method Method = RestSharp.Method.POST;

        public int? CustomerNumber { get; set; }
        public string Iban { get; set; }
        public string AccountHolder { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}