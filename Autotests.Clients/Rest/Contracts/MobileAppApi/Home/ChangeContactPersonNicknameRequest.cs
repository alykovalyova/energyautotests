﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class ChangeContactPersonNicknameRequest : RestRequestBase
    {
        private const string Route = "/home/v1/contactPerson/nickname";
        private const Method Method = RestSharp.Method.POST;

        public string Nickname { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}