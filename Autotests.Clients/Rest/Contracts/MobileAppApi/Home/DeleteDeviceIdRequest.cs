﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class DeleteDeviceIdRequest : RestRequestBase
    {
        private const string Route = "/home/v1/device";
        private const Method Method = RestSharp.Method.DELETE;

        public string DeviceId { get; set; }

        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}