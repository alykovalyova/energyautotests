﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetActionsRequest : RestRequestBase
    {
        private const string Route = "/home/v1/timeline/action";
        private const Method Method = RestSharp.Method.GET;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}