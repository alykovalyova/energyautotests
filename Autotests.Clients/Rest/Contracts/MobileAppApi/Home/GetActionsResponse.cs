﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetActions;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetActionsResponse
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public TimelineEvent[] TimelineEvents { get; set; }
    }
}