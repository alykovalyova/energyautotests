﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetContactPerson;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetContactPersonResponse
    {
        public ContactPerson ContactPerson { get; set; }
    }
}