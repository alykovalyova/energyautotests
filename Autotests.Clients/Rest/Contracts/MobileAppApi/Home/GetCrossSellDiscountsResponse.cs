﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetCrossSellDiscountsResponse
    {
        public decimal? TotalDiscount { get; set; }
    }
}