﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetCrossSellOffersRequest : RestRequestBase
    {
        private const string Route = "/home/v1/crossSell/offer";
        private const Method Method = RestSharp.Method.GET;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}