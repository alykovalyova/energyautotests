﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetCrossSellOffers;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetCrossSellOffersResponse
    {
        public CrossSellOffer[] CrossSellOffers { get; set; }
    }
}