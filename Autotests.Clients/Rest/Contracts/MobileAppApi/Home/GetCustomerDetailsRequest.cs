﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetCustomerDetailsRequest : RestRequestBase
    {
        private const string Route = "/home/v1/customer/{CustomerNumber}";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long CustomerNumber { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}