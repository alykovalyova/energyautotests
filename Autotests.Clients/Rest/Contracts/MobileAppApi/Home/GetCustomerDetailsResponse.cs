﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerDetails;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetCustomerDetailsResponse
    {
        public Customer Customer { get; set; }
    }
}