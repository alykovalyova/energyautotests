﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetEventsRequest : RestRequestBase
    {
        private const string Route = "/home/v1/timeline";
        private const Method Method = RestSharp.Method.GET;

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public bool OnlyUnread { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}