﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetEvents;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetEventsResponse
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public TimelineEvent[] TimelineEvents { get; set; }
    }
}