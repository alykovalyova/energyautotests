﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetTimelineCounters;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class GetTimelineCountersResponse
    {
        public Counter Counter { get; set; }
    }
}