﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class SetTimeLinesReadRequest : RestRequestBase
    {
        private const string Route = "/home/v1/timeline";
        private const Method Method = RestSharp.Method.PATCH;

        public string[] Ids { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}