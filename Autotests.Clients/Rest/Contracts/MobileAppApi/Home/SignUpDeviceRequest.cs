﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class SignUpDeviceRequest : RestRequestBase
    {
        private const string Route = "/home/v1/device";
        private const Method Method = RestSharp.Method.POST;

        public string DeviceId { get; set; }
        public string MessageType { get; set; }

        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
