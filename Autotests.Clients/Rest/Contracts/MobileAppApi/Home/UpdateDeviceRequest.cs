﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Home
{
    public class UpdateDeviceRequest : RestRequestBase
    {
        private const string Route = "/home/v1/device";
        private const Method Method = RestSharp.Method.PUT;

        public string CurrentDeviceId { get; set; }
        public string NewDeviceId { get; set; }

        public override string GetRoute() => Route;
        public override Method GetHttpMethod() => Method;
    }
}
