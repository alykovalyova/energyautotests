﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class CreateExtraCreditOrderResponse
    {
        public string PaymentUrl { get; set; }

        public string TransactionId { get; set; }

        public double PaymentAmount { get; set; }
    }
}