﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class CreateExtraDataOrderRequest : RestRequestBase
    {
        private const string Route = "mobile/v1/extraData/payment";
        private const Method Method = RestSharp.Method.POST;

        public long SubscriptionId { get; set; }

        public long ProductId { get; set; }

        public string ReturnUrl { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
