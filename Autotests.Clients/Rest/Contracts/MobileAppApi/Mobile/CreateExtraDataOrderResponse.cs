﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class CreateExtraDataOrderResponse
    {
        public string PaymentUrl { get; set; }

        public double PaymentAmount { get; set; }

        public long TransactionId { get; set; }
    }
}
