﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetExtraCreditBundlesRequest : RestRequestBase
    {
        private const string Route = "/mobile/v1/extraCredit";
        private const Method Method = RestSharp.Method.GET;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}