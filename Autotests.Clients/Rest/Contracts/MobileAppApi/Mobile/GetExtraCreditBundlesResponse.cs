﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetExtraCreditBundles;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetExtraCreditBundlesResponse
    {
        public decimal MaximumBundleCredit { get; set; }
        public Bundle[] Bundles { get; set; }
    }
}