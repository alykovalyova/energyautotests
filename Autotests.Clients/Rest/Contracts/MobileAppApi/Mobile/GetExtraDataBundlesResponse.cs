﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetExtraDataBundles;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetExtraDataBundlesResponse
    {
        public Bundle[] Bundles { get; set; }
    }
}