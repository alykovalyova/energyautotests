﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetGeneralTermById;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetGeneralTermByIdResponse
    {
        public PdfFile PdfFile { get; set; }
    }
}