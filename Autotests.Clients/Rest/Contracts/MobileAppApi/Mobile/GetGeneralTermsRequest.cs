﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetGeneralTermsRequest : RestRequestBase
    {
        private const string Route = "/mobile/v1/subscription/{SubscriptionId}/term";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public long SubscriptionId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}