﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetGeneralTerms;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetGeneralTermsResponse
    {
        public Term[] Terms { get; set; }
    }
}