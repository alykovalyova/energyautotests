﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetInvoicesBySubscriptionId;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetInvoicesBySubscriptionIdResponse
    {
        public Invoice[] Invoices { get; set; }
    }
}