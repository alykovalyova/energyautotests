﻿using Autotests.Clients.Attributes;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetPaymentStatusRequest : RestRequestBase
    {
        private const string Route = "/mobile/v1/payment/{TransactionId}/status";
        private const Method Method = RestSharp.Method.GET;

        [AsUrlSegment] public string TransactionId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}