﻿namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetPaymentStatusResponse
    {
        public string Status { get; set; }
    }
}