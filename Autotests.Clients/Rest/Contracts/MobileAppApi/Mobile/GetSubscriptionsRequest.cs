﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetSubscriptionsRequest : RestRequestBase
    {
        private const string Route = "/mobile/v1/subscriptions";
        private const Method Method = RestSharp.Method.POST;

        public long[] SubscriptionIds { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}