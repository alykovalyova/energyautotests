﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetSubscriptions;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetSubscriptionsResponse
    {
        public Subscription[] Subscriptions { get; set; }
    }
}