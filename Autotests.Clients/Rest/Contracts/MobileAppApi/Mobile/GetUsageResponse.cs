﻿using Autotests.Clients.Rest.Models.MobileAppApi.GetUsage;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class GetUsageResponse
    {
        public Extra Extra { get; set; }

        public Usage Usage { get; set; }

        public Bundle Bundle { get; set; }
    }
}
