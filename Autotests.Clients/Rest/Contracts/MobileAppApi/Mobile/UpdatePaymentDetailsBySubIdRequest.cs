﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.MobileAppApi.Mobile
{
    public class UpdatePaymentDetailsBySubIdRequest : RestRequestBase
    {
        private const string Route = "/mobile/v1/iban";
        private const Method Method = RestSharp.Method.POST;

        public long SubscriptionId { get; set; }
        public string Iban { get; set; }
        public string Name { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}