﻿using Autotests.Clients.Rest.Models.ResellerApi.AddMmCustomer;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class AddMmCustomerRequest : RestRequestBase
    {
        private const string Route = "AddMediaMarktCustomer/v1/";
        private const Method Method = RestSharp.Method.POST;

        public string ExternalReference { get; set; }
        public int SourceId { get; set; }
        public string AgentIdentification { get; set; }
        public PersonalDetails PersonalDetails { get; set; }
        public ConnectionAddress ConnectionAddress { get; set; }
        public ContractDetails ContractDetails { get; set; }
        public CorrespondenceAddress CorrespondenceAddress { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}