﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class AddMmCustomerResponse
    {
        public string Voucher { get; set; }
    }
}