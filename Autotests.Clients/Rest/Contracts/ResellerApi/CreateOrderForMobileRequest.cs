﻿using Autotests.Clients.Rest.Models.ResellerApi.CreateOrderForMobile;
using RestSharp;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class CreateOrderForMobileRequest : RestRequestBase
    {
        private const string Route = "Mobile/CreateOrder/v1/";
        private const Method Method = RestSharp.Method.POST;

        public string ResellerId { get; set; }
        public SubscriptionOrderCreation Subscription { get; set; }
        public OrderCreation Order { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}