﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class CreateOrderForMobileResponse
    {
        public int OrderId { get; set; }
        public int SubscriptionId { get; set; }
        public string OrderNumber { get; set; }
        public string Url { get; set; }
    }
}
