﻿using Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1;
using RestSharp;
using System;
using Mandate = Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2.Mandate;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class CreateOrderRequestV1 : RestRequestBase
    {
        private const string Route = "AllInOne/CreateOrder/v1";
        private const Method Method = RestSharp.Method.POST;

        public SubscribeBundleWithDiscount[] SubscribeBundleWithDiscounts { get; set; }
        public Mandate Mandate { get; set; }
        public AllInOneCustomer Customer { get; set; }
        public IngAddress ShippingAddress { get; set; }
        public VoipSettings VoipSettings { get; set; }
        public int ResellerId { get; set; }
        public long VendorId { get; set; }
        public long PropositionId { get; set; }
        public long[] CashbackIdList { get; set; }
        public bool? MandateGiven { get; set; }
        public string PaymentMethod { get; set; }
        public DateTimeOffset SaleDate { get; set; }
        public DateTimeOffset StartWishDate { get; set; }
        public bool? IsInstallationHelpNeeded { get; set; }
        public string PortingNumber { get; set; }
        public string CurrentProvider { get; set; }
        public bool? IsSwitchServiceNeeded { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}