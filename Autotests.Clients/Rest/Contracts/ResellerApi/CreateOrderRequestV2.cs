﻿using Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class CreateOrderRequestV2 : RestRequestBase
    {
        private const string Route = "AllInOne/CreateOrder/v2";
        private const Method Method = RestSharp.Method.POST;

        public long VendorId { get; set; }
        public long PropositionId { get; set; }
        public SubscribeBundleWithDiscount[] SubscribeBundleWithDiscounts { get; set; }
        public int[] CashbackIdList { get; set; }
        public bool? MandateGiven { get; set; }
        public Mandate Mandate { get; set; }
        public string PaymentMethod { get; set; }
        public Customer Customer { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public DateTimeOffset SaleDate { get; set; }
        public DateTimeOffset StartWishDate { get; set; }
        public bool? IsInstallationHelpNeeded { get; set; }
        public bool IsRemoteInstallationHelpNeeded { get; set; }
        public bool IsEarlyTermination { get; set; }
        public string PortingNumber { get; set; }
        public string CurrentProvider { get; set; }
        public bool? IsSwitchServiceNeeded { get; set; }
        public VoipSettings VoipSettings { get; set; }
        public string CreatedBy { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}