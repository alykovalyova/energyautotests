﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class CreateOrderResponseV1
    {
        public long OrderId { get; set; }
    }
}