﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class CreateOrderResponseV2
    {
        public long OrderId { get; set; }
    }
}