﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class GetAvailablePropositionsByAddressRequestV1 : RestRequestBase
    {
        private const string Route = "AllInOne/GetAvailablePropositionsbyAddress/v1/";
        private const Method Method = RestSharp.Method.GET;

        public string SalesOfficeId { get; set; }
        public string Zip { get; set; }
        public string HouseNumber { get; set; }
        public string Extension { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}