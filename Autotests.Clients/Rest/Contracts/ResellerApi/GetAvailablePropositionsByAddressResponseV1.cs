﻿using Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV1;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetAvailablePropositionsByAddressResponseV1
    {
        public PropositionByAddress PropositionsByAddress { get; set; }
    }
}