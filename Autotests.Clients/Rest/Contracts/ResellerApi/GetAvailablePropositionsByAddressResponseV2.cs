﻿using Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV2;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetAvailablePropositionsByAddressResponseV2
    {
        public string[] Remarks { get; set; }
        public Proposition[] Propositions { get; set; }
        public string[] PossibleExtentions { get; set; }
        public PqccResolvedAddress PqccResolvedAddress { get; set; }
        public int SpeedLimit { get; set; }
        public ConnectionСharacteristic[] ConnectionCharacteristics { get; set; }
    }
}