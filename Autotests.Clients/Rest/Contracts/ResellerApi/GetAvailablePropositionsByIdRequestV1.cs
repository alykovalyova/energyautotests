﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class GetAvailablePropositionsByIdRequestV1 : RestRequestBase
    {
        private const string Route = "AllInOne/GetAvailablePropositionById/v1";
        private const Method Method = RestSharp.Method.GET;

        public string PropositionId { get; set; }
        public string Zip { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}