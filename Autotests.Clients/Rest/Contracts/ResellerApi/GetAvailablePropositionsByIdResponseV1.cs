﻿using Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV1;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetAvailablePropositionsByIdResponseV1
    {
        public AllInOnePropositionWithoutDuration Proposition { get; set; }
    }
}