﻿using Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV2;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetAvailablePropositionsByIdResponseV2
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public int ContractDuration { get; set; }
        public Product[] Products { get; set; }
        public bool IsDefault { get; set; }
    }
}