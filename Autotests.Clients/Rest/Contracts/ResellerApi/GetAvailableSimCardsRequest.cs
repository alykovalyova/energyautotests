﻿using RestSharp;
using System;
using Autotests.Clients.Enums.ResellerApi;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetAvailableSimCardsRequest : RestRequestBase
    {
        private const string Route = "Mobile/GetAvailableSimCards/v1/";
        private const Method Method = RestSharp.Method.GET;

        public SimCardType SimCardType { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}