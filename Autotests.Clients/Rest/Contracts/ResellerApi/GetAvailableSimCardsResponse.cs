﻿using Autotests.Clients.Rest.Models.ResellerApi.GetAvailableSimCards;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetAvailableSimCardsResponse
    {
        public List<Content> Content { get; set; }
    }
}