﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetCreditCheckRequest : RestRequestBase
    {
        private const string Route = "CreditCheck/v1/";
        private const Method Method = RestSharp.Method.GET;

        public string Initials { get; set; }
        public string Surname { get; set; }
        public string DateOfBirth { get; set; }
        public string PostalCode { get; set; }
        public string CreatedBy { get; set; }
        public string HouseNumber { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}