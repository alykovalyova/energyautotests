﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetCreditCheckResponse
    {
        public bool IsCreditWorthy { get; set; }
        public string ScoreBasedOn { get; set; }
        public string ScoreFrom { get; set; }
    }
}