﻿using Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetDefaultQuoteRequest : RestRequestBase
    {
        private const string Route = "GetDefaultQuote/v1";
        private const Method Method = RestSharp.Method.POST;

        public List<Proposition> Propositions { get; set; }
        public List<Connection> Connections { get; set; }
        public string ZipCode { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}