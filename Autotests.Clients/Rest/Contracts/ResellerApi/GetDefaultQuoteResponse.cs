﻿using Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class GetDefaultQuoteResponse
    {
        public List<Offer> Offers { get; set; }
    }
}