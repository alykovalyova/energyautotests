﻿using Autotests.Clients.Rest.Models.ResellerApi.GetEndDate;
using RestSharp;
using System;
using PermissionGrand = Autotests.Clients.Rest.Models.ResellerApi.GetEndDate.PermissionGrand;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetEndDateRequest : RestRequestBase
    {
        private const string Route = "GetEndDate/v2?";
        private const Method Method = RestSharp.Method.POST;

        public GasConnection GasConnection { get; set; }
        public PermissionGrand PermissionGrand { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}