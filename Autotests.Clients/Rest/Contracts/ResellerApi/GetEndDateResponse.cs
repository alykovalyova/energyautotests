﻿using Autotests.Clients.Rest.Models.ResellerApi.GetEndDate;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetEndDateResponse
    {
        public ConnectionInfo ElectricityConnection { get; set; }
        public ConnectionInfo GasConnection { get; set; }
    }
}