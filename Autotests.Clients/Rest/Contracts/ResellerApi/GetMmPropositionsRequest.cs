﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMmPropositions;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMmPropositionsRequest : RestRequestBase
    {
        private const string Route = "GetMMPropositions/v1";
        private const Method Method = RestSharp.Method.POST;

        public int LeadSourceId { get; set; }
        public bool? IsCustomer { get; set; }
        public string EmployeeBE { get; set; }
        public PersonalDetails PersonalDetails { get; set; }
        public ConnectionAddress ConnectionAddress { get; set; }
        public DateTimeOffset MoveInDate { get; set; }
        public DateTimeOffset CerDate { get; set; }
        public bool? IsEdsnOffline { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
