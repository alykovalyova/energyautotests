﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMmPropositionsResponse
    {
        public List<Models.ResellerApi.GetMmPropositions.Proposition> Propositions { get; set; }
        public bool IsRenewal { get; set; }
        public bool IsCreditWorthy { get; set; }
        public bool HasFine { get; set; }
        public string ContractStartDate { get; set; }
    }
}
