﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMmQuoteRequest : RestRequestBase
    {
        private const string Route = "GetMMQuote/v2/";
        private const Method Method = RestSharp.Method.POST;

        public int LeadSourceId { get; set; }
        public bool IsCustomer { get; set; }
        public string EmployeeBE { get; set; }
        public PersonalDetails PersonalDetails { get; set; }
        public ConnectionAddress ConnectionAddress { get; set; }
        public string MoveInDate { get; set; }
        public string CerDate { get; set; }
        public bool IsEdsnOffline { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}