﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMmQuoteResponse
    {
        public MonthlyInstallment MonthlyInstallment { get; set; }
        public MonthlyAmountRawTotal MonthlyAmountRawTotal { get; set; }
        public YearlyAmountTotal YearlyAmountTotal { get; set; }
        public YearlyAmountIncludingCashBack YearlyAmountIncludingCashBack { get; set; }
        public CashBack CashBack { get; set; }
        public Voucher Voucher { get; set; }
        public List<Quote> Quotes { get; set; }
        public string ContractDuration { get; set; }
        public string PropositionId { get; set; }
        public bool IsRenewal { get; set; }
        public bool IsCreditWorthy { get; set; }
        public bool HasFine { get; set; }
        public string ContractStartDate { get; set; }
    }
}