﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMpOfferPhaseRequest : RestRequestBase
    {
        private const string Route = "GetMp/v2";
        private const Method Method = RestSharp.Method.POST;

        public List<MeteringPointDetail> MeteringPointDetails { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}