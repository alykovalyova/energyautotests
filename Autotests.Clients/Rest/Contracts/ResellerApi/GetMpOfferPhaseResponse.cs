﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMpOfferPhaseResponse
    {
        public List<MeteringPointInfo> MpInformation { get; set; }
    }
}