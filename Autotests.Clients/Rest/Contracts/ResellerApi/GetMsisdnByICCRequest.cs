﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class GetMsisdnByIccRequest : RestRequestBase
    {
        private const string Route = "/Mobile/GetMsisdnByICC/v1/";
        private const Method Method = RestSharp.Method.GET;

        public string Icc { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
