﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetMsisdnByIccResponse
    {
        public string Icc { get; set; }
        public string Msisdn { get; set; }
        public string Puk { get; set; }
    }
}
