﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetOrderDetailsRequest : RestRequestBase
    {
        private const string Route = "Mobile/GetOrderDetails/v1/";
        private const Method Method = RestSharp.Method.GET;

        public long OrderId { get; set; }
        public string ResellerId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}