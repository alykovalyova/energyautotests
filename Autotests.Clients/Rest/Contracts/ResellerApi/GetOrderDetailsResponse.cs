﻿using Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetOrderDetailsResponse
    {
        public OrderContent Content { get; set; }
    }
}