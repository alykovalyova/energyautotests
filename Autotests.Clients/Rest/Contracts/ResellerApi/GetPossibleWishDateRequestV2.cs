﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class GetPossibleWishDateRequestV2 : RestRequestBase
    {
        private const string Route = "AllInOne/GetPossibleWishdate/v2";
        private const Method Method = RestSharp.Method.GET;

        public string IsOverstapService { get; set; }
        public string IsUrgent { get; set; }
        public string IsZiggoOrKpn { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}