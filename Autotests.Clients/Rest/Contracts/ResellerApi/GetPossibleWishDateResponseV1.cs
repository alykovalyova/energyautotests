﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetPossibleWishDateResponseV1
    {
        public string WishDate { get; set; }
    }
}