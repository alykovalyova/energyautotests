﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetPossibleWishDateResponseV2
    {
        public string WishDate { get; set; }
    }
}