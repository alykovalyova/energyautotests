﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetPropositionsByResellerRequest : RestRequestBase
    {
        private const string Route = "GetPropositionsByReseller/v1";
        private const Method Method = RestSharp.Method.GET;

        public Guid ResellerId { get; set; }
        public int? SalesChannelId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}