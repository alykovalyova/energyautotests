﻿using Autotests.Clients.Rest.Models.ResellerApi.GetPropositionsByReseller;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetPropositionsByResellerResponse
    {
        public List<SalesChannel> SalesChannels { get; set; }
    }
}