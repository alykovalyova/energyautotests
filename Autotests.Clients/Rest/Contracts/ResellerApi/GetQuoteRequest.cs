﻿using Autotests.Clients.Rest.Models.ResellerApi.GetQuote;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetQuoteRequest : RestRequestBase
    {
        private const string Route = "GetQuote/v2/";
        private const Method Method = RestSharp.Method.POST;

        public List<Proposition> Propositions { get; set; }
        public List<Connection> Connections { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}