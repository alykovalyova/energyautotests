﻿using Autotests.Clients.Rest.Models.ResellerApi.GetQuote;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetQuoteResponse
    {
        public List<Offer> Offers { get; set; }
    }
}