﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetRegisterLeadRequest : RestRequestBase
    {
        private const string Route = "RegisterLead/v1/";
        private const Method Method = RestSharp.Method.POST;

        public bool IsWarmLead { get; set; }
        public int LeadSourceId { get; set; }
        public string EmployeeMM { get; set; }
        public string EmployeeBE { get; set; }
        public string Department { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}