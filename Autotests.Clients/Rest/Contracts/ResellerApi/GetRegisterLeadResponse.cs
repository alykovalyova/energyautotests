﻿using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetRegisterLeadResponse
    {
        public string MMLeadId { get; set; }
    }
}