﻿using Autotests.Clients.Attributes;
using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [GetToken(Username = "a.kovalyova", Password = "ol,!QAZ234%")]
    [Serializable]
    public class GetTokenRequest : RestRequestBase
    {
        private const string Route = "oauth/token";
        private const Method Method = RestSharp.Method.POST;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}