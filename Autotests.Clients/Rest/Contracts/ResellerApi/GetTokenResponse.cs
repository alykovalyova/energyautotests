﻿using System;
using Newtonsoft.Json;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class GetTokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("roles")]
        public string Roles { get; set; }
    }
}