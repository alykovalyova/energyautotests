﻿using RestSharp;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    public class PayOrderRequest : RestRequestBase
    {
        private const string Route = "Mobile/PayOrder/v1/";
        private const Method Method = RestSharp.Method.GET;

        public int OrderId { get; set; }
        public string ResellerId { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}
