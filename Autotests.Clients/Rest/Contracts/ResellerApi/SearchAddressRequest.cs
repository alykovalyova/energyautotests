﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SearchAddressRequest : RestRequestBase
    {
        private const string Route = "Mobile/SearchAddress/v1/";
        private const Method Method = RestSharp.Method.GET;

        public string ZipCode { get; set; }
        public int BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string City { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}