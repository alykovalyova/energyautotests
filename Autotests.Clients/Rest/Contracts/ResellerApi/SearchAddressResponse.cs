﻿using Autotests.Clients.Rest.Models.ResellerApi.SearchAddress;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SearchAddressResponse
    {
        public Address Address { get; set; }
    }
}