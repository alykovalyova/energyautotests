﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SearchBagAddressRequest : RestRequestBase
    {
        private const string Route = "SearchBagAddress/v1/";
        private const Method Method = RestSharp.Method.GET;

        public string ZipCode { get; set; }
        public string BuildingNr { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}