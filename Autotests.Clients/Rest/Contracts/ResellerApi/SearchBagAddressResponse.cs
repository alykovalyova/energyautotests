﻿using Autotests.Clients.Rest.Models.ResellerApi.SearchBagAddress;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SearchBagAddressResponse
    {
        public List<BagAddress> Addresses { get; set; }
    }
}