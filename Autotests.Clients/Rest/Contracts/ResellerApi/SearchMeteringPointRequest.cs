﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SearchMeteringPointRequest : RestRequestBase
    {
        private const string Route = "SearchMP/v2/";
        private const Method Method = RestSharp.Method.GET;

        public string ZipCode { get; set; }
        public string BuildingNumber { get; set; }
        public string BuildingNumberEx { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}