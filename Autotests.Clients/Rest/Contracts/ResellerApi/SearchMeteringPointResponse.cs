﻿using Autotests.Clients.Rest.Models.ResellerApi.SearchMeteringPoint;
using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SearchMeteringPointResponse
    {
        public List<MeteringPoint> MeteringPoints { get; set; }
    }
}