﻿using RestSharp;
using System;

namespace Autotests.Clients.Rest.Contracts.ResellerApi
{
    [Serializable]
    public class SendConsentPdfRequest : RestRequestBase
    {
        private const string Route = "SendConsentPdf/v1/";
        private const Method Method = RestSharp.Method.POST;

        public DateTime SignDate { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public string ZipCode { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string ConsentPdf { get; set; }
        public string ConsentPdfMd5Hash { get; set; }

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}