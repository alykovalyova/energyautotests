﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.AcceptOffer
{
    [Serializable]
    public class AcceptOfferData
    {
        public bool IsAccepted { get; set; }
    }
}