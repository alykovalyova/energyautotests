﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetCustomerCosts
{
    [Serializable]
    public class CostPeriod
    {
        public DateTimeOffset Date { get; set; }
        public long Actual { get; set; }
        public long Expected { get; set; }
    }
}
