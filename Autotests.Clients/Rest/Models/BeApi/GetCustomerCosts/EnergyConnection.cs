﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetCustomerCosts
{
    [Serializable]
    public class EnergyConnection
    {
        public string Ean { get; set; }
        public string ProductType { get; set; }
        public List<CostPeriod> CostPeriods { get; set; }
    }
}
