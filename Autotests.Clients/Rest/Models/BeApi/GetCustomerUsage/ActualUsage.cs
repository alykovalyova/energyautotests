﻿namespace Autotests.Clients.Rest.Models.BeApi.GetCustomerUsage
{
    public class ActualUsage
    {
        public string MeteringDirection { get; set; }
        public string TariffType { get; set; }
        public long Value { get; set; }
    }
}
