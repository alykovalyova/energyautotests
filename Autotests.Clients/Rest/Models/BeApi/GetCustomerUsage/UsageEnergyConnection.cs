﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetCustomerUsage
{
    public class UsageEnergyConnection
    {
        public string Ean { get; set; }
        public string ProductType { get; set; }
        public List<UsagePeriod> UsagePeriods { get; set; }
    }
}
