﻿using System;
using System.Collections.Generic;
namespace Autotests.Clients.Rest.Models.BeApi.GetCustomerUsage
{
    public class UsagePeriod
    {
        public DateTimeOffset Date { get; set; }
        public long ExpectedConsumptionTotalUsage { get; set; }
        public List<ActualUsage> ActualUsages { get; set; }
    }
}
