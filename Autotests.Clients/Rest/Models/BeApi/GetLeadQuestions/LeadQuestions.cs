﻿using System;
using System.Collections.Generic;
using Nuts.Questionnaire.Contract.Rest;

namespace Autotests.Clients.Rest.Models.BeApi.GetLeadQuestions
{
    [Serializable]
    public class LeadQuestions
    {
        public List<Question> Questions { get; set; }
    }
}
