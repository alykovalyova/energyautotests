﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetMeteringPointDetails
{
    [Serializable]
    public class Error
    {
        public string errorCode { get; set; }
        public string message { get; set; }
    }
}
