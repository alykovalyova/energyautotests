﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetMeteringPointDetails
{
    [Serializable]
    public class Errors
    {
        public Error[] errors { get; set; }
    }
}