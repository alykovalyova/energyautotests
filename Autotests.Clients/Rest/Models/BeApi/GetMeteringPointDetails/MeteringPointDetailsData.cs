﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetMeteringPointDetails
{
    [Serializable]
    public class MeteringPointDetailsData
    {
        public List<MeteringPoint> MeteringPoints { get; set; }
    }
}