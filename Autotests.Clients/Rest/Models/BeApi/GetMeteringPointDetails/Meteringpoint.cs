﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetMeteringPointDetails
{
    [Serializable]
    public class MeteringPoint
    {
        public string EanId { get; set; }
        public string EdsnMeterId { get; set; }
        public string GridArea { get; set; }
        public string CapTarCode { get; set; }
        public int? EACPeak { get; set; }
        public int? EACOffPeak { get; set; }
        public bool? ExpectedResidential { get; set; }
        public string ProductType { get; set; }
        public int? NrOfRegisters { get; set; }
        public bool? ActiveSmartMeter { get; set; }
        public string ProfileCategory { get; set; }
        public Address Address { get; set; }
        public List<Register> Registers { get; set; }
    }
}