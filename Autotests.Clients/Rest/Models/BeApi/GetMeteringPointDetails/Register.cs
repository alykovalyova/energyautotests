﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Clients.Rest.Models.BeApi.GetMeteringPointDetails
{
    [Serializable]
    public class Register
    {
        public string EdsnId { get; set; }
        public int NrOfDigits { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnergyFlowDirectionCode? MeteringDirection { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnergyTariffTypeCode? TariffType { get; set; }
    }
}