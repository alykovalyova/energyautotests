﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferByContractId
{
    [Serializable]
    public class Proposition
    {
        public Guid ProdmanId { get; set; }
        public decimal? Cashback { get; set; }
    }
}
