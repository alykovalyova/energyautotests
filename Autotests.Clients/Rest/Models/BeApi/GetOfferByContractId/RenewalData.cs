﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferByContractId
{
    [Serializable]
    public class RenewalData
    {
        public int ContractId { get; set; }
        public Guid ExternalReference { get; set; }
        public int RenewalPageId { get; set; }
        public List<Proposition> Propositions { get; set; }
        public List<Usage> Usages { get; set; }
        public bool IsCustomOffer { get; set; }
    }
}