﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferByContractId
{
    [Serializable]
    public class Usage
    {
        public string ProductType { get; set; }
        public long UsageNormal { get; set; }
        public long? UsageLow { get; set; }
    }
}