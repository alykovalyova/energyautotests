﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferByRelationId
{
    [Serializable]
    public class OfferByRelationData
    {
        public List<int> ContractsWithAvailableOffers { get; set; }
    }
}