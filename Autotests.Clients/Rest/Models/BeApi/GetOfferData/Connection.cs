﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferData
{
    [Serializable]
    public class Connection
    {
        public long? UsageNormal { get; set; }
        public string ProductType { get; set; }
        public long? UsageLow { get; set; }
    }
}