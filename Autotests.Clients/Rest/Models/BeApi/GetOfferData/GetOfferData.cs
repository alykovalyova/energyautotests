﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferData
{
    [Serializable]
    public class GetOfferData
    {
        public string ZipCode { get; set; }
        public long BuildingNr { get; set; }
        public object ExBuildingNr { get; set; }
        public string ClientType { get; set; }
        public List<Proposition> Proposition { get; set; }
        public List<Connection> Connections { get; set; }
    }
}