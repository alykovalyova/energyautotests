﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetOfferData
{
    [Serializable]
    public class Proposition
    {
        public Guid ProdmanId { get; set; }
        public decimal? CashBack { get; set; }
    }
}