﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetProspectData
{
    [Serializable]
    public class Company
    {
        public string CompanyName { get; set; }
        public int ChamberOfCommerceNumber { get; set; }
    }
}