﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.GetProspectData
{
    [Serializable]
    public class ProspectData
    {
        public string Gender { get; set; }
        public string ClientType { get; set; }
        public string Surname { get; set; }
        public string Initials { get; set; }
        public string Prefix { get; set; }
        public string PhoneNumberHome { get; set; }
        public string PhoneNumberMobile { get; set; }
        public string Email { get; set; }
        public string Iban { get; set; }
        public DateTime? Birthday { get; set; }
        public Company Company { get; set; }
        public string CorrespondenceAddress { get; set; }
    }
}