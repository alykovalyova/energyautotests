﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.SearchBeApiAddress
{
    [Serializable]
    public class Address
    {
        public string StreetName { get; set; }
        public int BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}