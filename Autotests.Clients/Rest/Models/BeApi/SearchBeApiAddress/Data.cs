﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.SearchBeApiAddress
{
    public class Data
    {
        public List<Address> Addresses { get; set; }
    }
}