﻿using System;

namespace Autotests.Clients.Rest.Models.BeApi.SearchMeteringPoint
{
    [Serializable]
    public class ConnectionInfo
    {
        public string EanId { get; set; }
        public string ProductType { get; set; }
    }
}