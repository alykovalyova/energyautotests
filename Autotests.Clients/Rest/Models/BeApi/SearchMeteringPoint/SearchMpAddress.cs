﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.SearchMeteringPoint
{
    [Serializable]
    public class SearchMpAddress
    {
        public string StreetName { get; set; }
        public long BuildingNr { get; set; }
        public object ExBuildingNr { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public List<ConnectionInfo> ConnectionInfos { get; set; }
    }
}