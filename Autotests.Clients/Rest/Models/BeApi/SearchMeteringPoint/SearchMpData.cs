﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.SearchMeteringPoint
{
    [Serializable]
    public class SearchMpData
    {
        public List<SearchMpAddress> Addresses { get; set; }
    }
}