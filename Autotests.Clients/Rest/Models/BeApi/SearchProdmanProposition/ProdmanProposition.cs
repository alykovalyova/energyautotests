﻿using System;
using System.Collections.Generic;
using Nuts.ProdMan.Core.Enums;
using Repositories.Prodman.Sql.ProdManModels;

namespace Autotests.Clients.Rest.Models.BeApi.SearchProdmanProposition
{
    [Serializable]
    public class ProdmanProposition
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime MarketFrom { get; set; }
        public DateTime MarketTo { get; set; }
        public bool IsEligibleAsRenewal { get; set; }
        public bool IsSalesEligible { get; set; }
        public int Duration { get; set; }
        public bool IsFineApplicable { get; set; }
        public int CoolDownPeriod { get; set; }
        public decimal ReimburseFine { get; set; }
        public List<TypeOfClient> ClientType { get; set; }
        public decimal MaxCashBack { get; set; }
        public string Description { get; set; }
        public List<string> EnergySources { get; set; }
        public Status PropositionStatus { get; set; }
        public string LastComment { get; set; }
        public List<ElectricityPrice> ElectricityPrices { get; set; }
        public List<GasPrice> GasPrices { get; set; }
        public List<PropositionStatusHistory> PropositionStatusHistory { get; set; }
    }
}