﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.BeApi.ValidateMeterReading
{
    [Serializable]
    public class ReadingsSetValidationResult
    {
        public string EanId { get; set; }
        public string ProductType { get; set; }
        public List<RegisterValidationResult> RegisterValidationResults { get; set; }
        public string MarketEvent { get; set; }
        public DateTimeOffset MarketEventDate { get; set; }
    }
}