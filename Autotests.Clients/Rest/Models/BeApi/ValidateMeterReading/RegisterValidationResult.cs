﻿using System;
using ValidationStatus = Autotests.Clients.Enums.ValidationStatus;

namespace Autotests.Clients.Rest.Models.BeApi.ValidateMeterReading
{
    [Serializable]
    public class RegisterValidationResult
    {
        public ValidationStatus ValidationStatus { get; set; }
        public bool IsValid { get; set; }
        public string ValidationMessage { get; set; }
        public string MeteringDirection { get; set; }
        public string TariffType { get; set; }
        public long Value { get; set; }
    }
}