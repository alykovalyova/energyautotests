﻿using System;

namespace Autotests.Clients.Rest.Models.FakeTimeLine.GetFakeTimeLine
{
    public class CustomerEvent
    {
        public int Id { get; set; }
        public string ProductType { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsRead { get; set; }
        public DateTime EventDate { get; set; }
        public string ProductDisplayString { get; set; }
        public Action Action { get; set; }
    }
}