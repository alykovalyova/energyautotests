﻿namespace Autotests.Clients.Rest.Models.FakeTimeLine.GetTimeLineCounter
{
    public class Counter
    {
        public int NumUnreadCustomerEvents { get; set; }
        public int NumIncompleteActions { get; set; }
    }
}