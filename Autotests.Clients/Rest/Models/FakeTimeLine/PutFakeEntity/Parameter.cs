﻿namespace Autotests.Clients.Rest.Models.FakeTimeLine.PutFakeEntity
{
    public class Parameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}