﻿using System;
using Autotests.Clients.Enums.TimeLine;

namespace Autotests.Clients.Rest.Models.FakeTimeLine.PutFakeEntity
{
    public class Timeline
    {
        public int Id { get; set; }
        public ProductType ProductType { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsRead { get; set; }
        public DateTime EventDate { get; set; }
        public string ProductDisplayString { get; set; }
        public Action Action { get; set; }
    }
}