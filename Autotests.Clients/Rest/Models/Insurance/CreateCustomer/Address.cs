﻿namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class Address
    {
        public string StreetName { get; set; }
        public long HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string Ownership { get; set; }
    }
}
