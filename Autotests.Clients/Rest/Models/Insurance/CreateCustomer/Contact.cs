﻿namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class Contact
    {
        public string Email { get; set; }
        public ContactPhoneNumbers ContactPhoneNumbers { get; set; }
    }
}
