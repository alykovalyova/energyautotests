﻿namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class ContactPhoneNumbers
    {
        public string Home { get; set; }
        public string Mobile { get; set; }
    }
}
