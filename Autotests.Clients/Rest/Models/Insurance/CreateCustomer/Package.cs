﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class Package
    {
        public long GrossPrice { get; set; }
        public long NetPrice { get; set; }
        public long Discount { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public List<Product> Products { get; set; }
    }
}
