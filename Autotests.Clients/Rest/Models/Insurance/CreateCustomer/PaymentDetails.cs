﻿namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class PaymentDetails
    {
        public string Iban { get; set; }
        public string AccountHolder { get; set; }
    }
}
