﻿using System;

namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class Person
    {
        public string Initials { get; set; }
        public string SurnamePrefix { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
    }
}
