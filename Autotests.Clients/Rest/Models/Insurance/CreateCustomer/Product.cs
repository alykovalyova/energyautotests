﻿using System;

namespace Autotests.Clients.Rest.Models.Insurance.CreateCustomer
{
    public class Product
    {
        public string Type { get; set; }
        public string Specification { get; set; }
        public long Price { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string PolicyNumber { get; set; }
        public long SpecificationKey { get; set; }
    }
}
