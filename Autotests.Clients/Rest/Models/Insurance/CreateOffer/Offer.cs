﻿using Autotests.Clients.Rest.Models.Insurance.CreateCustomer;

namespace Autotests.Clients.Rest.Models.Insurance.CreateOffer
{
    public class Offer
    {
        public string ErrorDescription { get; set; }
        public string IncludeInCampaign { get; set; }
        public long LeadId { get; set; }
        public string TrackingId { get; set; }
        public OfferPackage Package { get; set; }
    }
}
