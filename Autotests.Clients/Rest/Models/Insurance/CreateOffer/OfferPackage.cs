﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.Insurance.CreateOffer
{
    public class OfferPackage
    {
        public long GrossPrice { get; set; }
        public long NetPrice { get; set; }
        public long Discount { get; set; }
        public List<OfferProduct> Products { get; set; }
    }
}
