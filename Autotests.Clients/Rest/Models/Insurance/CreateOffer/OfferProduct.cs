﻿namespace Autotests.Clients.Rest.Models.Insurance.CreateOffer
{
    public partial class OfferProduct
    {
        public string Type { get; set; }
        public string Specification { get; set; }
        public long SpecificationKey { get; set; }
        public long Price { get; set; }
    }
}
