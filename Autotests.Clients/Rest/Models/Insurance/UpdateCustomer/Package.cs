﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.Insurance.UpdateCustomer
{
    public class Package
    {
        public decimal GrossPrice { get; set; }
        public decimal NetPrice { get; set; }
        public decimal Discount { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public List<Product> Products { get; set; }
    }
}
