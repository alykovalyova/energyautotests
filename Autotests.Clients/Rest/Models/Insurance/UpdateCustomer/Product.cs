﻿using System;

namespace Autotests.Clients.Rest.Models.Insurance.UpdateCustomer
{
    public class Product
    {
        public string Type { get; set; }
        public string Specification { get; set; }
        public decimal Price { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public long PolicyNumber { get; set; }
        public long SpecificationKey { get; set; }
        public string ChangeReason { get; set; }
        public long ChangeReasonKey { get; set; }
        public bool IsActive { get; set; }
    }
}
