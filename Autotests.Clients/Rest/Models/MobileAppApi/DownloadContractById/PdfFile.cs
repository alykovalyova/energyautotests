﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.DownloadContractById
{
    public class PdfFile
    {
        public string FileName { get; set; }
        public string Pdf { get; set; }
    }
}