﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.DownloadInvoiceById
{
    public class PdfFile
    {
        public string FileName { get; set; }
        public string Pdf { get; set; }
    }
}