﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.Enums
{
    public enum TvSubStatus
    {
        On,
        Off
    }
}