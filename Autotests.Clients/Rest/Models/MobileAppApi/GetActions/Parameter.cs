﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetActions
{
    public class Parameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}