﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetActions
{
    public class TimelineEvent
    {
        public int Id { get; set; }
        public string ProductType { get; set; }
        public string Description { get; set; }
        public string IconImageUrl { get; set; }
        public bool IsRead { get; set; }
        public DateTime EventDate { get; set; }
        public string ProductDisplayString { get; set; }
        public Action Action { get; set; }
    }
}