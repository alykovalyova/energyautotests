﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetAvailabilityOfActiveOffer
{
    public class OfferDetails
    {
        public bool HasActiveOffer { get; set; }
    }
}
