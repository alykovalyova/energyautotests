﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContactPerson
{
    public class ContactPerson
    {
        public long ContactPersonNumber { get; set; }
        public string NickName { get; set; }
        public Customer[] Customers { get; set; }
        public List<Device> Devices { get; set; }
    }
}