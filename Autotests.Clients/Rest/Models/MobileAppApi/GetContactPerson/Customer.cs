﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContactPerson
{
    public class Customer
    {
        public long CustomerNumber { get; set; }
        public string Role { get; set; }
        public ProductCustomer[] ProductCustomers { get; set; }
    }
}