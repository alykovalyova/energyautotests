﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContactPerson
{
    public class Device
    {
        public string DeviceId { get; set; }
        public string MessageType { get; set; }
    }
}
