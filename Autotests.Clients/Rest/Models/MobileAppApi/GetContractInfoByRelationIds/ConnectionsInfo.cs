﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInfoByRelationIds
{
    public class ConnectionsInfo
    {
        public string Ean { get; set; }
        public string ProductType { get; set; }
    }
}