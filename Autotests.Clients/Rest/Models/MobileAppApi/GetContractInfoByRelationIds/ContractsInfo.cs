﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInfoByRelationIds
{
    public class ContractsInfo
    {
        public SupplyAddress SupplyAddress { get; set; }
        public int ContractId { get; set; }
        public int RelationId { get; set; }
        public int NutsHomeCustomerNumber { get; set; }
        public PaymentDetails PaymentDetails { get; set; }
        public ConnectionsInfo[] ConnectionsInfo { get; set; }
    }
}