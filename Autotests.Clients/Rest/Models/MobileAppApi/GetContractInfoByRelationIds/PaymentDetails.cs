﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInfoByRelationIds
{
    public class PaymentDetails
    {
        public string Iban { get; set; }
        public string PaymentMethod { get; set; }
        public DateTimeOffset CollectionDate { get; set; }
    }
}