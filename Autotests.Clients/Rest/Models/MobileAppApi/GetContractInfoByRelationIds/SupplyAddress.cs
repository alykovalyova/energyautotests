﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInfoByRelationIds
{
    public class SupplyAddress
    {
        public string ZipCode { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
    }
}