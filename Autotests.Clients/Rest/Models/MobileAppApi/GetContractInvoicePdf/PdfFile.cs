﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicePdf
{
    public class PdfFile
    {
        public string Filename { get; set; }
        public string Pdf { get; set; }
    }
}