﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicesByRelationIds
{
    public class AddressInvoice
    {
        public SupplyAddress SupplyAddress { get; set; }
        public ContractInvoice[] ContractInvoices { get; set; }
    }
}