﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicesByRelationIds
{
    public class ContractInvoice
    {
        public int RelationId { get; set; }
        public int ContractId { get; set; }
        public Invoice[] Invoices { get; set; }
    }
}