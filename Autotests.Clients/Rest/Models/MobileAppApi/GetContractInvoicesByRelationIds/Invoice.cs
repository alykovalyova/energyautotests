﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicesByRelationIds
{
    public class Invoice
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}