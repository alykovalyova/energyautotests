﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractInvoicesByRelationIds
{
    public class SupplyAddress
    {
        public string ZipCode { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
    }
}