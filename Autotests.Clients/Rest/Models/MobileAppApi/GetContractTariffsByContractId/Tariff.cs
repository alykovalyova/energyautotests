﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContractTariffsByContractId
{
    public class Tariff
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
