﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetContracts
{
    public class Contract
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}