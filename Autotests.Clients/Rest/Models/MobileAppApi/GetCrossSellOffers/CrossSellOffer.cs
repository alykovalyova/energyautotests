﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCrossSellOffers
{
    public class CrossSellOffer
    {
        public string StreetName { get; set; }
        public string HouseNumber { get; set; }
        public string Extension { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Energy { get; set; }
        public string AllInOne { get; set; }
        public string Mobile { get; set; }
    }
}