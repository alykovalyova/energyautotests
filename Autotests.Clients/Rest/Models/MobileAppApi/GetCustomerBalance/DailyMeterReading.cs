﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerBalance
{
    public class DailyMeterReading
    {
        public string Ean { get; set; }
        public string MeterId { get; set; }
        public DateTime Date { get; set; }
        public double ConsumptionLow { get; set; }
        public double ConsumptionNormal { get; set; }
        public double ProductionLow { get; set; }
        public double ProductionNormal { get; set; }
    }
}