﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerBbaInfo
{
    public class ContractsBba
    {
        public int ContractId { get; set; }
        public double MonthlyAmount { get; set; }
    }
}