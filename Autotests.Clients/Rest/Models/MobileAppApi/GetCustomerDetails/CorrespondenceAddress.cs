﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerDetails
{
    public class CorrespondenceAddress
    {
        public int HouseNumber { get; set; }
        public string Street { get; set; }
        public string HouseNumberExtension { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}