﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerDetails
{
    public class Customer
    {
        public long CustomerNumber { get; set; }
        public string Gender { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string SurnamePrefix { get; set; }
        public DateTime DateOfBirth { get; set; }
        public ProductCustomer[] ProductCustomers { get; set; }
        public PhoneNumber[] PhoneNumbers { get; set; }
        public CorrespondenceAddress CorrespondenceAddress { get; set; }
    }
}