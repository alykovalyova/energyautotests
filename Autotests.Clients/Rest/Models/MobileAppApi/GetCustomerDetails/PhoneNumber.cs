﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerDetails
{
    public class PhoneNumber
    {
        public string PhoneNr { get; set; }
        public string PhoneNumberType { get; set; }
        public bool IsPreferredForCalls { get; set; }
        public bool IsPreferredForTextMessages { get; set; }
    }
}