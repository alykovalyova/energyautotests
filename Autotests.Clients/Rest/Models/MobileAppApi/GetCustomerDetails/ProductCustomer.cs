﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerDetails
{
    public class ProductCustomer
    {
        public long ProductCustomerId { get; set; }
        public string ProductType { get; set; }
        public bool HasActiveContracts { get; set; }
    }
}