﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerTariffsPdf
{
    public class PdfFile
    {
        public string Filename { get; set; }
        public string Pdf { get; set; }
    }
}