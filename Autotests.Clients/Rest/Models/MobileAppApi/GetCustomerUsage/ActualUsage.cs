﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerUsage
{
    public class ActualUsage
    {
        public string MeteringDirection { get; set; }
        public string TariffType { get; set; }
        public decimal Value { get; set; }
    }
}