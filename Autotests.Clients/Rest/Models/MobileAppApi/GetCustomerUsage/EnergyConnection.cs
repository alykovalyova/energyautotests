﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerUsage
{
    public class EnergyConnection
    {
        public string Ean { get; set; }
        public string ProductType { get; set; }
        public UsagePeriod[] UsagePeriods { get; set; }
    }
}