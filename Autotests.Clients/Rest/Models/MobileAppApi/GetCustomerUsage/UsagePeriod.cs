﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetCustomerUsage
{
    public class UsagePeriod
    {
        public DateTimeOffset Date { get; set; }
        public decimal? ExpectedConsumptionTotalUsage { get; set; }
        public ActualUsage[] ActualUsages { get; set; }
    }
}