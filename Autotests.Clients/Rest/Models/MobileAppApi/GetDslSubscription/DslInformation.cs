﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetDslSubscription
{
    public class DslInformation
    {
        public int MaximalDownloadSpeed { get; set; }
        public int MaximalUploadSpeed { get; set; }
        public string InternetType { get; set; }
        public List<OnetimeBundle> OneTimeBundles { get; set; }
    }
}