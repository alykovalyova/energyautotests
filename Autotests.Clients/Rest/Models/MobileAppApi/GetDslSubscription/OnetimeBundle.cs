﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetDslSubscription
{
    public class OnetimeBundle
    {
        public string BundleName { get; set; }
        public double Price { get; set; }
    }
}