﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetEvents
{
    public class Action
    {
        public string Type { get; set; }
        public string Text { get; set; }
        public string ActionUrl { get; set; }
        public bool IsCompleted { get; set; }
        public Parameter[] Parameters { get; set; }
    }
}