﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetEvents
{
    public class Parameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}