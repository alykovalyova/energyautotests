﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetExtraCreditBundles
{
    public class Bundle
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
    }
}