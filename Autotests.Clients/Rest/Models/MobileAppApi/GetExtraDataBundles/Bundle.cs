﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetExtraDataBundles
{
    public class Bundle
    {
        public int Id { get; set; }
        public long BundleSizeGb { get; set; }
        public string BundleType { get; set; }
        public string Duration { get; set; }
        public decimal Price { get; set; }
    }
}