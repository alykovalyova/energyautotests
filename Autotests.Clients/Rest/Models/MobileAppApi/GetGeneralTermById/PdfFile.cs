﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetGeneralTermById
{
    public class PdfFile
    {
        public string Filename { get; set; }
        public string Pdf { get; set; }
    }
}