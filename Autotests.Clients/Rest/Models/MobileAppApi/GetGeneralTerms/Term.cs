﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetGeneralTerms
{
    public class Term
    {
        public int TermId { get; set; }
        public string Name { get; set; }
    }
}