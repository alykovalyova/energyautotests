﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetInvoices
{
    public class Invoice
    {
        public long Id { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string Name { get; set; }
    }
}