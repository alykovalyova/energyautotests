﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetInvoicesBySubscriptionId
{
    public class Invoice
    {
        public string Name { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public int OrderId { get; set; }
        public decimal TotalAmount { get; set; }
    }
}