﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetOfferDetailsByContractId
{
    public class EnergyTaxTotal
    {
        public Amount Amount { get; set; }
        public double Quantity { get; set; }
        public Price Price { get; set; }
    }
}
