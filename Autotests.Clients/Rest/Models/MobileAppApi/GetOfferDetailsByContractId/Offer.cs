﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetOfferDetailsByContractId
{
    public class Offer
    {
        public string Proposition { get; set; }
        public double SavedAmount { get; set; }
        public string ContractDuration { get; set; }
        public MonthlyInstallment MonthlyInstallment { get; set; }
        public MonthlyAmountRawTotal MonthlyAmountRawTotal { get; set; }
        public YearlyAmountTotal YearlyAmountTotal { get; set; }
        public YearlyAmountIncludingCashback YearlyAmountIncludingCashback { get; set; }
        public Cashback Cashback { get; set; }
        public TotalCashback TotalCashback { get; set; }
        public List<Quote> Quotes { get; set; }
    }
}
