﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetOfferDetailsByContractId
{
    public class PeakBracket2
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}
