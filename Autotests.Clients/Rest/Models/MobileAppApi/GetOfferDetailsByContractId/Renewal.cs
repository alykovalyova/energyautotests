﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetOfferDetailsByContractId
{
    public class Renewal
    {
        public string ExternalReference { get; set; }
        public bool IsCustomOffer { get; set; }
    }
}
