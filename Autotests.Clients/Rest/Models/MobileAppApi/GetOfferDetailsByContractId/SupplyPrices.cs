﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetOfferDetailsByContractId
{
    public class SupplyPrices
    {
        public PeakBracket1 PeakBracket1 { get; set; }
        public PeakBracket2 PeakBracket2 { get; set; }
        public PeakBracket3 PeakBracket3 { get; set; }
        public OffPeakBracket1 OffPeakBracket1 { get; set; }
        public OffPeakBracket2 OffPeakBracket2 { get; set; }
        public OffPeakBracket3 OffPeakBracket3 { get; set; }

    }
}
