﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetServiceAccount
{
    public class ServiceAccountInformation
    {
        public long ServiceAccountId { get; set; }
        public long CustomerAccountId { get; set; }
        public string PaymentMethod { get; set; }
        public string Iban { get; set; }
        public DateTime CollectionDate { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
    }
}