﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetSubscriptions
{
    public class FinanceData
    {
        public string Iban { get; set; }
        public string PaymentMethod { get; set; }
        public string NextIncassoDate { get; set; }
    }
}