﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetSubscriptions
{
    public class ResidentialAddress
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string HouseUnit { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}