﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetSubscriptions
{
    public class Subscription
    {
        public long SubscriptionId { get; set; }
        public string Remark { get; set; }
        public string Msisdn { get; set; }
        public string MsisdnStatus { get; set; }
        public string SubscriptionStatus { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public ResidentialAddress ResidentialAddress { get; set; }
        public FinanceData FinanceData { get; set; }
    }
}