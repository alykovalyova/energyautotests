﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetTimelineCounters
{
    public class Counter
    {
        public int Unread { get; set; }
        public int IncompleteActions { get; set; }
    }
}