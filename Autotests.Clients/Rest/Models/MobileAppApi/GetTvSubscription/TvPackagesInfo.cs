﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetTvSubscription
{
    public class TvPackagesInfo
    {
        public string BundleId { get; set; }
        public string PackageName { get; set; }
        public string CategoryName { get; set; }
        public double? PricePerMonth { get; set; }
        public bool Subscribed { get; set; }
        public bool Discounted { get; set; }
        public string Status { get; set; }
    }
}