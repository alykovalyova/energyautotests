﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetTvSubscription
{
    public class TvReceiverInfo
    {
        public double? CurrentReceiversAmount { get; set; }
        public double? MaxReceiversAmount { get; set; }
        public double? PricePerReceiver { get; set; }
    }
}