﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetTvSubscription
{
    public class TvSubscriptionInfo
    {
        public string Number { get; set; }
        public string Pin { get; set; }
        public string TvProductName { get; set; }
        public double? BasicCosts { get; set; }
        public double? ExtraCost { get; set; }
        public double? OtherCosts { get; set; }
        public List<TvPackagesInfo> TvPackagesInfo { get; set; }
        public TvReceiverInfo TvReceiverInfo { get; set; }
    }
}