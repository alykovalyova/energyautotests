﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetUsage
{
    public class Bundle
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string Name { get; set; }
        public long CommercialDuration { get; set; }
        public string CommercialDurationType { get; set; }
        public bool IsUnlimited { get; set; }
        public string LimitType { get; set; }
    }
}
