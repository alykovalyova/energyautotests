﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetUsage
{
    public class Extra
    {
        public double Credit { get; set; }

        public long DataNl { get; set; }

        public long DataEu { get; set; }
    }
}
