﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetUsage
{
    public class InternationalSmsAndVoice
    {
        public long Total { get; set; }
        public long Left { get; set; }
        public long Used { get; set; }
    }
}
