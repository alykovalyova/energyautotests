﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetUsage
{
    public class Usage
    {
        public object Data { get; set; }
        public InternationalSmsAndVoice InternationalSmsAndVoice { get; set; }
        public object Sms { get; set; }
        public object Voice { get; set; }
    }
}
