﻿using System;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetUsageCosts
{
    public class CostPeriod
    {
        public DateTime Date { get; set; }

        public decimal? Actual { get; set; }

        public decimal Expected { get; set; }
    }
}
