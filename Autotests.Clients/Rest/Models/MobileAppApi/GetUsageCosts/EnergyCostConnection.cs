﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetUsageCosts
{
    public class EnergyCostConnection
    {
        public string Ean { get; set; }

        public string ProductType { get; set; }

        public List<CostPeriod> CostPeriods { get; set; }
    }
}
