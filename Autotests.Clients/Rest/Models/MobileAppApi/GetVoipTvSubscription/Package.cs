﻿namespace Autotests.Clients.Rest.Models.MobileAppApi.GetVoipTvSubscription
{
    public class Package
    {
        public string BundleId { get; set; }
        public string DisplayTitle { get; set; }
        public string BundleType { get; set; }
        public double PricePerMonth { get; set; }
        public bool Subscribed { get; set; }
        public string PackageDescription { get; set; }
    }
}