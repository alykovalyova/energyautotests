﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MobileAppApi.GetVoipTvSubscription
{
    public class VoipSubscriptionInfo
    {
        public MainPackage MainPackage { get; set; }
        public List<Package> Packages { get; set; }
        public double BasicCosts { get; set; }
        public double BundleCosts { get; set; }
        public double? OtherCosts { get; set; }
    }
}