﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MongoDb
{
    public class CalculationData
    {
        public string State { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Production { get; set; }
        public decimal? CalculatedAmount { get; set; }
        public decimal? RelativeAmountPaid { get; set; }
        public List<ConnectionCosts> ConnectionCosts { get; set; }
    }
}
