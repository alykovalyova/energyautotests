﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.MongoDb
{
    public class ConnectionCosts
    {
        public string EanId { get; set; }
        public string ProductType { get; set; }
        public decimal MonthlyAmount { get; set; }
        public bool IsSmartMeter { get; set; }
        public int Balance { get; set; }
        public decimal CalculatedConsumption { get; set; }
        public decimal CalculatedProduction { get; set; }
        public decimal CalculatedAmount { get; set; }
        public List<TariffPeriods> TariffPeriods { get; set; }
        public decimal TermsPaid { get; set; }
    }
}
