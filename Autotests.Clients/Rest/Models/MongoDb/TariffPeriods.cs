﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Autotests.Clients.Rest.Models.MongoDb
{
    public class TariffPeriods
    {
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime From { get; set; }
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime To { get; set; }
        public decimal PriceNonUsagePerDay { get; set; }
        public decimal? PriceUsagePerUnit { get; set; }
    }
}
