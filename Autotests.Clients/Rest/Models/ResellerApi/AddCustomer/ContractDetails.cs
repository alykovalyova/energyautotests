﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.AddCustomer
{
    public class ContractDetails
    {
        public Guid PropositionId { get; set; }
        public string Gift { get; set; }
        public DateTime PreferredStartDate { get; set; }
        public bool HasMovedInRecently { get; set; }
        public bool HasElec { get; set; }
        public bool HasGas { get; set; }
        public int Sjv { get; set; }
        public int Sjv_Piek { get; set; }
        public int Sjv_Dal { get; set; }
        public int Sjv_Production { get; set; }
        public int Sjv_Production_Piek { get; set; }
        public int Sjv_Production_Dal { get; set; }
        public int Sjv_Gas { get; set; }
        public string ContractPdf { get; set; }
        public string ContractPdfMd5Hash { get; set; }
        public string ConsentPdf { get; set; }
        public string ConsentPdfMd5Hash { get; set; }
        public DateTime SignUpDate { get; set; }
    }
}