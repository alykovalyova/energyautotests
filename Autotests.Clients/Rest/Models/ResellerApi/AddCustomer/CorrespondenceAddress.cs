﻿namespace Autotests.Clients.Rest.Models.ResellerApi.AddCustomer
{
    public class CorrespondenceAddress
    {
        public string StreetName { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}