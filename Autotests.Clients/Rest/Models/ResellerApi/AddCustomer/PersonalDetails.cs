﻿namespace Autotests.Clients.Rest.Models.ResellerApi.AddCustomer
{
    public class PersonalDetails
    {
        public string Gender { get; set; }
        public string Surname { get; set; }
        public string Initials { get; set; }
        public string SurnamePrefix { get; set; }
        public string PhoneNumber { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string BirthDate { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public Company Company { get; set; }
        public string PaymentMethod { get; set; }
    }
}