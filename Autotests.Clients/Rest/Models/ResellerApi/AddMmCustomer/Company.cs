﻿namespace Autotests.Clients.Rest.Models.ResellerApi.AddMmCustomer
{
    public class Company
    {
        public string JobPosition { get; set; }
        public string CompanyName { get; set; }
        public string ChamberOfCommerceNumber { get; set; }
    }
}