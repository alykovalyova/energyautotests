﻿namespace Autotests.Clients.Rest.Models.ResellerApi.AddMmCustomer
{
    public class ConnectionAddress
    {
        public string Eancode_Electricity { get; set; }
        public string Eancode_Gas { get; set; }
        public string StreetName { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}