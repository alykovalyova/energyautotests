﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderForMobile
{
    public class Affiliate
    {
        public string Partner { get; set; }
        public string Id { get; set; }
        public string SubId { get; set; }
        public string AddOne { get; set; }
        public string AddTwo { get; set; }
        public string AddThree { get; set; }
    }
}
