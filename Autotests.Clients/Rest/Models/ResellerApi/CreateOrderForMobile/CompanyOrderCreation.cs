﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderForMobile
{
    public class CompanyOrderCreation
    {
        public string Name { get; set; }
        public string CocNumber { get; set; }
    }
}