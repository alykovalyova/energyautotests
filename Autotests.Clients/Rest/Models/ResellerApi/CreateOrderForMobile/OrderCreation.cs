﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderForMobile
{
    public class OrderCreation
    {
        public List<OrderLineCreation> OrderLines { get; set; }
        public string PaymentMethod { get; set; }
        public bool? PortingRequired { get; set; }
        public TelbasePorting TelbasePorting { get; set; }
        public string DesiredMsisdn { get; set; }
        public string RequestedSimNumber { get; set; }
        public bool? UseLsp { get; set; }
    }
}