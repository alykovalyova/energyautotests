﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderForMobile
{
    public class SubscriptionOrderCreation
    {
        public CompanyOrderCreation Company { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public ResidentialAddress ResidentialAddress { get; set; }
        public FinanceData FinanceData { get; set; }
        public Affiliate Affiliate { get; set; }
    }
}