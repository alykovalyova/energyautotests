﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderForMobile
{
    public class TelbasePorting
    {
        public long PortingId { get; set; }
        public DateTime PortingDate { get; set; }
        public string Note { get; set; }
    }
}