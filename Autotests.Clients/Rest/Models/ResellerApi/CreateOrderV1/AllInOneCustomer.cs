﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1
{
   public class AllInOneCustomer
    {
        public IngAddress BillingAddress { get; set; }
        public ContactInfo ContactInfo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
        public string Prefix { get; set; }
        public string Gender { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
    }
}