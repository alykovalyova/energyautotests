﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1
{
    public class ContactInfo
    {
        public string Email { get; set; }
        public PhoneNumberInformation[] PhoneNumberInformation { get; set; }
    }
}