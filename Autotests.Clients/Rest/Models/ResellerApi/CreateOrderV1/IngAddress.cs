﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1
{
    public class IngAddress
    {
        public string Zip { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public long HouseNumber { get; set; }
    }
}