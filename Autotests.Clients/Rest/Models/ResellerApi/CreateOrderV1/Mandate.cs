﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1
{
    public class Mandate
    {
        public string MemberIban { get; set; }
        public DateTimeOffset StartOn { get; set; }
        public string City { get; set; }
    }
}