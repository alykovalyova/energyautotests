﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1
{
    public class PhoneNumberInformation
    {
        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }
    }
}