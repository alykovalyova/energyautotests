﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV1
{
    public class SubscribeBundleWithDiscount
    {
        public long BundleId { get; set; }
        public int? DiscountId { get; set; }
    }
}