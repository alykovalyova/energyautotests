﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2
{
    public class BillingAddress
    {
        public string Zip { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string Extension { get; set; }
    }
}