﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2
{
    public class ContactInfo
    {
        public string Email { get; set; }
        public PhoneNumberInformation[] PhoneNumberInformation { get; set; }
    }
}