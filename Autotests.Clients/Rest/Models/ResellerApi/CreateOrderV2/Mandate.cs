﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2
{
    public class Mandate
    {
        public string MemberIban { get; set; }
        public DateTimeOffset StartOn { get; set; }
        public string City { get; set; }
    }
}