﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2
{
    public class PhoneNumberInformation
    {
        public string PhoneNumber { get; set; }
        public string PhoneType { get; set; }
    }
}