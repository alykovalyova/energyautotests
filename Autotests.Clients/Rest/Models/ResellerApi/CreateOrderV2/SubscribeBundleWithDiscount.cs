﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2
{
    public class SubscribeBundleWithDiscount
    {
        public long BundleId { get; set; }
        public long? DiscountId { get; set; }
    }
}