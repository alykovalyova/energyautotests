﻿namespace Autotests.Clients.Rest.Models.ResellerApi.CreateOrderV2
{
    public class VoipSettings
    {
        public bool VisibleInPhoneBook { get; set; }
        public bool VisibleInElectronicPhoneBook { get; set; }
        public bool KnownInformationService { get; set; }
        public bool HidePhoneNumberOnInvoices { get; set; }
        public bool ShowPhoneNumberOnCalling { get; set; }
        public bool AllowMobileCall { get; set; }
        public bool AllowInternationalCall { get; set; }
        public string BlockServiceNumbers { get; set; }
    }
}