﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV1
{
    public class PqccResolvedAddress
    {
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public long HouseNumber { get; set; }
        public object HouseNumberAddition { get; set; }
        public object[] PossibleHouseNumberAdditions { get; set; }
    }
}