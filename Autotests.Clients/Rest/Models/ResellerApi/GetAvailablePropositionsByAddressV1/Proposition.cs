﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV1
{
    public class Proposition
    {
        public string Id { get; set; }
        public string CashBack { get; set; }
    }
}