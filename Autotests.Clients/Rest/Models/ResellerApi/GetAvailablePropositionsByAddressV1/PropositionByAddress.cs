﻿using Autotests.Clients.Enums.ResellerApi;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV1
{
    public class PropositionByAddress
    {
        public PqccResolvedAddress PqccResolvedAddress { get; set; }
        public Proposition[] Propositions { get; set; }
        public bool IsInstallationRequired { get; set; }
        public LineType[] LineTypes { get; set; }
        public object[] PossibleExtensions { get; set; }
        public long MaxNumberOfStb { get; set; }
        public long SpeedLimit { get; set; }
    }
}