﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV2
{
    public class ConnectionСharacteristic
    {
        public string LineType { get; set; }
        public int MaxUpSpeed { get; set; }
        public int MaxDownSpeed { get; set; }
        public int MaxNumberOfStb { get; set; }
        public bool InstallationRequired { get; set; }
        public string[] InstallationBundleIds { get; set; }
    }
}