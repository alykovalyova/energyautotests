﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV2
{
    public class PqccResolvedAddress
    {
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string[] PossibleHouseNumberExtensions { get; set; }
    }
}