﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV2
{
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public short TypeOfServiceId { get; set; }
        public Bundle[] Bundles { get; set; }
    }
}