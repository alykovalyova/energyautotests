﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByAddressV2
{
    public class Proposition
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int ContractDuration { get; set; }
        public Product[] Products { get; set; }
        public bool IsDefault { get; set; }
    }
}