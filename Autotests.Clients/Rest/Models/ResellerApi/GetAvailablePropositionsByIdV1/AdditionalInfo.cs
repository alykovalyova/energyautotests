﻿using Autotests.Clients.Enums.ResellerApi;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV1
{
    public class AdditionalInfo
    {
        public Name Name { get; set; }
        public int Value { get; set; }
    }
}