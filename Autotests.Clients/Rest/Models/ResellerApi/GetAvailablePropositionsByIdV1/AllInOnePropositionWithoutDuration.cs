﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV1
{
    public class AllInOnePropositionWithoutDuration
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ProductWithBundleDiscountType[] Products { get; set; }
        public bool IsDefault { get; set; }
    }
}