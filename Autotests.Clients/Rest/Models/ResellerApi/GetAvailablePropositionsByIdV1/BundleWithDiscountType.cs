﻿using Autotests.Clients.Enums.ResellerApi;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV1
{
    public class BundleWithDiscountType
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ProductType { get; set; }
        public string ExternalCode { get; set; }
        public double Price { get; set; }
        public LineType LineType { get; set; }
        public long InternetSpeed { get; set; }
        public HardwareType HardwareType { get; set; }
        public NetworkType? NetworkType { get; set; }
        public Technology? Technology { get; set; }
        public BundlePeriodicity BundlePeriodicity { get; set; }
        public bool IsMain { get; set; }
        public AdditionalInfo[] AdditionalInfos { get; set; }
        public PropositionDiscount PropositionDiscount { get; set; }
    }
}