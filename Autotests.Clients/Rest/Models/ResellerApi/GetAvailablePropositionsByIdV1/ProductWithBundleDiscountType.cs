﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV1
{
    public class ProductWithBundleDiscountType
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long TypeOfServiceId { get; set; }
        public BundleWithDiscountType[] Bundles { get; set; }
    }
}