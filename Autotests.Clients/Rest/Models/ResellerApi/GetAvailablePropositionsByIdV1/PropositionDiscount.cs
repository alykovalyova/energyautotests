﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV1
{
    public class PropositionDiscount
    {
        public double AbsoluteAmount { get; set; }
        public long BillPeriodQuantity { get; set; }
        public long BundleId { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public long PercentageAmount { get; set; }
        public string ToDateTime { get; set; }
        public string Type { get; set; }
    }
}