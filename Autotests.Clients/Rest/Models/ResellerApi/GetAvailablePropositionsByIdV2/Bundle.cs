﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV2
{
    public class Bundle
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ProductType { get; set; }
        public string ExternalCode { get; set; }
        public decimal Price { get; set; }
        public string LineType { get; set; }
        public int InternetSpeedUp { get; set; }
        public int InternetSpeedDown { get; set; }
        public string HardwareType { get; set; }
        public string NetworkType { get; set; }
        public string Technology { get; set; }
        public string BundlePeriodicity { get; set; }
        public bool IsMain { get; set; }
        public PropositionDiscount PropositionDiscount { get; set; }
        public string GiftType { get; set; }
        public string BundleTypeName { get; set; }
    }
}