﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailablePropositionsByIdV2
{
    public class PropositionDiscount
    {
        public decimal AbsoluteAmount { get; set; }
        public decimal BillPeriodQuantity { get; set; }
        public long BundleId { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal PercentageAmount { get; set; }
        public DateTime ToDateTime { get; set; }
        public string Type { get; set; }
    }
}