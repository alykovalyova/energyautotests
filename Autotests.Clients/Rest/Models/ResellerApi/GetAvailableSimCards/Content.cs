﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailableSimCards
{
    public class Content
    {
        public string SimCardType { get; set; }
        public List<SimCard> SimCards { get; set; }
    }
}