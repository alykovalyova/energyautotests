﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetAvailableSimCards
{
    public class SimCard
    {
        public string ICC { get; set; }
        public string Msisdn { get; set; }
        public string Puk { get; set; }
    }
}