﻿using Autotests.Clients.Attributes;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class Connection
    {
        public string EnergyProductType { get; set; }
        public int EacPeak { get; set; }
        [NonIgnorable] public int? EacOffPeak { get; set; }
        [NonIgnorable] public int? EapPeak { get; set; }
        [NonIgnorable] public int? EapOffPeak { get; set; }
    }
}