﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class MonthlyAmountRawTotal
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}