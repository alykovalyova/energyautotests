﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class OffPeakBracket
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}