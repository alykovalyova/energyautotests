﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class PeakBracket
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}