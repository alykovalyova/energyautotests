﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class Proposition
    {
        public Guid Id { get; set; }
        public double CashBack { get; set; }
    }
}