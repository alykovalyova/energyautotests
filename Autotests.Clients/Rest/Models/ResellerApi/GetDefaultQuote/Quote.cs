﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class Quote
    {
        public string Ean { get; set; }
        public string ProfileCategory { get; set; }
        public string ProductType { get; set; }
        public string PhysicalCapacity { get; set; }
        public SupplyPrices SupplyPrices { get; set; }
        public YearlyAmount YearlyAmount { get; set; }
        public MonthlyInstallAmount MonthlyInstallAmount { get; set; }
        public SupplyPeak SupplyPeak { get; set; }
        public SupplyOffPeak SupplyOffPeak { get; set; }
        public EnergyTaxTotal EnergyTaxTotal { get; set; }
        public EnergyTax EnergyTax1 { get; set; }
        public EnergyTax EnergyTax2 { get; set; }
        public EnergyTax EnergyTax3 { get; set; }
        public EnergyTax EnergyTax4 { get; set; }
        public EnergyTax EnergyTax5 { get; set; }
        public OdeTotal OdeTotal { get; set; }
        public Ode Ode1 { get; set; }
        public Ode Ode2 { get; set; }
        public Ode Ode3 { get; set; }
        public Ode Ode4 { get; set; }
        public Ode Ode5 { get; set; }
        public StandingChargesSupply StandingChargesSupply { get; set; }
        public StandingChargesSupplyByMonth StandingChargesSupplyByMonth { get; set; }
        public StandingChargesTransport StandingChargesTransport { get; set; }
        public TaxReduction TaxReduction { get; set; }
    }
}