﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetDefaultQuote
{
    public class SupplyPrices
    {
        public PeakBracket PeakBracket1 { get; set; }
        public OffPeakBracket OffPeakBracket1 { get; set; }
        public PeakBracket PeakBracket2 { get; set; }
        public OffPeakBracket OffPeakBracket2 { get; set; }
        public PeakBracket PeakBracket3 { get; set; }
        public OffPeakBracket OffPeakBracket3 { get; set; }
    }
}