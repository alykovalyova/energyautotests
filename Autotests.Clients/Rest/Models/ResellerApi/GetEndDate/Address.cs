﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetEndDate
{
    public class Address
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberSuffix { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}