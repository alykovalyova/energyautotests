﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetEndDate
{
    public class ConnectionInfo
    {
        public string EanId { get; set; }
        public string ContractEndDate { get; set; }
        public int NoticePeriod { get; set; }
        public string CalcStartDate { get; set; }
    }
}