﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetEndDate
{
    public class Customer
    {
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
    }
}