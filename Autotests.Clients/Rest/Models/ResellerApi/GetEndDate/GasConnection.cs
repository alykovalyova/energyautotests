﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetEndDate
{
    public class GasConnection
    {
        public string EanId { get; set; }
        public string SupplierEan { get; set; }
    }
}