﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetEndDate
{
    public class PermissionGrand
    {
        public PermissionInfo PermissionInfo { get; set; }
        public string BirthDayKey { get; set; }
        public string IBANKey { get; set; }
    }
}