﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmPropositions
{
    public class ConnectionAddress
    {
        public List<string> ProductTypes { get; set; }
        public long HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string PostalCode { get; set; }
    }
}
