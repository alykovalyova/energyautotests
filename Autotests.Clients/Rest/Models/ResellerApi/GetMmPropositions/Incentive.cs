﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmPropositions
{
    [Serializable]
    public class Incentive
    {
        public string IncentiveType { get; set; }
        public object IncentivePaymentType { get; set; }
        public long IncentiveAmount { get; set; }
    }
}
