﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmPropositions
{
    public class MediaMarketProposition
    {
        public long LeadSourceId { get; set; }
        public bool IsCustomer { get; set; }
        public string EmployeeBe { get; set; }
        public PersonalDetails PersonalDetails { get; set; }
        public ConnectionAddress ConnectionAddress { get; set; }
        public DateTimeOffset MoveInDate { get; set; }
        public DateTimeOffset CerDate { get; set; }
        public bool IsEdsnOffline { get; set; }
    }
}
