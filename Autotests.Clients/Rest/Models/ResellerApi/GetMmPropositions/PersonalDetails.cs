﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmPropositions
{
    public class PersonalDetails
    {
        public string Surname { get; set; }
        public string Initials { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Iban { get; set; }
        public string SurnamePrefix { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string AlternativePhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}
