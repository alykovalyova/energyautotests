﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmPropositions
{
    [Serializable]
    public class Proposition
    {
        public Guid PropositionId { get; set; }
        public string PropositionName { get; set; }
        public long Duration { get; set; }
        public Incentive Incentive { get; set; }
        public long ReimburseFine { get; set; }
    }
}
