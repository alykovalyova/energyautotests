﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class Company
    {
        public string JobPosition { get; set; }
        public string CompanyName { get; set; }
        public string ChamberOfCommerceNumber { get; set; }
    }
}