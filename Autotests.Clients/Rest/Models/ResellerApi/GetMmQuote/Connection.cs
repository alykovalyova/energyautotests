﻿using Autotests.Clients.Attributes;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class Connection
    {
        public string EanId { get; set; }
        public int UsageNormal { get; set; }
        [NonIgnorable] public int? UsageLow { get; set; }
        [NonIgnorable] public int? UsageProductionNormal { get; set; }
        [NonIgnorable] public int? UsageProductionLow { get; set; }
        public string CaptarCode { get; set; }
        public string ProductType { get; set; }
        public string ProfileCategory { get; set; }
        public string GridArea { get; set; }
        public bool IsResidential { get; set; }
    }
}