﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class ConnectionAddress
    {
        public List<Connection> Connections { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string PostalCode { get; set; }
    }
}