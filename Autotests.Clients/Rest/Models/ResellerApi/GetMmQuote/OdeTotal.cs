﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class OdeTotal
    {
        public Amount Amount { get; set; }
        public double Quantity { get; set; }
        public Price Price { get; set; }
    }
}