﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class Quote
    {
        public SupplyPrices SupplyPrices { get; set; }
        public YearlyAmount YearlyAmount { get; set; }
        public MonthlyInstallAmount MonthlyInstallAmount { get; set; }
        public SupplyPeak SupplyPeak { get; set; }
        public EnergyTaxTotal EnergyTaxTotal { get; set; }
        public EnergyTax1 EnergyTax1 { get; set; }
        public OdeTotal OdeTotal { get; set; }
        public Ode1 Ode1 { get; set; }
        public StandingChargesSupply StandingChargesSupply { get; set; }
        public StandingChargesSupplyByMonth StandingChargesSupplyByMonth { get; set; }
        public StandingChargesTransport StandingChargesTransport { get; set; }
        public TaxReduction TaxReduction { get; set; }
        public string Ean { get; set; }
        public string ProfileCategory { get; set; }
        public string ProductType { get; set; }
        public string PhysicalCapacity { get; set; }
        public object SupplyOffPeak { get; set; }
        public object EnergyTax2 { get; set; }
        public object EnergyTax3 { get; set; }
        public object EnergyTax4 { get; set; }
        public object EnergyTax5 { get; set; }
        public object Ode2 { get; set; }
        public object Ode3 { get; set; }
        public object Ode4 { get; set; }
        public object Ode5 { get; set; }
    }
}