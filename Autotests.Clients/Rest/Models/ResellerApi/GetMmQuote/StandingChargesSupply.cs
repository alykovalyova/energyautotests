﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class StandingChargesSupply
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}