﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMmQuote
{
    public class SupplyPrices
    {
        public PeakBracket PeakBracket1 { get; set; }
        public object OffPeakBracket1 { get; set; }
        public PeakBracket PeakBracket2 { get; set; }
        public object OffPeakBracket2 { get; set; }
        public PeakBracket PeakBracket3 { get; set; }
        public object OffPeakBracket3 { get; set; }
    }
}