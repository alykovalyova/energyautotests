﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class CommercialCharacteristics
    {
        public bool ExpectedResidential { get; set; }
    }
}