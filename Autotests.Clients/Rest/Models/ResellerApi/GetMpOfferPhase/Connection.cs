﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class Connection
    {
        public string EanId { get; set; }
        public int UsageNormal { get; set; }
        public int UsageLow { get; set; }
        public string CapTarCode { get; set; }
        public string ProductType { get; set; }
        public string ProfileCategory { get; set; }
        public string GridArea { get; set; }
        public bool IsResidential { get; set; }
        public string SupplierEan { get; set; }
        public string EacPeak { get; set; }
        public string EacOffPeak { get; set; }
    }
}