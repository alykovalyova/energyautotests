﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class Customer
    {
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
    }
}