﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class MeteringPointAddress
    {
        public string StreetName { get; set; }
        public int BuildingNr { get; set; }
        public object ExBuildingNr { get; set; }
        public string ZIPCode { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}