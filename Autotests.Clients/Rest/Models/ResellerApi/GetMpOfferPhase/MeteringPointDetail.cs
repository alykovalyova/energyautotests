﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class MeteringPointDetail
    {
        public Connection ElectricityConnection { get; set; }
        public PermissionGrand PermissionGrand { get; set; }
    }
}