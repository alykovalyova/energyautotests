﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class MeteringPointInfo
    {
        public CommercialCharacteristics CommercialCharacteristics { get; set; }
        public PhysicalCharacteristics PhysicalCharacteristics { get; set; }
        public EnergyMeter EnergyMeter { get; set; }
        public MeteringPointAddress Address { get; set; }
        public string EanId { get; set; }
        public object AdministrativeStatusSmartMeter { get; set; }
        public string ConnectionType { get; set; }
        public string GridOperatorId { get; set; }
        public object GridOperatorName { get; set; }
        public string GridArea { get; set; }
        public object LocationDescription { get; set; }
        public string MarketSegment { get; set; }
        public string ValidFromDate { get; set; }
    }
}