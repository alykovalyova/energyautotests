﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class PermissionGrand
    {
        public PermissionInfo PermissionInfo { get; set; }
        public string BirthDayKey { get; set; }
        public string IBANKey { get; set; }
    }
}