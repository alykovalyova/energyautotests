﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class PermissionInfo
    {
        public DateTime PermissionDateTime { get; set; }
        public string PermissionPurpose { get; set; }
        public string PermissionType { get; set; }
        public string RequestOrigin { get; set; }
        public string ResponsibleUser { get; set; }
        public bool HasOptedInByCheckbox { get; set; }
        public Customer Customer { get; set; }
        public Address Address { get; set; }
    }
}