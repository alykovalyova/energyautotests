﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase
{
    public class PhysicalCharacteristics
    {
        public string CapTarCode { get; set; }
        public int EACPeak { get; set; }
        public int EACOffPeak { get; set; }
        public int? EAPPeak { get; set; }
        public int? EAPOffPeak { get; set; }
        public string EnergyFlowDirection { get; set; }
        public string ProfileCategory { get; set; }
    }
}