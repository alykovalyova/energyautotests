﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class ContactPerson
    {
        public string Initials { get; set; }
        public string Gender { get; set; }
        public string Infix { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
    }
}