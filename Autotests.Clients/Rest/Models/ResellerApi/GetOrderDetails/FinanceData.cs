﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class FinanceData
    {
        public string Iban { get; set; }
        public string PaymentMethod { get; set; }
    }
}