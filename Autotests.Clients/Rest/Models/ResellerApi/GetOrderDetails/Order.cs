﻿using System;
using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class Order
    {
        public int OrderId { get; set; }
        public string Number { get; set; }
        public string Status { get; set; }
        public string CurrentPaymentMethod { get; set; }
        public List<OrderLine> OrderLines { get; set; }
        public string SimType { get; set; }
        public double TotalAmount { get; set; }
        public double VatAmount { get; set; }
        public DateTimeOffset InvoiceDate { get; set; }
        public int InvoiceNumber { get; set; }
    }
}