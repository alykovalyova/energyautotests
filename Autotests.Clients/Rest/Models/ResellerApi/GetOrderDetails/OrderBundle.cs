﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class OrderBundle
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public decimal StartupFee { get; set; }
        public string ContractType { get; set; }
        public string Duration { get; set; }
    }
}