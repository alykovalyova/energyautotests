﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class OrderContent
    {
        public string ResellerId { get; set; }
        public Subscription Subscription { get; set; }
        public Order Order { get; set; }
    }
}