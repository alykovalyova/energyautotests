﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class OrderLine
    {
        public string ProductChangeType { get; set; }
        public OrderBundle OrderBundle { get; set; }
    }
}