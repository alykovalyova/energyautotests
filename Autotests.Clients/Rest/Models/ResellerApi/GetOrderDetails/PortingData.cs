﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class PortingData
    {
        public string PortingDirection { get; set; }
        public string PortingId { get; set; }
        public DateTimeOffset PortingDate { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
        public string Donor { get; set; }
        public string Recipient { get; set; }
    }
}