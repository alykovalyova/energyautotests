﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class ResidentialAddress
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string HouseUnit { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}