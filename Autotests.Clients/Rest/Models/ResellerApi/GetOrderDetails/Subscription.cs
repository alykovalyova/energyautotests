﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetOrderDetails
{
    public class Subscription
    {
        public Company Company { get; set; }
        public ContactPerson ContactPerson { get; set; }
        public ResidentialAddress ResidentialAddress { get; set; }
        public FinanceData FinanceData { get; set; }
        public PortingData PortingData { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public string SubscriptionStart { get; set; }
        public bool SimActivationRequired { get; set; }
        public int SubscriptionId { get; set; }
    }
}