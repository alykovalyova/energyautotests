﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetPropositionsByReseller
{
    public class ResellerProposition
    {
        public Guid PropositionId { get; set; }
        public string ExternalName { get; set; }
        public int Duration { get; set; }
        public string ClientType { get; set; }
        public double ReimburseFine { get; set; }
        public string IncentivePaymentType { get; set; }
        public string IncentiveType { get; set; }
        public decimal? IncentiveValue { get; set; }
    }
}