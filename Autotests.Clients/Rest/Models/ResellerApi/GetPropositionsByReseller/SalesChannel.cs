﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetPropositionsByReseller
{
    public class SalesChannel
    {
        public int SalesChannelId { get; set; }
        public string ChannelName { get; set; }
        public string Label { get; set; }
        public List<ResellerProposition> Propositions { get; set; }
    }
}