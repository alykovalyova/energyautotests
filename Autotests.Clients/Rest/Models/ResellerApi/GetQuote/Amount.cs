﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class Amount
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}