﻿using Autotests.Clients.Attributes;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class Connection
    {
        public int EacPeak { get; set; }
        [NonIgnorable] public int? EacOffPeak { get; set; }
        [NonIgnorable] public int? EapPeak { get; set; }
        [NonIgnorable] public int? EapOffPeak { get; set; }
        public string CapTarCode { get; set; }
        public string ProductType { get; set; }
        public string ProfileCategory { get; set; }
        public string GridArea { get; set; }
    }
}