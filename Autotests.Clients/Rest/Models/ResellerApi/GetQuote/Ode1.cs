﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class Ode1
    {
        public Amount Amount { get; set; }
        public double Quantity { get; set; }
        public Price Price { get; set; }
    }
}