﻿using System.Collections.Generic;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class Offer
    {
        public MonthlyInstallment MonthlyInstallment { get; set; }
        public MonthlyAmountRawTotal MonthlyAmountRawTotal { get; set; }
        public YearlyAmountTotal YearlyAmountTotal { get; set; }
        public YearlyAmountIncludingCashBack YearlyAmountIncludingCashBack { get; set; }
        public CashBack CashBack { get; set; }
        public List<Quote> Quotes { get; set; }
        public string Proposition { get; set; }
        public string ContractDuration { get; set; }
    }
}