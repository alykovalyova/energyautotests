﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class Price
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}