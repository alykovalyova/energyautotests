﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class Proposition
    {
        public Guid Id { get; set; }
        public string CashBack { get; set; }
    }
}