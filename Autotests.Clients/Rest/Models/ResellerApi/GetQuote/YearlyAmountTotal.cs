﻿namespace Autotests.Clients.Rest.Models.ResellerApi.GetQuote
{
    public class YearlyAmountTotal
    {
        public double AmountNet { get; set; }
        public double AmountVat { get; set; }
        public double AmountGross { get; set; }
    }
}