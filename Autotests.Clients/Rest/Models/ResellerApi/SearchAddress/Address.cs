﻿using System;

namespace Autotests.Clients.Rest.Models.ResellerApi.SearchAddress
{
    [Serializable]
    public class Address
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string HouseUnit { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }
}