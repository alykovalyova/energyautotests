﻿namespace Autotests.Clients.Rest.Models.ResellerApi.SearchBagAddress
{
    public class BagAddress
    {
        public int BuildingNr { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public string ExBuildingNr { get; set; }
        public string StreetName { get; set; }
        public string ZipCode { get; set; }
    }
}