﻿namespace Autotests.Clients.Rest.Models.ResellerApi.SearchMeteringPoint
{
    public class EnergyMeter
    {
        public object CommunicationStatusCode { get; set; }
        public string Type { get; set; }
    }
}