﻿namespace Autotests.Clients.Rest.Models.ResellerApi.SearchMeteringPoint
{
    public class MeteringPoint
    {
        public Address Address { get; set; }
        public EnergyMeter EnergyMeter { get; set; }
        public string EanId { get; set; }
        public string GridArea { get; set; }
        public object LocationDescription { get; set; }
        public string MarketSegment { get; set; }
        public string ProductType { get; set; }
        public string GridOperator { get; set; }
        public object GridOperatorName { get; set; }
    }
}