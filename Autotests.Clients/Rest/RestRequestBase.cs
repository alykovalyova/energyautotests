﻿using RestSharp;

namespace Autotests.Clients.Rest
{
    public abstract class RestRequestBase
    {
        public abstract string GetRoute();

        public abstract Method GetHttpMethod();
    }
}