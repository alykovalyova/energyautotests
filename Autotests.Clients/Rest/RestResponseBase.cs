﻿using System.Linq;
using System.Net;
using Autotests.Core;
using Newtonsoft.Json.Linq;

namespace Autotests.Clients.Rest
{
    public class RestResponseBase<T>
    {
        public ResponseHeader Header { get; set; }
        public T Data { get; set; }
        public string Content { get; set; }

        public RestResponseBase(ResponseHeader header = null, T data = default(T), string content = null)
        {
            Header = header;
            Data = data;
            Content = content;
        }
    }

    public class ResponseHeader
    {
        public bool? HasException { get; set; }
        public string Message { get; set; }
        public string InternalErrorCode { get; set; }
        public string[] ErrorDetails { get; set; }
        public HttpStatusCode? StatusCode { get; set; }
        public string ErrorCode { get; set; }

        public ResponseHeader(string json = null)
        {
            if (string.IsNullOrEmpty(json)) return;
            var jObject = JObject.Parse(json);
            if (jObject.TryGetValue(nameof(HasException), out var hasException))
                HasException = bool.Parse(hasException?.ToString() ?? string.Empty);
            if (jObject.TryGetValue(nameof(Message), out var message))
                Message = message?.ToString();
            if (jObject.TryGetValue("message", out message))
                Message = message?.ToString();
            if (jObject.TryGetValue(nameof(StatusCode), out var statusCode))
                StatusCode = (HttpStatusCode)int.Parse(statusCode?.ToString() ?? string.Empty);
            if (jObject.TryGetValue("error", out var error))
            {
                Message = error.ToString().GetJTokenAsString("message");
                InternalErrorCode = error.ToString().GetJTokenAsString(nameof(InternalErrorCode));
                ErrorDetails = error.ToString().GetJTokenAs<JArray>(nameof(ErrorDetails)).Select(t => t["message"].ToString()).ToArray();
            }
            if (jObject.TryGetValue("errors", out var errors))
            {
                ErrorCode = errors?.First?.ToString().GetJTokenAsString("errorCode");
                Message = errors?.First?.ToString().GetJTokenAsString("message");
            }
        }
    }
}