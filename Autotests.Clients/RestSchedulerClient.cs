﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Allure.NUnit.Attributes;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using NUnit.Framework;
using Nuts.Scheduler.Model.Contract;
using RestSharp.Authenticators;
using TriggerJobRequest = Autotests.Clients.Scheduler.Contract.TriggerJobRequest;

namespace Autotests.Clients
{
    public sealed class RestSchedulerClient : Singleton<RestSchedulerClient>
    {
        private static readonly string _apiUrl = ConfigHandler.Instance.ApiUrls.NutsScheduler;
        private readonly RestSharpClient _client;
        private readonly Dictionary<string, JobInfo> _jobMap;

        public RestSchedulerClient()
        {
            _client = new RestSharpClient(new Uri(_apiUrl), new NtlmAuthenticator());
            _jobMap = SetMap(GetAllJobInfos());
        }

        [AllureStep("Trigger - &jobName&")]
        public void TriggerJob(QuartzJobName jobName, params int[] ids)
        {
            AllureHelpers.AddAttachments(jobName);
            if (_jobMap.TryGetValue(jobName.ToString(), out var jobInfo))
            {
                var request = new TriggerJobRequest(jobInfo, ids);
                var response = _client.ExecuteCall(request.Data);
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new AssertionException($"Job wasn't triggered.\nErrorMessage: {response.ErrorMessage}\nStatusCode: {response.StatusCode}");
            }
            else
            {
                throw new Exception($"{jobName} hasn't been found in {nameof(_jobMap)}");
            }
        }

        private IEnumerable<JobInfo> GetAllJobInfos()
        {
            var response = new Scheduler.Contract.GetJobsRequest().CallWith<GetJobsResponse>(_client);
            if (response.Header.StatusCode != HttpStatusCode.OK)
                throw new AssertionException(response.Header.Message);
            return response.Data.Jobs;
        }

        private static Dictionary<string, JobInfo> SetMap(IEnumerable<JobInfo> jobInfos)
        {
            return jobInfos.ToDictionary(entry => entry.Group + "_" + entry.Name, entry => entry);
        }
    }
}