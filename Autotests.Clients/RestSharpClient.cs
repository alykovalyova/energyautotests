﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Allure.NUnit.Attributes;
using Autotests.Clients.Attributes;
using Autotests.Clients.Rest;
using Autotests.Core;
using Autotests.Core.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;

namespace Autotests.Clients
{
    public class RestSharpClient
    {
        private readonly IRestClient _client;
        private readonly bool _callFakeTimeLine;

        public RestSharpClient(Uri baseUrl)
        {
            _client = new RestClient(baseUrl);
        }

        public RestSharpClient(Uri baseUrl, bool callFakeTimeLine)
        {
            _client = new RestClient(baseUrl);
            _callFakeTimeLine = callFakeTimeLine;
        }

        public RestSharpClient(Uri baseUrl, IAuthenticator authenticator)
        {
            _client = new RestClient(baseUrl) {Authenticator = authenticator};
        }

        [AllureStep("Call: &request& With: RestSharpClient")]
        public RestResponseBase<TResponse> ExecuteCall<TResponse>(RestRequestBase request, string token = null,
            bool ignoreNullProps = false)
            where TResponse : new()
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request), $"{nameof(request)} can't be null.");

            var requestToSend = Build(request, ignoreNullProps);
            if (!string.IsNullOrEmpty(token))
                requestToSend.AddHeader("Authorization", $"Bearer {token}");
            if (_callFakeTimeLine)
            {
                requestToSend.AddHeader("Fake", "true");
            }

            var result = _client.Execute(requestToSend);
            if (!result.Content.Length.Equals(0) && result.Content.TryParseJArray())
            {
                var jObject = new JObject(new JProperty("Results", JArray.Parse(result.Content)));
                result.Content = jObject.ToString();
            }

            RestResponseBase<TResponse> response;
            ResponseHeader header;
            if (!result.Content.TryParseJObject())
            {
                header = new ResponseHeader
                {
                    StatusCode = result.StatusCode,
                    Message = result.ErrorMessage,
                    HasException = !result.IsSuccessful
                };
                response = new RestResponseBase<TResponse>(header, content: result.Content);
                AllureHelpers.AddAttachments(request, response);
                return response;
            }

            var content = result.Content;
            header = new ResponseHeader(content);
            if (!result.IsSuccessful)
            {
                if (header.StatusCode == null)
                    header.StatusCode = result.StatusCode;

                response = new RestResponseBase<TResponse>(header);
                AllureHelpers.AddAttachments(request, response);
                return response;
            }

            JObject.Parse(content).First?.Remove();
            var data = JsonConvert.DeserializeObject<TResponse>(JObject.Parse(content).ToString());
            response = new RestResponseBase<TResponse>(header, data);
            if (response.Header.StatusCode == null)
                response.Header.StatusCode = result.StatusCode;

            AllureHelpers.AddAttachments(request, response);
            return response;
        }

        public IRestResponse ExecuteCall(RestRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request), $"{nameof(request)} can't be null.");

            if (request.Parameters.Count == 0)
                throw new Exception(
                    $"{nameof(request.Parameters)} haven't been set. You should use 'Build' Method of request object.");
            request.AddHeader("Content-Type", "application/json");
            var result = _client.Execute(request);

            return result;
        }

        private static RestRequest Build<T>(T request, bool ignoreNullProps) where T : RestRequestBase
        {
            try
            {
                var requestToSend = new RestRequest(request.GetRoute(), request.GetHttpMethod());
                var customAttribute = request.GetType().GetCustomAttribute<GetTokenAttribute>();
                if (customAttribute != null)
                {
                    requestToSend.AddParameter("Content-Type", "application/x-www-form-urlencoded");
                    requestToSend.AddParameter("grant_type", "password");
                    requestToSend.AddParameter(nameof(customAttribute.Username).ToLower(), customAttribute.Username);
                    requestToSend.AddParameter(nameof(customAttribute.Password).ToLower(), customAttribute.Password);
                }
                else
                {
                    requestToSend.AddHeader("Content-Type", "application/json");
                    if (requestToSend.Method.Equals(Method.GET))
                    {
                        var props = request.GetType().GetProperties();
                        foreach (var prop in props)
                        {
                            var propValue = prop.GetValue(request);
                            if (prop.HasCustomAttribute<AsUrlSegmentAttribute>())
                            {
                                requestToSend.AddUrlSegment(prop.Name, propValue);
                            }
                            else
                            {
                                requestToSend.AddQueryParameter(prop.Name,
                                    propValue != null ? propValue.ToString() : string.Empty);
                            }
                        }
                    }
                    else
                    {
                        if (ignoreNullProps)
                        {
                            var nullPropNames = GetNullPropNames(request).ToList();
                            var contractResolver = new SerializerContractResolver();
                            foreach (var nullPropName in nullPropNames)
                            {
                                contractResolver.IgnoreProperty(nullPropName.Key, nullPropName.Value);
                            }

                            var json = JsonConvert.SerializeObject(request,
                                new JsonSerializerSettings {ContractResolver = contractResolver});
                            requestToSend.AddParameter("application/json", json, ParameterType.RequestBody);
                        }
                        else if (request.HasCustomAttribute<AsJsonArrayAttribute>())
                        {
                            var prop = request.GetType().GetProperties().Single().GetValue(request);
                            requestToSend.AddJsonBody(prop);
                        }
                        else
                        {
                            requestToSend.AddJsonBody(request);
                        }
                    }
                }

                return requestToSend;
            }
            catch (Exception e)
            {
                throw new AssertionException(e.Message);
            }
        }

        private static IEnumerable<KeyValuePair<Type, string>> GetNullPropNames(object root)
        {
            var props = root.GetType().GetProperties();
            foreach (var propertyInfo in props)
            {
                if (propertyInfo.HasCustomAttribute<NonIgnorableAttribute>())
                {
                    continue;
                }

                var propValue = propertyInfo.GetValue(root);
                var propType = propertyInfo.PropertyType;

                if (propValue == null)
                {
                    yield return new KeyValuePair<Type, string>(root.GetType(), propertyInfo.Name);
                    continue;
                }

                if (propValue is IList<string>)
                {
                    continue;
                }

                if (propValue is IList list)
                {
                    foreach (var value in list)
                    {
                        var nullPropNames = GetNullPropNames(value).ToList();
                        foreach (var nestedProp in nullPropNames)
                        {
                            yield return nestedProp;
                        }
                    }

                    continue;
                }

                if (propType.IsClass && propType != typeof(string))
                {
                    var nullPropNames = GetNullPropNames(propValue).ToList();
                    foreach (var nestedProp in nullPropNames)
                    {
                        yield return nestedProp;
                    }
                }
            }
        }
    }
}