﻿using Autotests.Clients.Rest;
using RestSharp;

namespace Autotests.Clients.Scheduler.Contract
{
    public class GetJobsRequest : RestRequestBase
    {
        private const string Route = "Job";
        private const Method Method = RestSharp.Method.GET;

        public override string GetRoute() => Route;

        public override Method GetHttpMethod() => Method;
    }
}