﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Nuts.Scheduler.Model.Contract;
using RestSharp;

namespace Autotests.Clients.Scheduler.Contract
{
    public class TriggerJobRequest
    {
        public RestRequest Data { get; }

        public class JobData
        {
            public int[] CampaignIds { get; set; }
        }

        public TriggerJobRequest(JobInfo jobInfo, int[] ids)
        {
            if (ids != null)
            {
                var campaignIds = new JobData()
                {
                    CampaignIds = ids
                };

                if (!jobInfo.DataMap.ContainsKey("Payload"))
                {
                    jobInfo.DataMap.Add("Payload", JsonConvert.SerializeObject(campaignIds));
                }

                jobInfo.DataMap["Payload"] = JsonConvert.SerializeObject(campaignIds);
            }
            var contractRequest = new Nuts.Scheduler.Model.Contract.TriggerJobRequest
            {
                Name = jobInfo.Name,
                Group = jobInfo.Group,
                DataMap = Cast(jobInfo.DataMap).ToDictionary(entry => (string)entry.Key, entry => entry.Value)
            };
            Data = new RestRequest(contractRequest.GetRouteUri(), Method.POST);
            Data.AddJsonBody(contractRequest);
        }

        private static IEnumerable<DictionaryEntry> Cast(IDictionary dictionary)
        {
            foreach (DictionaryEntry dictionaryEntry in dictionary)
            {
                yield return dictionaryEntry;
            }
        }
    }
}