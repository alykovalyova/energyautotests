﻿using System;
using System.ServiceModel;

namespace Autotests.Clients
{
    public class ServiceClient<T> : ClientBase<T> where T : class
    {
        public ServiceClient(string andPoint) 
            : base(new BasicHttpBinding
            {
                MaxReceivedMessageSize = int.MaxValue, 
                SendTimeout = TimeSpan.MaxValue
            }, new EndpointAddress(andPoint))
        {
        }

        public T Proxy => Channel;
    }
}