﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autotests.Core.Helpers;

namespace Autotests.Core
{
    public class BuildDirector
    {
        private readonly Dictionary<Type, IBuilder> _builders;

        public BuildDirector()
        {
            _builders = GetAllBuilders();
        }

        public T Get<T>() where T: class, IBuilder, new()
        {
            return (T) _builders[typeof(T)];
        }

        private static Dictionary<Type, IBuilder> GetAllBuilders()
        {
            var buildersSet = Assembly.GetCallingAssembly().GetTypes()
                .Where(t => typeof(IBuilder).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
                .ToDictionary(k => k, v => Activator.CreateInstance(v) as IBuilder);
            return buildersSet;
        }
    }
}