﻿using System;
using System.IO;

namespace Autotests.Core
{
    public static class ConfigHelper
    {
        private const string BaseDir = "Framework/Files/";

        public static string GetFilePath(string path) => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BaseDir + path);
    }
}