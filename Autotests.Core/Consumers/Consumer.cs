﻿using Autotests.Core.Handlers;
using Nuts.QueueExchange;
using Nuts.QueueExchange.ConnectionInfo;
using Nuts.QueueExchange.ConsumerInfo;

namespace Autotests.Core.Consumers
{
    public static class Consumer
    {
        public static Consumer<T> CreateConsumer<T>(string dispatcherName, string queueNameSuffix, string queueName) where T : class
        {
            var queueConnectionInfo = new QueueConnectionInfo(ConfigHandler.Instance.QueueSettings.HostNameQueue, int.Parse(ConfigHandler.Instance.QueueSettings.PortQueue),
                ConfigHandler.Instance.QueueSettings.UserQueue, ConfigHandler.Instance.QueueSettings.PasswordQueue, "Autotest");

            var queueConsumerInfo = new QueueConsumerInfo(dispatcherName, queueNameSuffix, queueName);

            return new Consumer<T>(queueConnectionInfo, queueConsumerInfo);
        }
    }
}
