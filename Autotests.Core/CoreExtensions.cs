﻿using AutoMapper;
using Autotests.Core.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Nuts.ApiClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Autotests.Core
{
    public static class CoreExtensions
    {
        public static T DeepClone<T>(this T instance)
        {
            if (instance == null)
                return default;
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, instance);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }

        public static T DeepCopy<T>(this T instance)
        {
            if (instance == null)
                return default;
            var clone = JsonConvert.SerializeObject(instance);
            return JsonConvert.DeserializeObject<T>(clone);
        }

        public static TDest To<TDest>(this object source, bool needToAssert = true)
        {
            var assembly = Assembly.GetCallingAssembly();
            var config = new MapperConfiguration(cfg => cfg.AddMaps(assembly));
            if (needToAssert)
                config.AssertConfigurationIsValid();
            return config.CreateMapper().Map<object, TDest>(source);
        }

        public static TRes TypeTo<TRes>(this object source)
        {
            return (TRes)Convert.ChangeType(source, typeof(TRes));
        }

        public static T RandomItem<T>(this IEnumerable<T> src)
        {
            if (src == null)
                throw new ArgumentNullException(nameof(src), $"{nameof(src)} can't be null.");
            var random = new Random();
            var list = src.ToList();
            if (list.Count < 1)
                return default;
            var point = random.Next(0, list.Count);
            return list.ElementAt(point);
        }

        public static bool TryParseJObject(this string json)
        {
            if (string.IsNullOrEmpty(json))
                return false;

            try
            {
                JObject.Parse(json);
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        public static bool TryParseJArray(this string json)
        {
            try
            {
                JArray.Parse(json);
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        public static byte GetQuarter(this DateTime date)
        {
            if (date.Month >= 1 && date.Month < 3)
                return 1;
            if (date.Month >= 4 && date.Month < 7)
                return 2;
            if (date.Month >= 7 && date.Month < 9)
                return 3;
            return 4;
        }

        public static T InCase<T>(this T builder, bool statement, Func<T, T> func) where T : IBuilder
        {
            return statement ? func(builder) : builder;
        }

        public static T IgnoreIfNull<T>(this T objectToCheck) where T : class, new()
        {
            if (objectToCheck == null)
                throw new IgnoreException($"{typeof(T).Name} is null.");

            return objectToCheck;
        }

        public static T GetJTokenAs<T>(this string source, string valueName, bool valueNameStartsWithLower = true)
        {
            if (string.IsNullOrEmpty(valueName))
                throw new ArgumentException($"{nameof(valueName)} must be specified.");
            if (valueNameStartsWithLower)
                valueName = $"{char.ToLowerInvariant(valueName[0])}{valueName.Substring(1)}";
            if (!TryParseJObject(source))
                return default;

            return !JObject.Parse(source).TryGetValue(valueName, out var jToken)
                ? default
                : JsonConvert.DeserializeObject<T>(jToken.ToString());
        }

        public static string GetJTokenAsString(this string source, string valueName,
            bool valueNameStartsWithLower = true)
        {
            if (string.IsNullOrEmpty(valueName))
                throw new ArgumentException($"{nameof(valueName)} must be specified.");
            if (valueNameStartsWithLower)
                valueName = $"{char.ToLowerInvariant(valueName[0])}{valueName.Substring(1)}";
            if (!TryParseJObject(source))
                return string.Empty;

            return !JObject.Parse(source).TryGetValue(valueName, out var jToken) ? string.Empty : jToken.ToString();
        }

        public static string RemoveSpecChars(this string source)
        {
            const string charsToRemove = " !#$%&'()*+,-./:;<=>?@[\"]^_`{|}~…";
            var name = new StringBuilder(source);
            foreach (var c in name.ToString().Where(c => charsToRemove.Contains(c)))
            {
                name.Replace(c.ToString(), "");
            }

            return name.ToString();
        }

        public static string GetTypeName(this object o)
        {
            return o.GetType().Name;
        }

        public static void IfTrue(this bool condition, Action action)
        {
            if (condition) action.Invoke();
        }

        public static bool HasCustomAttribute<T>(this object source) where T : Attribute
        {
            return source.GetType().GetCustomAttribute<T>() != null;
        }

        public static bool HasCustomAttribute<T>(this PropertyInfo property) where T : Attribute
        {
            return property.CustomAttributes.Any(a => a.AttributeType == typeof(T));
        }

        public static void StatusCodeShouldBe(this ApiResponseHeader header, HttpStatusCode expectation)
        {
            if (header.StatusCode != expectation)
            {
                if (header.Message != null)
                    Console.WriteLine($"Message: {header.Message}");
                if (header.ErrorDetails != null)
                {
                    Console.WriteLine($"ErrorDetails:\n");
                    foreach (var errorDetail in header.ErrorDetails)
                        Console.WriteLine(errorDetail);
                }

                throw new AssertionException($"Expected: {expectation}, but was: {header.StatusCode}.");
            }
        }
    }
}