﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Autotests.Core.CsvTool
{
    public class CsvCreator<T> where T : class
    {
        private readonly IEnumerable<T> _objects;
        private readonly string _separator;
        private readonly bool _header;

        public CsvCreator(IEnumerable<T> objects, string separator, bool header = false)
        {
            _objects = objects;
            _separator = separator;
            _header = header;
        }

        /// <summary>
        /// It generates csv data and returns as bytes array.
        /// </summary>
        /// <exception cref="EncoderFallbackException">It throws, when csv data can't be converted to bytes.</exception>
        public byte[] ToBytes()
        {
            try
            {
                return Encoding.UTF8.GetBytes(GetCsvData());
            }
            catch (EncoderFallbackException)
            {
                throw new EncoderFallbackException("Vouchers CSV data can't be converted to bytes.");
            }
        }

        private string GetCsvData()
        {
            ValidateCsvObjectsList();
            var csvData = new StringBuilder();

            if (_header)
            {
                csvData.AppendLine(ProcessCsvRow(GetCsvHeader(typeof(T))));
            }

            foreach (var obj in _objects)
            {
                csvData.AppendLine(ProcessCsvRow(GetCsvFields(obj)));
            }

            return csvData.ToString();
        }

        private string GetCsvHeader(Type type)
        {
            var csvHeader = new StringBuilder();
            foreach (var property in type.GetProperties().Where(prop => prop.CanRead))
            {
                csvHeader.Append($"{property.Name}{_separator}");
            }

            return csvHeader.ToString();
        }

        private string GetCsvFields(object obj)
        {
            var csvRow = new StringBuilder();
            var properties = obj.GetType().GetProperties().Where(prop => prop.CanRead).ToList();

            foreach (var value in properties.Select(property => property.GetValue(obj)))
            {
                csvRow.Append($"{ProcessCsvValue(value)}{_separator}");
            }

            return csvRow.ToString();
        }

        private string ProcessCsvRow(string row)
        {
            return row.Length > 0 ? row.Remove(row.Length - 1, 1) : string.Empty;
        }

        private static string ProcessCsvValue(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var csvValue = value.ToString();

            decimal decimalValue;

            if (decimal.TryParse(csvValue, out decimalValue))
            {
                csvValue = decimalValue.ToString(CultureInfo.InvariantCulture);
            }

            if (csvValue.Contains("\""))
            {
                csvValue = '"' + csvValue.Replace("\"", "\"\"") + '"';
            }

            return csvValue;
        }

        private void ValidateCsvObjectsList()
        {
            if (_objects == null || !_objects.Any())
            {
                throw new ArgumentNullException(nameof(_objects), "Csv object can't be null or empty.");
            }
        }
    }
}
