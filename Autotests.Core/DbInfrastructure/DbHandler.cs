﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Autotests.Core.DbInfrastructure
{
    public class DbHandler<TContext> where TContext : DbContext
    {
        private readonly string _connectionString;

        public DbHandler(string connectionString) => _connectionString = connectionString;

        public T AddEntityToDb<T>(T entity) where T : class
        {
            using (var repo = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return repo.Create(entity);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public List<T> AddEntitiesToDb<T>(IEnumerable<T> entities) where T : class
        {
            using (var repo = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return repo.CreateMany(entities).ToList();
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public void DeleteEntityByCondition<T>(Func<T, bool> condition) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    var entity = rep.Set.SingleOrDefault(condition);
                    rep.Delete(entity);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public void DeleteEntitiesByCondition<T>(Expression<Func<T, bool>> condition) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    var entities = rep.Set.Where(condition).ToList();
                    rep.DeleteMany(entities);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public void DeleteEntities<T>(List<T> entities) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    rep.DeleteMany(entities);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T GetEntityByCondition<T>(Expression<Func<T, bool>> condition, string including = null) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return including == null
                        ? rep.Set.FirstOrDefault(condition)
                        : rep.Set.Include(including).FirstOrDefault(condition);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T GetSingleEntityByCondition<T>(Expression<Func<T, bool>> condition, string including = null) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return including == null
                        ? rep.Set.SingleOrDefault(condition)
                        : rep.Set.Include(including).FirstOrDefault(condition);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T GetEntityByCondition<T>(Expression<Func<T, bool>> condition, params string[] includes) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    switch (includes.Length)
                    {
                        case 1:
                            return rep.Set.Include(includes[0]).FirstOrDefault(condition);
                        case 2:
                            return rep.Set.Include(includes[0]).Include(includes[1])
                                .FirstOrDefault(condition);
                        case 3:
                            return rep.Set.Include(includes[0]).Include(includes[1]).Include(includes[2])
                                .FirstOrDefault(condition);
                        case 4:
                            return rep.Set.Include(includes[0]).Include(includes[1]).Include(includes[2])
                                .Include(includes[3])
                                .FirstOrDefault(condition);
                        case 5:
                            return rep.Set.Include(includes[0]).Include(includes[1]).Include(includes[2])
                                .Include(includes[3]).Include(includes[4]).FirstOrDefault(condition);
                    }

                    return null;
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public List<T> GetEntitiesByCondition<T>(Expression<Func<T, bool>> condition, string including = null)
            where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return including == null
                        ? rep.Set.Where(condition).ToList()
                        : rep.Set.Include(including).Where(condition).ToList();
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
        }

        public List<T> GetEntitiesByCondition<T>(int take, Expression<Func<T, bool>> condition, string including = null)
            where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return including == null
                        ? rep.Set.Take(take).Where(condition).AsNoTracking().ToList()
                        : rep.Set.Take(take).Include(including).Where(condition).AsNoTracking().ToList();
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public TOut GetMaxEntity<TIn, TOut>(Expression<Func<TIn, TOut>> condition) where TIn : class
        {
            using (var rep = new Repository<TIn, TContext>(_connectionString))
            {
                try
                {
                    return rep.Set.Max(condition);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T GetLastEntity<T, TKey>(
            Expression<Func<T, TKey>> orderBy,
            Expression<Func<T, bool>> condition = null,
            string including = null) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    if (string.IsNullOrEmpty(including))
                    {
                        return condition == null
                            ? rep.Set.OrderByDescending(orderBy).FirstOrDefault()
                            : rep.Set.OrderByDescending(orderBy).FirstOrDefault(condition);
                    }

                    return condition == null
                        ? rep.Set.OrderByDescending(orderBy).Include(including).FirstOrDefault()
                        : rep.Set.OrderByDescending(orderBy).Include(including)
                            .FirstOrDefault(condition);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T GetLastEntity<T, TKey>(
            Expression<Func<T, TKey>> orderBy,
            Expression<Func<T, bool>> condition,
            params string[] includes) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    switch (includes.Length)
                    {
                        case 1:
                            return rep.Set.Include(includes[0]).FirstOrDefault(condition);
                        case 2:
                            return rep.Set.Include(includes[0]).Include(includes[1])
                                .FirstOrDefault(condition);
                        case 3:
                            return rep.Set.Include(includes[0]).Include(includes[1]).Include(includes[2])
                                .FirstOrDefault(condition);
                        case 4:
                            return rep.Set.Include(includes[0]).Include(includes[1]).Include(includes[2])
                                .Include(includes[3])
                                .FirstOrDefault(condition);
                        case 5:
                            return rep.Set.Include(includes[0]).Include(includes[1]).Include(includes[2])
                                .Include(includes[3]).Include(includes[4]).FirstOrDefault(condition);
                    }

                    return null;
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public TRes GetEntityByConditionWithJoin<T1, T2, TKey, TRes>(
            Expression<Func<T1, TKey>> outerKeySelector,
            Expression<Func<T2, TKey>> innerKeySelector,
            Expression<Func<T1, T2, TRes>> resultSelector,
            Expression<Func<TRes, bool>> condition) where T1 : class where T2 : class
        {
            var context = (TContext)Activator.CreateInstance(typeof(TContext), _connectionString);
            using (var rep1 = new Repository<T1, TContext>(context))
            {
                using (var rep2 = new Repository<T2, TContext>(context))
                {
                    try
                    {
                        return rep1.Set
                            .Join(rep2.Set, outerKeySelector, innerKeySelector, resultSelector)
                            .FirstOrDefault(condition);
                    }
                    catch (Exception e)
                    {
                        throw new AssertionException(e.Message);
                    }
                }
            }
        }

        public List<TRes> GetEntitiesByConditionWithJoin<T1, T2, TKey, TRes>(
            Expression<Func<T1, TKey>> outerKeySelector,
            Expression<Func<T2, TKey>> innerKeySelector,
            Expression<Func<T1, T2, TRes>> resultSelector,
            Expression<Func<TRes, bool>> condition,
            int? take = null) where T1 : class where T2 : class
        {
            var context = (TContext)Activator.CreateInstance(typeof(TContext), _connectionString);
            using (var rep1 = new Repository<T1, TContext>(context))
            {
                using (var rep2 = new Repository<T2, TContext>(context))
                {
                    try
                    {
                        return take == null
                            ? rep1.Set.Join(rep2.Set, outerKeySelector, innerKeySelector, resultSelector)
                                .Where(condition).ToList()
                            : rep1.Set.Join(rep2.Set, outerKeySelector, innerKeySelector, resultSelector)
                                .Where(condition).Take(take.GetValueOrDefault()).ToList();
                    }
                    catch (Exception e)
                    {
                        throw new AssertionException(e.Message);
                    }
                }
            }
        }

        public bool EntityIsInDb<T>(Expression<Func<T, bool>> condition) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    return rep.Set.Any(condition);
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T UpdateFirstEntity<T>(Expression<Func<T, bool>> condition, Action<T> action) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    var entity = rep.Set.FirstOrDefault(condition);
                    rep.Update(entity, action);
                    return entity;
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }

        public T UpdateSingleEntity<T>(Expression<Func<T, bool>> condition, Action<T> action) where T : class
        {
            using (var rep = new Repository<T, TContext>(_connectionString))
            {
                try
                {
                    var entity = rep.Set.SingleOrDefault(condition);
                    rep.Update(entity, action);
                    return entity;
                }
                catch (Exception e)
                {
                    throw new AssertionException(e.Message);
                }
            }
        }
    }
}