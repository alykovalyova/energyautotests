﻿using System;
using System.Linq;
using Autotests.Core.Handlers;
using Autotests.Repositories.ICARModels;
using Autotests.Repositories.MasterDataModels;
using Microsoft.EntityFrameworkCore;
using MduHistory = Autotests.Repositories.ICARModels.MduHistory;

namespace Autotests.Core.DbInfrastructure
{
    public class DbHelper
    {
        private static readonly ConfigHandler _config = ConfigHandler.Instance;

        public static MduHistory GetMduHistory(string eanId)
        {
            try
            {
                using (var rep =
                    new Repository<MduHistory, MasterDataContext>(_config.GetConnectionString("MasterDataDatabase")))
                {
                    var result = rep.Set
                        .Include(m => m.BalanceSupplier)
                        .Include(m => m.AllocationMethod)
                        .Include(m => m.MarketSegment)
                        .Include(m => m.EnergyDeliveryStatus)
                        .Include(m => m.EnergyFlowDirection)
                        .Include(m => m.MeteringMethod)
                        .Include(m => m.PhysicalCapacity)
                        .Include(m => m.PhysicalStatus)
                        .Include(m => m.ProfileCategory)
                        .Include(m => m.ProductType)
                        .Include(m => m.GridArea)
                        .Include(m => m.CapTarCode)
                        .FirstOrDefault(md => md.EanId == eanId);
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}