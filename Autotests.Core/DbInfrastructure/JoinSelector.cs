﻿namespace Autotests.Core.DbInfrastructure
{
    public class JoinSelector<TOuter, TInner>
    {
        public TOuter Outer { get; set; }
        public TInner Inner { get; set; }
    }
}
