﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Autotests.Core.DbInfrastructure
{
    public class Repository<TSet, TContext> : IDisposable 
        where TSet : class
        where TContext : DbContext
    {
        private readonly DbContext _dbContext;

        public DbSet<TSet> Set { get; }

        public Repository(string connectionString)
        {
            _dbContext = (TContext)Activator.CreateInstance(typeof(TContext), connectionString);
            Set = _dbContext.Set<TSet>();
        }

        public Repository(TContext context)
        {
            _dbContext = context;
            Set = _dbContext.Set<TSet>();
        }

        public TSet Create(TSet entity)
        {
            Set.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public IEnumerable<TSet> CreateMany(IEnumerable<TSet> entities)
        {
            var sets = entities as TSet[] ?? entities.ToArray();
            Set.AddRange(sets);
            _dbContext.SaveChanges();
            return sets;
        }

        public void Update(TSet entity, Action<TSet> action)
        {
            action(entity);
            _dbContext.SaveChanges();
        }

        public TSet Update(TSet entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
            return entity;
        }

        public void Delete(TSet entity)
        {
            Set.Remove(entity);
            _dbContext.SaveChanges();
        }

        public void DeleteMany(IEnumerable<TSet> entities)
        {
            Set.RemoveRange(entities);
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
