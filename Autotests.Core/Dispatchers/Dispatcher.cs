﻿using System.Collections.Generic;
using Autotests.Core.Handlers;
using Nuts.QueueExchange.ConnectionInfo;
using Nuts.QueueExchange.Factory.Containers;

namespace Autotests.Core.Dispatchers
{
    public class Dispatcher
    {
        private readonly string _dispatcher;
        private readonly string _hostName;

        public Dispatcher(string dispatcher, string hostName)
        {
            _dispatcher = dispatcher;
            _hostName = hostName;
            var connectionInfo = GetConnectionInfo();
            DispatcherFactoryContainer.Configurator.RegisterComponent(connectionInfo);
        }

        public void DispatchMessages<T>(ICollection<T> messages)
            where T : class
        {
            var dispatcher = DispatcherFactoryContainer.FactoryInstance.GetDispatcher<T>(_dispatcher);
            foreach (var message in messages)
            {
                dispatcher.Dispatch(message);
            }
        }

        public void DispatchMessage<T>(T message)
            where T : class
        {
            DispatchMessages(new[] { message });
        }

        protected QueueConnectionInfo GetConnectionInfo()
        {
            var config = ConfigHandler.Instance;
            var settings = new QueueConnectionSettings
            {
                ComponentName = _dispatcher,
                UserName = _dispatcher == "HomeApi" ? config.QueueSettings.HomeUserQueue : config.QueueSettings.UserQueue,
                Password = config.QueueSettings.PasswordQueue,
                QueueHostPort = int.Parse(config.QueueSettings.PortQueue),
                QueueHostName = _hostName
            };
            return new QueueConnectionInfo(settings);
        }
    }
}