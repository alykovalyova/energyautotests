﻿namespace Autotests.Core.Dispatchers
{
    public class DispatcherName
    {
        public const string CustomerInfo = "CustomerInformation";
        public const string PreContract = "PreContract";
        public const string EDSNGateway = "EDSNGateway";
        public const string MeteringPoint = "MeteringPoint";
        public const string P4EdsnGateway = "P4EdsnGateway";
        public const string ContactPerson = "ContactPerson";
        public const string AllInOne = "AllInOne.Contract";
        public const string Voucher = "Voucher";
        public const string Incentive = "HomeApi";
    }
}
