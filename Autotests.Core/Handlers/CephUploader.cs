﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Autotests.Core.Handlers
{
    public class CephUploader
    {
        private readonly string _cephKeyPath = GetCephKeyPath();
        private readonly string _serviceUrl = ConfigHandler.Instance.CephSettings.ServiceUrl;
        private readonly string _accessKey = ConfigHandler.Instance.CephSettings.AccessKey;
        private readonly string _secretKey = ConfigHandler.Instance.CephSettings.SecretKey;
        private readonly AmazonS3Config _config;

        public string LastBucketName => GetLastBucket().BucketName;
        public string CephKey => GetCephKeyFromFile();

        public CephUploader()
        {
            _config = new AmazonS3Config { ServiceURL = _serviceUrl };
        }

        public async Task UploadCephFile(string dirName, string fileName, string filePath)
        {
            if (IsCephFileInStorage(fileName))
            {
                Console.WriteLine($"{fileName} is already in Ceph.");
                return;
            }

            using (var client = new AmazonS3Client(_accessKey, _secretKey, _config))
            {
                var putRequest = new PutObjectRequest
                {
                    BucketName = GetLastBucket().BucketName,
                    Key = $"{dirName}/{fileName}",
                    ContentType = "text/pdf",
                    ContentBody = PdfToString(filePath)
                };
                var res = await client.PutObjectAsync(putRequest);
                //var response = _client.PutObject(putRequest);

                if (res.HttpStatusCode != HttpStatusCode.OK)
                    throw new AmazonS3Exception($"{fileName} hasn't been uploaded to {putRequest.BucketName}");

                RewriteCephKeyFile(putRequest.BucketName, putRequest.Key);
            }
        }

        private S3Bucket GetLastBucket()
        {
            S3Bucket bucket;
            try
            {
                using (var client = new AmazonS3Client(_accessKey, _secretKey, _config))
                {
                    var buckets = client.ListBucketsAsync().Result.Buckets;
                    //var buckets = client.ListBuckets().Buckets;
                    var maxDate = buckets.Max(b => b.CreationDate);
                    bucket = buckets.Single(b => b.CreationDate == maxDate);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

            return bucket;
        }

        public bool IsCephFileInStorage(string fileName)
        {
            using (var client = new AmazonS3Client(_accessKey, _secretKey, _config))
            {
                var buckets = client.ListBucketsAsync().Result.Buckets;
                //var buckets = _client.ListBuckets().Buckets;
                var check = new List<bool>();

                foreach (var bucket in buckets)
                {
                    var request = new ListObjectsRequest { BucketName = bucket.BucketName };
                    var response = client.ListObjectsAsync(request.BucketName).Result;
                    //var response = client.ListObjects(request.BucketName);
                    check.Add(response.S3Objects.Any(o => o.Key.Contains(fileName)));
                }

                return check.Any(c => c);
            }
        }

        private string PdfToString(string filePath)
        {
            try
            {
                var bytes = File.ReadAllBytes(filePath);
                return Encoding.Default.GetString(bytes);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private string GetCephKeyFromFile()
        {
            if (!File.Exists(_cephKeyPath))
                throw new FileNotFoundException("'CephKey.txt' file or 'VoucherFile' folder haven't been found.");

            using (var sr = new StreamReader(_cephKeyPath))
            {
                try
                {
                    return sr.ReadLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        }

        private void RewriteCephKeyFile(string bucketName, string key)
        {
            using (var sw = new StreamWriter(_cephKeyPath, false))
            {
                try
                {
                    sw.WriteLine($"{bucketName}/{key}");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        }

        private static string GetCephKeyPath()
        {
            //var codeBaseDir = new StringBuilder(Path.GetDirectoryName(
            //    Path.GetDirectoryName(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase))));
            //codeBaseDir.Replace("file:\\", "");

            //return Path.Combine(codeBaseDir.ToString(), "VoucherFile\\CephKey.txt");
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Framework/Files/Voucher/CephKey.txt");
        }
    }
}
