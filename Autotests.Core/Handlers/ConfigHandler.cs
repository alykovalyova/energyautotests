﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace Autotests.Core.Handlers
{
    public class ConfigHandler : Singleton<ConfigHandler>
    {
        private readonly IConfigurationRoot _config;

        public ApiUrls ApiUrls { get; }
        public ServicesEndpoints ServicesEndpoints { get; }
        public CephSettings CephSettings { get; }
        public EdsnCephSettings EdsnCephSettings { get; }
        public QueueSettings QueueSettings { get; }
        public SftpSettings SftpSettings { get; }
        public EdsnSftpSettings EdsnSftpSettings { get; }
        public IdentityServerSettings IdentityServerSettings { get; }
        public MongoDbSettings MongoDbSettings { get; }
        public VariablesSettings VariablesSettings { get; }

        public ConfigHandler()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            _config = builder.Build();

            ApiUrls = new ApiUrls();
            ServicesEndpoints = new ServicesEndpoints();
            CephSettings = new CephSettings();
            EdsnCephSettings = new EdsnCephSettings();
            QueueSettings = new QueueSettings();
            SftpSettings = new SftpSettings();
            EdsnSftpSettings = new EdsnSftpSettings();
            IdentityServerSettings = new IdentityServerSettings();
            MongoDbSettings = new MongoDbSettings();
            VariablesSettings = new VariablesSettings();

            _config.GetSection(nameof(ApiUrls)).Bind(ApiUrls);
            _config.GetSection(nameof(ServicesEndpoints)).Bind(ServicesEndpoints);
            _config.GetSection(nameof(CephSettings)).Bind(CephSettings);
            _config.GetSection(nameof(EdsnCephSettings)).Bind(EdsnCephSettings);
            _config.GetSection(nameof(QueueSettings)).Bind(QueueSettings);
            _config.GetSection(nameof(SftpSettings)).Bind(SftpSettings);
            _config.GetSection(nameof(EdsnSftpSettings)).Bind(EdsnSftpSettings);
            _config.GetSection(nameof(IdentityServerSettings)).Bind(IdentityServerSettings);
            _config.GetSection(nameof(MongoDbSettings)).Bind(MongoDbSettings);
            _config.GetSection(nameof(VariablesSettings)).Bind(VariablesSettings);
        }

        public string GetConnectionString(string dbName) => _config.GetConnectionString(dbName);
    }

    public class ApiUrls
    {
        public string SalesChannel { get; set; }
        public string Reseller { get; set; }
        public string ResellerApiB2C { get; set; }
        public string ResellerApiB2B { get; set; }
        public string Renewal { get; set; }
        public string RegisterSync { get; set; }
        public string Mp { get; set; }
        public string NutsUi { get; set; }
        public string Bag { get; set; }
        public string Ccu { get; set; }
        public string MarketParty { get; set; }
        public string Mdu { get; set; }
        public string ChangeSupplier { get; set; }
        public string MoveIn { get; set; }
        public string MoveOut { get; set; }
        public string MpSupply { get; set; }
        public string MpPre { get; set; }
        public string MpOffer { get; set; }
        public string MpInfo { get; set; }
        public string Questionnaire { get; set; }
        public string IC92 { get; set; }
        public string GOTariff { get; set; }
        public string EdsnSwitch { get; set; }
        public string BEApi { get; set; }
        public string OfferDiffer { get; set; }
        public string MobileAppApi { get; set; }
        public string FakeEdsn { get; set; }
        public string UsageCalculation { get; set; }
        public string P4Daily { get; set; }
        public string P4Event { get; set; }
        public string PostOffice { get; set; }
        public string IdentityService { get; set; }
        public string Insurance { get; set; }
        public string NutsScheduler { get; set; }
        public string OfferBundle { get; set; }
        public string Incentive { get; set; }
        public string OptIn { get; set; }
    }

    public class ServicesEndpoints
    {
        public string IcarMeteringPointService { get; set; }
        public string ChangeOfSupplierService { get; set; }
        public string MoveInService { get; set; }
        public string MoveOutService { get; set; }
        public string EndOfSupplyService { get; set; }
        public string PublishCustomerInfoService { get; set; }
        public string ContractDataService { get; set; }
        public string EgwMeteringPointService { get; set; }
        public string MMVoucherService { get; set; }
        public string ProdManService { get; set; }
        public string FakeEdsnAdministrativeService { get; set; }
        public string BillingOfferService { get; set; }
        public string ProspectService { get; set; }
        public string CsdMeterReadingService { get; set; }
        public string EgwMeterReadingService { get; set; }
        public string FesMeterReadingService { get; set; }
        public string EgwDataSupplyingService { get; set; }
        public string P4Service { get; set; }
    }

    public class CephSettings
    {
        public string ServiceUrl { get; set; }
        public string BucketNamePrefix { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string CephKey { get; set; }
        public string FtpFileName { get; set; }
    }

    public class EdsnCephSettings
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string BucketName { get; set; }
        public string CephKey { get; set; }
        public string BalanceSupplierId { get; set; }
        public string ExternalReference { get; set; }
        public string FileHash { get; set; }
    }

    public class QueueSettings
    {
        public string HostNameQueue { get; set; }
        public string AllInOneHostNameQueue { get; set; }
        public string HomeHostNameQueue { get; set; }
        public string PortQueue { get; set; }
        public string UserQueue { get; set; }
        public string HomeUserQueue { get; set; }
        public string PasswordQueue { get; set; }
    }

    public class SftpSettings
    {
        public string SftpResponseCsvPath { get; set; }
        public string SftpRequestCsvPath { get; set; }
        public string SftpErrorCsvPath { get; set; }
        public string SftpRestValueSuccessCsvPath { get; set; }
        public string SFTPHost { get; set; }
        public string SFTPUser { get; set; }
        public string SFTPPass { get; set; }
    }

    public class EdsnSftpSettings
    {
        public string SFTPHost { get; set; }
        public string SFTPUser { get; set; }
        public string SFTPPass { get; set; }
        public string SFTPPort { get; set; }
        public string SFTPFilePath { get; set; }
    }

    public class IdentityServerSettings
    {
        public string UserPassword { get; set; }
        public string PasswordHash { get; set; }
        public string ClientSecrete { get; set; }
    }

    public class MongoDbSettings
    {
        public string DbName { get; set; }
        public string ConnectionString { get; set; }
    }

    public class VariablesSettings
    {
        public string Path { get; set; }
    }
}