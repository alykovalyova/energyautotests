﻿﻿using Amazon.S3;
using Amazon.S3.Model;
using NUnit.Framework;
using Nuts.CustomerInformation.Contract.Message;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Autotests.Core.Handlers
{
    public class EdsnCephUploader
    {
        private readonly string _serviceUrl = ConfigHandler.Instance.CephSettings.ServiceUrl;
        private readonly string _accessKey = ConfigHandler.Instance.EdsnCephSettings.AccessKey;
        private readonly string _secretKey = ConfigHandler.Instance.EdsnCephSettings.SecretKey;
        private readonly string _bucket = ConfigHandler.Instance.EdsnCephSettings.BucketName;
        private readonly string _cephKey;
        private readonly AmazonS3Config _config;

        public EdsnCephUploader(string cephKey)
        {
            _config = new AmazonS3Config { ServiceURL = _serviceUrl };
            _cephKey = cephKey;
        }

        public async void UploadCephFile()
        {
            if (IsCephFileInStorage())
            {
                Console.WriteLine($"{_cephKey} is already in Ceph.");
                return;
            }

            using (var client = new AmazonS3Client(_accessKey, _secretKey, _config))
            {
                var putRequest = new PutObjectRequest
                {
                    BucketName = _bucket,
                    Key = _cephKey,
                    ContentType = "*.csv",
                    ContentBody = CsvToString(GetLocalFilePath(_cephKey))
                };
                var res = await client.PutObjectAsync(putRequest);

                if (res.HttpStatusCode != HttpStatusCode.OK)
                    throw new AmazonS3Exception($"{_cephKey} hasn't been uploaded to {putRequest.BucketName}");
            }
        }

        public bool IsCephFileInStorage()
        {
            using (var client = new AmazonS3Client(_accessKey, _secretKey, _config))
            {
                var buckets = client.ListBucketsAsync().Result.Buckets;

                var check = new List<bool>();

                foreach (var bucket in buckets)
                {
                    var request = new ListObjectsRequest { BucketName = bucket.BucketName };
                    var response = client.ListObjectsAsync(request.BucketName).Result;
                    check.Add(response.S3Objects.Any(o => o.Key.Contains(_cephKey)));
                }

                return check.Any(c => c);
            }
        }

        private string CsvToString(string filePath)
        {
            try
            {
                var bytes = File.ReadAllBytes(filePath);
                return Encoding.Default.GetString(bytes);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private string GetLocalFilePath(string fileName)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Framework\\Files\\EdsnGateway\\{fileName}");
        }

        public UploadCerFileMessage GetUploadCerFileMesageModel()
        {
            return new UploadCerFileMessage()
            {
                CephKey = _cephKey,
                CephBucket = _bucket,
                ExternalReference = ConfigHandler.Instance.EdsnCephSettings.ExternalReference,
                FileHash = ConfigHandler.Instance.EdsnCephSettings.FileHash,
                BalanceSupplierId = ConfigHandler.Instance.EdsnCephSettings.BalanceSupplierId
            };
        }
    }
}