﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Autotests.Core.Handlers
{
    public static class XmlObjectsHandler<T> where T : class, new()
    {
        private static readonly XmlSerializer _serializer = new XmlSerializer(typeof(T));

        /// <summary>
        /// Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="path"></param>
        public static void Serialize(T source, string path)
        {
            try
            {
                using (TextWriter tw = new StreamWriter(path))
                {
                    _serializer.Serialize(tw, source);
                }
            }  
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Serializes the specified <see cref="T:System.Object" /> to string.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string Serialize(T source)
        {
            try
            {
                using (var sw = new StringWriter())
                {
                    _serializer.Serialize(sw, source);
                    return sw.ToString();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Reads file and Deserializes it content to the specified <see cref="T:System.Object" />.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T Deserialize(string path)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.Open))
                {
                    return _serializer.Deserialize(fs).TypeTo<T>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}