﻿using System;
using System.Text;
using System.Threading.Tasks;
using Allure.Commons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nuts.ApiClient;

namespace Autotests.Core.Helpers
{
    public static class AllureHelpers
    {
        public static void Log(JObject message)
        {
            AllureLifecycle.Instance.AddAttachment("Log:", AllureLifecycle.AttachFormat.Json, message.ToString());
        }

        public static void AddAttachments(params object[] attachments)
        {
            foreach (var attachment in attachments)
            {
                AllureLifecycle.Instance.AddAttachment($"{attachment.GetType().Name}_Attachment",
                    AllureLifecycle.AttachFormat.Json, JsonConvert.SerializeObject(attachment));
            }
        }

        public static void AddAttachments<TResponse>(object request, Task<ApiResponse<TResponse>> response)
        {
            AllureLifecycle.Instance.AddAttachment($"{request.GetType().Name}", AllureLifecycle.AttachFormat.Json,
                JsonConvert.SerializeObject(request));
            AllureLifecycle.Instance.AddAttachment($"{typeof(TResponse).Name}", AllureLifecycle.AttachFormat.Json,
                JsonConvert.SerializeObject(response.Result));
        }

        public static string ClearStepName(string methodName)
        {
            if (string.IsNullOrEmpty(methodName))
                throw new ArgumentNullException(nameof(methodName), "methodName cannot be null or empty.");

            var name = new StringBuilder(methodName);
            name.Replace("b_", "");
            for (var i = 0; i < name.Length; i++)
            {
                if (!char.IsDigit(name[i]) && name[i] != '_' && name[i] != '<' && name[i] != '>') continue;
                name.Remove(i, 1);
                i--;
            }

            return name.ToString();
        }
    }
}
