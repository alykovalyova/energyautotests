﻿namespace Autotests.Core.Helpers
{
    public enum BankCode
    {
        ABNA,
        RABO,
        INGB,
        SNB
    }
}