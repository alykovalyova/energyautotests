﻿namespace Autotests.Core.Helpers
{
    public struct Categories
    {
        public const string GoTariff = "GoTariff";
        public const string ConsumptionRegression = "ConsumptionRegression";
        public const string ConsumptionSmoke = "ConsumptionSmoke";
        public const string IC92 = "IC92";
        public const string Questionnaire = "Questionnaire";
        public const string Reseller = "Reseller";
        public const string ResellerApi = "ResellerApi";
        public const string Voucher = "Voucher";
        public const string Prospect = "Prospect";
        public const string Icar = "Icar";
        public const string IcarServices = "IcarServices";
        public const string GtwServices = "GtwServices";
        public const string SalesChannel = "SalesChannel";
        public const string ProdMan = "ProdMan";
        public const string BeApi = "BeApi";
        public const string OfferDifferentiation = "OfferDifferentiation";
        public const string Renewal = "Renewal";
        public const string RegisterSync = "RegisterSync";
        public const string MobileAppApi = "MobileAppApi";
        public const string UsageCalculation = "UsageCalculation";
        public const string P4DailyReadings = "P4DailyReadings";
        public const string P4EventReadings = "P4EventReadings";
        public const string Insurance = "Insurance";
        public const string OfferBundle = "OfferBundle";
        public const string EdsnEndpoints = "EdsnEndpoints";
        public const string Incentive = "Incentive";
        public const string OptIn = "OptIn";
    }
}