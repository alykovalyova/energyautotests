﻿using SinKien.IBAN4Net;

namespace Autotests.Core.Helpers
{
    public static class IbanProvider
    {
        public static string GetIbanNumber(BankCode bankCode)
        {
            return new IbanBuilder()
                .CountryCode(CountryCode.GetCountryCode("NL"))
                .NationalCheckDigit(RandomDataProvider.GetRandomNumbersString(2))
                .BankCode(bankCode.ToString())
                .AccountNumber(RandomDataProvider.GetRandomNumbersString(10))
                .Build().Value;
        }
    }
}