﻿using static System.Activator;

namespace Autotests.Core.Helpers
{
    public abstract class MainBuilder<T> : IBuilder
    {
        public T Instance;

        public MainBuilder()
        {
            Instance = CreateInstance<T>();
        }

        public T Build()
        {
            var res = Instance;
            Instance = CreateInstance<T>();
            return res;
        }
    }
}