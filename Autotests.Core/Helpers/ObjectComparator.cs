﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using static System.String;

namespace Autotests.Core.Helpers
{
    public static class ObjectComparator
    {
        private static readonly List<string> _errors = new List<string>();
        private const string ValueIsNotEqual = "Value of {0} [{1}] isn't equal to value of {2} [{3}]";
        private const string ListOutOfRange = "Count of Actual List is {0} and Pattern List is {1}.";
        private const string DictOutOfRange = "Count of Actual Dictionary is {0} and Pattern Dictionary is {1}.";
        private const string ListsAreNotEqual = "Items of the {0} List aren't equal to items of the {1} List.";

        /// <summary>
        /// Validates the properties of both argument types on equality.
        /// </summary>
        /// <typeparam name="TActual"></typeparam>
        /// <typeparam name="TPattern"></typeparam>
        /// <param name="actual"></param>
        /// <param name="pattern"></param>
        /// <param name="needToRepeat"></param>
        /// <param name="ignores"></param>
        /// <returns></returns>
        public static bool ComparePropsOfTypes<TActual, TPattern>(
            TActual actual, TPattern pattern, 
            bool needToRepeat = true, params string[] ignores)
        {
            var res = new List<bool>();
            var actualProps = actual.GetType().GetProperties();

            foreach (var actualProp in actualProps)
            {
                if (ignores.Contains(actualProp.Name))
                    continue;
                try
                {
                    var patternProp = pattern.GetType().GetProperty(actualProp.Name);
                    if (patternProp == null && needToRepeat)
                    {
                        RepeatSearch(pattern, actualProp, ref patternProp);
                        if (patternProp == null) continue;
                    }
                    object actualValue;
                    object patternValue;
                    ExtractValuesFromProperty(out actualValue, out patternValue, actualProp, patternProp, actual, pattern);
                    if (!(patternProp.PropertyType.IsValueType || patternProp.PropertyType == typeof(string)
                          && actualProp.PropertyType.IsValueType || actualProp.PropertyType == typeof(string)))
                    {
                        if (actualValue is IList<string> || patternValue is IList<string>)
                        {
                            ValidateLists<string>(actualValue, patternValue, res);
                            continue;
                        }
                        if (actualValue.GetType().IsArray || patternValue.GetType().IsArray ||
                            actualValue is IList || patternValue is IList)
                        {
                            ValidateArrays(patternValue, actualValue, res);
                            continue;
                        }
                        if (actualValue is IDictionary<string, string> || patternValue is IDictionary<string, string>)
                        {
                            ValidateDictionaries<string, string>(actualValue, patternValue, res);
                            continue;
                        }
                        res.Add(ComparePropsOfTypes(actualValue, patternValue));
                        continue;
                    }
                    LeadValuesToOneType(ref actualValue, ref patternValue);
                    RetrieveDateFromDateTimeObjects(ref actualValue, ref patternValue);
                    if (actualValue.Equals(patternValue) || actualValue == patternValue)
                        res.Add(true);
                    else
                    {
                        res.Add(false);
                        var errorMessage = Format(ValueIsNotEqual, actualProp.Name, actualValue, patternProp.Name, patternValue);
                        Console.WriteLine(errorMessage);
                        _errors.Add(errorMessage);
                    }
                }
                catch (ArgumentOutOfRangeException e)
                {
                    res.Add(false);
                    Console.WriteLine(e.Message);
                    _errors.Add(e.Message);
                }
                catch (NullReferenceException)
                {
                    // ignored
                }
                catch (TargetInvocationException)
                {
                    // ignored
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    _errors.Add(e.Message);
                }
            }

            AllureHelpers.AddAttachments(_errors);
            return res.TrueForAll(el => el);
        }

        private static void RetrieveDateFromDateTimeObjects(ref object actualValue, ref object patternValue)
        {
            if (!(actualValue is DateTime) || !(patternValue is DateTime)) return;
            actualValue = ((DateTime)actualValue).Date;
            patternValue = ((DateTime)patternValue).Date;
        }

        private static void ValidateLists<T>(object actualValue, object patternValue, ICollection<bool> res)
        {
            var actualList = (IList<T>) actualValue;
            var patternList = (IList<T>) patternValue;
            if (actualList.Count == 0 & patternList.Count == 0) return;
            if ((actualList.Count == 0 & patternList.Count > 0) | (actualList.Count > 0 & patternList.Count == 0))
                throw new ArgumentOutOfRangeException(Format(ListOutOfRange, actualList.Count, patternList.Count));
            var check = actualList.Concat(patternList).Distinct().Count() == (actualList.Count + patternList.Count) / 2;
            res.Add(check);
            if (!check)
                throw new Exception(Format(ListsAreNotEqual, nameof(actualValue), nameof(patternValue)));
        }

        private static void ValidateArrays<TActual, TPattern>(TActual patternValue, TPattern actualValue, ICollection<bool> res)
        {
            var actualArray = ((IEnumerable<TActual>) actualValue).ToList();
            var patternArray = ((IEnumerable<TPattern>) patternValue).ToList();
            if (actualArray.Count == 0 & patternArray.Count == 0) return;
            if ((actualArray.Count == 0 & patternArray.Count > 0) | (actualArray.Count > 0 & patternArray.Count == 0))
                throw new ArgumentOutOfRangeException(Format(ListOutOfRange, actualArray.Count, patternArray.Count));
            for (var i = 0; i < patternArray.Count; i++)
                res.Add(ComparePropsOfTypes(actualArray[i], patternArray[i]));
        }

        private static void ValidateDictionaries<TKey, TValue>(object actualValue, object patternValue, ICollection<bool> res)
        {
            var actualDictionary = (IDictionary<TKey, TValue>)actualValue;
            var patternDictionary = (IDictionary<TKey, TValue>)patternValue;
            if (actualDictionary.Count == 0 & patternDictionary.Count == 0) return;
            if ((actualDictionary.Count == 0 & patternDictionary.Count > 0) | (actualDictionary.Count > 0 & patternDictionary.Count == 0))
                throw new ArgumentOutOfRangeException(Format(DictOutOfRange, actualDictionary.Count, patternDictionary.Count));
            for (var i = 0; i < patternDictionary.Count; i++)
                res.Add(ComparePropsOfTypes(actualDictionary.ElementAt(i), patternDictionary.ElementAt(i)));
        }

        private static void RepeatSearch<TPattern>(TPattern pattern, PropertyInfo actualProp, ref PropertyInfo patternProp)
        {
            var patternProps = pattern.GetType().GetProperties();
            foreach (var property in patternProps)
            {
                if (!property.Name.ToLower().Contains(actualProp.Name.ToLower())
                    && !actualProp.Name.ToLower().Contains(property.Name.ToLower()))
                {
                    continue;
                }
                patternProp = property;
                break;
            }
        }

        private static void ExtractValuesFromProperty<TActual, TPattern>(
            out object actualValue, out object patternValue, 
            PropertyInfo actualProp, PropertyInfo patternProp,
            TActual actual, TPattern pattern)
        {
            actualValue = actualProp.GetValue(actual).GetType().IsEnum
                ? (int)actualProp.GetValue(actual) : actualProp.GetValue(actual);
            patternValue = patternProp.GetValue(pattern).GetType().IsEnum
                ? (int)patternProp.GetValue(pattern) : patternProp.GetValue(pattern);
        }

        private static void LeadValuesToOneType(ref object actualValue, ref object patternValue)
        {
            var actualType = actualValue.GetType();
            var patternType = patternValue.GetType();
            if (actualType == patternType) return;
            if (actualType == typeof(string) && patternType == typeof(int))
            {
                actualValue = int.Parse((string) actualValue);
            }
            else if (actualType == typeof(int) && patternType == typeof(string))
            {
                patternValue = int.Parse((string) patternValue);
            }
            else if (actualType == typeof(string) && patternType == typeof(bool))
            {
                actualValue = bool.Parse((string) actualValue);
            }
            else if(actualType == typeof(bool) && patternType == typeof(string))
            {
                patternValue = bool.Parse((string) patternValue);
            }
            else if (actualType == typeof(byte) && patternType == typeof(int))
            {
                var bc = new ByteConverter();
                actualValue = bc.ConvertTo(actualValue, patternType);
            }
            else if (actualType == typeof(int) && patternType == typeof(byte))
            {
                var bc = new ByteConverter();
                patternValue = bc.ConvertTo(patternValue, actualType);
            }
        }
    }
}
