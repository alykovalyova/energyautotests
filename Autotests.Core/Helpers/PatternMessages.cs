﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Autotests.Core.Helpers
{
    public static class PatternMessages
    {
        public static readonly Regex _zipCodeRegex = new Regex("^[1-9][0-9]{3}\\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$");
        public static readonly Regex _resellerApiZipCodeRegex = new Regex("^[1-9][0-9]{3}(?!SA|SD|SS)[a-zA-Z]{2}$");
        public static readonly Regex EanIdRegex = new Regex("^[0-9]{18}$");

        #region General
        public const string FieldIsRequired = "The {0} field is required.";
        public const string FieldIsInvalid = "The field {0} is invalid.";
        public const string FieldIsRequired2 = "The field {0} is required.";
        public const string ToDateLessThanFromDate = "ToDate can't be less than the FromDate.";
        public const string InvalidEanField = "The field EanId must match the regular expression ^[0-9]{18}$";
        public const string InvalidGridOperatorField = "The field GridOperator must match the regular expression ^[0-9]{13}$";
        public const string InvalidSupplierField = "The field BalanceSupplier must match the regular expression ^[0-9]{13}$";
        #endregion
        #region Consumption
        public const string NoDossierForReading = "MeterReadingsSet with Ean ('{0}') has reading with MarketEvent {1} and empty DossierId";
        public const string NotEnoughHistoricalData = "There is no historic date available to do the calculation, for ean {0}.";
        public const string NoProfileCategoryForTlv = "Can't calculate usage: MeteringPoint has no ProfileCategory";
        public const string NoProfileCategoryForUsageByMeterReading = "Can't calculate usage: MeteringPoint has no ProfileCategory for ean {0}.";
        public const string NoReadingsFoundForUsageCalculation = "Can't calculate usage: can't find last accepted reading";
        public const string NoStartReadingIsFound = "Can't calculate usage: can't find previous accepted reading early than {0}";
        public const string WrongRegisterSet = "Can't calculate usage. Can't find register from Metering Point. Direction: {0}, tariff type: {1}";
        public const string NotEnoughDataWhenExchangeOfMeter =
            "Can't determine intervals for calculating yearly usage for readings with exchange of meter";
        public const string EstimationAssertMessage = "Estimations min and max are unexpectedly equal";
        public const string InvalidTfSignalReading = "Tf signal LVR Readings value is not in the trend";
        #endregion
        #region GoTariff
        public const string GoTariffNotFound = "Grid Operator tariff with Id {0} not found";
        public const string StatusTransitionNotAllowed = "Status transition not allowed '{0}' -> '{1}'";
        public const string StatusDoesNotHaveTransition = "Current status: '{0}' - does not have allowed transition";
        public const string InvalidModifiedByLength = "The field ModifiedBy must be a string or array type with a maximum length of '255'.";
        public const string InvalidStatus = "Please enter one of the allowable values for Status: New, Approved, Rejected, Accepted.";
        public const string InvalidRange = "The field {0} must be between 1 and 2147483647.";
        public const string NoParamsError = "Request must have at least one parameter.";
        #endregion
        #region IC92Settlements
        public const string TariffExisted = "Tariff for date '{0}' already exists";
        public const string InvalidYearRegex = "The field Year must match the regular expression '^[0-9]{4}$'.";
        public const string InvalidQuarterRegex = "The field Quarter must match the regular expression '^[1-4]{1}$'.";
        public const string InvalidEanRegex = "The field Ean must match the regular expression '^[0-9]{18}$'.";
        public const string WrongModifiedByLength = "The field ModifiedBy must be a string or array type with a maximum length of '255'.";
        public const string WrongModifiedByLength2 = "The field ModifiedBy must be a string with a maximum length of 255.";
        public const string InvalidPrice = "The field {0} must be between 0 and 1.7976931348623157E+308.";
        public const string InvalidIntFieldValue = "The field {0} must be between 1 and 2147483647.";
        public const string TariffNotFound = "Can't find tariff to update id {0}";
        public const string IndicateYearMessage = "Please indicate the year (quarter without year is invalid).";
        public const string InvalidEventDateValue = "Property EventDate contains an invalid value of (System.DateTime) type or default value.";
        public const string InvalidArrayFieldLength = "The field {0} must be a string or array type with a minimum length of '1'.";
        public const string SettlementNotFound = "Settlement with Id: {0} does not exist in database.";
        public const string InvalidLabel = "Please enter one of the allowable values for Label: BudgetEnergie, NLE.";
        public const string InvalidSettlementStatuses = "{0} options are not valid for enum New, InProforma, Accepted, Invoiced, Canceled";
        public const string InvalidUpdateStatus = "Please enter one of the allowable values for SettlementStatus: New, InProforma, Accepted, Invoiced, Canceled.";
        public const string InvalidMeteringDirection = "Please enter one of the allowable values for MeteringDirection: CMB, LVR, TLV.";
        public const string InvalidProductType = "Please enter one of the allowable values for ProductType: ELK, GAS.";
        public const string InvalidTariffTape = "Please enter one of the allowable values for TariffType: L, N, T.";
        public const string SettlementAlreadyHasStatus = "Settlements with Ids: {0} already have target status.";
        public const string InvalidStatusToInvoice = "All provided settlements must have one the following statuses InProforma status.";
        public const string WrongSettlementStatus = "Settlements with ids {0} are not allowed for target status transitions";
        #endregion
        #region Questionnaire
        public const string InvalidDestinationValue = "Please enter one of the allowable values for Destination: CustomerOnline, NutsUI, Dashboard, LeadOnline.";
        public const string InvalidSourceValue = "Please enter one of the allowable values for Source: CustomerOnline, NutsUI.";
        public const string NoRelationIdFound = "The field RelationId must be between 1 and 2147483647.";
        public const string NoQuestionId = "The field QuestionId must be between 1 and 2147483647.";
        public const string NoAnswerInfo = "Answers should contain at least one item.";
        public const string ExistingRelationId = "Survey for relationId {0} already exists";
        public const string AnswerHasWrongField = "Answer should not have text if answerId is specified";
        public const string NoEmailOrPhone = "Phone or email should be provided";
        public const string DublicationFound = "Answer duplication found";
        public const string MultipleAnswersToOneQuestion = "Multiple answers sets are not allowed for the same question";
        #endregion
        #region Reseller
        public const string SalesChannelAlreadyExists = "Specified sales channels already exists in db";
        public const string ResellerAlreadyExists = "Specified name for reseller already exists in db";
        public const string InvalidSalesChannelIdRange = "The field SalesChannelId must be between 1 and 2147483647.";
        public const string ResellerWasNotFound = "Reseller with id = {0} was not found.";
        public const string InvalidResellerIdValue = "Property ResellerId contains an invalid value of (System.Nullable`1[System.Guid]) type or default value.";
        public const string OfferBundlesAreNotFound = "There are no offerBundles for salesChannel id = {0}";
        #endregion
        #region ResellerApi
        public const string AddressNotFound = "\"Voor deze combinatie van postcode en huisnummer hebben wij geen aansluitingen kunnen vinden.\"";        
        public const string EanNotFound = "EAN-code aansluiting onbekend: Het verzoek betreft een onbekende aansluiting.";
        public const string NoKeySpecified = "Parameters BirthDayKey and IBANKey are empty. Need to specify at least one of them.";
        public const string EdsnRejection = "EDSN Rejection. RejectionCode: 607.Rejection Text: Geen klantsleutel aanwezig voor deze aansluiting in de centrale klantsleuteladministratie";
        public const string NoGasOrElkSpecified = "ElectricityConnection and GasConnection are empty. Need to specify at least one of them.";
        public const string WrongCheckboxState = "HasOptedInByCheckbox and RequestOrigin have to be set to true for FaceToFace.";
        public const string InvalidSurname = "Failed validation, The PersonalDetails.Surname:  has incorrect format.";
        public const string InvalidSalesOfficeId = "Invalid SalesOfficeId {0}";
        public const string InvalidInitials = "The PersonalDetails.Initials: '' has incorrect format.";
        public const string InvalidIban = "The PersonalDetails.BankAccountNumber:'' is not a valid IBAN.";
        public const string AssertMessage = "Status of the proposition hasn't been changed to: ";
        public const string ZipCodeInvalid = "zip, you submitted ('zip') does not match regular expression ^[1-9][0-9]{3}(?!SA|SD|SS)[a-zA-Z]{2}$";
        public const string ZipCodeInvalidV3 = "zipCode, you submitted ('zipCode') does not match regular expression ^[1-9][0-9]{3}(?!SA|SD|SS)[a-zA-Z]{2}$";
        public const string InvalidZipCodeV2 = "zip, you submitted ('zip') does not match regular expression [1-9][0-9]{3}\\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}";
        public const string IntFieldInvalid = "{0} cannot be less then 1 and should be not longer then";
        public const string AuthFailed = "Authorization has been denied for this request.";
        public const string ResellerIsInactive = "Reseller {0} is inactive";
        public const string ExBuildingWithoutExNotAllowed = "It is restricted to specify ExBuildingNr without BuildingNr";
        public const string FieldNameRequiredAndCantBeDefault = "{0} is required and can't have default value";
        public const string WrongIntFieldFormat = "The field {0} must be between 1 and 2147483647.";
        public const string WrongFieldRange2 = "The field {0} must be between 1 and 99999.";
        public const string WrongFieldRange = "The field {0} must be between 1 and 1,84467440737096E+19.";
        public const string WrongFieldRange3 = "The field {0} must be between 1 and 9.223372036854776E+18.";
        public const string PropertyNotFound = "Required property '{0}' not found in JSON.";
        public const string InvalidIcc = "Get Msisdn by ICC {0}, SIM not found. Please supply a valid ICC.";
        public const string IccAlreadyInUse = "Retail SIM already in use by Subscription ID";
        public const string WrongOrderIds = "There is no order for OrderID = {0}, ResellerID = {1}.";
        public const string InvalidPeaksForGasConnection = "Only peak consumption usage can be filled in for gas connection";
        public const string emptyPropositionsError = "The field {0} must be a string or array type with a minimum length of '1'.";
        public const string invalidProductType = "EnergyProductType has unknown value";
        public static readonly string resellerApiZipCodeError = $"The field ZipCode must match the regular expression '{_resellerApiZipCodeRegex}'.";
        public const string InvalidPhoneNumber = "{0} is not a valid phone number.";
        public const string InvalidIban2 = "Iban is not a valid IBAN.";
        public const string WrongWishStartDate = "Order start wish date isn't correct";
        public const string WrongVendorId = "Proposition with Id: {0} doesn't avaliable for sales office with Id: {1}";
        public const string PropositionTypeNotFound = "Can't find propositions with type {0}";
        public const string NoOfferBundleFound = "There are no offerBundles for salesChannel id = {0}";
        public const string WrongOBId = "There is no offer bundle with id = {0}";
        public const string EmptyProductTypesCollection = "ProductTypes collection must have at least one item.";
        public const string MandateMustBeProvided = "Mandate must be provided";
        public const string WrongProposition = "Proposition with id {0}is not available for current address";
        public const string NoBundlesSpecified = "You haven't specified any bundles";
        #endregion
        #region Voucher
        public const string VoucherIsNotFound = "Can't find voucher with Id {0}";
        public const string InvalidZipCodeRegex = @"ZipCode, you submitted does not match regular expression ^[1-9][0-9]{3}\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$";
        public const string EmptyProdManResponse = "Empty response from Prodman service for Id '{0}'";
        public const string CannotFindVoucherToDistribute = "Can't find voucher with Id {0} for distribution";
        public const string CannotFindVoucherToCancelReserve = "Can't find voucher with Id {0} for cancel of reservation";
        public const string VoucherReceived = "Customer with initials '{0}', Surname '{1}', Birth date {2} and Iban {3} has already received a voucher";
        #endregion
        #region Prospect
        public const string FieldLessThan2 = "The field {0} must be a string or array type with a minimum length of '2'.";
        public const string FieldOverMaxLength = "The field {0} must be a string with a maximum length of {1}.";
        public const string PrivateClientWithCompanyName = "Voor een particuliere klant mag geen bedrijfsnaam opgegeven worden.";
        public const string PrivateClientWithCompanyCocNr = "Voor een particuliere klant mag geen KvK-nummer opgegeven worden.";
        public const string BusinessClientWithoutCompanyName = "De bedrijfsnaam is verplicht voor zakelijke klanten.";
        public const string InvalidEmail = "The EmailAddress field is not a valid e-mail address.";
        public const string InvalidHomePhoneNr = "Het thuis telefoonnummer '{0}' is geen geldig telefoonnummer.";
        public const string InvalidMobilePhoneNr = "Het mobiel telefoonnummer '{0}' is geen geldig telefoonnummer.";
        public const string InitialsFieldOverMaxLength = "Initials length should be less 20 symbols, keep in mind that after the each symbol the Dot is added.";
        public const string ProspectInvalidIban = "'{0}' is geen geldige IBAN.";
        public const string WrongFormatInitials = "De voorletters '{0}' zijn ongeldig.";
        public const string WrongFormatPrefix = "Het tussenvoegsel '{0}' is ongeldig.";
        public const string WrongFormatSurname = "De achternaam '{0}' is ongeldig.";
        public const string WrongFormatCocNr = "Het KvK-nummer '{0}' is ongeldig.";
        public const string BirthdayLessThan18 = "De geboortedatum is ({0:yyyy-MM-dd}), maar de leeftijd is kleiner dan de minimumleeftijd van 18.";
        public const string BirthdayMoreThan125 = "De geboortedatum is ({0:yyyy-MM-dd}), maar de leeftijd is groter dan de maximumleeftijd van 125.";
        public const string InvalidZipCode = @"ZipCode, you submitted does not match regular expression ^[1-9][0-9]{3}\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$";
        public const string InvalidBuildingNr = "BuildingNr, you submitted cannot be casted to Integer type or is not between 1 and 99999";
        public const string ObjectWasNotFoundById = "{0} with ID : {1} was not found";
        public const string ObjectWasNotFoundByExRef = "{0} for offer with ExternalReference : {1} was not found";
        public const string OfferWasNotFoundByExFef = "Offer with external reference : {0} was not found";
        public const string IdIsNotBetweenValues = "{0} is not between 1 and 2147483647";
        public const string SearchWithoutRequiredFields = "Parameters Surname, ZipCode, PhoneNumberHome, PhoneNumberMobile, Email and CompanyName are empty. Need to specify at least one of them.";
        public const string SearchWithEmptyBuildNr = "BuildingNr can't be null if ZipCode and ExBuildingNr are set.";
        public const string RestrictedError = "It is restricted to specify ExBuildingNr and BuildingNr without ZipCode";
        public const string InvalidPhoneFormat = "{0} phone number has incorrect format.";
        public const string InvalidFieldLength = "{0} length can't be more than {1} characters";
        public const string InvalidProspectIdForOffer = "ProspectId can't be 0 when adding offer.";
        public const string PropositionIdIsRequired = "PropositionId is required and can't have default value";
        public const string AddOfferMpsIsEmpty = "Offer should containt information at least about 1 metering point.";
        public const string AddOfferEanInvalidRegex = "The field Ean must match the regular expression '^[0-9]{18}$'.";
        public const string TransitionUnavailable = "Transition from status {0} to status {1} for offer {2} unavailable";
        #endregion
        #region Icar
        public const string WrongFieldFormat = "The field {0} must match the regular expression '^[0-9]";
        public const string WrongFieldFormat2 = "{0}, you submitted ('{1}') does not match regular expression ^[0-9]";
        #endregion
        #region IcarServices
        public const string MarketPartyNotFound = "Market Party Info with Id: {0} does not exist in database.";
        public static string ZipCodeError = $"ZipCode, you submitted does not match regular expression {_zipCodeRegex}";
        public const string BuildingNumberIntError = "BuildingNr, you submitted cannot be casted to Integer type or is not between 1 and 99999";
        public const string NoMessageTypesFound = "No message types found for DossierId '22222222'.";
        public const string NoHistoryFound = "No history found for Ean '{0}' for the specified period.";
        public const string InvalidMarketPartyEan = "MarketPartyEan, you submitted ('value') does not match regular expression ^[0-9]{13}$";
        public const string MarketPartiesListIsEmpty = "List of MarketPartiesEans is empty";
        public const string InvalidEanIdExpression2 = "EanId, you submitted does not match regular expression ^[0-9]{18}$";
        public const string EanFieldRegexWrong = "The field Ean must match the regular expression '^[0-9]{18}$'.";
        public const string ZipCodeRegexWrong = "ZipCode, you submitted does not match regular expression ^[1-9][0-9]{3}\\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$";
        public const string BuildingNrRegexWrong = "BuildingNr, you submitted cannot be casted to Integer type or is not between 1 and 99999";
        public const string ExBuildingNrRegexWrong = "ExBuilding number length can't be more than 6 characters";
        public const string EansListIsEmpty = "The field Eans must contain at least one item";
        public const string EanRegularExpressionError = "Ean {0} must match the regular expression ";
        public const string MeteringPointNotFound = "Metering point with Ean '{0}' not found.";
        public const string NotFoundInDb = "EAN is not found in ICAR database";
        public const string DifferentDataArrived = "different data arrived for the same day";
        public const string DuplicateSkipped = "skipped as duplicate";
        public const string CcuUpdatedInTheFuture = "CC update in the future";
        public const string CcuUpdated = "CC updated";
        public const string ProcessedWithoutValidation = "Processed without validation";
        public const string DublicateCcu = "Duplicate Ccu";
        public const string NoMduFound = "No MDU history found";
        public const string NoMpFound = "No Metering points found";
        public const string WrongToDate = "FromDate can't be more then ToDate";
        #endregion
        #region GtwServices
        public const string EanWrong = "The field Ean must match the regular expression '^[0-9]{18}$'.";
        public const string BalanceSupplierWrong = "The field BalanceSupplier must match the regular expression ^[0-9]{13}$";
        public const string EdsnRejectionName = "EDSN Rejection";
        public const string BirthdayKeyWrong = "Month and day Customer's birthday is in the wrong format. It should be following '--mm-dd' , where mm is Month with values 01-12 and dd is Day with values 01-31.";
        public const string IbanWrong = "The field IBANKey must match the regular expression '^[0-9]{3}$'.";
        public const string IbanWrongNl = "Het veld IBANKey moet overeenkomen met de reguliere expressie ^[0-9]{3}$";
        public const string IbanIsNull = "Result from EDSN is null. Please make BackEnd team aware asap.";
        public const string ConsentIdWrong = "Birthday or Iban must be specified when ConsentId is used.";
        public const string BirthdayIbanEmpty = "Parameters BirthDayKey and IBANKey are empty. Need to specify at least one of them.";
        public const string EdsnRejectionText = "EDSN Rejection. RejectionCode: 202. Rejection text: EAN-code opvragende partij onbekend..";
        public const string EanRegexIsWrong = "EanId, you submitted does not match regular expression ^[0-9]{18}$";
        public const string EanIdFieldRegexIsWrong = "Ean, you submitted does not match regular expression ^[0-9]{18}$";
        public const string GridOperatorIdFieldRegexIsWrong = "The field GridOperatorId must match the regular expression ^[0-9]{13}$";
        public const string BalanceSupplierIdFieldRegexIsWrong = "The field BalanceSupplierId must match the regular expression ^[0-9]{13}$";
        public const string BalanceResponsibleIdFieldRegexIsWrong = "The field BalanceResponsibleId must match the regular expression ^[0-9]{13}$";
        public const string ZipCodeFieldRegexIsWrong = "ZipCode, you submitted does not match regular expression ^[1-9][0-9]{3}\\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$";
        public const string BuildingNrFieldIsWrong = "BuildingNr, you submitted cannot be cast to Integer type or is not between 1 and 99999";
        public const string SupplierFieldRegexIsWrong = "The field SupplierEan must match the regular expression ^[0-9]{13}$";
        public const string DateTimeRangeRequirement = "Value for MutationDate must be between 1/1/1900 12:00:00 AM and 12/12/9999 12:00:00 AM";
        public const string EanIsNotRecognizedByEdsn = "EAN-code aansluiting onbekend: Het verzoek betreft een onbekende aansluiting.";
        public const string WithoutAnyConnectionDetails = "ElectricityConnection and GasConnection are empty. Need to specify at least one of them.";
        public const string WrongBirthdayKeyRegex = "Month and day Customer's birthday is in the wrong format. It should be following '--mm-dd' , where mm is Month with values 01-12 and dd is Day with values 01-31.";
        public const string WrongIbanKeyRegex = "The field IBANKey must match the regular expression '^[0-9]{3}$'.";
        public const string WrongPostalCodeRegex = "PostalCode (zip code), you submitted does not match regular expression ^[1-9][0-9]{3}\\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$";
        public const string ErrorForFaceToFaceType = "HasOptedInByCheckbox and RequestOrigin have to be set to true for FaceToFace.";
        public const string AreRestrictedParams = "It is restricted to specify ZipCode, BuildingNr or ExBuildingNr if EanId is set";
        public const string NoRequiredParams = "Parameters ZipCode and EanId are empty. Need to specify at least one of them.";
        public const string GtwEdsnRejection = "EAN-code aansluiting onbekend: Het verzoek betreft een onbekende aansluiting.";
        public const string Edsn201Rejection = "EDSN Rejection. RejectionCode: 201. Rejection Text: EAN-code aansluiting onbekend: Het verzoek betreft een onbekende aansluiting.";
        public const string InvalidGender = "Please enter one of the allowable values for Gender: Unknown, Male, Female.";
        public const string BothIbanAndBirthDayKeyNotSpecified = "Parameters BirthDayKey and IBANKey are empty. Need to specify at least one of them.";
        #endregion
        #region SalesChannel
        public const string RegionAndStoreShouldBeEmpty = "Region and Store should be empty if Channel is not 'Retail'";
        public const string EditWithWrongStatus = "You must use EditNewSalesChannelModel to modify sales channel with status = New.";
        public const string EditFinancialWithWrongStatus = "You must use EditNewSalesChannelFinancialModel to modify sales channel with status = New.";
        public const string EditNewWithActiveStatus = "You must use EditSalesChannelModel to modify sales channel with status = Active.";
        public const string EditNewFinancialWithActiveStatus = "You must use EditSalesChannelFinancialModel to modify sales channel with status = Active.";
        public const string EditNewWithInactiveStatus = "You must use EditSalesChannelModel to modify sales channel with status = Inactive.";
        public const string EditNewFinancialWithInactiveStatus = "You must use EditSalesChannelFinancialModel to modify sales channel with status = Inactive.";
        public const string WrongDictionaryNameValue = "DictionaryName has unknown value";
        public const string WrongNewStatusFieldValue = "NewStatus has unknown value";
        public const string InvalidRangeOfSalesChannelId = "The field SalesChannelId must be between 1 and 2147483647.";
        public const string InvalidSalesChannelId = "Sales channel Id value must be present and above zero";
        public const string InvalidSalesChannelIdFinancial = "The value 'detailsFinancial' is not valid for SalesChannelId.";
        public const string RegionIsRequiredForRetail = "Region is required for 'Retail' channel";
        public const string SalesChannelNotFound = "Sales channel with ID = {0} doesn't exist.";
        #endregion
        #region ProdMan
        public const string BundleNameIsNotUnique = "Creëer een unieke naam voor de nieuwe offer bundel";
        public const string InvalidMaxBundleNameLength = "The field Name must be a string with a maximum length of 255.";
        public const string InvalidProposition2 = "All propositions in offer bundle should exist in Prodman database";
        public const string InvalidProposition3 = "All propositions in offer bundle should have same label and client type";
        public const string EmptyPropositionBundleList = "OfferBundle should have at least one PropositionBundle.";
        public const string EmptyOfferBundleId = "OfferBundle shouldn't have empty Id.";
        public const string CannotFindProposition = "Can't find proposition with id: {0}";
        public const string CannotEditOrDeleteProposition = "Can't edit/delete proposition with status {0}";
        public const string GetCashBackWithInvalidIdOrName = "Please specify PropositionName or PropositionId";
        public const string CannotChangeMarketToDate = "Can't change MarketTo to less from today value {0}";
        public const string CannotEditNotActiveProposition = "Can't edit proposition with status {0}";
        public const string PropositionIdsIsEmpty = "PropositionIds must have at least 1 item(s)";
        public const string CannotReactivatePropWithInvalidDate = "Can't reactivate proposition where expected MarketTo ({0}) is equal or earlier than today";
        public const string CannotReactivatePropWithInvalidStatus = "Can't reactivate proposition with status {0}";
        public const string CannotReactivatePropWhenDateLater = "Can't reactivate proposition where MarketTo ({0}) is later than today";
        public const string OfferBundlesArrayIsEmpty = "OfferBundleIds shouldn't be empty";
        public const string SpacesBetweenDates = "Please verify dates so there is no empty space between lowest From date and highest To date";
        public const string PreviouslyAddedPropositionShouldExist = "All previously added propositions should exist in updated offer bundle";
        public const string InvalidOfferBundleId = "Offer bundle with Id {0} doesn't exist in database";
        public const string InvalidPropositionDates = "'Valid from' can not be bigger than 'Valid To' for proposition bundle with PropositionId '{0}'";
        public const string WithCashBeckAndEligableAsRenewal = "Cashback upfront and directly after signup are not allowed for renewal";
        public const string TariffMustHaveCapacity = "Elektra tariffs should contain profile category";
        public const string ElkPriceMustHaveTariffs = "ElectricityPrice must contain elektra tariffs.";
        public const string GasPriceMustHaveTariffs = "GasPrice must contain gas tariffs.";
        public const string MarketFromDateWrong = "MarketFrom date should start from {0}";
        public const string StandingChargeWrong = "Standing charge per year excl vat should be more then 0 and less or equals 180";
        public const string GasRetailPriceWrong = "Retail price for gas should be more then 0.1 and less then 3.0";
        public const string ElkRetailPriceWrong = "Retail price for electricity should be more then 0.02 and less then 1.5";
        public const string ElkTariffTypeWrong = "If an elektra tariff has Low (or High) type, it must have High (or Low) type. It should have 1 pair tariffs.";
        public const string CodeLengthWrong = "The field Code must be a string with a minimum length of 6 and a maximum length of 70.";
        public const string NoEnergySources = "EnergySources must have at least 1 item(s)";
        public const string NoEnergySourcesForElkPrice = "The proposition with name {0} doesn't contain energy sources for ElectricityPrice";
        public const string NoEnergySourcesForGasPrice = "The proposition with name {0} doesn't contain energy sources for GasPrice";
        public const string NoClientType = "ClientType must have at least 1 item(s)";
        public const string PropositionAlreadyExist = "Can't save proposition: Such proposition name already exists: {0}";
        public const string PropositionHasNoPrices = "The proposition with name {0} must have at least 1 product price";
        public const string NotPossibleToEditPrice = "Can't edit product price with status {0}";
        public const string NotPossibleToChangePriceStatus = "Can't change product price status from {0} to {1} (Market from: {2})";
        public const string NotPossibleToChangePropositionStatus = "Can't change proposition status from {0} to {1} (valid from: {2})";
        public const string NotPossibleToAddPrice = "Can't add/edit ProductPrice when proposition is not in status {0} or {1}(Paused)";
        public const string PercentDiscountWrong = "Price discount can't be more then 50 %";
        public const string AbsoluteDiscountWrong = "Price discount can't be more then 0,05 €";
        #endregion
        #region BeApi
        public const string NoDataFound = "Data was not found";
        public const string InvalidZipcode = "The field zipCode must match the regular expression '^[1-9][0-9]{3}\\s?(?!SA|SD|SS|sa|sd|ss|Sa|Sd|Ss|sA|sD|sS)[a-zA-Z]{2}$'.";
        public const string EmptyReadingsValue = "Readings set cannot be empty";
        public const string InvalidRequest = "The request is invalid.";
        public const string WrongEanField = "eanId, you submitted ('{0}') does not match regular expression ^[0-9]";
        public const string WrongIdField = "The field {0} must be between 1 and 2147483647.";
        public const string AlreadyExistSurvey = "Survey for relationId {0} already exists";
        public const string DataNotFound = "Data was not found";
        public const string NoEansField = "The field Eans must contain at least one item";
        public const string publicError = "public API error";
        public const string InvalidEan = "Ean {0} must match the regular expression ^[0-9]";
        public const string NoContractFound = "There is no active renewable contract with a contract id {0}.";
        public const string OfferAlreadyAccepted = "Offer with a contract id {0} is already accepted.";
        public const string FieldRequiredPattern2 = "{0} is required and can't have default value";
        public const string NoProspectFound = "Prospect for offer with ExternalReference : {0} was not found";
        public const string WrongExtRef = "ExternalReference cannot be null or empty";
        public const string EmptyHash = "Hash cannot be null or empty";
        public const string ErrorOccured = "An error has occurred.";
        public const string AlreadyAcceptedOffer = "Offer with code {0} is already accepted";
        public const string NoInfoRegardingConn = "Cannot find any information about connections for contract with id: {0}";
        public const string NoContractsFound3 = "Did not find energy customer with id";
        public const string NoInfoById = "Cannot find any contract information by id: {0}";
        public const string WrongStartDate2 = "Start date can't be later than end date";
        #endregion
        #region MobileAppApi
        public const string ContactPersonDoNotHaveDevice = "The contact person with contact person number {0} does not have the app notification token {1}.";
        public const string InvalidSubscription = "Subscription doesn't exist.";
        public const string UnknownSub = "Unknown subscription.";
        public const string SubNotBelongToContactPerson = "Following subscription ids {0} do not belong to contact person id: {1}";
        public const string RelationIdNotBelongsToContactPerson = "Following relation ids {0} do not belong to contact person id: {1}";
        public const string ContractNotBelongsContactPerson = "Following contract id {0} do not belong to contact person id: {1}";
        public const string CustomerNrNotBelongsToContactPerson = "Following customer accounts ids {0} do not belong to contact person id: {1}";
        public const string CustomerNrsNotBelongsToContactPerson = "Following customer numbers [{0} ] do not belong to contact person id: {1}";
        public const string IbanCheckSumIncorrect = "- : IBAN checksum incorrect , sent value: '{0}'.";
        public const string ServiceAccountNotFound = "Service account with ID {0} was not found";
        public const string ServiceAccountIdNotBelongsContactPerson =
            "Service account with id - {0} doesn't match for customer account with id - {1}";
        public const string CanNotFindContract = "Cannot find any contract information by id: {0}";
        public const string NotExistedTransId =
            "De betaling kon niet worden gevonden. Controleer of er geld van je rekening is afgeschreven alvorens een nieuwe betaling te starten.";
        public const string WrongCurrentMonthlyAmount = "Current monthly amount in request {0} is not equal to the db value {1}.";
        public const string InvalidEnergyVoucherId =
            "Error making request with Error Code AccessDenied and Http Status Code Forbidden. No further error information was returned by the service.";
        public const string InvalidVoucherId = "Can't find voucher with Id {0}";
        public const string InvalidNicknameLength = "The field Nickname must be a string or array type with a maximum length of '15'.";
        public const string InvalidProductId =
            "Het gekozen product is op dit moment niet beschikbaar. Mogelijk heb je al Extra internet gekocht en is dit nog niet op.";
        #endregion
        #region Renewal
        public const string InvalidRenewableContractId = "Can't find renewable contract with id 0";
        public const string NotUniqueNameAndPriority = "Campaign must have unique name and priority";
        public const string WrongOfferName = "Creëer een unieke naam voor de nieuwe offer bundel";
        public const string RenewalInvalidLabel = "Can't create '{0}' campaign  with offer of the different label: {1}";
        public const string CannotFindEmailId = "Can't find email template with id {0}.";
        public const string EmptyEventsList = "Campaign should have at least one event";
        public const string MismatchedDates = "Event for proposition bundle {0} has start date {1} earlier than campaign start date {2}";
        public const string NotEligibleForBna = "Can't create campaign with contract which isn't eligible for Bna.";
        public const string CannotFindSalesChannelId = "Can't find sales channel with id {0}";
        public const string InvalidType = "Can't create '{0}' campaign  with contract of the different type: {1}.";
        public const string AssertionMessage = "Bna campaign with name [{0}] hasn't been found in RenewalDb";
        public const string WrongStartDate = "Campaign start date can not be less than Today";
        public const string WrongCampaignName = "Campaign name didnt pass validation, make sure it follows the format [4digits][letter][2digits][letter][2digits][_][CampaignType][_][Name]";
        public const string WrongTemplateLabel = "Template {0} has incorrect label code: {1}. Expected: {2}";
        public const string InactiveContract = "Can't create campaign with inactive contract.";
        public const string InvalidOfferId = "Invalid OfferId: must be greater than 0";
        public const string NoDateForEvent = "Event must have one property: EventDateTime or ContractEndDateOffsetInDays";
        public const string WrongEmailTemplateLabel = "Template {0} has incorrect label code: {1}. Expected: {2}";
        public const string InvalidRelationCategory = "Can't create '{0}' campaign  with offer of the different relation category: {1}";
        public const string NoOfferFound = "OfferId is required and can't have default value : 0";
        public const string NoEventFoundForRenewal = "No event found for renewal : {0} in campaign: {1} at {2}";
        public const string NoActiveProposition = "Can't find active proposition with id {0} from request for the {1}";
        public const string InactiveContractInOffer = "Can't create custom offer: contract {0} is not active";
        public const string WrongRelationType = "Can't create custom offer: Contract and CustomOffer have different relation types. Contract relation type: {0}. CustomOffer relation type: {1}";
        public const string WrongLabel = "Can't create custom offer: Contract and CustomOffer have different labels. Contract label: NLE. CustomOffer label: BudgetEnergie";
        public const string PausedCampaign = "Can't create a custom offer while campaigns are paused";
        public const string LabelCanNotBeChanged = "Can't update offer: label cannot be changed";
        public const string CategoryCanNotBeChanged = "Can't update offer: RelationType cannot be changed";
        public const string NoPropositionsForOffer = "OfferPropositions collection must have at least one item.";
        public const string OfferDatesConflict = "Offer Proposition dates conflict";
        public const string NoContractsFound = "No contracts found by given search criteria";
        public const string LeadShouldBePositive = "Lead source id should be a positive integer";
        #endregion
        #region RegisterSync
        public const string NoReportFound = "No report found by specified id{0}";
        public const string CreatedByIsRequired = "The CreatedBy field is required.";
        public const string CommentIsRequired = "The Comment field is required.";
        public const string ReportDataIsRequired = "The ReportData field is required.";
        public const string StatusIsRequired = "The Status field is required.";
        public const string AssignedToRequired = "The AssignedTo field is required.";
        public const string WrongMismatchId = "The field MismatchId must be between 1 and 2147483647.";
        public const string MismatchesDataIsRequired = "The MismatchesData field is required.";
        public const string MismatchNotFound = "No mismatch found by specified id: '{0}";
        #endregion
        #region OfferDifferentiation
        public const string ODAddressNotFound = "Address with zipCode = {0} was not found.";
        public const string ODInvalidZipCode = "ZipCode is not a valid Dutch postal code.";
        public const string ODInvalidBuildingNr = "The field BuildingNr must be between 1 and 2147483647.";
        public const string ODInvalidExBuildingNr = "The field ExBuildingNr must be a string or array type with a maximum length of '4'.";
        #endregion
        #region UsageCalculations
        public const string ContractInfoNotFound = "Cannot find any contract information by id: {0}";
        public const string InvalidContractId = "Geen contracten of te beleveren aansluitingen gevonden voor deze relatie.";
        public const string MandateHasRevoked = "Mandate is revoked for metering points";
        public const string NoContractFound2 = "Did not find energy customer with id {0}.";
        public const string ContractIdInvalid = "Contract id should be greater than 0";
        public const string EnterUsagePeriodRequired = "Please enter one of the allowable values for UsagePeriodType: Day, Week, Month.";
        public const string EnterPeriodRequired = "Please enter one of the allowable values for Period: Day, Week, Month.";
        #endregion
        #region P4Daily
        public const string EanNotUnique = "List of mandates should contain unique EANs.";
        public const string EanHasWrongEndDate = "EndDate should be today or later for EAN {0}.";
        public const string EanHasWrongStartDate = "StartDate should be less then EndDate for EAN {0}.";
        public const string P4MpListIsEmpty = "The field Eans must contain at least one item";
        public const string InvalidEnergyProductType = "Please enter one of the allowable values for EnergyProductTypeCode: ELK, GAS, Unknown.";
        public const string EanMustMatchRegex = "Ean {0} must match the regular expression ";
        public const string InvalidDateFrom = "DateFrom should be less then DateTo";
        #endregion

        #region P4Event
        public const string EansIsRequired = "The Eans field is required.";
        public const string SourceIsRequired = "The Source field is required.";
        public const string QueryDateIsOld = "QueryDate shouldn't be earlier than 39 days in the past";
        public const string RequestDateIsOld = "RequestDate shouldn't be earlier than today";
        public const string DuplicatedEans = "Duplicated Eans";
        // public const string EansListIsEmpty = "The field Eans must contain at least one item";
        #endregion

        public const string InvalidClientType = "Please enter one of the allowable values for ClientType: Business, Private.";
        public const string CreateObEmptyList = "{0} collection must have at least one item.";
        public const string InvalidPropType = "{0} options are not valid for enum NewCustomer, FineCompensation, Cashback, Renewal";
        public const string InvalidProdType = "Please enter one of the allowable values for CustomerProductType: Energy, Mobile, AllInOne.";
        public const string InvalidPropDateFrom = "Property ValidFrom contains an invalid value of (System.DateTime) type or default value.";
        public const string InvalidPropDuration = "OfferProposition : {0} has incorrect duration {1}";
        public const string PropDateFromLessToday = "'Valid from' can not be less than today for proposition bundle with PropositionId {0} ";
        public const string EmptyIdsList = "Ids collection must have at least one item.";
        public const string InvalidObLabel = "{0} options are not valid for enum BudgetEnergie, NLE";
        public const string InvalidClientTypeInList = "{0} options are not valid for enum Business, Private";
        public const string InvalidCustomProdType = "{0} options are not valid for enum Energy, Mobile, AllInOne";
        public const string InvalidObStatus = "Please enter one of the allowable values for Status: Active, Pending, Expired.";
        public const string OnRelationTypeUpdate = "Can't update offer: RelationType cannot be changed";

        #region Insurance
        public const string InvalidAge = "An age that is not between 18 and 125 years is not allowed.";
        public const string InvalidStartDate = "The start date cannot be before the end date";
        public const string InvalidProduct = "The product type ({0}) is invalid";
        public const string InvalidHomeNumber = "{0} is not a valid phone number.";
        public const string InvalidDutchNumber = "{0} is not a valid Dutch mobile phone number.";
        #endregion

        #region OptIn
        public const string NotUniqExternalReferenceProvided = "OptIn (ExternalReference {0}) already exists.";
        public const string NoLogsProvided = "At least one log (online or voice) must be provided.";
        public static readonly string InvalidPostalCode = "The field PostalCode must match the regular expression";
        public const string InvalidOptInSearchByExternalReference = "OptIn (Reference {0}) does not exist.";
        public const string InvalidOptInSearchById = "OptIn (Id {0}) does not exist.";
        public const string EmptyAudioType = "Please enter one of the allowable values for AudioType: MP3, M4A, FLAC, MP4, WAV, WMA, AAC.";
        public const string InvalidOptInToUpdate = "OptIn (Id {0}) does not exist.";
        public const string InvalidNutsHomeCustomerNumberUpdateToNull = "The field NutsHomeCustomerNumber must be between 1 and 2147483647.";
        public const string InvalidSearchCriteria = "At least one search criteria (SalesChannelId/NutsHomeCustomerNumber/ExternalReference) must be specified.";
        #endregion
    }
}