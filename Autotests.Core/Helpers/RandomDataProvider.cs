﻿using System;
using System.Linq;

namespace Autotests.Core.Helpers
{
    public static class RandomDataProvider
    {
        private static readonly Random _random = new Random();
        private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string Numbers = "123456789";

        public static string GetRandomNumCharString(int length)
        {
            return new string(Enumerable
                .Repeat(Chars + Numbers, length)
                .Select(s => s[_random.Next(s.Length)])
                .ToArray());
        }

        public static string GetRandomLettersString(int length)
        {
            return new string(Enumerable
                .Repeat(Chars, length)
                .Select(s => s[_random.Next(s.Length)])
                .ToArray());
        }

        public static string GetRandomNumbersString(int length)
        {
            return new string(Enumerable
                .Repeat(Numbers, length)
                .Select(s => s[_random.Next(s.Length)])
                .ToArray());
        }

        public static string GetRandomEmailString(int length)
        {
            var emailAddressWithoutDomain = new string(Enumerable
                .Repeat(Chars + Numbers, length)
                .Select(s => s[_random.Next(s.Length)])
                .ToArray());

            return emailAddressWithoutDomain + "-autotest@be-test.nl";
        }
    }
}
