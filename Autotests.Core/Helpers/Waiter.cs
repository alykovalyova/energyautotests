﻿using System;
using System.Threading;
using NUnit.Framework;

namespace Autotests.Core.Helpers
{
    public static class Waiter
    {
        private const string TimeoutError = "Timeout after '{0}' seconds.\n{1} has failed.";
        private const int MillisecondsTimeout = 1000;

        /// <summary>
        /// Waits till predicate returns true and throws a timeout error after time has over.
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="timeSpan"></param>
        /// <param name="errorMessage"></param>
        public static void Wait(Func<bool> predicate, int timeSpan = 120, string errorMessage = null)
        {
            var start = DateTime.Now;
            while (true)
            {
                Thread.Sleep(MillisecondsTimeout);
                if (predicate() && (DateTime.Now - start).TotalSeconds <= timeSpan)
                {
                    break;
                }
                if ((DateTime.Now - start).TotalSeconds >= timeSpan)
                {
                    throw errorMessage == null 
                        ? new AssertionException(string.Format(TimeoutError, timeSpan, nameof(predicate))) 
                        : new AssertionException(errorMessage);
                }
            }
        }

        /// <summary>
        /// Waits till predicate will return searching element and throw a timeout error if element's value is null after time has over.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <param name="timeSpan"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static T Wait<T>(Func<T> predicate, int timeSpan = 120, string errorMessage = null)
        {
            var start = DateTime.Now;
            while (true)
            {
                Thread.Sleep(MillisecondsTimeout);
                T res;
                try
                {
                    res = predicate();
                }
                catch (Exception)
                {
                    res = default(T);
                }

                if (res != null && (DateTime.Now - start).TotalSeconds <= timeSpan)
                {
                    return res;
                }
                if ((DateTime.Now - start).TotalSeconds >= timeSpan)
                {
                    throw errorMessage == null
                        ? new AssertionException(string.Format(TimeoutError, timeSpan, nameof(predicate)))
                        : new AssertionException(errorMessage);
                }
            }
        }
    }
}
