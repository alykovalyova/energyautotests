﻿namespace Autotests.Core.Sql.Entities
{
    public class AllInOneUser
    {
        public AllInOneUser(string customerAccountId, string serviceAccountId, string userName, int contactPersonNumber)
        {
            CustomerAccountId = customerAccountId;
            ServiceAccountId = serviceAccountId;
            UserName = userName;
            ContactPersonNumber = contactPersonNumber;
        }

        public string CustomerAccountId { get; }
        public string ServiceAccountId { get; }
        public string UserName { get; }
        public int ContactPersonNumber { get; }
    }
}