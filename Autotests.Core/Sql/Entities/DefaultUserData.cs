﻿using System.Data;

namespace Autotests.Core.Sql.Entities
{
    public class DefaultUserData : Data<DefaultUserData>, IData
    {
        public DefaultUserData(IDataRecord reader) : base(reader)
        {
        }

        public int ContactPersonNumber { get; set; }
        public string UserName { get; set; }
    }
}
