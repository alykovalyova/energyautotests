﻿using System.Data;

namespace Autotests.Core.Sql.Entities
{
    public class EanInfoData : Data<EanInfoData>, IData
    {
        public EanInfoData(IDataRecord reader) : base(reader)
        {
        }

        public int ContractId { get; set; }
        public string EanId { get; set; }
    }
}