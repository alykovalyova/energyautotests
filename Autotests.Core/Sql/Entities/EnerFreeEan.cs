﻿using System.Data;

namespace Autotests.Core.Sql.Entities
{
    public class EnerFreeEan : Data<EnerFreeEan>, IData
    {
        public EnerFreeEan(IDataRecord reader) : base(reader)
        {
        }

        public string EanId { get; set; }
        public int ProductType { get; set; }
    }
}
