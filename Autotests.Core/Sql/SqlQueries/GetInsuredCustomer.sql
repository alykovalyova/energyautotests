﻿SELECT c.RelationId, c.Discount, c.GrossPrice,c.NetPrice,c.StartDate,c.EndDate,cp.PolicyNumber,cp.IsActive,
cp.Price, ps.Description, cp.ProductChangeReasonId,  dpt.Description as ProductType
  FROM [Insurance].[dbo].[Customer] c
  join CustomerProduct cp on cp.CustomerId = c.Id
  join Dictionary_ProductType dpt on dpt.Id = cp.ProductTypeId
  join ProductSpecification ps on ps.Id = cp.ProductSpecificationId
   where c.RelationId = condition