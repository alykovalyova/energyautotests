﻿select top 1 cr.ContractId, cpk.ContactPersonNumber
from Contracten c
inner join ContractRegels cr on cr.ContractID = c.ContractID
inner join Eanboek eb on eb.EanID = cr.EanID
join [Customer].[dbo].[ProductCustomer] pc on pc.ProductCustomerId = c.ContractantID
join [Customer].[dbo].[CustomerContactPerson] ccp on ccp.NutsHomeCustomerId = pc.NutsHomeCustomerId
join [Customer].[dbo].[ContactPersonKey] cpk on cpk.Id = ccp.ContactPersonKeyId
inner join ICAR.dbo.MeteringPoint mp on eb.EanGebruiker collate SQL_Latin1_General_CP1_CI_AS = mp.EanId collate SQL_Latin1_General_CP1_CI_AS
where cr.Actief = 1 and mp.AdministrativeStatusSmartMeter = 1 