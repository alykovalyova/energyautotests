﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Autotests.Core.Handlers;
using Microsoft.Extensions.Configuration;

namespace Autotests.Core.TestDataConfiguration
{
    public sealed class ConfigurationService<T>
    {
        private static ConfigurationService<T> _instance;
        public ConfigurationService() => Root = InitializeConfiguration();
        public static ConfigurationService<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ConfigurationService<T>();
                }
                return _instance;
            }
        }
        public IConfigurationRoot Root { get; }
        public T GetDefaultValues(string section)
        {
           var result = ConfigurationService<T>.Instance.Root.GetSection(section).Get<T>();
            if (result == null)
            {
                throw new Exception($"No default variables for {section} are found.");
            }
            return result;
        }
       
        private IConfigurationRoot InitializeConfiguration()
        {
            var filesInExecutionDir = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            var settingsFile =
                filesInExecutionDir.FirstOrDefault(x => x.Contains(ConfigHandler.Instance.VariablesSettings.Path));
            var builder = new ConfigurationBuilder();
            if (settingsFile != null)
            {
                builder.AddJsonFile(settingsFile, optional: true, reloadOnChange: true);
            }
            return builder.Build();
        }
    }
}
