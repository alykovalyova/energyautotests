﻿namespace Autotests.Core.TestDataConfiguration
{
    public sealed class MobileAppApi
    {
        public int CompensationContractId { get; set; }
        public int UsageContractId { get; set; }
        public string MobileUserName { get; set; }
        public long MobileSubscriptionId { get; set; }
        public int VoucherContractId { get; set; }
        public int VoucherInfoContractId { get; set; }
        public int VoucherId { get; set; }
        public string ContractInvoiceId { get; set; }
        public string InvoiceId { get; set; }
        public string CustomerAccountId { get; set; }
        public string ServiceAccountId { get; set; }
    }
}
