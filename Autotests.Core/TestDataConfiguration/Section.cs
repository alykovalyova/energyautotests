﻿namespace Autotests.Core.TestDataConfiguration
{
    public enum Section
    {
        UsageCalculation,
        MobileAppApi,
        P4DailyReadings
    }
}
