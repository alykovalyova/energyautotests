﻿namespace Autotests.Core.TestDataConfiguration
{
    public sealed class UsageCalculation
    {
        public int ContractId { get; set; }
        public int ContractWithMandateAndNoTariffs{ get; set; }
        public int ContractWithoutTariffsAndMandate { get; set; }
    }
}
