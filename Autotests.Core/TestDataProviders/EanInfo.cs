﻿using System;
using System.Collections.Generic;
using Nuts.Consumption.Model.Contract;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Core.TestDataProviders
{
    public class EanInfo
    {
        public string EanId { get; set; }
        public bool AdministrativeStatusSmartMeter { get; set; }
        public string GridArea { get; set; }
        public string GridOperator { get; set; }
        public MarketSegmentCode MarketSegment { get; set; }
        public int RegisterNumber { get; set; }
        public GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register[] Registers { get; set; } 
        public string EnergyMeterId { get; set; }
        public EnergyAllocationMethodCode AllocationCode { get; set; }
        public EnergyProductTypeCode ProductType { get; set; }
        public int EacPeak { get; set; }
        public int EacOffPeak { get; set; }
        public int EapPeak { get; set; }
        public int EapOffPeak { get; set; }
        public PhysicalCapacityCode Capacity { get; set; }
        public EnergyUsageProfileCode? ProfileCategory { get; set; }
        public EnergyFlowDirectionCode? FlowDirection { get; set; }
        public EnergyMeterTypeCode EnergyTypeCode { get; set; }
        public EnergyConnectionPhysicalStatusCode? PhysicalStatus { get; set; }
        public string DossierId { get; set; }
        public MutationReasonCode? MutationReason { get; set; }
    }
}