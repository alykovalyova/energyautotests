﻿using System;
using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Core.TestDataProviders
{
    public static class EanInfoProvider
    {
        private static readonly EnergyUsageProfileCode[] _elkProfileCodes = new EnergyUsageProfileCode[]
        {
            EnergyUsageProfileCode.E1A, EnergyUsageProfileCode.E1B, EnergyUsageProfileCode.E1C,
            EnergyUsageProfileCode.E2A, EnergyUsageProfileCode.E2B, EnergyUsageProfileCode.E3A,
            EnergyUsageProfileCode.E3B, EnergyUsageProfileCode.E3C, EnergyUsageProfileCode.E3D
        };

        private static readonly EnergyUsageProfileCode[] _gasProfileCodes = new EnergyUsageProfileCode[]
        {
            EnergyUsageProfileCode.G1A, EnergyUsageProfileCode.G2A, EnergyUsageProfileCode.G2C
        };

        private static readonly EnergyMeterTypeCode[] _smartMeterTypeCodes = new EnergyMeterTypeCode[]
        {
            EnergyMeterTypeCode.DUN, EnergyMeterTypeCode.DUS
        };

        private static readonly PhysicalCapacityCode[] _elkPhysicalCapacityCodes = new PhysicalCapacityCode[]
        {
            PhysicalCapacityCode.E1x10, PhysicalCapacityCode.E1x25, PhysicalCapacityCode.E1x35,
            PhysicalCapacityCode.E1x50, PhysicalCapacityCode.E3x25, PhysicalCapacityCode.E3x50
        };

        private static readonly PhysicalCapacityCode[] _gasPhysicalCapacityCodes = new PhysicalCapacityCode[]
        {
            PhysicalCapacityCode.G10, PhysicalCapacityCode.G100, PhysicalCapacityCode.G16,
            PhysicalCapacityCode.G25, PhysicalCapacityCode.G4
        };

        public static EanInfo GetRandomElkEan(bool isSmartMeter, int registerNr = 2,
            MutationReasonCode mutationReason = MutationReasonCode.MOVEIN,
            EnergyConnectionPhysicalStatusCode physicalStatusCode = EnergyConnectionPhysicalStatusCode.IBD,
            EnergyAllocationMethodCode allocationMethod = EnergyAllocationMethodCode.SMA,
            EnergyUsageProfileCode? profileCode = EnergyUsageProfileCode.E1B, MarketSegmentCode marketSegment = MarketSegmentCode.KVB,
            string gridArea = "9782434235455", string gridOperator = "2438439739743")
        {
            return new EanInfo()
            {
                AdministrativeStatusSmartMeter = isSmartMeter,
                AllocationCode = allocationMethod,
                PhysicalStatus = physicalStatusCode,
                Capacity = (PhysicalCapacityCode) _elkPhysicalCapacityCodes.GetValue(
                    new Random().Next(_elkPhysicalCapacityCodes.Length)),
                FlowDirection = registerNr == 4 ? EnergyFlowDirectionCode.TLV : EnergyFlowDirectionCode.LVR,
                EnergyTypeCode = isSmartMeter
                    ? (EnergyMeterTypeCode) _smartMeterTypeCodes.GetValue(
                        new Random().Next(_smartMeterTypeCodes.Length))
                    : EnergyMeterTypeCode.CVN,
                DossierId = RandomDataProvider.GetRandomNumbersString(8),
                EacOffPeak = new Random().Next(100, 200),
                EacPeak = new Random().Next(200, 500),
                EanId = $"000911{RandomDataProvider.GetRandomNumbersString(12)}",
                ProductType = EnergyProductTypeCode.ELK,
                ProfileCategory = profileCode != null
                    ? profileCode.GetValueOrDefault() : (EnergyUsageProfileCode)_elkProfileCodes.GetValue(new Random().Next(_elkProfileCodes.Length)),
                RegisterNumber = registerNr,
                MutationReason = mutationReason,
                EnergyMeterId = $"TEST{RandomDataProvider.GetRandomNumbersString(6)}",
                MarketSegment = marketSegment,
                GridOperator = gridOperator,
                GridArea = gridArea
            };
        }

        public static EanInfo GetRandomGasEan(bool isSmartMeter,
            MutationReasonCode mutationReason = MutationReasonCode.MOVEIN,
            EnergyConnectionPhysicalStatusCode physicalStatusCode = EnergyConnectionPhysicalStatusCode.IBD,
            EnergyAllocationMethodCode allocationMethod = EnergyAllocationMethodCode.SMA,
            EnergyUsageProfileCode? profileCode = EnergyUsageProfileCode.G1A, MarketSegmentCode marketSegment = MarketSegmentCode.KVB,
            string gridArea = "9782434235455", string gridOperator = "2438439739743")
        {
            return new EanInfo()
            {
                AdministrativeStatusSmartMeter = isSmartMeter,
                AllocationCode = allocationMethod,
                PhysicalStatus = physicalStatusCode,
                Capacity = (PhysicalCapacityCode) _gasPhysicalCapacityCodes.GetValue(
                    new Random().Next(_gasPhysicalCapacityCodes.Length)),
                FlowDirection = EnergyFlowDirectionCode.LVR,
                DossierId = RandomDataProvider.GetRandomNumbersString(8),
                EacOffPeak = new Random().Next(100, 200),
                EacPeak = new Random().Next(200, 500),
                EanId = $"000911{RandomDataProvider.GetRandomNumbersString(12)}",
                ProductType = EnergyProductTypeCode.GAS,
                ProfileCategory = profileCode != null
                    ? profileCode : (EnergyUsageProfileCode)_gasProfileCodes.GetValue(new Random().Next(_gasProfileCodes.Length)),
                RegisterNumber = 1,
                MutationReason = mutationReason,
                EnergyTypeCode = isSmartMeter
                    ? (EnergyMeterTypeCode) _smartMeterTypeCodes.GetValue(
                        new Random().Next(_smartMeterTypeCodes.Length))
                    : EnergyMeterTypeCode.CVN,
                EnergyMeterId = $"TEST{RandomDataProvider.GetRandomNumbersString(6)}",
                MarketSegment = marketSegment,
                GridArea = gridArea,
                GridOperator = gridOperator
            };
        }
    }
}