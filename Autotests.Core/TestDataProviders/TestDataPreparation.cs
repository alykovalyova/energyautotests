﻿using System.Collections.Generic;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Core.TestDataProviders
{
    public static class TestDataPreparation
    {
        public enum MeterType
        {
            ElkEanTfSignalSmartMeter,
            ElkEanTfSignalOneRegister,
            ElkEanTfSignalInvalidProfileCode,
            ElkEanTfSignalValid2Reg,
            ElkEanTfSignalValid4Reg
        }

        private static readonly EanInfo _elkEanTfSignalOneRegister = EanInfoProvider.GetRandomElkEan(false, 1, profileCode: EnergyUsageProfileCode.E1A);
        private static readonly EanInfo _elkEanTfSignalSmartMeter = EanInfoProvider.GetRandomElkEan(true, 2, profileCode: EnergyUsageProfileCode.E1A);
        private static readonly EanInfo _elkEanTfSignalInvalidProfileCode = EanInfoProvider.GetRandomElkEan(false, 2, profileCode: EnergyUsageProfileCode.E4A);
        private static readonly EanInfo _elkEanTfSignalValid2Reg = EanInfoProvider.GetRandomElkEan(false, 2, profileCode: EnergyUsageProfileCode.E1A);
        private static readonly EanInfo _elkEanTfSignalValid4Reg = EanInfoProvider.GetRandomElkEan(false, 4, profileCode: EnergyUsageProfileCode.E1A);

        private static readonly Dictionary<MeterType, EanInfo> _eanInfoBatchForTestCases =
           new Dictionary<MeterType, EanInfo>()
           {
                {MeterType.ElkEanTfSignalOneRegister, _elkEanTfSignalOneRegister},
                {MeterType.ElkEanTfSignalSmartMeter, _elkEanTfSignalSmartMeter },
                {MeterType.ElkEanTfSignalInvalidProfileCode, _elkEanTfSignalInvalidProfileCode },
                {MeterType.ElkEanTfSignalValid2Reg, _elkEanTfSignalValid2Reg },
                {MeterType.ElkEanTfSignalValid4Reg, _elkEanTfSignalValid4Reg }
           };

        public static Dictionary<MeterType, EanInfo> TestEans => _eanInfoBatchForTestCases;
    }
}
