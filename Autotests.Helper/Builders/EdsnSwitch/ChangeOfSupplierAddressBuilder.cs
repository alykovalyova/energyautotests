﻿using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class ChangeOfSupplierAddressBuilder : MainBuilder<Address>
    {
        internal ChangeOfSupplierAddressBuilder SetDefaultAddress()
        {
            Instance.CityName = "Kharkiv";
            Instance.Country = "Nl";
            Instance.StreetName = "Freedom";
            Instance.ExBuildingNr = "A";
            return this;
        }

        internal ChangeOfSupplierAddressBuilder SetBuildingNr(int buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal ChangeOfSupplierAddressBuilder SetCityName(string cityName)
        {
            Instance.CityName = cityName;
            return this;
        }

        internal ChangeOfSupplierAddressBuilder SetCountry(string country)
        {
            Instance.Country = country;
            return this;
        }

        internal ChangeOfSupplierAddressBuilder SetExBuildingNrd(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }

        internal ChangeOfSupplierAddressBuilder SetZipcode(string zipcode)
        {
            Instance.ZipCode = zipcode;
            return this;
        }

        internal ChangeOfSupplierAddressBuilder SetStreetName(string streetName)
        {
            Instance.StreetName = streetName;
            return this;
        }
    }
}
