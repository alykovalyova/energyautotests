﻿using System;
using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class ChangeOfSupplierDataBuilder : MainBuilder<ChangeOfSupplierData>
    {
        internal ChangeOfSupplierDataBuilder SetEan(string ean)
        {
            Instance.Ean = ean;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetBalanceResponsibleId(string balanceResponsibleId)
        {
            Instance.BalanceResponsibleId = balanceResponsibleId;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetBalanceSupplierId(string balanceSupplierId)
        {
            Instance.BalanceSupplierId = balanceSupplierId;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetGridOperatorId(string gridOperatorId)
        {
            Instance.GridOperatorId = gridOperatorId;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetReferenceId(string referenceId)
        {
            Instance.ReferenceId = referenceId;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetGridContractParty(ChangeOfSupplierContract gridContractParty)
        {
            Instance.GridContractParty = gridContractParty;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetAddress(Address address)
        {
            Instance.Address = address;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetKvkNumber(string kvkNumber)
        {
            Instance.KvkNumber = kvkNumber;
            return this;
        }

        internal ChangeOfSupplierDataBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }
    }
}
