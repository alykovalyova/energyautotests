﻿using System;
using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class ChangeOfSupplierGridContractPartBuilder : MainBuilder<ChangeOfSupplierContract>
    {
        internal ChangeOfSupplierGridContractPartBuilder SetDefaultGridContractParty()
        {
            Instance.BirthDate = DateTime.Today;
            Instance.Initials = "T.D.";
            Instance.Surname = "Taras";
            Instance.SurnamePrefix = null;
            return this;
        }
        internal ChangeOfSupplierGridContractPartBuilder SetBirthDate(DateTime? birthDate)
        {
            Instance.BirthDate = birthDate;
            return this;
        }
        internal ChangeOfSupplierGridContractPartBuilder SetInitials(string initials)
        {
            Instance.Initials = initials;
            return this;
        }
        internal ChangeOfSupplierGridContractPartBuilder SetSurname(string surname)
        {
            Instance.Surname = surname;
            return this;
        }
        internal ChangeOfSupplierGridContractPartBuilder SetSurnamePrefix(string surnamePrefix)
        {
            Instance.SurnamePrefix = surnamePrefix;
            return this;
        }
    }
}
