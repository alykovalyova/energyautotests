﻿using System;
using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class EndOfSupplyDataBuilder : MainBuilder<EndOfSupplyData>
    {
        internal EndOfSupplyDataBuilder SetBalanceSupplierId(string balanceSupplierId)
        {
            Instance.BalanceSupplierId = balanceSupplierId;
            return this;
        }

        internal EndOfSupplyDataBuilder SetGridOperatorId(string gridOperatorId)
        {
            Instance.GridOperatorId = gridOperatorId;
            return this;
        }

        internal EndOfSupplyDataBuilder SetEan(string ean)
        {
            Instance.Ean = ean;
            return this;
        }

        internal EndOfSupplyDataBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }
        internal EndOfSupplyDataBuilder SetReferenceId(string referenceId)
        {
            Instance.ReferenceId = referenceId;
            return this;
        }
    }
}
