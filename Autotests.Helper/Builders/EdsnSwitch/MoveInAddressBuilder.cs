﻿using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class MoveInAddressBuilder : MainBuilder<Address>
    {
        internal MoveInAddressBuilder SetDefaultAddress()
        {
            Instance.CityName = "Kharkiv";
            Instance.Country = "Nl";
            Instance.StreetName = "Freedom";
            Instance.ExBuildingNr = "A";
            return this;
        }

        internal MoveInAddressBuilder SetBuildingNr(int buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal MoveInAddressBuilder SetCityName(string cityName)
        {
            Instance.CityName = cityName;
            return this;
        }

        internal MoveInAddressBuilder SetCountry(string country)
        {
            Instance.Country = country;
            return this;
        }

        internal MoveInAddressBuilder SetExBuildingNr(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }

        internal MoveInAddressBuilder SetZipcode(string zipcode)
        {
            Instance.ZipCode = zipcode;
            return this;
        }

        internal MoveInAddressBuilder SetStreetName(string streetName)
        {
            Instance.StreetName = streetName;
            return this;
        }
    }
}
