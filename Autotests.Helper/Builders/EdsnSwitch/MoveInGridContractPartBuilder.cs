﻿using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class MoveInGridContractPartBuilder : MainBuilder<MoveInContract>
    {
        internal MoveInGridContractPartBuilder SetDefaultGridContractParty()
        {
            Instance.Initials = "T.D.";
            Instance.Surname = "Taras";
            Instance.SurnamePrefix = null;
            return this;
        }

        internal MoveInGridContractPartBuilder SetInitials(string initials)
        {
            Instance.Initials = initials;
            return this;
        }

        internal MoveInGridContractPartBuilder SetSurname(string surname)
        {
            Instance.Surname = surname;
            return this;
        }

        internal MoveInGridContractPartBuilder SetSurnamePrefix(string surnamePrefix)
        {
            Instance.SurnamePrefix = surnamePrefix;
            return this;
        }
    }
}
