﻿using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;

namespace Autotests.Helper.Builders.EdsnSwitch
{
    internal class MoveOutDataBuilder : MainBuilder<MoveOutData>
    {
        internal MoveOutDataBuilder SetBalanceSupplierId(string balanceSupplierId)
        {
            Instance.BalanceSupplierId = balanceSupplierId;
            return this;
        }

        internal MoveOutDataBuilder SetGridOperatorId(string gridOperatorId)
        {
            Instance.GridOperatorId = gridOperatorId;
            return this;
        }

        internal MoveOutDataBuilder SetEan(string ean)
        {
            Instance.Ean = ean;
            return this;
        }

        internal MoveOutDataBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }
        internal MoveOutDataBuilder SetReferenceId(string referenceId)
        {
            Instance.ReferenceId = referenceId;
            return this;
        }
    }
}
