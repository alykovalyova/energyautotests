﻿using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;

namespace Autotests.Helper.Builders.FakeEdsn.CommercialCharateristics
{
    public class CommercialCharacteristicsBuilder : MainBuilder<MasterDataUpdateResponseEnvelope_PC_PMP_MPCC>
    {
        public CommercialCharacteristicsBuilder SetBase()
        {
            Instance.BalanceResponsibleParty_Company =
                new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC_BalanceResponsibleParty_Company()
                    {ID = TestConstants.BalanceResponsible};
            Instance.MeteringResponsibleParty_Company =
                new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC_MeteringResponsibleParty_Company()
                    {ID = TestConstants.BalanceResponsible};
            return this;
        }

        public CommercialCharacteristicsBuilder SetBalanceSupplier(string balanceSupplierId)
        {
            Instance.BalanceSupplier_Company =
                new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC_BalanceSupplier_Company()
                    {ID = balanceSupplierId};
            return this;
        }
    }
}