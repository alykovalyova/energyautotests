﻿using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using Nuts.FakeEdsn.Contracts;

namespace Autotests.Helper.Builders.FakeEdsn
{
    public class ExternalLossBuilder : MainBuilder<LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint>
    {
        public ExternalLossBuilder SetEanId(string eanId)
        {
            Instance.EANID = eanId;
            return this;
        }

        public ExternalLossBuilder SetGridOperator(string gridOperatorId)
        {
            Instance.GridOperator_Company =
                new LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_GridOperator_Company()
                {
                    ID = gridOperatorId
                };
            return this;
        }

        public ExternalLossBuilder SetProductType()
        {
            Instance.ProductType = LossResultResponseEnvelope_EnergyProductPortaalTypeCode.GAS;
            return this;
        }

        public ExternalLossBuilder SetCommercialCharacteristics(string balanceSupplier, string oldBalanceSupplier)
        {
            Instance.MPCommercialCharacteristics =
                new LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_MPCommercialCharacteristics()
                {
                    BalanceResponsibleParty_Company =
                        new
                            LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_MPCommercialCharacteristics_BalanceResponsibleParty_Company()
                        {
                            ID = TestConstants.BalanceResponsible
                        },
                    BalanceSupplier_Company =
                        new
                            LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_MPCommercialCharacteristics_BalanceSupplier_Company()
                        {
                            ID = balanceSupplier
                        },
                    MeteringResponsibleParty_Company =
                        new
                            LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_MPCommercialCharacteristics_MeteringResponsibleParty_Company()
                        {
                            ID = TestConstants.BalanceResponsible
                        },
                    OldBalanceSupplier_Company =
                        new
                            LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_MPCommercialCharacteristics_OldBalanceSupplier_Company()
                        {
                            ID = oldBalanceSupplier
                        }
                };
            return this;
        }

        public ExternalLossBuilder SetMutationData(DateTime mutationDate)
        {
            Instance.Portaal_Mutation =
                new LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_Mutation()
                {
                    Dossier =
                        new LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_Mutation_Dossier()
                        {
                            ID = TestConstants.DossierId
                        },
                    ExternalReference = TestConstants.ExternalReference,
                    MutationReason = LossResultResponseEnvelope_MutationReasonPortaalCode.SWITCHLV,
                    MutationDate = mutationDate
                };
            return this;
        }
    }
}
