﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.MasterData
{
    public class MasterDataPortalMutationBuilder : MainBuilder<MasterDataUpdateResponseEnvelope_PC_PMP_PM>
    {
        public MasterDataPortalMutationBuilder SetDossier(string dossierId)
        {
            Instance.Dossier = new MasterDataUpdateResponseEnvelope_PC_PMP_PM_Dossier()
                {ID = dossierId};
            return this;
        }

        public MasterDataPortalMutationBuilder SetExternalReference(string extRef)
        {
            Instance.ExternalReference = extRef;
            return this;
        }

        public MasterDataPortalMutationBuilder SetValidFromDate(DateTime date)
        {
            Instance.MutationDate = date;
            return this;
        }

        public MasterDataPortalMutationBuilder SetMutationReason(string mutationReason)
        {
            Instance.MutationReason = mutationReason;
            return this;
        }
    }
}