﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.MasterData
{
    public class MasterDataUpdateBuilder : MainBuilder<MasterDataUpdateResponseEnvelope_PC_PMP>
    {
        public MasterDataUpdateBuilder SetEanId(string eanId)
        {
            Instance.EANID = eanId;
            return this;
        }

        public MasterDataUpdateBuilder SetMarketSegment(string marketSegment)
        {
            Instance.MarketSegment = marketSegment;
            return this;
        }

        public MasterDataUpdateBuilder SetGridoperator(
            MasterDataUpdateResponseEnvelope_PC_PMP_GridOperator_Company gridOperator)
        {
            Instance.GridOperator_Company = gridOperator;
            return this;
        }

        public MasterDataUpdateBuilder SetGridarea(string gridArea)
        {
            Instance.GridArea = gridArea;
            return this;
        }

        public MasterDataUpdateBuilder SetAddress(MasterDataUpdateResponseEnvelope_MPAddressRequestType address)
        {
            Instance.EDSN_AddressSearch = address;
            return this;
        }

        public MasterDataUpdateBuilder SetEnergyMeter(
            MasterDataUpdateResponseEnvelope_PC_PMP_Portaal_EnergyMeter energyMeter)
        {
            Instance.Portaal_EnergyMeter = energyMeter;
            return this;
        }

        public MasterDataUpdateBuilder SetProductType(string productType)
        {
            Instance.ProductType = productType;
            return this;
        }

        public MasterDataUpdateBuilder SetPhysicalCharacteristics(
            MasterDataUpdateResponseEnvelope_PC_PMP_MPPhysicalCharacteristics physicalCharacteristics)
        {
            Instance.MPPhysicalCharacteristics = physicalCharacteristics;
            return this;
        }

        public MasterDataUpdateBuilder SetCommercialCharacteristics(
            MasterDataUpdateResponseEnvelope_PC_PMP_MPCC commercialCharacteristics)
        {
            Instance.MPCommercialCharacteristics = commercialCharacteristics;
            return this;
        }

        public MasterDataUpdateBuilder SetPortalMutation(MasterDataUpdateResponseEnvelope_PC_PMP_PM[] portalMutation)
        {
            Instance.Portaal_Mutation = portalMutation;
            return this;
        }

        public MasterDataUpdateBuilder SetMeteringPointGroup(
            MasterDataUpdateResponseEnvelope_PC_PMP_MeteringPointGroup meteringPointGroup)
        {
            Instance.MeteringPointGroup = meteringPointGroup;
            return this;
        }
    }
}