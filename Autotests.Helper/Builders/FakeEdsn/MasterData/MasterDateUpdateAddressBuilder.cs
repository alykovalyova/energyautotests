﻿using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;

namespace Autotests.Helper.Builders.FakeEdsn.MasterData
{
    public class MasterDateUpdateAddressBuilder : MainBuilder<MasterDataUpdateResponseEnvelope_MPAddressRequestType>
    {
        public MasterDateUpdateAddressBuilder SetBase()
        {
            Instance.Country = TestConstants.Country;
            Instance.ExBuildingNr = TestConstants.ExBuildingNr;
            Instance.StreetName = TestConstants.Street;
            return this;
        }

        public MasterDateUpdateAddressBuilder SetBuildingNr(string buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        public MasterDateUpdateAddressBuilder SetBag(MasterDataUpdateResponseEnvelope_BAGType bag)
        {
            Instance.BAG = bag;
            return this;
        }

        public MasterDateUpdateAddressBuilder SetZipcode(string zipcode)
        {
            Instance.ZIPCode = zipcode;
            return this;
        }

        public MasterDateUpdateAddressBuilder SetCity(string city)
        {
            Instance.CityName = city;
            return this;
        }
    }
}