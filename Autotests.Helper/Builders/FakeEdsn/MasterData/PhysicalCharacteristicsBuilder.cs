﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.MasterData
{
    public class PhysicalCharacteristicsBuilder : MainBuilder<GetMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics>
    {
        public PhysicalCharacteristicsBuilder SetEnergyFlowDirection(string energyFlowDirection)
        {
            Instance.EnergyFlowDirection = energyFlowDirection;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetCaptarCode(string captarcode)
        {
            Instance.CapTarCode = captarcode;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetEacPeaks(string peak)
        {
            Instance.EAEnergyConsumptionNettedPeak = peak;
            Instance.EAEnergyConsumptionNettedOffPeak = peak;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetEapPeaks(string peak)
        {
            Instance.EAEnergyProductionNettedPeak = peak;
            Instance.EAEnergyProductionNettedOffPeak = peak;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetProfileCategory(string profileCategory)
        {
            Instance.ProfileCategory = profileCategory;
            return this;
        }
    }
}
