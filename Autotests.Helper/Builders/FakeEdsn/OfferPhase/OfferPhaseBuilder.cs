﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.OfferPhase
{
    public class OfferPhaseBuilder : MainBuilder<GetMPInformationResponseEnvelope_PC_PMP>
    {
        public OfferPhaseBuilder SetEanId(string eanId)
        {
            Instance.EANID = eanId;
            return this;
        }

        public OfferPhaseBuilder SetGridOperator(GetMPInformationResponseEnvelope_PC_PMP_GridOperator_Company gridOperator)
        {
            Instance.GridOperator_Company = gridOperator;
            return this;
        }

        public OfferPhaseBuilder SetGridArea(string gridArea)
        {
            Instance.GridArea = gridArea;
            return this;
        }

        public OfferPhaseBuilder SetMeteringPointGroup(GetMPInformationResponseEnvelope_PC_PMP_MeteringPointGroup meteringPointGroup)
        {
            Instance.MeteringPointGroup = meteringPointGroup;
            return this;
        }

        public OfferPhaseBuilder SetAdminStatusSmartMeter(bool isSmartMeter)
        {
            Instance.AdministrativeStatusSmartMeter = isSmartMeter.ToString();
            return this;
        }

        public OfferPhaseBuilder SetMarketSegment(string marketSegment)
        {
            Instance.MarketSegment = marketSegment;
            return this;
        }

        public OfferPhaseBuilder SetAddress(params GetMPInformationResponseEnvelope_MPAddressResponseType[] address)
        {
            Instance.EDSN_AddressSearch = address;
            return this;
        }

        public OfferPhaseBuilder SetProductType(string productType)
        {
            Instance.ProductType = productType;
            return this;
        }

        public OfferPhaseBuilder SetEnergyMeter(GetMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter meter)
        {
            Instance.Portaal_EnergyMeter = meter;
            return this;
        }

        public OfferPhaseBuilder SetPhysicalCharacteristics(GetMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics physicalCharacteristics)
        {
            Instance.MPPhysicalCharacteristics = physicalCharacteristics;
            return this;
        }

        public OfferPhaseBuilder SetPortalMutation(GetMPInformationResponseEnvelope_PC_PMP_Portaal_Mutation mutationData)
        {
            Instance.Portaal_Mutation = mutationData;
            return this;
        }

        public OfferPhaseBuilder SetValidFromDate(DateTime validFrom)
        {
            Instance.ValidFromDate = validFrom;
            return this;
        }
    }
}