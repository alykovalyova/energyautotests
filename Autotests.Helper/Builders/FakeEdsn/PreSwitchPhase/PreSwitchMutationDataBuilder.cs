﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.PreSwitchPhase
{
    public class PreSwitchMutationDataBuilder : MainBuilder<GetSCMPInformationResponseEnvelope_PC_PMP_PM>
    {
        public PreSwitchMutationDataBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }

        public PreSwitchMutationDataBuilder SetDossier(GetSCMPInformationResponseEnvelope_PC_PMP_PM_Dossier dossier)
        {
            Instance.Dossier = dossier;
            return this;
        }

        public PreSwitchMutationDataBuilder SetExternalReference(string externalReference)
        {
            Instance.ExternalReference = externalReference;
            return this;
        }
    }
}