﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.PreSwitchPhase
{
    public class PreSwitchPhaseBuilder : MainBuilder<GetSCMPInformationResponseEnvelope_PC_PMP>
    {
        public PreSwitchPhaseBuilder SetEanId(string eanId)
        {
            Instance.EANID = eanId;
            return this;
        }

        public PreSwitchPhaseBuilder SetGridOperator(GetSCMPInformationResponseEnvelope_PC_PMP_GridOperator_Company gridOperator)
        {
            Instance.GridOperator_Company = gridOperator;
            return this;
        }

        public PreSwitchPhaseBuilder SetGridArea(string gridArea)
        {
            Instance.GridArea = gridArea;
            return this;
        }

        public PreSwitchPhaseBuilder SetMarketSegment(string marketSegment)
        {
            Instance.MarketSegment = marketSegment;
            return this;
        }

        public PreSwitchPhaseBuilder SetAddress(GetSCMPInformationResponseEnvelope_MPAddressResponseType address)
        {
            Instance.EDSN_AddressSearch = address;
            return this;
        }

        public PreSwitchPhaseBuilder SetProductType(string productType)
        {
            Instance.ProductType = productType;
            return this;
        }

        public PreSwitchPhaseBuilder SetEnergyMeter(GetSCMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter meter)
        {
            Instance.Portaal_EnergyMeter = meter;
            return this;
        }

        public PreSwitchPhaseBuilder SetPhysicalCharacteristics(GetSCMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics physicalCharacteristics)
        {
            Instance.MPPhysicalCharacteristics = physicalCharacteristics;
            return this;
        }

        public PreSwitchPhaseBuilder SetPortalMutation(GetSCMPInformationResponseEnvelope_PC_PMP_PM mutationData)
        {
            Instance.Portaal_Mutation = mutationData;
            return this;
        }
    }
}