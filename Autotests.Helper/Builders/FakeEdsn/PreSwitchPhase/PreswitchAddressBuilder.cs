﻿using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;

namespace Autotests.Helper.Builders.FakeEdsn.PreSwitchPhase
{
    public class PreswitchAddressBuilder : MainBuilder<GetSCMPInformationResponseEnvelope_MPAddressResponseType>
    {
        public PreswitchAddressBuilder SetBuildingNr(string buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        public PreswitchAddressBuilder SetStreet(string street)
        {
            Instance.StreetName = street;
            return this;
        }

        public PreswitchAddressBuilder SetCity(string city)
        {
            Instance.CityName = city;
            return this;
        }

        public PreswitchAddressBuilder SetZipcode(string zipcode)
        {
            Instance.ZIPCode = zipcode;
            return this;
        }

        public PreswitchAddressBuilder SetExbuildingNr(string exbuildingNr)
        {
            Instance.ExBuildingNr = exbuildingNr;
            return this;
        }

        public PreswitchAddressBuilder SetBagAddress()
        {
            Instance.BAG = new GetSCMPInformationResponseEnvelope_BAGType()
            {
                BAGBuildingID = TestConstants.BagBuildingId,
                BAGID = TestConstants.BagId
            };
            return this;
        }
    }
}
