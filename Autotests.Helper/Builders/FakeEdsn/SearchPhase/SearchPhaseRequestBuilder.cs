﻿using Autotests.Core.Helpers;
using Nuts.FakeEdsn.Contracts;

namespace Autotests.Helper.Builders.FakeEdsn.SearchPhase
{
    public class SearchPhaseRequestBuilder : MainBuilder<LocalSearchMeteringPointsRequest>
    {
        public SearchPhaseRequestBuilder SetBuildingNr(string buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        public SearchPhaseRequestBuilder SetEanId(string eanId)
        {
            Instance.EanId = eanId;
            return this;
        }

        public SearchPhaseRequestBuilder SetZipcode(string zipcode)
        {
            Instance.ZIPCode = zipcode;
            return this;
        }

        public SearchPhaseRequestBuilder SetExBuildingNr(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }
    }
}
