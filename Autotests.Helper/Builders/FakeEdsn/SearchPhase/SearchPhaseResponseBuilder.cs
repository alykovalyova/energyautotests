﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.Builders.FakeEdsn.SearchPhase
{
    public class SearchPhaseResponseBuilder : MainBuilder<SearchMeteringPointsResponseEnvelope_PC_Result_PMP>
    {
        public SearchPhaseResponseBuilder SetEanId(string eanId)
        {
            Instance.EANID = eanId;
            return this;
        }

        public SearchPhaseResponseBuilder SetGridOperator(SearchMeteringPointsResponseEnvelope_PC_Result_PMP_GridOperator_Company gridOperator)
        {
            Instance.GridOperator_Company = gridOperator;
            return this;
        }

        public SearchPhaseResponseBuilder SetGridArea(string gridArea)
        {
            Instance.GridArea = gridArea;
            return this;
        }

        public SearchPhaseResponseBuilder SetMarketSegment(string marketSegment)
        {
            Instance.MarketSegment = marketSegment;
            return this;
        }

        public SearchPhaseResponseBuilder SetAddress(SearchMeteringPointsResponseEnvelope_MPAddressResponseType address)
        {
            Instance.EDSN_AddressSearch = address;
            return this;
        }

        public SearchPhaseResponseBuilder SetProductType(string productType)
        {
            Instance.ProductType = productType;
            return this;
        }

        public SearchPhaseResponseBuilder SetEnergyMeter(SearchMeteringPointsResponseEnvelope_PC_Result_PMP_Portaal_EnergyMeter[] meter)
        {
            Instance.Portaal_EnergyMeter = meter;
            return this;
        }

        public SearchPhaseResponseBuilder SetPhysicalCharacteristics(SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MPPhysicalCharacteristics physicalCharacteristics)
        {
            Instance.MPPhysicalCharacteristics = physicalCharacteristics;
            return this;
        }

        public SearchPhaseResponseBuilder SetMeteringPointGroup(SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MeteringPointGroup mpGroup)
        {
            Instance.MeteringPointGroup = mpGroup;
            return this;
        }
    }
}
