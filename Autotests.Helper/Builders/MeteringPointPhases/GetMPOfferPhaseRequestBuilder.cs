﻿using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Nuts.MeteringPointOfferPhase.Contract;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    internal class GetMPOfferPhaseRequestBuilder : MainBuilder<GetMeteringPointsOfferPhaseRequest>
    {
        internal GetMPOfferPhaseRequestBuilder SetGasConnection(int index, string eanId, string externalRef, string supplierEan)
        {
            Instance.MeteringPointDetails[index].GasConnection = new ConnectionDetail
            {
                EanId = eanId,
                ExternalReference = externalRef,
                SupplierEan = supplierEan
            };

            return this;
        }

        internal GetMPOfferPhaseRequestBuilder SetBirthdayKey(int index, string key)
        {
            Instance.MeteringPointDetails[index].PermissionGrand.BirthDayKey = key;
            return this;
        }

        internal GetMPOfferPhaseRequestBuilder SetIbanKey(int index, string key)
        {
            Instance.MeteringPointDetails[index].PermissionGrand.IBANKey = key;
            return this;
        }

        internal GetMPOfferPhaseRequestBuilder SetPermissionType(int index, PermissionType type)
        {
            Instance.MeteringPointDetails[index].PermissionGrand.PermissionInfo.PermissionType = type.ToString();
            return this;
        }

        internal GetMPOfferPhaseRequestBuilder SetHasOptedInByCheckbox(int index, bool value)
        {
            Instance.MeteringPointDetails[index].PermissionGrand.PermissionInfo.HasOptedInByCheckbox = value;
            return this;
        }

        internal GetMPOfferPhaseRequestBuilder SetRequestOrigin(int index, string value)
        {
            Instance.MeteringPointDetails[index].PermissionGrand.PermissionInfo.RequestOrigin = value;
            return this;
        }
    }
}