﻿using Autotests.Clients.Rest.Contracts.ResellerApi;
using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using Autotests.Helper.Enums;
using Nuts.MeteringPointOfferPhase.Contract;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    public static class MeteringPointOfferPhaseBuilder
    {
        public static GetMeteringPointsOfferPhaseRequest WithGasConnection(
            this GetMeteringPointsOfferPhaseRequest source, int index, string eanId, string externalRef, string supplierEan)
        {
            source.MeteringPointDetails[index].GasConnection = new ConnectionDetail
            {
                EanId = eanId,
                ExternalReference = externalRef,
                SupplierEan = supplierEan
            };

            return source;
        }

        public static GetMeteringPointsOfferPhaseRequest SetBirthdayKey(this GetMeteringPointsOfferPhaseRequest source, int index, string key)
        {
            source.MeteringPointDetails[index].PermissionGrand.BirthDayKey = key;
            return source;
        }

        public static GetMeteringPointsOfferPhaseRequest SetIbanKey(this GetMeteringPointsOfferPhaseRequest source, int index, string key)
        {
            source.MeteringPointDetails[index].PermissionGrand.IBANKey = key;
            return source;
        }

        public static GetMeteringPointsOfferPhaseRequest SetPermissionType(
            this GetMeteringPointsOfferPhaseRequest source, int index, PermissionType type)
        {
            source.MeteringPointDetails[index].PermissionGrand.PermissionInfo.PermissionType = type.ToString();
            return source;
        }

        public static GetMeteringPointsOfferPhaseRequest SetHasOptedInByCheckbox(
            this GetMeteringPointsOfferPhaseRequest source, int index, bool value)
        {
            source.MeteringPointDetails[index].PermissionGrand.PermissionInfo.HasOptedInByCheckbox = value;
            return source;
        }

        public static GetMeteringPointsOfferPhaseRequest SetRequestOrigin(
            this GetMeteringPointsOfferPhaseRequest source, int index, string value)
        {
            source.MeteringPointDetails[index].PermissionGrand.PermissionInfo.RequestOrigin = value;
            return source;
        }

        public static GetMpOfferPhaseRequest WithElkConnection(
           this GetMpOfferPhaseRequest source, int index, string eanId, string supplierEan)
        {
            source.MeteringPointDetails[index].ElectricityConnection = new Connection()
            {
                EanId = eanId,
                SupplierEan = supplierEan
            };

            return source;
        }
    }
}
