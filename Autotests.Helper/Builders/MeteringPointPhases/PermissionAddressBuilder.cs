﻿using Autotests.Core.Helpers;
using Nuts.MeteringPointOfferPhase.Contract;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    internal class PermissionAddressBuilder : MainBuilder<PermissionAddress>
    {
        internal PermissionAddressBuilder SetDefaults()
        {
            Instance.City = "Kharkiv";
            Instance.HouseNumber = 71;
            Instance.HouseNumberSuffix = "B";
            Instance.Street = "Peremoga";
            return this;
        }

        internal PermissionAddressBuilder SetCity(string city)
        {
            Instance.City = city;
            return this;
        }

        internal PermissionAddressBuilder SetHouseNumber(int hsNr)
        {
            Instance.HouseNumber = hsNr;
            return this;
        }

        internal PermissionAddressBuilder SetHouseNumberSuffix(string exHsNr)
        {
            Instance.HouseNumberSuffix = exHsNr;
            return this;
        }

        internal PermissionAddressBuilder SetPostalCode(string postalCode)
        {
            Instance.PostalCode = postalCode;
            return this;
        }

        internal PermissionAddressBuilder SetStreet(string street)
        {
            Instance.Street = street;
            return this;
        }
    }
}