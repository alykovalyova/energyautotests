﻿using Autotests.Core.Helpers;
using Nuts.MeteringPointOfferPhase.Contract;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    internal class PermissionCustomerBuilder : MainBuilder<PermissionCustomer>
    {
        internal PermissionCustomerBuilder SetDefaults()
        {
            Instance.Initials = "J.J";
            Instance.LastName = "Wick";
            Instance.Prefix = "mr.";
            return this;
        }

        internal PermissionCustomerBuilder SetGender(string gender)
        {
            Instance.Gender = gender;
            return this;
        }

        internal PermissionCustomerBuilder SetInitials(string initials)
        {
            Instance.Initials = initials;
            return this;
        }

        internal PermissionCustomerBuilder SetLastName(string lastName)
        {
            Instance.LastName = lastName;
            return this;
        }

        internal PermissionCustomerBuilder SetPrefix(string prefix)
        {
            Instance.Prefix = prefix;
            return this;
        }
    }
}