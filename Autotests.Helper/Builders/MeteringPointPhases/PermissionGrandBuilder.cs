﻿using Autotests.Core.Helpers;
using Nuts.MeteringPointOfferPhase.Contract;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    internal class PermissionGrandBuilder : MainBuilder<PermissionGrand>
    {
        internal PermissionGrandBuilder SetPermissionInfo(PermissionInfo permissionInfo)
        {
            Instance.PermissionInfo = permissionInfo;
            return this;
        }

        internal PermissionGrandBuilder SetBirthDayKey(string birthDayKey)
        {
            Instance.BirthDayKey = birthDayKey;
            return this;
        }

        internal PermissionGrandBuilder SetIBANKey(string ibanKey)
        {
            Instance.IBANKey = ibanKey;
            return this;
        }
    }
}