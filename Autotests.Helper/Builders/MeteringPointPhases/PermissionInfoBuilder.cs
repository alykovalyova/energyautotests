﻿using Autotests.Core.Helpers;
using Nuts.MeteringPointOfferPhase.Contract;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    internal class PermissionInfoBuilder : MainBuilder<PermissionInfo>
    {
        internal PermissionInfoBuilder SetPermissionAddress(PermissionAddress address)
        {
            Instance.Address = address;
            return this;
        }

        internal PermissionInfoBuilder SetPermissionCustomer(PermissionCustomer customer)
        {
            Instance.Customer = customer;
            return this;
        }

        internal PermissionInfoBuilder SetHasOptedInByCheckbox(bool hasOptedInByCheckbox)
        {
            Instance.HasOptedInByCheckbox = hasOptedInByCheckbox;
            return this;
        }

        internal PermissionInfoBuilder SetPermissionDateTime(DateTime permissionDateTime)
        {
            Instance.PermissionDateTime = permissionDateTime;
            return this;
        }

        internal PermissionInfoBuilder SetPermissionPurpose(string permissionPurpose)
        {
            Instance.PermissionPurpose = permissionPurpose;
            return this;
        }

        internal PermissionInfoBuilder SetPermissionType(string permissionType)
        {
            Instance.PermissionType = permissionType;
            return this;
        }

        internal PermissionInfoBuilder SetRequestOrigin(string requestOrigin = "Origin")
        {
            Instance.RequestOrigin = requestOrigin;
            return this;
        }

        internal PermissionInfoBuilder SetResponsibleUser(string responsibleUser = "autotest_responsible_user")
        {
            Instance.ResponsibleUser = responsibleUser;
            return this;
        }
    }
}