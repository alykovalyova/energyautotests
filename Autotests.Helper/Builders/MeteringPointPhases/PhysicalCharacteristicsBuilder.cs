﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Helper.Builders.MeteringPointPhases
{
    public class PhysicalCharacteristicsBuilder : MainBuilder<Nuts.EdsnGateway.Contract.Queue.Mdu.PhysicalCharacteristics>
    {
        public PhysicalCharacteristicsBuilder SetDefaults()
        {
            Instance.CapTarCode = "987612345678";
            Instance.ContractedCapacity = "12155";
            Instance.EnergyDeliveryStatus = EnergyDeliveryStatusCode.ACT;
            Instance.MeteringMethod = EnergyMeteringMethodCode.NCE;
            Instance.PhysicalStatus = EnergyConnectionPhysicalStatusCode.IBD;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetAllocationMethod(EnergyAllocationMethodCode allocationMethod)
        {
            Instance.AllocationMethod = allocationMethod;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetEacPeak(int eacPeak)
        {
            Instance.EACPeak = eacPeak;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetEacOffPeak(int eacOffPeak)
        {
            Instance.EACOffPeak = eacOffPeak;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetFlowDirection(EnergyFlowDirectionCode flowDirection)
        {
            Instance.EnergyFlowDirection = flowDirection;
            return this;
        }

        public PhysicalCharacteristicsBuilder SetProfileCategory(EnergyUsageProfileCode? profileCategory)
        {
            Instance.ProfileCategory = profileCategory;
            return this;
        }
    }
}
