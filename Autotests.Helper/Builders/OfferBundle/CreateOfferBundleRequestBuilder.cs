﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.OfferBundle.Contract.Rest.Model.CreateOfferBundle;
using Nuts.OfferBundle.Contract.Rest.Transport;

namespace Autotests.Helper.Builders.OfferBundle
{
    public class CreateOfferBundleRequestBuilder : MainBuilder<CreateOfferBundleRequest>
    {
        public CreateOfferBundleRequestBuilder SetDefaults(string defaultName=null)
        {
            Instance.Name = defaultName;
            return this;
        }

        public CreateOfferBundleRequestBuilder SetClientType(string clientType = null)
        {
            Instance.ClientType = clientType != null ? clientType : string.Empty;
            return this;
        }

        public CreateOfferBundleRequestBuilder SetCustomerProductType(string customerProductType = null)
        {
            Instance.CustomerProductType = customerProductType != null ? customerProductType : string.Empty;
            return this;
        }

        public CreateOfferBundleRequestBuilder SetLabel(Labels? label = null)
        {
            Instance.Label = label != null ? label.ToString() : string.Empty;
            return this;
        }

        public CreateOfferBundleRequestBuilder SetName(string name = null)
        {
            Instance.Name = name;
            return this;
        }

        public CreateOfferBundleRequestBuilder SetOfferPropositions(params OfferProposition[] offerPropositions)
        {
            Instance.OfferPropositions = offerPropositions.ToList();
            return this;
        }

        public CreateOfferBundleRequestBuilder SetPropositionTypes(params string[] propositionTypes)
        {
            Instance.PropositionTypes = propositionTypes.ToList();
            return this;
        }
    }
}