﻿using System.Globalization;
using Amazon.Runtime.Internal;
using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using ElectricityPrice = Nuts.ProdMan.Model.Contract.ProductPricesToSave.ElectricityPrice;
using Proposition = Nuts.ProdMan.Model.Contract.PropositionsToSave.Proposition;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Helper.Builders.OfferBundle
{
    public class OfferBundlePropositionBuilder : MainBuilder<Proposition>
    {
        public OfferBundlePropositionBuilder GetBaseProposition(DateTime marketFrom, int duration = 12)
        {
            var propositionName = $"Autotests_{DateTime.Now.Ticks}";
            var elkRetailPrice = 0.05m;
            var elkAbsoluteDiscount = -0.05m;
            var marketTo = DateTime.Today.AddDays(30);
            var formattedDate = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(marketTo.ToString("dMMM", new CultureInfo("nl-NL")));
            var clientType = new List<TypeOfClient>() { TypeOfClient.Private, TypeOfClient.Business };
            var elkProfileCategories = new List<EnergyUsageProfileCode>()
            {
                EnergyUsageProfileCode.E1A, EnergyUsageProfileCode.E1B,
                EnergyUsageProfileCode.E1C, EnergyUsageProfileCode.E2A,
                EnergyUsageProfileCode.E2B
            };
            Instance.ElectricityPrices = new List<ElectricityPrice>()
            {
                
            };
            Instance.Name = propositionName;
            Instance.ClientType = clientType;
            Instance.Duration = duration;
            Instance.StandingChargePeriod = StandingChargePeriodType.PerDay;
            Instance.EnergySources = new List<EnergySource>() { EnergySource.NlWindE };
            Instance.MargeTypeN = MargeTypeN.Business;
            Instance.MargeTypeR = MargeTypeR.Passing;
            Instance.MarketFrom = marketFrom;
            return this;
        }

        public OfferBundlePropositionBuilder WithLabel(Labels label)
        {
            Instance.Label = label;
            return this;
        }

        public OfferBundlePropositionBuilder WithMarketTo(DateTime marketTo)
        {
            Instance.MarketTo = marketTo;
            return this;
        }

        public OfferBundlePropositionBuilder WithCashBackTest(CashBack cashBack)
        {
            Instance.CashBack = cashBack;
            return this;
        }
    }
}