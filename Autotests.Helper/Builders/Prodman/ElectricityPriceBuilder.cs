﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using Nuts.ProdMan.Model.Contract.ProductPricesToSave;

namespace Autotests.Helper.Builders.Prodman
{
    internal class ElectricityPriceBuilder : MainBuilder<ElectricityPrice>
    {
        internal ElectricityPriceBuilder SetCashBack(int cashBack)
        {
            Instance.CashBack = cashBack;
            return this;
        }

        internal ElectricityPriceBuilder SetCode(string code)
        {
            Instance.Code = code;
            return this;
        }

        internal ElectricityPriceBuilder SetElektraTariffs(List<ElektraTariff> tariffs)
        {
            Instance.ElektraTariffs = tariffs;
            return this;
        }

        internal ElectricityPriceBuilder SetId(Guid id)
        {
            Instance.Id = id;
            return this;
        }

        internal ElectricityPriceBuilder SetMeasureUnit(MeasureUnitCode measureUnit)
        {
            Instance.MeasureUnit = measureUnit;
            return this;
        }

        internal ElectricityPriceBuilder SetNpvPerCustomer(int value)
        {
            Instance.NpvPerCustomer = value;
            return this;
        }

        internal ElectricityPriceBuilder SetStandingCharge(int value)
        {
            Instance.StandingCharge = value;
            return this;
        }
    }
}