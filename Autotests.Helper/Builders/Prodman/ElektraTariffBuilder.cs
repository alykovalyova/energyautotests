﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;

namespace Autotests.Helper.Builders.Prodman
{
    internal class ElektraTariffBuilder : MainBuilder<ElektraTariff>
    {
        internal ElektraTariffBuilder SetProfileCategories(params EnergyUsageProfileCode[] energyUsageProfileCodes)
        {
            Instance.ProfileCategories = energyUsageProfileCodes.ToList();
            return this;
        }

        internal ElektraTariffBuilder SetProductionPrice(decimal value)
        {
            Instance.ProductionPrice = value;
            return this;
        }

        internal ElektraTariffBuilder SetRetailPrice(decimal value)
        {
            Instance.RetailPrice = value;
            return this;
        }

        internal ElektraTariffBuilder SetTariffType(ElektraTariffType elektraTariffType)
        {
            Instance.TariffType = elektraTariffType;
            return this;
        }

        internal ElektraTariffBuilder SetPriceDiscount(PriceDiscountType type, decimal value)
        {
            Instance.PriceDiscount = new PriceDiscount{Type = type, Value = value};
            return this;
        }

        internal ElektraTariffBuilder SetLoyalCustomerDiscount(decimal value)
        {
            Instance.LoyalCustomerDiscount = value;
            return this;
        }
    }
}