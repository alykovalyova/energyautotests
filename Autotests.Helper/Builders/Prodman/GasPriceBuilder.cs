﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using Nuts.ProdMan.Model.Contract.ProductPricesToSave;

namespace Autotests.Helper.Builders.Prodman
{
    internal class GasPriceBuilder : MainBuilder<GasPrice>
    {
        internal GasPriceBuilder SetGasTariffs(params GasTariff[] gasTariffs)
        {
            Instance.GasTariffs = gasTariffs.ToList();
            return this;
        }

        internal GasPriceBuilder SetStandingCharge(decimal value)
        {
            Instance.StandingCharge = value;
            return this;
        }

        internal GasPriceBuilder SetCashBack(decimal value)
        {
            Instance.CashBack = value;
            return this;
        }

        internal GasPriceBuilder SetNpvPerCustomer(decimal value)
        {
            Instance.NpvPerCustomer = value;
            return this;
        }

        internal GasPriceBuilder SetMeasureUnit(MeasureUnitCode unit)
        {
            Instance.MeasureUnit = unit;
            return this;
        }

        internal GasPriceBuilder SetCode(string code)
        {
            Instance.Code = code;
            return this;
        }

        internal GasPriceBuilder SetId(Guid id)
        {
            Instance.Id = id;
            return this;
        }
    }
}