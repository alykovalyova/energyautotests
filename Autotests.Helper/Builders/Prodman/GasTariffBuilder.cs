﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;

namespace Autotests.Helper.Builders.Prodman
{
    internal class GasTariffBuilder : MainBuilder<GasTariff>
    {
        internal GasTariffBuilder SetGasRegion(GasRegion gasRegion)
        {
            Instance.GasRegion = gasRegion;
            return this;
        }

        internal GasTariffBuilder SetProfileCategories(params EnergyUsageProfileCode[] profileCodes)
        {
            Instance.ProfileCategories = profileCodes.ToList();
            return this;
        }

        internal GasTariffBuilder SetRegionalSurcharge(decimal value)
        {
            Instance.RegionalSurcharge = value;
            return this;
        }

        internal GasTariffBuilder SetRetailPrice(decimal value)
        {
            Instance.RetailPrice = value;
            return this;
        }

        internal GasTariffBuilder SetLoyalCustomerDiscount(decimal value)
        {
            Instance.LoyalCustomerDiscount = value;
            return this;
        }

        internal GasTariffBuilder SetPriceDiscount(PriceDiscount priceDiscount)
        {
            Instance.PriceDiscount = priceDiscount;
            return this;
        }

        internal GasTariffBuilder SetTotalRetailPrice(decimal value)
        {
            Instance.TotalRetailPrice = value;
            return this;
        }
    }
}