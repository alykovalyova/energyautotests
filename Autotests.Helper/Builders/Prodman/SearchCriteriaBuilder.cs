﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;

namespace Autotests.Helper.Builders.Prodman
{
    internal class SearchCriteriaBuilder : MainBuilder<SearchCriteria>
    {
        internal SearchCriteriaBuilder SetClientType(TypeOfClient? typeOfClient)
        {
            Instance.ClientType = typeOfClient;
            return this;
        }

        internal SearchCriteriaBuilder SetLabelCode(Labels? label)
        {
            Instance.LabelCode = label;
            return this;
        }

        internal SearchCriteriaBuilder SetDurations(List<int> durations)
        {
            Instance.Durations = durations;
            return this;
        }

        internal SearchCriteriaBuilder SetIsEligibleAsRenewal(bool? isEligibleAsRenewal)
        {
            Instance.IsEligibleAsRenewal = isEligibleAsRenewal;
            return this;
        }

        internal SearchCriteriaBuilder SetIsSalesEligible(bool? isSalesEligible)
        {
            Instance.IsSalesEligible = isSalesEligible;
            return this;
        }

        internal SearchCriteriaBuilder SetStatus(Status? status)
        {
            Instance.Status = status;
            return this;
        }

        internal SearchCriteriaBuilder SetFrom(DateTime? from)
        {
            Instance.From = from;
            return this;
        }

        internal SearchCriteriaBuilder SetTo(DateTime? to)
        {
            Instance.To = to;
            return this;
        }
    }
}