﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;

namespace Autotests.Helper.Builders.Prodman
{
    internal class SearchCriteriaWithMultipleStatusesBuilder : MainBuilder<SearchCriteriaWithMultipleStatuses>
    {
        internal SearchCriteriaWithMultipleStatusesBuilder SetClientType(TypeOfClient? typeOfClient)
        {
            Instance.ClientType = typeOfClient;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetLabelCode(Labels? label)
        {
            Instance.LabelCode = label;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetDurations(List<int> durations)
        {
            Instance.Durations = durations;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetIsEligibleAsRenewal(bool? isEligibleAsRenewal)
        {
            Instance.IsEligibleAsRenewal = isEligibleAsRenewal;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetIsSalesEligible(bool? isSalesEligible)
        {
            Instance.IsSalesEligible = isSalesEligible;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetStatuses(List<Status> statuses)
        {
            Instance.Statuses = statuses;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetFrom(DateTime? from)
        {
            Instance.From = from;
            return this;
        }

        internal SearchCriteriaWithMultipleStatusesBuilder SetTo(DateTime? to)
        {
            Instance.To = to;
            return this;
        }
    }
}