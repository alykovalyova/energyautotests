﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract;

namespace Autotests.Helper.Builders.SalesChannel
{
    public class AddSalesChannelDetailsBuilder : MainBuilder<AddSalesChannelDetails>
    {
        public AddSalesChannelDetailsBuilder SetIsConsumerEligible(bool? isConsumerEligible)
        {
            Instance.IsConsumerEligible = isConsumerEligible;
            return this;
        }

        public AddSalesChannelDetailsBuilder SetIsBusinessEligible(bool? isBusinessEligible)
        {
            Instance.IsBusinessEligible = isBusinessEligible;
            return this;
        }

        public AddSalesChannelDetailsBuilder SetIsContractProvider(bool isContractProvider)
        {
            Instance.IsContractProvider = isContractProvider;
            return this;
        }

        public AddSalesChannelDetailsBuilder SetExternalReference(string exRef)
        {
            Instance.ExternalReference = exRef;
            return this;
        }

        public AddSalesChannelDetailsBuilder SetCampaign(int id, string name = "Test")
        {
            Instance.Campaign = new Campaign{Id = id, Name = name};
            return this;
        }

        public AddSalesChannelDetailsBuilder SetChannel(int id, string name)
        {
            Instance.Channel = new Channel{Id = id, Name = name};
            return this;
        }

        public AddSalesChannelDetailsBuilder SetPartner(int id, string name = "5-steps")
        {
            Instance.Partner = new Partner{Id = id, Name = name};
            return this;
        }

        public AddSalesChannelDetailsBuilder SetCoolDownPeriod(int id, int value)
        {
            Instance.CoolDownPeriod = new CoolDownPeriod{Id = id, Value = value};
            return this;
        }

        public AddSalesChannelDetailsBuilder SetSwitchWindow(int id, int value)
        {
            Instance.SwitchWindow = new SwitchWindow{Id = id, Value = value};
            return this;
        }

        public AddSalesChannelDetailsBuilder SetLabel(Labels label)
        {
            Instance.Label = label;
            return this;
        }

        public AddSalesChannelDetailsBuilder SetRegion(int id, string name = "Regio 1")
        {
            Instance.Region = new Region{Id = id, Name = name};
            return this;
        }

        public AddSalesChannelDetailsBuilder SetStore(int id, string name = "Alkmaar")
        {
            Instance.Store = new Store{Id = id, Name = name};
            return this;
        }
    }
}