﻿using Nuts.MeteringPoint.Contract;

namespace Autotests.Helper
{
    public static class DataProvider
    {
        internal static IEnumerable<int> GetNumOfTests()
        {
            var list = new List<int>(Enumerable.Range(1, 3));
            foreach (var i in list)
            {
                yield return i;
            }
        }

        public static IEnumerable<KeyValuePair<string, Address>> GetValidAddressesForTest()
        {
            var listOfData = new List<KeyValuePair<string, Address>>
            {
                new KeyValuePair<string, Address>("'Change all values'",
                    GetNewAddress("1234QQ", 61, "C", "Kiev", "UA", "Asharova")),
                new KeyValuePair<string, Address>("'Change only BuildingNr'", GetNewAddress(buildingNr: 61)),
                new KeyValuePair<string, Address>("'Change only ExBuildingNr'", GetNewAddress(exBuildingNr: null)),
                new KeyValuePair<string, Address>("'Change only ZipCode'", GetNewAddress("1234QQ")),
                new KeyValuePair<string, Address>("'Change only CityName'", GetNewAddress(cityName: "Kiev")),
                new KeyValuePair<string, Address>("'Change only County'", GetNewAddress(country: "UA")),
                new KeyValuePair<string, Address>("'Change only StreetName'", GetNewAddress(streetName: "Asharova"))
            };
            foreach (var address in listOfData)
            {
                yield return address;
            }
        }

        public static Address GetNewAddress(
            string zipCode = "1111QQ", int buildingNr = 71, string exBuildingNr = "B",
            string cityName = "Kharkiv", string country = "NL", string streetName = "Peremoga")
        {
            return new Address
            {
                ZipCode = zipCode,
                BuildingNr = buildingNr,
                ExBuildingNr = exBuildingNr,
                CityName = cityName,
                Country = country,
                StreetName = streetName
            };
        }

        public static IEnumerable<KeyValuePair<int, string>> GetCampaignTestData()
        {
            var data = new List<KeyValuePair<int, string>>
            {
                new KeyValuePair<int, string>(0, "Test"),
                new KeyValuePair<int, string>(1, string.Empty),
                new KeyValuePair<int, string>(1, "Test_InvalidName")
            };

            foreach (var values in data) yield return values;
        }

        public static IEnumerable<KeyValuePair<int, string>> GetPartnerTestData()
        {
            var data = new List<KeyValuePair<int, string>>
            {
                new KeyValuePair<int, string>(0, "5-steps"),
                new KeyValuePair<int, string>(1, string.Empty),
                new KeyValuePair<int, string>(1, "Test_InvalidName")
            };

            foreach (var values in data) yield return values;
        }

        public static IEnumerable<KeyValuePair<int, string>> GetRegionTestData()
        {
            var data = new List<KeyValuePair<int, string>>
            {
                new KeyValuePair<int, string>(0, "Regio 1"),
                new KeyValuePair<int, string>(1, string.Empty),
                new KeyValuePair<int, string>(1, "Test_InvalidName")
            };

            foreach (var values in data) yield return values;
        }

        public static IEnumerable<KeyValuePair<int, string>> GetStoreTestData()
        {
            var data = new List<KeyValuePair<int, string>>
            {
                new KeyValuePair<int, string>(0, "Alkmaar"),
                new KeyValuePair<int, string>(1, string.Empty),
                new KeyValuePair<int, string>(1, "Test_InvalidName")
            };

            foreach (var values in data) yield return values;
        }

        public static IEnumerable<KeyValuePair<int, int>> GetCoolDownPeriodTestData()
        {
            var data = new List<KeyValuePair<int, int>>
            {
                new KeyValuePair<int, int>(0, 14)
            };

            foreach (var values in data) yield return values;
        }

        public static IEnumerable<KeyValuePair<int, int>> GetSwitchWindowTestData()
        {
            var data = new List<KeyValuePair<int, int>>
            {
                new KeyValuePair<int, int>(0, 244),
                new KeyValuePair<int, int>(1, 0)
            };

            foreach (var values in data) yield return values;
        }
    }
}