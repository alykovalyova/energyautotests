﻿using System.Globalization;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public class CcuEntitiesProvider
    {
        private static ConfigHandler _config = new ConfigHandler();
        private static RestSchedulerClient _restSchedulerClient = RestSchedulerClient.Instance;
        protected static HttpClient EdsnSwitchHttpClient = new BaseHttpClient(_config.ApiUrls.EdsnSwitch);
        private static NutsHttpClient _edsnSwitchClient = new NutsHttpClient(EdsnSwitchHttpClient);

        public Nuts.FakeEdsn.Contracts.CommercialCharacteristic CreateCommercialCharacteristic(
            string eanId, string balanceSupplier = null, string balanceResponsible = null,
            DateTime? mutationDate = null, string dossierId = null, string exRef = null,
            EnergyProductTypeCode? productType = null)
        {
            return new Nuts.FakeEdsn.Contracts.CommercialCharacteristic
            {
                EanId = eanId,
                DossierId = dossierId ?? TestConstants.DossierId,
                ExternalReference = exRef ?? RandomDataProvider.GetRandomNumbersString(5),
                ProductType = productType ?? EnergyProductTypeCode.ELK,
                BalanceSupplier = balanceSupplier,
                BalanceSupplierResponsible = balanceResponsible ?? TestConstants.BalanceResponsible,
                MutationDate = mutationDate
            };
        }

        public void CallMoveIn(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            GtwFactory.CreateMoveInRequest(eanId, TestConstants.BalanceResponsible, balanceSupplier,
                    TestConstants.GridOperator,
                    TestConstants.ExternalReference, mutationDate.ToString(CultureInfo.InvariantCulture),
                    TestConstants.KvkNumber,
                    int.Parse(TestConstants.BuildingNr),
                    TestConstants.Zipcode)
                .CallWith<MoveInRequest, MoveInResponse>(_edsnSwitchClient);
            _restSchedulerClient.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetGainsFake);          
        }

        public void CallChangeOfSupplier(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            GtwFactory.CreateChangeOfSupplierRequest(eanId, TestConstants.BalanceResponsible, balanceSupplier,
                    TestConstants.GridOperator,
                    TestConstants.ExternalReference, int.Parse(TestConstants.BuildingNr),
                    TestConstants.Zipcode,
                    TestConstants.KvkNumber, mutationDate)
                .CallWith<ChangeOfSupplierRequest, ChangeOfSupplierResponse>(_edsnSwitchClient);
            _restSchedulerClient.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetGainsFake);
        }

        public void CallMoveOut(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            var request = GtwFactory.CreateMoveOutRequest(eanId, balanceSupplier, TestConstants.GridOperator,
                    TestConstants.ExternalReference, mutationDate);
            request.CallWith<MoveOutRequest, MoveOutResponse>(_edsnSwitchClient);
            _restSchedulerClient.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);
        }

        public void CallEndOfSupply(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            GtwFactory.CreateEndOfSupplyRequest(eanId, balanceSupplier, TestConstants.GridOperator,
                    TestConstants.ExternalReference, mutationDate)
                .CallWith<EndOfSupplyRequest, EndOfSupplyResponse>(_edsnSwitchClient);
            _restSchedulerClient.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);
        }
    }
}
