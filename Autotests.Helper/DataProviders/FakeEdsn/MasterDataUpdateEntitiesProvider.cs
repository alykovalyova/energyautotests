﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public class MasterDataUpdateEntitiesProvider
    {
        public static MasterDataUpdateResponseEnvelope_PC_PMP_GridOperator_Company GridOperator =
            new MasterDataUpdateResponseEnvelope_PC_PMP_GridOperator_Company()
            {
                ID = TestConstants.GridOperator
            };

        public static MasterDataUpdateResponseEnvelope_MPAddressRequestType Address =
            new MasterDataUpdateResponseEnvelope_MPAddressRequestType()
            {
                BuildingNr = TestConstants.BuildingNr,
                BAG = new MasterDataUpdateResponseEnvelope_BAGType()
                {
                    BAGBuildingID = TestConstants.BagBuildingId,
                    BAGID = TestConstants.BagId
                },
                Country = TestConstants.Country,
                CityName = TestConstants.City,
                ExBuildingNr = TestConstants.ExBuildingNr,
                StreetName = TestConstants.Street,
                ZIPCode = TestConstants.Zipcode
            };

        public static MasterDataUpdateResponseEnvelope_PC_PMP_Portaal_EnergyMeter EnergyMeter =
            new MasterDataUpdateResponseEnvelope_PC_PMP_Portaal_EnergyMeter()
            {
                ID = TestConstants.EnergyMeterId,
                NrOfRegisters = "1",
                Type = TestConstants.MeterType,
                Register = new[]
                {
                    new MasterDataUpdateResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register()
                    {
                        ID = "1",
                        MultiplicationFactorSpecified = false,
                        MeteringDirection = TestConstants.EnergyFlowDirection,
                        NrOfDigits = "5",
                        TariffType = TestConstants.TariffType
                    }
                },
                TechnicalCommunicationSM = TestConstants.TechnicalCommunicationSm
            };

        public static MasterDataUpdateResponseEnvelope_PC_PMP_MPPhysicalCharacteristics PhysicalCharacteristics =
            new MasterDataUpdateResponseEnvelope_PC_PMP_MPPhysicalCharacteristics()
            {
                AllocationMethod = TestConstants.AllocationMethod,
                CapTarCode = TestConstants.CapTarCode,
                ContractedCapacity = TestConstants.ContractedCapacity,
                EnergyFlowDirection = TestConstants.EnergyFlowDirection,
                EAEnergyConsumptionNettedOffPeak = RandomDataProvider.GetRandomNumbersString(3),
                EAEnergyConsumptionNettedPeak = RandomDataProvider.GetRandomNumbersString(2),
                EAEnergyProductionNettedOffPeak = RandomDataProvider.GetRandomNumbersString(3),
                EAEnergyProductionNettedPeak = RandomDataProvider.GetRandomNumbersString(2),
                EnergyDeliveryStatus = TestConstants.EnergyDeliveryStatus,
                InvoiceMonth = TestConstants.InvoiceMonth,
                ProfileCategory = TestConstants.ProfileCategory,
                PhysicalStatus = TestConstants.PhysicalStatus,
                PhysicalCapacity = TestConstants.PhysicalCapacity,
                MeteringMethod = TestConstants.MeteringMethod
            };

        public static MasterDataUpdateResponseEnvelope_PC_PMP_MPCC CommercialCharacteristics =
            new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC()
            {
                BalanceResponsibleParty_Company =
                    new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC_BalanceResponsibleParty_Company()
                    {
                        ID = TestConstants.BalanceResponsible
                    },
                BalanceSupplier_Company = new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC_BalanceSupplier_Company()
                {
                    ID = TestConstants.BudgetSupplier
                },
                MeteringResponsibleParty_Company =
                    new MasterDataUpdateResponseEnvelope_PC_PMP_MPCC_MeteringResponsibleParty_Company()
                    {
                        ID = TestConstants.BalanceResponsible
                    }
            };

        public static MasterDataUpdateResponseEnvelope_PC_PMP_MeteringPointGroup MeteringPointGroup =
            new MasterDataUpdateResponseEnvelope_PC_PMP_MeteringPointGroup()
            {
                PAP = new MasterDataUpdateResponseEnvelope_PC_PMP_MeteringPointGroup_PAP()
                {
                    EANID = TestConstants.PAPEan
                },
                SAP = new[]
                {
                    new MasterDataUpdateResponseEnvelope_PC_PMP_MeteringPointGroup_SAP()
                    {
                        EANID = TestConstants.SAPEan
                    }
                }
            };
    }
}
