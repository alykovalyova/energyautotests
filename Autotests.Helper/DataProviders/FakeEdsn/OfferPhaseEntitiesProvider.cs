﻿namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public class OfferPhaseEntitiesProvider
    {
        public static string TechnicalCommunicationSm = "SMN";
        public static string MeterType = "DUN";
        public static string EacOffPeak = "100";
        public static string EacPeak = "230";
        public static string EapOffPeak = "0";
        public static string EapPeak = "0";
        
        public static string DossierId = "11111111";
        public bool IsSmartMeter = false;

        public GetMPInformationResponseEnvelope_MPAddressResponseType EdsnAddress { get; set; } =
            new GetMPInformationResponseEnvelope_MPAddressResponseType
            {
                BuildingNr = TestConstants.BuildingNr,
                CityName = TestConstants.City,
                BAG = new GetMPInformationResponseEnvelope_BAGType
                {
                    BAGBuildingID = TestConstants.BagBuildingId,
                    BAGID = TestConstants.BagId
                },
                StreetName = TestConstants.Street,
                ZIPCode = TestConstants.Zipcode
            };

        public GetMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter EnergyMeter { get; set; } =
            new GetMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter
            {
                TechnicalCommunicationSM = TechnicalCommunicationSm,
                Type = MeterType
            };

        public GetMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics PhysicalCharacteristics
        {
            get;
            set;
        } = new GetMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics
        {
            EnergyFlowDirection = TestConstants.EnergyFlowDirection,
            CapTarCode = TestConstants.CapTarCode,
            EAEnergyConsumptionNettedOffPeak = EacOffPeak,
            EAEnergyConsumptionNettedPeak = EacPeak,
            EAEnergyProductionNettedOffPeak = EapOffPeak,
            EAEnergyProductionNettedPeak = EapPeak,
            ProfileCategory = TestConstants.ProfileCategory
        };

        public GetMPInformationResponseEnvelope_PC_PMP_Portaal_Mutation Mutation { get; set; }
            = new GetMPInformationResponseEnvelope_PC_PMP_Portaal_Mutation
            {
                Dossier = new GetMPInformationResponseEnvelope_PC_PMP_Portaal_Mutation_Dossier
                {
                    ID = DossierId
                }
            };

        public GetMPInformationResponseEnvelope_PC_PMP_GridOperator_Company GridOperator { get; set; } =
            new GetMPInformationResponseEnvelope_PC_PMP_GridOperator_Company
            {
                ID = "983743737423"
            };

        public GetMPInformationResponseEnvelope_PC_PMP_MeteringPointGroup MeteringPointGroup { get; set; } =
            new GetMPInformationResponseEnvelope_PC_PMP_MeteringPointGroup
            {
                PAP = new GetMPInformationResponseEnvelope_PC_PMP_MeteringPointGroup_PAP
                    {EANID = "7654321987654321"},
                SAP = new[]
                {
                    new GetMPInformationResponseEnvelope_PC_PMP_MeteringPointGroup_SAP
                        {EANID = "7654321987654321"}
                }
            };

    }
}
