﻿using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.TestDataProviders;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public static class P4DataProvider
    {
        private static ConfigHandler _config = ConfigHandler.Instance;
        private static DbHandler<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPointContext> _meteringPointDb = new DbHandler<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPointContext>(_config.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));

        public static GetHistoricalSmartMeterReadingSeriesResponseEnvelope AddHistoricalReadings(EanInfo ean,
            string externalReference, DateTime date)
        {
            if (ean.ProductType == EnergyProductTypeCode.ELK)
                {
                    return new GetHistoricalSmartMeterReadingSeriesResponseEnvelope()

                    {
                        EDSNBusinessDocumentHeader =
                            new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader()
                            {
                                Destination =
                                    new
                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader_Destination()
                                        {
                                            Receiver =
                                                new
                                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader_Destination_Receiver()
                                                    {
                                                        ReceiverID = "8712423010208"
                                                    }
                                        },
                                Source =
                                    new
                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader_Source()
                                        {
                                            SenderID = TestConstants.BudgetSupplier
                                        }
                            },
                        Portaal_Content = new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content()
                        {
                            Query = new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Query()
                            {
                                ExternalReference = externalReference
                            },
                            Portaal_MeteringPoint =
                                new
                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint()
                                    {
                                        EANID = ean.EanId,
                                        Portaal_EnergyMeter =
                                            new
                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter
                                                []
                                                {
                                                    new
                                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter()
                                                        {
                                                            ID = ean.EnergyMeterId,
                                                            Register =
                                                                new
                                                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register
                                                                    []
                                                                    {
                                                                        new
                                                                            GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register()
                                                                            {
                                                                                ID = "1.8.1",
                                                                                MeasureUnit = "KWH",
                                                                                Reading =
                                                                                    new
                                                                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading
                                                                                        []
                                                                                        {
                                                                                            new
                                                                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                                                                                                {
                                                                                                    Reading =
                                                                                                        new Random()
                                                                                                            .Next(0,
                                                                                                                500)
                                                                                                            .ToString(),
                                                                                                    ReadingDateTime = date
                                                                                                        
                                                                                                }
                                                                                        }
                                                                            },
                                                                        new
                                                                            GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register()
                                                                            {
                                                                                ID = "1.8.2",
                                                                                MeasureUnit = "KWH",
                                                                                Reading =
                                                                                    new
                                                                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading
                                                                                        []
                                                                                        {
                                                                                            new
                                                                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                                                                                                {
                                                                                                    Reading =
                                                                                                        new Random()
                                                                                                            .Next(0,
                                                                                                                500)
                                                                                                            .ToString(),
                                                                                                    ReadingDateTime =
                                                                                                        date
                                                                                                }
                                                                                        }
                                                                            },
                                                                        new
                                                                            GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register()
                                                                            {
                                                                                ID = "2.8.1",
                                                                                MeasureUnit = "KWH",
                                                                                Reading =
                                                                                    new
                                                                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading
                                                                                        []
                                                                                        {
                                                                                            new
                                                                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                                                                                                {
                                                                                                    Reading =
                                                                                                        new Random()
                                                                                                            .Next(0,
                                                                                                                500)
                                                                                                            .ToString(),
                                                                                                    ReadingDateTime =
                                                                                                        date
                                                                                                }
                                                                                        }
                                                                            },
                                                                        new
                                                                            GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register()
                                                                            {
                                                                                ID = "2.8.2",
                                                                                MeasureUnit = "KWH",
                                                                                Reading =
                                                                                    new
                                                                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading
                                                                                        []
                                                                                        {
                                                                                            new
                                                                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                                                                                                {
                                                                                                    Reading =
                                                                                                        new Random()
                                                                                                            .Next(0,
                                                                                                                500)
                                                                                                            .ToString(),
                                                                                                    ReadingDateTime =
                                                                                                        date
                                                                                                }
                                                                                        }
                                                                            }

                                                                    }
                                                        }
                                                }
                                    }
                        }
                    };
                }

                return new GetHistoricalSmartMeterReadingSeriesResponseEnvelope()
                {
                    EDSNBusinessDocumentHeader =
                        new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader()
                        {
                            Destination =
                                new
                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader_Destination()
                                    {
                                        Receiver =
                                            new
                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader_Destination_Receiver()
                                                {
                                                    ReceiverID = "8712423010208"
                                                }
                                    },
                            Source =
                                new
                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_EDSNBusinessDocumentHeader_Source()
                                    {
                                        SenderID = TestConstants.BudgetSupplier
                                    }
                        },
                    Portaal_Content = new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content()
                    {
                        Query = new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Query()
                        {
                            ExternalReference = externalReference
                        },
                        Portaal_MeteringPoint =
                            new
                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint()
                                {
                                    EANID = ean.EanId,
                                    Portaal_EnergyMeter =
                                        new
                                            GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter
                                            []
                                            {
                                                new
                                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter()
                                                    {
                                                        ID = ean.EnergyMeterId,

                                                        Register =
                                                            new
                                                                GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register
                                                                []
                                                                {
                                                                    new
                                                                        GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register()
                                                                        {
                                                                            ID = "1.8.0",
                                                                            MeasureUnit = "MTQ",
                                                                            Reading =
                                                                                new
                                                                                    GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading
                                                                                    []
                                                                                    {
                                                                                        new
                                                                                            GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                                                                                            {
                                                                                                Reading = new Random()
                                                                                                    .Next(0, 500)
                                                                                                    .ToString(),
                                                                                                ReadingDateTime = date
                                                                                            }
                                                                                    }
                                                                        }
                                                                }
                                                    }
                                            }
                                }
                    }
                };
            }

        public static object[] AddDailyReadingsDetails(EanInfo mp, bool isProcessed = true, string rejectionCode = null)
        {
            var energyMeterId = _meteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(m =>
                m.EanId == mp.EanId).MeterEdsnId;
            if (isProcessed)
            {
                return GenerateP4EnergyMeter(energyMeterId, mp);
            }

            return new object[]
            {
                new P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4Rejection()
                {
                    Rejection = new P4CollectedDataBatchResultResponseEnvelope_RejectionP4Type()
                    {
                        RejectionCode = rejectionCode,
                        RejectionText = "RejectedByFakeEdsn_ControllerCall"
                    }
                }
            };
        }

        public static object[] GenerateP4EnergyMeter(string energyMeterId, EanInfo mp)
        {
            if (mp.ProductType == EnergyProductTypeCode.ELK)
            {
                return new object[]
                        {
                            new P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter()
                            {
                                ID = energyMeterId,
                                P4Register =
                                    new
                                        P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register
                                        []
                                        {
                                            new
                                                P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register()
                                                {
                                                    ID = "1.8.1",
                                                    MeasureUnit =
                                                        P4CollectedDataBatchResultResponseEnvelope_MeasureUnitCode.KWH,
                                                    P4Reading =
                                                        new
                                                            P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading
                                                            []
                                                            {
                                                                new
                                                                    P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading()
                                                                    {
                                                                        ReadingDateTime = DateTime.Now,
                                                                        Reading = new Random().Next(0, 200),
                                                                        ReadingSpecified = true
                                                                    }
                                                            }
                                                },
                                            new
                                                P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register()
                                                {
                                                    ID = "1.8.2",
                                                    MeasureUnit =
                                                        P4CollectedDataBatchResultResponseEnvelope_MeasureUnitCode.KWH,
                                                    P4Reading =
                                                        new
                                                            P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading
                                                            []
                                                            {
                                                                new
                                                                    P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading()
                                                                    {
                                                                        ReadingDateTime = DateTime.Now,
                                                                        Reading = new Random().Next(0, 200),
                                                                        ReadingSpecified = true
                                                                    }
                                                            }
                                                },
                                            new
                                                P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register()
                                                {
                                                    ID = "2.8.1",
                                                    MeasureUnit =
                                                        P4CollectedDataBatchResultResponseEnvelope_MeasureUnitCode.KWH,
                                                    P4Reading =
                                                        new
                                                            P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading
                                                            []
                                                            {
                                                                new
                                                                    P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading()
                                                                    {
                                                                        ReadingDateTime = DateTime.Now,
                                                                        Reading = new Random().Next(0, 200),
                                                                        ReadingSpecified = true
                                                                    }
                                                            }
                                                },
                                            new
                                                P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register()
                                                {
                                                    ID = "2.8.2",
                                                    MeasureUnit =
                                                        P4CollectedDataBatchResultResponseEnvelope_MeasureUnitCode.KWH,
                                                    P4Reading =
                                                        new
                                                            P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading
                                                            []
                                                            {
                                                                new
                                                                    P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading()
                                                                    {
                                                                        ReadingDateTime = DateTime.Now,
                                                                        Reading = new Random().Next(0, 200),
                                                                        ReadingSpecified = true
                                                                    }
                                                            }
                                                }

                                        }
                            }
                        
                    
                };
            }

            return new object[]
            {
                new P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter()
                {
                    ID = energyMeterId,
                    P4Register =
                        new
                            P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register
                            []
                            {
                                new
                                    P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register()
                                    {
                                        ID = "1.8.0",
                                        MeasureUnit = P4CollectedDataBatchResultResponseEnvelope_MeasureUnitCode.MTQ,
                                        P4Reading =
                                            new
                                                P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading
                                                []
                                                {
                                                    new
                                                        P4CollectedDataBatchResultResponseEnvelope_P4Content_P4MeteringPoint_P4EnergyMeter_P4Register_P4Reading()
                                                        {
                                                            ReadingDateTime = DateTime.Now,
                                                            Reading = new Random().Next(0, 200),
                                                            ReadingSpecified = true
                                                        }
                                                }
                                    }
                            }
                }

            };
        }

        }
    }

