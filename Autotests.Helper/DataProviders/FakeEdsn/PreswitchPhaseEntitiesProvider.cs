﻿
namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public class PreswitchPhaseEntitiesProvider
    {
        public GetSCMPInformationResponseEnvelope_PC_PMP_GridOperator_Company GridOperatorCompany =>
            new GetSCMPInformationResponseEnvelope_PC_PMP_GridOperator_Company
            { ID = "83489745564" };

        public GetSCMPInformationResponseEnvelope_MPAddressResponseType Address =>
            new GetSCMPInformationResponseEnvelope_MPAddressResponseType
            {
                BAG = new GetSCMPInformationResponseEnvelope_BAGType { BAGBuildingID = "27843", BAGID = "3445453" },
                BuildingNr = "71",
                CityName = "Kharkiv",
                Country = "NL",
                ExBuildingNr = "B",
                StreetName = "Peremoga",
                ZIPCode = "1111QQ"
            };

        public GetSCMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter EnergyMeter =>
            new GetSCMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter
            {
                ID = "1234554321Q",
                NrOfRegisters = "1",
                Type = "SLM",
                TechnicalCommunicationSM = "SMU",
                TemperatureCorrection = "0",
                Register = new[]
                {
                    new GetSCMPInformationResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register
                    {
                        ID = "1",
                        MeteringDirection = "LVR",
                        MultiplicationFactor = 1.00m,
                        MultiplicationFactorSpecified = true,
                        NrOfDigits = "5",
                        TariffType = "N"
                    }
                }
            };

        public GetSCMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics PhysicalCharacteristics =>
            new GetSCMPInformationResponseEnvelope_PC_PMP_MPPhysicalCharacteristics
            {
                CapTarCode = "1234567890123",
                EAEnergyConsumptionNettedOffPeak = "123",
                EAEnergyConsumptionNettedPeak = "45",
                EAEnergyProductionNettedOffPeak = "0",
                EAEnergyProductionNettedPeak = "0",
                EnergyFlowDirection = TestConstants.EnergyFlowDirection,
                MeteringMethod = "JRL",
                PhysicalStatus = "IBD",
                ProfileCategory = TestConstants.ProfileCategory
            };

        public GetSCMPInformationResponseEnvelope_PC_PMP_PM Mutation =>
            new GetSCMPInformationResponseEnvelope_PC_PMP_PM()
            {
                Dossier = new GetSCMPInformationResponseEnvelope_PC_PMP_PM_Dossier
                { ID = "11111111" },
                ExternalReference = "autotest_external_reference",
                MutationDate = DateTime.Now.AddMonths(-1)
            };

        public GetSCMPInformationResponseEnvelope_PC_PMP_PM_Dossier Dossier =>
            new GetSCMPInformationResponseEnvelope_PC_PMP_PM_Dossier
            { ID = "11111111" };
    }
}