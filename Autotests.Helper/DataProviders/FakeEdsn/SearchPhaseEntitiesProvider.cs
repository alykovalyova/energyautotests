﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public class SearchPhaseEntitiesProvider
    {
        public static SearchMeteringPointsResponseEnvelope_MPAddressResponseType Address =
            new SearchMeteringPointsResponseEnvelope_MPAddressResponseType()
            {
                BuildingNr = TestConstants.BuildingNr,
                BAG = new SearchMeteringPointsResponseEnvelope_BAGType()
                {
                    BAGBuildingID = TestConstants.BagBuildingId,
                    BAGID = TestConstants.BagId
                },
                Country = TestConstants.Country,
                CityName = TestConstants.City, ExBuildingNr = TestConstants.ExBuildingNr,
                StreetName = TestConstants.Street,
                ZIPCode = TestConstants.Zipcode
            };

        public static SearchMeteringPointsResponseEnvelope_PC_Result_PMP_Portaal_EnergyMeter EnergyMeter =
            new SearchMeteringPointsResponseEnvelope_PC_Result_PMP_Portaal_EnergyMeter()
            {
                ID = RandomDataProvider.GetRandomNumbersString(6)
            };

        public static SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MPPhysicalCharacteristics
            PhysicalCharacteristics =
                new SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MPPhysicalCharacteristics()
                {
                    AllocationMethod = TestConstants.AllocationMethod,
                    ContractedCapacity = TestConstants.ContractedCapacity,
                    EnergyFlowDirection = TestConstants.EnergyFlowDirection,
                    InvoiceMonth = TestConstants.InvoiceMonth,
                    MeteringMethod = TestConstants.MeteringMethod,
                    PhysicalCapacity = TestConstants.PhysicalCapacity,
                    ProfileCategory = TestConstants.ProfileCategory,
                    Subtype = TestConstants.SubType
                };

        public static SearchMeteringPointsResponseEnvelope_PC_Result_PMP_GridOperator_Company GridOperator =
            new SearchMeteringPointsResponseEnvelope_PC_Result_PMP_GridOperator_Company()
            {
                ID = TestConstants.GridOperator
            };

        public static SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MeteringPointGroup MpGroup =
            new SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MeteringPointGroup()
            {
                PAP = new SearchMeteringPointsResponseEnvelope_PC_Result_PMP_MeteringPointGroup_PAP()
                    {EANID = TestConstants.PAPEan}
            };
    }
}
