﻿using Autotests.Core.Helpers;

namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public class SupplyPhaseTestConstants
    {
        public static GetMeteringPointResponseEnvelope_PC_PMP_GridOperator_Company GridOperator =
            new GetMeteringPointResponseEnvelope_PC_PMP_GridOperator_Company()
                {ID = TestConstants.GridOperator};

        public static GetMeteringPointResponseEnvelope_PC_PMP_MeteringPointGroup MeteringPointGroup =
            new GetMeteringPointResponseEnvelope_PC_PMP_MeteringPointGroup()
            {
                PAP = new GetMeteringPointResponseEnvelope_PC_PMP_MeteringPointGroup_PAP()
                    {EANID = TestConstants.PAPEan},
                SAP = new[]
                {
                    new GetMeteringPointResponseEnvelope_PC_PMP_MeteringPointGroup_SAP()
                        {EANID = TestConstants.SAPEan}
                }
            };

        public static string EnergyMeterId = "TEST19283";

        public static GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter GetPortalEnergyMeter(string energyMeterId)
        {
            return new GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter()
            {
                ID = energyMeterId ?? $"TEST{RandomDataProvider.GetRandomNumbersString(5)}",
                NrOfRegisters = "1",
                Register = new[]
                {
                    new GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register()
                    {
                        ID = "1",
                        MeteringDirection = TestConstants.EnergyFlowDirection,
                        MultiplicationFactorSpecified = false,
                        NrOfDigits = "5",
                        TariffType = TestConstants.TariffType
                    }
                }
            };
        }

        public static GetMeteringPointResponseEnvelope_PC_PMP_MPPhysicalCharacteristics PhysicalCharacteristics =
            new GetMeteringPointResponseEnvelope_PC_PMP_MPPhysicalCharacteristics()
            {
                AllocationMethod = TestConstants.AllocationMethod,
                Appliance = "false",
                Subtype = "ALD",
                SustainableEnergy = "BIO",
                Switchability = "N",
                MeteringMethod = TestConstants.MeteringMethod,
                ProfileCategory = TestConstants.ProfileCategory,
                PhysicalStatus = TestConstants.PhysicalStatus,
                ArticleSub = "true",
                CapTarCode = TestConstants.CapTarCode,
                ContractedCapacity = TestConstants.ContractedCapacity,
                DisconnectionMethod = "BIN",
                EnergyFlowDirection = TestConstants.EnergyFlowDirection,
                EAEnergyConsumptionNettedOffPeak = "100",
                EAEnergyConsumptionNettedPeak = "200",
                EAEnergyProductionNettedOffPeak = "50",
                EAEnergyProductionNettedPeak = "30",
                EnergyDeliveryStatus = TestConstants.EnergyDeliveryStatus
            };

        public static GetMeteringPointResponseEnvelope_Address Address = new GetMeteringPointResponseEnvelope_Address()
        {
            BuildingNr = TestConstants.BuildingNr,
            ExBuildingNr = TestConstants.ExBuildingNr,
            Country = TestConstants.Country,
            StreetName = TestConstants.Street,
            BAG = new GetMeteringPointResponseEnvelope_BAGType()
            {
                BAGBuildingID = TestConstants.BagBuildingId,
                BAGID = TestConstants.BagId
            },
            CityName = TestConstants.City,
            ZIPCode = TestConstants.Zipcode,
            EDSN_GeographicalCoordinate = new GetMeteringPointResponseEnvelope_GeographicalCoordinate()
            {
                Latitude = 5.5m, LatitudeSpecified = true, Longitude = 8.9m, LongitudeSpecified = true
            },
            TNTID = "Test_TNTID"
        };

        public static GetMeteringPointResponseEnvelope_PC_PMP_MPCC CommercialCharacteristics =
            new GetMeteringPointResponseEnvelope_PC_PMP_MPCC()
            {
                BalanceResponsibleParty_Company =
                    new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_BalanceResponsibleParty_Company()
                        {ID = TestConstants.BalanceResponsible},
                BalanceSupplier_Company = new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_BalanceSupplier_Company()
                    {ID = TestConstants.BudgetSupplier},
                GridContractParty = new[]
                {
                    new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_GridContractParty()
                        {Initials = "I.V.", Surname = "QAUser", SurnamePrefix = "van"}
                },
                MeteringResponsibleParty_Company =
                    new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_MeteringResponsibleParty_Company()
                        {ID = "92834374394"}
            };
    }
}