﻿
namespace Autotests.Helper.DataProviders.FakeEdsn
{
    public static class TestConstants
    {
        public const string BuildingNr = "71";
        public const string ExBuildingNr = "B";
        public const string Zipcode = "1111QQ";
        public const string Country = "NL";
        public const string City = "Kharkiv";
        public const string Street = "Peremoga";
        public const string BagId = "345";
        public const string BagBuildingId = "123";

        public const string GridArea = "9782434235455";
        public const string GridOperator = "2438439739743";
        public const string GridOperatorFake = "0006859000017";
        public const string ProductionGridOperator = "8710000004442";
        public const string MarketSegment = "KVB";
        public const string DossierId = "11111111";
        public const string ExternalReference = "autotest";
        public const string ExternalReference_SWITCH = "SWITCH_autotest";
        public const string ExternalReference_EOS = "EOS_autotest";
        public const string ExternalReference_MOVEIN = "MOVEIN_autotest";
        public const string ExternalReference_MOVEOUT = "MOVEOUT_autotest";

        public const string GasProductType = "GAS";
        public const string ElkProductType = "ELK";

        public const string RejectionLevel = "Edsn";
        public const string RejectionMessage = "TestRejectionMessage";
        public const string MeteringPointStatus = "Active";
        public const string MeterReadingStatus = "Accepted";
        public const string MeasureUnitCode = "KWH";
        public const string AllocationMethod = "SMA";
        public const string ContractedCapacity = "454545453";
        public const string EnergyFlowDirection = "LVR";
        public const string CapTarCode = "1234567890123";
        public const string EnergyDeliveryStatus = "ACT";
        public const string InvoiceMonth = "200";
        public const string MeteringMethod = "NCE";
        public const string PhysicalCapacity = "G10";
        public const string PhysicalStatus = "IBD";
        public const string ProfileCategory = "G1A";
        public const string SubType = "subType";
        public const string KvkNumber = "12435687";

        public const string BalanceResponsible = "8934547493531";
        public const string MeteringResponsible = "6572819463425";
        public const string BudgetSupplier = "8714252018141";
        public const string NleSupplier = "8712423016965";
        public const string OtherPartyBalanceSupplier = "8714252005318";

        public const string EnergyMeterId = "545435";
        public const string TariffType = "N";
        public const string TechnicalCommunicationSm = "SMN";
        public const string MeterType = "SLM";

        public const string PAPEan = "111425200000007777";
        public const string SAPEan = "121425200000007777";

        public const string LocationDescription = "TestDescription";
        public const string MutationReason = "MoveIn";

        public const string Initials = "J. J.";
        public const string Surname = "Wick";
        public const string SurnamePrefix = "van";
        public static readonly DateTime BirthDate = DateTime.Today.AddYears(-30);

        public const string TestMarker = "Autotest_";
        public static string SalesChannel_UserName = $"{TestMarker}{Guid.NewGuid().ToString().Remove(4)}";
        public static string salesChannel_ExternalReference = $"{TestMarker}{Guid.NewGuid().ToString().Remove(5)}";
    }
}
