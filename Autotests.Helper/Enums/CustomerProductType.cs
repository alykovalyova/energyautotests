﻿namespace Autotests.Helper.Enums
{
    public enum CustomerProductType
    {
        Energy = 1,
        AllInOne = 2,
        Mobile = 3,
    }
}
