﻿namespace Autotests.Helper.Enums
{
    public enum IcarProcessStatus
    {
        New,
        Updated,
        FutureUpdate,
        Skipped,
        Duplicate,
        AlreadyInDb,
        ScheduledRetry,
        NotExistsInDb
    }
}
