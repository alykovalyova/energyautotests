﻿namespace Autotests.Helper.Enums
{
    public enum MessageType
    {
        Loss = 0,
        ChangeOfSupplier,
        MoveIn,
        MoveOut,
        Gain
    }
}