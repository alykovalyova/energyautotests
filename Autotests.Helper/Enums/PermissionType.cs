﻿namespace Autotests.Helper.Enums
{
    public enum PermissionType
    {
        FaceToFace = 1,
        ByPhone,
        OnlineChat,
        OnlineOther
    }
}
