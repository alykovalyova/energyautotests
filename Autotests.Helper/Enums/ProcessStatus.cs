﻿namespace Autotests.Helper.Enums
{
    public enum ProcessStatus
    {
        New,
        Accepted,
        FutureProcessing,
        Duplicate,
        ScheduledRetry 
    }
}