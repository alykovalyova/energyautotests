﻿namespace Autotests.Helper.Enums.SalesChannel
{
    public enum Channels
    {
        Retail = 1,
        Veiling,
        Prijsvergelijker,
        D2D,
        Wederverkoper,
        Website,
        Intern,
        Onbekend
    }
}