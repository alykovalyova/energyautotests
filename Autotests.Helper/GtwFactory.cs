using Nuts.EdsnGateway.Contract.PublishCustomerInfo.PublishCustomerApprovalKey;
using Nuts.EdsnGateway.Contract.PublishCustomerInfo.PublishCustomerContract;
using Nuts.EdsnSwitch.Contract.Rest.Model;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using Nuts.InterDom.Models.Enums;
using Nuts.MeteringPointInfoPhase.Contract;
using Nuts.MeteringPointOfferPhase.Contract;
using Nuts.MeteringPointSupplyPhase.Contract;
using System.Globalization;
using Autotests.Core;
using ConnectionDetail = Nuts.MeteringPointPrePhase.Contract.ConnectionDetail;
using GetMeteringPointsPreSwitchPhaseRequest = Nuts.MeteringPointPrePhase.Contract.GetMeteringPointsPreSwitchPhaseRequest;
using Autotests.Helper.Builders.MeteringPointPhases;
using Autotests.Helper.Builders.EdsnSwitch;
using Autotests.Helper.Enums;

namespace Autotests.Helper
{
    public static class GtwFactory
    {
        private const string OtherPartyFirstBalanceSupplier = "1982541287235";
        private const string IbanKey = "436";
        private const string BirthDayKey = "--12-12";
        private static readonly BuildDirector _buildDirector = new BuildDirector();

        public static GetMeteringPointsPreSwitchPhaseRequest CreateGetMeteringPointsPreSwitchPhaseRequest(
            string eanId, string externalRef, string supplierEan)
        {
            return new GetMeteringPointsPreSwitchPhaseRequest
            {
                Connections = new List<ConnectionDetail>
                {
                    new ConnectionDetail
                    {
                        EanId = eanId,
                        ExternalReference = externalRef,
                        SupplierEan = supplierEan
                    }
                }
            };
        }

        public static GetMeteringPointsOfferPhaseRequest CreateGetMeteringPointsOfferPhaseRequest(
            string birthDayKey = "--11-01", string ibanKey = "436", string postalCode = "1111QQ")
        {
            return new GetMeteringPointsOfferPhaseRequest
            {
                MeteringPointDetails = new List<MeteringPointDetail>
                {
                    new MeteringPointDetail
                    {
                        PermissionGrand = _buildDirector.Get<PermissionGrandBuilder>()
                            .SetBirthDayKey(birthDayKey)
                            .SetIBANKey(ibanKey)
                            .SetPermissionInfo(_buildDirector.Get<PermissionInfoBuilder>()
                                .SetHasOptedInByCheckbox(true)
                                .SetPermissionDateTime(DateTime.Now)
                                .SetPermissionPurpose(PermissionPurpose.CustomizedOffer.ToString())
                                .SetPermissionType(PermissionType.OnlineOther.ToString())
                                .SetRequestOrigin()
                                .SetResponsibleUser()
                                .SetPermissionAddress(_buildDirector.Get<PermissionAddressBuilder>()
                                    .SetDefaults()
                                    .SetPostalCode(postalCode).Build())
                                .SetPermissionCustomer(_buildDirector.Get<PermissionCustomerBuilder>()
                                    .SetDefaults()
                                    .SetGender(Gender.Male.ToString()).Build())
                                .Build())
                            .Build()
                    }
                }
            };
        }

        public static GetMeteringPointInfoRequest CreateGetMeteringPointInfoRequest(
            string eanId = null, int? buildingNr = null,
            string exBuildingNr = null, string zipCode = null)
        {
            return new GetMeteringPointInfoRequest
            {
                EanId = eanId,
                BuildingNr = buildingNr,
                ExBuildingNr = exBuildingNr,
                ZIPCode = zipCode
            };
        }

        public static PublishCustomerContractRequest CreatePublishCustomerContractRequest(string supplier, string reference, string eanId)
        {
            return new PublishCustomerContractRequest
            {
                BalanceSupplier = supplier,
                Ean = eanId,
                ExternalReference = reference
            };
        }

        public static PublishCustomerApprovalKeyRequest PublishCustomerApprovalKey(string externalReference, string eanId,
            string birthdayKey = BirthDayKey, string ibanKey = IbanKey)
        {
            return new PublishCustomerApprovalKeyRequest
            {
                BalanceSupplier = OtherPartyFirstBalanceSupplier,
                Ean = eanId,
                ExternalReference = externalReference,
                BirthDayKey = birthdayKey,
                IBANKey = ibanKey
            };
        }

        public static GetMeteringPointSupplyRequest CreateGetMeteringPointSupplyRequest(string eanId = null)
        {
            return new GetMeteringPointSupplyRequest { EanId = eanId };
        }

        public static ChangeOfSupplierRequest CreateChangeOfSupplierRequest(
            string ean, string balanceResponsibleId,
            string balanceSupplierId, string gridOperatorId,
            string referenceId, int buildingNr, string zipCode, string kvkNumber,
            DateTime mutationDate)
        {
            var request = _buildDirector.Get<ChangeOfSupplierDataBuilder>()
                .SetEan(ean)
                .SetBalanceResponsibleId(balanceResponsibleId)
                .SetBalanceSupplierId(balanceSupplierId)
                .SetGridContractParty(_buildDirector.Get<ChangeOfSupplierGridContractPartBuilder>()
                    .SetDefaultGridContractParty().Build())
                .SetGridOperatorId(gridOperatorId)
                .SetReferenceId(referenceId)
                .SetAddress(_buildDirector.Get<ChangeOfSupplierAddressBuilder>()
                    .SetDefaultAddress()
                    .SetBuildingNr(buildingNr)
                    .SetZipcode(zipCode).Build())
                .SetKvkNumber(kvkNumber)
                .SetMutationDate(mutationDate).Build();
            return new ChangeOfSupplierRequest
            {
                DataList = new List<ChangeOfSupplierData> { request }
            };
        }

        public static EndOfSupplyRequest CreateEndOfSupplyRequest(
            string ean, string balanceSupplierId, string gridOperatorId,
            string referenceId, DateTime mutationDate)
        {
            var request = _buildDirector.Get<EndOfSupplyDataBuilder>()
                .SetEan(ean)
                .SetReferenceId(referenceId)
                .SetBalanceSupplierId(balanceSupplierId)
                .SetGridOperatorId(gridOperatorId)
                .SetMutationDate(mutationDate).Build();

            return new EndOfSupplyRequest { DataList = new List<EndOfSupplyData> { request } };
        }

        public static MoveInRequest CreateMoveInRequest(
             string ean, string balanceResponsibleId, string balanceSupplierId, string gridOperatorId,
             string referenceId, string mutationDate, string kvkNumber, int buildingNr, string zipCode)
        {
            return new MoveInRequest()
            {
                DataList = new List<MoveInData>()
                {
                    new MoveInData()
                    {
                        Ean = ean,
                        BalanceResponsibleId = balanceResponsibleId,
                        BalanceSupplierId = balanceSupplierId,
                        GridOperatorId = gridOperatorId,
                        ReferenceId = referenceId,
                        MutationDate = Convert.ToDateTime(mutationDate, new CultureInfo("en-US")),
                        KvkNumber = kvkNumber,
                        Address = new Nuts.EdsnSwitch.Contract.Rest.Model.Address()
                        {
                            BuildingNr = buildingNr,
                            ZipCode = zipCode,
                            CityName = "Kharkiv",
                            Country = "NL",
                            StreetName = "Freedom"
                        }
                    }
                }
            };
        }
                              
        public static MoveOutRequest CreateMoveOutRequest(
            string ean, string balanceSupplierId, string gridOperatorId,
            string referenceId, DateTime mutationDate)
        {
            var request = _buildDirector.Get<MoveOutDataBuilder>()
                .SetEan(ean)
                .SetReferenceId(referenceId)
                .SetBalanceSupplierId(balanceSupplierId)
                .SetGridOperatorId(gridOperatorId)
                .SetMutationDate(mutationDate).Build();

            return new MoveOutRequest { DataList = new List<MoveOutData> { request } };
        }
    }
}