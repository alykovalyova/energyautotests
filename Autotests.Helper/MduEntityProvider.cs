﻿using Allure.NUnit.Attributes;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Nuts.FakeEdsn.Contracts;
using Autotests.Core;
using Autotests.Helper.Builders.FakeEdsn.CommercialCharateristics;
using Autotests.Helper.Builders.FakeEdsn.MasterData;
using Autotests.Helper.DataProviders.FakeEdsn;
using Repositories.ICAR.Sql.ICARModels;
using Repositories.MeteringPoint.Sql.MeteringPointModels;
using Repositories.MasterData.Sql.MasterDataModels;

namespace Autotests.Helper
{
    public class MduEntityProvider
    {
        protected BuildDirector BuildDirector { get; } = new BuildDirector();
        protected FakeEdsnAdminClient FakeEdsnAdminClient { get; } = new FakeEdsnAdminClient();

        protected DbHandler<MasterDataContext> MasterDataDb { get; } =
            new DbHandler<MasterDataContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.MasterDataDatabase.ToString()));

        protected DbHandler<ICARContext> IcarDb { get; } =
            new DbHandler<ICARContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.ICARDatabase.ToString()));

        protected DbHandler<MeteringPointContext> MeteringPointDb { get; } =
            new DbHandler<MeteringPointContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));

        protected RestSchedulerClient RestSchedulerClient { get; } = RestSchedulerClient.Instance;

        private MasterDataUpdateResponseEnvelope_MPAddressRequestType GetMduAddress(string buildingNr, string zipCode,
            string city)
        {
            return BuildDirector.Get<MasterDateUpdateAddressBuilder>()
                .SetBase()
                .SetBag(MasterDataUpdateEntitiesProvider.Address.BAG)
                .SetBuildingNr(buildingNr)
                .SetZipcode(zipCode)
                .SetCity(city)
                .Build();
        }

        private MasterDataUpdateResponseEnvelope_PC_PMP_MPCC GetCommercialChars(string balanceSupplierId)
        {
            return BuildDirector.Get<CommercialCharacteristicsBuilder>()
                .SetBase()
                .SetBalanceSupplier(balanceSupplierId)
                .Build();
        }

        private MasterDataUpdateResponseEnvelope_PC_PMP_PM GetMutationData(DateTime validFromDate)
        {
            return BuildDirector.Get<MasterDataPortalMutationBuilder>()
                .SetValidFromDate(validFromDate)
                .SetDossier(TestConstants.DossierId)
                .SetExternalReference(TestConstants.ExternalReference)
                .SetMutationReason(TestConstants.MutationReason)
                .Build();
        }

        private void SaveMasterDataUpdateToFake(string eanId, DateTime validFromDate,
            string balanceSupplierId = "8714252018141",
            string buildingNr = "71", string zipCode = "1111QQ", string city = "Kharkiv")
        {
            var address = GetMduAddress(buildingNr, zipCode, city);
            var commercialCharacteristics = GetCommercialChars(balanceSupplierId);
            var portalMutation = GetMutationData(validFromDate);
            var masterData = BuildDirector.Get<MasterDataUpdateBuilder>()
                .SetEanId(eanId)
                .SetAddress(address)
                .SetProductType(TestConstants.GasProductType)
                .SetPortalMutation(new[] {portalMutation})
                .SetEnergyMeter(MasterDataUpdateEntitiesProvider.EnergyMeter)
                .SetPhysicalCharacteristics(MasterDataUpdateEntitiesProvider.PhysicalCharacteristics)
                .SetCommercialCharacteristics(commercialCharacteristics)
                .SetGridarea(TestConstants.GridArea)
                .SetGridoperator(MasterDataUpdateEntitiesProvider.GridOperator)
                .SetMarketSegment(TestConstants.MarketSegment)
                .SetMeteringPointGroup(MasterDataUpdateEntitiesProvider.MeteringPointGroup)
                .Build();
            FakeEdsnAdminClient.SendMasterDataUpdate(new List<MasterDataUpdateResponseEnvelope_PC_PMP> {masterData});
        }

        [AllureStep("SendMduToFakeEdsn")]
        public void SendMduToFakeEdsn(string eanId, DateTime validFrom, string balanceSupplierId = "8714252018141",
            string buildingNr = "71", string zipCode = "1111QQ", string city = "Kharkiv")
        {
            SaveMasterDataUpdateToFake(eanId, validFrom, balanceSupplierId, buildingNr, zipCode, city);
            FakeEdsnAdminClient.ProcessMasterDataUpdateByExternalReference(TestConstants.ExternalReference,
                MasterDataUpdateStatus.Accepted);
            RestSchedulerClient.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetMasterDataUpdatesFake);
            Waiter.Wait(
                () => MasterDataDb.EntityIsInDb<Repositories.MasterData.Sql.MasterDataModels.MduHistory>(mdu => mdu.EanId == eanId),
                15);
            Waiter.Wait(() => IcarDb.EntityIsInDb<Repositories.ICAR.Sql.ICARModels.MduHistory>(mdu => mdu.EanId == eanId), 25);
        }
    }
}