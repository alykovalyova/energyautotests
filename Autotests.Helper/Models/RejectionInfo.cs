﻿namespace Autotests.Helper.Models
{
    public class RejectionInfo
    {
        public Rejection Rejection { get; set; }
    }

    public class Rejection
    {
        public List<RejectionData> Rejections { get; set; }
        public string RejectionMessage { get; set; }
    }

    public class RejectionData
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}