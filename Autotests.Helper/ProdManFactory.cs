﻿using System.Globalization;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Builders.Prodman;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using Nuts.ProdMan.Model.Contract.Transport;
using GasTariff = Nuts.ProdMan.Model.Contract.GasTariff;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;

namespace Autotests.Helper
{
    public static class ProdManFactory
    {
        private static readonly Random _random = new Random();
        private static readonly BuildDirector _buildDirector = new BuildDirector();
        
        public static GasTariff GetGasTariff(decimal retailPrice, int duration, GasRegion gasRegion,
            List<EnergyUsageProfileCode> profileCategories)
        {
            return duration == 12 || duration == 36 || duration == 60 
                ? _buildDirector.Get<GasTariffBuilder>()
                    .SetProfileCategories(profileCategories.ToArray()).SetGasRegion(gasRegion)
                    .SetRegionalSurcharge(0.03m).SetRetailPrice(retailPrice).Build() 
                : _buildDirector.Get<GasTariffBuilder>()
                    .SetProfileCategories(profileCategories.ToArray()).SetGasRegion(gasRegion)
                    .SetRegionalSurcharge(0.03m).SetRetailPrice(retailPrice).SetLoyalCustomerDiscount(-0.7m).Build();
        }

        public static List<GasTariff> GetGasTariffsSet(decimal retailPrice, int duration,
            List<EnergyUsageProfileCode> profileCategories)
        {
            return new List<GasTariff>
            {
                GetGasTariff(retailPrice, duration, GasRegion.Region1, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region2, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region3, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region4, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region5, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region6, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region7, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region8, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region9, profileCategories),
                GetGasTariff(retailPrice, duration, GasRegion.Region10, profileCategories)
            };
        }

        public static ProductPricesToSave.GasPrice GetGasPrice(
            string propositionName, int duration, decimal retailPrice,
            DateTime marketFromDate, List<EnergyUsageProfileCode> profileCategories)
        {
            return _buildDirector.Get<GasPriceBuilder>()
                .SetCashBack(_random.Next(200)).SetCode(CreateCodeForGasPrice(propositionName, marketFromDate, duration))
                .SetGasTariffs(GetGasTariffsSet(retailPrice, duration, profileCategories).ToArray()).SetId(new Guid())
                .SetMeasureUnit(MeasureUnitCode.MTQ).SetNpvPerCustomer(_random.Next(200))
                .SetStandingCharge(_random.Next(1, 7)).Build();
        }

        public static ProductPricesToSave.GasPrice GetGasPriceWithTariffsSet(
            string propositionName, int duration, DateTime marketFromDate, List<GasTariff> tariffsSet)
        {
            return _buildDirector.Get<GasPriceBuilder>()
                .SetCashBack(_random.Next(200)).SetCode(CreateCodeForGasPrice(propositionName, marketFromDate, duration))
                .SetGasTariffs(tariffsSet.ToArray()).SetId(new Guid())
                .SetMeasureUnit(MeasureUnitCode.MTQ).SetNpvPerCustomer(_random.Next(200))
                .SetStandingCharge(_random.Next(1, 7)).Build();
        }

        public static ElektraTariff GetElektraTariff(
            ElektraTariffType elektraTariffType, int duration, decimal retailPrice,
            List<EnergyUsageProfileCode> profileCategories, PriceDiscountType type, decimal value)
        {
            return duration == 12 || duration == 36 || duration == 60
                ? _buildDirector.Get<ElektraTariffBuilder>()
                    .SetProfileCategories(profileCategories.ToArray()).SetProductionPrice(_random.Next(20))
                    .SetRetailPrice(retailPrice).SetTariffType(elektraTariffType).SetPriceDiscount(type, value).Build()
                : _buildDirector.Get<ElektraTariffBuilder>()
                    .SetProfileCategories(profileCategories.ToArray()).SetProductionPrice(_random.Next(20))
                    .SetRetailPrice(retailPrice).SetTariffType(elektraTariffType).SetLoyalCustomerDiscount(-1.5m).Build();
        }

        public static List<ElektraTariff> GetElektraTariffSet(
            decimal retailPrice, int duration, List<EnergyUsageProfileCode> profileCategories,
            PriceDiscountType type, decimal value)
        {
            return new List<ElektraTariff>
            {
                GetElektraTariff(ElektraTariffType.Single, duration, retailPrice, profileCategories, type, value),
                GetElektraTariff(ElektraTariffType.High, duration, retailPrice, profileCategories, type, value),
                GetElektraTariff(ElektraTariffType.Low, duration, retailPrice, profileCategories, type, value),
            };
        }

        public static List<ElektraTariff> GetElektraTariffSetWithAllOptions(
            decimal retailPrice, int duration, List<EnergyUsageProfileCode> profileCategories,
            PriceDiscountType type, decimal value)
        {
            return new List<ElektraTariff>
            {
                GetElektraTariff(ElektraTariffType.Single, duration, retailPrice, profileCategories, type, value),
                GetElektraTariff(ElektraTariffType.High, duration, retailPrice, profileCategories, type, value),
                GetElektraTariff(ElektraTariffType.Low, duration, retailPrice, profileCategories, type, value),
                GetElektraTariff(ElektraTariffType.Peak, duration, retailPrice, profileCategories, type, value),
                GetElektraTariff(ElektraTariffType.OffPeak, duration, retailPrice, profileCategories, type, value),
            };
        }

        public static ProductPricesToSave.ElectricityPrice GetElectricityPrice(
            string propositionName, int duration, DateTime date, decimal retailPrice,
            List<EnergyUsageProfileCode> profileCategories, PriceDiscountType type, decimal value)
        {
            return _buildDirector.Get<ElectricityPriceBuilder>()
                .SetCashBack(_random.Next(200)).SetCode(CreateCodeForElkPrice(propositionName, date, duration))
                .SetElektraTariffs(GetElektraTariffSet(retailPrice, duration, profileCategories, type, value))
                .SetId(new Guid()).SetMeasureUnit(MeasureUnitCode.KWH).SetNpvPerCustomer(_random.Next(300))
                .SetStandingCharge(_random.Next(1, 7)).Build();
        }

        public static ProductPricesToSave.ElectricityPrice GetElectricityPriceWithAllTariffOptions(
            string propositionName, int duration, DateTime date, decimal retailPrice,
            List<EnergyUsageProfileCode> profileCategories, PriceDiscountType type, decimal value)
        {
            return _buildDirector.Get<ElectricityPriceBuilder>()
                .SetCashBack(_random.Next(200)).SetCode(CreateCodeForElkPrice(propositionName, date, duration))
                .SetElektraTariffs(GetElektraTariffSetWithAllOptions(retailPrice, duration, profileCategories, type, value))
                .SetId(new Guid()).SetMeasureUnit(MeasureUnitCode.KWH).SetNpvPerCustomer(_random.Next(300))
                .SetStandingCharge(_random.Next(1, 7)).Build();
        }

        public static ProductPricesToSave.ElectricityPrice GetElectricityPriceWithTariffsSet(
            string propositionName, int duration, DateTime date, List<ElektraTariff> tariffSet)
        {
            return _buildDirector.Get<ElectricityPriceBuilder>()
                .SetCashBack(_random.Next(200)).SetCode(CreateCodeForElkPrice(propositionName, date, duration))
                .SetElektraTariffs(tariffSet).SetId(new Guid()).SetMeasureUnit(MeasureUnitCode.KWH)
                .SetNpvPerCustomer(_random.Next(300)).SetStandingCharge(_random.Next(1, 7)).Build();
        }

        public static string CreateCodeForElkPrice(string propositionName, DateTime date, int duration)
        {
            string formattedCode;
            var cultureInfo = new CultureInfo("nl-NL");
            var formattedDate = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(date.ToString("dMMM", cultureInfo));

            switch (duration)
            {
                case 12:
                    formattedCode = $"{propositionName}.BudgetEnergie.E.1 Jaar.{formattedDate}.{date.Year}";
                    break;
                case 36:
                    formattedCode = $"{propositionName}.BudgetEnergie.E.3 Jaar.{formattedDate}.{date.Year}";
                    break;
                case 60:
                    formattedCode = $"{propositionName}.BudgetEnergie.E.5 Jaar.{formattedDate}.{date.Year}";
                    break;
                default:
                    formattedCode = $"{propositionName}.BudgetEnergie.E.Variable.{formattedDate}.{date.Year}";
                    break;
            }

            return formattedCode;
        }

        public static string CreateCodeForGasPrice(string propositionName, DateTime date, int duration)
        {
            string formattedCode;
            var cultureInfo = new CultureInfo("nl-NL");
            var formattedDate = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(date.ToString("dMMM", cultureInfo));

            switch (duration)
            {
                case 12:
                    formattedCode = $"{propositionName}.BudgetEnergie.G.1 Jaar.{formattedDate}.{date.Year}";
                    break;
                case 36:
                    formattedCode = $"{propositionName}.BudgetEnergie.G.3 Jaar.{formattedDate}.{date.Year}";
                    break;
                case 60:
                    formattedCode = $"{propositionName}.BudgetEnergie.G.5 Jaar.{formattedDate}.{date.Year}";
                    break;
                default:
                    formattedCode = $"{propositionName}.BudgetEnergie.G.Variable.{formattedDate}.{date.Year}";
                    break;
            }

            return formattedCode;
        }

        public static DeletePropositionRequest CreateDeletePropositionRequest(Guid propositionId)
        {
            return new DeletePropositionRequest { PropositionId = propositionId };
        }

        public static GetCashBackOptionsRequest CreateGetCashBackOptionsRequest(Guid? id = null, string name = null)
        {
            return new GetCashBackOptionsRequest { PropositionId = id, PropositionName = name };
        }

        public static EditActivePropositionRequest CreateEditActivePropositionRequest(
            Repositories.Prodman.Sql.ProdManModels.Proposition proposition, Guid? id = null, 
            string description = null, bool? isSalesEligible = null, 
            DateTime? marketTo = null, decimal? reimburseFine = null)
        {
            return new EditActivePropositionRequest
            {
                PropositionId = id ?? proposition.Id,
                Description = description ?? proposition.Description,
                IsSalesEligible = isSalesEligible ?? proposition.IsSalesEligible,
                MarketTo = marketTo ?? proposition.MarketTo.GetValueOrDefault(),
                ReimburseFine = reimburseFine ?? proposition.ReimburseFine,
                MargeTypeN = MargeTypeN.Business,
                MargeTypeR = MargeTypeR.Passing
            };
        }

        public static GetPropositionInfoRequest CreateGetPropositionInfoRequest(Guid id)
        {
            return new GetPropositionInfoRequest { PropositionId = id };
        }

        public static GetPropositionInfosRequest CreateGetPropositionInfosRequest(params Guid[] ids)
        {
            return new GetPropositionInfosRequest
            {
                PropositionIds = ids.ToList()
            };
        }

        public static GetPropositionsRequest CreateGetPropositionsRequest(List<Guid> ids)
        {
            return new GetPropositionsRequest { PropositionIds = ids };
        }

        public static T CreateSearchRequest<T>(
            TypeOfClient? type = null, Labels? label = null, 
            List<int> durations = null, bool? isEligibleAsRenewal = null, bool? isSalesEligible = null, 
            Status? status = null, DateTime? from = null, DateTime? to = null) where T : class, new()
        {
            var searchCriteria = _buildDirector.Get<SearchCriteriaBuilder>()
                .SetClientType(type).SetLabelCode(label).SetDurations(durations)
                .SetIsEligibleAsRenewal(isEligibleAsRenewal).SetIsSalesEligible(isSalesEligible)
                .SetStatus(status).SetFrom(from).SetTo(to).Build();
            if (typeof(T) == typeof(SearchProductPricesRequest))
                return new SearchProductPricesRequest { SearchInfo = searchCriteria } as T;
            if (typeof(T) == typeof(SearchPropositionsRequest))
                return new SearchPropositionsRequest { SearchInfo = searchCriteria } as T;
            return default(T);
        }

        public static SearchPropositionInfosWithMultpleStatusesRequest CreateSearchPropositionInfosWithMultipleStatusesRequest(
            TypeOfClient? type = null, Labels? label = null,
            List<int> durations = null, bool? isEligibleAsRenewal = null, bool? isSalesEligible = null,
            List<Status> statuses = null, DateTime? from = null, DateTime? to = null)
        {
            var searchInfo = _buildDirector.Get<SearchCriteriaWithMultipleStatusesBuilder>()
                .SetClientType(type).SetLabelCode(label).SetDurations(durations)
                .SetIsEligibleAsRenewal(isEligibleAsRenewal).SetIsSalesEligible(isSalesEligible)
                .SetStatuses(statuses).SetFrom(from).SetTo(to).Build();
            return new SearchPropositionInfosWithMultpleStatusesRequest { SearchInfo = searchInfo };
        }

        public static ReactivatePropositionRequest CreateReactivatePropositionRequest(
            string comment, Guid id, DateTime marketTo, string userName = null)
        {
            return new ReactivatePropositionRequest
            {
                Comment = comment,
                PropositionId = id,
                MarketTo = marketTo,
                UserName = userName
            };
        }

        public static AddPropositionRequest CreateAddPropositionRequest(
            List<ProductPricesToSave.ElectricityPrice> elkPrice, List<ProductPricesToSave.GasPrice> gasPrice,
            List<TypeOfClient> clientType, int duration, List<Nuts.ProdMan.Core.Enums.EnergySource> sourceOfEnergy, DateTime marketFromDate,
            DateTime marketToDate, string nameProposition, Labels label = Labels.BudgetEnergie, IncentiveType incentiveType = IncentiveType.Voucher)
        {
            return new AddPropositionRequest
            {
                PropositionInfo = _buildDirector.Get<PropositionBuilder>().SetDefaults()
                    .SetElectricityPrices(elkPrice).SetGasPrices(gasPrice)
                    .SetClientType(clientType.ToArray()).SetDuration(duration)
                    .SetEnergySources(sourceOfEnergy.ToArray())
                    .SetMarketFrom(marketFromDate).SetMarketTo(marketToDate).SetName(nameProposition).SetLabel(label)
                    .InCase(incentiveType.Equals(IncentiveType.CashBack),
                        b => b.SetCashBack(200, IncentivePaymentType.DirectlyAfterSignup))
                    .InCase(incentiveType.Equals(IncentiveType.Voucher),
                        b => b.SetVoucher(200, IncentivePaymentType.DirectlyAfterSignup)).Build()
            };
        }

        public static UpdatePropositionStatusRequest CreateUpdatePropositionStatusRequest(
            Status status, Guid propositionId, string comment = null, string userName = null)
        {
            return new UpdatePropositionStatusRequest
            {
                Comment = comment,
                NewStatus = status,
                PropositionId = propositionId,
                UserName = userName
            };
        }
    }
}