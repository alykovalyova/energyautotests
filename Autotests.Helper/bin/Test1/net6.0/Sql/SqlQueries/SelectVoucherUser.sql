﻿SELECT TOP (250) cv.ContractID as ContractId, cpk.ContactPersonNumber, v.Id as VoucherId
FROM [EnerFreeDB].[dbo].[ContractVoucher] cv
join [EnerFreeDB].[dbo].[Contracten] c on c.ContractID = cv.ContractID
join [MMVDB].[dbo].[Voucher] v on cv.VoucherID = v.Id
join [Customer].[dbo].[ProductCustomer] pc on pc.ProductCustomerId = c.ContractantID
join [Customer].[dbo].[CustomerContactPerson] ccp on ccp.NutsHomeCustomerId = pc.NutsHomeCustomerId
join [Customer].[dbo].[ContactPersonKey] cpk on cpk.Id = ccp.ContactPersonKeyId
where v.CanceledOn is null
order by v.CreatedOn desc