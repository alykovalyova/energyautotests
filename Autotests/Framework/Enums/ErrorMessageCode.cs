﻿namespace Autotests.Framework.Enums
{
    public enum ErrorMessageCode
    {
        ObjectNotFoundException,
        InputParameterValidationException,
        Unknown
    }
}