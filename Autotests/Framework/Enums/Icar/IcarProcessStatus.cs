﻿namespace Autotests.Framework.Enums.Icar
{
    internal enum IcarProcessStatus
    {
        New,
        Updated,
        FutureUpdate,
        Skipped,
        Duplicate,
        AlreadyInDb,
        ScheduledRetry,
        NotExistsInDb
    }
}
