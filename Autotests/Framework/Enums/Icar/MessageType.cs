﻿namespace Autotests.Framework.Enums.Icar
{
    internal enum MessageType
    {
        Loss = 0,
        ChangeOfSupplier,
        MoveIn,
        MoveOut,
        Gain
    }
}