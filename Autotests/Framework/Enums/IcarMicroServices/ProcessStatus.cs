﻿namespace Autotests.Framework.Enums.IcarMicroServices
{
    internal enum ProcessStatus
    {
        New,
        Accepted,
        FutureProcessing,
        Duplicate,
        ScheduledRetry 
    }
}