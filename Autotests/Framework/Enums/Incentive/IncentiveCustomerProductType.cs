﻿namespace Autotests.Framework.Enums.Incentive
{
    public enum IncentiveCustomerProductType
    {
        Energy = 1,
        Mobile,
        AllInOne,
        Invalid
    }
}
