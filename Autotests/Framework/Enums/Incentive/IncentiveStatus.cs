﻿namespace Autotests.Framework.Enums
{
    public enum IncentiveStatus
    {
        New=1,
        EmailSent,
        GiftRegistered,
        GiftSent,
        GiftCancelled,
        ContractCancelled
    }
}
