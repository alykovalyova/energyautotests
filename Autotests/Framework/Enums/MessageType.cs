﻿namespace Autotests.Framework.Enums
{
    public enum MessageType
    {
        NotificationMessage,
        DataMessage,
        Unknown
    }
}
