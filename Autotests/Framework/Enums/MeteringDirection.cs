﻿namespace Autotests.Framework.Enums
{
    internal enum MeteringDirection
    {
        CMB = 1,
        LVR,
        TLV
    }
}