﻿namespace Autotests.Framework.Enums
{
    public enum OfferBundleDicKey
    {
        WithBudgetEnergyLabel_5Years_Private,
        WithBudgetEnergyLabel_3Years_Private,
        WithBudgetEnergyLabel_1Years_Private,
        WithNleLabel_5Years_Private,
        WithNleLabel_3Years_Private,
        WithNleLabel_1Years_Private,
        WithBudgetEnergyLabel_5Years_Business,
        WithBudgetEnergyLabel_3Years_Business,
        WithBudgetEnergyLabel_1Years_Business,
        WithNleLabel_5Years_Business,
        WithNleLabel_3Years_Business,
        WithNleLabel_1Years_Business
    }
}