﻿namespace Autotests.Framework.Enums
{
    public enum Strategy
    {
        ai1_resetpassword,
        reset_password,
        mobile_resetpassword,
        renew_offer_campaign,
        renew_offer_custom
    }
}
