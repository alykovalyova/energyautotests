﻿using System;
using System.IO;

namespace Autotests.Framework.Helpers
{
    internal static class ConfigHelper
    {
        private const string BaseDir = "Framework/Files/";

        internal static string GetFilePath(string path) => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BaseDir + path);
    }
}