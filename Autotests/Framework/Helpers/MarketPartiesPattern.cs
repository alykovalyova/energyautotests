﻿using System;

namespace Autotests.Framework.Helpers
{
    public class MarketPartiesPattern : IComparable<Nuts.MarketParty.Contract.MarketParty[]>
    {
        public Nuts.MarketParty.Contract.MarketParty[] MarketParties { get; set; }

        public int CompareTo(Nuts.MarketParty.Contract.MarketParty[] other)
        {
            var res = -1;
            for (var i = 0; i < MarketParties.Length; i++)
            {
                if (other[i].Name == null) continue;
                if (other[i].MarketPartyEan.Equals(MarketParties[i].MarketPartyEan)
                    && other[i].Name.Equals(MarketParties[i].Name)
                    && other[i].Role.Equals(MarketParties[i].Role))
                {
                    res = 0;
                }
                else
                {
                    res = -1;
                }
            }

            return res;
        }
    }
}