﻿using Autotests.Clients;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.Reseller.Contract;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace Autotests.Framework.Helpers
{
    public static class ResellerHelper
    {
        private static readonly string _apiUrl = ConfigHandler.Instance.ApiUrls.Reseller;
        public static HttpClient ResellerHttpClient = new BaseHttpClient(ConfigHandler.Instance.ApiUrls.Reseller);
        private static readonly NutsHttpClient _client = new NutsHttpClient(ResellerHttpClient);
        public static string ResellerName = $"Autotest_Reseller_Name{RandomDataProvider.GetRandomNumbersString(8)}";
        private const string SalesChannelName = "Autotest_Channel_Name";

        public static Guid AddReseller(string resellerName, int lastSalesChannelId, string offerBundleId,
            string offerBundleName, bool isActive)
        {
            var request = new UpsertResellerRequest
            {
                Reseller = new Reseller
                {
                    Name = resellerName,
                    IsActive = isActive,
                    SalesChannels = new List<SalesChannel>
                    {
                        new SalesChannel
                        {
                            SalesChannelId = lastSalesChannelId,
                            ChannelName = SalesChannelName,
                            Label = Labels.BudgetEnergie.ToString(),
                            OfferBundles =  new List<OfferBundle>
                            {
                                new OfferBundle
                                {
                                    OfferBundleId = offerBundleId,
                                    Name = offerBundleName
                                }
                            }
                        }
                    }
                }
            };

            var response = _client.MakeRequest<UpsertResellerRequest, UpsertResellerResponse>(request);
            Assert.That(response.Header.StatusCode, Is.EqualTo(HttpStatusCode.OK), response.Header.Message);

            return response.Data.ResellerCode;
        }
    }
}