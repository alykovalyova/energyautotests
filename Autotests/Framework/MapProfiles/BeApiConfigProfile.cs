﻿using AutoMapper;
using Nuts.Consumption.Model.Contract;

namespace Autotests.Framework.MapProfiles
{
    public class BeApiConfigProfile : Profile
    {
        public BeApiConfigProfile()
        {
            CreateMap<Nuts.ICAR.Contract.MeteringPointService.MeteringPoint.Register, Reading>()
                .ForMember(dest => dest.MeteringDirection, opt => opt.MapFrom(src => src.MeteringDirection))
                .ForMember(dest => dest.TariffType, opt => opt.MapFrom(src => src.TariffType))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => 0));
        }
    }
}