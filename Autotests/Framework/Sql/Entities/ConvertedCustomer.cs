﻿using System;
using System.Data;

namespace Autotests.Framework.Sql.Entities
{
    public class ConvertedCustomer : Data<ConvertedCustomer>, IData
    {
        public ConvertedCustomer(IDataRecord reader) : base(reader)
        {
        }

        public long RelationId { get; set; }
        public decimal Discount { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal NetPrice { get; set; }
        public decimal Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PolicyNumber { get; set; }
        public string ProductSpecification { get; set; }
        public bool IsActive { get; set; }
        public string ProductType { get; set; }
    }
}
