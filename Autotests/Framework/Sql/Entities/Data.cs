﻿using System;
using System.Data;

namespace Autotests.Framework.Sql.Entities
{
    public class Data<T> where T: IData
    {
        private readonly Type _dataType = typeof(T);

        public Data(IDataRecord reader)
        {
            var props = _dataType.GetProperties();
            foreach (var propertyInfo in props)
            {
                try
                {
                    object value = null;
                    try
                    {
                        value = reader[propertyInfo.Name];
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                    if (value == null)
                    {
                        continue;
                    }
                    propertyInfo.SetValue(this, reader[propertyInfo.Name]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        }
    }
}