﻿using System.Data;

namespace Autotests.Framework.Sql.Entities
{
    internal class EanInfoData : Data<EanInfoData>, IData
    {
        public EanInfoData(IDataRecord reader) : base(reader)
        {
        }

        public int ContractId { get; set; }
        public string EanId { get; set; }
    }
}