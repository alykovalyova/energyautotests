﻿using System.Data;

namespace Autotests.Framework.Sql.Entities
{
    internal class EnerFreeEan : Data<EnerFreeEan>, IData
    {
        public EnerFreeEan(IDataRecord reader) : base(reader)
        {
        }

        public string EanId { get; set; }
        public int ProductType { get; set; }
    }
}
