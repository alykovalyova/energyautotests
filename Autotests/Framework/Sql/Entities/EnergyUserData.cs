﻿using System.Data;

namespace Autotests.Framework.Sql.Entities
{
    public class EnergyUserData : Data<EnergyUserData>, IData
    {
        public EnergyUserData(IDataRecord reader) : base(reader)
        {
        }

        public int ContractId { get; set; }
        public string UserName { get; set; }
        public int ContactPersonNumber { get; set; }
        public int VoucherId { get; set; }
    }
}