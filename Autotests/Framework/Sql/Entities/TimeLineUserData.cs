﻿using System.Data;

namespace Autotests.Framework.Sql.Entities
{
    public class TimeLineUserData : Data<TimeLineUserData>, IData
    {
        public TimeLineUserData(IDataRecord reader) : base(reader)
        {
        }

        public string UserName { get; set; }
        public int ContactPersonNumber { get; set; }
    }
}