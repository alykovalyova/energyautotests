﻿using Autotests.Core.Handlers;
using Microsoft.Data.SqlClient;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Autotests.Framework.Sql
{
    internal class SqlQueryExecutor
    {
        private string _connectionString;
        private int _commandTimeout = 5000;
        private static readonly Lazy<SqlQueryExecutor> _lazy = new Lazy<SqlQueryExecutor>(() => new SqlQueryExecutor());
        internal static SqlQueryExecutor Instance => _lazy.Value;

        private SqlQueryExecutor()
        {
        }

        internal SqlQueryExecutor ConnectTo(string dbName)
        {
            Instance._connectionString = ConfigHandler.Instance.GetConnectionString(dbName);
            return Instance;
        }

        internal SqlQueryExecutor SetTimeout(int timeout)
        {
            Instance._commandTimeout = timeout;
            return Instance;
        }

        public IEnumerable<TEntity> ExecuteSqlQuery<TEntity>(string sqlQueryFileName, int take = 10, string condition = null) where TEntity : class
        {
            var queryFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Framework\Sql\SqlQueries", sqlQueryFileName + ".sql");
            var queryResults = new List<TEntity>();
            if (string.IsNullOrEmpty(queryFilePath) || string.IsNullOrWhiteSpace(queryFilePath))
                throw new ArgumentNullException(nameof(queryFilePath), "Path to query file is empty or null.");
            if (!File.Exists(queryFilePath))
                throw new FileNotFoundException("Query script file hasn't been found.");
            if (string.IsNullOrEmpty(_connectionString))
                throw new Exception("Set Connection string first.");

            using (var connection = new SqlConnection(_connectionString))
            {
                var script = File.ReadAllText(queryFilePath);

                if (condition != null && script.Contains(nameof(condition)))
                {
                    script = File.ReadAllText(queryFilePath).Replace(nameof(condition), condition);
                }

                var command = new SqlCommand(script, connection) { CommandTimeout = _commandTimeout };
                connection.Open();
                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read() && take > 0)
                        {
                            var queryRes = (TEntity)Activator.CreateInstance(typeof(TEntity), reader);
                            queryResults.Add(queryRes);
                            take--;
                        }

                        if (!queryResults.Any())
                            TestContext.Out.WriteLine(
                                $"Test data for {Path.GetFileName(queryFilePath)} can't be found.");

                        return queryResults;
                    }
                }
                catch (Exception e)
                {
                    TestContext.Out.WriteLine(e.InnerException?.InnerException?.Message);
                    connection.Close();
                    throw;
                }

            }
        }
    }
}