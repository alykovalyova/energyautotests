﻿using Microsoft.EntityFrameworkCore;

namespace Repositories.BAG.Sql.BAGModels
{
    public partial class BAGContext : DbContext
    {
        private readonly string _connectionString;

        public BAGContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public BAGContext(DbContextOptions<BAGContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BagDatum> BagData { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<BagDatum>(entity =>
            {
                entity.HasKey(e => e.Perceelid)
                    .HasName("PK__bag_data__C7B7434B235AB04E");

                entity.ToTable("bag_data", "dbo");

                entity.HasIndex(e => new { e.Huisnr, e.HuisnrBagLetter, e.HuisnrBagToevoeging, e.Wijkcode, e.Lettercombinatie }, "IX_bag_data_huisnr_toevoegingen");

                entity.HasIndex(e => new { e.Huisnr, e.Wijkcode }, "IX_bag_data_huisnr_wijkcode");

                entity.Property(e => e.Perceelid)
                    .ValueGeneratedNever()
                    .HasColumnName("perceelid");

                entity.Property(e => e.BagAdresseerbaarobjectid)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("bag_adresseerbaarobjectid");

                entity.Property(e => e.BagNummeraanduidingid)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("bag_nummeraanduidingid");

                entity.Property(e => e.Breedtegraad)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("breedtegraad");

                entity.Property(e => e.Cebucocode).HasColumnName("cebucocode");

                entity.Property(e => e.Gebruiksdoel)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("gebruiksdoel");

                entity.Property(e => e.Gemeentecode).HasColumnName("gemeentecode");

                entity.Property(e => e.Gemeenteid).HasColumnName("gemeenteid");

                entity.Property(e => e.Gemeentenaam)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("gemeentenaam");

                entity.Property(e => e.GemeentenaamNen)
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("gemeentenaam_nen");

                entity.Property(e => e.Huisnr).HasColumnName("huisnr");

                entity.Property(e => e.HuisnrBagLetter)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("huisnr_bag_letter");

                entity.Property(e => e.HuisnrBagToevoeging)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("huisnr_bag_toevoeging");

                entity.Property(e => e.HuisnrTm).HasColumnName("huisnr_tm");

                entity.Property(e => e.HuisnrVan).HasColumnName("huisnr_van");

                entity.Property(e => e.Lengtegraad)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("lengtegraad");

                entity.Property(e => e.Lettercombinatie)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("lettercombinatie");

                entity.Property(e => e.Oppervlakte).HasColumnName("oppervlakte");

                entity.Property(e => e.Perceeltype)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("perceeltype");

                entity.Property(e => e.Plaatscode).HasColumnName("plaatscode");

                entity.Property(e => e.Plaatsid).HasColumnName("plaatsid");

                entity.Property(e => e.Plaatsnaam)
                    .IsRequired()
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("plaatsnaam");

                entity.Property(e => e.PlaatsnaamNen)
                    .IsRequired()
                    .HasMaxLength(35)
                    .IsUnicode(false)
                    .HasColumnName("plaatsnaam_nen");

                entity.Property(e => e.Provinciecode)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("provinciecode");

                entity.Property(e => e.Provincienaam)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("provincienaam");

                entity.Property(e => e.Rdx)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("rdx");

                entity.Property(e => e.Rdy)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("rdy");

                entity.Property(e => e.Reeksid).HasColumnName("reeksid");

                entity.Property(e => e.Reeksindicatie).HasColumnName("reeksindicatie");

                entity.Property(e => e.Straatid).HasColumnName("straatid");

                entity.Property(e => e.Straatnaam)
                    .HasMaxLength(75)
                    .IsUnicode(false)
                    .HasColumnName("straatnaam");

                entity.Property(e => e.StraatnaamNen)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("straatnaam_nen");

                entity.Property(e => e.Wijkcode).HasColumnName("wijkcode");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
