﻿using System;
using System.Collections.Generic;

namespace Repositories.CommercialCharacteristics.Sql.CcuModels
{
    public partial class CcuHistory
    {
        public CcuHistory()
        {
            CcuStatusHistories = new HashSet<CcuStatusHistory>();
        }

        public int Id { get; set; }
        public string? EanId { get; set; }
        public short? BalanceSupplierId { get; set; }
        public short? OldBalanceSupplierId { get; set; }
        public short? BalanceSupplierResponsibleId { get; set; }
        public string? ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public string? DossierId { get; set; }
        public short StatusId { get; set; }
        public string? LastComment { get; set; }
        public Guid XmlHeaderMessageId { get; set; }
        public DateTime XmlHeaderCreationTs { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public short MessageSourceId { get; set; }
        public short MutationReasonId { get; set; }

        public virtual MarketParty? BalanceSupplier { get; set; }
        public virtual MarketParty? BalanceSupplierResponsible { get; set; }
        public virtual EnumMessageSource MessageSource { get; set; } = null!;
        public virtual EnumMutationReason MutationReason { get; set; } = null!;
        public virtual MarketParty? OldBalanceSupplier { get; set; }
        public virtual EnumProcessStatus Status { get; set; } = null!;
        public virtual ICollection<CcuStatusHistory> CcuStatusHistories { get; set; }
    }
}
