﻿using System;
using System.Collections.Generic;

namespace Repositories.CommercialCharacteristics.Sql.CcuModels
{
    public partial class CcuStatusHistory
    {
        public int Id { get; set; }
        public int CcuHistoryId { get; set; }
        public short ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string? Comment { get; set; }

        public virtual CcuHistory CcuHistory { get; set; } = null!;
        public virtual EnumProcessStatus ProcessStatus { get; set; } = null!;
    }
}
