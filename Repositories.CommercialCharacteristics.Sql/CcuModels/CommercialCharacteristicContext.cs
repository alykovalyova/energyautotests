﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.CommercialCharacteristics.Sql.CcuModels
{
    public partial class CommercialCharacteristicContext : DbContext
    {
        private readonly string _connectionString;

        public CommercialCharacteristicContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CommercialCharacteristicContext(DbContextOptions<CommercialCharacteristicContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CcuHistory> CcuHistories { get; set; } = null!;
        public virtual DbSet<CcuStatusHistory> CcuStatusHistories { get; set; } = null!;
        public virtual DbSet<EnumMessageSource> EnumMessageSources { get; set; } = null!;
        public virtual DbSet<EnumMutationReason> EnumMutationReasons { get; set; } = null!;
        public virtual DbSet<EnumProcessStatus> EnumProcessStatuses { get; set; } = null!;
        public virtual DbSet<MarketParty> MarketParties { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CcuHistory>(entity =>
            {
                entity.ToTable("CcuHistory", "dbo");

                entity.HasIndex(e => e.BalanceSupplierId, "IX_CcuHistory_BalanceSupplierId");

                entity.HasIndex(e => e.BalanceSupplierResponsibleId, "IX_CcuHistory_BalanceSupplierResponsibleId");

                entity.HasIndex(e => e.MessageSourceId, "IX_CcuHistory_MessageSourceId");

                entity.HasIndex(e => e.MutationReasonId, "IX_CcuHistory_MutationReasonId");

                entity.HasIndex(e => e.OldBalanceSupplierId, "IX_CcuHistory_OldBalanceSupplierId");

                entity.HasIndex(e => e.DossierId, "IX_DossierId");

                entity.HasIndex(e => e.EanId, "IX_EanId");

                entity.HasIndex(e => e.StatusId, "IX_StatusId");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.CcuHistoryBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.BalanceSupplierResponsible)
                    .WithMany(p => p.CcuHistoryBalanceSupplierResponsibles)
                    .HasForeignKey(d => d.BalanceSupplierResponsibleId);

                entity.HasOne(d => d.MessageSource)
                    .WithMany(p => p.CcuHistories)
                    .HasForeignKey(d => d.MessageSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.CcuHistories)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.OldBalanceSupplier)
                    .WithMany(p => p.CcuHistoryOldBalanceSuppliers)
                    .HasForeignKey(d => d.OldBalanceSupplierId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CcuHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CcuStatusHistory>(entity =>
            {
                entity.ToTable("CcuStatusHistory", "dbo");

                entity.HasIndex(e => e.CcuHistoryId, "IX_CcuHistoryId");

                entity.HasIndex(e => e.ProcessStatusId, "IX_CcuStatusHistory_ProcessStatusId");

                entity.HasOne(d => d.CcuHistory)
                    .WithMany(p => p.CcuStatusHistories)
                    .HasForeignKey(d => d.CcuHistoryId);

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.CcuStatusHistories)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EnumMessageSource>(entity =>
            {
                entity.ToTable("EnumMessageSource", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumMessageSource_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMutationReason>(entity =>
            {
                entity.ToTable("EnumMutationReason", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumMutationReason_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumProcessStatus>(entity =>
            {
                entity.ToTable("EnumProcessStatus", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumProcessStatus_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean, "IX_MarketParty_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
