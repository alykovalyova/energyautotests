﻿using System;
using System.Collections.Generic;

namespace Repositories.CommercialCharacteristics.Sql.CcuModels
{
    public partial class EnumMutationReason
    {
        public EnumMutationReason()
        {
            CcuHistories = new HashSet<CcuHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<CcuHistory> CcuHistories { get; set; }
    }
}
