﻿using System;
using System.Collections.Generic;

namespace Repositories.CommercialCharacteristics.Sql.CcuModels
{
    public partial class EnumProcessStatus
    {
        public EnumProcessStatus()
        {
            CcuHistories = new HashSet<CcuHistory>();
            CcuStatusHistories = new HashSet<CcuStatusHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<CcuHistory> CcuHistories { get; set; }
        public virtual ICollection<CcuStatusHistory> CcuStatusHistories { get; set; }
    }
}
