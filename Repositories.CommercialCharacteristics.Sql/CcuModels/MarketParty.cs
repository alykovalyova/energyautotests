﻿using System;
using System.Collections.Generic;

namespace Repositories.CommercialCharacteristics.Sql.CcuModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            CcuHistoryBalanceSupplierResponsibles = new HashSet<CcuHistory>();
            CcuHistoryBalanceSuppliers = new HashSet<CcuHistory>();
            CcuHistoryOldBalanceSuppliers = new HashSet<CcuHistory>();
        }

        public short Id { get; set; }
        public string Ean { get; set; } = null!;

        public virtual ICollection<CcuHistory> CcuHistoryBalanceSupplierResponsibles { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryBalanceSuppliers { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryOldBalanceSuppliers { get; set; }
    }
}
