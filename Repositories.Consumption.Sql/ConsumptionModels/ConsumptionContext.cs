﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class ConsumptionContext : DbContext
    {
        private readonly string _connectionString;

        public ConsumptionContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ConsumptionContext(DbContextOptions<ConsumptionContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryAditionalWindowStatus> DictionaryAditionalWindowStatuses { get; set; } = null!;
        public virtual DbSet<DictionaryBusinessEventType> DictionaryBusinessEventTypes { get; set; } = null!;
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevels { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCodes { get; set; } = null!;
        public virtual DbSet<DictionaryInitiatedParty> DictionaryInitiatedParties { get; set; } = null!;
        public virtual DbSet<DictionaryMarketEvent> DictionaryMarketEvents { get; set; } = null!;
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCodes { get; set; } = null!;
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatuses { get; set; } = null!;
        public virtual DbSet<DictionaryReadingSource> DictionaryReadingSources { get; set; } = null!;
        public virtual DbSet<DictionaryStatusReason> DictionaryStatusReasons { get; set; } = null!;
        public virtual DbSet<DictionaryValidationStatus> DictionaryValidationStatuses { get; set; } = null!;
        public virtual DbSet<EdsnFraction> EdsnFractions { get; set; } = null!;
        public virtual DbSet<EdsnRejection> EdsnRejections { get; set; } = null!;
        public virtual DbSet<MeterReading> MeterReadings { get; set; } = null!;
        public virtual DbSet<MigrationHistory> MigrationHistories { get; set; } = null!;
        public virtual DbSet<RegisterReading> RegisterReadings { get; set; } = null!;
        public virtual DbSet<RegisterVolume> RegisterVolumes { get; set; } = null!;
        public virtual DbSet<StatusHistory> StatusHistories { get; set; } = null!;
        public virtual DbSet<TransitionEventAction> TransitionEventActions { get; set; } = null!;
        public virtual DbSet<TransitionInitiatedPartyInStatusCondition> TransitionInitiatedPartyInStatusConditions { get; set; } = null!;
        public virtual DbSet<TransitionMarketEventInStatusCondition> TransitionMarketEventInStatusConditions { get; set; } = null!;
        public virtual DbSet<TransitionReadingSourceInStatusCondition> TransitionReadingSourceInStatusConditions { get; set; } = null!;
        public virtual DbSet<TransitionStatusCondition> TransitionStatusConditions { get; set; } = null!;
        public virtual DbSet<TransitionStatusTransition> TransitionStatusTransitions { get; set; } = null!;
        public virtual DbSet<ValidationResultLog> ValidationResultLogs { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<DictionaryAditionalWindowStatus>(entity =>
            {
                entity.ToTable("Dictionary_AditionalWindowStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryBusinessEventType>(entity =>
            {
                entity.ToTable("Dictionary_BusinessEventType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryInitiatedParty>(entity =>
            {
                entity.ToTable("Dictionary_InitiatedParty", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMarketEvent>(entity =>
            {
                entity.ToTable("Dictionary_MarketEvent", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryReadingSource>(entity =>
            {
                entity.ToTable("Dictionary_ReadingSource", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStatusReason>(entity =>
            {
                entity.ToTable("Dictionary_StatusReason", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryValidationStatus>(entity =>
            {
                entity.ToTable("Dictionary_ValidationStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<EdsnFraction>(entity =>
            {
                entity.ToTable("EdsnFraction", "dbo");

                entity.HasIndex(e => new { e.ProfileCodeId, e.FractionDate }, "IX_ProfileCode_FractionDate_incl_FractionValue");

                entity.Property(e => e.FractionDate).HasColumnType("datetime");

                entity.Property(e => e.FractionValue).HasColumnType("decimal(18, 9)");

                entity.HasOne(d => d.ProfileCode)
                    .WithMany(p => p.EdsnFractions)
                    .HasForeignKey(d => d.ProfileCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EdsnFraction_dbo.Dictionary_EnergyUsageProfileCode_ProfileCodeId");
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.ToTable("EdsnRejection", "dbo");

                entity.HasIndex(e => e.StatusHistoryId, "IX_StatusHistoryId");

                entity.Property(e => e.Code).HasMaxLength(10);

                entity.HasOne(d => d.RejectionLevel)
                    .WithMany(p => p.EdsnRejections)
                    .HasForeignKey(d => d.RejectionLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EdsnRejection_dbo.Dictionary_EdsnRejectionLevel_RejectionLevelId");

                entity.HasOne(d => d.StatusHistory)
                    .WithMany(p => p.EdsnRejections)
                    .HasForeignKey(d => d.StatusHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EdsnRejection_dbo.StatusHistory_StatusHistoryId");
            });

            modelBuilder.Entity<MeterReading>(entity =>
            {
                entity.ToTable("MeterReading", "dbo");

                entity.HasIndex(e => new { e.EanId, e.EdsnMeterId }, "IX_Ean_MeterId");

                entity.HasIndex(e => new { e.MarketEventDate, e.EanId }, "IX_MeterReading_MarketEventDate_EanId");

                entity.HasIndex(e => e.StatusId, "IX_ProcessId");

                entity.HasIndex(e => new { e.StatusId, e.ReadingSourceId, e.MarketEventDate }, "IX_Search_By_Criteria");

                entity.HasIndex(e => e.MarketEventDate, "IX_StatusCount");

                entity.HasIndex(e => e.ModifiedOn, "IX_StatusCountByModifiedOn");

                entity.Property(e => e.ClientReadingDate).HasColumnType("date");

                entity.Property(e => e.Consumer).HasMaxLength(13);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EdsnMeterId).HasMaxLength(50);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.Initiator).HasMaxLength(13);

                entity.Property(e => e.MarketEventDate).HasColumnType("date");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.EnergyProductType)
                    .WithMany(p => p.MeterReadings)
                    .HasForeignKey(d => d.EnergyProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_EnergyProductTypeCode_EnergyProductTypeId");

                entity.HasOne(d => d.InitiatedParty)
                    .WithMany(p => p.MeterReadings)
                    .HasForeignKey(d => d.InitiatedPartyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_InitiatedParty_InitiatedPartyId");

                entity.HasOne(d => d.MarketEvent)
                    .WithMany(p => p.MeterReadings)
                    .HasForeignKey(d => d.MarketEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_MarketEvent_MarketEventId");

                entity.HasOne(d => d.ReadingSource)
                    .WithMany(p => p.MeterReadings)
                    .HasForeignKey(d => d.ReadingSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_ReadingSource_ReadingSourceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MeterReadings)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_ProcessStatus_StatusId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.ProductVersion).HasMaxLength(32);
            });

            modelBuilder.Entity<RegisterReading>(entity =>
            {
                entity.ToTable("RegisterReading", "dbo");

                entity.HasIndex(e => new { e.MeterReadingId, e.MeteringDirectionId, e.TariffTypeId }, "IX_TariffType")
                    .IsUnique();

                entity.Property(e => e.RegisterNo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.MeasureUnit)
                    .WithMany(p => p.RegisterReadings)
                    .HasForeignKey(d => d.MeasureUnitId)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.Dictionary_MeasureUnitCode_MeasureUnitId");

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.RegisterReadings)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.MeterReading_MeterReadingId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.RegisterReadings)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.Dictionary_EnergyFlowDirectionCode_MeteringDirectionId");

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.RegisterReadings)
                    .HasForeignKey(d => d.TariffTypeId)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.Dictionary_EnergyTariffTypeCode_TariffTypeId");
            });

            modelBuilder.Entity<RegisterVolume>(entity =>
            {
                entity.ToTable("RegisterVolume", "dbo");

                entity.HasIndex(e => e.BeginReadingId, "IX_BeginReadingId");

                entity.HasIndex(e => e.EndReadingId, "IX_EndReadingId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.BeginReading)
                    .WithMany(p => p.RegisterVolumeBeginReadings)
                    .HasForeignKey(d => d.BeginReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RegisterVolume_dbo.RegisterReading_BeginReadingId");

                entity.HasOne(d => d.EndReading)
                    .WithMany(p => p.RegisterVolumeEndReadings)
                    .HasForeignKey(d => d.EndReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RegisterVolume_dbo.RegisterReading_EndReadingId");
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory", "dbo");

                entity.HasIndex(e => new { e.MeterReadingId, e.CreatedOn }, "IX_MeterReadingIdCreatedOn");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.XmlHeaderCreationTs)
                    .HasColumnType("datetime")
                    .HasColumnName("XmlHeaderCreationTS");

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.MeterReading_MeterReadingId");

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.Dictionary_ProcessStatus_ProcessStatusId");

                entity.HasOne(d => d.Reason)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.ReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.Dictionary_StatusReason_ReasonId");

                entity.HasOne(d => d.StatusTransition)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.StatusTransitionId)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.Transition_StatusTransition_StatusTransitionId");
            });

            modelBuilder.Entity<TransitionEventAction>(entity =>
            {
                entity.ToTable("Transition_EventAction", "dbo");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.TransitionEventActions)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_EventAction_dbo.Dictionary_BusinessEventType_EventTypeId");
            });

            modelBuilder.Entity<TransitionInitiatedPartyInStatusCondition>(entity =>
            {
                entity.ToTable("Transition_InitiatedPartyInStatusCondition", "dbo");

                entity.HasIndex(e => e.ConditionId, "IX_ConditionId");

                entity.HasOne(d => d.Condition)
                    .WithMany(p => p.TransitionInitiatedPartyInStatusConditions)
                    .HasForeignKey(d => d.ConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_InitiatedPartyInStatusCondition_dbo.Transition_StatusCondition_ConditionId");

                entity.HasOne(d => d.InitiatedParty)
                    .WithMany(p => p.TransitionInitiatedPartyInStatusConditions)
                    .HasForeignKey(d => d.InitiatedPartyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_InitiatedPartyInStatusCondition_dbo.Dictionary_InitiatedParty_InitiatedPartyId");
            });

            modelBuilder.Entity<TransitionMarketEventInStatusCondition>(entity =>
            {
                entity.ToTable("Transition_MarketEventInStatusCondition", "dbo");

                entity.HasIndex(e => e.ConditionId, "IX_ConditionId");

                entity.HasOne(d => d.Condition)
                    .WithMany(p => p.TransitionMarketEventInStatusConditions)
                    .HasForeignKey(d => d.ConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_MarketEventInStatusCondition_dbo.Transition_StatusCondition_ConditionId");

                entity.HasOne(d => d.MarketEvent)
                    .WithMany(p => p.TransitionMarketEventInStatusConditions)
                    .HasForeignKey(d => d.MarketEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_MarketEventInStatusCondition_dbo.Dictionary_MarketEvent_MarketEventId");
            });

            modelBuilder.Entity<TransitionReadingSourceInStatusCondition>(entity =>
            {
                entity.ToTable("Transition_ReadingSourceInStatusCondition", "dbo");

                entity.HasIndex(e => e.ConditionId, "IX_ConditionId");

                entity.HasOne(d => d.Condition)
                    .WithMany(p => p.TransitionReadingSourceInStatusConditions)
                    .HasForeignKey(d => d.ConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_ReadingSourceInStatusCondition_dbo.Transition_StatusCondition_ConditionId");

                entity.HasOne(d => d.ReadingSource)
                    .WithMany(p => p.TransitionReadingSourceInStatusConditions)
                    .HasForeignKey(d => d.ReadingSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_ReadingSourceInStatusCondition_dbo.Dictionary_ReadingSource_ReadingSourceId");
            });

            modelBuilder.Entity<TransitionStatusCondition>(entity =>
            {
                entity.ToTable("Transition_StatusCondition", "dbo");

                entity.HasIndex(e => e.TransitionId, "IX_TransitionId");

                entity.HasOne(d => d.DisputeWindowStatus)
                    .WithMany(p => p.TransitionStatusConditionDisputeWindowStatuses)
                    .HasForeignKey(d => d.DisputeWindowStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_AditionalWindowStatus_DisputeWindowStatusId");

                entity.HasOne(d => d.ExchangeWindowStatus)
                    .WithMany(p => p.TransitionStatusConditionExchangeWindowStatuses)
                    .HasForeignKey(d => d.ExchangeWindowStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_AditionalWindowStatus_ExchangeWindowStatusId");

                entity.HasOne(d => d.SettlementWindowStatus)
                    .WithMany(p => p.TransitionStatusConditionSettlementWindowStatuses)
                    .HasForeignKey(d => d.SettlementWindowStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_AditionalWindowStatus_SettlementWindowStatusId");

                entity.HasOne(d => d.Transition)
                    .WithMany(p => p.TransitionStatusConditions)
                    .HasForeignKey(d => d.TransitionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Transition_StatusTransition_TransitionId");

                entity.HasOne(d => d.ValidationStatus)
                    .WithMany(p => p.TransitionStatusConditions)
                    .HasForeignKey(d => d.ValidationStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_ValidationStatus_ValidationStatusId");
            });

            modelBuilder.Entity<TransitionStatusTransition>(entity =>
            {
                entity.ToTable("Transition_StatusTransition", "dbo");

                entity.HasIndex(e => e.ActionId, "IX_ActionId");

                entity.HasIndex(e => e.ParentId, "IX_ParentId");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Action)
                    .WithMany(p => p.TransitionStatusTransitions)
                    .HasForeignKey(d => d.ActionId)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Transition_EventAction_ActionId");

                entity.HasOne(d => d.CurrentReadingStatus)
                    .WithMany(p => p.TransitionStatusTransitionCurrentReadingStatuses)
                    .HasForeignKey(d => d.CurrentReadingStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Dictionary_ProcessStatus_CurrentReadingStatusId");

                entity.HasOne(d => d.NewReadingStatus)
                    .WithMany(p => p.TransitionStatusTransitionNewReadingStatuses)
                    .HasForeignKey(d => d.NewReadingStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Dictionary_ProcessStatus_NewReadingStatusId");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Transition_StatusTransition_ParentId");
            });

            modelBuilder.Entity<ValidationResultLog>(entity =>
            {
                entity.ToTable("ValidationResultLog", "dbo");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.Property(e => e.EventDate).HasColumnType("date");

                entity.Property(e => e.RegisterNo)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
