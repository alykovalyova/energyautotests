﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryAditionalWindowStatus
    {
        public DictionaryAditionalWindowStatus()
        {
            TransitionStatusConditionDisputeWindowStatuses = new HashSet<TransitionStatusCondition>();
            TransitionStatusConditionExchangeWindowStatuses = new HashSet<TransitionStatusCondition>();
            TransitionStatusConditionSettlementWindowStatuses = new HashSet<TransitionStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditionDisputeWindowStatuses { get; set; }
        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditionExchangeWindowStatuses { get; set; }
        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditionSettlementWindowStatuses { get; set; }
    }
}
