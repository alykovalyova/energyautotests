﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryBusinessEventType
    {
        public DictionaryBusinessEventType()
        {
            TransitionEventActions = new HashSet<TransitionEventAction>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<TransitionEventAction> TransitionEventActions { get; set; }
    }
}
