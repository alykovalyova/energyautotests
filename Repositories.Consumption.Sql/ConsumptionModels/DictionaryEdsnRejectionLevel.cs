﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryEdsnRejectionLevel
    {
        public DictionaryEdsnRejectionLevel()
        {
            EdsnRejections = new HashSet<EdsnRejection>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<EdsnRejection> EdsnRejections { get; set; }
    }
}
