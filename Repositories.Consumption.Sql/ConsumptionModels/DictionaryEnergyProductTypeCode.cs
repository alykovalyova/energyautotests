﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryEnergyProductTypeCode
    {
        public DictionaryEnergyProductTypeCode()
        {
            MeterReadings = new HashSet<MeterReading>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MeterReading> MeterReadings { get; set; }
    }
}
