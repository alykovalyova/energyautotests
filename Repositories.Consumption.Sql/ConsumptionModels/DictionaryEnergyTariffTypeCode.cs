﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryEnergyTariffTypeCode
    {
        public DictionaryEnergyTariffTypeCode()
        {
            RegisterReadings = new HashSet<RegisterReading>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<RegisterReading> RegisterReadings { get; set; }
    }
}
