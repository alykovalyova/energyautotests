﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryEnergyUsageProfileCode
    {
        public DictionaryEnergyUsageProfileCode()
        {
            EdsnFractions = new HashSet<EdsnFraction>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<EdsnFraction> EdsnFractions { get; set; }
    }
}
