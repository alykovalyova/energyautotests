﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryInitiatedParty
    {
        public DictionaryInitiatedParty()
        {
            MeterReadings = new HashSet<MeterReading>();
            TransitionInitiatedPartyInStatusConditions = new HashSet<TransitionInitiatedPartyInStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MeterReading> MeterReadings { get; set; }
        public virtual ICollection<TransitionInitiatedPartyInStatusCondition> TransitionInitiatedPartyInStatusConditions { get; set; }
    }
}
