﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryMarketEvent
    {
        public DictionaryMarketEvent()
        {
            MeterReadings = new HashSet<MeterReading>();
            TransitionMarketEventInStatusConditions = new HashSet<TransitionMarketEventInStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MeterReading> MeterReadings { get; set; }
        public virtual ICollection<TransitionMarketEventInStatusCondition> TransitionMarketEventInStatusConditions { get; set; }
    }
}
