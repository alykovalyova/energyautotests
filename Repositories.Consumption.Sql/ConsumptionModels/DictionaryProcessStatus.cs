﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryProcessStatus
    {
        public DictionaryProcessStatus()
        {
            MeterReadings = new HashSet<MeterReading>();
            StatusHistories = new HashSet<StatusHistory>();
            TransitionStatusTransitionCurrentReadingStatuses = new HashSet<TransitionStatusTransition>();
            TransitionStatusTransitionNewReadingStatuses = new HashSet<TransitionStatusTransition>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MeterReading> MeterReadings { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
        public virtual ICollection<TransitionStatusTransition> TransitionStatusTransitionCurrentReadingStatuses { get; set; }
        public virtual ICollection<TransitionStatusTransition> TransitionStatusTransitionNewReadingStatuses { get; set; }
    }
}
