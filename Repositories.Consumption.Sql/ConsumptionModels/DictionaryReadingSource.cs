﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryReadingSource
    {
        public DictionaryReadingSource()
        {
            MeterReadings = new HashSet<MeterReading>();
            TransitionReadingSourceInStatusConditions = new HashSet<TransitionReadingSourceInStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MeterReading> MeterReadings { get; set; }
        public virtual ICollection<TransitionReadingSourceInStatusCondition> TransitionReadingSourceInStatusConditions { get; set; }
    }
}
