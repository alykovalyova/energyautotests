﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryStatusReason
    {
        public DictionaryStatusReason()
        {
            StatusHistories = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
