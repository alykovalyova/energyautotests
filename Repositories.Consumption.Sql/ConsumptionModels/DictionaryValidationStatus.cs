﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class DictionaryValidationStatus
    {
        public DictionaryValidationStatus()
        {
            TransitionStatusConditions = new HashSet<TransitionStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditions { get; set; }
    }
}
