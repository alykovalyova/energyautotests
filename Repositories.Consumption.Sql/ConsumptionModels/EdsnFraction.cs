﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class EdsnFraction
    {
        public int Id { get; set; }
        public DateTime FractionDate { get; set; }
        public decimal FractionValue { get; set; }
        public int ProfileCodeId { get; set; }

        public virtual DictionaryEnergyUsageProfileCode ProfileCode { get; set; } = null!;
    }
}
