﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class EdsnRejection
    {
        public int Id { get; set; }
        public string? Code { get; set; }
        public string? Message { get; set; }
        public int StatusHistoryId { get; set; }
        public int RejectionLevelId { get; set; }

        public virtual DictionaryEdsnRejectionLevel RejectionLevel { get; set; } = null!;
        public virtual StatusHistory StatusHistory { get; set; } = null!;
    }
}
