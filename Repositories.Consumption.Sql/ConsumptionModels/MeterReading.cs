﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class MeterReading
    {
        public MeterReading()
        {
            RegisterReadings = new HashSet<RegisterReading>();
            StatusHistories = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; } = null!;
        public string? EdsnMeterId { get; set; }
        public string? ExternalReference { get; set; }
        public int EnergyProductTypeId { get; set; }
        public int NrOfRegisters { get; set; }
        public DateTime MarketEventDate { get; set; }
        public DateTime? ClientReadingDate { get; set; }
        public int ReadingSourceId { get; set; }
        public int StatusId { get; set; }
        public string? Consumer { get; set; }
        public string? Initiator { get; set; }
        public string? DossierId { get; set; }
        public int MarketEventId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? ProcessId { get; set; }
        public int InitiatedPartyId { get; set; }

        public virtual DictionaryEnergyProductTypeCode EnergyProductType { get; set; } = null!;
        public virtual DictionaryInitiatedParty InitiatedParty { get; set; } = null!;
        public virtual DictionaryMarketEvent MarketEvent { get; set; } = null!;
        public virtual DictionaryReadingSource ReadingSource { get; set; } = null!;
        public virtual DictionaryProcessStatus Status { get; set; } = null!;
        public virtual ICollection<RegisterReading> RegisterReadings { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
