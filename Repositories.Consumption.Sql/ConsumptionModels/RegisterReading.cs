﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class RegisterReading
    {
        public RegisterReading()
        {
            RegisterVolumeBeginReadings = new HashSet<RegisterVolume>();
            RegisterVolumeEndReadings = new HashSet<RegisterVolume>();
        }

        public int Id { get; set; }
        public int MeterReadingId { get; set; }
        public int? MeasureUnitId { get; set; }
        public int? MeteringDirectionId { get; set; }
        public int NrOfDigits { get; set; }
        public int? TariffTypeId { get; set; }
        public int Value { get; set; }
        public string? RegisterNo { get; set; }

        public virtual DictionaryMeasureUnitCode? MeasureUnit { get; set; }
        public virtual MeterReading MeterReading { get; set; } = null!;
        public virtual DictionaryEnergyFlowDirectionCode? MeteringDirection { get; set; }
        public virtual DictionaryEnergyTariffTypeCode? TariffType { get; set; }
        public virtual ICollection<RegisterVolume> RegisterVolumeBeginReadings { get; set; }
        public virtual ICollection<RegisterVolume> RegisterVolumeEndReadings { get; set; }
    }
}
