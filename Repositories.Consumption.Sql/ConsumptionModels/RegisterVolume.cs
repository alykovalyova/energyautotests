﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class RegisterVolume
    {
        public int Id { get; set; }
        public int? CalorificCorrectedVolume { get; set; }
        public int Volume { get; set; }
        public int BeginReadingId { get; set; }
        public int EndReadingId { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual RegisterReading BeginReading { get; set; } = null!;
        public virtual RegisterReading EndReading { get; set; } = null!;
    }
}
