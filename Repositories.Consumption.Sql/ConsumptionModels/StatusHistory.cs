﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class StatusHistory
    {
        public StatusHistory()
        {
            EdsnRejections = new HashSet<EdsnRejection>();
        }

        public int Id { get; set; }
        public int MeterReadingId { get; set; }
        public Guid? XmlHeaderMessageId { get; set; }
        public DateTime? XmlHeaderCreationTs { get; set; }
        public int ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? StatusTransitionId { get; set; }
        public int ReasonId { get; set; }
        public string? Comment { get; set; }
        public string? User { get; set; }

        public virtual MeterReading MeterReading { get; set; } = null!;
        public virtual DictionaryProcessStatus ProcessStatus { get; set; } = null!;
        public virtual DictionaryStatusReason Reason { get; set; } = null!;
        public virtual TransitionStatusTransition? StatusTransition { get; set; }
        public virtual ICollection<EdsnRejection> EdsnRejections { get; set; }
    }
}
