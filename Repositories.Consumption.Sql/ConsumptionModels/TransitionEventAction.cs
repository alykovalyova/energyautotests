﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class TransitionEventAction
    {
        public TransitionEventAction()
        {
            TransitionStatusTransitions = new HashSet<TransitionStatusTransition>();
        }

        public int Id { get; set; }
        public int EventTypeId { get; set; }

        public virtual DictionaryBusinessEventType EventType { get; set; } = null!;
        public virtual ICollection<TransitionStatusTransition> TransitionStatusTransitions { get; set; }
    }
}
