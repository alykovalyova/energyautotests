﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class TransitionInitiatedPartyInStatusCondition
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int InitiatedPartyId { get; set; }

        public virtual TransitionStatusCondition Condition { get; set; } = null!;
        public virtual DictionaryInitiatedParty InitiatedParty { get; set; } = null!;
    }
}
