﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class TransitionMarketEventInStatusCondition
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int MarketEventId { get; set; }

        public virtual TransitionStatusCondition Condition { get; set; } = null!;
        public virtual DictionaryMarketEvent MarketEvent { get; set; } = null!;
    }
}
