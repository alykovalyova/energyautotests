﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class TransitionReadingSourceInStatusCondition
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int ReadingSourceId { get; set; }

        public virtual TransitionStatusCondition Condition { get; set; } = null!;
        public virtual DictionaryReadingSource ReadingSource { get; set; } = null!;
    }
}
