﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class TransitionStatusCondition
    {
        public TransitionStatusCondition()
        {
            TransitionInitiatedPartyInStatusConditions = new HashSet<TransitionInitiatedPartyInStatusCondition>();
            TransitionMarketEventInStatusConditions = new HashSet<TransitionMarketEventInStatusCondition>();
            TransitionReadingSourceInStatusConditions = new HashSet<TransitionReadingSourceInStatusCondition>();
        }

        public int Id { get; set; }
        public int TransitionId { get; set; }
        public int? ValidationStatusId { get; set; }
        public int? ExchangeWindowStatusId { get; set; }
        public int? DisputeWindowStatusId { get; set; }
        public int? SettlementWindowStatusId { get; set; }

        public virtual DictionaryAditionalWindowStatus? DisputeWindowStatus { get; set; }
        public virtual DictionaryAditionalWindowStatus? ExchangeWindowStatus { get; set; }
        public virtual DictionaryAditionalWindowStatus? SettlementWindowStatus { get; set; }
        public virtual TransitionStatusTransition Transition { get; set; } = null!;
        public virtual DictionaryValidationStatus? ValidationStatus { get; set; }
        public virtual ICollection<TransitionInitiatedPartyInStatusCondition> TransitionInitiatedPartyInStatusConditions { get; set; }
        public virtual ICollection<TransitionMarketEventInStatusCondition> TransitionMarketEventInStatusConditions { get; set; }
        public virtual ICollection<TransitionReadingSourceInStatusCondition> TransitionReadingSourceInStatusConditions { get; set; }
    }
}
