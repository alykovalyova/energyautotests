﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class TransitionStatusTransition
    {
        public TransitionStatusTransition()
        {
            InverseParent = new HashSet<TransitionStatusTransition>();
            StatusHistories = new HashSet<StatusHistory>();
            TransitionStatusConditions = new HashSet<TransitionStatusCondition>();
        }

        public int Id { get; set; }
        public int? ActionId { get; set; }
        public int? ParentId { get; set; }
        public int? CurrentReadingStatusId { get; set; }
        public int NewReadingStatusId { get; set; }
        public bool? IsEnabled { get; set; }

        public virtual TransitionEventAction? Action { get; set; }
        public virtual DictionaryProcessStatus? CurrentReadingStatus { get; set; }
        public virtual DictionaryProcessStatus NewReadingStatus { get; set; } = null!;
        public virtual TransitionStatusTransition? Parent { get; set; }
        public virtual ICollection<TransitionStatusTransition> InverseParent { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditions { get; set; }
    }
}
