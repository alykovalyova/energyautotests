﻿using System;
using System.Collections.Generic;

namespace Repositories.Consumption.Sql.ConsumptionModels
{
    public partial class ValidationResultLog
    {
        public int Id { get; set; }
        public string Ean { get; set; } = null!;
        public DateTime EventDate { get; set; }
        public int Event { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int ReadingValue { get; set; }
        public int? MeasureUnitId { get; set; }
        public int? MeterReadingDirectionId { get; set; }
        public int? TariffTypeId { get; set; }
        public string? RegisterNo { get; set; }
        public int ValidationResult { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
