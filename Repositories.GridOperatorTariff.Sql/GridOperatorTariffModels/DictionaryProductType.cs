﻿using System;
using System.Collections.Generic;

namespace Repositories.GridOperatorTariff.Sql.GridOperatorTariffModels
{
    public partial class DictionaryProductType
    {
        public DictionaryProductType()
        {
            Tariffs = new HashSet<Tariff>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Tariff> Tariffs { get; set; }
    }
}
