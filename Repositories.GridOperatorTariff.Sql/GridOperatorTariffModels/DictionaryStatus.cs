﻿using System;
using System.Collections.Generic;

namespace Repositories.GridOperatorTariff.Sql.GridOperatorTariffModels
{
    public partial class DictionaryStatus
    {
        public DictionaryStatus()
        {
            GridOperatorTariffs = new HashSet<GridOperatorTariff>();
            StatusHistories = new HashSet<StatusHistory>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GridOperatorTariff> GridOperatorTariffs { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
