﻿using System;
using System.Collections.Generic;

namespace Repositories.GridOperatorTariff.Sql.GridOperatorTariffModels
{
    public partial class GridOperatorTariff
    {
        public GridOperatorTariff()
        {
            StatusHistories = new HashSet<StatusHistory>();
            Tariffs = new HashSet<Tariff>();
        }

        public int Id { get; set; }
        public string EdsnGridOperatorTariffId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime QueryDate { get; set; }
        public string Currency { get; set; }
        public string GridOperator { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual DictionaryStatus Status { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
        public virtual ICollection<Tariff> Tariffs { get; set; }
    }
}
