﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.GridOperatorTariff.Sql.GridOperatorTariffModels
{
    public partial class GridOperatorTariffContext : DbContext
    {
        public GridOperatorTariffContext()
        {
        }

        public GridOperatorTariffContext(DbContextOptions<GridOperatorTariffContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryProductType> DictionaryProductTypes { get; set; }
        public virtual DbSet<DictionaryStatus> DictionaryStatuses { get; set; }
        public virtual DbSet<GridOperatorTariff> GridOperatorTariffs { get; set; }
        public virtual DbSet<StatusHistory> StatusHistories { get; set; }
        public virtual DbSet<Tariff> Tariffs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=GridOperatorTariff;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<DictionaryProductType>(entity =>
            {
                entity.ToTable("Dictionary_ProductType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryStatus>(entity =>
            {
                entity.ToTable("Dictionary_Status", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<GridOperatorTariff>(entity =>
            {
                entity.ToTable("GridOperatorTariff", "dbo");

                entity.HasIndex(e => e.StatusId, "IX_GridOperatorTariff_StatusId");

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.DateFrom).HasColumnType("date");

                entity.Property(e => e.EdsnGridOperatorTariffId).HasMaxLength(50);

                entity.Property(e => e.GridOperator)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.QueryDate).HasColumnType("date");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GridOperatorTariffs)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory", "dbo");

                entity.HasIndex(e => e.GridOperatorTariffId, "IX_StatusHistory_GridOperatorTariffId");

                entity.HasIndex(e => e.StatusId, "IX_StatusHistory_StatusId");

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.HasOne(d => d.GridOperatorTariff)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.GridOperatorTariffId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<Tariff>(entity =>
            {
                entity.ToTable("Tariff", "dbo");

                entity.HasIndex(e => e.GridOperatorTariffId, "IX_Tariff_GridOperatorTariffId");

                entity.HasIndex(e => e.ProductTypeId, "IX_Tariff_ProductTypeId");

                entity.Property(e => e.CapTarCode)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.DayTariff).HasColumnType("decimal(12, 4)");

                entity.HasOne(d => d.GridOperatorTariff)
                    .WithMany(p => p.Tariffs)
                    .HasForeignKey(d => d.GridOperatorTariffId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Tariffs)
                    .HasForeignKey(d => d.ProductTypeId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
