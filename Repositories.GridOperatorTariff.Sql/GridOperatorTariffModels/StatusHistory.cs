﻿using System;
using System.Collections.Generic;

namespace Repositories.GridOperatorTariff.Sql.GridOperatorTariffModels
{
    public partial class StatusHistory
    {
        public int Id { get; set; }
        public string ModifiedBy { get; set; }
        public int GridOperatorTariffId { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual GridOperatorTariff GridOperatorTariff { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
