﻿using System;
using System.Collections.Generic;

namespace Repositories.GridOperatorTariff.Sql.GridOperatorTariffModels
{
    public partial class Tariff
    {
        public int Id { get; set; }
        public short ProductTypeId { get; set; }
        public decimal DayTariff { get; set; }
        public string CapTarCode { get; set; }
        public int GridOperatorTariffId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual GridOperatorTariff GridOperatorTariff { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
    }
}
