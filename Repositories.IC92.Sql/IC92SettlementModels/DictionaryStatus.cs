﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class DictionaryStatus
    {
        public DictionaryStatus()
        {
            SettlementStatusHistories = new HashSet<SettlementStatusHistory>();
            Settlements = new HashSet<Settlement>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SettlementStatusHistory> SettlementStatusHistories { get; set; }
        public virtual ICollection<Settlement> Settlements { get; set; }
    }
}
