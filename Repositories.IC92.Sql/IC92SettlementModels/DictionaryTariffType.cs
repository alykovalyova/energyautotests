﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class DictionaryTariffType
    {
        public DictionaryTariffType()
        {
            Registers = new HashSet<Register>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Register> Registers { get; set; }
    }
}
