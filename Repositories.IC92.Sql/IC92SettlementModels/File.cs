﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class File
    {
        public File()
        {
            SettlementInvoices = new HashSet<Settlement>();
            SettlementProformas = new HashSet<Settlement>();
            SettlementStatusHistories = new HashSet<SettlementStatusHistory>();
        }

        public int Id { get; set; }
        public string FullPath { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Settlement> SettlementInvoices { get; set; }
        public virtual ICollection<Settlement> SettlementProformas { get; set; }
        public virtual ICollection<SettlementStatusHistory> SettlementStatusHistories { get; set; }
    }
}
