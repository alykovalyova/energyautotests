﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class IC92SettlementContext : DbContext
    {
        private readonly string _connectionString;

        public IC92SettlementContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IC92SettlementContext(DbContextOptions<IC92SettlementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryMeteringDirection> DictionaryMeteringDirections { get; set; }
        public virtual DbSet<DictionaryProductType> DictionaryProductTypes { get; set; }
        public virtual DbSet<DictionaryStatus> DictionaryStatuses { get; set; }
        public virtual DbSet<DictionaryTariffType> DictionaryTariffTypes { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<Register> Registers { get; set; }
        public virtual DbSet<Settlement> Settlements { get; set; }
        public virtual DbSet<SettlementStatusHistory> SettlementStatusHistories { get; set; }
        public virtual DbSet<Tariff> Tariffs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeteringDirection>(entity =>
            {
                entity.ToTable("Dictionary_MeteringDirection", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProductType>(entity =>
            {
                entity.ToTable("Dictionary_ProductType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryStatus>(entity =>
            {
                entity.ToTable("Dictionary_Status", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryTariffType>(entity =>
            {
                entity.ToTable("Dictionary_TariffType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<File>(entity =>
            {
                entity.ToTable("File", "dbo");

                entity.Property(e => e.FullPath).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.ToTable("Register", "dbo");

                entity.HasIndex(e => e.MeteringDirectionId, "IX_Register_MeteringDirectionId");

                entity.HasIndex(e => e.SettlementId, "IX_Register_SettlementId");

                entity.HasIndex(e => e.TariffTypeId, "IX_Register_TariffTypeId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.MeteringDirectionId);

                entity.HasOne(d => d.Settlement)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.SettlementId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.TariffTypeId);
            });

            modelBuilder.Entity<Settlement>(entity =>
            {
                entity.ToTable("Settlement", "dbo");

                entity.HasIndex(e => e.InvoiceId, "IX_Settlement_InvoiceId");

                entity.HasIndex(e => e.LabelId, "IX_Settlement_LabelId");

                entity.HasIndex(e => e.ProductTypeId, "IX_Settlement_ProductTypeId");

                entity.HasIndex(e => e.ProformaId, "IX_Settlement_ProformaId");

                entity.HasIndex(e => e.StatusId, "IX_Settlement_StatusId");

                entity.HasIndex(e => new { e.SupplierId, e.LabelId, e.StatusId, e.EventDate }, "IX_Settlement_SupplierId_LabelId_StatusId_EventDate");

                entity.HasIndex(e => e.TariffId, "IX_Settlement_TariffId");

                entity.Property(e => e.Comment).HasMaxLength(250);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.EventDate).HasColumnType("date");

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(12, 4)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.SettlementInvoices)
                    .HasForeignKey(d => d.InvoiceId);

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.Settlements)
                    .HasForeignKey(d => d.LabelId);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Settlements)
                    .HasForeignKey(d => d.ProductTypeId);

                entity.HasOne(d => d.Proforma)
                    .WithMany(p => p.SettlementProformas)
                    .HasForeignKey(d => d.ProformaId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Settlements)
                    .HasForeignKey(d => d.StatusId);

                entity.HasOne(d => d.Tariff)
                    .WithMany(p => p.Settlements)
                    .HasForeignKey(d => d.TariffId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<SettlementStatusHistory>(entity =>
            {
                entity.ToTable("SettlementStatusHistory", "dbo");

                entity.HasIndex(e => e.FileId, "IX_SettlementStatusHistory_FileId");

                entity.HasIndex(e => e.SettlementId, "IX_SettlementStatusHistory_SettlementId");

                entity.HasIndex(e => e.StatusId, "IX_SettlementStatusHistory_StatusId");

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.HasOne(d => d.File)
                    .WithMany(p => p.SettlementStatusHistories)
                    .HasForeignKey(d => d.FileId);

                entity.HasOne(d => d.Settlement)
                    .WithMany(p => p.SettlementStatusHistories)
                    .HasForeignKey(d => d.SettlementId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SettlementStatusHistories)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<Tariff>(entity =>
            {
                entity.ToTable("Tariff", "dbo");

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.Property(e => e.PriceElec).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.PriceGas).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.ValidFrom).HasColumnType("date");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
