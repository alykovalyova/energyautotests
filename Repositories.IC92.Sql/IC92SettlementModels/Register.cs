﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class Register
    {
        public int Id { get; set; }
        public int Usage { get; set; }
        public int? OriginalReading { get; set; }
        public int? NewReading { get; set; }
        public int SettlementId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short TariffTypeId { get; set; }

        public virtual DictionaryMeteringDirection MeteringDirection { get; set; }
        public virtual Settlement Settlement { get; set; }
        public virtual DictionaryTariffType TariffType { get; set; }
    }
}
