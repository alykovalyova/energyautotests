﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class Settlement
    {
        public Settlement()
        {
            Registers = new HashSet<Register>();
            SettlementStatusHistories = new HashSet<SettlementStatusHistory>();
        }

        public int Id { get; set; }
        public int SupplierId { get; set; }
        public string Ean { get; set; }
        public DateTime EventDate { get; set; }
        public int TotalUsage { get; set; }
        public decimal TotalAmount { get; set; }
        public int TariffId { get; set; }
        public int ContractId { get; set; }
        public short ProductTypeId { get; set; }
        public short StatusId { get; set; }
        public short LabelId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? InvoiceId { get; set; }
        public int? ProformaId { get; set; }
        public string Comment { get; set; }

        public virtual File Invoice { get; set; }
        public virtual DictionaryLabel Label { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
        public virtual File Proforma { get; set; }
        public virtual DictionaryStatus Status { get; set; }
        public virtual Tariff Tariff { get; set; }
        public virtual ICollection<Register> Registers { get; set; }
        public virtual ICollection<SettlementStatusHistory> SettlementStatusHistories { get; set; }
    }
}
