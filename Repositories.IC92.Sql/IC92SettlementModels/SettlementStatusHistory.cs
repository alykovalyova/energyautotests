﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class SettlementStatusHistory
    {
        public int Id { get; set; }
        public string ModifiedBy { get; set; }
        public int SettlementId { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? FileId { get; set; }

        public virtual File File { get; set; }
        public virtual Settlement Settlement { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
