﻿using System;
using System.Collections.Generic;

namespace Repositories.IC92.Sql.IC92SettlementModels
{
    public partial class Tariff
    {
        public Tariff()
        {
            Settlements = new HashSet<Settlement>();
        }

        public int Id { get; set; }
        public decimal PriceElec { get; set; }
        public decimal PriceGas { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<Settlement> Settlements { get; set; }
    }
}
