﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class Address
    {
        public Address()
        {
            AddressHistories = new HashSet<AddressHistory>();
            MduHistories = new HashSet<MduHistory>();
            MeteringPoints = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoints = new HashSet<PreSwitchMeteringPoint>();
        }

        public int Id { get; set; }
        public string EanId { get; set; } = null!;
        public int? BuildingNr { get; set; }
        public string? ExBuildingNr { get; set; }
        public string? Zipcode { get; set; }
        public string? StreetName { get; set; }
        public string? CityName { get; set; }
        public string? Country { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool IsChangedByAgent { get; set; }

        public virtual ICollection<AddressHistory> AddressHistories { get; set; }
        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; }
    }
}
