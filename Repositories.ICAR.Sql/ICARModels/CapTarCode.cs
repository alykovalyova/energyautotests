﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class CapTarCode
    {
        public CapTarCode()
        {
            FallbackMeteringPoints = new HashSet<FallbackMeteringPoint>();
            MduHistories = new HashSet<MduHistory>();
            MeteringPoints = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoints = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string Ean { get; set; } = null!;

        public virtual ICollection<FallbackMeteringPoint> FallbackMeteringPoints { get; set; }
        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; }
    }
}
