﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class CcuStatusHistory
    {
        public int Id { get; set; }
        public int CcuHistoryId { get; set; }
        public byte ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string? Comment { get; set; }
        public string? User { get; set; }

        public virtual CcuHistory CcuHistory { get; set; } = null!;
        public virtual DictionaryProcessStatus ProcessStatus { get; set; } = null!;
    }
}
