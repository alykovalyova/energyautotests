﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryEdsnmessageType
    {
        public DictionaryEdsnmessageType()
        {
            CcuHistories = new HashSet<CcuHistory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<CcuHistory> CcuHistories { get; set; }
    }
}
