﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryEnergyAllocationMethodCode
    {
        public DictionaryEnergyAllocationMethodCode()
        {
            MduHistories = new HashSet<MduHistory>();
            MeteringPoints = new HashSet<MeteringPoint>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
    }
}
