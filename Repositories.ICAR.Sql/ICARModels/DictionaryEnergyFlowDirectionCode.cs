﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryEnergyFlowDirectionCode
    {
        public DictionaryEnergyFlowDirectionCode()
        {
            FallbackMeteringPoints = new HashSet<FallbackMeteringPoint>();
            MduHistories = new HashSet<MduHistory>();
            MduRegisters = new HashSet<MduRegister>();
            MeteringPoints = new HashSet<MeteringPoint>();
            Registers = new HashSet<Register>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<FallbackMeteringPoint> FallbackMeteringPoints { get; set; }
        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MduRegister> MduRegisters { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
        public virtual ICollection<Register> Registers { get; set; }
    }
}
