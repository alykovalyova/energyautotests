﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryEnergyTariffTypeCode
    {
        public DictionaryEnergyTariffTypeCode()
        {
            MduRegisters = new HashSet<MduRegister>();
            Registers = new HashSet<Register>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MduRegister> MduRegisters { get; set; }
        public virtual ICollection<Register> Registers { get; set; }
    }
}
