﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryMarketPartyRoleCode
    {
        public DictionaryMarketPartyRoleCode()
        {
            MarketParties = new HashSet<MarketParty>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MarketParty> MarketParties { get; set; }
    }
}
