﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryMutationReasonCode
    {
        public DictionaryMutationReasonCode()
        {
            CcuHistories = new HashSet<CcuHistory>();
            MduMutations = new HashSet<MduMutation>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<CcuHistory> CcuHistories { get; set; }
        public virtual ICollection<MduMutation> MduMutations { get; set; }
    }
}
