﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class DictionaryProcessStatus
    {
        public DictionaryProcessStatus()
        {
            CcuHistories = new HashSet<CcuHistory>();
            CcuStatusHistories = new HashSet<CcuStatusHistory>();
            MduHistories = new HashSet<MduHistory>();
            MduStatusHistories = new HashSet<MduStatusHistory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<CcuHistory> CcuHistories { get; set; }
        public virtual ICollection<CcuStatusHistory> CcuStatusHistories { get; set; }
        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MduStatusHistory> MduStatusHistories { get; set; }
    }
}
