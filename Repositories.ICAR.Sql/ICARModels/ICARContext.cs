﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class ICARContext : DbContext
    {
        private readonly string _connectionString;

        public ICARContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ICARContext(DbContextOptions<ICARContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; } = null!;
        public virtual DbSet<AddressHistory> AddressHistories { get; set; } = null!;
        public virtual DbSet<CapTarCode> CapTarCodes { get; set; } = null!;
        public virtual DbSet<CcuHistory> CcuHistories { get; set; } = null!;
        public virtual DbSet<CcuStatusHistory> CcuStatusHistories { get; set; } = null!;
        public virtual DbSet<DictionaryCommunicationStatusCode> DictionaryCommunicationStatusCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEdsnmessageType> DictionaryEdsnmessageTypes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyAllocationMethodCode> DictionaryEnergyAllocationMethodCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyConnectionPhysicalStatusCode> DictionaryEnergyConnectionPhysicalStatusCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyDeliveryStatusCode> DictionaryEnergyDeliveryStatusCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyMeterTypeCode> DictionaryEnergyMeterTypeCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyMeteringMethodCode> DictionaryEnergyMeteringMethodCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCodes { get; set; } = null!;
        public virtual DbSet<DictionaryMarketPartyRoleCode> DictionaryMarketPartyRoleCodes { get; set; } = null!;
        public virtual DbSet<DictionaryMarketSegmentCode> DictionaryMarketSegmentCodes { get; set; } = null!;
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCodes { get; set; } = null!;
        public virtual DbSet<DictionaryMutationReasonCode> DictionaryMutationReasonCodes { get; set; } = null!;
        public virtual DbSet<DictionaryPhysicalCapacityCode> DictionaryPhysicalCapacityCodes { get; set; } = null!;
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatuses { get; set; } = null!;
        public virtual DbSet<FallbackMeteringPoint> FallbackMeteringPoints { get; set; } = null!;
        public virtual DbSet<GridArea> GridAreas { get; set; } = null!;
        public virtual DbSet<MarketParty> MarketParties { get; set; } = null!;
        public virtual DbSet<MduHistory> MduHistories { get; set; } = null!;
        public virtual DbSet<MduMutation> MduMutations { get; set; } = null!;
        public virtual DbSet<MduRegister> MduRegisters { get; set; } = null!;
        public virtual DbSet<MduStatusHistory> MduStatusHistories { get; set; } = null!;
        public virtual DbSet<MeteringPoint> MeteringPoints { get; set; } = null!;
        public virtual DbSet<MigrationHistory> MigrationHistories { get; set; } = null!;
        public virtual DbSet<NleXml> NleXmls { get; set; } = null!;
        public virtual DbSet<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; } = null!;
        public virtual DbSet<PreSwitchRegister> PreSwitchRegisters { get; set; } = null!;
        public virtual DbSet<Register> Registers { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address", "dbo");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Zipcode, e.BuildingNr, e.ExBuildingNr }, "Search_ZipCode_BuildingNr");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(10)
                    .HasColumnName("ZIPCode");
            });

            modelBuilder.Entity<AddressHistory>(entity =>
            {
                entity.ToTable("AddressHistory", "dbo");

                entity.HasIndex(e => e.AddressId, "IX_AddressId");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.User).HasMaxLength(100);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(10)
                    .HasColumnName("ZIPCode");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.AddressHistories)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.AddressChange_dbo.Address_AddressId");
            });

            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.ToTable("CapTarCode", "dbo");

                entity.HasIndex(e => e.Ean, "IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);
            });

            modelBuilder.Entity<CcuHistory>(entity =>
            {
                entity.ToTable("CcuHistory", "dbo");

                entity.HasIndex(e => e.DossierId, "IX_DossierId");

                entity.HasIndex(e => e.EanId, "IX_EanId");

                entity.HasIndex(e => e.StatusId, "IX_StatusId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.XmlHeaderCreationTs)
                    .HasColumnType("datetime")
                    .HasColumnName("XmlHeaderCreationTS");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.CcuHistoryBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.MarketParty_BalanceSupplierId");

                entity.HasOne(d => d.BalanceSupplierResponsible)
                    .WithMany(p => p.CcuHistoryBalanceSupplierResponsibles)
                    .HasForeignKey(d => d.BalanceSupplierResponsibleId)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.MarketParty_BalanceSupplierResponsibleId");

                entity.HasOne(d => d.MessageType)
                    .WithMany(p => p.CcuHistories)
                    .HasForeignKey(d => d.MessageTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.Dictionary_EDSNMessageType_MessageTypeId");

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.CcuHistories)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.Dictionary_MutationReasonCode_MutationReasonId");

                entity.HasOne(d => d.OldBalanceSupplier)
                    .WithMany(p => p.CcuHistoryOldBalanceSuppliers)
                    .HasForeignKey(d => d.OldBalanceSupplierId)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.MarketParty_OldBalanceSupplierId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CcuHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.Dictionary_ProcessStatus_StatusId");
            });

            modelBuilder.Entity<CcuStatusHistory>(entity =>
            {
                entity.ToTable("CcuStatusHistory", "dbo");

                entity.HasIndex(e => e.CcuHistoryId, "IX_CcuHistoryId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.User).HasMaxLength(100);

                entity.HasOne(d => d.CcuHistory)
                    .WithMany(p => p.CcuStatusHistories)
                    .HasForeignKey(d => d.CcuHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuStatusHistory_dbo.CcuHistory_CcuHistoryId");

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.CcuStatusHistories)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuStatusHistory_dbo.Dictionary_ProcessStatus_ProcessStatusId");
            });

            modelBuilder.Entity<DictionaryCommunicationStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_CommunicationStatusCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEdsnmessageType>(entity =>
            {
                entity.ToTable("Dictionary_EDSNMessageType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyAllocationMethodCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyAllocationMethodCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyConnectionPhysicalStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyConnectionPhysicalStatusCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyDeliveryStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyDeliveryStatusCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyMeterTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyMeterTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyMeteringMethodCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyMeteringMethodCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMarketPartyRoleCode>(entity =>
            {
                entity.ToTable("Dictionary_MarketPartyRoleCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMarketSegmentCode>(entity =>
            {
                entity.ToTable("Dictionary_MarketSegmentCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMutationReasonCode>(entity =>
            {
                entity.ToTable("Dictionary_MutationReasonCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryPhysicalCapacityCode>(entity =>
            {
                entity.ToTable("Dictionary_PhysicalCapacityCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<FallbackMeteringPoint>(entity =>
            {
                entity.ToTable("FallbackMeteringPoint", "dbo");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Zipcode, e.BuildingNr, e.ExBuildingNr }, "Search_ZipCode_BuildingNr");

                entity.Property(e => e.CityName).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(50);

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StreetName).HasMaxLength(100);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(50)
                    .HasColumnName("ZIPCode");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.EnergyFlowDirectionId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_EnergyFlowDirectionCode_EnergyFlowDirectionId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.MarketParty_GridOperatorId");

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_MarketSegmentCode_MarketSegmentId");

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.PhysicalCapacityId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_PhysicalCapacityCode_PhysicalCapacityId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.FallbackMeteringPoints)
                    .HasForeignKey(d => d.ProfileCategoryId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_EnergyUsageProfileCode_ProfileCategoryId");
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.ToTable("GridArea", "dbo");

                entity.HasIndex(e => e.Ean, "IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(20);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean, "IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.MarketParties)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.MarketParty_dbo.Dictionary_MarketPartyRoleCode_RoleId");
            });

            modelBuilder.Entity<MduHistory>(entity =>
            {
                entity.ToTable("MduHistory", "dbo");

                entity.HasIndex(e => new { e.BalanceSupplierId, e.StatusId }, "IDX_ICAR_MduHistory_EANID_adhoc");

                entity.HasIndex(e => e.AddressId, "IX_AddressId");

                entity.HasIndex(e => new { e.EanId, e.MutationDate }, "IX_MduHistory_EanId_MutationDate");

                entity.HasIndex(e => new { e.StatusId, e.MutationDate }, "IX_StatusId_MutationDate");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.Papean)
                    .HasMaxLength(18)
                    .HasColumnName("PAPEan");

                entity.Property(e => e.Sapeans)
                    .HasMaxLength(189)
                    .HasColumnName("SAPEans");

                entity.Property(e => e.XmlHeaderCreationTs)
                    .HasColumnType("datetime")
                    .HasColumnName("XmlHeaderCreationTS");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Address_AddressId");

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.AllocationMethodId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyAllocationMethodCode_AllocationMethodId");

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MduHistoryBalanceResponsibleParties)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_BalanceResponsiblePartyId");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MduHistoryBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_BalanceSupplierId");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.CommunicationStatusCode)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.CommunicationStatusCodeId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_CommunicationStatusCode_CommunicationStatusCodeId");

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyDeliveryStatusCode_EnergyDeliveryStatusId");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.EnergyFlowDirectionId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyFlowDirectionCode_EnergyFlowDirectionId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MduHistoryGridOperators)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_GridOperatorId");

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_MarketSegmentCode_MarketSegmentId");

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyMeteringMethodCode_MeteringMethodId");

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MduHistoryMeteringResponsibleParties)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_MeteringResponsiblePartyId");

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.PhysicalCapacityId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_PhysicalCapacityCode_PhysicalCapacityId");

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyConnectionPhysicalStatusCode_PhysicalStatusId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.ProfileCategoryId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyUsageProfileCode_ProfileCategoryId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_ProcessStatus_StatusId");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyMeterTypeCode_TypeId");
            });

            modelBuilder.Entity<MduMutation>(entity =>
            {
                entity.ToTable("MduMutation", "dbo");

                entity.HasIndex(e => e.MduHistoryId, "IX_MduHistoryId");

                entity.HasIndex(e => new { e.MutationReasonId, e.MduHistoryId }, "IX_ReasonId_MduHistoryId");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduMutations)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduMutation_dbo.MduHistory_MduHistoryId");

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.MduMutations)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduMutation_dbo.Dictionary_MutationReasonCode_MutationReasonId");
            });

            modelBuilder.Entity<MduRegister>(entity =>
            {
                entity.ToTable("MduRegister", "dbo");

                entity.HasIndex(e => e.MduHistoryId, "IX_MduHistoryId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.MduHistory_MduHistoryId");

                entity.HasOne(d => d.MeasureUnit)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.MeasureUnitId)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.Dictionary_MeasureUnitCode_MeasureUnitId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.Dictionary_EnergyFlowDirectionCode_MeteringDirectionId");

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.Dictionary_EnergyTariffTypeCode_TariffTypeId");
            });

            modelBuilder.Entity<MduStatusHistory>(entity =>
            {
                entity.ToTable("MduStatusHistory", "dbo");

                entity.HasIndex(e => e.MduHistoryId, "IX_MduHistoryId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.User).HasMaxLength(100);

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduStatusHistories)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduStatusHistory_dbo.MduHistory_MduHistoryId");

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.MduStatusHistories)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduStatusHistory_dbo.Dictionary_ProcessStatus_ProcessStatusId");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint", "dbo");

                entity.HasIndex(e => e.AddressId, "IX_AddressId");

                entity.HasIndex(e => new { e.BalanceSupplierId, e.PhysicalStatusId }, "IX_BalanceSupplierId_PhysicalStatusId");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.Property(e => e.CcModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CcValidFrom).HasColumnType("date");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MduValidFrom).HasColumnType("date");

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Papean)
                    .HasMaxLength(18)
                    .HasColumnName("PAPEan");

                entity.Property(e => e.Sapeans)
                    .HasMaxLength(189)
                    .HasColumnName("SAPEans");

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Address_AddressId");

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.AllocationMethodId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyAllocationMethodCode_AllocationMethodId");

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MeteringPointBalanceResponsibleParties)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_BalanceResponsiblePartyId");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MeteringPointBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_BalanceSupplierId");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.CommunicationStatusCode)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.CommunicationStatusCodeId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_CommunicationStatusCode_CommunicationStatusCodeId");

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyDeliveryStatusCode_EnergyDeliveryStatusId");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.EnergyFlowDirectionId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyFlowDirectionCode_EnergyFlowDirectionId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPointGridOperators)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_GridOperatorId");

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_MarketSegmentCode_MarketSegmentId");

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyMeteringMethodCode_MeteringMethodId");

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MeteringPointMeteringResponsibleParties)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_MeteringResponsiblePartyId");

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.PhysicalCapacityId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_PhysicalCapacityCode_PhysicalCapacityId");

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyConnectionPhysicalStatusCode_PhysicalStatusId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.ProfileCategoryId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyUsageProfileCode_ProfileCategoryId");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyMeterTypeCode_TypeId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.ProductVersion).HasMaxLength(32);
            });

            modelBuilder.Entity<NleXml>(entity =>
            {
                entity.ToTable("NleXmls", "dbo");

                entity.Property(e => e.Status).HasMaxLength(3);
            });

            modelBuilder.Entity<PreSwitchMeteringPoint>(entity =>
            {
                entity.ToTable("PreSwitchMeteringPoint", "dbo");

                entity.HasIndex(e => e.AddressId, "IX_AddressId");

                entity.HasIndex(e => e.CapTarCodeId, "IX_CapTarCodeId");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => e.GridAreaId, "IX_GridAreaId");

                entity.HasIndex(e => e.GridOperatorId, "IX_GridOperatorId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.ValidDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("('1900-01-01T00:00:00.000')");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.Address_AddressId");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.MarketParty_GridOperatorId");
            });

            modelBuilder.Entity<PreSwitchRegister>(entity =>
            {
                entity.ToTable("PreSwitchRegister", "dbo");

                entity.HasIndex(e => e.MeteringPointPreSwitchId, "IX_MeteringPointPreSwitch");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringPointPreSwitch)
                    .WithMany(p => p.PreSwitchRegisters)
                    .HasForeignKey(d => d.MeteringPointPreSwitchId)
                    .HasConstraintName("FK_dbo.PreSwitchRegister_dbo.PreSwitchMeteringPoint_MeteringPointPreSwitchId");
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.ToTable("Register", "dbo");

                entity.HasIndex(e => e.MeteringPointId, "IX_MeteringPointId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeasureUnit)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.MeasureUnitId)
                    .HasConstraintName("FK_dbo.Register_dbo.Dictionary_MeasureUnitCode_MeasureUnitId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Register_dbo.Dictionary_EnergyFlowDirectionCode_MeteringDirectionId");

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Register_dbo.MeteringPoint_MeteringPointId");

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Register_dbo.Dictionary_EnergyTariffTypeCode_TariffTypeId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
