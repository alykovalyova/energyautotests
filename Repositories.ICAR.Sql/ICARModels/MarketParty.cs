﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            CcuHistoryBalanceSupplierResponsibles = new HashSet<CcuHistory>();
            CcuHistoryBalanceSuppliers = new HashSet<CcuHistory>();
            CcuHistoryOldBalanceSuppliers = new HashSet<CcuHistory>();
            FallbackMeteringPoints = new HashSet<FallbackMeteringPoint>();
            MduHistoryBalanceResponsibleParties = new HashSet<MduHistory>();
            MduHistoryBalanceSuppliers = new HashSet<MduHistory>();
            MduHistoryGridOperators = new HashSet<MduHistory>();
            MduHistoryMeteringResponsibleParties = new HashSet<MduHistory>();
            MeteringPointBalanceResponsibleParties = new HashSet<MeteringPoint>();
            MeteringPointBalanceSuppliers = new HashSet<MeteringPoint>();
            MeteringPointGridOperators = new HashSet<MeteringPoint>();
            MeteringPointMeteringResponsibleParties = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoints = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string? Ean { get; set; }
        public string? Name { get; set; }
        public byte? RoleId { get; set; }

        public virtual DictionaryMarketPartyRoleCode? Role { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryBalanceSupplierResponsibles { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryBalanceSuppliers { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryOldBalanceSuppliers { get; set; }
        public virtual ICollection<FallbackMeteringPoint> FallbackMeteringPoints { get; set; }
        public virtual ICollection<MduHistory> MduHistoryBalanceResponsibleParties { get; set; }
        public virtual ICollection<MduHistory> MduHistoryBalanceSuppliers { get; set; }
        public virtual ICollection<MduHistory> MduHistoryGridOperators { get; set; }
        public virtual ICollection<MduHistory> MduHistoryMeteringResponsibleParties { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointBalanceResponsibleParties { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointBalanceSuppliers { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointGridOperators { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointMeteringResponsibleParties { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; }
    }
}
