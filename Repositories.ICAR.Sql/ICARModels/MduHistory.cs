﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class MduHistory
    {
        public MduHistory()
        {
            MduMutations = new HashSet<MduMutation>();
            MduRegisters = new HashSet<MduRegister>();
            MduStatusHistories = new HashSet<MduStatusHistory>();
        }

        public int Id { get; set; }
        public string? EanId { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
        public short? GridOperatorId { get; set; }
        public short? GridAreaId { get; set; }
        public string? LocationDescription { get; set; }
        public byte MarketSegmentId { get; set; }
        public byte ProductTypeId { get; set; }
        public bool? DeterminationComplex { get; set; }
        public bool? Residential { get; set; }
        public short? BalanceSupplierId { get; set; }
        public short? BalanceResponsiblePartyId { get; set; }
        public short? MeteringResponsiblePartyId { get; set; }
        public bool HasCommercialCharacteristics { get; set; }
        public byte? AllocationMethodId { get; set; }
        public short? CapTarCodeId { get; set; }
        public string? ContractedCapacity { get; set; }
        public int? Eacpeak { get; set; }
        public int? EacoffPeak { get; set; }
        public byte EnergyDeliveryStatusId { get; set; }
        public byte? EnergyFlowDirectionId { get; set; }
        public byte MeteringMethodId { get; set; }
        public byte? PhysicalCapacityId { get; set; }
        public byte PhysicalStatusId { get; set; }
        public byte? ProfileCategoryId { get; set; }
        public string? InvoiceMonth { get; set; }
        public string? MaxConsumption { get; set; }
        public string? MeterEdsnId { get; set; }
        public byte? NrOfRegisters { get; set; }
        public byte? TypeId { get; set; }
        public bool? TemperatureCorrection { get; set; }
        public byte? CommunicationStatusCodeId { get; set; }
        public byte StatusId { get; set; }
        public string? LastComment { get; set; }
        public Guid XmlHeaderMessageId { get; set; }
        public DateTime XmlHeaderCreationTs { get; set; }
        public DateTime MutationDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int AddressId { get; set; }
        public string? Papean { get; set; }
        public string? Sapeans { get; set; }
        public int? Eappeak { get; set; }
        public int? EapoffPeak { get; set; }
        public bool IsTfSignal { get; set; }

        public virtual Address Address { get; set; } = null!;
        public virtual DictionaryEnergyAllocationMethodCode? AllocationMethod { get; set; }
        public virtual MarketParty? BalanceResponsibleParty { get; set; }
        public virtual MarketParty? BalanceSupplier { get; set; }
        public virtual CapTarCode? CapTarCode { get; set; }
        public virtual DictionaryCommunicationStatusCode? CommunicationStatusCode { get; set; }
        public virtual DictionaryEnergyDeliveryStatusCode EnergyDeliveryStatus { get; set; } = null!;
        public virtual DictionaryEnergyFlowDirectionCode? EnergyFlowDirection { get; set; }
        public virtual GridArea? GridArea { get; set; }
        public virtual MarketParty? GridOperator { get; set; }
        public virtual DictionaryMarketSegmentCode MarketSegment { get; set; } = null!;
        public virtual DictionaryEnergyMeteringMethodCode MeteringMethod { get; set; } = null!;
        public virtual MarketParty? MeteringResponsibleParty { get; set; }
        public virtual DictionaryPhysicalCapacityCode? PhysicalCapacity { get; set; }
        public virtual DictionaryEnergyConnectionPhysicalStatusCode PhysicalStatus { get; set; } = null!;
        public virtual DictionaryEnergyProductTypeCode ProductType { get; set; } = null!;
        public virtual DictionaryEnergyUsageProfileCode? ProfileCategory { get; set; }
        public virtual DictionaryProcessStatus Status { get; set; } = null!;
        public virtual DictionaryEnergyMeterTypeCode? Type { get; set; }
        public virtual ICollection<MduMutation> MduMutations { get; set; }
        public virtual ICollection<MduRegister> MduRegisters { get; set; }
        public virtual ICollection<MduStatusHistory> MduStatusHistories { get; set; }
    }
}
