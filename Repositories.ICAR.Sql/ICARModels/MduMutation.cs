﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class MduMutation
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public string? ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public byte MutationReasonId { get; set; }
        public string? DossierId { get; set; }

        public virtual MduHistory MduHistory { get; set; } = null!;
        public virtual DictionaryMutationReasonCode MutationReason { get; set; } = null!;
    }
}
