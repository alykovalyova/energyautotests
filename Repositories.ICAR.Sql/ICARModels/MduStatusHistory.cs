﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class MduStatusHistory
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public byte ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string? Comment { get; set; }
        public string? User { get; set; }

        public virtual MduHistory MduHistory { get; set; } = null!;
        public virtual DictionaryProcessStatus ProcessStatus { get; set; } = null!;
    }
}
