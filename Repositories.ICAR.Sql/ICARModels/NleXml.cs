﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class NleXml
    {
        public int Id { get; set; }
        public string XmlName { get; set; } = null!;
        public string Status { get; set; } = null!;
    }
}
