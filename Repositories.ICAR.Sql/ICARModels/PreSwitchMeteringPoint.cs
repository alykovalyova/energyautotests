﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class PreSwitchMeteringPoint
    {
        public PreSwitchMeteringPoint()
        {
            PreSwitchRegisters = new HashSet<PreSwitchRegister>();
        }

        public int Id { get; set; }
        public string? EanId { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
        public int AddressId { get; set; }
        public short? GridAreaId { get; set; }
        public string? LocationDescription { get; set; }
        public byte MarketSegmentId { get; set; }
        public byte ProductTypeId { get; set; }
        public short? GridOperatorId { get; set; }
        public string? MeterEdsnId { get; set; }
        public byte? EnergyMeterTypeId { get; set; }
        public byte? CommunicationStatusId { get; set; }
        public byte? NrOfRegisters { get; set; }
        public short? CapTarCodeId { get; set; }
        public int? EacoffPeak { get; set; }
        public int? Eacpeak { get; set; }
        public byte? EnergyFlowDirection { get; set; }
        public byte? MeteringMethod { get; set; }
        public byte? PhysicalStatus { get; set; }
        public byte? ProfileCategory { get; set; }
        public string? DossierId { get; set; }
        public string? ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime ValidDate { get; set; }
        public bool? TemperatureCorrection { get; set; }
        public int? EapoffPeak { get; set; }
        public int? Eappeak { get; set; }

        public virtual Address Address { get; set; } = null!;
        public virtual CapTarCode? CapTarCode { get; set; }
        public virtual GridArea? GridArea { get; set; }
        public virtual MarketParty? GridOperator { get; set; }
        public virtual ICollection<PreSwitchRegister> PreSwitchRegisters { get; set; }
    }
}
