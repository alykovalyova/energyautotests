﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class PreSwitchRegister
    {
        public int Id { get; set; }
        public int MeteringPointPreSwitchId { get; set; }
        public string? EdsnId { get; set; }
        public byte? TariffTypeId { get; set; }
        public byte? MeteringDirectionId { get; set; }
        public byte NrOfDigits { get; set; }
        public decimal? MultiplicationFactor { get; set; }

        public virtual PreSwitchMeteringPoint MeteringPointPreSwitch { get; set; } = null!;
    }
}
