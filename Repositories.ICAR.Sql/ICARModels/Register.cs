﻿using System;
using System.Collections.Generic;

namespace Repositories.ICAR.Sql.ICARModels
{
    public partial class Register
    {
        public int Id { get; set; }
        public int MeteringPointId { get; set; }
        public string? EdsnId { get; set; }
        public byte TariffTypeId { get; set; }
        public byte MeteringDirectionId { get; set; }
        public byte? MeasureUnitId { get; set; }
        public byte NrOfDigits { get; set; }
        public decimal? MultiplicationFactor { get; set; }

        public virtual DictionaryMeasureUnitCode? MeasureUnit { get; set; }
        public virtual DictionaryEnergyFlowDirectionCode MeteringDirection { get; set; } = null!;
        public virtual MeteringPoint MeteringPoint { get; set; } = null!;
        public virtual DictionaryEnergyTariffTypeCode TariffType { get; set; } = null!;
    }
}
