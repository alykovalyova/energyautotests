﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class Batch
    {
        public Batch()
        {
            Leads = new HashSet<Lead>();
        }

        public int Id { get; set; }
        public string Comment { get; set; }
        public short BatchStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual DictionaryBatchStatus BatchStatus { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
    }
}
