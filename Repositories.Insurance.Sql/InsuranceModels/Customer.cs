﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerProducts = new HashSet<CustomerProduct>();
        }

        public int Id { get; set; }
        public int LeadId { get; set; }
        public long RelationId { get; set; }
        public string StreetName { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public short? OwnershipId { get; set; }
        public short? FamilySizeId { get; set; }
        public int MonthlyIncome { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal NetPrice { get; set; }
        public decimal Discount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int NutsHomeCustomerNumber { get; set; }

        public virtual DictionaryFamilySize FamilySize { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual DictionaryOwnership Ownership { get; set; }
        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
    }
}
