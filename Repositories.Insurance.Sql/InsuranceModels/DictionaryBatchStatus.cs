﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class DictionaryBatchStatus
    {
        public DictionaryBatchStatus()
        {
            Batches = new HashSet<Batch>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Batch> Batches { get; set; }
    }
}
