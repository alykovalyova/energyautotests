﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class DictionaryCampaignType
    {
        public DictionaryCampaignType()
        {
            Offers = new HashSet<Offer>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }
    }
}
