﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class DictionaryDateOfBirthRange
    {
        public DictionaryDateOfBirthRange()
        {
            Leads = new HashSet<Lead>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Lead> Leads { get; set; }
    }
}
