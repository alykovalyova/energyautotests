﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class DictionaryEnergyUsageRange
    {
        public DictionaryEnergyUsageRange()
        {
            LeadEnergyProductElkUsageRanges = new HashSet<LeadEnergyProduct>();
            LeadEnergyProductGasUsageRanges = new HashSet<LeadEnergyProduct>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<LeadEnergyProduct> LeadEnergyProductElkUsageRanges { get; set; }
        public virtual ICollection<LeadEnergyProduct> LeadEnergyProductGasUsageRanges { get; set; }
    }
}
