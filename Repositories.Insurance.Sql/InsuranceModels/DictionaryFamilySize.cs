﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class DictionaryFamilySize
    {
        public DictionaryFamilySize()
        {
            Customers = new HashSet<Customer>();
            Leads = new HashSet<Lead>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
    }
}
