﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class DictionaryProductType
    {
        public DictionaryProductType()
        {
            CustomerProducts = new HashSet<CustomerProduct>();
            OfferProducts = new HashSet<OfferProduct>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
        public virtual ICollection<OfferProduct> OfferProducts { get; set; }
    }
}
