﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class InsuranceContext : DbContext
    {
        public InsuranceContext()
        {
        }

        public InsuranceContext(DbContextOptions<InsuranceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Batch> Batches { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerProduct> CustomerProducts { get; set; }
        public virtual DbSet<DictionaryBatchStatus> DictionaryBatchStatuses { get; set; }
        public virtual DbSet<DictionaryCampaignType> DictionaryCampaignTypes { get; set; }
        public virtual DbSet<DictionaryDateOfBirthRange> DictionaryDateOfBirthRanges { get; set; }
        public virtual DbSet<DictionaryEnergyUsageRange> DictionaryEnergyUsageRanges { get; set; }
        public virtual DbSet<DictionaryFamilySize> DictionaryFamilySizes { get; set; }
        public virtual DbSet<DictionaryGender> DictionaryGenders { get; set; }
        public virtual DbSet<DictionaryOwnership> DictionaryOwnerships { get; set; }
        public virtual DbSet<DictionaryProductType> DictionaryProductTypes { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }
        public virtual DbSet<LeadEnergyProduct> LeadEnergyProducts { get; set; }
        public virtual DbSet<Offer> Offers { get; set; }
        public virtual DbSet<OfferProduct> OfferProducts { get; set; }
        public virtual DbSet<ProductChangeReason> ProductChangeReasons { get; set; }
        public virtual DbSet<ProductSpecification> ProductSpecifications { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=Insurance;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<Batch>(entity =>
            {
                entity.ToTable("Batch");

                entity.HasIndex(e => e.BatchStatusId, "IX_Batch_BatchStatusId");

                entity.HasOne(d => d.BatchStatus)
                    .WithMany(p => p.Batches)
                    .HasForeignKey(d => d.BatchStatusId);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");

                entity.HasIndex(e => e.FamilySizeId, "IX_Customer_FamilySizeId");

                entity.HasIndex(e => e.LeadId, "IX_Customer_LeadId");

                entity.HasIndex(e => e.OwnershipId, "IX_Customer_OwnershipId");

                entity.HasIndex(e => e.RelationId, "IX_Customer_RelationId")
                    .IsUnique();

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.GrossPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.HouseNumberExtension).HasMaxLength(50);

                entity.Property(e => e.NetPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.ZipCode).HasMaxLength(6);

                entity.HasOne(d => d.FamilySize)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.FamilySizeId);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Ownership)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.OwnershipId);
            });

            modelBuilder.Entity<CustomerProduct>(entity =>
            {
                entity.ToTable("CustomerProduct");

                entity.HasIndex(e => e.CustomerId, "IX_CustomerProduct_CustomerId");

                entity.HasIndex(e => e.PolicyNumber, "IX_CustomerProduct_PolicyNumber")
                    .IsUnique();

                entity.HasIndex(e => e.ProductChangeReasonId, "IX_CustomerProduct_ProductChangeReasonId");

                entity.HasIndex(e => e.ProductSpecificationId, "IX_CustomerProduct_ProductSpecificationId");

                entity.HasIndex(e => e.ProductTypeId, "IX_CustomerProduct_ProductTypeId");

                entity.Property(e => e.PolicyNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerProducts)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductChangeReason)
                    .WithMany(p => p.CustomerProducts)
                    .HasForeignKey(d => d.ProductChangeReasonId);

                entity.HasOne(d => d.ProductSpecification)
                    .WithMany(p => p.CustomerProducts)
                    .HasForeignKey(d => d.ProductSpecificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.CustomerProducts)
                    .HasForeignKey(d => d.ProductTypeId);
            });

            modelBuilder.Entity<DictionaryBatchStatus>(entity =>
            {
                entity.ToTable("Dictionary_BatchStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryCampaignType>(entity =>
            {
                entity.ToTable("Dictionary_CampaignType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryDateOfBirthRange>(entity =>
            {
                entity.ToTable("Dictionary_DateOfBirthRange");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyUsageRange>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageRange");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryFamilySize>(entity =>
            {
                entity.ToTable("Dictionary_FamilySize");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryGender>(entity =>
            {
                entity.ToTable("Dictionary_Gender");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryOwnership>(entity =>
            {
                entity.ToTable("Dictionary_Ownership");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProductType>(entity =>
            {
                entity.ToTable("Dictionary_ProductType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Lead>(entity =>
            {
                entity.ToTable("Lead");

                entity.HasIndex(e => e.BatchId, "IX_Lead_BatchId");

                entity.HasIndex(e => e.DateOfBirthRangeId, "IX_Lead_DateOfBirthRangeId");

                entity.HasIndex(e => e.EnergyProductId, "IX_Lead_EnergyProductId");

                entity.HasIndex(e => e.FamilySizeId, "IX_Lead_FamilySizeId");

                entity.HasIndex(e => e.GenderId, "IX_Lead_GenderId");

                entity.HasIndex(e => e.OwnershipId, "IX_Lead_OwnershipId");

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.HouseNumberExtensionLetter).HasMaxLength(10);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.HasOne(d => d.Batch)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.BatchId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.DateOfBirthRange)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.DateOfBirthRangeId);

                entity.HasOne(d => d.EnergyProduct)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.EnergyProductId);

                entity.HasOne(d => d.FamilySize)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.FamilySizeId);

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.GenderId);

                entity.HasOne(d => d.Ownership)
                    .WithMany(p => p.Leads)
                    .HasForeignKey(d => d.OwnershipId);
            });

            modelBuilder.Entity<LeadEnergyProduct>(entity =>
            {
                entity.ToTable("LeadEnergyProduct");

                entity.HasIndex(e => e.ElkUsageRangeId, "IX_LeadEnergyProduct_ElkUsageRangeId");

                entity.HasIndex(e => e.GasUsageRangeId, "IX_LeadEnergyProduct_GasUsageRangeId");

                entity.HasOne(d => d.ElkUsageRange)
                    .WithMany(p => p.LeadEnergyProductElkUsageRanges)
                    .HasForeignKey(d => d.ElkUsageRangeId);

                entity.HasOne(d => d.GasUsageRange)
                    .WithMany(p => p.LeadEnergyProductGasUsageRanges)
                    .HasForeignKey(d => d.GasUsageRangeId);
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.ToTable("Offer");

                entity.HasIndex(e => e.CampaignTypeId, "IX_Offer_CampaignTypeId");

                entity.HasIndex(e => e.LeadId, "IX_Offer_LeadId");

                entity.Property(e => e.Comment).HasMaxLength(255);

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.GrossPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.NetPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.TrackingId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CampaignType)
                    .WithMany(p => p.Offers)
                    .HasForeignKey(d => d.CampaignTypeId);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.Offers)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<OfferProduct>(entity =>
            {
                entity.ToTable("OfferProduct");

                entity.HasIndex(e => e.OfferId, "IX_OfferProduct_OfferId");

                entity.HasIndex(e => e.ProductSpecificationId, "IX_OfferProduct_ProductSpecificationId");

                entity.HasIndex(e => e.ProductTypeId, "IX_OfferProduct_ProductTypeId");

                entity.Property(e => e.Price).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.OfferProducts)
                    .HasForeignKey(d => d.OfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductSpecification)
                    .WithMany(p => p.OfferProducts)
                    .HasForeignKey(d => d.ProductSpecificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.OfferProducts)
                    .HasForeignKey(d => d.ProductTypeId);
            });

            modelBuilder.Entity<ProductChangeReason>(entity =>
            {
                entity.ToTable("ProductChangeReason");

                entity.Property(e => e.Description).HasMaxLength(100);
            });

            modelBuilder.Entity<ProductSpecification>(entity =>
            {
                entity.ToTable("ProductSpecification");

                entity.Property(e => e.Description).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
