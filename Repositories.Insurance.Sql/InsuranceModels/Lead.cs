﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class Lead
    {
        public Lead()
        {
            Customers = new HashSet<Customer>();
            Offers = new HashSet<Offer>();
        }

        public int Id { get; set; }
        public int BatchId { get; set; }
        public string Email { get; set; }
        public int HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public long BagId { get; set; }
        public int? EnergyProductId { get; set; }
        public short? GenderId { get; set; }
        public short? OwnershipId { get; set; }
        public short? FamilySizeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? FamilySizeCount { get; set; }
        public string HouseNumberExtensionLetter { get; set; }
        public int? HouseNumberExtensionNumber { get; set; }
        public int NutsHomeCustomerNumber { get; set; }
        public short? DateOfBirthRangeId { get; set; }

        public virtual Batch Batch { get; set; }
        public virtual DictionaryDateOfBirthRange DateOfBirthRange { get; set; }
        public virtual LeadEnergyProduct EnergyProduct { get; set; }
        public virtual DictionaryFamilySize FamilySize { get; set; }
        public virtual DictionaryGender Gender { get; set; }
        public virtual DictionaryOwnership Ownership { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
    }
}
