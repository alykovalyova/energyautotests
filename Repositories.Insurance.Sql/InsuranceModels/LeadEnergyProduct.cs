﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class LeadEnergyProduct
    {
        public LeadEnergyProduct()
        {
            Leads = new HashSet<Lead>();
        }

        public int Id { get; set; }
        public short? ElkUsageRangeId { get; set; }
        public short? GasUsageRangeId { get; set; }

        public virtual DictionaryEnergyUsageRange ElkUsageRange { get; set; }
        public virtual DictionaryEnergyUsageRange GasUsageRange { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
    }
}
