﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class Offer
    {
        public Offer()
        {
            OfferProducts = new HashSet<OfferProduct>();
        }

        public int Id { get; set; }
        public int LeadId { get; set; }
        public short CampaignTypeId { get; set; }
        public string TrackingId { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal NetPrice { get; set; }
        public decimal Discount { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual DictionaryCampaignType CampaignType { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual ICollection<OfferProduct> OfferProducts { get; set; }
    }
}
