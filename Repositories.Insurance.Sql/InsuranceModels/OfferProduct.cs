﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class OfferProduct
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public short ProductTypeId { get; set; }
        public short ProductSpecificationId { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal Price { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual ProductSpecification ProductSpecification { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
    }
}
