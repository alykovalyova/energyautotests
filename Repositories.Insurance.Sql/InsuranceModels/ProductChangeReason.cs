﻿using System;
using System.Collections.Generic;

namespace Repositories.Insurance.Sql.InsuranceModels
{
    public partial class ProductChangeReason
    {
        public ProductChangeReason()
        {
            CustomerProducts = new HashSet<CustomerProduct>();
        }

        public short Id { get; set; }
        public int ExternalKey { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
    }
}
