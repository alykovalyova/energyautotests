﻿using System;
using System.Collections.Generic;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class DictionaryMarketPartyRoleCode
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
