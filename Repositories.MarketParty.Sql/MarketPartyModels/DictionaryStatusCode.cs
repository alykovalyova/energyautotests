﻿using System;
using System.Collections.Generic;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class DictionaryStatusCode
    {
        public DictionaryStatusCode()
        {
            MarketPartyInfos = new HashSet<MarketPartyInfo>();
            StatusHistories = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MarketPartyInfo> MarketPartyInfos { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
