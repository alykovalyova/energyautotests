﻿using System;
using System.Collections.Generic;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class MarketParty
    {
        public short Id { get; set; }
        public string Ean { get; set; }
        public string Name { get; set; }
        public byte? RoleId { get; set; }
    }
}
