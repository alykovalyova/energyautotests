﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class MarketPartyContext : DbContext
    {
        public MarketPartyContext()
        {
        }

        public MarketPartyContext(DbContextOptions<MarketPartyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryMarketPartyRoleCode> DictionaryMarketPartyRoleCodes { get; set; }
        public virtual DbSet<DictionaryStatusCode> DictionaryStatusCodes { get; set; }
        public virtual DbSet<MarketParty> MarketParties { get; set; }
        public virtual DbSet<MarketPartyEan> MarketPartyEans { get; set; }
        public virtual DbSet<MarketPartyInfo> MarketPartyInfos { get; set; }
        public virtual DbSet<StatusHistory> StatusHistories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=MarketParty;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryMarketPartyRoleCode>(entity =>
            {
                entity.ToTable("Dictionary_MarketPartyRoleCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_StatusCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean, "IX_MarketParty_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<MarketPartyEan>(entity =>
            {
                entity.ToTable("MarketPartyEan", "dbo");

                entity.HasIndex(e => e.MarketPartyInfoId, "IX_MarketPartyEan_MarketPartyInfoId");

                entity.HasOne(d => d.MarketPartyInfo)
                    .WithMany(p => p.MarketPartyEans)
                    .HasForeignKey(d => d.MarketPartyInfoId);
            });

            modelBuilder.Entity<MarketPartyInfo>(entity =>
            {
                entity.ToTable("MarketPartyInfo", "dbo");

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.HasOne(d => d.LastStatus)
                    .WithMany(p => p.MarketPartyInfos)
                    .HasForeignKey(d => d.LastStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MarketPartyInfo_Dictionary_StatusCode_Id");
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory", "dbo");

                entity.HasIndex(e => e.MarketPartyInfoId, "IX_StatusHistory_MarketPartyInfoId");

                entity.HasOne(d => d.MarketPartyInfo)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.MarketPartyInfoId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StatusHistory_Dictionary_StatusCode_Id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
