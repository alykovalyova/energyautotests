﻿using System;
using System.Collections.Generic;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class MarketPartyEan
    {
        public int Id { get; set; }
        public int MarketPartyInfoId { get; set; }
        public string Ean { get; set; }

        public virtual MarketPartyInfo MarketPartyInfo { get; set; }
    }
}
