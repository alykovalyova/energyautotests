﻿using System;
using System.Collections.Generic;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class MarketPartyInfo
    {
        public MarketPartyInfo()
        {
            MarketPartyEans = new HashSet<MarketPartyEan>();
            StatusHistories = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string MarketPartyName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string RequestType { get; set; }
        public string Other { get; set; }
        public int LastStatusId { get; set; }
        public bool AvailableForIc92 { get; set; }

        public virtual DictionaryStatusCode LastStatus { get; set; }
        public virtual ICollection<MarketPartyEan> MarketPartyEans { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
