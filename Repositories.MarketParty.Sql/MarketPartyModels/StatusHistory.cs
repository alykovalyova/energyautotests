﻿using System;
using System.Collections.Generic;

namespace Repositories.MarketParty.Sql.MarketPartyModels
{
    public partial class StatusHistory
    {
        public int Id { get; set; }
        public int StatusId { get; set; }
        public int MarketPartyInfoId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual MarketPartyInfo MarketPartyInfo { get; set; }
        public virtual DictionaryStatusCode Status { get; set; }
    }
}
