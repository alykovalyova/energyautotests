﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class DictionaryEdsnRejectionLevel
    {
        public DictionaryEdsnRejectionLevel()
        {
            EdsnRejections = new HashSet<EdsnRejection>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<EdsnRejection> EdsnRejections { get; set; }
    }
}
