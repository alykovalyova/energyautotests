﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class DictionaryMasterDataBatchStatus
    {
        public DictionaryMasterDataBatchStatus()
        {
            MasterDataBatchStatusHistories = new HashSet<MasterDataBatchStatusHistory>();
            MasterDataBatches = new HashSet<MasterDataBatch>();
        }

        public byte Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }

        public virtual ICollection<MasterDataBatchStatusHistory> MasterDataBatchStatusHistories { get; set; }
        public virtual ICollection<MasterDataBatch> MasterDataBatches { get; set; }
    }
}
