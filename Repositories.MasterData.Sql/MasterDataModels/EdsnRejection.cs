﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EdsnRejection
    {
        public int Id { get; set; }
        public byte? RejectionLevelId { get; set; }
        public string? Code { get; set; }
        public string? Message { get; set; }

        public virtual DictionaryEdsnRejectionLevel? RejectionLevel { get; set; }
    }
}
