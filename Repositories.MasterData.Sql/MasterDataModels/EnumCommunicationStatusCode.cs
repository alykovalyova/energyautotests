﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EnumCommunicationStatusCode
    {
        public EnumCommunicationStatusCode()
        {
            MduHistories = new HashSet<MduHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MduHistory> MduHistories { get; set; }
    }
}
