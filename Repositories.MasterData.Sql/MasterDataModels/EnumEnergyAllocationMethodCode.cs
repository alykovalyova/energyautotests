﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EnumEnergyAllocationMethodCode
    {
        public EnumEnergyAllocationMethodCode()
        {
            MduHistories = new HashSet<MduHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MduHistory> MduHistories { get; set; }
    }
}
