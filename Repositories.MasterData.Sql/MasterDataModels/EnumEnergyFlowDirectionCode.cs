﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EnumEnergyFlowDirectionCode
    {
        public EnumEnergyFlowDirectionCode()
        {
            MduHistories = new HashSet<MduHistory>();
            MduRegisters = new HashSet<MduRegister>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MduRegister> MduRegisters { get; set; }
    }
}
