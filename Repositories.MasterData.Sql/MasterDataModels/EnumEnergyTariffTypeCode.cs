﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EnumEnergyTariffTypeCode
    {
        public EnumEnergyTariffTypeCode()
        {
            MduRegisters = new HashSet<MduRegister>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MduRegister> MduRegisters { get; set; }
    }
}
