﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EnumMutationReasonCode
    {
        public EnumMutationReasonCode()
        {
            MduMutations = new HashSet<MduMutation>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MduMutation> MduMutations { get; set; }
    }
}
