﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class EnumProcessStatus
    {
        public EnumProcessStatus()
        {
            MduHistories = new HashSet<MduHistory>();
            MduStatusHistories = new HashSet<MduStatusHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MduHistory> MduHistories { get; set; }
        public virtual ICollection<MduStatusHistory> MduStatusHistories { get; set; }
    }
}
