﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class GridArea
    {
        public GridArea()
        {
            MduHistories = new HashSet<MduHistory>();
        }

        public short Id { get; set; }
        public string Ean { get; set; } = null!;

        public virtual ICollection<MduHistory> MduHistories { get; set; }
    }
}
