﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            MduHistoryBalanceResponsibleParties = new HashSet<MduHistory>();
            MduHistoryBalanceSuppliers = new HashSet<MduHistory>();
            MduHistoryGridOperators = new HashSet<MduHistory>();
            MduHistoryMeteringResponsibleParties = new HashSet<MduHistory>();
        }

        public short Id { get; set; }
        public string Ean { get; set; } = null!;

        public virtual ICollection<MduHistory> MduHistoryBalanceResponsibleParties { get; set; }
        public virtual ICollection<MduHistory> MduHistoryBalanceSuppliers { get; set; }
        public virtual ICollection<MduHistory> MduHistoryGridOperators { get; set; }
        public virtual ICollection<MduHistory> MduHistoryMeteringResponsibleParties { get; set; }
    }
}
