﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MasterDataBatch
    {
        public MasterDataBatch()
        {
            MasterDataBatchStatusHistories = new HashSet<MasterDataBatchStatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; } = null!;
        public byte StatusId { get; set; }
        public string GridOperatorEan { get; set; } = null!;
        public string BalanceSupplierEan { get; set; } = null!;
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual DictionaryMasterDataBatchStatus Status { get; set; } = null!;
        public virtual ICollection<MasterDataBatchStatusHistory> MasterDataBatchStatusHistories { get; set; }
    }
}
