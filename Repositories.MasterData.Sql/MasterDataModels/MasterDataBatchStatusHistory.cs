﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MasterDataBatchStatusHistory
    {
        public int Id { get; set; }
        public int MasterDataBatchId { get; set; }
        public byte StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string? Comment { get; set; }

        public virtual MasterDataBatch MasterDataBatch { get; set; } = null!;
        public virtual DictionaryMasterDataBatchStatus Status { get; set; } = null!;
    }
}
