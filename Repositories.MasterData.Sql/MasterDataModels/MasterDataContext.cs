﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MasterDataContext : DbContext
    {
        private readonly string _connectionString;

        public MasterDataContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MasterDataContext(DbContextOptions<MasterDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CapTarCode> CapTarCodes { get; set; } = null!;
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevels { get; set; } = null!;
        public virtual DbSet<DictionaryMasterDataBatchStatus> DictionaryMasterDataBatchStatuses { get; set; } = null!;
        public virtual DbSet<EdsnRejection> EdsnRejections { get; set; } = null!;
        public virtual DbSet<EnumCommunicationStatusCode> EnumCommunicationStatusCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyAllocationMethodCode> EnumEnergyAllocationMethodCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyConnectionPhysicalStatusCode> EnumEnergyConnectionPhysicalStatusCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyDeliveryStatusCode> EnumEnergyDeliveryStatusCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyFlowDirectionCode> EnumEnergyFlowDirectionCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyMeterTypeCode> EnumEnergyMeterTypeCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyMeteringMethodCode> EnumEnergyMeteringMethodCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyProductTypeCode> EnumEnergyProductTypeCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyTariffTypeCode> EnumEnergyTariffTypeCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyUsageProfileCode> EnumEnergyUsageProfileCodes { get; set; } = null!;
        public virtual DbSet<EnumMarketSegmentCode> EnumMarketSegmentCodes { get; set; } = null!;
        public virtual DbSet<EnumMutationReasonCode> EnumMutationReasonCodes { get; set; } = null!;
        public virtual DbSet<EnumPhysicalCapacityCode> EnumPhysicalCapacityCodes { get; set; } = null!;
        public virtual DbSet<EnumProcessStatus> EnumProcessStatuses { get; set; } = null!;
        public virtual DbSet<GridArea> GridAreas { get; set; } = null!;
        public virtual DbSet<MarketParty> MarketParties { get; set; } = null!;
        public virtual DbSet<MasterDataBatch> MasterDataBatches { get; set; } = null!;
        public virtual DbSet<MasterDataBatchStatusHistory> MasterDataBatchStatusHistories { get; set; } = null!;
        public virtual DbSet<MduHistory> MduHistories { get; set; } = null!;
        public virtual DbSet<MduMutation> MduMutations { get; set; } = null!;
        public virtual DbSet<MduRegister> MduRegisters { get; set; } = null!;
        public virtual DbSet<MduStatusHistory> MduStatusHistories { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.ToTable("CapTarCode", "dbo");

                entity.HasIndex(e => e.Ean, "IX_CapTarCode_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMasterDataBatchStatus>(entity =>
            {
                entity.ToTable("Dictionary_MasterDataBatchStatus", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.ToTable("EdsnRejection", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code).HasMaxLength(10);

                entity.HasOne(d => d.RejectionLevel)
                    .WithMany(p => p.EdsnRejections)
                    .HasForeignKey(d => d.RejectionLevelId)
                    .HasConstraintName("FK_dbo.EdsnRejection_dbo.Dictionary_EdsnRejectionLevel_RejectionLevelId");
            });

            modelBuilder.Entity<EnumCommunicationStatusCode>(entity =>
            {
                entity.ToTable("EnumCommunicationStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumCommunicationStatusCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyAllocationMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyAllocationMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyAllocationMethodCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyConnectionPhysicalStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyConnectionPhysicalStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyConnectionPhysicalStatusCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyDeliveryStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyDeliveryStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyDeliveryStatusCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("EnumEnergyFlowDirectionCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyFlowDirectionCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeterTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeterTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyMeterTypeCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeteringMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeteringMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyMeteringMethodCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyProductTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyProductTypeCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyTariffTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyTariffTypeCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("EnumEnergyUsageProfileCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyUsageProfileCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMarketSegmentCode>(entity =>
            {
                entity.ToTable("EnumMarketSegmentCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumMarketSegmentCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMutationReasonCode>(entity =>
            {
                entity.ToTable("EnumMutationReasonCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumMutationReasonCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumPhysicalCapacityCode>(entity =>
            {
                entity.ToTable("EnumPhysicalCapacityCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumPhysicalCapacityCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumProcessStatus>(entity =>
            {
                entity.ToTable("EnumProcessStatus", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumProcessStatus_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.ToTable("GridArea", "dbo");

                entity.HasIndex(e => e.Ean, "IX_GridArea_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(20);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean, "IX_MarketParty_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);
            });

            modelBuilder.Entity<MasterDataBatch>(entity =>
            {
                entity.ToTable("MasterDataBatch", "dbo");

                entity.Property(e => e.BalanceSupplierEan).HasMaxLength(13);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.GridOperatorEan).HasMaxLength(13);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MasterDataBatches)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MasterDataBatchStatus_dbo.Dictionary_MasterDataBatchStatus_StatusId");
            });

            modelBuilder.Entity<MasterDataBatchStatusHistory>(entity =>
            {
                entity.ToTable("MasterDataBatchStatusHistory", "dbo");

                entity.HasIndex(e => e.MasterDataBatchId, "IX_MasterDataBatchStatusHistory_MasterDataBatchId");

                entity.HasOne(d => d.MasterDataBatch)
                    .WithMany(p => p.MasterDataBatchStatusHistories)
                    .HasForeignKey(d => d.MasterDataBatchId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MasterDataBatchStatusHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MasterDataBatchStatusHistory_dbo.Dictionary_MasterDataBatchStatus_StatusId");
            });

            modelBuilder.Entity<MduHistory>(entity =>
            {
                entity.ToTable("MduHistory", "dbo");

                entity.HasIndex(e => e.EanId, "IX_EanId");

                entity.HasIndex(e => e.AllocationMethodId, "IX_MduHistory_AllocationMethodId");

                entity.HasIndex(e => e.BalanceResponsiblePartyId, "IX_MduHistory_BalanceResponsiblePartyId");

                entity.HasIndex(e => e.BalanceSupplierId, "IX_MduHistory_BalanceSupplierId");

                entity.HasIndex(e => e.CapTarCodeId, "IX_MduHistory_CapTarCodeId");

                entity.HasIndex(e => e.CommunicationStatusCodeId, "IX_MduHistory_CommunicationStatusCodeId");

                entity.HasIndex(e => e.EnergyDeliveryStatusId, "IX_MduHistory_EnergyDeliveryStatusId");

                entity.HasIndex(e => e.EnergyFlowDirectionId, "IX_MduHistory_EnergyFlowDirectionId");

                entity.HasIndex(e => e.GridAreaId, "IX_MduHistory_GridAreaId");

                entity.HasIndex(e => e.GridOperatorId, "IX_MduHistory_GridOperatorId");

                entity.HasIndex(e => e.MarketSegmentId, "IX_MduHistory_MarketSegmentId");

                entity.HasIndex(e => e.MeteringMethodId, "IX_MduHistory_MeteringMethodId");

                entity.HasIndex(e => e.MeteringResponsiblePartyId, "IX_MduHistory_MeteringResponsiblePartyId");

                entity.HasIndex(e => e.PhysicalCapacityId, "IX_MduHistory_PhysicalCapacityId");

                entity.HasIndex(e => e.PhysicalStatusId, "IX_MduHistory_PhysicalStatusId");

                entity.HasIndex(e => e.ProductTypeId, "IX_MduHistory_ProductTypeId");

                entity.HasIndex(e => e.ProfileCategoryId, "IX_MduHistory_ProfileCategoryId");

                entity.HasIndex(e => e.TypeId, "IX_MduHistory_TypeId");

                entity.HasIndex(e => new { e.StatusId, e.MutationDate }, "IX_StatusId_MutationDate");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.Papean)
                    .HasMaxLength(18)
                    .HasColumnName("PAPEan");

                entity.Property(e => e.Sapeans)
                    .HasMaxLength(189)
                    .HasColumnName("SAPEans");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(10)
                    .HasColumnName("ZIPCode");

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.AllocationMethodId);

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MduHistoryBalanceResponsibleParties)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId);

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MduHistoryBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatusCode)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.CommunicationStatusCodeId);

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MduHistoryGridOperators)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MduHistoryMeteringResponsibleParties)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId);

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.PhysicalCapacityId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.ProfileCategoryId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MduHistories)
                    .HasForeignKey(d => d.TypeId);
            });

            modelBuilder.Entity<MduMutation>(entity =>
            {
                entity.ToTable("MduMutation", "dbo");

                entity.HasIndex(e => e.MduHistoryId, "IX_MduHistoryId");

                entity.HasIndex(e => new { e.MutationReasonId, e.MduHistoryId }, "IX_ReasonId_MduHistoryId");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduMutations)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.MduMutations)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MduRegister>(entity =>
            {
                entity.ToTable("MduRegister", "dbo");

                entity.HasIndex(e => e.MduHistoryId, "IX_MduHistoryId");

                entity.HasIndex(e => e.MeteringDirectionId, "IX_MduRegister_MeteringDirectionId");

                entity.HasIndex(e => e.TariffTypeId, "IX_MduRegister_TariffTypeId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.MduRegisters)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MduStatusHistory>(entity =>
            {
                entity.ToTable("MduStatusHistory", "dbo");

                entity.HasIndex(e => e.MduHistoryId, "IX_MduHistoryId");

                entity.HasIndex(e => e.ProcessStatusId, "IX_MduStatusHistory_ProcessStatusId");

                entity.Property(e => e.User).HasMaxLength(100);

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduStatusHistories)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.MduStatusHistories)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
