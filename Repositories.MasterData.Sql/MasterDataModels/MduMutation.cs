﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MduMutation
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public string? ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public short MutationReasonId { get; set; }
        public string? DossierId { get; set; }

        public virtual MduHistory MduHistory { get; set; } = null!;
        public virtual EnumMutationReasonCode MutationReason { get; set; } = null!;
    }
}
