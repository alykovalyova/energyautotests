﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MduRegister
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public string? EdsnId { get; set; }
        public short TariffTypeId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short NrOfDigits { get; set; }
        public decimal? MultiplicationFactor { get; set; }

        public virtual MduHistory MduHistory { get; set; } = null!;
        public virtual EnumEnergyFlowDirectionCode MeteringDirection { get; set; } = null!;
        public virtual EnumEnergyTariffTypeCode TariffType { get; set; } = null!;
    }
}
