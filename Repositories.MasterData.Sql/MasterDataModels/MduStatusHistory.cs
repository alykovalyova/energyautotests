﻿using System;
using System.Collections.Generic;

namespace Repositories.MasterData.Sql.MasterDataModels
{
    public partial class MduStatusHistory
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public short ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string? Comment { get; set; }
        public string? User { get; set; }

        public virtual MduHistory MduHistory { get; set; } = null!;
        public virtual EnumProcessStatus ProcessStatus { get; set; } = null!;
    }
}
