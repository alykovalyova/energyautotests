﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class EnumCommunicationStatusCode
    {
        public EnumCommunicationStatusCode()
        {
            MeteringPointHistories = new HashSet<MeteringPointHistory>();
            MeteringPoints = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoints = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MeteringPointHistory> MeteringPointHistories { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; }
    }
}
