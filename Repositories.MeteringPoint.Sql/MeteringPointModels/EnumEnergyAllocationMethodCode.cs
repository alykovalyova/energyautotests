﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class EnumEnergyAllocationMethodCode
    {
        public EnumEnergyAllocationMethodCode()
        {
            MeteringPointHistories = new HashSet<MeteringPointHistory>();
            MeteringPoints = new HashSet<MeteringPoint>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MeteringPointHistory> MeteringPointHistories { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
    }
}
