﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class EnumEnergyFlowDirectionCode
    {
        public EnumEnergyFlowDirectionCode()
        {
            MeteringPointHistories = new HashSet<MeteringPointHistory>();
            MeteringPoints = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoints = new HashSet<PreSwitchMeteringPoint>();
            PreSwitchRegisters = new HashSet<PreSwitchRegister>();
            RegisterHistories = new HashSet<RegisterHistory>();
            Registers = new HashSet<Register>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<MeteringPointHistory> MeteringPointHistories { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; }
        public virtual ICollection<PreSwitchRegister> PreSwitchRegisters { get; set; }
        public virtual ICollection<RegisterHistory> RegisterHistories { get; set; }
        public virtual ICollection<Register> Registers { get; set; }
    }
}
