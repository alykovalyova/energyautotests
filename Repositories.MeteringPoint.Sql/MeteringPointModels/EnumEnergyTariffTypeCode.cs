﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class EnumEnergyTariffTypeCode
    {
        public EnumEnergyTariffTypeCode()
        {
            PreSwitchRegisters = new HashSet<PreSwitchRegister>();
            RegisterHistories = new HashSet<RegisterHistory>();
            Registers = new HashSet<Register>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; } = null!;

        public virtual ICollection<PreSwitchRegister> PreSwitchRegisters { get; set; }
        public virtual ICollection<RegisterHistory> RegisterHistories { get; set; }
        public virtual ICollection<Register> Registers { get; set; }
    }
}
