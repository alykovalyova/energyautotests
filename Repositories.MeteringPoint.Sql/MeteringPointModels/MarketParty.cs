﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            MeteringPointBalanceResponsibleParties = new HashSet<MeteringPoint>();
            MeteringPointBalanceSuppliers = new HashSet<MeteringPoint>();
            MeteringPointGridOperators = new HashSet<MeteringPoint>();
            MeteringPointHistoryBalanceResponsibleParties = new HashSet<MeteringPointHistory>();
            MeteringPointHistoryBalanceSuppliers = new HashSet<MeteringPointHistory>();
            MeteringPointHistoryGridOperators = new HashSet<MeteringPointHistory>();
            MeteringPointHistoryMeteringResponsibleParties = new HashSet<MeteringPointHistory>();
            MeteringPointMeteringResponsibleParties = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoints = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string? Ean { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPointBalanceResponsibleParties { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointBalanceSuppliers { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointGridOperators { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryBalanceResponsibleParties { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryBalanceSuppliers { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryGridOperators { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryMeteringResponsibleParties { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointMeteringResponsibleParties { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; }
    }
}
