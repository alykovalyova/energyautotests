﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class MeteringPointContext : DbContext
    {
        private readonly string _connectionString;

        public MeteringPointContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MeteringPointContext(DbContextOptions<MeteringPointContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; } = null!;
        public virtual DbSet<AddressHistory> AddressHistories { get; set; } = null!;
        public virtual DbSet<CapTarCode> CapTarCodes { get; set; } = null!;
        public virtual DbSet<EnumCommunicationStatusCode> EnumCommunicationStatusCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyAllocationMethodCode> EnumEnergyAllocationMethodCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyConnectionPhysicalStatusCode> EnumEnergyConnectionPhysicalStatusCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyDeliveryStatusCode> EnumEnergyDeliveryStatusCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyFlowDirectionCode> EnumEnergyFlowDirectionCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyMeterTypeCode> EnumEnergyMeterTypeCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyMeteringMethodCode> EnumEnergyMeteringMethodCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyProductTypeCode> EnumEnergyProductTypeCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyTariffTypeCode> EnumEnergyTariffTypeCodes { get; set; } = null!;
        public virtual DbSet<EnumEnergyUsageProfileCode> EnumEnergyUsageProfileCodes { get; set; } = null!;
        public virtual DbSet<EnumMarketSegmentCode> EnumMarketSegmentCodes { get; set; } = null!;
        public virtual DbSet<EnumPhysicalCapacityCode> EnumPhysicalCapacityCodes { get; set; } = null!;
        public virtual DbSet<GridArea> GridAreas { get; set; } = null!;
        public virtual DbSet<MarketParty> MarketParties { get; set; } = null!;
        public virtual DbSet<MeteringPoint> MeteringPoints { get; set; } = null!;
        public virtual DbSet<MeteringPointHistory> MeteringPointHistories { get; set; } = null!;
        public virtual DbSet<PreSwitchMeteringPoint> PreSwitchMeteringPoints { get; set; } = null!;
        public virtual DbSet<PreSwitchRegister> PreSwitchRegisters { get; set; } = null!;
        public virtual DbSet<ProcLog> ProcLogs { get; set; } = null!;
        public virtual DbSet<Register> Registers { get; set; } = null!;
        public virtual DbSet<RegisterHistory> RegisterHistories { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address", "dbo");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Zipcode, e.BuildingNr, e.ExBuildingNr }, "Search_ZipCode_BuildingNr");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(10)
                    .HasColumnName("ZIPCode");
            });

            modelBuilder.Entity<AddressHistory>(entity =>
            {
                entity.ToTable("AddressHistory", "dbo");

                entity.HasIndex(e => e.AddressId, "IX_AddressId");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.User).HasMaxLength(100);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(10)
                    .HasColumnName("ZIPCode");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.AddressHistories)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.ToTable("CapTarCode", "dbo");

                entity.HasIndex(e => e.Ean, "IX_CapTarCode_Ean")
                    .IsUnique()
                    .HasFilter("([Ean] IS NOT NULL)");
            });

            modelBuilder.Entity<EnumCommunicationStatusCode>(entity =>
            {
                entity.ToTable("EnumCommunicationStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumCommunicationStatusCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyAllocationMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyAllocationMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyAllocationMethodCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyConnectionPhysicalStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyConnectionPhysicalStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyConnectionPhysicalStatusCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyDeliveryStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyDeliveryStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyDeliveryStatusCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("EnumEnergyFlowDirectionCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyFlowDirectionCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeterTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeterTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyMeterTypeCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeteringMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeteringMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyMeteringMethodCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyProductTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyProductTypeCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyTariffTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyTariffTypeCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("EnumEnergyUsageProfileCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumEnergyUsageProfileCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMarketSegmentCode>(entity =>
            {
                entity.ToTable("EnumMarketSegmentCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumMarketSegmentCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<EnumPhysicalCapacityCode>(entity =>
            {
                entity.ToTable("EnumPhysicalCapacityCode", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumPhysicalCapacityCode_Identifier")
                    .IsUnique();

                entity.Property(e => e.Identifier).HasMaxLength(50);
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.ToTable("GridArea", "dbo");

                entity.HasIndex(e => e.Ean, "IX_GridArea_Ean")
                    .IsUnique()
                    .HasFilter("([Ean] IS NOT NULL)");
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean, "IX_MarketParty_Ean")
                    .IsUnique()
                    .HasFilter("([Ean] IS NOT NULL)");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint", "dbo");

                entity.HasIndex(e => new { e.BalanceSupplierId, e.PhysicalStatusId }, "IX_BalanceSupplierId_PhysicalStatusId");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique()
                    .HasFilter("([EanId] IS NOT NULL)");

                entity.HasIndex(e => e.AddressId, "IX_MeteringPoint_AddressId");

                entity.HasIndex(e => e.AllocationMethodId, "IX_MeteringPoint_AllocationMethodId");

                entity.HasIndex(e => e.BalanceResponsiblePartyId, "IX_MeteringPoint_BalanceResponsiblePartyId");

                entity.HasIndex(e => e.CapTarCodeId, "IX_MeteringPoint_CapTarCodeId");

                entity.HasIndex(e => e.CommunicationStatusId, "IX_MeteringPoint_CommunicationStatusId");

                entity.HasIndex(e => e.EnergyDeliveryStatusId, "IX_MeteringPoint_EnergyDeliveryStatusId");

                entity.HasIndex(e => e.EnergyFlowDirectionId, "IX_MeteringPoint_EnergyFlowDirectionId");

                entity.HasIndex(e => e.GridAreaId, "IX_MeteringPoint_GridAreaId");

                entity.HasIndex(e => e.GridOperatorId, "IX_MeteringPoint_GridOperatorId");

                entity.HasIndex(e => e.MarketSegmentId, "IX_MeteringPoint_MarketSegmentId");

                entity.HasIndex(e => e.MeteringMethodId, "IX_MeteringPoint_MeteringMethodId");

                entity.HasIndex(e => e.MeteringResponsiblePartyId, "IX_MeteringPoint_MeteringResponsiblePartyId");

                entity.HasIndex(e => e.PhysicalCapacityId, "IX_MeteringPoint_PhysicalCapacityId");

                entity.HasIndex(e => e.PhysicalStatusId, "IX_MeteringPoint_PhysicalStatusId");

                entity.HasIndex(e => e.ProductTypeId, "IX_MeteringPoint_ProductTypeId");

                entity.HasIndex(e => e.ProfileCategoryId, "IX_MeteringPoint_ProfileCategoryId");

                entity.HasIndex(e => e.TypeId, "IX_MeteringPoint_TypeId");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.HeaderCreationDate).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.Papean)
                    .HasMaxLength(18)
                    .HasColumnName("PAPEan");

                entity.Property(e => e.Sapeans)
                    .HasMaxLength(189)
                    .HasColumnName("SAPEans");

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.AllocationMethodId);

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MeteringPointBalanceResponsibleParties)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId);

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MeteringPointBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPointGridOperators)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MeteringPointMeteringResponsibleParties)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId);

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.PhysicalCapacityId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.ProfileCategoryId);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.TypeId);
            });

            modelBuilder.Entity<MeteringPointHistory>(entity =>
            {
                entity.ToTable("MeteringPointHistory", "dbo");

                entity.HasIndex(e => e.EanId, "IX_EanId");

                entity.HasIndex(e => new { e.EanId, e.ValidFrom }, "IX_EanId_ValidFrom");

                entity.HasIndex(e => e.AddressId, "IX_MeteringPointHistory_AddressId");

                entity.HasIndex(e => e.AllocationMethodId, "IX_MeteringPointHistory_AllocationMethodId");

                entity.HasIndex(e => e.BalanceResponsiblePartyId, "IX_MeteringPointHistory_BalanceResponsiblePartyId");

                entity.HasIndex(e => e.BalanceSupplierId, "IX_MeteringPointHistory_BalanceSupplierId");

                entity.HasIndex(e => e.CapTarCodeId, "IX_MeteringPointHistory_CapTarCodeId");

                entity.HasIndex(e => e.CommunicationStatusId, "IX_MeteringPointHistory_CommunicationStatusId");

                entity.HasIndex(e => e.EnergyDeliveryStatusId, "IX_MeteringPointHistory_EnergyDeliveryStatusId");

                entity.HasIndex(e => e.EnergyFlowDirectionId, "IX_MeteringPointHistory_EnergyFlowDirectionId");

                entity.HasIndex(e => e.GridAreaId, "IX_MeteringPointHistory_GridAreaId");

                entity.HasIndex(e => e.GridOperatorId, "IX_MeteringPointHistory_GridOperatorId");

                entity.HasIndex(e => e.MarketSegmentId, "IX_MeteringPointHistory_MarketSegmentId");

                entity.HasIndex(e => e.MeteringMethodId, "IX_MeteringPointHistory_MeteringMethodId");

                entity.HasIndex(e => e.MeteringResponsiblePartyId, "IX_MeteringPointHistory_MeteringResponsiblePartyId");

                entity.HasIndex(e => e.PhysicalCapacityId, "IX_MeteringPointHistory_PhysicalCapacityId");

                entity.HasIndex(e => e.PhysicalStatusId, "IX_MeteringPointHistory_PhysicalStatusId");

                entity.HasIndex(e => e.ProductTypeId, "IX_MeteringPointHistory_ProductTypeId");

                entity.HasIndex(e => e.ProfileCategoryId, "IX_MeteringPointHistory_ProfileCategoryId");

                entity.HasIndex(e => e.TypeId, "IX_MeteringPointHistory_TypeId");

                entity.HasIndex(e => e.MeteringPointId, "IX_MeteringPointId");

                entity.HasIndex(e => new { e.MeteringPointId, e.ValidFrom }, "IX_MeteringPointId_ValidFrom");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.HeaderCreationDate).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.Papean)
                    .HasMaxLength(18)
                    .HasColumnName("PAPEan");

                entity.Property(e => e.Sapeans)
                    .HasMaxLength(189)
                    .HasColumnName("SAPEans");

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.AllocationMethodId);

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MeteringPointHistoryBalanceResponsibleParties)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId);

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MeteringPointHistoryBalanceSuppliers)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPointHistoryGridOperators)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MeteringPointHistoryMeteringResponsibleParties)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId);

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.PhysicalCapacityId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.ProfileCategoryId);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MeteringPointHistories)
                    .HasForeignKey(d => d.TypeId);
            });

            modelBuilder.Entity<PreSwitchMeteringPoint>(entity =>
            {
                entity.ToTable("PreSwitchMeteringPoint", "dbo");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique()
                    .HasFilter("([EanId] IS NOT NULL)");

                entity.HasIndex(e => new { e.EanId, e.ValidDate }, "IX_EanId_ValidDate");

                entity.HasIndex(e => e.AddressId, "IX_PreSwitchMeteringPoint_AddressId");

                entity.HasIndex(e => e.CapTarCodeId, "IX_PreSwitchMeteringPoint_CapTarCodeId");

                entity.HasIndex(e => e.CommunicationStatusId, "IX_PreSwitchMeteringPoint_CommunicationStatusId");

                entity.HasIndex(e => e.EnergyFlowDirectionId, "IX_PreSwitchMeteringPoint_EnergyFlowDirectionId");

                entity.HasIndex(e => e.GridAreaId, "IX_PreSwitchMeteringPoint_GridAreaId");

                entity.HasIndex(e => e.GridOperatorId, "IX_PreSwitchMeteringPoint_GridOperatorId");

                entity.HasIndex(e => e.MarketSegmentId, "IX_PreSwitchMeteringPoint_MarketSegmentId");

                entity.HasIndex(e => e.MeteringMethodId, "IX_PreSwitchMeteringPoint_MeteringMethodId");

                entity.HasIndex(e => e.PhysicalStatusId, "IX_PreSwitchMeteringPoint_PhysicalStatusId");

                entity.HasIndex(e => e.ProductTypeId, "IX_PreSwitchMeteringPoint_ProductTypeId");

                entity.HasIndex(e => e.ProfileCategoryId, "IX_PreSwitchMeteringPoint_ProfileCategoryId");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.ValidDate).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.MarketSegmentId);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.MeteringMethodId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.PhysicalStatusId);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.PreSwitchMeteringPoints)
                    .HasForeignKey(d => d.ProfileCategoryId);
            });

            modelBuilder.Entity<PreSwitchRegister>(entity =>
            {
                entity.ToTable("PreSwitchRegister", "dbo");

                entity.HasIndex(e => e.MeteringDirectionId, "IX_PreSwitchRegister_MeteringDirectionId");

                entity.HasIndex(e => e.PreSwitchMeteringPointId, "IX_PreSwitchRegister_PreSwitchMeteringPointId");

                entity.HasIndex(e => e.TariffTypeId, "IX_PreSwitchRegister_TariffTypeId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.PreSwitchRegisters)
                    .HasForeignKey(d => d.MeteringDirectionId);

                entity.HasOne(d => d.PreSwitchMeteringPoint)
                    .WithMany(p => p.PreSwitchRegisters)
                    .HasForeignKey(d => d.PreSwitchMeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.PreSwitchRegisters)
                    .HasForeignKey(d => d.TariffTypeId);
            });

            modelBuilder.Entity<ProcLog>(entity =>
            {
                entity.HasKey(e => e.Oid)
                    .HasName("PK_proclog");

                entity.ToTable("ProcLog", "dbo");

                entity.Property(e => e.Oid).HasColumnName("OID");

                entity.Property(e => e.ErrorNumber).HasDefaultValueSql("((0))");

                entity.Property(e => e.Message)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.ObjectName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RowCount).HasDefaultValueSql("((0))");

                entity.Property(e => e.RunDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RunUser)
                    .HasMaxLength(128)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(suser_sname())");

                entity.Property(e => e.ServerName)
                    .HasMaxLength(128)
                    .HasDefaultValueSql("(CONVERT([varchar](50),serverproperty('ServerName')))");

                entity.Property(e => e.StepDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.ToTable("Register", "dbo");

                entity.HasIndex(e => e.MeteringPointId, "IX_MeteringPointId");

                entity.HasIndex(e => e.MeteringDirectionId, "IX_Register_MeteringDirectionId");

                entity.HasIndex(e => e.TariffTypeId, "IX_Register_TariffTypeId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Registers)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RegisterHistory>(entity =>
            {
                entity.ToTable("RegisterHistory", "dbo");

                entity.HasIndex(e => e.MeteringPointHistoryId, "IX_MeteringPointHistoryId");

                entity.HasIndex(e => e.MeteringDirectionId, "IX_RegisterHistory_MeteringDirectionId");

                entity.HasIndex(e => e.TariffTypeId, "IX_RegisterHistory_TariffTypeId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.RegisterHistories)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPointHistory)
                    .WithMany(p => p.RegisterHistories)
                    .HasForeignKey(d => d.MeteringPointHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.RegisterHistories)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
