﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class ProcLog
    {
        public int Oid { get; set; }
        public string ServerName { get; set; } = null!;
        public DateTime? RunDate { get; set; }
        public string? StepDescription { get; set; }
        public string? ObjectName { get; set; }
        public string? Message { get; set; }
        public long? RowCount { get; set; }
        public int? ErrorNumber { get; set; }
        public string? RunUser { get; set; }
    }
}
