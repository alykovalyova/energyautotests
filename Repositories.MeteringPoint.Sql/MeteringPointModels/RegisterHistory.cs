﻿using System;
using System.Collections.Generic;

namespace Repositories.MeteringPoint.Sql.MeteringPointModels
{
    public partial class RegisterHistory
    {
        public int Id { get; set; }
        public string? EdsnId { get; set; }
        public short TariffTypeId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short NrOfDigits { get; set; }
        public decimal? MultiplicationFactor { get; set; }
        public int MeteringPointHistoryId { get; set; }

        public virtual EnumEnergyFlowDirectionCode MeteringDirection { get; set; } = null!;
        public virtual MeteringPointHistory MeteringPointHistory { get; set; } = null!;
        public virtual EnumEnergyTariffTypeCode TariffType { get; set; } = null!;
    }
}
