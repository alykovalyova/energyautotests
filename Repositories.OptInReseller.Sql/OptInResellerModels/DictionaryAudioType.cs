﻿using System;
using System.Collections.Generic;

namespace Repositories.OptInReseller.Sql.OptInResellerModels
{
    public partial class DictionaryAudioType
    {
        public DictionaryAudioType()
        {
            VoiceLogs = new HashSet<VoiceLog>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<VoiceLog> VoiceLogs { get; set; }
    }
}
