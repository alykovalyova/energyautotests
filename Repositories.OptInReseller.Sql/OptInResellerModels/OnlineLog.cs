﻿using System;
using System.Collections.Generic;

namespace Repositories.OptInReseller.Sql.OptInResellerModels
{
    public partial class OnlineLog
    {
        public OnlineLog()
        {
            OptIns = new HashSet<OptIn>();
        }

        public int Id { get; set; }
        public string OnlineExternalReference { get; set; }
        public string IpAddress { get; set; }
        public string CephReference { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual ICollection<OptIn> OptIns { get; set; }
    }
}
