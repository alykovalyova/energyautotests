﻿using System;
using System.Collections.Generic;

namespace Repositories.OptInReseller.Sql.OptInResellerModels
{
    public partial class OptIn
    {
        public OptIn()
        {
            VoiceLogs = new HashSet<VoiceLog>();
        }

        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public DateTime OptInDate { get; set; }
        public int SalesChannelId { get; set; }
        public int CustomerId { get; set; }
        public int? OnlineLogId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual OnlineLog OnlineLog { get; set; }
        public virtual ICollection<VoiceLog> VoiceLogs { get; set; }
    }
}
