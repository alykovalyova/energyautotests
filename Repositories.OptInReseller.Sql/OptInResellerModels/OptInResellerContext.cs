﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.OptInReseller.Sql.OptInResellerModels
{
    public partial class OptInResellerContext : DbContext
    {
        public OptInResellerContext()
        {
        }

        public OptInResellerContext(DbContextOptions<OptInResellerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DictionaryAudioType> DictionaryAudioTypes { get; set; }
        public virtual DbSet<DictionaryVoiceLogStatus> DictionaryVoiceLogStatuses { get; set; }
        public virtual DbSet<OnlineLog> OnlineLogs { get; set; }
        public virtual DbSet<OptIn> OptIns { get; set; }
        public virtual DbSet<VoiceLog> VoiceLogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=OptInReseller;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");

                entity.HasIndex(e => e.NutsHomeCustomerNumber, "IX_Customer_NutsHomeCustomerNumber");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.HouseNumberExtension).HasMaxLength(10);

                entity.Property(e => e.Initials).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.PostalCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Street).HasMaxLength(100);

                entity.Property(e => e.Surname).HasMaxLength(100);

                entity.Property(e => e.SurnamePrefix).HasMaxLength(10);
            });

            modelBuilder.Entity<DictionaryAudioType>(entity =>
            {
                entity.ToTable("Dictionary_AudioType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryVoiceLogStatus>(entity =>
            {
                entity.ToTable("Dictionary_VoiceLogStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<OnlineLog>(entity =>
            {
                entity.ToTable("OnlineLog");

                entity.Property(e => e.CephReference)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.IpAddress).HasMaxLength(45);

                entity.Property(e => e.OnlineExternalReference)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<OptIn>(entity =>
            {
                entity.ToTable("OptIn");

                entity.HasIndex(e => e.CustomerId, "IX_OptIn_CustomerId");

                entity.HasIndex(e => e.ExternalReference, "IX_OptIn_ExternalReference")
                    .IsUnique();

                entity.HasIndex(e => new { e.ExternalReference, e.SalesChannelId }, "IX_OptIn_ExternalReference_SalesChannelId");

                entity.HasIndex(e => e.OnlineLogId, "IX_OptIn_OnlineLogId");

                entity.Property(e => e.ExternalReference)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.OptIns)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.OnlineLog)
                    .WithMany(p => p.OptIns)
                    .HasForeignKey(d => d.OnlineLogId);
            });

            modelBuilder.Entity<VoiceLog>(entity =>
            {
                entity.ToTable("VoiceLog");

                entity.HasIndex(e => e.AudioTypeId, "IX_VoiceLog_AudioTypeId");

                entity.HasIndex(e => e.OptInId, "IX_VoiceLog_OptInId");

                entity.HasIndex(e => e.StatusId, "IX_VoiceLog_StatusId");

                entity.Property(e => e.StatusId).HasDefaultValueSql("(CONVERT([smallint],(0)))");

                entity.Property(e => e.VoiceExternalReference)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.AudioType)
                    .WithMany(p => p.VoiceLogs)
                    .HasForeignKey(d => d.AudioTypeId);

                entity.HasOne(d => d.OptIn)
                    .WithMany(p => p.VoiceLogs)
                    .HasForeignKey(d => d.OptInId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.VoiceLogs)
                    .HasForeignKey(d => d.StatusId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
