﻿using System;
using System.Collections.Generic;

namespace Repositories.OptInReseller.Sql.OptInResellerModels
{
    public partial class VoiceLog
    {
        public int Id { get; set; }
        public string VoiceExternalReference { get; set; }
        public short AudioTypeId { get; set; }
        public string CephReference { get; set; }
        public int OptInId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public short StatusId { get; set; }

        public virtual DictionaryAudioType AudioType { get; set; }
        public virtual OptIn OptIn { get; set; }
        public virtual DictionaryVoiceLogStatus Status { get; set; }
    }
}
