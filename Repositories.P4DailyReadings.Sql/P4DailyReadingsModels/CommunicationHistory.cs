﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class CommunicationHistory
    {
        public CommunicationHistory()
        {
            StatusHistories = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public DateTime ReadingDate { get; set; }
        public int MeteringPointId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual MeteringPoint MeteringPoint { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
