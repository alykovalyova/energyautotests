﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class DictionaryRejectionCode
    {
        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
