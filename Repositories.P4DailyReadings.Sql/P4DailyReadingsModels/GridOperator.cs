﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class GridOperator
    {
        public GridOperator()
        {
            MeteringPoints = new HashSet<MeteringPoint>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
    }
}
