﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class MandateHistory
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public short MandateTypeId { get; set; }
        public int MeteringPointId { get; set; }

        public virtual DictionaryMandateType MandateType { get; set; }
        public virtual MeteringPoint MeteringPoint { get; set; }
    }
}
