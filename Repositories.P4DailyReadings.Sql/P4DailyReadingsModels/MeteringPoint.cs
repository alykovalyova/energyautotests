﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class MeteringPoint
    {
        public MeteringPoint()
        {
            CommunicationHistories = new HashSet<CommunicationHistory>();
            MandateHistories = new HashSet<MandateHistory>();
            MeteringPointStatusHistories = new HashSet<MeteringPointStatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public int GridOperatorId { get; set; }
        public short ProductTypeId { get; set; }
        public short StatusId { get; set; }
        public string BalanceSupplier { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Allocation { get; set; }
        public bool Mandate { get; set; }
        public bool CallHistoricalData { get; set; }

        public virtual GridOperator GridOperator { get; set; }
        public virtual ICollection<CommunicationHistory> CommunicationHistories { get; set; }
        public virtual ICollection<MandateHistory> MandateHistories { get; set; }
        public virtual ICollection<MeteringPointStatusHistory> MeteringPointStatusHistories { get; set; }
    }
}
