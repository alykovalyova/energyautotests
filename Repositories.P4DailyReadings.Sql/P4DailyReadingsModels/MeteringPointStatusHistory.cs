﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class MeteringPointStatusHistory
    {
        public int Id { get; set; }
        public short StatusId { get; set; }
        public string Comment { get; set; }
        public int MeteringPointId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual MeteringPoint MeteringPoint { get; set; }
    }
}
