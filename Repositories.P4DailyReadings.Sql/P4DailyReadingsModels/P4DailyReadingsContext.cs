﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class P4DailyReadingsContext : DbContext
    {
        public P4DailyReadingsContext()
        {
        }

        public P4DailyReadingsContext(DbContextOptions<P4DailyReadingsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CommunicationHistory> CommunicationHistories { get; set; }
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevels { get; set; }
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCodes { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; }
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCodes { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryMandateType> DictionaryMandateTypes { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCodes { get; set; }
        public virtual DbSet<DictionaryMeterReadingStatus> DictionaryMeterReadingStatuses { get; set; }
        public virtual DbSet<DictionaryMeteringPointStatus> DictionaryMeteringPointStatuses { get; set; }
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatuses { get; set; }
        public virtual DbSet<DictionaryRejectionCode> DictionaryRejectionCodes { get; set; }
        public virtual DbSet<EdsnRejection> EdsnRejections { get; set; }
        public virtual DbSet<GridOperator> GridOperators { get; set; }
        public virtual DbSet<MandateHistory> MandateHistories { get; set; }
        public virtual DbSet<MeterReading> MeterReadings { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoints { get; set; }
        public virtual DbSet<MeteringPointStatusHistory> MeteringPointStatusHistories { get; set; }
        public virtual DbSet<RegisterReading> RegisterReadings { get; set; }
        public virtual DbSet<StatusHistory> StatusHistories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=P4DailyReadings;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<CommunicationHistory>(entity =>
            {
                entity.ToTable("CommunicationHistory");

                entity.HasIndex(e => e.ExternalReference, "IX_ExternalReference");

                entity.HasIndex(e => new { e.MeteringPointId, e.ReadingDate }, "IX_MeteringPointId_ReadingDate");

                entity.Property(e => e.ReadingDate).HasColumnType("date");

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.CommunicationHistories)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMandateType>(entity =>
            {
                entity.ToTable("Dictionary_MandateType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeterReadingStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeterReadingStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeteringPointStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeteringPointStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryRejectionCode>(entity =>
            {
                entity.ToTable("Dictionary_RejectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.ToTable("EdsnRejection");

                entity.HasIndex(e => e.StatusHistoryId, "IX_EdsnRejection_StatusHistoryId");

                entity.HasOne(d => d.StatusHistory)
                    .WithMany(p => p.EdsnRejections)
                    .HasForeignKey(d => d.StatusHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<GridOperator>(entity =>
            {
                entity.ToTable("GridOperator");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();
            });

            modelBuilder.Entity<MandateHistory>(entity =>
            {
                entity.ToTable("MandateHistory");

                entity.HasIndex(e => e.CreatedOn, "IX_CreatedOn");

                entity.HasIndex(e => e.MandateTypeId, "IX_MandateHistory_MandateTypeId");

                entity.HasIndex(e => e.MeteringPointId, "IX_MandateHistory_MeteringPointId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.MandateType)
                    .WithMany(p => p.MandateHistories)
                    .HasForeignKey(d => d.MandateTypeId);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.MandateHistories)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MeterReading>(entity =>
            {
                entity.ToTable("MeterReading");

                entity.HasIndex(e => new { e.EanId, e.EdsnMeterId }, "IX_EanId_EdsnMeterId");

                entity.HasIndex(e => new { e.EanId, e.ReadingDate }, "IX_EanId_ReadingDate");

                entity.Property(e => e.ReadingDate).HasColumnType("date");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => e.GridOperatorId, "IX_MeteringPoint_GridOperatorId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.GridOperatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MeteringPointStatusHistory>(entity =>
            {
                entity.ToTable("MeteringPointStatusHistory");

                entity.HasIndex(e => new { e.MeteringPointId, e.CreatedOn }, "IX_MeteringPointId_CreatedOn");

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.MeteringPointStatusHistories)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RegisterReading>(entity =>
            {
                entity.ToTable("RegisterReading");

                entity.HasIndex(e => e.MeterReadingId, "IX_RegisterReading_MeterReadingId");

                entity.HasIndex(e => new { e.TariffTypeId, e.MeteringDirectionId, e.MeterReadingId }, "IX_TariffType_MeteringDirection")
                    .IsUnique();

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.RegisterReadings)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory");

                entity.HasIndex(e => new { e.CommunicationHistoryId, e.CreatedOn }, "IX_CommunicationHistoryId_CreatedOn");

                entity.HasOne(d => d.CommunicationHistory)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.CommunicationHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
