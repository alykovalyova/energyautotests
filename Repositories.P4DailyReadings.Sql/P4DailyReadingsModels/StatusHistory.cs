﻿using System;
using System.Collections.Generic;

namespace Repositories.P4DailyReadings.Sql.P4DailyReadingsModels
{
    public partial class StatusHistory
    {
        public StatusHistory()
        {
            EdsnRejections = new HashSet<EdsnRejection>();
        }

        public int Id { get; set; }
        public Guid? XmlMessageId { get; set; }
        public DateTime? XmlCreationTs { get; set; }
        public short StatusId { get; set; }
        public int CommunicationHistoryId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual CommunicationHistory CommunicationHistory { get; set; }
        public virtual ICollection<EdsnRejection> EdsnRejections { get; set; }
    }
}
