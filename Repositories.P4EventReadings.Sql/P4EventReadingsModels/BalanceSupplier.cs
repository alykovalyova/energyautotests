﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class BalanceSupplier
    {
        public BalanceSupplier()
        {
            CommunicationHistories = new HashSet<CommunicationHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }

        public virtual ICollection<CommunicationHistory> CommunicationHistories { get; set; }
    }
}
