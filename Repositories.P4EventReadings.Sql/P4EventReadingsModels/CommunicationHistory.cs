﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class CommunicationHistory
    {
        public CommunicationHistory()
        {
            Sources = new HashSet<Source>();
            StatusHistories = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public DateTime ReadingDate { get; set; }
        public DateTime RequestDate { get; set; }
        public int MeteringPointId { get; set; }
        public int BalanceSupplierId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual BalanceSupplier BalanceSupplier { get; set; }
        public virtual MeteringPoint MeteringPoint { get; set; }
        public virtual ICollection<Source> Sources { get; set; }
        public virtual ICollection<StatusHistory> StatusHistories { get; set; }
    }
}
