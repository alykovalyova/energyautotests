﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class DictionaryMeteringPointStatus
    {
        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
