﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class DictionarySource
    {
        public DictionarySource()
        {
            Sources = new HashSet<Source>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Source> Sources { get; set; }
    }
}
