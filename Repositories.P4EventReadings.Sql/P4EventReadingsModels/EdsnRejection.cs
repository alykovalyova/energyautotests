﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class EdsnRejection
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public short CodeId { get; set; }
        public short? RejectionLevelId { get; set; }
        public int StatusHistoryId { get; set; }

        public virtual StatusHistory StatusHistory { get; set; }
    }
}
