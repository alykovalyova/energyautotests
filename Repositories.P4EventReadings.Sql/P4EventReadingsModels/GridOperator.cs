﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class GridOperator
    {
        public GridOperator()
        {
            MeteringPoints = new HashSet<MeteringPoint>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
    }
}
