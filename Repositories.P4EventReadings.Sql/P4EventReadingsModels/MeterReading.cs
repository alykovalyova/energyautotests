﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class MeterReading
    {
        public MeterReading()
        {
            RegisterReadings = new HashSet<RegisterReading>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public short ProductTypeId { get; set; }
        public string ExternalReference { get; set; }
        public DateTime ReadingDate { get; set; }
        public string EdsnMeterId { get; set; }
        public short NrOfRegisters { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<RegisterReading> RegisterReadings { get; set; }
    }
}
