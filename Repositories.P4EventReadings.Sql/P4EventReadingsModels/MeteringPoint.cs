﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class MeteringPoint
    {
        public MeteringPoint()
        {
            CommunicationHistories = new HashSet<CommunicationHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public short ProductTypeId { get; set; }
        public int GridOperatorId { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual GridOperator GridOperator { get; set; }
        public virtual ICollection<CommunicationHistory> CommunicationHistories { get; set; }
    }
}
