﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class P4EventReadingsContext : DbContext
    {
        public P4EventReadingsContext()
        {
        }

        public P4EventReadingsContext(DbContextOptions<P4EventReadingsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BalanceSupplier> BalanceSuppliers { get; set; }
        public virtual DbSet<CommunicationHistory> CommunicationHistories { get; set; }
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevels { get; set; }
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCodes { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; }
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCodes { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCodes { get; set; }
        public virtual DbSet<DictionaryMeterReadingStatus> DictionaryMeterReadingStatuses { get; set; }
        public virtual DbSet<DictionaryMeteringPointStatus> DictionaryMeteringPointStatuses { get; set; }
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatuses { get; set; }
        public virtual DbSet<DictionaryRejectionCode> DictionaryRejectionCodes { get; set; }
        public virtual DbSet<DictionarySource> DictionarySources { get; set; }
        public virtual DbSet<EdsnRejection> EdsnRejections { get; set; }
        public virtual DbSet<GridOperator> GridOperators { get; set; }
        public virtual DbSet<MeterReading> MeterReadings { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoints { get; set; }
        public virtual DbSet<RegisterReading> RegisterReadings { get; set; }
        public virtual DbSet<Source> Sources { get; set; }
        public virtual DbSet<StatusHistory> StatusHistories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=P4EventReadings;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<BalanceSupplier>(entity =>
            {
                entity.ToTable("BalanceSupplier");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();
            });

            modelBuilder.Entity<CommunicationHistory>(entity =>
            {
                entity.ToTable("CommunicationHistory");

                entity.HasIndex(e => e.BalanceSupplierId, "IX_CommunicationHistory_BalanceSupplierId");

                entity.HasIndex(e => e.ExternalReference, "IX_ExternalReference");

                entity.HasIndex(e => new { e.MeteringPointId, e.ReadingDate }, "IX_MeteringPointId_ReadingDate");

                entity.Property(e => e.ReadingDate).HasColumnType("date");

                entity.Property(e => e.RequestDate).HasColumnType("date");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.CommunicationHistories)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.CommunicationHistories)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeterReadingStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeterReadingStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeteringPointStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeteringPointStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryRejectionCode>(entity =>
            {
                entity.ToTable("Dictionary_RejectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionarySource>(entity =>
            {
                entity.ToTable("Dictionary_Source");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.ToTable("EdsnRejection");

                entity.HasIndex(e => e.StatusHistoryId, "IX_EdsnRejection_StatusHistoryId");

                entity.HasOne(d => d.StatusHistory)
                    .WithMany(p => p.EdsnRejections)
                    .HasForeignKey(d => d.StatusHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<GridOperator>(entity =>
            {
                entity.ToTable("GridOperator");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();
            });

            modelBuilder.Entity<MeterReading>(entity =>
            {
                entity.ToTable("MeterReading");

                entity.HasIndex(e => new { e.EanId, e.EdsnMeterId }, "IX_EanId_EdsnMeterId");

                entity.HasIndex(e => new { e.EanId, e.ReadingDate }, "IX_EanId_ReadingDate");

                entity.Property(e => e.ReadingDate).HasColumnType("date");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint");

                entity.HasIndex(e => e.EanId, "IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => e.GridOperatorId, "IX_MeteringPoint_GridOperatorId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPoints)
                    .HasForeignKey(d => d.GridOperatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RegisterReading>(entity =>
            {
                entity.ToTable("RegisterReading");

                entity.HasIndex(e => e.MeterReadingId, "IX_RegisterReading_MeterReadingId");

                entity.HasIndex(e => new { e.TariffTypeId, e.MeteringDirectionId, e.MeterReadingId }, "IX_TariffType_MeteringDirection")
                    .IsUnique();

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.RegisterReadings)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Source>(entity =>
            {
                entity.ToTable("Source");

                entity.HasIndex(e => e.CommunicationHistoryId, "IX_Source_CommunicationHistoryId");

                entity.HasIndex(e => e.SourceId, "IX_Source_SourceId");

                entity.HasOne(d => d.CommunicationHistory)
                    .WithMany(p => p.Sources)
                    .HasForeignKey(d => d.CommunicationHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.SourceNavigation)
                    .WithMany(p => p.Sources)
                    .HasForeignKey(d => d.SourceId);
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory");

                entity.HasIndex(e => new { e.CommunicationHistoryId, e.CreatedOn }, "IX_CommunicationHistoryId_CreatedOn");

                entity.HasOne(d => d.CommunicationHistory)
                    .WithMany(p => p.StatusHistories)
                    .HasForeignKey(d => d.CommunicationHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
