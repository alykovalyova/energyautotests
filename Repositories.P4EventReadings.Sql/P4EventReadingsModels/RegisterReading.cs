﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class RegisterReading
    {
        public int Id { get; set; }
        public string EdsnId { get; set; }
        public int Value { get; set; }
        public short TariffTypeId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short MeasureUnitId { get; set; }
        public int MeterReadingId { get; set; }

        public virtual MeterReading MeterReading { get; set; }
    }
}
