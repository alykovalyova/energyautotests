﻿using System;
using System.Collections.Generic;

namespace Repositories.P4EventReadings.Sql.P4EventReadingsModels
{
    public partial class Source
    {
        public int Id { get; set; }
        public int CommunicationHistoryId { get; set; }
        public short SourceId { get; set; }

        public virtual CommunicationHistory CommunicationHistory { get; set; }
        public virtual DictionarySource SourceNavigation { get; set; }
    }
}
