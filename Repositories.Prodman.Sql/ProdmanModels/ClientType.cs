﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class ClientType
    {
        public Guid Id { get; set; }
        public byte TypeOfClientId { get; set; }
        public Guid PropositionId { get; set; }

        public virtual Proposition Proposition { get; set; }
        public virtual DictionaryTypeOfClient TypeOfClient { get; set; }
    }
}
