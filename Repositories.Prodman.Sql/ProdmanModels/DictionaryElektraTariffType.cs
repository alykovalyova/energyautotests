﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryElektraTariffType
    {
        public DictionaryElektraTariffType()
        {
            ElectricityTariffs = new HashSet<ElectricityTariff>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityTariff> ElectricityTariffs { get; set; }
    }
}
