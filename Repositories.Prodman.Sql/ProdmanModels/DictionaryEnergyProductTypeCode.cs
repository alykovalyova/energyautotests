﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryEnergyProductTypeCode
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
