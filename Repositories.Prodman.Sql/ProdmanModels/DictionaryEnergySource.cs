﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryEnergySource
    {
        public DictionaryEnergySource()
        {
            EnergySources = new HashSet<EnergySource>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<EnergySource> EnergySources { get; set; }
    }
}
