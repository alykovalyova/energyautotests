﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryEnergyUsageProfileCode
    {
        public DictionaryEnergyUsageProfileCode()
        {
            ElectricityProfileCategories = new HashSet<ElectricityProfileCategory>();
            GasProfileCategories = new HashSet<GasProfileCategory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityProfileCategory> ElectricityProfileCategories { get; set; }
        public virtual ICollection<GasProfileCategory> GasProfileCategories { get; set; }
    }
}
