﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryGasRegion
    {
        public DictionaryGasRegion()
        {
            GasTariffs = new HashSet<GasTariff>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GasTariff> GasTariffs { get; set; }
    }
}
