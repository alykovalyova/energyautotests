﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryIncentivePaymentType
    {
        public DictionaryIncentivePaymentType()
        {
            IncentivePayments = new HashSet<IncentivePayment>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<IncentivePayment> IncentivePayments { get; set; }
    }
}
