﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryIncentiveType
    {
        public DictionaryIncentiveType()
        {
            Incentives = new HashSet<Incentive>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Incentive> Incentives { get; set; }
    }
}
