﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryMargeTypeR
    {
        public DictionaryMargeTypeR()
        {
            Propositions = new HashSet<Proposition>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Proposition> Propositions { get; set; }
    }
}
