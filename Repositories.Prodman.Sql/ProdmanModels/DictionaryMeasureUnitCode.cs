﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryMeasureUnitCode
    {
        public DictionaryMeasureUnitCode()
        {
            ElectricityPrices = new HashSet<ElectricityPrice>();
            GasPrices = new HashSet<GasPrice>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityPrice> ElectricityPrices { get; set; }
        public virtual ICollection<GasPrice> GasPrices { get; set; }
    }
}
