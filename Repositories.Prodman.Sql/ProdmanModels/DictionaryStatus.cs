﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryStatus
    {
        public DictionaryStatus()
        {
            ElectricityPriceStatusHistories = new HashSet<ElectricityPriceStatusHistory>();
            ElectricityPrices = new HashSet<ElectricityPrice>();
            GasPriceStatusHistories = new HashSet<GasPriceStatusHistory>();
            GasPrices = new HashSet<GasPrice>();
            PropositionStatusHistories = new HashSet<PropositionStatusHistory>();
            Propositions = new HashSet<Proposition>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityPriceStatusHistory> ElectricityPriceStatusHistories { get; set; }
        public virtual ICollection<ElectricityPrice> ElectricityPrices { get; set; }
        public virtual ICollection<GasPriceStatusHistory> GasPriceStatusHistories { get; set; }
        public virtual ICollection<GasPrice> GasPrices { get; set; }
        public virtual ICollection<PropositionStatusHistory> PropositionStatusHistories { get; set; }
        public virtual ICollection<Proposition> Propositions { get; set; }
    }
}
