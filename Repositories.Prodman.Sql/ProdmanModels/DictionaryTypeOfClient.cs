﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class DictionaryTypeOfClient
    {
        public DictionaryTypeOfClient()
        {
            ClientTypes = new HashSet<ClientType>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ClientType> ClientTypes { get; set; }
    }
}
