﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class ElectricityPrice
    {
        public ElectricityPrice()
        {
            ElectricityPriceStatusHistories = new HashSet<ElectricityPriceStatusHistory>();
            ElectricityTariffs = new HashSet<ElectricityTariff>();
        }

        public Guid Id { get; set; }
        public Guid PropositionId { get; set; }
        public byte StatusId { get; set; }
        public DateTime MarketFrom { get; set; }
        public string Code { get; set; }
        public decimal StandingCharge { get; set; }
        public decimal CashBack { get; set; }
        public decimal NpvPerCustomer { get; set; }
        public byte MeasureUnitCodeId { get; set; }

        public virtual DictionaryMeasureUnitCode MeasureUnitCode { get; set; }
        public virtual Proposition Proposition { get; set; }
        public virtual DictionaryStatus Status { get; set; }
        public virtual ICollection<ElectricityPriceStatusHistory> ElectricityPriceStatusHistories { get; set; }
        public virtual ICollection<ElectricityTariff> ElectricityTariffs { get; set; }
    }
}
