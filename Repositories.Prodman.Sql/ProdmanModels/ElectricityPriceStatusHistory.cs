﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class ElectricityPriceStatusHistory
    {
        public Guid Id { get; set; }
        public Guid ElectricityPriceId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusModifiedOn { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }

        public virtual ElectricityPrice ElectricityPrice { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
