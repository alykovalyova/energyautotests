﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class ElectricityProfileCategory
    {
        public Guid Id { get; set; }
        public byte EnergyUsageProfileCodeId { get; set; }
        public Guid ElectricityTariffId { get; set; }

        public virtual ElectricityTariff ElectricityTariff { get; set; }
        public virtual DictionaryEnergyUsageProfileCode EnergyUsageProfileCode { get; set; }
    }
}
