﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class ElectricityTariff
    {
        public ElectricityTariff()
        {
            ElectricityProfileCategories = new HashSet<ElectricityProfileCategory>();
        }

        public Guid Id { get; set; }
        public byte ElektraTariffTypeId { get; set; }
        public decimal ProductionPrice { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal LoyalCustomerDiscount { get; set; }
        public byte? PriceDiscountTypeId { get; set; }
        public decimal? PriceDiscountValue { get; set; }
        public Guid ElectricityPriceId { get; set; }

        public virtual ElectricityPrice ElectricityPrice { get; set; }
        public virtual DictionaryElektraTariffType ElektraTariffType { get; set; }
        public virtual DictionaryPriceDiscountType PriceDiscountType { get; set; }
        public virtual ICollection<ElectricityProfileCategory> ElectricityProfileCategories { get; set; }
    }
}
