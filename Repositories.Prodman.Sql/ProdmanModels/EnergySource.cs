﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class EnergySource
    {
        public Guid Id { get; set; }
        public Guid PropositionId { get; set; }
        public byte EnergySourceId { get; set; }

        public virtual DictionaryEnergySource EnergySourceNavigation { get; set; }
        public virtual Proposition Proposition { get; set; }
    }
}
