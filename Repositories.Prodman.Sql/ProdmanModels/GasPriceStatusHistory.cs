﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class GasPriceStatusHistory
    {
        public Guid Id { get; set; }
        public Guid GasPriceId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusModifiedOn { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }

        public virtual GasPrice GasPrice { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
