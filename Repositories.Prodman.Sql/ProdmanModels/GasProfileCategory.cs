﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class GasProfileCategory
    {
        public Guid Id { get; set; }
        public byte EnergyUsageProfileCodeId { get; set; }
        public Guid GasTariffId { get; set; }

        public virtual DictionaryEnergyUsageProfileCode EnergyUsageProfileCode { get; set; }
        public virtual GasTariff GasTariff { get; set; }
    }
}
