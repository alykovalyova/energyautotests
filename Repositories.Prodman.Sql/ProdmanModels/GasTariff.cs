﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class GasTariff
    {
        public GasTariff()
        {
            GasProfileCategories = new HashSet<GasProfileCategory>();
        }

        public Guid Id { get; set; }
        public byte GasRegionId { get; set; }
        public decimal RegionalSurcharge { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal LoyalCustomerDiscount { get; set; }
        public byte? PriceDiscountTypeId { get; set; }
        public decimal? PriceDiscountValue { get; set; }
        public Guid GasPriceId { get; set; }

        public virtual GasPrice GasPrice { get; set; }
        public virtual DictionaryGasRegion GasRegion { get; set; }
        public virtual DictionaryPriceDiscountType PriceDiscountType { get; set; }
        public virtual ICollection<GasProfileCategory> GasProfileCategories { get; set; }
    }
}
