﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class Incentive
    {
        public Incentive()
        {
            IncentivePayments = new HashSet<IncentivePayment>();
        }

        public Guid Id { get; set; }
        public byte IncentiveTypeId { get; set; }
        public decimal MaxCashback { get; set; }
        public decimal VoucherAmount { get; set; }
        public string GiftName { get; set; }
        public Guid PropositionId { get; set; }

        public virtual DictionaryIncentiveType IncentiveType { get; set; }
        public virtual Proposition Proposition { get; set; }
        public virtual ICollection<IncentivePayment> IncentivePayments { get; set; }
    }
}
