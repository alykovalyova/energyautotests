﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class IncentivePayment
    {
        public Guid Id { get; set; }
        public byte IncentivePaymentTypeId { get; set; }
        public Guid IncentiveId { get; set; }

        public virtual Incentive Incentive { get; set; }
        public virtual DictionaryIncentivePaymentType IncentivePaymentType { get; set; }
    }
}
