﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class ProdManContext : DbContext
    {
        public ProdManContext()
        {
        }

        public ProdManContext(DbContextOptions<ProdManContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClientType> ClientTypes { get; set; }
        public virtual DbSet<DictionaryElektraTariffType> DictionaryElektraTariffTypes { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; }
        public virtual DbSet<DictionaryEnergySource> DictionaryEnergySources { get; set; }
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCodes { get; set; }
        public virtual DbSet<DictionaryGasRegion> DictionaryGasRegions { get; set; }
        public virtual DbSet<DictionaryIncentivePaymentType> DictionaryIncentivePaymentTypes { get; set; }
        public virtual DbSet<DictionaryIncentiveType> DictionaryIncentiveTypes { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryMargeTypeN> DictionaryMargeTypeNs { get; set; }
        public virtual DbSet<DictionaryMargeTypeNnle> DictionaryMargeTypeNnles { get; set; }
        public virtual DbSet<DictionaryMargeTypeR> DictionaryMargeTypeRs { get; set; }
        public virtual DbSet<DictionaryMargeTypeRnle> DictionaryMargeTypeRnles { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCodes { get; set; }
        public virtual DbSet<DictionaryPriceDiscountType> DictionaryPriceDiscountTypes { get; set; }
        public virtual DbSet<DictionaryStandingChargePeriodType> DictionaryStandingChargePeriodTypes { get; set; }
        public virtual DbSet<DictionaryStatus> DictionaryStatuses { get; set; }
        public virtual DbSet<DictionaryTypeOfClient> DictionaryTypeOfClients { get; set; }
        public virtual DbSet<ElectricityPrice> ElectricityPrices { get; set; }
        public virtual DbSet<ElectricityPriceStatusHistory> ElectricityPriceStatusHistories { get; set; }
        public virtual DbSet<ElectricityProfileCategory> ElectricityProfileCategories { get; set; }
        public virtual DbSet<ElectricityTariff> ElectricityTariffs { get; set; }
        public virtual DbSet<EnergySource> EnergySources { get; set; }
        public virtual DbSet<GasPrice> GasPrices { get; set; }
        public virtual DbSet<GasPriceStatusHistory> GasPriceStatusHistories { get; set; }
        public virtual DbSet<GasProfileCategory> GasProfileCategories { get; set; }
        public virtual DbSet<GasTariff> GasTariffs { get; set; }
        public virtual DbSet<Incentive> Incentives { get; set; }
        public virtual DbSet<IncentivePayment> IncentivePayments { get; set; }
        public virtual DbSet<Label> Labels { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistories { get; set; }
        public virtual DbSet<Proposition> Propositions { get; set; }
        public virtual DbSet<PropositionStatusHistory> PropositionStatusHistories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=ProdMan;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientType>(entity =>
            {
                entity.ToTable("ClientType", "dbo");

                entity.HasIndex(e => e.PropositionId, "IX_PropositionId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.ClientTypes)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.ClientType_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.TypeOfClient)
                    .WithMany(p => p.ClientTypes)
                    .HasForeignKey(d => d.TypeOfClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ClientType_dbo.Dictionary_TypeOfClient_TypeOfClientId");
            });

            modelBuilder.Entity<DictionaryElektraTariffType>(entity =>
            {
                entity.ToTable("Dictionary_ElektraTariffType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergySource>(entity =>
            {
                entity.ToTable("Dictionary_EnergySource", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryGasRegion>(entity =>
            {
                entity.ToTable("Dictionary_GasRegion", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryIncentivePaymentType>(entity =>
            {
                entity.ToTable("Dictionary_IncentivePaymentType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryIncentiveType>(entity =>
            {
                entity.ToTable("Dictionary_IncentiveType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Labels", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeN>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeN", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeNnle>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeNNLE", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeR>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeR", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeRnle>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeRNLE", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryPriceDiscountType>(entity =>
            {
                entity.ToTable("Dictionary_PriceDiscountType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStandingChargePeriodType>(entity =>
            {
                entity.ToTable("Dictionary_StandingChargePeriodType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStatus>(entity =>
            {
                entity.ToTable("Dictionary_Status", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryTypeOfClient>(entity =>
            {
                entity.ToTable("Dictionary_TypeOfClient", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<ElectricityPrice>(entity =>
            {
                entity.ToTable("ElectricityPrice", "dbo");

                entity.HasIndex(e => e.PropositionId, "IX_PropositionId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CashBack).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MarketFrom).HasColumnType("date");

                entity.Property(e => e.NpvPerCustomer).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StandingCharge).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeasureUnitCode)
                    .WithMany(p => p.ElectricityPrices)
                    .HasForeignKey(d => d.MeasureUnitCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityPrice_dbo.Dictionary_MeasureUnitCode_MeasureUnitCodeId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.ElectricityPrices)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.ElectricityPrice_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ElectricityPrices)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityPrice_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<ElectricityPriceStatusHistory>(entity =>
            {
                entity.ToTable("ElectricityPriceStatusHistory", "dbo");

                entity.HasIndex(e => e.ElectricityPriceId, "IX_ElectricityPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ElectricityPrice)
                    .WithMany(p => p.ElectricityPriceStatusHistories)
                    .HasForeignKey(d => d.ElectricityPriceId)
                    .HasConstraintName("FK_dbo.ElectricityPriceStatusHistory_dbo.ElectricityPrice_ElectricityPriceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ElectricityPriceStatusHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityPriceStatusHistory_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<ElectricityProfileCategory>(entity =>
            {
                entity.ToTable("ElectricityProfileCategory", "dbo");

                entity.HasIndex(e => e.ElectricityTariffId, "IX_ElectricityTariffId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.ElectricityTariff)
                    .WithMany(p => p.ElectricityProfileCategories)
                    .HasForeignKey(d => d.ElectricityTariffId)
                    .HasConstraintName("FK_dbo.ElectricityProfileCategory_dbo.ElectricityTariff_ElectricityTariffId");

                entity.HasOne(d => d.EnergyUsageProfileCode)
                    .WithMany(p => p.ElectricityProfileCategories)
                    .HasForeignKey(d => d.EnergyUsageProfileCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityProfileCategory_dbo.Dictionary_EnergyUsageProfileCode_EnergyUsageProfileCodeId");
            });

            modelBuilder.Entity<ElectricityTariff>(entity =>
            {
                entity.ToTable("ElectricityTariff", "dbo");

                entity.HasIndex(e => e.ElectricityPriceId, "IX_ElectricityPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.LoyalCustomerDiscount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.PriceDiscountValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductionPrice).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RetailPrice).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.ElectricityPrice)
                    .WithMany(p => p.ElectricityTariffs)
                    .HasForeignKey(d => d.ElectricityPriceId)
                    .HasConstraintName("FK_dbo.ElectricityTariff_dbo.ElectricityPrice_ElectricityPriceId");

                entity.HasOne(d => d.ElektraTariffType)
                    .WithMany(p => p.ElectricityTariffs)
                    .HasForeignKey(d => d.ElektraTariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityTariff_dbo.Dictionary_ElektraTariffType_ElektraTariffTypeId");

                entity.HasOne(d => d.PriceDiscountType)
                    .WithMany(p => p.ElectricityTariffs)
                    .HasForeignKey(d => d.PriceDiscountTypeId)
                    .HasConstraintName("FK_dbo.ElectricityTariff_dbo.Dictionary_PriceDiscountType_PriceDiscountTypeId");
            });

            modelBuilder.Entity<EnergySource>(entity =>
            {
                entity.ToTable("EnergySource", "dbo");

                entity.HasIndex(e => e.PropositionId, "IX_PropositionId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.EnergySourceNavigation)
                    .WithMany(p => p.EnergySources)
                    .HasForeignKey(d => d.EnergySourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EnergySource_dbo.Dictionary_EnergySource_EnergySourceId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.EnergySources)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.EnergySource_dbo.Proposition_PropositionId");
            });

            modelBuilder.Entity<GasPrice>(entity =>
            {
                entity.ToTable("GasPrice", "dbo");

                entity.HasIndex(e => e.PropositionId, "IX_PropositionId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CashBack).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MarketFrom).HasColumnType("date");

                entity.Property(e => e.NpvPerCustomer).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StandingCharge).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeasureUnitCode)
                    .WithMany(p => p.GasPrices)
                    .HasForeignKey(d => d.MeasureUnitCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasPrice_dbo.Dictionary_MeasureUnitCode_MeasureUnitCodeId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.GasPrices)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.GasPrice_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GasPrices)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasPrice_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<GasPriceStatusHistory>(entity =>
            {
                entity.ToTable("GasPriceStatusHistory", "dbo");

                entity.HasIndex(e => e.GasPriceId, "IX_GasPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.GasPrice)
                    .WithMany(p => p.GasPriceStatusHistories)
                    .HasForeignKey(d => d.GasPriceId)
                    .HasConstraintName("FK_dbo.GasPriceStatusHistory_dbo.GasPrice_GasPriceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GasPriceStatusHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasPriceStatusHistory_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<GasProfileCategory>(entity =>
            {
                entity.ToTable("GasProfileCategory", "dbo");

                entity.HasIndex(e => e.GasTariffId, "IX_GasTariffId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.EnergyUsageProfileCode)
                    .WithMany(p => p.GasProfileCategories)
                    .HasForeignKey(d => d.EnergyUsageProfileCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasProfileCategory_dbo.Dictionary_EnergyUsageProfileCode_EnergyUsageProfileCodeId");

                entity.HasOne(d => d.GasTariff)
                    .WithMany(p => p.GasProfileCategories)
                    .HasForeignKey(d => d.GasTariffId)
                    .HasConstraintName("FK_dbo.GasProfileCategory_dbo.GasTariff_GasTariffId");
            });

            modelBuilder.Entity<GasTariff>(entity =>
            {
                entity.ToTable("GasTariff", "dbo");

                entity.HasIndex(e => e.GasPriceId, "IX_GasPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.LoyalCustomerDiscount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.PriceDiscountValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RegionalSurcharge).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RetailPrice).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.GasPrice)
                    .WithMany(p => p.GasTariffs)
                    .HasForeignKey(d => d.GasPriceId)
                    .HasConstraintName("FK_dbo.GasTariff_dbo.GasPrice_GasPriceId");

                entity.HasOne(d => d.GasRegion)
                    .WithMany(p => p.GasTariffs)
                    .HasForeignKey(d => d.GasRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasTariff_dbo.Dictionary_GasRegion_GasRegionId");

                entity.HasOne(d => d.PriceDiscountType)
                    .WithMany(p => p.GasTariffs)
                    .HasForeignKey(d => d.PriceDiscountTypeId)
                    .HasConstraintName("FK_dbo.GasTariff_dbo.Dictionary_PriceDiscountType_PriceDiscountTypeId");
            });

            modelBuilder.Entity<Incentive>(entity =>
            {
                entity.ToTable("Incentive", "dbo");

                entity.HasIndex(e => e.PropositionId, "IX_PropositionId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.MaxCashback).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VoucherAmount).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.IncentiveType)
                    .WithMany(p => p.Incentives)
                    .HasForeignKey(d => d.IncentiveTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Incentive_dbo.Dictionary_IncentiveType_IncentiveTypeId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.Incentives)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.Incentive_dbo.Proposition_PropositionId");
            });

            modelBuilder.Entity<IncentivePayment>(entity =>
            {
                entity.ToTable("IncentivePayment", "dbo");

                entity.HasIndex(e => e.IncentiveId, "IX_IncentiveId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.Incentive)
                    .WithMany(p => p.IncentivePayments)
                    .HasForeignKey(d => d.IncentiveId)
                    .HasConstraintName("FK_dbo.IncentivePayment_dbo.Incentive_IncentiveId");

                entity.HasOne(d => d.IncentivePaymentType)
                    .WithMany(p => p.IncentivePayments)
                    .HasForeignKey(d => d.IncentivePaymentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.IncentivePayment_dbo.Dictionary_IncentivePaymentType_IncentivePaymentTypeId");
            });

            modelBuilder.Entity<Label>(entity =>
            {
                entity.HasKey(e => e.LabelCode)
                    .HasName("PK_dbo.Label");

                entity.ToTable("Label", "dbo");

                entity.HasIndex(e => e.IsActive, "IX_IsActive");

                entity.Property(e => e.BalanceResponsiblePartyElektricityEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceResponsiblePartyGasEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceResponsiblePartyLargeElektricityEan).HasMaxLength(13);

                entity.Property(e => e.BalanceResponsiblePartyLargeGasEan).HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierElektricityEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierGasEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierLargeElektricityEan).HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierLargeGasEan).HasMaxLength(13);

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CompanyName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(50);

                entity.Property(e => e.Iban).HasMaxLength(35);

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasMaxLength(50)
                    .HasColumnName("ZIPCode");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithOne(p => p.Label)
                    .HasForeignKey<Label>(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Label_dbo.Dictionary_Labels_LabelCode");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Proposition>(entity =>
            {
                entity.ToTable("Proposition", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Bna)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MargeTypeNnle).HasColumnName("MargeTypeNNLE");

                entity.Property(e => e.MargeTypeRnle).HasColumnName("MargeTypeRNLE");

                entity.Property(e => e.MarketFrom).HasColumnType("date");

                entity.Property(e => e.MarketTo).HasColumnType("date");

                entity.Property(e => e.ReimburseFine).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StandingChargePeriodId).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_Labels_LabelCode");

                entity.HasOne(d => d.MargeTypeNNavigation)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.MargeTypeN)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeN_MargeTypeN");

                entity.HasOne(d => d.MargeTypeNnleNavigation)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.MargeTypeNnle)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeNNLE_MargeTypeNNLE");

                entity.HasOne(d => d.MargeTypeRNavigation)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.MargeTypeR)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeR_MargeTypeR");

                entity.HasOne(d => d.MargeTypeRnleNavigation)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.MargeTypeRnle)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeRNLE_MargeTypeRNLE");

                entity.HasOne(d => d.StandingChargePeriod)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.StandingChargePeriodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_StandingChargePeriodType_StandingChargePeriodId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Propositions)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<PropositionStatusHistory>(entity =>
            {
                entity.ToTable("PropositionStatusHistory", "dbo");

                entity.HasIndex(e => e.PropositionId, "IX_PropositionId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.PropositionStatusHistories)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.PropositionStatusHistory_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PropositionStatusHistories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PropositionStatusHistory_dbo.Dictionary_Status_StatusId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
