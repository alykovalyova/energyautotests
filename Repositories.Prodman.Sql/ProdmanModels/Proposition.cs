﻿using System;
using System.Collections.Generic;

namespace Repositories.Prodman.Sql.ProdManModels
{
    public partial class Proposition
    {
        public Proposition()
        {
            ClientTypes = new HashSet<ClientType>();
            ElectricityPrices = new HashSet<ElectricityPrice>();
            EnergySources = new HashSet<EnergySource>();
            GasPrices = new HashSet<GasPrice>();
            Incentives = new HashSet<Incentive>();
            PropositionStatusHistories = new HashSet<PropositionStatusHistory>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime MarketFrom { get; set; }
        public DateTime? MarketTo { get; set; }
        public bool IsEligibleAsRenewal { get; set; }
        public bool IsSalesEligible { get; set; }
        public int Duration { get; set; }
        public bool IsFineApplicable { get; set; }
        public int CoolDownPeriod { get; set; }
        public decimal ReimburseFine { get; set; }
        public int? ContractStartWindow { get; set; }
        public byte StatusId { get; set; }
        public bool IsFixedPriceCombination { get; set; }
        public string Description { get; set; }
        public byte LabelCode { get; set; }
        public byte StandingChargePeriodId { get; set; }
        public byte? MargeTypeN { get; set; }
        public byte? MargeTypeR { get; set; }
        public byte? MargeTypeNnle { get; set; }
        public byte? MargeTypeRnle { get; set; }
        public bool ReclaimPenaltyOneYear { get; set; }
        public string GeneralInformation { get; set; }
        public bool? Bna { get; set; }
        public int? PresentValue { get; set; }
        public int? CostPerOrder { get; set; }

        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual DictionaryMargeTypeN MargeTypeNNavigation { get; set; }
        public virtual DictionaryMargeTypeNnle MargeTypeNnleNavigation { get; set; }
        public virtual DictionaryMargeTypeR MargeTypeRNavigation { get; set; }
        public virtual DictionaryMargeTypeRnle MargeTypeRnleNavigation { get; set; }
        public virtual DictionaryStandingChargePeriodType StandingChargePeriod { get; set; }
        public virtual DictionaryStatus Status { get; set; }
        public virtual ICollection<ClientType> ClientTypes { get; set; }
        public virtual ICollection<ElectricityPrice> ElectricityPrices { get; set; }
        public virtual ICollection<EnergySource> EnergySources { get; set; }
        public virtual ICollection<GasPrice> GasPrices { get; set; }
        public virtual ICollection<Incentive> Incentives { get; set; }
        public virtual ICollection<PropositionStatusHistory> PropositionStatusHistories { get; set; }
    }
}
