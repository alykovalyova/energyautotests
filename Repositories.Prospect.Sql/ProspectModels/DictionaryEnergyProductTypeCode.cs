﻿using System;
using System.Collections.Generic;

namespace Repositories.Prospect.Sql.ProspectModels
{
    public partial class DictionaryEnergyProductTypeCode
    {
        public DictionaryEnergyProductTypeCode()
        {
            MeteringPoints = new HashSet<MeteringPoint>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
    }
}
