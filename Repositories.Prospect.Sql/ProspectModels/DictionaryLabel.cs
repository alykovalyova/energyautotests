﻿using System;
using System.Collections.Generic;

namespace Repositories.Prospect.Sql.ProspectModels
{
    public partial class DictionaryLabel
    {
        public DictionaryLabel()
        {
            Prospects = new HashSet<Prospect>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Prospect> Prospects { get; set; }
    }
}
