﻿using System;
using System.Collections.Generic;

namespace Repositories.Prospect.Sql.ProspectModels
{
    public partial class DictionaryOfferStatusCode
    {
        public DictionaryOfferStatusCode()
        {
            OfferStatusHistories = new HashSet<OfferStatusHistory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<OfferStatusHistory> OfferStatusHistories { get; set; }
    }
}
