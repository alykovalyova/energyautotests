﻿using System;
using System.Collections.Generic;

namespace Repositories.Prospect.Sql.ProspectModels
{
    public partial class MeteringPoint
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int? UsageEacPeak { get; set; }
        public int? UsageEacOffPeak { get; set; }
        public string Ean { get; set; }
        public bool? Residential { get; set; }
        public string CaptarCode { get; set; }
        public string GridArea { get; set; }
        public int ProductTypeId { get; set; }
        public int? UsageProfileId { get; set; }
        public int? UsageEapPeak { get; set; }
        public int? UsageEapOffPeak { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual DictionaryEnergyProductTypeCode ProductType { get; set; }
        public virtual DictionaryEnergyUsageProfileCode UsageProfile { get; set; }
    }
}
