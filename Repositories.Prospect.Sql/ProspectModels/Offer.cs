﻿using System;
using System.Collections.Generic;

namespace Repositories.Prospect.Sql.ProspectModels
{
    public partial class Offer
    {
        public Offer()
        {
            MeteringPoints = new HashSet<MeteringPoint>();
            OfferStatusHistories = new HashSet<OfferStatusHistory>();
        }

        public int Id { get; set; }
        public int ProspectId { get; set; }
        public Guid PropositionId { get; set; }
        public string PropositionName { get; set; }
        public string SalesAgentName { get; set; }
        public decimal? Cashback { get; set; }
        public byte LastStatusId { get; set; }
        public string LastComment { get; set; }
        public Guid ExternalReference { get; set; }
        public string CalculationDetailsSnapshot { get; set; }
        public string Zipcode { get; set; }
        public int BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public DateTime? ElkContractEndDate { get; set; }
        public DateTime? GasContractEndDate { get; set; }
        public DateTime? GetEndDateRequestDate { get; set; }
        public bool IsCalculationsBasedOnEdsn { get; set; }

        public virtual Prospect Prospect { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
        public virtual ICollection<OfferStatusHistory> OfferStatusHistories { get; set; }
    }
}
