﻿using System;
using System.Collections.Generic;

namespace Repositories.Prospect.Sql.ProspectModels
{
    public partial class OfferStatusHistory
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Comment { get; set; }
        public string User { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual DictionaryOfferStatusCode Status { get; set; }
    }
}
