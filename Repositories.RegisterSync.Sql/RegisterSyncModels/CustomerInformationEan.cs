﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class CustomerInformationEan
    {
        public string Ean { get; set; }
        public short LabelCodeId { get; set; }
        public int ContractId { get; set; }

        public virtual EnumLabel LabelCode { get; set; }
    }
}
