﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EanMismatch
    {
        public int Id { get; set; }
        public string Ean { get; set; }
        public short StatusId { get; set; }
        public string AssignedTo { get; set; }
        public int ReportId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public short? EdsnLabelCodeId { get; set; }
        public short? EnerfreeLabelCodeId { get; set; }
        public short? IcarLabelCodeId { get; set; }
        public int? ContractId { get; set; }
        public short? MeteringMethodCodeId { get; set; }
        public DateTime? MutationDate { get; set; }
        public short? PhysicalStatusMeterCodeId { get; set; }

        public virtual EnumLabel EdsnLabelCode { get; set; }
        public virtual EnumLabel EnerfreeLabelCode { get; set; }
        public virtual EnumLabel IcarLabelCode { get; set; }
        public virtual EnumMeteringMethod MeteringMethodCode { get; set; }
        public virtual EnumPhysicalStatusMeter PhysicalStatusMeterCode { get; set; }
        public virtual Report Report { get; set; }
        public virtual EnumMismatchStatus Status { get; set; }
    }
}
