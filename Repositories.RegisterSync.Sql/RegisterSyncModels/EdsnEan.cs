﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EdsnEan
    {
        public string Ean { get; set; }
        public string BalanceSupplier { get; set; }
        public short LabelCodeId { get; set; }
        public short MeteringMethodCodeId { get; set; }
        public short PhysicalStatusMeterCodeId { get; set; }

        public virtual EnumLabel LabelCode { get; set; }
        public virtual EnumMeteringMethod MeteringMethodCode { get; set; }
        public virtual EnumPhysicalStatusMeter PhysicalStatusMeterCode { get; set; }
    }
}
