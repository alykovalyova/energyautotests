﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EnumEnergyFlowDirection
    {
        public EnumEnergyFlowDirection()
        {
            IcarEans = new HashSet<IcarEan>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<IcarEan> IcarEans { get; set; }
    }
}
