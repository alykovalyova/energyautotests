﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EnumLabel
    {
        public EnumLabel()
        {
            CustomerInformationEans = new HashSet<CustomerInformationEan>();
            EanMismatchEdsnLabelCodes = new HashSet<EanMismatch>();
            EanMismatchEnerfreeLabelCodes = new HashSet<EanMismatch>();
            EanMismatchIcarLabelCodes = new HashSet<EanMismatch>();
            EdsnEans = new HashSet<EdsnEan>();
            IcarEans = new HashSet<IcarEan>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<CustomerInformationEan> CustomerInformationEans { get; set; }
        public virtual ICollection<EanMismatch> EanMismatchEdsnLabelCodes { get; set; }
        public virtual ICollection<EanMismatch> EanMismatchEnerfreeLabelCodes { get; set; }
        public virtual ICollection<EanMismatch> EanMismatchIcarLabelCodes { get; set; }
        public virtual ICollection<EdsnEan> EdsnEans { get; set; }
        public virtual ICollection<IcarEan> IcarEans { get; set; }
    }
}
