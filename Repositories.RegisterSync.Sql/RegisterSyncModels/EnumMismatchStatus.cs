﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EnumMismatchStatus
    {
        public EnumMismatchStatus()
        {
            EanMismatches = new HashSet<EanMismatch>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<EanMismatch> EanMismatches { get; set; }
    }
}
