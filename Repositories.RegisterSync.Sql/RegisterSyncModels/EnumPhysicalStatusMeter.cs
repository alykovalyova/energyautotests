﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EnumPhysicalStatusMeter
    {
        public EnumPhysicalStatusMeter()
        {
            EanMismatches = new HashSet<EanMismatch>();
            EdsnEans = new HashSet<EdsnEan>();
            IcarEans = new HashSet<IcarEan>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<EanMismatch> EanMismatches { get; set; }
        public virtual ICollection<EdsnEan> EdsnEans { get; set; }
        public virtual ICollection<IcarEan> IcarEans { get; set; }
    }
}
