﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class EnumSourceType
    {
        public EnumSourceType()
        {
            Extractions = new HashSet<Extraction>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Extraction> Extractions { get; set; }
    }
}
