﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class Extraction
    {
        public int Id { get; set; }
        public DateTime ExtractionDate { get; set; }
        public int TotalMessages { get; set; }
        public int CountMessages { get; set; }
        public short SourceTypeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual EnumSourceType SourceType { get; set; }
    }
}
