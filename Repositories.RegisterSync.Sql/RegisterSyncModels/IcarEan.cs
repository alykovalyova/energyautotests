﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class IcarEan
    {
        public string Ean { get; set; }
        public string BalanceSupplier { get; set; }
        public short LabelCodeId { get; set; }
        public DateTime MutationDate { get; set; }
        public short MeteringMethodCodeId { get; set; }
        public short PhysicalStatusMeterCodeId { get; set; }
        public short? EnergyFlowDirectionId { get; set; }

        public virtual EnumEnergyFlowDirection EnergyFlowDirection { get; set; }
        public virtual EnumLabel LabelCode { get; set; }
        public virtual EnumMeteringMethod MeteringMethodCode { get; set; }
        public virtual EnumPhysicalStatusMeter PhysicalStatusMeterCode { get; set; }
    }
}
