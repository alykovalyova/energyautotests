﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class RegisterSyncContext : DbContext
    {
        public RegisterSyncContext()
        {
        }

        public RegisterSyncContext(DbContextOptions<RegisterSyncContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CustomerInformationEan> CustomerInformationEans { get; set; }
        public virtual DbSet<EanMismatch> EanMismatches { get; set; }
        public virtual DbSet<EdsnEan> EdsnEans { get; set; }
        public virtual DbSet<EnumEnergyFlowDirection> EnumEnergyFlowDirections { get; set; }
        public virtual DbSet<EnumLabel> EnumLabels { get; set; }
        public virtual DbSet<EnumMeteringMethod> EnumMeteringMethods { get; set; }
        public virtual DbSet<EnumMismatchStatus> EnumMismatchStatuses { get; set; }
        public virtual DbSet<EnumPhysicalStatusMeter> EnumPhysicalStatusMeters { get; set; }
        public virtual DbSet<EnumSourceType> EnumSourceTypes { get; set; }
        public virtual DbSet<Extraction> Extractions { get; set; }
        public virtual DbSet<IcarEan> IcarEans { get; set; }
        public virtual DbSet<Report> Reports { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=RegisterSync;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerInformationEan>(entity =>
            {
                entity.HasKey(e => e.Ean);

                entity.ToTable("CustomerInformationEan", "dbo");

                entity.HasIndex(e => e.LabelCodeId, "IX_CustomerInformationEan_LabelCodeId");

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.HasOne(d => d.LabelCode)
                    .WithMany(p => p.CustomerInformationEans)
                    .HasForeignKey(d => d.LabelCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EanMismatch>(entity =>
            {
                entity.ToTable("EanMismatch", "dbo");

                entity.HasIndex(e => e.Ean, "IX_Ean");

                entity.HasIndex(e => e.EdsnLabelCodeId, "IX_EanMismatch_EdsnLabelCodeId");

                entity.HasIndex(e => e.EnerfreeLabelCodeId, "IX_EanMismatch_EnerfreeLabelCodeId");

                entity.HasIndex(e => e.IcarLabelCodeId, "IX_EanMismatch_IcarLabelCodeId");

                entity.HasIndex(e => e.MeteringMethodCodeId, "IX_EanMismatch_MeteringMethodCodeId");

                entity.HasIndex(e => e.PhysicalStatusMeterCodeId, "IX_EanMismatch_PhysicalStatusMeterCodeId");

                entity.HasIndex(e => e.ReportId, "IX_EanMismatch_ReportId");

                entity.HasIndex(e => e.StatusId, "IX_EanMismatch_StatusId");

                entity.Property(e => e.AssignedTo).HasMaxLength(150);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.EdsnLabelCode)
                    .WithMany(p => p.EanMismatchEdsnLabelCodes)
                    .HasForeignKey(d => d.EdsnLabelCodeId);

                entity.HasOne(d => d.EnerfreeLabelCode)
                    .WithMany(p => p.EanMismatchEnerfreeLabelCodes)
                    .HasForeignKey(d => d.EnerfreeLabelCodeId);

                entity.HasOne(d => d.IcarLabelCode)
                    .WithMany(p => p.EanMismatchIcarLabelCodes)
                    .HasForeignKey(d => d.IcarLabelCodeId);

                entity.HasOne(d => d.MeteringMethodCode)
                    .WithMany(p => p.EanMismatches)
                    .HasForeignKey(d => d.MeteringMethodCodeId);

                entity.HasOne(d => d.PhysicalStatusMeterCode)
                    .WithMany(p => p.EanMismatches)
                    .HasForeignKey(d => d.PhysicalStatusMeterCodeId);

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.EanMismatches)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.EanMismatches)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EdsnEan>(entity =>
            {
                entity.HasKey(e => e.Ean);

                entity.ToTable("EdsnEan", "dbo");

                entity.HasIndex(e => e.LabelCodeId, "IX_EdsnEan_LabelCodeId");

                entity.HasIndex(e => e.MeteringMethodCodeId, "IX_EdsnEan_MeteringMethodCodeId");

                entity.HasIndex(e => e.PhysicalStatusMeterCodeId, "IX_EdsnEan_PhysicalStatusMeterCodeId");

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.Property(e => e.BalanceSupplier)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.HasOne(d => d.LabelCode)
                    .WithMany(p => p.EdsnEans)
                    .HasForeignKey(d => d.LabelCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethodCode)
                    .WithMany(p => p.EdsnEans)
                    .HasForeignKey(d => d.MeteringMethodCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PhysicalStatusMeterCode)
                    .WithMany(p => p.EdsnEans)
                    .HasForeignKey(d => d.PhysicalStatusMeterCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EnumEnergyFlowDirection>(entity =>
            {
                entity.ToTable("EnumEnergyFlowDirection", "dbo");
            });

            modelBuilder.Entity<EnumLabel>(entity =>
            {
                entity.ToTable("EnumLabel", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumLabel_Identifier")
                    .IsUnique();
            });

            modelBuilder.Entity<EnumMeteringMethod>(entity =>
            {
                entity.ToTable("EnumMeteringMethod", "dbo");
            });

            modelBuilder.Entity<EnumMismatchStatus>(entity =>
            {
                entity.ToTable("EnumMismatchStatus", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumMismatchStatus_Identifier")
                    .IsUnique();
            });

            modelBuilder.Entity<EnumPhysicalStatusMeter>(entity =>
            {
                entity.ToTable("EnumPhysicalStatusMeter", "dbo");
            });

            modelBuilder.Entity<EnumSourceType>(entity =>
            {
                entity.ToTable("EnumSourceType", "dbo");

                entity.HasIndex(e => e.Identifier, "IX_EnumSourceType_Identifier")
                    .IsUnique();
            });

            modelBuilder.Entity<Extraction>(entity =>
            {
                entity.ToTable("Extraction", "dbo");

                entity.HasIndex(e => e.ExtractionDate, "IX_ExtractionDate");

                entity.HasIndex(e => e.SourceTypeId, "IX_Extraction_SourceTypeId");

                entity.Property(e => e.ExtractionDate).HasColumnType("date");

                entity.HasOne(d => d.SourceType)
                    .WithMany(p => p.Extractions)
                    .HasForeignKey(d => d.SourceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<IcarEan>(entity =>
            {
                entity.HasKey(e => e.Ean);

                entity.ToTable("IcarEan", "dbo");

                entity.HasIndex(e => e.EnergyFlowDirectionId, "IX_IcarEan_EnergyFlowDirectionId");

                entity.HasIndex(e => e.LabelCodeId, "IX_IcarEan_LabelCodeId");

                entity.HasIndex(e => e.MeteringMethodCodeId, "IX_IcarEan_MeteringMethodCodeId");

                entity.HasIndex(e => e.PhysicalStatusMeterCodeId, "IX_IcarEan_PhysicalStatusMeterCodeId");

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.Property(e => e.BalanceSupplier)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.IcarEans)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.LabelCode)
                    .WithMany(p => p.IcarEans)
                    .HasForeignKey(d => d.LabelCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethodCode)
                    .WithMany(p => p.IcarEans)
                    .HasForeignKey(d => d.MeteringMethodCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PhysicalStatusMeterCode)
                    .WithMany(p => p.IcarEans)
                    .HasForeignKey(d => d.PhysicalStatusMeterCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.ToTable("Report", "dbo");

                entity.HasIndex(e => e.ExtractionDate, "IX_ExtractionDate");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.ExtractionDate).HasColumnType("date");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
