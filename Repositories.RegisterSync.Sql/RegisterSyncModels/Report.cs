﻿using System;
using System.Collections.Generic;

namespace Repositories.RegisterSync.Sql.RegisterSyncModels
{
    public partial class Report
    {
        public Report()
        {
            EanMismatches = new HashSet<EanMismatch>();
        }

        public int Id { get; set; }
        public DateTime ExtractionDate { get; set; }
        public bool CreatedManually { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Labels { get; set; }
        public int MismatchesCount { get; set; }
        public bool Completed { get; set; }

        public virtual ICollection<EanMismatch> EanMismatches { get; set; }
    }
}
