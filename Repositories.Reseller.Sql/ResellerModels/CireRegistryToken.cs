﻿using System;
using System.Collections.Generic;

namespace Repositories.Reseller.Sql.ResellerModels
{
    public partial class CireRegistryToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string TokenType { get; set; }
        public DateTime AcquiredOn { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
