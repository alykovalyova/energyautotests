﻿using System;
using System.Collections.Generic;

namespace Repositories.Reseller.Sql.ResellerModels
{
    public partial class CireStatus
    {
        public CireStatus()
        {
            Resellers = new HashSet<Reseller>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Reseller> Resellers { get; set; }
    }
}
