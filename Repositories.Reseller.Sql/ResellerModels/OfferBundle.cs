﻿using System;
using System.Collections.Generic;

namespace Repositories.Reseller.Sql.ResellerModels
{
    public partial class OfferBundle
    {
        public int Id { get; set; }
        public int SalesChannelId { get; set; }
        public Guid OfferBundleId { get; set; }
        public string Name { get; set; }
        public int? NewOfferBundleId { get; set; }

        public virtual SalesChannel SalesChannel { get; set; }
    }
}
