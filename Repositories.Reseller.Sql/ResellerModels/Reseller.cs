﻿using System;
using System.Collections.Generic;

namespace Repositories.Reseller.Sql.ResellerModels
{
    public partial class Reseller
    {
        public Reseller()
        {
            SalesChannels = new HashSet<SalesChannel>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CireId { get; set; }
        public short? CireStatusId { get; set; }

        public virtual CireStatus CireStatus { get; set; }
        public virtual ICollection<SalesChannel> SalesChannels { get; set; }
    }
}
