﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.Reseller.Sql.ResellerModels
{
    public partial class ResellerContext : DbContext
    {
        public ResellerContext()
        {
        }

        public ResellerContext(DbContextOptions<ResellerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CireRegistryToken> CireRegistryTokens { get; set; }
        public virtual DbSet<CireStatus> CireStatuses { get; set; }
        public virtual DbSet<OfferBundle> OfferBundles { get; set; }
        public virtual DbSet<Reseller> Resellers { get; set; }
        public virtual DbSet<SalesChannel> SalesChannels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=Reseller;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CireRegistryToken>(entity =>
            {
                entity.ToTable("CireRegistryToken", "dbo");

                entity.Property(e => e.Token).IsRequired();

                entity.Property(e => e.TokenType).IsRequired();
            });

            modelBuilder.Entity<CireStatus>(entity =>
            {
                entity.ToTable("CireStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<OfferBundle>(entity =>
            {
                entity.ToTable("OfferBundle", "dbo");

                entity.HasIndex(e => e.Name, "IX_OfferBundle_Name");

                entity.HasIndex(e => e.SalesChannelId, "IX_OfferBundle_SalesChannelId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.SalesChannel)
                    .WithMany(p => p.OfferBundles)
                    .HasForeignKey(d => d.SalesChannelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Reseller>(entity =>
            {
                entity.ToTable("Reseller", "dbo");

                entity.HasIndex(e => e.CireStatusId, "IX_Reseller_CireStatusId");

                entity.HasIndex(e => new { e.IsActive, e.Name, e.Id }, "IX_Reseller_IsActive_Name_Id");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CireStatus)
                    .WithMany(p => p.Resellers)
                    .HasForeignKey(d => d.CireStatusId);
            });

            modelBuilder.Entity<SalesChannel>(entity =>
            {
                entity.ToTable("SalesChannel", "dbo");

                entity.HasIndex(e => e.ResellerId, "IX_SalesChannel_ResellerId");

                entity.Property(e => e.SalesChannelId).ValueGeneratedNever();

                entity.Property(e => e.ChannelName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.ResellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
