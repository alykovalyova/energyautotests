﻿using System;
using System.Collections.Generic;

namespace Repositories.Reseller.Sql.ResellerModels
{
    public partial class SalesChannel
    {
        public SalesChannel()
        {
            OfferBundles = new HashSet<OfferBundle>();
        }

        public int SalesChannelId { get; set; }
        public string ChannelName { get; set; }
        public string Label { get; set; }
        public Guid ResellerId { get; set; }

        public virtual Reseller Reseller { get; set; }
        public virtual ICollection<OfferBundle> OfferBundles { get; set; }
    }
}
