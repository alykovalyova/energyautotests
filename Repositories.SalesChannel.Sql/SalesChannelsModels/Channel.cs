﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class Channel
    {
        public Channel()
        {
            SalesChannels = new HashSet<SalesChannel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SalesChannel> SalesChannels { get; set; }
    }
}
