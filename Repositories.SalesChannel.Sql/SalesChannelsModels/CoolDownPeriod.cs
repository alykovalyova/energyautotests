﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class CoolDownPeriod
    {
        public CoolDownPeriod()
        {
            SalesChannels = new HashSet<SalesChannel>();
        }

        public int Id { get; set; }
        public int Value { get; set; }

        public virtual ICollection<SalesChannel> SalesChannels { get; set; }
    }
}
