﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class DictionaryLabel
    {
        public DictionaryLabel()
        {
            SalesChannels = new HashSet<SalesChannel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SalesChannel> SalesChannels { get; set; }
    }
}
