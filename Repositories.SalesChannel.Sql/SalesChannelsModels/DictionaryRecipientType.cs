﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class DictionaryRecipientType
    {
        public DictionaryRecipientType()
        {
            SalesChannelEmails = new HashSet<SalesChannelEmail>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SalesChannelEmail> SalesChannelEmails { get; set; }
    }
}
