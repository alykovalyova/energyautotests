﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class Partner
    {
        public Partner()
        {
            PartnerEmails = new HashSet<PartnerEmail>();
            SalesChannels = new HashSet<SalesChannel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PartnerEmail> PartnerEmails { get; set; }
        public virtual ICollection<SalesChannel> SalesChannels { get; set; }
    }
}
