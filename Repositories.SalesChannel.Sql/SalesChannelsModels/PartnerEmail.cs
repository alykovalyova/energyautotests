﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class PartnerEmail
    {
        public int Id { get; set; }
        public int PartnerId { get; set; }
        public string Address { get; set; }

        public virtual Partner Partner { get; set; }
    }
}
