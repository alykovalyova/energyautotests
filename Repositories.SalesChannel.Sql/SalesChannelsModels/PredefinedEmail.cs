﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class PredefinedEmail
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
