﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class SalesChannel
    {
        public SalesChannel()
        {
            SalesChannelEmails = new HashSet<SalesChannelEmail>();
        }

        public int Id { get; set; }
        public int LabelId { get; set; }
        public int ChannelId { get; set; }
        public int PartnerId { get; set; }
        public int? CampaignId { get; set; }
        public int? RegionId { get; set; }
        public int? StoreId { get; set; }
        public int StatusId { get; set; }
        public bool? IsConsumerEligible { get; set; }
        public bool? IsBusinessEligible { get; set; }
        public bool IsContractProvider { get; set; }
        public string OldReferenceId { get; set; }
        public int? CoolDownPeriodId { get; set; }
        public int? SwitchWindowId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string UserName { get; set; }
        public bool SkipPaidCreditCheck { get; set; }
        public bool IsAllowedForCsvImport { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual Channel Channel { get; set; }
        public virtual CoolDownPeriod CoolDownPeriod { get; set; }
        public virtual DictionaryLabel Label { get; set; }
        public virtual Partner Partner { get; set; }
        public virtual Region Region { get; set; }
        public virtual DictionarySalesChannelStatus Status { get; set; }
        public virtual Store Store { get; set; }
        public virtual SwitchWindow SwitchWindow { get; set; }
        public virtual ICollection<SalesChannelEmail> SalesChannelEmails { get; set; }
    }
}
