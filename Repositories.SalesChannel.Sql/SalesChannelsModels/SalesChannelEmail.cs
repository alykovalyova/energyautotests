﻿using System;
using System.Collections.Generic;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class SalesChannelEmail
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public int SalesChannelId { get; set; }
        public int RecipientTypeId { get; set; }

        public virtual DictionaryRecipientType RecipientType { get; set; }
        public virtual SalesChannel SalesChannel { get; set; }
    }
}
