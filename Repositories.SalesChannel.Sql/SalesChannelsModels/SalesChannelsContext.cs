﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Repositories.SalesChannel.Sql.SalesChannelsModels
{
    public partial class SalesChannelsContext : DbContext
    {
        public SalesChannelsContext()
        {
        }

        public SalesChannelsContext(DbContextOptions<SalesChannelsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<Channel> Channels { get; set; }
        public virtual DbSet<CoolDownPeriod> CoolDownPeriods { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryRecipientType> DictionaryRecipientTypes { get; set; }
        public virtual DbSet<DictionarySalesChannelStatus> DictionarySalesChannelStatuses { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<PartnerEmail> PartnerEmails { get; set; }
        public virtual DbSet<PredefinedEmail> PredefinedEmails { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<SalesChannel> SalesChannels { get; set; }
        public virtual DbSet<SalesChannelEmail> SalesChannelEmails { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<SwitchWindow> SwitchWindows { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=db20.nutsservices.local;Database=SalesChannels;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.ToTable("Campaign");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Channel>(entity =>
            {
                entity.ToTable("Channel");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CoolDownPeriod>(entity =>
            {
                entity.ToTable("CoolDownPeriod");
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Labels");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryRecipientType>(entity =>
            {
                entity.ToTable("Dictionary_RecipientType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionarySalesChannelStatus>(entity =>
            {
                entity.ToTable("Dictionary_SalesChannelStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Partner>(entity =>
            {
                entity.ToTable("Partner");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PartnerEmail>(entity =>
            {
                entity.ToTable("PartnerEmail");

                entity.HasIndex(e => e.PartnerId, "IX_PartnerEmail_PartnerId");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.PartnerEmails)
                    .HasForeignKey(d => d.PartnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PredefinedEmail>(entity =>
            {
                entity.ToTable("PredefinedEmail");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.ToTable("Region");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SalesChannel>(entity =>
            {
                entity.ToTable("SalesChannel");

                entity.HasIndex(e => e.CampaignId, "IX_SalesChannel_CampaignId");

                entity.HasIndex(e => e.ChannelId, "IX_SalesChannel_ChannelId");

                entity.HasIndex(e => e.CoolDownPeriodId, "IX_SalesChannel_CoolDownPeriodId");

                entity.HasIndex(e => e.LabelId, "IX_SalesChannel_LabelId");

                entity.HasIndex(e => e.PartnerId, "IX_SalesChannel_PartnerId");

                entity.HasIndex(e => e.RegionId, "IX_SalesChannel_RegionId");

                entity.HasIndex(e => e.StatusId, "IX_SalesChannel_StatusId");

                entity.HasIndex(e => e.StoreId, "IX_SalesChannel_StoreId");

                entity.HasIndex(e => e.SwitchWindowId, "IX_SalesChannel_SwitchWindowId");

                entity.Property(e => e.OldReferenceId).HasMaxLength(50);

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.CampaignId);

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CoolDownPeriod)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.CoolDownPeriodId);

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.LabelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.PartnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.RegionId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.StoreId);

                entity.HasOne(d => d.SwitchWindow)
                    .WithMany(p => p.SalesChannels)
                    .HasForeignKey(d => d.SwitchWindowId);
            });

            modelBuilder.Entity<SalesChannelEmail>(entity =>
            {
                entity.ToTable("SalesChannelEmail");

                entity.HasIndex(e => e.SalesChannelId, "IX_SalesChannelEmail_SalesChannelId");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.RecipientType)
                    .WithMany(p => p.SalesChannelEmails)
                    .HasForeignKey(d => d.RecipientTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.SalesChannel)
                    .WithMany(p => p.SalesChannelEmails)
                    .HasForeignKey(d => d.SalesChannelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.ToTable("Store");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SwitchWindow>(entity =>
            {
                entity.ToTable("SwitchWindow");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
