﻿using Autotests.Clients;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using ConsDomSqlModels = Repositories.Consumption.Sql.ConsumptionModels;
using CcuSqlModels = Repositories.CommercialCharacteristics.Sql.CcuModels;
using MeteringPointSqlModels = Repositories.MeteringPoint.Sql.MeteringPointModels;
using MasterDataSqlModels = Repositories.MasterData.Sql.MasterDataModels;
using IcarSqlModels = Repositories.ICAR.Sql.ICARModels;
using Repositories.MasterData.Sql.MasterDataModels;
using Allure.Commons;
using Autotests.Core.Handlers;
using FakeMeterReadingService;
using Autotests.Core;
using Repositories.ICAR.Sql.ICARModels;
using Repositories.Consumption.Sql.ConsumptionModels;
using Repositories.MeteringPoint.Sql.MeteringPointModels;
using Repositories.CommercialCharacteristics.Sql.CcuModels;
using IMeterReadingService = Nuts.Consumption.Model.Contract.IMeterReadingService;

namespace Autotests.Consumption.Base
{
    [SetUpFixture]
    internal abstract class BaseConsDomTest : AllureReport
    {
        private ConfigHandler configHandler;

        //databases declaration
        protected DbHandler<ICARContext> IcarRepository;
        protected DbHandler<ConsumptionContext> ConsumptionRepository;
        protected DbHandler<MasterDataContext> MasterDataDb;
        protected DbHandler<MeteringPointContext> MeteringPointDb;
        protected DbHandler<CommercialCharacteristicContext> CcuDb;

        //clients declaration
        protected FakeEdsnAdminClient FakeEdsnAdminClient;
        protected ServiceClient<IMeterReadingService> MeterReadingClient;
        protected MeterReadingBindingClient FakeMeterReadingService;
        protected RestSchedulerClient Scheduler;

        //variables and properties initialization
        private const int NumOfRepeats = 3;
        public static string UserNameResponsible => "Autotests";
        public static string ConsDomResponsible => "ConsumptionService";
        public static string EdsnServiceResponsible => "EdsnService";
        public static DateTime ClosedWindow => DateTime.Now.AddDays(-100);
        public static DateTime OpenedWindow => DateTime.Now.Date;
        public static DateTime NotOpenedWindow => DateTime.Now.Date.AddDays(+10);
        public static DateTime CcuMutationDate => DateTime.Now.Date.AddMonths(-1);

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            configHandler = new ConfigHandler();
            IcarRepository =
                new DbHandler<ICARContext>(configHandler.GetConnectionString(DbConnectionName.ICARDatabase.ToString()));
            ConsumptionRepository =
                new DbHandler<ConsumptionContext>(
                    configHandler.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));
            MasterDataDb =
                new DbHandler<MasterDataContext>(
                    configHandler.GetConnectionString(DbConnectionName.MasterDataDatabase.ToString()));
            MeteringPointDb =
                new DbHandler<MeteringPointContext>(
                    configHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
            CcuDb = new DbHandler<CommercialCharacteristicContext>(
                configHandler.GetConnectionString(DbConnectionName.CcuDatabase.ToString()));

            Scheduler = new RestSchedulerClient();
            FakeEdsnAdminClient = new FakeEdsnAdminClient();
            MeterReadingClient =
                new Clients.ServiceClient<IMeterReadingService>(configHandler.ServicesEndpoints.CsdMeterReadingService);
            FakeMeterReadingService = new MeterReadingBindingClient(MeterReadingBindingClient.EndpointConfiguration.MeterReadingBinding);
            
            CleanTestDataInDb();
            FakeEdsnAdminClient.ClearAllReadings();
            FakeEdsnAdminClient.ClearAllMdus();
            FakeEdsnAdminClient.ClearAllCommercialCharacteristics();
        }

        private void CleanTestDataInDb()
        {
            var repeats = NumOfRepeats;
            while (repeats > 0)
            {
                try
                {                   
                    CleanConsumptionDb();
                    CleanIcarDb();
                    CleanCommercialCharacteristicsDb();
                    CleanMasterDataDb();
                    CleanMeteringPointDb();
                    break;
                }
                catch (DbUpdateException)
                {
                    repeats--;
                }
            }
        }

        private void CleanCommercialCharacteristicsDb()
        {
            var eans = CcuDb.GetEntitiesByCondition<CcuSqlModels.CcuHistory>(ccu =>
                ccu.EanId.StartsWith("000911"));
            foreach (var ean in eans)
            {
                foreach (var id in eans.Select(ccuHistory => ccuHistory.Id))
                {
                    CcuDb.DeleteEntitiesByCondition<CcuSqlModels.CcuStatusHistory>(ccu =>
                        ccu.CcuHistoryId == id);
                }

                CcuDb.DeleteEntitiesByCondition<CcuSqlModels.CcuHistory>(ccu => ccu.EanId.Equals(ean.EanId));
                Waiter.Wait(() => CcuDb.GetEntitiesByCondition<CcuSqlModels.CcuHistory>(ccu =>
                    ccu.EanId == ean.EanId).Count == 0);
            }
        }

        private void CleanMeteringPointDb()
        {
            var eans = MeteringPointDb.GetEntitiesByCondition<MeteringPointSqlModels.MeteringPoint>(mp =>
                mp.EanId.StartsWith("000911"));
            foreach (var ean in eans)
            {
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.AddressHistory>(r =>
                    r.AddressId == ean.AddressId);
                var mpHistories = MeteringPointDb.GetEntitiesByCondition<MeteringPointSqlModels.MeteringPointHistory>(mph =>
                
                    mph.EanId == ean.EanId);

                foreach (var mpHistory in mpHistories)
                {
                    MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.RegisterHistory>(rh =>
                        rh.MeteringPointHistoryId == mpHistory.Id);
                    MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.MeteringPointHistory>(mph =>
                        mph.Id == mpHistory.Id);
                }

                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.Register>(r =>
                    r.MeteringPoint.Id == ean.Id);
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.MeteringPoint>(mp =>
                    mp.Id == ean.Id);
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.Address>(a =>
                    a.Id == ean.AddressId);
            }

            var preSwitchEans =
                MeteringPointDb.GetEntitiesByCondition<MeteringPointSqlModels.PreSwitchMeteringPoint>(
                    mp => mp.EanId.StartsWith("000911"));
            foreach (var preSwitchEan in preSwitchEans)
            {
                var mpHistories = MeteringPointDb.GetEntitiesByCondition<MeteringPointSqlModels.MeteringPointHistory>(mph =>
                    mph.EanId == preSwitchEan.EanId);
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.PreSwitchRegister>(r =>
                    r.PreSwitchMeteringPoint.EanId == preSwitchEan.EanId);
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.AddressHistory>(r => r.EanId == preSwitchEan.EanId);
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.PreSwitchMeteringPoint>(
                    r => r.EanId == preSwitchEan.EanId);
                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.Address>(r =>
                    r.EanId == preSwitchEan.EanId);
                foreach (var mpHistory in mpHistories)
                {
                    MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.RegisterHistory>(rh =>
                        rh.MeteringPointHistoryId == mpHistory.Id);
                }

                MeteringPointDb.DeleteEntitiesByCondition<MeteringPointSqlModels.MeteringPoint>(r =>
                    r.EanId == preSwitchEan.EanId);
            }
        }

        private void CleanMasterDataDb()
        {
            var eans = MasterDataDb.GetEntitiesByCondition<MasterDataSqlModels.MduHistory>(mdu =>
                mdu.EanId.StartsWith("000911"));

            foreach (var ean in eans)
            {
                var mduHistories =
                    MasterDataDb.GetEntitiesByCondition<MasterDataSqlModels.MduHistory>(mdu =>
                        mdu.EanId == ean.EanId);
                foreach (var md in mduHistories)
                {
                    MasterDataDb.DeleteEntitiesByCondition<MasterDataSqlModels.MduStatusHistory>(mdus =>
                        mdus.MduHistoryId == md.Id);
                    MasterDataDb.DeleteEntitiesByCondition<MasterDataSqlModels.MduRegister>(mdus =>
                        mdus.MduHistoryId == md.Id);
                    MasterDataDb.DeleteEntitiesByCondition<MasterDataSqlModels.MduMutation>(mdu =>
                        mdu.MduHistoryId == md.Id);
                    MasterDataDb.DeleteEntitiesByCondition<MasterDataSqlModels.MduHistory>(mdu =>
                        mdu.Id == md.Id);
                }
            }
        }

        private void CleanIcarDb() 
        {
            var eans = IcarRepository.GetEntitiesByCondition<IcarSqlModels.MeteringPoint>(mp => mp.EanId.StartsWith("000911"));
            
            foreach (var ean in eans)
            {
                var mduHistories = IcarRepository.GetEntitiesByCondition<IcarSqlModels.MduHistory>(mdu => mdu.EanId == ean.EanId);

                foreach (var mdu in mduHistories)
                {
                    var id = mdu.Id;
                    IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.MduStatusHistory>(mdus => mdus.MduHistoryId == id);
                    IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.MduRegister>(mdus => mdus.MduHistoryId == id);
                    IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.MduMutation>(mdus => mdus.MduHistoryId == id);
                }

                IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.MduHistory>(md => md.EanId == ean.EanId);

                var meteringPoints = IcarRepository.GetEntitiesByCondition<IcarSqlModels.MeteringPoint>(mp => mp.EanId == ean.EanId);
                foreach (var mp in meteringPoints)
                {
                    IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.Register>(r => r.MeteringPointId == mp.Id);
                }

                IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.MeteringPoint>(mp => mp.EanId == ean.EanId);
                IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.AddressHistory>(ah => ah.EanId == ean.EanId);
                IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.Address>(a => a.EanId == ean.EanId);

                var ccuHistories = IcarRepository.GetEntitiesByCondition<IcarSqlModels.CcuHistory>(ccu => ccu.EanId == ean.EanId);
                foreach (var ccuHistory in ccuHistories)
                {
                    IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.CcuStatusHistory>(csh =>
                        csh.CcuHistoryId == ccuHistory.Id);
                }

                IcarRepository.DeleteEntitiesByCondition<IcarSqlModels.CcuHistory>(ccu => ccu.EanId == ean.EanId);
            }
        }

        protected void CleanMeterReadingsForEan(params EanInfo[] eanInfos)
        {
            foreach (var ean in eanInfos)
            {
                var meterReadings = ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr => mr.EanId == ean.EanId);

                foreach (var reading in meterReadings)
                {
                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.EdsnRejection>(er =>
                        er.StatusHistory.MeterReadingId == reading.Id);
                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.StatusHistory>(
                        sh => sh.MeterReadingId == reading.Id);
                    var registerReadings = ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.RegisterReading>(rr =>
                        rr.MeterReadingId == reading.Id);
                    foreach (var registerReading in registerReadings)
                    {
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.StatusHistory>(sh =>
                            sh.MeterReadingId == registerReading.Id);
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterVolume>(rv =>
                            rv.EndReadingId == registerReading.Id);
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterVolume>(rv =>
                            rv.BeginReadingId == registerReading.Id);
                    }

                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterReading>(rr =>
                        rr.MeterReadingId == reading.Id);
                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr => mr.Id == reading.Id);
                }
            }
        }
    
        private void CleanConsumptionDb()
    {
        var meterReadings =
            ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr => mr.EanId.StartsWith("000911"));
        {
                foreach (var reading in meterReadings)
                {
                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.EdsnRejection>(er =>
                        er.StatusHistory.MeterReadingId == reading.Id);
                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.StatusHistory>(
                        sh => sh.MeterReadingId == reading.Id);
                    var registerReadings = ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.RegisterReading>(rr =>
                        rr.MeterReadingId == reading.Id);
                    foreach (var registerReading in registerReadings)
                    {
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.StatusHistory>(sh =>
                            sh.MeterReadingId == registerReading.Id);
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterVolume>(rv =>
                            rv.EndReadingId == registerReading.Id);
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterVolume>(rv =>
                            rv.BeginReadingId == registerReading.Id);
                    }

                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterReading>(rr =>
                        rr.MeterReadingId == reading.Id);
                    ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr => mr.Id == reading.Id);
                }
            }
        }

        protected void RemoveAllNonHistoricalTestReadingsFromDb(params EanInfo[] eanInfos)
        {
            foreach (var eanInfo in eanInfos)
            {
                var eans= ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr=> mr.EanId == eanInfo.EanId);
                foreach (var ean in eans)
                {
                    var meterReadings= ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr =>
                        mr.EanId == ean.EanId && mr.MarketEventId != (int) MarketEvent.Historical);
                    foreach (var reading in meterReadings)
                    {

                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.EdsnRejection>(er=> er.StatusHistory.MeterReadingId == reading.Id);
                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.StatusHistory>(
                            sh => sh.MeterReadingId == reading.Id);
                        var registerReadings = ConsumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.RegisterReading>(rr =>
                            rr.MeterReadingId == reading.Id);
                        foreach (var registerReading in registerReadings)
                        {
                            ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.StatusHistory>(sh=>
                                sh.MeterReadingId == registerReading.Id);
                            ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterVolume>(rv=>
                                rv.EndReadingId == registerReading.Id);
                            ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterVolume>(rv=>
                                rv.BeginReadingId == registerReading.Id);
                        }

                        ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.RegisterReading>(rr =>
                            rr.MeterReadingId == reading.Id); ConsumptionRepository.DeleteEntitiesByCondition<ConsDomSqlModels.MeterReading>(mr => mr.Id == reading.Id);
                    }
                }
            }
        }
    }
}