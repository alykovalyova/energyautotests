﻿using System.Text.RegularExpressions;
using NUnit.Framework;
using Nuts.Consumption.Model.Contract;

namespace Autotests.Consumption.Base
{
    [SetUpFixture]
    internal abstract class CheckLastSmartMeterReadingsBase : BaseConsDomTest
    {
        protected static readonly Regex EanRegex = new Regex("^[0-9]{18}$");
        protected string WrongEanRegex = "Ean {0} doesn't match expression {1}";

        protected static CheckLastSmartMeterReadingsRequest GetCheckLastSmartMeterReadingsRequest(DateTime? fromDate = null, DateTime? toDate = null, params string[] eans)
        {
            return new CheckLastSmartMeterReadingsRequest
            {
                EansList = eans.ToList(),
                FromDate = fromDate ?? DateTime.Now,
                ToDate = toDate ?? DateTime.Now
            };
        }
    }
}