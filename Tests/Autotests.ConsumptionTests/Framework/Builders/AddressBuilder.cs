﻿using Autotests.Core.Helpers;
using Address = Nuts.EdsnGateway.Contract.Queue.Mdu.Address;

namespace Autotests.Consumption.Framework.Builders
{
    internal class AddressBuilder : MainBuilder<Address>
    {
        internal AddressBuilder SetDefaults()
        {
            Instance.Country = "NL";
            return this;
        }

        internal AddressBuilder SetZipCode(string zipCode)
        {
            Instance.ZIPCode = zipCode;
            return this;
        }

        internal AddressBuilder SetBuildingNr(int buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal AddressBuilder SetStreetName(string streetName)
        {
            Instance.StreetName = streetName;
            return this;
        }

        internal AddressBuilder SetExBuildingNr(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }
    }
}
