﻿namespace Autotests.Consumption.Framework.Builders
{
    public class BuildDirector
    {
        private static readonly Lazy<BuildDirector> _lazy = new Lazy<BuildDirector>(() => new BuildDirector());

        private BuildDirector()
        {
            AddressBuilder = new AddressBuilder();
            CommercialCharacteristicsBuilder = new CommercialCharacteristicsBuilder();
            EnergyMeterBuilder = new EnergyMeterBuilder();
            MasterDataUpdateBuilder = new MasterDataUpdateBuilder();
            MutationBuilder = new MutationBuilder();
            RegisterBuilder = new RegisterBuilder();
            PhysicalCharacteristicsBuilder = new PhysicalCharacteristicsBuilder();
            SupplyPhaseBuilder = new SupplyPhaseBuilder();
            CommercialCharacteristicsForSupplyPhaseBuilder = new CommercialCharacteristicsForSupplyPhaseBuilder();
            SupplyPhaseAddressBuilder = new SupplyPhaseAddressBuilder();
            MoveInDataBuilder = new MoveInDataBuilder();
        }

        internal static BuildDirector Get => _lazy.Value;
        internal AddressBuilder AddressBuilder { get; }
        internal CommercialCharacteristicsBuilder CommercialCharacteristicsBuilder { get; }
        internal EnergyMeterBuilder EnergyMeterBuilder { get; }
        internal MasterDataUpdateBuilder MasterDataUpdateBuilder { get;  }
        internal MutationBuilder MutationBuilder { get; }
        internal RegisterBuilder RegisterBuilder { get; }
        internal PhysicalCharacteristicsBuilder PhysicalCharacteristicsBuilder { get; }
        internal SupplyPhaseBuilder SupplyPhaseBuilder { get; }
        internal CommercialCharacteristicsForSupplyPhaseBuilder CommercialCharacteristicsForSupplyPhaseBuilder { get; }
        internal SupplyPhaseAddressBuilder SupplyPhaseAddressBuilder { get;  }
        internal MoveInDataBuilder MoveInDataBuilder { get; }
    }
}