﻿using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using CommercialCharacteristics = Nuts.EdsnGateway.Contract.Queue.Mdu.CommercialCharacteristics;

namespace Autotests.Consumption.Framework.Builders
{
    public class CommercialCharacteristicsBuilder : MainBuilder<CommercialCharacteristics>
    {
        internal CommercialCharacteristicsBuilder SetDefaults()
        {
            Instance.BalanceResponsibleParty = TestConstants.BalanceResponsible;
            Instance.MeteringResponsibleParty = TestConstants.MeteringResponsible;
            return this;
        }

        internal CommercialCharacteristicsBuilder SetBalanceSupplier(string balanceSupplier)
        {
            Instance.BalanceSupplier = balanceSupplier;
            return this;
        }
    }
}
