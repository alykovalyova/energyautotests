﻿using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;

namespace Autotests.Consumption.Framework.Builders
{
    public class CommercialCharacteristicsForSupplyPhaseBuilder : MainBuilder<GetMeteringPointResponseEnvelope_PC_PMP_MPCC>
    {
        public CommercialCharacteristicsForSupplyPhaseBuilder SetBase(string balanceResponsible)
        {
            Instance.BalanceResponsibleParty_Company =
                new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_BalanceResponsibleParty_Company()
                {
                    ID = balanceResponsible
                };
            Instance.GridContractParty = new[]
            {
                new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_GridContractParty()
                {
                    Initials = TestConstants.Initials,
                    Surname = TestConstants.Surname,
                    SurnamePrefix = TestConstants.SurnamePrefix
                }
            };
            Instance.MeteringResponsibleParty_Company =
                new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_MeteringResponsibleParty_Company()
                {
                    ID = TestConstants.BalanceResponsible
                };
            return this;
        }

        internal CommercialCharacteristicsForSupplyPhaseBuilder SetBalanceSupplier(string balanceSupplier)
        {
            Instance.BalanceSupplier_Company =
                new GetMeteringPointResponseEnvelope_PC_PMP_MPCC_BalanceSupplier_Company()
                {
                    ID = balanceSupplier
                };
            return this;
        }
    }
}
