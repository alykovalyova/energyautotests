﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using EnergyMeter = Nuts.EdsnGateway.Contract.Queue.Mdu.EnergyMeter;
using Register = Nuts.EdsnGateway.Contract.Queue.Mdu.Register;

namespace Autotests.Consumption.Framework.Builders
{
    internal class EnergyMeterBuilder : MainBuilder<EnergyMeter>
    {
        internal EnergyMeterBuilder SetRegisterNr(int regNr)
        {
            Instance.NrOfRegisters = regNr;
            return this;
        }

        internal EnergyMeterBuilder SetEnergyMeterId(string meterId)
        {
            Instance.EdsnId = meterId;
            return this;
        }

        internal EnergyMeterBuilder SetMeterType(EnergyMeterTypeCode meterType)
        {
            Instance.Type = meterType;
            return this;
        }

        internal EnergyMeterBuilder SetRegisters(params Register[] registers)
        {
            Instance.Registers = registers.ToList();
            return this;
        }
    }
}
