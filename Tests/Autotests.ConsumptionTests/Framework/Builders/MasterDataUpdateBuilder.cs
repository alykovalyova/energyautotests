﻿using Nuts.InterDom.Model.Core.Enums;
using MasterDataUpdate = Nuts.EdsnGateway.Contract.Queue.Mdu.MasterDataUpdate;
using Mutations = Nuts.InterDom.Model.Messages.Mutation;
using Address = Nuts.EdsnGateway.Contract.Queue.Mdu.Address;
using EnergyMeter = Nuts.EdsnGateway.Contract.Queue.Mdu.EnergyMeter;
using PhysicalCharacteristics = Nuts.EdsnGateway.Contract.Queue.Mdu.PhysicalCharacteristics;
using CommercialCharacteristics = Nuts.EdsnGateway.Contract.Queue.Mdu.CommercialCharacteristics;
using Autotests.Core.TestDataProviders;
using Autotests.Core.Helpers;

namespace Autotests.Consumption.Framework.Builders
{
    internal class MasterDataUpdateBuilder : MainBuilder<MasterDataUpdate>
    {
        internal MasterDataUpdateBuilder SetDefaults(EanInfo ean,  
            MarketSegmentCode marketSegment = MarketSegmentCode.KVB, string locationDescription = "autotestLocationDescription")
        {
            Instance.EanId = ean.EanId;
            Instance.LocationDescription = locationDescription;
            Instance.MarketSegment = marketSegment;
            Instance.HeaderCreationTS = DateTime.Today;
            Instance.ProductType = ean.ProductType;
            Instance.AdministrativeStatusSmartMeter = ean.AdministrativeStatusSmartMeter;
            Instance.GridArea = ean.GridArea;
            Instance.GridOperator = ean.GridOperator;
            return this;
        }

        internal MasterDataUpdateBuilder SetMutation(params Mutations[] mutations)
        {
            this.Instance.Mutation = mutations.ToList();
            return this;
        }

        internal MasterDataUpdateBuilder SetAddress(Address address)
        {
            this.Instance.Address = address;
            return this;
        }

        internal MasterDataUpdateBuilder SetEnergyMeter(EnergyMeter energyMeter)
        {
            this.Instance.EnergyMeter = energyMeter;
            return this;
        }

        internal MasterDataUpdateBuilder SetPhysicalCharacteristics(PhysicalCharacteristics physicalCharacteristics)
        {
            this.Instance.PhysicalCharacteristics = physicalCharacteristics;
            return this;
        }

        internal MasterDataUpdateBuilder SetCommercialCharacteristics(CommercialCharacteristics commercialCharacteristics)
        {
            this.Instance.CommercialCharacteristics = commercialCharacteristics;
            return this;
        }
    }
}
