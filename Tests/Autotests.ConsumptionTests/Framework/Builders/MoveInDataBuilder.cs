﻿using Autotests.Core.Helpers;
using Nuts.EdsnSwitch.Contract.Rest.Model;
using System;

namespace Autotests.Consumption.Framework.Builders
{
    internal class MoveInDataBuilder : MainBuilder<MoveInData>
    {
        internal MoveInDataBuilder SetEan(string ean)
        {
            Instance.Ean = ean;
            return this;
        }

        internal MoveInDataBuilder SetBalanceResponsibleId(string balanceResponsibleId)
        {
            Instance.BalanceResponsibleId = balanceResponsibleId;
            return this;
        }

        internal MoveInDataBuilder SetBalanceSupplierId(string balanceSupplierId)
        {
            Instance.BalanceSupplierId = balanceSupplierId;
            return this;
        }

        internal MoveInDataBuilder SetGridOperatorId(string gridOperatorId)
        {
            Instance.GridOperatorId = gridOperatorId;
            return this;
        }

        internal MoveInDataBuilder SetReferenceId(string referenceId)
        {
            Instance.ReferenceId = referenceId;
            return this;
        }

        internal MoveInDataBuilder SetGridContractParty(MoveInContract gridContractParty)
        {
            Instance.GridContractParty = gridContractParty;
            return this;
        }

        internal MoveInDataBuilder SetAddress(Address address)
        {
            Instance.Address = address;
            return this;
        }

        internal MoveInDataBuilder SetKvkNumber(string kvkNumber)
        {
            Instance.KvkNumber = kvkNumber;
            return this;
        }

        internal MoveInDataBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }
    }
}
