﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Mutations = Nuts.InterDom.Model.Messages.Mutation;

namespace Autotests.Consumption.Framework.Builders
{
    internal class MutationBuilder : MainBuilder<Mutations>
    {
        internal MutationBuilder SetDossierId(string dossierId)
        {
            Instance.DossierID = dossierId;
            return this;
        }

        internal MutationBuilder SetExternalReference(string extReference)
        {
            Instance.ExternalReference = extReference;
            return this;
        }

        internal MutationBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }

        internal MutationBuilder SetMutationReason(MutationReasonCode mutationReason)
        {
            Instance.MutationReason = mutationReason;
            return this;
        }
    }
}
