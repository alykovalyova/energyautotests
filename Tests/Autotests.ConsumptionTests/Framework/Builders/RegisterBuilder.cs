﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using Register = Nuts.EdsnGateway.Contract.Queue.Mdu.Register;

namespace Autotests.Consumption.Framework.Builders
{
    internal class RegisterBuilder : MainBuilder<Register>
    {
        internal RegisterBuilder SetDefaults()
        {
            Instance.MultiplicationFactor = 1;
            Instance.NrOfDigits = 6;
            return this;
        }

        internal RegisterBuilder SetEdsnId(string id)
        {
            Instance.EdsnId = id;
            return this;
        }

        internal RegisterBuilder SetMeteringDirection(EnergyFlowDirectionCode flowDirection)
        {
            Instance.MeteringDirection = flowDirection;
            return this;
        }

        internal RegisterBuilder SetTariffType(EnergyTariffTypeCode tariffType)
        {
            Instance.TariffType = tariffType;
            return this;
        }
    }
}
