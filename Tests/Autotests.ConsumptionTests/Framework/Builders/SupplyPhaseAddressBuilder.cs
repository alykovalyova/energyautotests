﻿using Autotests.Core.Helpers;

namespace Autotests.Consumption.Framework.Builders
{
    internal class SupplyPhaseAddressBuilder : MainBuilder<GetMeteringPointResponseEnvelope_Address>
    {
        internal SupplyPhaseAddressBuilder SetBase()
        {
            Instance.ExBuildingNr = "B";
            Instance.Country = "NL";
            return this;
        }
        internal SupplyPhaseAddressBuilder SetBuildingNr(string buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal SupplyPhaseAddressBuilder SetZipcode(string zipcode)
        {
            Instance.ZIPCode = zipcode;
            return this;
        }

        internal SupplyPhaseAddressBuilder SetStreetName(string streetName)
        {
            Instance.StreetName = streetName;
            return this;
        }
    }
}
