﻿using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;

namespace Autotests.Consumption.Framework.Builders
{
    public class SupplyPhaseBuilder : MainBuilder<GetMeteringPointResponseEnvelope_PC_PMP>
    {
        public SupplyPhaseBuilder SetEanId(string eanId)
        {
            Instance.EANID = eanId;
            return this;
        }

        public SupplyPhaseBuilder SetLocationDescription(string locationDescription)
        {
            Instance.LocationDescription = locationDescription;
            return this;
        }

        public SupplyPhaseBuilder SetIsSmartMeterProperty(bool isSmartMeter)
        {
            Instance.AdministrativeStatusSmartMeter = isSmartMeter == true ? "AAN" : "0";
            return this;
        }

        public SupplyPhaseBuilder SetGridOperator(string gridoperator)
        {
            Instance.GridOperator_Company = new GetMeteringPointResponseEnvelope_PC_PMP_GridOperator_Company()
            { ID = gridoperator };
            return this;
        }

        public SupplyPhaseBuilder SetGridArea(string gridArea)
        {
            Instance.GridArea = gridArea;
            return this;
        }

        public SupplyPhaseBuilder SetMarketSegment(string marketSegment)
        {
            Instance.MarketSegment = marketSegment;
            return this;
        }

        public SupplyPhaseBuilder SetProductType(string productType)
        {
            Instance.ProductType = productType;
            return this;
        }

        public SupplyPhaseBuilder SetEnergyMeter(GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter energyMeter)
        {
            Instance.Portaal_EnergyMeter = energyMeter;
            return this;
        }

        public SupplyPhaseBuilder SetEnergyMeterFromEanInfo(EanInfo ean)
        {
            Instance.Portaal_EnergyMeter = new GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter()
            {
                ID = ean.EnergyMeterId,
                NrOfRegisters = ean.RegisterNumber.ToString(),
                Register = ean.Registers
            };
            return this;
        }

        public SupplyPhaseBuilder SetMeteringPointGroup(GetMeteringPointResponseEnvelope_PC_PMP_MeteringPointGroup mpGroup)
        {
            Instance.MeteringPointGroup = mpGroup;
            return this;
        }

        public SupplyPhaseBuilder SetPhysicalCharacteristic(GetMeteringPointResponseEnvelope_PC_PMP_MPPhysicalCharacteristics physicalCharacteristics)
        {
            Instance.MPPhysicalCharacteristics = physicalCharacteristics;
            return this;
        }

        public SupplyPhaseBuilder SetPhysicalCharacteristicFromEanInfo(GetMeteringPointResponseEnvelope_PC_PMP_MPPhysicalCharacteristics physicalCharacteristics, EanInfo ean)
        {
            Instance.MPPhysicalCharacteristics = new GetMeteringPointResponseEnvelope_PC_PMP_MPPhysicalCharacteristics()
            {
                AllocationMethod = ean.AllocationCode.ToString(),
                CapTarCode = physicalCharacteristics.CapTarCode,
                EAEnergyConsumptionNettedOffPeak = ean.EacOffPeak.ToString(),
                EAEnergyConsumptionNettedPeak = ean.EacPeak.ToString(),
                EAEnergyProductionNettedOffPeak = physicalCharacteristics.EAEnergyProductionNettedOffPeak,
                EAEnergyProductionNettedPeak = physicalCharacteristics.EAEnergyProductionNettedPeak,
                EnergyDeliveryStatus = physicalCharacteristics.EnergyDeliveryStatus,
                EnergyFlowDirection = ean.FlowDirection.ToString(),
                PhysicalStatus = ean.PhysicalStatus.ToString(),
                PhysicalCapacity = ean.Capacity.ToString().Remove(0, 1),
                ProfileCategory = ean.ProfileCategory.ToString(),
                MeteringMethod = physicalCharacteristics.MeteringMethod,
            };
            return this;
        }

        public SupplyPhaseBuilder SetAddress(GetMeteringPointResponseEnvelope_Address address)
        {
            Instance.EDSN_AddressExtended = new[] { address };
            return this;
        }

        public SupplyPhaseBuilder SetCommercialCharacteristics(GetMeteringPointResponseEnvelope_PC_PMP_MPCC commercialCharacteristics)
        {
            Instance.MPCommercialCharacteristics = commercialCharacteristics;
            return this;
        }

        public SupplyPhaseBuilder SetValidFromDate(DateTime date)
        {
            Instance.ValidFromDate = date;
            return this;
        }
    }
}
