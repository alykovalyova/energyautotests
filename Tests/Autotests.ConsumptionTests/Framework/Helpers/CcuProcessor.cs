﻿using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using CcuSqlModels = Repositories.CommercialCharacteristics.Sql.CcuModels;

namespace Autotests.Consumption.Framework.Helpers
{
    public class CcuProcessor
    {
        private static readonly SupplyPhaseEntitiesProvider _supplyPhaseEntity = new SupplyPhaseEntitiesProvider();
        private static readonly RestSchedulerClient _restSchedulerClient = RestSchedulerClient.Instance;
        private static readonly CcuEntitiesProvider _ccuEntityProvider = new CcuEntitiesProvider();
        private static readonly FakeEdsnAdminClient _fakeEdsnClient = new FakeEdsnAdminClient();
        private static readonly DbHandler<CcuSqlModels.CommercialCharacteristicContext> _ccuRepository = new DbHandler<CcuSqlModels.CommercialCharacteristicContext>(
           ConfigHandler.Instance.GetConnectionString(DbConnectionName.CcuDatabase.ToString()));

        public static void SendMoveInCcu(List<EanInfo> eans, DateTime mutationDate)
        {
            foreach (var ean in eans)
            {
                _supplyPhaseEntity.AddSupplyPhaseFromEanInfo(ean, mutationDate);
                var ccu = _ccuEntityProvider.CreateCommercialCharacteristic(ean.EanId,
                    TestConstants.OtherPartyBalanceSupplier, TestConstants.OtherPartyBalanceSupplier,
                    mutationDate, ean.DossierId);
                _fakeEdsnClient.SendInitialCommercialCharacteristic(ccu);

                _ccuEntityProvider.CallMoveIn(ean.EanId, TestConstants.BudgetSupplier, mutationDate);
                _restSchedulerClient.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetGainsFake);
                Waiter.Wait(() => _ccuRepository.GetEntityByCondition<CcuSqlModels.CcuHistory>(
                        m => m.EanId == ean.EanId && m.MutationDate == mutationDate && m.StatusId == 2));
            }
        }
    }
}
