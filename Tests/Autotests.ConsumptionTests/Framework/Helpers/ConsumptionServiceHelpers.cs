﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Consumption.Base;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FakeMeterReadingService;
using NUnit.Framework;
using Nuts.Consumption.Model.Contract;
using Nuts.Consumption.Model.Core;
using Nuts.EdsnGateway.Contract.MeterReadings.Headers;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;
using EdsnIMeterReadingService = Nuts.EdsnGateway.Contract.MeterReadings.IMeterReadingService;
using MeterReading = Nuts.Consumption.Model.Contract.MeterReading;
using RegisterReading = Nuts.Consumption.Model.Contract.RegisterReading;
using ConsDomSqlModels = Repositories.Consumption.Sql.ConsumptionModels;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class ConsumptionServiceHelpers
    {
        private static readonly ConfigHandler _config = ConfigHandler.Instance;
        private static readonly RestSchedulerClient _nutsScheduler = RestSchedulerClient.Instance;
        private static readonly FakeEdsnAdminClient _fakeEdsnAdminClient;
        private static readonly ServiceClient<IMeterReadingService> _conMeterReadingClient;
        private static readonly ServiceClient<EdsnIMeterReadingService> _egvMeterReadingClient;
        private static readonly DbHandler<ConsDomSqlModels.ConsumptionContext> _consumptionRepository;
        private static readonly MeterReadingBindingClient _fakeMeterReadingService;

        static ConsumptionServiceHelpers()
        {
            _fakeEdsnAdminClient = new FakeEdsnAdminClient();
            _fakeMeterReadingService = new MeterReadingBindingClient(MeterReadingBindingClient.EndpointConfiguration.MeterReadingBinding);
            _conMeterReadingClient = new ServiceClient<IMeterReadingService>(_config.ServicesEndpoints.CsdMeterReadingService);
            _egvMeterReadingClient = new ServiceClient<EdsnIMeterReadingService>(_config.ServicesEndpoints.EgwMeterReadingService);
            _consumptionRepository = new DbHandler<ConsDomSqlModels.ConsumptionContext>(_config.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));
        }

        public static GetLastAcceptedReadingResponse GetLastAcceptedReading(string ean, DateTime fromDate,
            ReadingSource sourceToSkip)
        {
            var response = new GetLastAcceptedReadingRequest
            {
                Eans = new List<string> {ean},
                FromDate = fromDate,
                SourceToSkip = new List<ReadingSource> {sourceToSkip}
            }.CallWith(_conMeterReadingClient.Proxy.GetLastAcceptedReading);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response;
        }

        public static ProcessStatus GetMeterReadingStatusById(SavedMeterReadingInfo saveReading)
        {
            var meterReadingId = saveReading.MeterReadingId;
            var response = new GetMeterReadingByIdRequest {MeterReadingId = meterReadingId}
                .CallWith(_conMeterReadingClient.Proxy.GetMeterReadingById);
            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response.MeterReading.Status;
        }

        public static SaveMeterReadingRequest SaveMeterReadingRequest(ReadingsSet readingsSet,
            bool skipValidation = false)
        {
            return new SaveMeterReadingRequest
            {
                MeterReadingsSet = new List<ReadingsSet> {readingsSet},
                Reason = new ProcessStatusReason
                {
                    Reason = StatusReason.InsertedByAgent,
                    Comment = "Insert test readings",
                    User = BaseConsDomTest.UserNameResponsible
                },
                SkipValidation = skipValidation,
            };
        }

        public static ReadingsSet CreateVerifyByAgentReadingSet(string ean, MarketEvent me, ReadingSource rs,
            DateTime date, bool isTfSignal = false)
        {
            var estimation = EstimateReadings(ean, me, date);
            var estimationResult = estimation.EstimationData.First();
            estimationResult.EstimationRegisters.ForEach(re =>
                re.Estimation.Estimation = re.Estimation.EstimationMax + 500);

            var meterReading =
                GeneralHelpers.GetDomainMeterReadingFromEstimationResult(estimationResult, ean, rs, me, date);
            var readingsSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            readingsSet.ReadingSource = rs;
            if (isTfSignal)
            {
                var value = readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.N)
                    .Value + readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.L)
                    .Value;
                readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.N)
                    .Value = value;
            }

            return readingsSet;
        }

        public static ReadingsSet CreateReadingSet(EanInfo ean, MarketEvent me, ReadingSource rs, DateTime date, 
            bool isTfSignalReading = false)
        {
            var estimation = EstimateReadings(ean.EanId, me, date);
            var meterReading = GeneralHelpers.GetDomainMeterReadingFromEstimationResult(
                estimation.EstimationData.FirstOrDefault(), ean.DossierId, rs, me, date);

            var readingsSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            readingsSet.ReadingSource = rs;
            if (me == MarketEvent.Yearly || me == MarketEvent.Spontaneous || me == MarketEvent.TwoMonthly)
            {
                readingsSet.DossierId = null;
            }

            if (isTfSignalReading)
            {
                var value = readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.N)
                    .Value + readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.L)
                    .Value;
                readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.N)
                    .Value = value;
                readingsSet.Readings.First(r => r.TariffType == EnergyTariffTypeCode.L)
                    .Value = 0;
            }

            return readingsSet;
        }

        public static ReadingsSetInfo CreateReadingSetInfo(EanInfo ean, MarketEvent me, ReadingSource rs, DateTime date, bool isTfSignalReading = false)
        {
            var estimation = EstimateReadings(ean.EanId, me, date);
            var meterReading = GeneralHelpers.GetDomainMeterReadingFromEstimationResult(estimation.EstimationData
                .FirstOrDefault(), ean.DossierId, rs, me, date);
            var readingsSetInfo = GeneralHelpers.GetReadingSetInfoFromReading(meterReading);
            if (isTfSignalReading)
            {
                var value = readingsSetInfo.Readings.First(r => r.TariffType == EnergyTariffTypeCode.N)
                    .Value + readingsSetInfo.Readings.First(r => r.TariffType == EnergyTariffTypeCode.L)
                    .Value;
                readingsSetInfo.Readings.First(r => r.TariffType == EnergyTariffTypeCode.N)
                    .Value = value;
            }

            return readingsSetInfo;
        }

        public static ReadingsSet CreateTechExcpReadingSet(EanInfo ean, MarketEvent me, ReadingSource rs, DateTime date)
        {
            var estimation = EstimateReadings(ean.EanId, me, date);
            var meterReading = GeneralHelpers.GetDomainMeterReadingFromEstimationResult(estimation.EstimationData
                .FirstOrDefault(), ean.DossierId, rs, me, date);

            if (meterReading.RegisterReadings.Count >= 2)
            {
                meterReading.RegisterReadings.RemoveAt(1);
            }
            else if (meterReading.RegisterReadings.Count == 1)
            {
                var rr = new RegisterReading
                {
                    MeasureUnit = MeasureUnitCode.MTQ,
                    MeteringDirection = EnergyFlowDirectionCode.LVR,
                    NrOfDigits = 5,
                    RegisterNo = "2",
                    TariffType = EnergyTariffTypeCode.L,
                    Value = 400
                };
                meterReading.RegisterReadings.Add(rr);
            }

            var readingsSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            readingsSet.ReadingSource = rs;

            return readingsSet;
        }

        public static string ProcessReadingInFakeEdsn(
            Nuts.EdsnGateway.Contract.MeterReadings.MeterReadings.MeterReading meterReading, MeterReadingStatus status)
        {
            var response = new SendMeterReadingsRequest
            {
                MeteringReadings = new List<Nuts.EdsnGateway.Contract.MeterReadings.MeterReadings.MeterReading>
                {
                    meterReading
                }
            }.CallWith(_egvMeterReadingClient.Proxy.SendMeterReadings);
            if (response == null) return string.Empty;
            _fakeEdsnAdminClient.ProcessMeterReadingByExternalReference(meterReading.Mutation.ExternalReference,
                status);

            return meterReading.Mutation.ExternalReference;
        }

        public static ReadingsSet CreateReadingsSetWithoutDossier(EanInfo ean, MarketEvent me, ReadingSource rs, DateTime date)
        {
            var estimation = EstimateReadings(ean.EanId, me, date);
            var meterReading = GeneralHelpers.GetDomainMeterReadingFromEstimationResult(estimation.EstimationData
                .FirstOrDefault(), ean.DossierId, rs, me, date);
            meterReading.Mutation.DossierId = null;
            var readingsSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            readingsSet.ReadingSource = rs;

            return readingsSet;
        }

        public static EstimateReadingResponse EstimateReadings(string ean, MarketEvent me, DateTime date)
        {
            var response = new EstimateReadingRequest
            {
                EansList = new List<string> {ean},
                EstimationDate = date,
                MarketEvent = me
            }.CallWith(_conMeterReadingClient.Proxy.EstimateReading);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response;
        }

        public static void UpdateReadingById(ProcessStatus newStatus, int meterReadingId)
        {
            var response = new UpdateMeterReadingStatusRequest
            {
                MeterReadingIds = new List<int> {meterReadingId},
                NewStatus = newStatus,
                Reason = new ProcessStatusReason
                {
                    Reason = StatusReason.InsertedByAgent,
                    User = BaseConsDomTest.UserNameResponsible
                }
            }.CallWith(_conMeterReadingClient.Proxy.UpdateMeterReadingStatus);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);
        }

        public static ValidateMeterReadingResponse ValidateReadingsByEstimation(EstimationResult estimationResult,
            MarketEvent me, DateTime date, bool isTfSignal = false, bool isInvalidTfSignal = false)
        {
            var request = new ValidateMeterReadingRequest
            {
                ReadingsToValidate = new List<ReadingsSetInfo>
                {
                    new ReadingsSetInfo
                    {
                        EanId = estimationResult.EanId,
                        MarketEvent = me,
                        MarketEventDate = date,
                        Readings = estimationResult.EstimationRegisters.Select(e => new Reading
                        {
                            MeteringDirection = e.MeteringDirection.GetValueOrDefault(),
                            TariffType = e.TariffType.GetValueOrDefault(), Value = e.Estimation.Estimation
                        }).ToList()
                    }
                }
            };

            if (isTfSignal)
            {
                var value = request.ReadingsToValidate.First().Readings
                    .First(r => r.TariffType == EnergyTariffTypeCode.N && r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value + request.ReadingsToValidate
                    .First().Readings.First(r => r.TariffType == EnergyTariffTypeCode.L && r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value;
                request.ReadingsToValidate.First().Readings.First(r => r.TariffType == EnergyTariffTypeCode.N && r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value = value;
                request.ReadingsToValidate.First().Readings.First(r => r.TariffType == EnergyTariffTypeCode.L && r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value = 0;
            }

            if (isTfSignal && isInvalidTfSignal)
            {
                request.ReadingsToValidate.First().Readings
                    .First(r => r.TariffType == EnergyTariffTypeCode.L && r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value = 500;
            }

            var response = request.CallWith(_conMeterReadingClient.Proxy.ValidateMeterReading);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response;
        }

        public static StartSettlementResponse StartSettlementByOtherParty(MeterReading meterReading, int meterReadingId)
        {
            meterReading.RegisterReadings.ForEach(re => re.Value += 5);
            var readingSetToSettle = GeneralHelpers.GetReadingSetFromReading(meterReading);
            Thread.Sleep(3000);
            var response = new StartSettlementRequest
            {
                SettlementRequest = new SettlementRequest
                {
                    Readings = readingSetToSettle.Readings,
                    MeterReadingId = meterReadingId,
                    User = BaseConsDomTest.UserNameResponsible
                }
            }.CallWith(_conMeterReadingClient.Proxy.OtherPartyStartSettlement);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response;
        }

        public static StartSettlementResponse StartSettlementByBudget(MeterReading meterReading, int id)
        {
            meterReading.RegisterReadings.ForEach(re => re.Value = re.Value + 5);
            var readingSetToSettle = GeneralHelpers.GetReadingSetFromReading(meterReading);
            Thread.Sleep(30);
            var response = new StartSettlementRequest
            {
                SettlementRequest = new SettlementRequest
                {
                    Readings = readingSetToSettle.Readings,
                    MeterReadingId = id,
                    User = BaseConsDomTest.UserNameResponsible
                }
            }.CallWith(_conMeterReadingClient.Proxy.BudgetStartSettlement);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response;
        }

        public static void StartDisputeByBudget(MeterReading meterReading, int id)
        {
            meterReading.RegisterReadings.ForEach(re => re.Value = re.Value + 5);
            var readingSetToDispute = GeneralHelpers.GetReadingSetFromReading(meterReading);
            
            var response = new StartDisputeRequest
            {
                DisputeRequest = new DisputeRequest
                {
                    Readings = readingSetToDispute.Readings,
                    MeterReadingId = id,
                    User = BaseConsDomTest.UserNameResponsible,
                    Comment = "testtest"
                }
            }.CallWith(_conMeterReadingClient.Proxy.BudgetStartDispute);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);
        }

        public static GetMeterReadingByIdResponse GetMeterReadingById(int meterReadingId)
        {
            var response = new GetMeterReadingByIdRequest {MeterReadingId = meterReadingId}
                .CallWith(_conMeterReadingClient.Proxy.GetMeterReadingById);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response;
        }

        public static void AnswerSettlementByBudget(int readingId, bool answer)
        {
            var response = new AnswerSettlementRequest
            {
                MeterReadingId = readingId,
                IsAccepted = answer,
                Comment = "accept settlement",
                User = BaseConsDomTest.UserNameResponsible
            }.CallWith(_conMeterReadingClient.Proxy.BudgetAnswerSettlement);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);
        }

        public static void AnswerSettlementByOtherParty(int readingId, bool answer)
        {
            var response = new AnswerSettlementRequest
            {
                MeterReadingId = readingId,
                IsAccepted = answer,
                Comment = "accept settlement",
                User = BaseConsDomTest.UserNameResponsible
            }.CallWith(_conMeterReadingClient.Proxy.OtherPartyAnswerSettlement);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);
        }

        public static MeterReading AcceptConsDomMeterReading(EanInfo ean, DateTime date, MarketEvent me,
            ReadingSource rs)
        {
            var mp = GeneralHelpers.GetMeteringPointsByDate(ean.EanId, date);
            var meterReading = GeneralHelpers.GetDomainMeterReadingFromMeterPoint(mp, ean.DossierId, me, date, rs);
            var readingSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            readingSet.MarketEventDate = readingSet.MarketEventDate.Date;
            var saveReadingResponse = SaveMeterReadingRequest(readingSet, true)
                .CallWith(_conMeterReadingClient.Proxy.SaveMeterReading);
            SendMrToEdsn(saveReadingResponse);

            var mrId = saveReadingResponse.SavedMeterReadings.First().MeterReadingId;
            var externalReference = _consumptionRepository
                .GetEntityByCondition<ConsDomSqlModels.MeterReading>(m => m.Id == mrId,
                    "RegisterReading")
                .ExternalReference;
            SetReadingStatusToAccepted(mrId, externalReference);

            var eanId = saveReadingResponse.SavedMeterReadings.First().EanId;
            var getMeterReadingByIdResponse = new GetMeterReadingByIdRequest
            {
                MeterReadingId = _consumptionRepository.GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(
                    m => m.MarketEventDate, m => m.EanId == eanId && m.StatusId == (int) ProcessStatus.Accepted).Id
            }.CallWith(_conMeterReadingClient.Proxy.GetMeterReadingById);

            if (!getMeterReadingByIdResponse.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(getMeterReadingByIdResponse.Header.Message);
            Assert.AreEqual(ProcessStatus.Accepted, getMeterReadingByIdResponse.MeterReading.Status);

            return getMeterReadingByIdResponse.MeterReading;
        }

        public static MeterReading RejectConsDomMeterReading(
            EanInfo ean, DateTime date, MarketEvent me, ReadingSource rs, MeterReadingStatus status)
        {
            var estimationResult = EstimateReadings(ean.EanId, me, date).EstimationData.First();
            var meterReading =
                GeneralHelpers.GetDomainMeterReadingFromEstimationResult(estimationResult, null, rs, me, date);
            var readingSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            var saveReadingResponse = SaveMeterReadingRequest(readingSet)
                .CallWith(_conMeterReadingClient.Proxy.SaveMeterReading);
            Assert.AreEqual(ProcessStatus.ReadyToSend,
                GetMeterReadingStatusFromDb(saveReadingResponse.SavedMeterReadings.First()));
            SendMrToEdsn(saveReadingResponse);

            var mrId = saveReadingResponse.SavedMeterReadings.First().MeterReadingId;
            var externalReference = _consumptionRepository
                .GetEntityByCondition<ConsDomSqlModels.MeterReading>(m => m.Id == mrId,
                    "RegisterReading").ExternalReference;
            Waiter.Wait(() =>
                _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(mr =>
                    mr.Id == mrId));

            FakeEdsnServiceHelpers.ProcessRejectedReadingByEdsn(externalReference, status);
            Waiter.Wait(() =>
                _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                    mr => mr.Id == mrId && mr.StatusId == (int) ProcessStatus.RejectedByEdsn));

            var getMeterReadingByIdResponse = new GetMeterReadingByIdRequest
            {
                MeterReadingId = _consumptionRepository.GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(
                        m => m.MarketEventDate, m => m.StatusId == (int) ProcessStatus.RejectedByEdsn,
                        "RegisterReading")
                    .Id
            }.CallWith(_conMeterReadingClient.Proxy.GetMeterReadingById);
            Assert.AreEqual(ProcessStatus.RejectedByEdsn, getMeterReadingByIdResponse.MeterReading.Status);

            return getMeterReadingByIdResponse.MeterReading;
        }

        public static int AcceptOtherPartyMeterReading(MeterReading meterReading, MarketEvent me)
        {
            var mrDom = meterReading.To();
            var marketEventId = (int)me;
            if (me == MarketEvent.Yearly || me == MarketEvent.TwoMonthly)
            {
                marketEventId = (int)MarketEvent.Spontaneous;
            }
            
            ProcessReadingInFakeEdsn(mrDom, MeterReadingStatus.Accepted);
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() => _consumptionRepository.GetEntityByCondition<ConsDomSqlModels.MeterReading>(
                           mr => mr.EanId == mrDom.Ean && mr.MarketEventId ==
                               marketEventId), 200);

            return _consumptionRepository.GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(
                mr => mr.MarketEventDate, mr => mr.EanId == mrDom.Ean &&
                                                mr.StatusId == (int) ProcessStatus.Accepted).Id;
        }

        public static MeterReading ApproveConsDomMeterReading(MeterReading reading, bool skipValidation = false)
        {
            var readingsSet = GeneralHelpers.GetReadingSetFromReading(reading);
            var saveReadingResponse = SaveMeterReadingRequest(readingsSet, skipValidation)
                .CallWith(_conMeterReadingClient.Proxy.SaveMeterReading);
            Assert.AreEqual(ProcessStatus.ReadyToSend,
                GetMeterReadingStatusFromDb(saveReadingResponse.SavedMeterReadings.First()));

            _nutsScheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            var mrId = saveReadingResponse.SavedMeterReadings.First().MeterReadingId;
            var meterReading =
                _consumptionRepository.GetEntityByCondition<ConsDomSqlModels.MeterReading>(m => m.Id == mrId,
                    "RegisterReading");
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == meterReading.Id && mr.StatusId == (int) ProcessStatus.Approved));

            var response = new GetMeterReadingByIdRequest {MeterReadingId = meterReading.Id}
                .CallWith(_conMeterReadingClient.Proxy.GetMeterReadingById);
            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);

            return response.MeterReading;
        }

        public static void CreateEndOfMeterSupportReading(EanInfo ean, DateTime date, int meterNumber)
        {
            var volume = FakeEdsnServiceHelpers.GetMeterReadingVolumesRequest(ean, date, meterNumber.ToString());
            volume.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.MTRUPD;
            _fakeMeterReadingService.MeterReadingExchangeNotificationAsync(volume);

            var extRef = volume.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            _fakeEdsnAdminClient.ProcessMeterReadingByExternalReference(extRef, MeterReadingStatus.Accepted);
            RestSchedulerClient.Instance.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.EanId == ean.EanId && mr.MarketEventId == (int)MarketEvent.EndOfMeterSupport));
        }

        public static void CreateBeginOfMeterSupportReading(EanInfo ean, DateTime date, int newMeterId,
            bool isChangeValue = false)
        {
            var beginOfMeterSupportReading = MeteringPointsServiceHelpers
                .GetMeterReading(ean, date, MarketEvent.BeginOfMeterSupport, ReadingSource.Customer_EDSN);
            beginOfMeterSupportReading.EdsnMeterId = newMeterId.ToString();
            if (isChangeValue)
            {
                beginOfMeterSupportReading.RegisterReadings.ForEach(rr => rr.Value = 120);
            }

            AcceptOtherPartyMeterReading(beginOfMeterSupportReading, MarketEvent.BeginOfMeterSupport);
        }

        private static void SetReadingStatusToAccepted(int readingId, string externalReference)
        {
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == readingId && mr.StatusId == (int)ProcessStatus.SentToEdsn), 180);
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(externalReference);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == readingId && mr.StatusId == (int)ProcessStatus.Accepted));
        }

        private static void SendMrToEdsn(SaveMeterReadingResponse saveReadingResponse)
        {
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            var id = saveReadingResponse.SavedMeterReadings.First().MeterReadingId;
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == id && mr.StatusId == (int) ProcessStatus.SentToEdsn));
        }

        private static ProcessStatus GetMeterReadingStatusFromDb(SavedMeterReadingInfo savedMeterReading)
        {
            var readingId = savedMeterReading.MeterReadingId;
            var acceptedMeterReading = _consumptionRepository
                .GetEntityByCondition<ConsDomSqlModels.MeterReading>(
                    m => m.Id == readingId, "RegisterReading");
            return (ProcessStatus) acceptedMeterReading.StatusId;
        }
    }
}