﻿using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.TestDataProviders;
using FakeMeterReadingService;
using Nuts.Consumption.Model.Contract;
using Nuts.FakeEdsn.Contracts;
using Nuts.FakeEdsn.Contracts.Enums;
using Nuts.InterDom.Model.Core.Enums;
using Repositories.Consumption.Sql.ConsumptionModels;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class FakeEdsnServiceHelpers
    {
        private static readonly RestSchedulerClient _nutsScheduler = RestSchedulerClient.Instance;
        private static readonly FakeEdsnAdminClient _fakeEdsnAdminClient = new FakeEdsnAdminClient();

        private static readonly DbHandler<ConsumptionContext> _consumptionRepository =
            new DbHandler<ConsumptionContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));

        public static void ProcessDisputeResultsByEdsn(string externalReference, MeterReadingDisputeStatus status)
        {
            _fakeEdsnAdminClient.ProcessDisputeByExternalReference(externalReference, status);
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetDisputeResultsFake);
        }

        public static void ProcessDisputeByEdsn(string externalReference, MeterReadingDisputeStatus status)
        {
            _fakeEdsnAdminClient.ProcessDisputeByExternalReference(externalReference, status);
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetDisputesFake);
        }

        public static void ProcessAcceptedReadingByEdsn(string externalReference)
        {
            _fakeEdsnAdminClient.ProcessMeterReadingByExternalReference(externalReference, MeterReadingStatus.Accepted);
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
        }

        public static void ProcessRejectedReadingByEdsn(string externalReference, MeterReadingStatus status)
        {
            _fakeEdsnAdminClient.ProcessMeterReadingByExternalReference(externalReference, status);
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetMeterReadingRejectionsFake);
        }

        public static MeterReadingExchangeNotificationEnvelope GetMeterReadingVolumesRequest(EanInfo ean,
            DateTime mpDate, string meterId = null)
        {
            var mp = GeneralHelpers.GetMeteringPointsByDate(ean.EanId, mpDate);
            meterId = meterId == null ? ean.EnergyMeterId : meterId;

            var test = new MeterReadingExchangeNotificationEnvelope()
            {
                Portaal_Content = new[]
                {
                    new MeterReadingExchangeNotificationEnvelope_PC_PMP
                    {
                        EANID = ean.EanId,
                        Portaal_Mutation = InitializeMeterReadingExchangeNotificationEnvelope(ean),
                        ProductType = (MeterReadingExchangeNotificationEnvelope_EnergyProductPortaalTypeCode) Enum
                            .Parse(typeof(MeterReadingExchangeNotificationEnvelope_EnergyProductPortaalTypeCode),
                                ean.ProductType.ToString()),
                        Items = new object[]
                        {
                            InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeterForVolume(ean, mpDate, mp, meterId)
                        }
                    }
                }
            };
            return test;
        }

        public static MeterReadingExchangeNotificationEnvelope GetMeterReadingRequest(EanInfo ean, DateTime mpDate,
            MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode meteringMethod, string meterId = "0")
        {
            var mp = GeneralHelpers.GetMeteringPointsByDate(ean.EanId, mpDate);
            meterId = meterId == "0" ? ean.EnergyMeterId : meterId;

            return new MeterReadingExchangeNotificationEnvelope()
            {
                Portaal_Content = new[]
                {
                    new MeterReadingExchangeNotificationEnvelope_PC_PMP
                    {
                        EANID = ean.EanId,
                        Portaal_Mutation = InitializeMeterReadingExchangeNotificationEnvelope(ean),
                        ProductType = (MeterReadingExchangeNotificationEnvelope_EnergyProductPortaalTypeCode) Enum
                            .Parse(typeof(MeterReadingExchangeNotificationEnvelope_EnergyProductPortaalTypeCode),
                                ean.ProductType.ToString()),
                        Items = new object[]
                        {
                            InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeter(ean, mpDate, mp, meteringMethod, meterId)
                        }
                    }
                }
            };
        }

        public static MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register[]
            GenerateListOfVolumeRegisters(Nuts.MeteringPoint.Contract.MeteringPoint mp, EanInfo ean, DateTime marketEventDate)
        {
            var meterReading = GeneralHelpers
                .GetReadingSetFromSqlMeterReading(
                    _consumptionRepository.GetLastEntity<MeterReading, DateTime>(
                        m => m.CreatedOn, m => m.EanId == ean.EanId, "RegisterReading"));
            var registers = new List<MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register>();
            foreach (var re in mp.EnergyMeter.Registers)
            {
                registers.Add(
                    new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register
                    {
                        NrOfDigits = re.NrOfDigits.ToString(),
                        MeteringDirectionSpecified = true,
                        TariffTypeSpecified = true,
                        Item = InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeterRegisterVolume(
                            marketEventDate, meterReading, re),
                        TariffType = (MeterReadingExchangeNotificationEnvelope_EnergyTariffTypeCode)Enum
                            .Parse(typeof(MeterReadingExchangeNotificationEnvelope_EnergyTariffTypeCode),
                                re.TariffType),
                        MeteringDirection = (MeterReadingExchangeNotificationEnvelope_EnergyFlowDirectionCode)Enum
                            .Parse(typeof(MeterReadingExchangeNotificationEnvelope_EnergyFlowDirectionCode),
                                re.MeteringDirection)

                    });
            }

            return registers.ToArray();
        }

        public static MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register[]
            GenerateListOfRegisters(Nuts.MeteringPoint.Contract.MeteringPoint mp, EanInfo ean, DateTime marketEventDate,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode meteringMethod)
        {
            var meterReading = GeneralHelpers
                .GetReadingSetFromSqlMeterReading(
                    _consumptionRepository.GetLastEntity<MeterReading, DateTime>(
                        m => m.MarketEventDate, m => m.EanId == ean.EanId, "RegisterReading"));
            var registers = new List<MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register>();
            foreach (var re in mp.EnergyMeter.Registers)
            {
                registers.Add(
                    new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register
                    {
                        NrOfDigits = re.NrOfDigits.ToString(),
                        MeteringDirectionSpecified = true,
                        TariffTypeSpecified = true,
                        Item = InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeterRegister(marketEventDate, meterReading, re, meteringMethod),
                        TariffType = (MeterReadingExchangeNotificationEnvelope_EnergyTariffTypeCode)Enum
                            .Parse(typeof(MeterReadingExchangeNotificationEnvelope_EnergyTariffTypeCode),
                                re.TariffType),
                        MeteringDirection = (MeterReadingExchangeNotificationEnvelope_EnergyFlowDirectionCode)Enum
                            .Parse(typeof(MeterReadingExchangeNotificationEnvelope_EnergyFlowDirectionCode),
                                re.MeteringDirection)

                    });
            }

            return registers.ToArray();
        }

        private static MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register_Volume
            InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeterRegisterVolume(
                DateTime marketEventDate, ReadingsSet meterReading, Nuts.MeteringPoint.Contract.Register re)
        {
            return new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register_Volume
            {
                Reading = new[]
                {
                    new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register_Volume_Reading
                    {
                        Reading = (meterReading.Readings
                            .First(x => x.MeteringDirection.ToString() == re.MeteringDirection && x.TariffType.ToString() == re.TariffType)
                            .Value).ToString(),
                        ReadingDate = marketEventDate,
                        ReadingMethod = MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item22
                    },
                    new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register_Volume_Reading
                    {
                        Reading = (meterReading.Readings
                            .First(x => x.MeteringDirection.ToString() == re.MeteringDirection && x.TariffType.ToString() == re.TariffType)
                            .Value + 100).ToString(),
                        ReadingDate = marketEventDate,
                        ReadingMethod = MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item22
                    }
                },
                Volume = "100"
            };
        }

        private static MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register_Reading
            InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeterRegister(
                DateTime marketEventDate, ReadingsSet meterReading, Nuts.MeteringPoint.Contract.Register re, MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode meteringMethod)
        {
            return new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter_Register_Reading
            {
                Reading = (meterReading.Readings
                    .First(x => x.MeteringDirection.ToString() == re.MeteringDirection && 
                                x.TariffType.ToString() == re.TariffType)
                    .Value).ToString(),
                ReadingDate = marketEventDate,
                ReadingMethod = meteringMethod
            };
        }


        private static MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter
            InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeterForVolume(EanInfo ean, DateTime mpDate,
                Nuts.MeteringPoint.Contract.MeteringPoint mp, string meterId)
        {
            return new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter
            {
                ID = meterId,
                NrOfRegisters = mp.EnergyMeter.NrOfRegisters.ToString(),
                Register = GenerateListOfVolumeRegisters(mp, ean, mpDate)
            };
        }

        private static MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter
            InitializeMeterReadingExchangeNotificationEnvelopePcPmpPortaalEnergyMeter(EanInfo ean, DateTime mpDate,
                Nuts.MeteringPoint.Contract.MeteringPoint mp, MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode meteringMethod, string meterId)
        {
            return new MeterReadingExchangeNotificationEnvelope_PC_PMP_Portaal_EnergyMeter
            {
                ID = meterId.ToString(),
                NrOfRegisters = mp.EnergyMeter.NrOfRegisters.ToString(),
                Register = GenerateListOfRegisters(mp, ean, mpDate, meteringMethod)
            };
        }

        private static MeterReadingExchangeNotificationEnvelope_PC_PMP_PM
            InitializeMeterReadingExchangeNotificationEnvelope(EanInfo ean)
        {
            return new MeterReadingExchangeNotificationEnvelope_PC_PMP_PM
            {
                Dossier = new MeterReadingExchangeNotificationEnvelope_PC_PMP_PM_Dossier {ID = ean.DossierId},
                ExternalReference = $"ExternalReference for ean {ean.EanId} and dossier {ean.DossierId}",
                MutationReason = (MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode) Enum
                    .Parse(typeof(MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode),
                        MutationReasonCode.PERMTR.ToString()),
                Consumer = "8714252018141",
                Initiator = "8408486416047"
            };
        }
    }
}