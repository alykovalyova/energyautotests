﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Consumption.Base;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using NUnit.Framework;
using Nuts.Consumption.Model.Contract;
using Nuts.Consumption.Model.Core;
using Nuts.EdsnGateway.Contract.MeterReadings.Headers;
using Nuts.FakeEdsn.Contracts;
using Nuts.FakeEdsn.Contracts.Enums;
using Nuts.ICAR.Contract.MeteringPointService;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.MeteringPoint.Contract;
using ConsDomSqlModels = Repositories.Consumption.Sql.ConsumptionModels;
using EdsnIMeterReadingService = Nuts.EdsnGateway.Contract.MeterReadings.IMeterReadingService;
using MeterReading = Nuts.EdsnGateway.Contract.MeterReadings.MeterReadings.MeterReading;
using Mutation = Nuts.Consumption.Model.Contract.Mutation;
using RegisterReading = Nuts.Consumption.Model.Contract.RegisterReading;
using StatusHistory = Repositories.Consumption.Sql.ConsumptionModels.StatusHistory;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class GeneralHelpers
    {
        private static readonly ConfigHandler _config = ConfigHandler.Instance;
        private static readonly ServiceClient<EdsnIMeterReadingService> _egvMeterReadingClient;
        private static readonly ServiceClient<IMeteringPointService> _meteringPointClient;
        private static readonly ServiceClient<IMeterReadingService> _conMeterReadingClient;
        private static readonly HttpClient MpHttpClient = new BaseHttpClient(_config.ApiUrls.Mp);
        private static readonly NutsHttpClient _mpClient = new NutsHttpClient(MpHttpClient);
        private static readonly RestSchedulerClient _nutsScheduler = RestSchedulerClient.Instance;
        private static readonly DbHandler<ConsDomSqlModels.ConsumptionContext> _consumptionRepository;
        private static readonly decimal[] SunFractions = { 0m, 0.02m, 0.03m, 0.07m, 0.115m, 0.145m, 0.16m, 0.16m, 0.13m, 0.08m, 0.045m, 0.025m, 0.02m };

        static GeneralHelpers()
        {
            _egvMeterReadingClient = new ServiceClient<EdsnIMeterReadingService>(_config.ServicesEndpoints
                .EgwMeterReadingService);
            _meteringPointClient = new ServiceClient<IMeteringPointService>(_config.ServicesEndpoints
                .IcarMeteringPointService);
            _conMeterReadingClient = new ServiceClient<IMeterReadingService>(_config.ServicesEndpoints
                .CsdMeterReadingService);
            _consumptionRepository =
                new DbHandler<ConsDomSqlModels.ConsumptionContext>(
                    _config.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));
        }

        public static decimal GetFactor(ConsDomSqlModels.MeterReading meterReading, MeteringPoint mp)
        {
            var factor = mp.EnergyMeter.Registers.First(rr => meterReading.RegisterReadings.First().TariffType.Name
                != null && rr.TariffType == meterReading.RegisterReadings.First().TariffType.Name).MultiplicationFactor;
            return factor ?? 1;
        }
    
        public static void SendDisputeFromOtherParty(string ean, MeterReading meterReading, DateTime date,
            ConsDomSqlModels.MeterReading meterReadingInfo)
        {
            var response = new SendMeterReadingDisputeRequest
            {
                DisputeMeterReading = new MeterReading
                {
                    Ean = ean,
                    EnergyMeter = meterReading.EnergyMeter,
                    HeaderCreationTS = date,

                    Mutation = new Nuts.EdsnGateway.Contract.MeterReadings.MeterReadings.Mutation
                    {
                        Consumer = meterReadingInfo.Consumer,
                        DossierId = _consumptionRepository
                            .GetLastEntity< ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn,
                                m => m.EanId == ean, "RegisterReading").DossierId,
                        ExternalReference = meterReadingInfo.ExternalReference,
                        Initiator = meterReadingInfo.Initiator,
                        MutationReason = MutationReasonCode.DISPUTE
                    },
                    ProductType = meterReading.ProductType,
                    HeaderMessageId = new Guid()
                }
            }.CallWith(_egvMeterReadingClient.Proxy.SendMeterReadingDispute);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new Exception(response.Header.Message);
        }

        public static MeteringPoint GetMeteringPointsByDate(string ean, DateTime date)
        {
            var response = new GetMeteringPointRequest(){Ean = ean }.CallWith<GetMeteringPointRequest, GetMeteringPointResponse>(_mpClient);

            return response.Data.MeteringPoint;
        }

        public static void SetUpEstimationResult(EstimationResult estimationResult)
        {
            if (estimationResult.EstimationRegisters.Count >= 2)
            {
                estimationResult.EstimationRegisters.RemoveAt(0);
            }
            else if (estimationResult.EstimationRegisters.Count == 1)
            {
                var rr = new EstimationRegister
                {
                    MeasureUnit = MeasureUnitCode.MTQ,
                    MeteringDirection = EnergyFlowDirectionCode.LVR,
                    NrOfDigits = 5,
                    RegisterNo = "1",
                    TariffType = EnergyTariffTypeCode.L,
                    Estimation = new EstimationInfo {Estimation = 100, EstimationMax = 150, EstimationMin = 90},
                    RegisterNumber = "2",
                    ValidationMessage = "test"
                };
                estimationResult.EstimationRegisters.Add(rr);
            }
        }

        public static void SetUpElkAndGasReadingsSets(string gasEan, EanInfo elkEan, ReadingSource rs, MarketEvent me, int processId,
            out ReadingsSet readingsSetGasMeter, out ReadingsSet readingsSetElkMeter)
        {
            //creation of readings sets for Gas address and for Elk address
            readingsSetGasMeter = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(
                gasEan, me, rs, BaseConsDomTest.OpenedWindow);
            readingsSetElkMeter = ConsumptionServiceHelpers.CreateReadingSet(
                elkEan, me, rs, BaseConsDomTest.OpenedWindow);

            //assign to elk reading set the same dossierId as gas readings set has
            readingsSetGasMeter.DossierId = readingsSetElkMeter.DossierId;

            //assigning the same processId for both address 
            readingsSetElkMeter.ProcessId = processId;
            readingsSetGasMeter.ProcessId = processId;

            //send of readings sets
            var saveReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetElkMeter);
            saveReadingRequest.MeterReadingsSet.Add(readingsSetGasMeter);
            saveReadingRequest.CallWith(_conMeterReadingClient.Proxy.SaveMeterReading);
        }

        public static void WaitForSentToEdsnStatus(int newReadingId, bool assertion = false)
        {
            _nutsScheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendDisputeMeterReadingJob);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.SentToEdsn), 200);

            if (assertion)
            {
                Assert.AreEqual(
                    BaseConsDomTest.ConsDomResponsible,
                    _consumptionRepository
                        .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == newReadingId)
                        .User);
            }
        }

        public static void AcceptDisputeByFakeEdsn(string newReadingExternalReference, int disputedReadingId,
            bool assertion = false)
        {
            FakeEdsnServiceHelpers.ProcessDisputeResultsByEdsn(newReadingExternalReference,
                MeterReadingDisputeStatus.Accepted);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int) ProcessStatus.DisputeAccepted));

            if (assertion)
            {
                Assert.AreEqual(
                    BaseConsDomTest.EdsnServiceResponsible,
                    _consumptionRepository
                        .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn,
                            s => s.MeterReadingId == disputedReadingId).User);
            }
        }

        public static void RejectDisputeByFakeEdsn(string newReadingExternalReference, int newReadingId)
        {
            FakeEdsnServiceHelpers.ProcessDisputeResultsByEdsn(newReadingExternalReference,
                MeterReadingDisputeStatus.Rejected);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.DisputeRejected), 200);
        }

        public static void SendDisputeFromOtherPartyAndWaitStatus(EanInfo eanInfo, MeterReading readingInfo,
            DateTime date,
            ConsDomSqlModels.MeterReading disputedReadingInfo)
        {
            SendDisputeFromOtherParty(eanInfo.EanId, readingInfo, date, disputedReadingInfo);
            FakeEdsnServiceHelpers.ProcessDisputeByEdsn(disputedReadingInfo.ExternalReference,
                MeterReadingDisputeStatus.SaveAsOtherPartyDispute);

            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == disputedReadingInfo.Id && mr.StatusId == (int) ProcessStatus.Disputed), 600);
        }

        public static void SendDisputeFromOtherPartyAndAssertStatus(EanInfo eanInfo, MeterReading readingInfo,
            DateTime date,
            ConsDomSqlModels.MeterReading disputedReadingInfo)
        {
            SendDisputeFromOtherParty(eanInfo.EanId, readingInfo, date, disputedReadingInfo);
            FakeEdsnServiceHelpers.ProcessDisputeByEdsn(disputedReadingInfo.ExternalReference,
                MeterReadingDisputeStatus.SaveAsOtherPartyDispute);

            Assert.AreEqual((int) ProcessStatus.Accepted, disputedReadingInfo.StatusId);
        }

        public static void AnswerDisputeByBudgetAndValidateRejectedStatus(
            ConsDomSqlModels.MeterReading newReading,
            ConsDomSqlModels.MeterReading disputedReadingInfo)
        {
            ProcessDisputeByBudget(newReading.Id, true);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == disputedReadingInfo.Id && mr.StatusId == (int) ProcessStatus.Rejected));
        }

        public static void AnswerDisputeByBudgetAndValidateAcceptedStatus(
            ConsDomSqlModels.MeterReading newReading,
            ConsDomSqlModels.MeterReading disputedReadingInfo)
        {
            ProcessDisputeByBudget(newReading.Id, false);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == disputedReadingInfo.Id && mr.StatusId == (int) ProcessStatus.Accepted), 300);
        }

        public static void StartOfSettlementAsOtherParty(Nuts.Consumption.Model.Contract.MeterReading meterReading,
            int settledReadingId, EanInfo eanInfo)
        {
            ConsumptionServiceHelpers.StartSettlementByOtherParty(meterReading, settledReadingId);

            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Settled));

            var newReadingId = _consumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn,
                    m => m.EanId == eanInfo.EanId, "RegisterReading").Id;
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.CheckSettlement));
        }

        public static void ValidateInSettlementAndWaitForSettlementStatuses(int settledReadingId, EanInfo eanInfo)
        {
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.InSettlement));

            var newReadingId = _consumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn,
                    m => m.EanId == eanInfo.EanId, "RegisterReading").Id;
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.WaitForSettlement));
        }

        public static int ValidateWaitForDisputeStatus(EanInfo eanInfo)
        {
            var newReadingId = _consumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn,
                    m => m.EanId == eanInfo.EanId, "RegisterReading").Id;
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.WaitForDispute));

            return newReadingId;
        }

        public static GetHistoricalMeterReadingByEanResponse GetHistoricalReadingsByEan(EanInfo ean)
        {
            var response = new GetHistoricalMeterReadingByEanRequest {Eans = new List<string> {ean.EanId}}
                .CallWith(_conMeterReadingClient.Proxy.GetHistoricalMeterReadingsByEan);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new Exception(response.Header.Message);

            return response;
        }

        public static void AcceptMeterReadingForEan(EanInfo ean, MarketEvent me, ReadingSource rs, DateTime date)
        {
            var estimate = ConsumptionServiceHelpers.EstimateReadings(ean.EanId, me, date);
            GetDomainMeterReadingFromEstimationResult(estimate.EstimationData.FirstOrDefault(), null, rs, me, date);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(ean, date, me, rs);
        }

        public static void ApproveMeterReadingForEan(EanInfo ean, MarketEvent me, ReadingSource rs)
        {
            var estimate = ConsumptionServiceHelpers.EstimateReadings(ean.EanId, me, BaseConsDomTest.OpenedWindow);
            var meterReading = GetDomainMeterReadingFromEstimationResult(estimate.EstimationData.FirstOrDefault(), null,
                rs, me, BaseConsDomTest.OpenedWindow.AddDays(-5));

            ConsumptionServiceHelpers.ApproveConsDomMeterReading(meterReading, true);
        }

        public static void CreateReadingWithNewMeterForEan(EanInfo ean, DateTime startDate, MarketEvent me,
            ReadingSource rs, string meterId)
        {
            var startMeterReading = MeteringPointsServiceHelpers.GetMeterReading(ean, startDate, me, rs);
            startMeterReading.EdsnMeterId = meterId;
            var firstMeterPoint = startMeterReading.To();
            var mrExternalReference =
                ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(firstMeterPoint, MeterReadingStatus.Accepted);
            RestSchedulerClient.Instance.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() =>
                _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                    mr => mr.EanId == ean.EanId && mr.StatusId == (int)ProcessStatus.Accepted));
        }

        public static decimal GetSummOfFractions(EanInfo ean, DateTime startDate, DateTime endDate, string directionCode)
        {
            var sum = 0m;
            var profileCodeId =
                _consumptionRepository.GetEntityByCondition<ConsDomSqlModels.DictionaryEnergyUsageProfileCode>(f =>
                    f.Name == ean.ProfileCategory.ToString()).Id;
            if (directionCode == "LVR")
            {
                var fractions = _consumptionRepository.GetEntitiesByCondition<ConsDomSqlModels.EdsnFraction>(f =>
                    f.ProfileCodeId == profileCodeId && f.FractionDate >= startDate && f.FractionDate <= endDate).ToList();
                sum = fractions.Sum(x => x.FractionValue);
            }
           
            if (directionCode == "TLV")
            {
                var result = SunFractions[0];
                while (startDate <= endDate)
                {
                    var firstOfNextMonth = new DateTime(startDate.Year, startDate.Month, 1).AddMonths(1);

                    // startDate might not be first of month, in which case days must be skipped
                    var daysToSkip = startDate.Day == 1 ? (int?)null : startDate.Day - 1;
                    // endDate might not be first of month, in which case only part of the month should be included 
                    var daysToInclude = (firstOfNextMonth > endDate) ? endDate.Day - 1 : (int?)null;

                    if (daysToSkip.HasValue || daysToInclude.HasValue)
                    {
                        // when not started or ended on first of a month:
                        // add sunfraction for fraction of month
                        var totalDaysInMonth = firstOfNextMonth.AddDays(-1).Day;
                        var daysInMonth = daysToInclude.GetValueOrDefault(totalDaysInMonth) - daysToSkip.GetValueOrDefault();
                        result += SunFractions[startDate.Month] * daysInMonth / totalDaysInMonth;
                    }
                    else
                    {
                        // when complete month:
                        // add sunfraction for month
                        result += SunFractions[startDate.Month];
                    }

                    startDate = firstOfNextMonth;
                }
                sum = Decimal.Round(result, 6, MidpointRounding.AwayFromZero);
            }

            return sum;
        }

        public static GetYearlyUsageByMeterReadingResponse GetYearlyUsageByMeterReadingResponse(ReadingsSetInfo readingSetOfPreviousReading)
        {
            var response = new GetYearlyUsageByMeterReadingRequest
            {
                ReadingsSetInfo = readingSetOfPreviousReading
            }.CallWith(_conMeterReadingClient.Proxy.GetYearlyUsageByMeterReading);

            return response;
        }

        public static ReadingsSet GetReadingSetFromReading(Nuts.Consumption.Model.Contract.MeterReading meterReading)
        {
            var set = new ReadingsSet
            {
                EanId = meterReading.EanId,
                ClientReadingDate = meterReading.ClientReadingDate,
                ReadingSource = meterReading.ReadingSource,
                DossierId = meterReading.Mutation.DossierId,
                MarketEventDate = meterReading.Mutation.MarketEventDate,
                MarketEvent = meterReading.Mutation.MarketEvent,
                Readings = meterReading.RegisterReadings.Select(r => new Reading
                {
                    MeteringDirection = r.MeteringDirection.GetValueOrDefault(EnergyFlowDirectionCode.TLV),
                    TariffType = r.TariffType.GetValueOrDefault(EnergyTariffTypeCode.N),
                    Value = r.Value
                }).ToList()
            };

            return set;
        }

        public static ReadingsSetInfo GetReadingSetInfoFromReading(
            Nuts.Consumption.Model.Contract.MeterReading meterReading)
        {
            var set = new ReadingsSetInfo
            {
                EanId = meterReading.EanId,
                MarketEventDate = meterReading.Mutation.MarketEventDate,
                MarketEvent = meterReading.Mutation.MarketEvent,
                Readings = meterReading.RegisterReadings.Select(r => new Reading
                {
                    MeteringDirection = r.MeteringDirection.GetValueOrDefault(EnergyFlowDirectionCode.TLV),
                    TariffType = r.TariffType.GetValueOrDefault(EnergyTariffTypeCode.N),
                    Value = r.Value
                }).ToList()
            };

            return set;
        }

        public static ReadingsSet GetReadingSetFromSqlMeterReading(
            ConsDomSqlModels.MeterReading meterReading)
        {
            var set = new ReadingsSet
            {
                EanId = meterReading.EanId,
                ClientReadingDate = meterReading.ClientReadingDate,
                ReadingSource = (ReadingSource) meterReading.ReadingSourceId,
                DossierId = meterReading.DossierId,
                MarketEventDate = meterReading.MarketEventDate,
                MarketEvent = (MarketEvent) meterReading.MarketEventId,
                Readings = meterReading.RegisterReadings.Select(r => new Reading
                {
                    MeteringDirection = (EnergyFlowDirectionCode) r.MeteringDirectionId.GetValueOrDefault(),
                    TariffType = (EnergyTariffTypeCode) r.TariffTypeId.GetValueOrDefault(),
                    Value = r.Value
                }).ToList()
            };

            return set;
        }

        public static Nuts.Consumption.Model.Contract.MeterReading GetDomainMeterReadingFromEstimationResult(
            EstimationResult estimationResult, string dossierId,
            ReadingSource rs, MarketEvent? marketEvent, DateTime? readingDate)
        {
            var newDomainMeterReading = new Nuts.Consumption.Model.Contract.MeterReading
            {
                EanId = estimationResult.EanId,
                EdsnMeterId = estimationResult.MeterNumber,
                ReadingSource = rs,
                Status = ProcessStatus.New,
                EnergyProductType = estimationResult.EnergyProductType,
                NrOfRegisters = estimationResult.EstimationRegisters.Count,
                Mutation = new Mutation
                {
                    Consumer = "8714252018141",
                    DossierId = string.IsNullOrEmpty(dossierId)
                        ? new Random().Next(100000000, 999999999).ToString()
                        : dossierId,
                    Initiator = new Random().Next(100000000, 999999999).ToString(),
                    MarketEvent = marketEvent.GetValueOrDefault(MarketEvent.MoveIn),
                    MarketEventDate = readingDate.GetValueOrDefault(DateTime.Now.Date)
                },

                RegisterReadings = estimationResult.EstimationRegisters.Select(GetDomainRegisterReadingFromEstimation)
                    .ToList()
            };

            return newDomainMeterReading;
        }

        private static RegisterReading GetDomainRegisterReadingFromEstimation(EstimationRegister estimation)
        {
            var registerReading = new RegisterReading
            {
                Value = estimation.Estimation.Estimation,
                MeasureUnit = estimation.MeasureUnit,
                MeteringDirection = estimation.MeteringDirection,
                NrOfDigits = estimation.NrOfDigits,
                TariffType = estimation.TariffType
            };

            return registerReading;
        }

        public static void CreateReadingForEan(EanInfo ean, DateTime startDate, MarketEvent me, ReadingSource rs)
        {
            var startMeterReading = MeteringPointsServiceHelpers.GetMeterReading(ean, startDate, me, rs);
            var firstMeterPoint = startMeterReading.To();
            var extReference =
                ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(firstMeterPoint, MeterReadingStatus.Accepted);
            RestSchedulerClient.Instance.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() =>
                _consumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                    mr => mr.EanId == ean.EanId && mr.StatusId == (int) ProcessStatus.Accepted));
        }

        public static void CreateInitialReadingsInTmr(List<EanInfo> eans)
        {
            foreach (var eanInfo in eans)
            {
                var dateSecond = DateTime.Now.AddDays(-1060);
                var dateFirstReading = dateSecond.AddDays(-295);
                var dateThirdReading = DateTime.Now.AddDays(-300);
                var dateFourthReading = DateTime.Now.AddDays(-45);
                var meteringPoint = GetMeteringPointsByDate(eanInfo.EanId, dateSecond);

                if (meteringPoint.EnergyMeter.NrOfRegisters > 2)
                {
                    var firstReading = GetDomainMeterReadingFromMeterPoint(meteringPoint,
                        eanInfo.DossierId, MarketEvent.Switch, dateFirstReading, ReadingSource.Customer_Backoffice);
                    var secondReading = GetDomainMeterReadingFromMeterPoint(meteringPoint,
                        eanInfo.DossierId, MarketEvent.Switch, dateSecond, ReadingSource.Customer_Backoffice);

                    foreach (var reading in secondReading.RegisterReadings)
                    {
                        var re = firstReading.RegisterReadings.Find(r =>
                            r.MeteringDirection == reading.MeteringDirection && r.TariffType == reading.TariffType);
                        reading.Value = re.Value + 1000;
                    }

                    var thirdReading = GetDomainMeterReadingFromMeterPoint(meteringPoint,
                        eanInfo.DossierId, MarketEvent.Switch, dateThirdReading, ReadingSource.Customer_Backoffice);

                    foreach (var reading3 in thirdReading.RegisterReadings)
                    {
                        var reading1 =
                            secondReading.RegisterReadings.Find(
                                r => r.MeteringDirection == reading3.MeteringDirection &&
                                     r.TariffType == reading3.TariffType);
                        reading3.Value = reading1.Value + 1000;
                    }

                    var fourthReading = GetDomainMeterReadingFromMeterPoint(meteringPoint,
                        eanInfo.DossierId, MarketEvent.Spontaneous, dateFourthReading,
                        ReadingSource.Customer_Backoffice);

                    foreach (var reading4 in fourthReading.RegisterReadings)
                    {
                        var reading1 =
                            thirdReading.RegisterReadings.Find(
                                r => r.MeteringDirection == reading4.MeteringDirection &&
                                     r.TariffType == reading4.TariffType);
                        reading4.Value = reading1.Value + 1000;
                    }

                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(fourthReading.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);
                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(thirdReading.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);
                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(secondReading.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);
                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(firstReading.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);
                }
                else if (meteringPoint.EnergyMeter.NrOfRegisters == 2)
                {
                    var meteringPointSecond = GetMeteringPointsByDate(eanInfo.EanId, dateFirstReading);
                    var readingFirst = GetDomainMeterReadingFromMeterPoint(meteringPointSecond,
                        eanInfo.DossierId, MarketEvent.Switch, dateThirdReading, ReadingSource.Customer_Backoffice);

                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(readingFirst.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);

                    var readingSecond = GetDomainMeterReadingFromMeterPoint(meteringPoint,
                        eanInfo.DossierId, MarketEvent.Spontaneous, dateFourthReading,
                        ReadingSource.Customer_Backoffice);
                    readingSecond.Mutation.DossierId = null;

                    foreach (var reading2 in readingSecond.RegisterReadings)
                    {
                        var reading1 =
                            readingFirst.RegisterReadings.Find(
                                r => r.MeteringDirection == reading2.MeteringDirection &&
                                     r.TariffType == reading2.TariffType);
                        reading2.Value = reading1.Value + 1000;
                    }

                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(readingSecond.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);
                }
                else if (meteringPoint.EnergyMeter.NrOfRegisters == 1)
                {
                    var meteringPointFirst = GetMeteringPointsByDate(eanInfo.EanId, dateFirstReading);
                    var readingFirst = GetDomainMeterReadingFromMeterPoint(meteringPointFirst,
                        eanInfo.DossierId, MarketEvent.Switch, dateThirdReading, ReadingSource.Customer_Backoffice);

                    ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(readingFirst.To(),
                        MeterReadingStatus.MoveToTmrSupplierPhase);
                }

                var response = new GetTmrDataRequest {Eans = new List<string> {eanInfo.EanId}}
                    .CallWith(_conMeterReadingClient.Proxy.GetTmrData);

                if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                    throw new Exception(response.Header.Message);
            }
        }

        public static Nuts.Consumption.Model.Contract.MeterReading GetDomainMeterReadingFromMeterPoint(
            MeteringPoint meteringPoint, string dossierId,
            MarketEvent? marketEvent, DateTime? readingDate, ReadingSource rs)
        {
            var newDomainMeterReading = new Nuts.Consumption.Model.Contract.MeterReading
            {
                EanId = meteringPoint.EanId,
                EdsnMeterId = meteringPoint.EnergyMeter.EdsnId,
                ReadingSource = rs,
                Status = ProcessStatus.New,
                EnergyProductType = Enum.Parse<EnergyProductTypeCode>(meteringPoint.ProductType),
                NrOfRegisters = meteringPoint.EnergyMeter.NrOfRegisters,
                Mutation = new Mutation
                {
                    Consumer = "8714252018141",
                    DossierId = String.IsNullOrEmpty(dossierId)
                        ? new Random().Next(100000000, 999999999).ToString()
                        : dossierId,
                    Initiator = new Random().Next(100000000, 999999999).ToString(),
                    MarketEvent = marketEvent.GetValueOrDefault(MarketEvent.MoveIn),
                    MarketEventDate = readingDate.GetValueOrDefault(DateTime.Now)
                },

                RegisterReadings = GetDomainRegisterReading(meteringPoint)
            };

            return newDomainMeterReading;
        }

        private static List<RegisterReading> GetDomainRegisterReading(MeteringPoint mp)
        {
            var mpRegisters = mp.EnergyMeter.Registers;
            return mpRegisters.Select(register => new RegisterReading
                {
                    Value = new Random().Next(0, mp.EnergyMeter.Registers.First().NrOfDigits),
                    RegisterNo = register.EdsnId,
                    MeasureUnit = mp.ProductType == EnergyProductTypeCode.ELK.ToString() ? MeasureUnitCode.KWH : MeasureUnitCode.MTQ,
                    MeteringDirection = Enum.Parse<EnergyFlowDirectionCode>(register.MeteringDirection),
                    NrOfDigits = register.NrOfDigits,
                    TariffType = Enum.Parse<EnergyTariffTypeCode>(register.TariffType)
                })
                .ToList();
        }

        private static void ProcessDisputeByBudget(int meterReadingId, bool isAccepted)
        {
            var response = new AnswerDisputeRequest {IsAccepted = isAccepted, MeterReadingId = meterReadingId}
                .CallWith(_conMeterReadingClient.Proxy.AnswerDispute);

            if (!response.Header.StatusCode.Equals((int) HttpStatusCode.OK))
                throw new Exception(response.Header.Message);
        }

        public static void UpdateReadingById(ProcessStatus newStatus, int readingId)
        {
            var response = new UpdateMeterReadingStatusRequest
            {
                MeterReadingIds = new List<int> {readingId},
                NewStatus = newStatus,
                Reason = new ProcessStatusReason
                    {Reason = StatusReason.InsertedByAgent, User = BaseConsDomTest.UserNameResponsible}
            }.CallWith(_conMeterReadingClient.Proxy.UpdateMeterReadingStatus);

            Assert.False(response.Header.HasException, response.Header.Message);
        }
    }
}