﻿using AutoMapper;
using Nuts.EdsnGateway.Contract.MeterReadings.MeterReadings;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.InterDom.Models.Mappers;
using ConMeterReading = Nuts.Consumption.Model.Contract.MeterReading;
using ConMutation = Nuts.Consumption.Model.Contract.Mutation;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class Mapper
    {
        public static MeterReading To(this ConMeterReading source)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ConMutation, Mutation>()
                .ForMember(dest => dest.MutationReason, 
                    opt => opt.MapFrom(src => CoreMapper.GetReasonCode(src.MarketEvent))));

            if (source == null)
            {
                return null;
            }

            var date = source.Mutation.MarketEventDate;
            var curDate = source.Mutation.MarketEventDate;

            var edsnMeterPoint = new MeterReading
            {
                Ean = source.EanId,
                ProductType = source.EnergyProductType,
                HeaderMessageId = Guid.Empty,
                HeaderCreationTS = new DateTime(date.Year, date.Month, date.Day, curDate.Hour, curDate.Minute, curDate.Second, curDate.Millisecond),
                Mutation = config.CreateMapper().Map<ConMutation, Mutation>(source.Mutation)
            };

            edsnMeterPoint.Mutation.ExternalReference = $"FakeEdsn_{Guid.NewGuid().ToString().Remove(5)}";

            edsnMeterPoint.EnergyMeter = new EnergyMeter
            {
                MeterNumber = source.EdsnMeterId,
                NumberOfRegisters = source.NrOfRegisters
            };

            if (source.RegisterReadings.Count > 0)
            {
                edsnMeterPoint.EnergyMeter.Registers = new List<Register>();

                foreach (var reading in source.RegisterReadings)
                {
                    var reg = new Register
                    {
                        FlowDirection = reading.MeteringDirection,
                        MeasureUnit = reading.MeasureUnit,
                        TariffType = reading.TariffType,
                        NumberOfDigits = reading.NrOfDigits,
                        EndReading = new RegisterReading
                        {
                            Reading = reading.Value,
                            ReadingDate = source.Mutation.MarketEventDate,
                            ReadingSource = source.ReadingSource
                        }
                    };

                    if (source.Mutation.MarketEvent == MarketEvent.EndOfMeterSupport)
                    {
                        reg.BeginReading = new RegisterReading()
                        {
                            Reading = reading.Value,
                            ReadingDate = source.Mutation.MarketEventDate,
                            ReadingSource = source.ReadingSource
                        };

                    }

                    edsnMeterPoint.EnergyMeter.Registers.Add(reg);
                }
            }

            return edsnMeterPoint;
        } 
    }
}