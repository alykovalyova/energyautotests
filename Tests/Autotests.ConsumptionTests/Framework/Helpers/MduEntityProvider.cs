﻿using System.Globalization;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Nuts.InterDom.Model.Core.Enums;
using MasterDataUpdate = Nuts.EdsnGateway.Contract.Queue.Mdu.MasterDataUpdate;
using Address = Nuts.EdsnGateway.Contract.Queue.Mdu.Address;
using BuildDirector = Autotests.Consumption.Framework.Builders.BuildDirector;
using CommercialCharacteristics = Nuts.EdsnGateway.Contract.Queue.Mdu.CommercialCharacteristics;
using EnergyMeter = Nuts.EdsnGateway.Contract.Queue.Mdu.EnergyMeter;
using PhysicalCharacteristics = Nuts.EdsnGateway.Contract.Queue.Mdu.PhysicalCharacteristics;
using Mutations = Nuts.InterDom.Model.Messages.Mutation;

namespace Autotests.Consumption.Framework.Helpers
{
    public class MduEntityProvider
    {
        public static Address GetAddress()
        {
            return BuildDirector.Get.AddressBuilder
                .SetDefaults()
                .SetBuildingNr(62)
                .SetExBuildingNr("B")
                .SetStreetName("Larikstraat")
                .SetZipCode("1111QQ")
                .Build();
        }

        public static Mutations GetMutation(EanInfo eanInfo, DateTime? mutationDate)
        {
            return BuildDirector.Get.MutationBuilder
                .SetDossierId(string.IsNullOrEmpty(eanInfo.DossierId)
                        ? new Random().Next(100000000, 999999999).ToString(CultureInfo.InvariantCulture)
                        : eanInfo.DossierId)
                .SetExternalReference("Mdu_" + Guid.NewGuid().ToString("N"))
                .SetMutationDate(mutationDate.HasValue ? mutationDate.Value : DateTime.Now.AddDays(-1550))
                .SetMutationReason(eanInfo.MutationReason ?? default(MutationReasonCode))
                .Build();
        }

        public static PhysicalCharacteristics GetPhysicalCharacteristics(EanInfo eanInfo, bool tfSignal = false, bool hasProfileCategory = true)
        {
           return BuildDirector.Get.PhysicalCharacteristicsBuilder
                .SetDefaults()
                .SetAllocationMethod(EnergyAllocationMethodCode.SMA)
                .SetEacOffPeak(tfSignal ? 0 : eanInfo.EacOffPeak)
                .SetEacPeak(tfSignal ? eanInfo.EacPeak + eanInfo.EacOffPeak : eanInfo.EacPeak)
                .SetFlowDirection(eanInfo.FlowDirection.GetValueOrDefault())
                .SetProfileCategory(hasProfileCategory ? eanInfo.ProfileCategory : null)
                .Build();
        }

        public static CommercialCharacteristics GetCommercialCharacteristics()
        {
            return BuildDirector.Get.CommercialCharacteristicsBuilder
                .SetDefaults()
                .SetBalanceSupplier(TestConstants.BudgetSupplier)
                .Build();
        }

        public static EnergyMeter GetEnergyMeter(EanInfo ean)
        {
            return BuildDirector.Get.EnergyMeterBuilder
                .SetRegisterNr(ean.RegisterNumber)
                .SetMeterType(ean.EnergyTypeCode)
                .SetEnergyMeterId(ean.EnergyMeterId)
                .SetRegisters(RegisterHelper.CreateRandomEdsnRegisters(ean).ToArray())
                .Build();
        }

        public static MasterDataUpdate GetMasterDataUpdate(EanInfo ean, Address address, 
            CommercialCharacteristics commercialCharacteristics, EnergyMeter energyMeter,
            PhysicalCharacteristics physCharacteristics, Mutations mutation)
        {
            return BuildDirector.Get.MasterDataUpdateBuilder
                .SetDefaults(ean)
                .SetAddress(address)
                .SetEnergyMeter(energyMeter)
                .SetCommercialCharacteristics(commercialCharacteristics)
                .SetPhysicalCharacteristics(physCharacteristics)
                .SetMutation(mutation)
                .Build();
        }
    }
}
