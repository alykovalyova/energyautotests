﻿using System.Xml.Serialization;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Dispatchers;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Repositories.MeteringPoint.Sql.MeteringPointModels;
using MeteringPoint = Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint;

namespace Autotests.Consumption.Framework.Helpers
{
    public class MduProcessor
    {
        private static readonly DbHandler<MeteringPointContext> _meteringPointRepository = new DbHandler<MeteringPointContext>(
            ConfigHandler.Instance.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
        private static readonly Dispatcher _dispatcher = new Dispatcher(DispatcherName.EDSNGateway, ConfigHandler.Instance.QueueSettings.HostNameQueue);

        public static void SendMdu(List<EanInfo> eans, DateTime? mutationDate = null, bool tfSignal = false,
            bool hasProfileCategory = true)
        {
            foreach (var eanInfo in eans)
            {
                
                var mdu = MduEntityProvider.GetMasterDataUpdate(eanInfo, MduEntityProvider.GetAddress(),
                    MduEntityProvider.GetCommercialCharacteristics(), MduEntityProvider.GetEnergyMeter(eanInfo),
                    MduEntityProvider.GetPhysicalCharacteristics(eanInfo, tfSignal, hasProfileCategory), MduEntityProvider.GetMutation(eanInfo, mutationDate));

                _dispatcher.DispatchMessage(mdu);
                Waiter.Wait(() => _meteringPointRepository.EntityIsInDb<MeteringPoint>(mp => mp.EanId == eanInfo.EanId));
            }
        }
    }
}