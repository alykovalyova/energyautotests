﻿using Autotests.Clients;
using Autotests.Core.Handlers;
using Autotests.Core.TestDataProviders;
using Nuts.Consumption.Model.Contract;
using Nuts.ICAR.Contract.MeteringPointService;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class MeteringPointsServiceHelpers
    {
        private static readonly ServiceClient<IMeteringPointService> _client;

        static MeteringPointsServiceHelpers()
        {
            _client = new ServiceClient<IMeteringPointService>(ConfigHandler.Instance.ServicesEndpoints
                .IcarMeteringPointService);
        }

        public static string GetFirstRegisterNumber(string ean, DateTime date)
        {
            using (var meterPointService = new ServiceClient<IMeteringPointService>(ConfigHandler.Instance.ServicesEndpoints.IcarMeteringPointService))
            {
                var request = new GetMeteringPointsByDateRequest
                {
                    Eans = new List<string> { ean },
                    MaxDate = date
                };

                var response = meterPointService.Proxy.GetMeteringPointsByDate(request);
                return response.MeteringPoints.First().EnergyMeter.Registers.ElementAt(0).EdsnId;
            }
        }

        public static MeterReading GetMeterReading(EanInfo ean, DateTime date, MarketEvent me, ReadingSource rs)
        {
            var mp = GeneralHelpers.GetMeteringPointsByDate(ean.EanId, date);
            var meterReading = GeneralHelpers.GetDomainMeterReadingFromMeterPoint(mp, null, me, date, rs);
            return meterReading;
        }

        public static decimal GetMultiplicationFactor(string ean)
        {
            var response = new GetMeteringPointsByDateRequest
            {
                Eans = new List<string> { ean },
                MaxDate = DateTime.Now
            }.CallWith(_client.Proxy.GetMeteringPointsByDate);
            var multiplicationFactor = response.MeteringPoints.First().EnergyMeter.Registers.FirstOrDefault()?.MultiplicationFactor;
            
            return multiplicationFactor.GetValueOrDefault();
        }
    }
}
