﻿using Autotests.Core;
using Autotests.Core.TestDataProviders;
using Nuts.InterDom.Model.Core.Enums;
using Register = Nuts.EdsnGateway.Contract.Queue.Mdu.Register;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class RegisterHelper
    {
        public static Register GetEdsnRegister(string id, EnergyFlowDirectionCode direction,
            EnergyTariffTypeCode tariff)
        {
            return new Register
            {
                EdsnId = id,
                MeteringDirection = direction,
                MultiplicationFactor = 1,
                NrOfDigits = 5,
                TariffType = tariff
            };
        }

        public static List<Register> CreateRandomEdsnRegisters(EanInfo ean)
        {
            var resultList = new List<Register>();
            var registers = new List<GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register>();
            if (ean.AdministrativeStatusSmartMeter)
            {
                for (var i = 0; i < ean.RegisterNumber; i++)
                {
                    switch (i)
                    {
                        case 0:
                            resultList.Add(
                                GetEdsnRegister("1.8.2", EnergyFlowDirectionCode.LVR, EnergyTariffTypeCode.N));
                            continue;
                        case 1:
                            resultList.Add(
                                GetEdsnRegister("1.8.1", EnergyFlowDirectionCode.LVR, EnergyTariffTypeCode.L));
                            continue;
                        case 2:
                            resultList.Add(
                                GetEdsnRegister("2.8.2", EnergyFlowDirectionCode.TLV, EnergyTariffTypeCode.N));
                            continue;
                        case 3:
                            resultList.Add(
                                GetEdsnRegister("2.8.1", EnergyFlowDirectionCode.TLV, EnergyTariffTypeCode.L));
                            continue;
                    }
                }
            }

            else
            {
                for (var i = 0; i < ean.RegisterNumber; i++)
                {
                    switch (i)
                    {
                        case 0:
                            resultList.Add(GetEdsnRegister("1", EnergyFlowDirectionCode.LVR, EnergyTariffTypeCode.N));
                            continue;
                        case 1:
                            resultList.Add(GetEdsnRegister("2", EnergyFlowDirectionCode.LVR, EnergyTariffTypeCode.L));
                            continue;
                        case 2:
                            resultList.Add(GetEdsnRegister("3", EnergyFlowDirectionCode.TLV, EnergyTariffTypeCode.N));
                            continue;
                        case 3:
                            resultList.Add(GetEdsnRegister("4", EnergyFlowDirectionCode.TLV, EnergyTariffTypeCode.L));
                            continue;
                    }
                }
            }

            for (int i = 0; i < resultList.Count; i++)
            {
                var register = resultList[i].To<GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register>();
                registers.Add(register);
            }

            ean.Registers = registers.ToArray();

            return resultList;
        }
    }
}
