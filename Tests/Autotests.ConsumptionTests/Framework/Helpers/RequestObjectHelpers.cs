﻿using Nuts.Consumption.Model.Contract;
using Nuts.Consumption.Model.Core;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class RequestObjectHelpers
    {
        public static  ValidateMeterReadingRequest GenerateValidateMeterReadingRequest(string eanId, MarketEvent me, MeterReading meterReading, DateTime openedWindow)
        {
            return new ValidateMeterReadingRequest
            {
                ReadingsToValidate = new List<ReadingsSetInfo>
                {
                    new ReadingsSetInfo
                    {
                        EanId = eanId,
                        MarketEvent = me,
                        MarketEventDate = openedWindow.AddDays(+5),
                        Readings = meterReading.RegisterReadings.Select(r => new Reading
                        { MeteringDirection = r.MeteringDirection.GetValueOrDefault(),
                            TariffType = r.TariffType.GetValueOrDefault(),
                            Value = r.Value
                        }).ToList()
                    }
                }
            };
        }

        public static void SetUpFirstSaveMeterReadingRequest(SaveMeterReadingRequest saveReadingRequest)
        {
            saveReadingRequest.MeterReadingsSet
                    .First().Readings
                    .First(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value =
                saveReadingRequest.MeterReadingsSet.First().Readings
                    .First(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value;
        }

        public static void SetUpSaveRejectedReadingRequestForSmart(SaveMeterReadingRequest saveRejectedReadingRequestForSmart)
        {
            saveRejectedReadingRequestForSmart.MeterReadingsSet
                    .First().Readings.First(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR)
                    .Value = saveRejectedReadingRequestForSmart.MeterReadingsSet.First().Readings
                                 .First(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR).Value - 50;
        }

        public static UpdateMeterReadingStatusRequest CreateUpdateRequest(
            Repositories.Consumption.Sql.ConsumptionModels.MeterReading gasSmartMeterLastReading,
            Repositories.Consumption.Sql.ConsumptionModels.MeterReading elkMeterTwoRegistersLastReading)
        {
            return new UpdateMeterReadingStatusRequest
            {
                MeterReadingIds = new List<int> { gasSmartMeterLastReading.Id, elkMeterTwoRegistersLastReading.Id },
                NewStatus = ProcessStatus.WaitForCustomer,
                Reason = new ProcessStatusReason()
                {
                    Reason = StatusReason.InsertedByAgent,
                }
            };
        }
    }
}
