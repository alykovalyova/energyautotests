﻿using Amazon.Runtime.Internal;
using Autotests.Clients;
using Autotests.Consumption.Framework.Builders;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using BuildDirector = Autotests.Consumption.Framework.Builders.BuildDirector;

namespace Autotests.Consumption.Framework.Helpers
{
    public class SupplyPhaseEntitiesProvider
    {
        //protected BuildDirector BuildDirector { get; } = new BuildDirector();

        protected FakeEdsnAdminClient FakeEdsnAdminClient = new FakeEdsnAdminClient();

        protected const string BalanceSupplierId = TestConstants.BudgetSupplier;

        private GetMeteringPointResponseEnvelope_PC_PMP_MPCC GetCommercialChars(string balanceSupplier, string balanceResponsible)
        {
            return BuildDirector.Get.CommercialCharacteristicsForSupplyPhaseBuilder
                .SetBase(balanceResponsible)
                .SetBalanceSupplier(balanceSupplier)
                .Build();
        }

        private GetMeteringPointResponseEnvelope_Address GetAddress(string buildingNr, string street, string zipCode)
        {
            return BuildDirector.Get.SupplyPhaseAddressBuilder
                .SetBase()
                .SetBuildingNr(buildingNr)
                .SetStreetName(street)
                .SetZipcode(zipCode)
                .Build();
        }

        public GetMeteringPointResponseEnvelope_PC_PMP AddSupplyPhase(string eanId, DateTime mutationDate, string balanceSupplier = BalanceSupplierId,
            string buildingNr = TestConstants.BuildingNr, string street = TestConstants.Street,
            string zipCode = TestConstants.Zipcode, string productType = "GAS", string balanceResponsible = TestConstants.BalanceResponsible,
            string gridOperator = TestConstants.GridOperator, bool isSmartMeter = false, string energyMeter = null)
        {
            var commercialCharacteristics = GetCommercialChars(balanceSupplier, balanceResponsible);
            var address = GetAddress(buildingNr, street, zipCode);
            var portalContent = BuildDirector.Get.SupplyPhaseBuilder
                .SetEanId(eanId)
                .SetProductType(productType)
                .SetAddress(address)
                .SetCommercialCharacteristics(commercialCharacteristics)
                .SetPhysicalCharacteristic(SupplyPhaseTestConstants.PhysicalCharacteristics)
                .SetEnergyMeter(SupplyPhaseTestConstants.GetPortalEnergyMeter(energyMeter))
                .SetGridArea(TestConstants.GridArea)
                .SetGridOperator(gridOperator)
                .SetMeteringPointGroup(SupplyPhaseTestConstants.MeteringPointGroup)
                .SetMarketSegment(TestConstants.MarketSegment)
                .SetLocationDescription(TestConstants.LocationDescription)
                .SetIsSmartMeterProperty(isSmartMeter)
                .SetValidFromDate(mutationDate)
                .Build();
            var mpSupplyPhase = new GetMeteringPointResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetMeteringPointResponseEnvelope_EDSNBusinessDocumentHeader()
                { MessageID = "ffec7c54-97bf-49f2-86b7-3a9f4f0db674", CreationTimestamp = DateTime.Now },
                Portaal_Content = new GetMeteringPointResponseEnvelope_PC() { Item = portalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsSupplierPhase();
            FakeEdsnAdminClient.SaveSupplierPhaseMeteringPoint(new AutoConstructedList<GetMeteringPointResponseEnvelope>() { mpSupplyPhase });
            return portalContent;
        }

        public GetMeteringPointResponseEnvelope_PC_PMP AddSupplyPhaseFromEanInfo(EanInfo ean, DateTime mutationDate, string balanceSupplier = BalanceSupplierId,
           string buildingNr = TestConstants.BuildingNr, string street = TestConstants.Street,
           string zipCode = TestConstants.Zipcode, string balanceResponsible = TestConstants.BalanceResponsible)
        {
            var commercialCharacteristics = GetCommercialChars(balanceSupplier, balanceResponsible);
            var address = GetAddress(buildingNr, street, zipCode);
            var portalContent = BuildDirector.Get.SupplyPhaseBuilder
                .SetEanId(ean.EanId)
                .SetProductType(ean.ProductType.ToString())
                .SetAddress(address)
                .SetCommercialCharacteristics(commercialCharacteristics)
                .SetPhysicalCharacteristicFromEanInfo(SupplyPhaseTestConstants.PhysicalCharacteristics, ean)
                .SetEnergyMeterFromEanInfo(ean)
                .SetGridArea(TestConstants.GridArea)
                .SetGridOperator(TestConstants.GridOperator)
                .SetMeteringPointGroup(SupplyPhaseTestConstants.MeteringPointGroup)
                .SetMarketSegment(TestConstants.MarketSegment)
                .SetLocationDescription(TestConstants.LocationDescription)
                .SetIsSmartMeterProperty(ean.AdministrativeStatusSmartMeter)
                .SetValidFromDate(mutationDate)
                .Build();
            var mpSupplyPhase = new GetMeteringPointResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetMeteringPointResponseEnvelope_EDSNBusinessDocumentHeader()
                { MessageID = "ffec7c54-97bf-49f2-86b7-3a9f4f0db674", CreationTimestamp = DateTime.Now },
                Portaal_Content = new GetMeteringPointResponseEnvelope_PC() { Item = portalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsSupplierPhase();
            FakeEdsnAdminClient.SaveSupplierPhaseMeteringPoint(new AutoConstructedList<GetMeteringPointResponseEnvelope>() { mpSupplyPhase });
            return portalContent;
        }
    }
}
