﻿using Autotests.Clients;
using Autotests.Consumption.Base;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Consumption.Model.Contract;
using Nuts.Consumption.Model.Core;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.MeteringPoint.Contract;
using Repositories.Consumption.Sql.ConsumptionModels;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;
using StatusHistory = Repositories.Consumption.Sql.ConsumptionModels.StatusHistory;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class TestAssertionHelpers
    {
        private static readonly DbHandler<ConsumptionContext> _consumptionRepository =
            new DbHandler<ConsumptionContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));
        private static readonly HttpClient MpHttpClient = new BaseHttpClient(ConfigHandler.Instance.ApiUrls.Mp);
        private static readonly NutsHttpClient _mpClient = new NutsHttpClient(MpHttpClient);

        public static void AssertRegisterValidationResults(RegisterValidationResult result)
        {
            if (result.MeteringDirection == EnergyFlowDirectionCode.TLV
                && (result.TariffType == EnergyTariffTypeCode.N || result.TariffType == EnergyTariffTypeCode.L))
            {
                Assert.AreEqual(ValidationStatus.Valid, result.ValidationStatus);
            }
            else if (result.MeteringDirection == EnergyFlowDirectionCode.LVR
                     && (result.TariffType == EnergyTariffTypeCode.N || result.TariffType == EnergyTariffTypeCode.L))
            {
                Assert.AreEqual(ValidationStatus.InvalidFunctionality, result.ValidationStatus);
            }
        }

        public static void ValidateInDisputeReadingStatus(int disputedReadingId)
        {
            var inDisputeReading = ConsumptionServiceHelpers.GetMeterReadingById(disputedReadingId);
            Assert.AreEqual(ProcessStatus.InDispute, inDisputeReading.MeterReading.Status);
        }

        public static void ValidateRejectedAndAcceptedStatuses(int disputedReadingId, EanInfo eanInfo)
        {
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int) ProcessStatus.Rejected));

            var acceptedReading = _consumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == eanInfo.EanId,
                    "RegisterReading");
            Assert.AreEqual((int) ProcessStatus.Accepted, acceptedReading.StatusId);
        }

        public static MeterReading ValidateCheckDisputeStatus(EanInfo eanInfo)
        {
            var newReading = _consumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == eanInfo.EanId,
                    "RegisterReading");
            Assert.AreEqual((int) ProcessStatus.CheckDispute, newReading.StatusId);
            return newReading;
        }

        public static void AssertInDisputeReadingStatus(int disputedReadingId)
        {
            var inDisputeReading = ConsumptionServiceHelpers.GetMeterReadingById(disputedReadingId);
            Assert.AreEqual(ProcessStatus.InDispute, inDisputeReading.MeterReading.Status);
            Assert.AreEqual(
                BaseConsDomTest.UserNameResponsible,
                _consumptionRepository
                    .GetLastEntity<StatusHistory, DateTime>(e => e.CreatedOn,
                        e => e.MeterReadingId == disputedReadingId).User);
        }

        public static int WaitAndAssertWaitFroDisputeStatus(EanInfo ean)
        {
            var newReadingId = _consumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == ean.EanId, "RegisterReading")
                .Id;
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.WaitForDispute));

            Assert.AreEqual(
                BaseConsDomTest.UserNameResponsible,
                _consumptionRepository
                    .GetLastEntity<StatusHistory, DateTime>(e => e.CreatedOn, e => e.MeterReadingId == newReadingId)
                    .User);
            return newReadingId;
        }

        public static void AcceptNewReadingAndAssertAcceptedStatus(EanInfo ean, string newReadingExternalReference,
            int disputedReadingId, int newReadingId)
        {
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(newReadingExternalReference);
            Waiter.Wait(() => _consumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int) ProcessStatus.Rejected));

            var acceptedReading = _consumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == ean.EanId, "RegisterReading");
            Assert.AreEqual((int) ProcessStatus.Accepted, acceptedReading.StatusId);
            Assert.AreEqual(BaseConsDomTest.EdsnServiceResponsible, _consumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(e => e.CreatedOn, e => e.MeterReadingId == disputedReadingId)
                .User);
            Assert.AreEqual(BaseConsDomTest.EdsnServiceResponsible, _consumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(e => e.CreatedOn, e => e.MeterReadingId == newReadingId).User);
        }

        public static void AssertHistoricalDisputeReadings(EanInfo eanInfo)
        {
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(eanInfo);

            Assert.IsNotNull(historicalReadings.HistoricalReadingSet);
            Assert.AreEqual(1, historicalReadings.HistoricalReadingSet
                .First().HistoricalReadings.Count(x => x.Status == ProcessStatus.CheckDispute));
            Assert.AreEqual(1, historicalReadings.HistoricalReadingSet
                .First().HistoricalReadings.Count(x => x.Status == ProcessStatus.Disputed));
        }

        public static void AssertHistoricalSettlementsReadings(EanInfo eanInfo)
        {
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(eanInfo);

            Assert.IsNotNull(historicalReadings.HistoricalReadingSet);
            Assert.AreEqual(1, historicalReadings.HistoricalReadingSet
                .First().HistoricalReadings.Count(x => x.Status == ProcessStatus.CheckSettlement));
            Assert.AreEqual(1, historicalReadings.HistoricalReadingSet
                .First().HistoricalReadings.Count(x => x.Status == ProcessStatus.Settled));
        }

        public static void AssertHistoricalSettlementsReadingsAndWaitForSettlementStatus(EanInfo eanInfo)
        {
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(eanInfo);

            Assert.IsNotNull(historicalReadings.HistoricalReadingSet);
            Assert.AreEqual(1, historicalReadings.HistoricalReadingSet
                .First().HistoricalReadings.Count(x => x.Status == ProcessStatus.WaitForSettlement));
        }

        public static void AssertHistoricalDisputeRejectedReadings(EanInfo eanInfo)
        {
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(eanInfo);

            Assert.IsNotNull(historicalReadings.HistoricalReadingSet);
            Assert.AreEqual(0, historicalReadings.HistoricalReadingSet
                .First().HistoricalReadings.Count(x => x.Status == ProcessStatus.DisputeRejected));
        }

        public static void AssertYearlyUsageByMeterReadingResponse(int registerNr,
            GetYearlyUsageByMeterReadingResponse response, ReadingsSetInfo readingSetOfPreviousReading)
        {
            var balanceUsageNormal = response.ReadingsSetUsage.BalanceUsageNormal;
            var balanceUsageLow = response.ReadingsSetUsage.BalanceUsageLow;

            if (registerNr == 1)
            {
                switch (readingSetOfPreviousReading.Readings.First().TariffType)
                {
                    case EnergyTariffTypeCode.N:
                        Assert.IsTrue(
                            response.ReadingsSetUsage.RegisterUsages.First().YearlyUsage <= balanceUsageNormal,
                            "'normalBalance' is not less or equal to 'balanceUsageNormal'");
                        Assert.AreEqual(balanceUsageLow, null);
                        break;
                    case EnergyTariffTypeCode.L:
                        Assert.IsTrue(response.ReadingsSetUsage.RegisterUsages.First().YearlyUsage <= balanceUsageLow,
                            "'lowBalance' is not less or equal to 'balanceUsageLow'");
                        Assert.AreEqual(null, balanceUsageNormal);
                        break;
                }
            }

            switch (registerNr)
            {
                case 2:
                {
                    CheckUsageWhenRegisterNrIsTwo(response, balanceUsageNormal, balanceUsageLow);
                    break;
                }
                case 4:
                {
                    CheckUsageWhenRegisterNrIsFour(response, balanceUsageNormal, balanceUsageLow);
                    break;
                }
            }
        }

        private static void CheckUsageWhenRegisterNrIsFour(GetYearlyUsageByMeterReadingResponse response,
            int? balanceUsageNormal, int? balanceUsageLow)
        {
            var lvrNormalUsage = response.ReadingsSetUsage.RegisterUsages.First(r =>
                    r.TariffType == EnergyTariffTypeCode.N && r.MeteringDirection == EnergyFlowDirectionCode.LVR)
                .YearlyUsage;
            var lvrLowUsage = response.ReadingsSetUsage.RegisterUsages.First(r =>
                    r.TariffType == EnergyTariffTypeCode.L && r.MeteringDirection == EnergyFlowDirectionCode.LVR)
                .YearlyUsage;
            var tlvNormalUsage = response.ReadingsSetUsage.RegisterUsages.First(r =>
                    r.TariffType == EnergyTariffTypeCode.N && r.MeteringDirection == EnergyFlowDirectionCode.TLV)
                .YearlyUsage;
            var tlvLowUsage = response.ReadingsSetUsage.RegisterUsages.First(r =>
                    r.TariffType == EnergyTariffTypeCode.L && r.MeteringDirection == EnergyFlowDirectionCode.TLV)
                .YearlyUsage;
            var actualNormalBalance = lvrNormalUsage - tlvNormalUsage;
            var actualLowBalance = lvrLowUsage - tlvLowUsage;
            Assert.IsTrue(actualNormalBalance <= balanceUsageNormal,
                "'actualNormalBalance' is not less or equal to 'balanceUsageNormal'");
            Assert.IsTrue(actualLowBalance <= balanceUsageLow,
                "'actualLowBalance' is not less or equal to 'balanceUsageLow'");
        }

        private static void CheckUsageWhenRegisterNrIsTwo(GetYearlyUsageByMeterReadingResponse response,
            int? balanceUsageNormal, int? balanceUsageLow)
        {
            var normalUsage = response.ReadingsSetUsage.RegisterUsages
                .First(r => r.TariffType == EnergyTariffTypeCode.N).YearlyUsage;
            var lowUsage = response.ReadingsSetUsage.RegisterUsages
                .First(r => r.TariffType == EnergyTariffTypeCode.L).YearlyUsage;
            Assert.IsTrue(normalUsage <= balanceUsageNormal,
                "'normalUsage' is not less or equal to 'balanceUsageNormal'");
            Assert.IsTrue(lowUsage <= balanceUsageLow, "'lowUsage' is not less or equal to 'balanceUsageLow'");
        }

        public static void AssertYearlyUsageCalculation(MeterReading startReading, ReadingsSetInfo endReading, 
            EanInfo ean, GetYearlyUsageByMeterReadingResponse response)
        {
            var mp = new GetMeteringPointRequest() { Ean = ean.EanId }
                .CallWith<GetMeteringPointRequest, GetMeteringPointResponse>(_mpClient).Data.MeteringPoint;
            Dictionary<string, int> registerUsages = new Dictionary<string, int>();
            foreach (var register in startReading.RegisterReadings)
            {
                    var endReadingValue = endReading.Readings.First(rr =>
                        rr.MeteringDirection == (EnergyFlowDirectionCode)Enum.Parse(typeof(EnergyFlowDirectionCode), register.MeteringDirection.Name)
                        && rr.TariffType == (EnergyTariffTypeCode)Enum.Parse(typeof(EnergyTariffTypeCode), register.TariffType.Name)).Value;
                    var difference = endReadingValue - register.Value;
                    
                    var factor = GeneralHelpers.GetFactor(startReading, mp);
                    var usage = difference * factor;

                    var sumOfFractions = GeneralHelpers.GetSummOfFractions(ean,
                        startReading.MarketEventDate, endReading.MarketEventDate, register.MeteringDirection.Name);
                    registerUsages.Add($"{register.MeteringDirection.Name}{register.TariffType.Name}", (int) (usage / sumOfFractions));
            }

            foreach (var register in response.ReadingsSetUsage.RegisterUsages)
            {
                var actualUsage = register.YearlyUsage;
                var expected = registerUsages.First(k => k.Key.Contains($"{register.MeteringDirection}{register.TariffType}")).Value;
               
                actualUsage.Should().Equals(expected);
            }
        }
    }
}