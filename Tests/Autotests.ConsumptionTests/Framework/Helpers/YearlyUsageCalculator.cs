﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.TestDataProviders;
using NUnit.Framework;
using Nuts.Consumption.Model.Contract;
using Nuts.InterDom.Model.Core.Enums;
using Repositories.Consumption.Sql.ConsumptionModels;
using EdsnFraction = Repositories.Consumption.Sql.ConsumptionModels.EdsnFraction;

namespace Autotests.Consumption.Framework.Helpers
{
    public static class YearlyUsageCalculator
    {
        private static readonly DbHandler<ConsumptionContext> _consumptionRepository =
            new DbHandler<ConsumptionContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));

        private static int GetMaxRegisterValue(int numberOfDigits)
        {
            return (int) Math.Pow(10, numberOfDigits) - 1;
        }

        public static void GetYearlyUsageWhenEndAndStartReadingsExist(EanInfo testEan,
            Repositories.Consumption.Sql.ConsumptionModels.MeterReading endReading, Repositories.Consumption.Sql.ConsumptionModels.MeterReading startReading)
        {
            var response = new GetYearlyUsageRequest
            {
                EanList = new List<string>
                {
                    testEan.EanId
                }
            }.CallWith(new ServiceClient<IMeterReadingService>(ConfigHandler.Instance.ServicesEndpoints.CsdMeterReadingService).Proxy.GetYearlyUsage);
            Assert.That(response.Header.StatusCode, Is.EqualTo((int) HttpStatusCode.OK));
            var registerUsages = response.ReadingsSetUsages.First().RegisterUsages.ToList();

            foreach (var register in registerUsages)
            {
                decimal yearlyUsage = 0;
                if (register.MeteringDirection == EnergyFlowDirectionCode.LVR)
                {
                    switch (register.TariffType)
                    {
                        case EnergyTariffTypeCode.L:
                            yearlyUsage = testEan.EacOffPeak;
                            break;
                        case EnergyTariffTypeCode.N:
                            yearlyUsage = testEan.EacPeak;
                            break;
                        case EnergyTariffTypeCode.T:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    Assert.AreEqual(yearlyUsage, register.YearlyUsage);
                }

                if (register.MeteringDirection == EnergyFlowDirectionCode.TLV)
                {
                    var startReadingRegisterValue = startReading.RegisterReadings.First(r =>
                        r.MeteringDirectionId == (int) register.MeteringDirection
                        && r.TariffTypeId == (int) register.TariffType).Value;
                    var endReadingRegisterValue = endReading.RegisterReadings.First(r =>
                        r.MeteringDirectionId == (int) register.MeteringDirection
                        && r.TariffTypeId == (int) register.TariffType).Value;
                    var sum1 = endReadingRegisterValue - startReadingRegisterValue;
                    var factor = MeteringPointsServiceHelpers.GetMultiplicationFactor(testEan.EanId);
                    if (sum1 > 0)
                    {
                        var usage = sum1 * factor;
                        var profileCategory = testEan.ProfileCategory.ToString();
                        var profileFractionSum = _consumptionRepository.GetEntitiesByCondition<EdsnFraction>(
                                e => e.ProfileCode.Name == profileCategory
                                     && e.FractionDate >= startReading.MarketEventDate
                                     && e.FractionDate <= endReading.MarketEventDate)
                            .Sum(e => e.FractionValue);
                        yearlyUsage = (int) (usage / profileFractionSum);
                    }
                    else
                    {
                        var meteringPoint = GeneralHelpers.GetMeteringPointsByDate(testEan.EanId, DateTime.Now);
                        var maxValue = GetMaxRegisterValue(meteringPoint.EnergyMeter.Registers.First().NrOfDigits);
                        var halfMaxValue = maxValue / 2;
                        if ((startReadingRegisterValue > halfMaxValue) && (endReadingRegisterValue < halfMaxValue))
                        {
                            yearlyUsage = (maxValue - startReadingRegisterValue + endReadingRegisterValue) * factor;
                        }
                        else
                        {
                            yearlyUsage = 0;
                        }
                    }

                    Assert.AreEqual(yearlyUsage, register.YearlyUsage);
                }
            }
        }
    }
}