﻿using AutoMapper;

namespace Autotests.Consumption.Framework.MapProfiles
{
    public class ConsDomConfigProfile : Profile
    {
        public ConsDomConfigProfile()
        {
            CreateMap<Nuts.EdsnGateway.Contract.Queue.Mdu.Register,
                    GetMeteringPointResponseEnvelope_PC_PMP_Portaal_EnergyMeter_Register>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.EdsnId))
                .ForMember(dest => dest.TariffType, opt => opt.MapFrom(src => src.TariffType))
                .ForMember(dest => dest.MeteringDirection,
                    opt => opt.MapFrom(src => src.MeteringDirection.GetValueOrDefault()))
                .ForMember(dest => dest.NrOfDigits, opt => opt.MapFrom(src => src.NrOfDigits))
                .ForMember(dest => dest.MultiplicationFactor, opt => opt.MapFrom(src => src.MultiplicationFactor.GetValueOrDefault()))
                .ForMember(dest => dest.MultiplicationFactorSpecified, opt => opt.Ignore());
        }
    }
}

