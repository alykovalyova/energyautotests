﻿using Autotests.Clients;
using Autotests.Consumption.Base;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using System.Net;
using Autotests.Consumption.Framework.Helpers;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class CheckLastSmartMeterReadingsTests : CheckLastSmartMeterReadingsBase
    {
        private EanInfo _elkEan;
        private EanInfo _gasEan;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEan = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEan, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEan, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEan, _gasEan });
        }

        [Test]
        public void CheckLastSmartMeterReadings_DefaultValidTest_ReturnsTrue()
        {
            CleanMeterReadingsForEan(_elkEan);
            var maxDate = DateTime.Now.AddYears(-1);

            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEan, OpenedWindow,
                MarketEvent.Yearly, ReadingSource.P4_BackOffice);

            var lastSmartMrResponse = GetCheckLastSmartMeterReadingsRequest(maxDate, eans: _elkEan.EanId)
                .CallWith(MeterReadingClient.Proxy.CheckLastSmartMeterReadings);

            Assert.That(lastSmartMrResponse.Header.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));
            Assert.That(lastSmartMrResponse.SmartMeterReadingsExist, Is.True);
        }

        [Test]
        public void CheckLastSmartMeterReadings_NoneSmartMeterReading_ReturnsFalse()
        {
            var maxDate = DateTime.Now.AddYears(-1);
            CleanMeterReadingsForEan(_gasEan);

            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_gasEan, OpenedWindow,
                MarketEvent.Yearly, ReadingSource.Customer_Backoffice);

            var lastSmartMrResponse = GetCheckLastSmartMeterReadingsRequest(maxDate, eans: _gasEan.EanId)
                .CallWith(MeterReadingClient.Proxy.CheckLastSmartMeterReadings);

            Assert.That(lastSmartMrResponse.Header.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));
            Assert.That(lastSmartMrResponse.SmartMeterReadingsExist, Is.False);
        }

        [Test]
        public void CheckLastSmartMeterReadings_WithInvalidDates_ReturnsFalse()
        {
            var maxDate = DateTime.Now.AddYears(-1);
            var mrWithoutSmartReadings = ConsumptionRepository.GetEntityByCondition<MeterReading>(mr =>
                mr.ReadingSourceId == 16 && mr.MarketEventDate < maxDate && mr.StatusId == 1);

            var response = GetCheckLastSmartMeterReadingsRequest(maxDate, eans: mrWithoutSmartReadings.EanId)
                .CallWith(MeterReadingClient.Proxy.CheckLastSmartMeterReadings);

            Assert.That(response.Header.StatusCode, Is.EqualTo((int)HttpStatusCode.OK));
            Assert.That(response.SmartMeterReadingsExist, Is.True);
        }

        [Test]
        [TestCase("8716871100002515189", TestName = "CheckLastSmartMeterReadings_EanLengthLonger18_Returns500")]
        [TestCase("87168711000025151", TestName = "CheckLastSmartMeterReadings_EanLengthLess18_Returns500")]
        [TestCase("ffffffffffffffffff", TestName = "CheckLastSmartMeterReadings_WithEanOfChars_Returns500")]
        [TestCase("", TestName = "CheckLastSmartMeterReadings_WithEmptyEan_Returns500")]
        public void CheckLastSmartMeterReadings_WithInvalidEanField_Returns500(string eanId)
        {
            var response = GetCheckLastSmartMeterReadingsRequest(eans: eanId)
                .CallWith(MeterReadingClient.Proxy.CheckLastSmartMeterReadings);

            var message = string.Format(WrongEanRegex, eanId, EanRegex);
            Assert.That(response.Header.StatusCode, Is.EqualTo((int)HttpStatusCode.InternalServerError));
            Assert.That(response.SmartMeterReadingsExist, Is.False);
            Assert.That(response.Header.Message, Is.EqualTo(message));
        }
    }
}