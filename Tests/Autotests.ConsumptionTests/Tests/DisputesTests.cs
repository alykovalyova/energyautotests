﻿using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using ConsDomSqlModels = Repositories.Consumption.Sql.ConsumptionModels;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using CcuProcessor = Autotests.Consumption.Framework.Helpers.CcuProcessor;
using GeneralHelpers = Autotests.Consumption.Framework.Helpers.GeneralHelpers;
using MeteringPointsServiceHelpers = Autotests.Consumption.Framework.Helpers.MeteringPointsServiceHelpers;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class DisputesTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
        }

        [Test]
        public void AcceptDisputeStartedByBudget(
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.Switch)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, OpenedWindow, me, ReadingSource.Customer_EDSN);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, me);

            //start dispute on created reading, wait and validate WaitForDispute status
            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);
            var newReadingId = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart4Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForDispute));
            
            TestAssertionHelpers.ValidateInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart4Reg.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.AcceptDisputeByFakeEdsn(newReadingExternalReference, disputedReadingId);
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(newReadingExternalReference);

            TestAssertionHelpers.ValidateRejectedAndAcceptedStatuses(disputedReadingId, _elkEanSmart4Reg);
        }

        [Test]
        public void CheckResponsibleAcceptDisputeStartedByBudget(
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _gasEanSmart);
            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEanSmart, OpenedWindow, me, ReadingSource.Customer_Backoffice);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, me);
            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);

            var newReadingId = TestAssertionHelpers.WaitAndAssertWaitFroDisputeStatus(_gasEanSmart);
            TestAssertionHelpers.AssertInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId, true);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEanSmart.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.AcceptDisputeByFakeEdsn(newReadingExternalReference, disputedReadingId, true);
            TestAssertionHelpers.AcceptNewReadingAndAssertAcceptedStatus(_gasEanSmart, newReadingExternalReference, disputedReadingId, newReadingId);
        }

        [Test]
        public void RejectDisputeStartedByBudget(
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.Spontaneous, MarketEvent.Switch)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _gasEan);
            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, OpenedWindow, me, ReadingSource.Customer_EDSN);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, me);

            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);
            var newReadingId = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForDispute), 200);
            
            TestAssertionHelpers.ValidateInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.RejectDisputeByFakeEdsn(newReadingExternalReference, newReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int)ProcessStatus.Accepted), 200);
        }

        [Test]
        public void AcceptDisputeStartedByOtherParty(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice, 
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.MoveOut, MarketEvent.Spontaneous, 
                MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            var date = DateTime.Now.AddDays(-16);
            var oldMeterReading = ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanNonSmart4Reg, date, me, ReadingSource.Customer_Backoffice);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart4Reg, date, me, ReadingSource.Customer_Backoffice);
            meterReading.RegisterReadings.ForEach(re => re.Value = oldMeterReading.RegisterReadings
                .Find(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.N).Value + 5);
            var readingInfo = meterReading.To();
            var disputedReadingInfo = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart4Reg.EanId, "RegisterReading");

            GeneralHelpers.SendDisputeFromOtherPartyAndWaitStatus(_elkEanNonSmart4Reg, readingInfo, date, disputedReadingInfo);
            var newReading = TestAssertionHelpers.ValidateCheckDisputeStatus(_elkEanNonSmart4Reg);
            GeneralHelpers.AnswerDisputeByBudgetAndValidateRejectedStatus(newReading, disputedReadingInfo);

            var acceptedReading = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart4Reg.EanId, "RegisterReading");
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(acceptedReading.ExternalReference);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == acceptedReading.Id && mr.StatusId == (int)ProcessStatus.Accepted));
        }

        [Test, Pairwise, Order(1)]
        public void RejectDisputeStartedByOtherParty(
            [Values(ReadingSource.Customer_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.Spontaneous)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            var date = DateTime.Now.AddDays(-16);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanNonSmart2Reg, date, me, rs);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, date, me, rs);
            var readingInfo = meterReading.To();
            var disputedReadingInfo = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading");

            GeneralHelpers.SendDisputeFromOtherPartyAndWaitStatus(_elkEanNonSmart2Reg, readingInfo, date, disputedReadingInfo);
            var newReading = TestAssertionHelpers.ValidateCheckDisputeStatus(_elkEanNonSmart2Reg);
            GeneralHelpers.AnswerDisputeByBudgetAndValidateAcceptedStatus(newReading, disputedReadingInfo);

            var rejectedReading = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading");
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<ConsDomSqlModels.MeterReading>(
                mr => mr.Id == rejectedReading.Id && mr.StatusId == (int)ProcessStatus.Rejected), 200);
        }

        [Test]
        public void DisputeNotProcessedWhenSameValuesAreReceived(
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanNonSmart2Reg);
            var date = DateTime.Now.AddDays(-16);
            var odlMeterReading = ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanNonSmart2Reg, date, me, ReadingSource.Customer_Backoffice);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, date, me, ReadingSource.Customer_Backoffice);
            meterReading.RegisterReadings
                .ForEach(re => re.Value = odlMeterReading.RegisterReadings
                    .Find(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.N).Value);

            var readingInfo = meterReading.To();
            var disputedReadingInfo = ConsumptionRepository
                .GetLastEntity<ConsDomSqlModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading");
            readingInfo.EnergyMeter.Registers
                .ForEach(re => re.EndReading.Reading = disputedReadingInfo.RegisterReadings
                    .FirstOrDefault(r => r.MeteringDirectionId == (int)re.FlowDirection && r.TariffTypeId == (int)re.TariffType).Value);
            GeneralHelpers.SendDisputeFromOtherPartyAndAssertStatus(_elkEanNonSmart2Reg, readingInfo, date, disputedReadingInfo);
        }
    }
}