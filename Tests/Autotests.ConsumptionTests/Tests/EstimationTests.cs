﻿using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FakeMeterReadingService;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression), Order(1)]
    internal class EstimationTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _elkEanSmart4RegTfSignal;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _elkEanSmart4RegTfSignal = EanInfoProvider.GetRandomElkEan(false, 4, profileCode: EnergyUsageProfileCode.E1A);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanSmart4RegTfSignal, _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanSmart4RegTfSignal, _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _gasEanSmart, _gasEan });
        }

        [Test]
        public void EstimateForTheFutureDate(
            [Values(MarketEvent.BeginOfMeterSupport, MarketEvent.Dispute, MarketEvent.EndOfMeterSupport,
                MarketEvent.EndOfSupply, MarketEvent.ExchangeOfMeter, MarketEvent.Historical, MarketEvent.MoveIn,
                MarketEvent.MoveOut, MarketEvent.Physical, MarketEvent.Renewal, MarketEvent.Settlement,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Unknown,
                MarketEvent.VolumeRegisterReading, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart4Reg }, CcuMutationDate);
            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanNonSmart4Reg.EanId, me, NotOpenedWindow);

            estimationResponse.EstimationData.First().EstimationRegisters.ForEach(re =>
                re.Estimation.EstimationMin.Should().NotBe(re.Estimation.EstimationMax));
        }

        [Test]
        public void EstimateForThePastDate_Smoke(
            [Values(MarketEvent.EndOfMeterSupport)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate);
            var estimation = ConsumptionServiceHelpers.EstimateReadings(_gasEanSmart.EanId, me, ClosedWindow);

            estimation.EstimationData.First().EstimationRegisters.ForEach(re =>
                re.Estimation.EstimationMin.Should().NotBe(re.Estimation.EstimationMax));
        }

        [Test]
        public void EstimateForFutureDateIfAcceptedReadingIsToday_Smoke(
            [Values(MarketEvent.Physical, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, OpenedWindow, me, ReadingSource.Customer_EDSN);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId, me, OpenedWindow.AddDays(5));

            foreach (var register in estimationResponse.EstimationData.First().EstimationRegisters)
            {
                if (register.MeteringDirection != EnergyFlowDirectionCode.LVR) continue;
                register.Estimation.EstimationMax.Should().BeInRange(0, Int32.MaxValue); 
                register.Estimation.Estimation.Should().BeInRange(0, Int32.MaxValue); 
                register.Estimation.EstimationMin.Should().BeInRange(0, Int32.MaxValue);
            }
        }

        [Test]
        public void EstimateYearlyEvent()
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var estimationSpontaneous = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId,
                MarketEvent.Spontaneous, NotOpenedWindow).EstimationData.First();
            var estimationYearly = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId,
                MarketEvent.Yearly, NotOpenedWindow).EstimationData.First();

            var expectedRes = estimationSpontaneous.EstimationRegisters
                .Find(r => r.TariffType == EnergyTariffTypeCode.N && r.MeteringDirection == EnergyFlowDirectionCode.LVR)
                .Estimation.Estimation;
            var actualRes = estimationYearly.EstimationRegisters
                .Find(r => r.TariffType == EnergyTariffTypeCode.N && r.MeteringDirection == EnergyFlowDirectionCode.LVR)
                .Estimation.Estimation;

            expectedRes.Should().Be(actualRes);
        }

        [Test]
        public void EstimateTfSignalReading_ValidResponse()
        {
            var mutationDate = DateTime.Today.AddDays(-3);
            MduProcessor.SendMdu(new List<EanInfo>() { _elkEanSmart4RegTfSignal }, mutationDate, tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(_elkEanSmart4RegTfSignal, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == _elkEanSmart4RegTfSignal.EanId && mp.IsTfSignal == true), 10);
            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4RegTfSignal.EanId,
                MarketEvent.Spontaneous, DateTime.Today);

            estimationResponse.EstimationData.First().EstimationRegisters.ForEach(er => er.Estimation.Estimation.Should().BePositive());
            estimationResponse.Header.Message.Should().BeNullOrEmpty();
        }

        [Test, Pairwise]
        public void EstimateForThePastDate(
            [Values(MarketEvent.BeginOfMeterSupport, MarketEvent.Dispute, MarketEvent.EndOfMeterSupport, MarketEvent.EndOfSupply,
                MarketEvent.ExchangeOfMeter, MarketEvent.Historical, MarketEvent.MoveIn, MarketEvent.MoveOut, MarketEvent.Physical,
                MarketEvent.Renewal, MarketEvent.Settlement, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly,
                MarketEvent.Unknown, MarketEvent.VolumeRegisterReading, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate);
            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_gasEanSmart.EanId, me, ClosedWindow);

            estimationResponse.EstimationData.First().EstimationRegisters.ForEach(re =>
                re.Estimation.EstimationMin.Should().NotBe(re.Estimation.EstimationMax));
        }

        [Test]
        public void EstimateMoveInReading()
        {
            const MarketEvent marketEvent = MarketEvent.MoveIn;

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId, marketEvent, NotOpenedWindow);

            estimationResponse.EstimationData.First().EstimationRegisters.ForEach(re =>
                re.Estimation.Estimation.Should().Be(re.Estimation.EstimationMin));
        }

        [Test]
        public void EstimateIfNoHistoricalReadings(
            [Values(MarketEvent.BeginOfMeterSupport, MarketEvent.Dispute, MarketEvent.EndOfMeterSupport,
                MarketEvent.EndOfSupply, MarketEvent.ExchangeOfMeter, MarketEvent.Historical, MarketEvent.MoveIn, MarketEvent.MoveOut,
                MarketEvent.Physical, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId, me, NotOpenedWindow);

            foreach (var register in estimationResponse.EstimationData.First().EstimationRegisters)
            {
                register.Estimation.Estimation.Should().Be(0);
                register.Estimation.EstimationMin.Should().Be(0);
                register.Estimation.EstimationMax.Should().Be(0);
            }
        }

        [Test]
        public void EstimateForTodayDateIfAcceptedReadingIsYesterday(
            [Values(MarketEvent.Dispute, MarketEvent.EndOfSupply, MarketEvent.Historical,
                MarketEvent.MoveIn, MarketEvent.MoveOut, MarketEvent.Physical, MarketEvent.Spontaneous,
                MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, OpenedWindow.AddDays(-1), me, ReadingSource.Customer_EDSN);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId, me, OpenedWindow);

            foreach (var register in estimationResponse.EstimationData.First().EstimationRegisters)
            {
                register.Estimation.Estimation.Should().BeInRange(0, Int32.MaxValue);
                register.Estimation.EstimationMin.Should().BeInRange(0, Int32.MaxValue);
                register.Estimation.EstimationMax.Should().BeInRange(0, Int32.MaxValue);
            }
        }

        [Test]
        public void EstimateForFutureDateIfAcceptedReadingIsToday(
            [Values(MarketEvent.Dispute, MarketEvent.EndOfSupply, MarketEvent.Historical,
                MarketEvent.MoveOut, MarketEvent.Physical, MarketEvent.Spontaneous, MarketEvent.Switch,
                MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEanSmart, OpenedWindow, me, ReadingSource.Customer_EDSN);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_gasEanSmart.EanId, me, OpenedWindow.AddDays(5));

            foreach (var register in estimationResponse.EstimationData.First().EstimationRegisters)
            {
                if (register.MeteringDirection != EnergyFlowDirectionCode.LVR) continue;
                register.Estimation.Estimation.Should().BeInRange(0, Int32.MaxValue);
                register.Estimation.EstimationMin.Should().BeInRange(0, Int32.MaxValue);
                register.Estimation.EstimationMax.Should().BeInRange(0, Int32.MaxValue);
            }
        }

        [Test]
        public void EstimateForTodayWithAdditionalTmrCall(
            [Values(MarketEvent.MoveIn, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch,
                MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEanSmart, OpenedWindow, me, ReadingSource.Customer_TMR);
            var meterReadingGatewayModel = meterReading.To();
            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(meterReadingGatewayModel, Nuts.FakeEdsn.Contracts.MeterReadingStatus.MoveToTmrSupplierPhase);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);

            ConsumptionServiceHelpers.EstimateReadings(_gasEanSmart.EanId, me, OpenedWindow);
            var tmrReading = ConsumptionRepository
                .GetLastEntity<Repositories.Consumption.Sql.ConsumptionModels.MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEanSmart.EanId);

            tmrReading.ReadingSourceId.Should().Be((int)ReadingSource.Customer_TMR);
        }
    }
}