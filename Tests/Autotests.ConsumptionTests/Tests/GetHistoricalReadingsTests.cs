﻿using Autotests.Core.Helpers;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core.TestDataProviders;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;
using FluentAssertions;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class GetHistoricalReadingsTests : BaseConsDomTest
    {
        private EanInfo _elkEan;
        private EanInfo _gasEan;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEan = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            MduProcessor.SendMdu(new List<EanInfo> {_elkEan, _gasEan});
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEan, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEan, _gasEan });
        }

        [Test]
        public void GetApproveHistoricalReadings(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            
            GeneralHelpers.AcceptMeterReadingForEan(_elkEan, me, rs, OpenedWindow);
            GeneralHelpers.ApproveMeterReadingForEan(_elkEan, me, rs);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_elkEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeEmpty();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.Approved).Should().Be(1);
        }

        [Test]
        public void GetAcceptedHistoricalReadings(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var readingDate = DateTime.Now.AddDays(-1);
            
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, readingDate, me, ReadingSource.Customer_Backoffice);
            var meterPoint = meterReading.To();
            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(meterPoint, MeterReadingStatus.Accepted);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.EanId == _gasEan.EanId && mr.MarketEventDate == readingDate && mr.StatusId == (int)ProcessStatus.Accepted));
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_gasEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeEmpty();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.First().Status.Should()
                .Be(ProcessStatus.Accepted);
        }

        [Test]
        public void GetVerifyByAgentHistoricalReadings(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);

            var readingSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEan.EanId, me, rs, OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_gasEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeEmpty();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.VerifyByAgent).Should().Be(1);
        }

        [Test]
        public void GetWaitForCustomerHistoricalReadings(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.EndOfSupply)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEan.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_elkEan);

            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.WaitForCustomer).Should().Be(1);
        }

        [Test]
        public void GetNoTooLateHistoricalReadingsIfNewReadingsExist(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Spontaneous)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);

            var tooLateReadingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEan, me, rs, ClosedWindow);
            var readyToSendReadingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEan, me, rs, OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(tooLateReadingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readyToSendReadingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_gasEan);
           
            historicalReadings.HistoricalReadingSet.Should().NotBeEmpty();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.TooLate).Should().Be(0);
        }

        [Test]
        public void GetNoTechExcpHistoricalReadingsIfNewReadingsExist(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Spontaneous)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            var techExcpReadingDate = DateTime.Now.AddDays(-25);

            var techExcpReadingsSet = ConsumptionServiceHelpers.CreateTechExcpReadingSet(_elkEan, me, rs, techExcpReadingDate);
            var readyToSendReadingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEan, me, rs, OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(techExcpReadingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readyToSendReadingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_elkEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeEmpty();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.TechExcp).Should().Be(0);
        }

        [Test]
        public void GetNoWaitToSendHistoricalReadingsIfNewReadingsExist(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Spontaneous)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var acceptedReadingDate = DateTime.Now.AddDays(+30);
            
            var waitToSendReadingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEan, me, rs, NotOpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(waitToSendReadingsSet);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, acceptedReadingDate, me, rs);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_gasEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeNull();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.WaitToSend).Should().Be(0);
        }

        [Test]
        public void GetDisputeHistoricalReadings(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.EndOfSupply)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            var date = DateTime.Now.AddDays(-16);

            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEan, date, me, ReadingSource.Customer_Backoffice);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEan, date, me, ReadingSource.Customer_Backoffice);
            var readingInfo = meterReading.To();
            var disputedReadingInfo = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEan.EanId, "RegisterReading");
            GeneralHelpers.SendDisputeFromOtherPartyAndWaitStatus(_elkEan, readingInfo, date, disputedReadingInfo);

            TestAssertionHelpers.ValidateCheckDisputeStatus(_elkEan);
            TestAssertionHelpers.AssertHistoricalDisputeReadings(_elkEan);
        }

        [Test]
        public void GetSettlementsHistoricalReadings(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            var settlementWindow = OpenedWindow.AddMonths(-4);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEan, settlementWindow, me, rs);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            GeneralHelpers.StartOfSettlementAsOtherParty(meterReading, settledReadingId, _elkEan);
            TestAssertionHelpers.AssertHistoricalSettlementsReadings(_elkEan);
        }

        [Test]
        public void GetWaitForSettlementHistoricalReadings(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var settlementWindow = OpenedWindow.AddMonths(-4);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, settlementWindow, me, rs);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);
            ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);
            GeneralHelpers.ValidateInSettlementAndWaitForSettlementStatuses(settledReadingId, _gasEan);
            TestAssertionHelpers.AssertHistoricalSettlementsReadingsAndWaitForSettlementStatus(_gasEan);
        }

        [Test]
        public void GetRejectedHistoricalReadingsNotPossible(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEan }, CcuMutationDate.AddDays(1));

            MeteringPointsServiceHelpers.GetMeterReading(_elkEan, OpenedWindow, me, ReadingSource.Customer_EDSN);
            ConsumptionServiceHelpers.RejectConsDomMeterReading(_elkEan, OpenedWindow, me, rs, MeterReadingStatus.RejectedByEdsn);
            var readingId = ConsumptionRepository.GetEntityByCondition<MeterReading>(mr =>
                mr.EanId == _elkEan.EanId && mr.StatusId == (int)ProcessStatus.RejectedByEdsn).Id;
            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.Rejected, readingId);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_elkEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeNull();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.Rejected).Should().Be(0);
        }

        [Test]
        public void GetDisputeRejectedHistoricalReadingsNotPossible(
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);

            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, OpenedWindow, me, ReadingSource.Customer_Backoffice);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, me);
            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);
            var newReadingId = GeneralHelpers.ValidateWaitForDisputeStatus(_gasEan);
            TestAssertionHelpers.ValidateInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.RejectDisputeByFakeEdsn(newReadingExternalReference, newReadingId);

            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int)ProcessStatus.Accepted));

            TestAssertionHelpers.AssertHistoricalDisputeRejectedReadings(_gasEan);
        }

        [Test]
        public void GetRejectedByEdsnHistoricalReadingsNotPossible(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEan }, CcuMutationDate.AddDays(1));

            ConsumptionServiceHelpers.RejectConsDomMeterReading(_elkEan, OpenedWindow, me, rs, MeterReadingStatus.RejectedByEdsn);
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_elkEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeNull();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.Count(x => x.Status == ProcessStatus.RejectedByEdsn).Should().Be(0);
        }

        [Test]
        public void GetHistoricalReadingsWithoutVolume(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEan);
            var readingDate = DateTime.Now.AddDays(-1);
            
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEan, readingDate, me, ReadingSource.Customer_Backoffice);
            var meterPoint = meterReading.To();
            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(meterPoint, MeterReadingStatus.Accepted);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                    mr => mr.EanId == _elkEan.EanId && mr.MarketEventDate == readingDate && mr.StatusId == (int)ProcessStatus.Accepted));
            var historicalReadings = GeneralHelpers.GetHistoricalReadingsByEan(_elkEan);

            historicalReadings.HistoricalReadingSet.Should().NotBeNull();
            historicalReadings.HistoricalReadingSet.First().HistoricalReadings.First().RegisterReadings.First().Volume.Should().BeNull();
        }
    }
}