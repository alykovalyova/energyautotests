﻿using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Repositories.Consumption.Sql.ConsumptionModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class GetLastAcceptedReadingTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
        }

        [Test]
        public void CheckSourceToSkipIsNotReturned(
            [Values(ReadingSource.Customer_Mijn, ReadingSource.Customer_Backoffice, ReadingSource.Customer_TMR, ReadingSource.Estimated_TMR)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart4Reg }, CcuMutationDate.AddDays(1));
            var date = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart4Reg.EanId, "RegisterReading").MarketEventDate;
            var readings = ConsumptionServiceHelpers.GetLastAcceptedReading(_elkEanSmart4Reg.EanId, date, rs).MeterReadings;

            readings.ForEach(re => re.ReadingSource.Should().NotBe(rs));
        }

        [Test]
        public void CheckReadingsEarlierThanSetDateAreNotReturned(
            [Values(ReadingSource.Customer_Mijn, ReadingSource.Customer_Backoffice, ReadingSource.Customer_TMR, ReadingSource.Estimated_TMR)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_gasEan);
            var readingDate = DateTime.Now.AddMonths(-10);
            var requestDate = DateTime.Now;

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, readingDate, MarketEvent.Yearly, rs);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, MarketEvent.Yearly);
            var lastAcceptedReadingResponse = ConsumptionServiceHelpers.GetLastAcceptedReading(_gasEan.EanId, requestDate, rs);

            lastAcceptedReadingResponse.MeterReadings.Count.Should().Be(0);
        }
    }
}
