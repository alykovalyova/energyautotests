﻿using Autotests.Clients;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Consumption.Model.Contract;
using Nuts.InterDom.Model.Core.Enums;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class GetUsageCalculationsTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanNonSmart4Reg2;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _elkEan4RegNoReadings;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanNonSmart4Reg2 = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _elkEan4RegNoReadings = EanInfoProvider.GetRandomElkEan(false, 4);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanNonSmart4Reg2, _elkEan4RegNoReadings, _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart2Reg, _elkEanSmart2Reg, _gasEanSmart, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanNonSmart4Reg2, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _gasEan });
        }

        [Test]
        public void GetYearlyUsage_EanWithoutProfileCategory()
        {
            MduProcessor.SendMdu(new List<EanInfo>() { _elkEanSmart4Reg }, DateTime.Now.AddDays(-5), hasProfileCategory: false);
            var response = new GetYearlyUsageRequest
            {
                EanList = new List<string> { _elkEanSmart4Reg.EanId }
            }.CallWith(MeterReadingClient.Proxy.GetYearlyUsage);

            foreach (var register in response.ReadingsSetUsages)
            {
                var expectedRegisterTlv = register.RegisterUsages.Where(r => r.MeteringDirection == EnergyFlowDirectionCode.TLV);
                expectedRegisterTlv.First().ErrorMessage.Should().Be(PatternMessages.NoProfileCategoryForTlv);
                var expectedRegisterLvr = register.RegisterUsages.Where(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR);
                expectedRegisterLvr.First().YearlyUsage.Should().NotBe(0);
            }
        }

        [Test]
        public void GetYearlyUsage_EanWithoutReadings()
        {
            var response = new GetYearlyUsageRequest
            {
                EanList = new List<string> { _elkEan4RegNoReadings.EanId }
            }.CallWith(MeterReadingClient.Proxy.GetYearlyUsage);

            foreach (var register in response.ReadingsSetUsages)
            {
                var expectedRegisterTlv = register.RegisterUsages.Where(
                    r => r.MeteringDirection == EnergyFlowDirectionCode.TLV);
                expectedRegisterTlv.First().ErrorMessage.Should().Be(PatternMessages.NoReadingsFoundForUsageCalculation);
                var expectedRegisterLvrN = register.RegisterUsages.Where(
                    r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.N);
                expectedRegisterLvrN.First().YearlyUsage.Should().Be(_elkEan4RegNoReadings.EacPeak);
                var expectedRegisterLvrL = register.RegisterUsages.Where(
                    r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.L);
                expectedRegisterLvrL.First().YearlyUsage.Should().Be(_elkEan4RegNoReadings.EacOffPeak);
            }
        }

        [Test]
        public void GetYearlyUsage_EanWithoutStartReading()
        {
            CleanMeterReadingsForEan(_elkEan4RegNoReadings);
            var endReadingDate = DateTime.Now.AddMonths(-23);
            var minDate = endReadingDate.AddDays(-300).ToString("d-M-yyyy");
            GeneralHelpers.CreateReadingForEan(_elkEan4RegNoReadings, endReadingDate, MarketEvent.Spontaneous, rs: ReadingSource.Customer_Backoffice);

            var response = new GetYearlyUsageRequest
            {
                EanList = new List<string> { _elkEan4RegNoReadings.EanId }
            }.CallWith(MeterReadingClient.Proxy.GetYearlyUsage);

            var errorMessage = string.Format(PatternMessages.NoStartReadingIsFound, minDate);
            foreach (var register in response.ReadingsSetUsages)
            {
                var expectedRegisterTlv = register.RegisterUsages.Where(
                    r => r.MeteringDirection == EnergyFlowDirectionCode.TLV);
                expectedRegisterTlv.First().ErrorMessage.Should().Contain(errorMessage);
                var expectedRegisterLvrN = register.RegisterUsages.Where(
                    r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.N);
                expectedRegisterLvrN.First().YearlyUsage.Should().Be(_elkEan4RegNoReadings.EacPeak);
                var expectedRegisterLvrL = register.RegisterUsages.Where(
                    r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.L);
                expectedRegisterLvrL.First().YearlyUsage.Should().Be(_elkEan4RegNoReadings.EacOffPeak);
            }
        }

        [Test, Ignore("unstable test, will investigate in https://budget.atlassian.net/browse/UT-4644")]
        public void GetBalancesYearlyUsageByMeterReadingForLVRMeter_ValidCase(
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            var startReadingDate = DateTime.Now.AddDays(-1095);

            //create start reading
            GeneralHelpers.CreateReadingForEan(_elkEanSmart2Reg, startReadingDate, me, ReadingSource.Customer_Backoffice);

            var readingSetOfPreviousReading = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanSmart2Reg, me, ReadingSource.Customer_Backoffice, OpenedWindow);
            var registerNr = GeneralHelpers.GetMeteringPointsByDate(_elkEanSmart2Reg.EanId, OpenedWindow).EnergyMeter.NrOfRegisters;

            var response = GeneralHelpers.GetYearlyUsageByMeterReadingResponse(readingSetOfPreviousReading);
            Waiter.Wait(() => ConsumptionRepository.GetEntityByCondition<MeterReading>(
                mr => mr.EanId == _elkEanSmart2Reg.EanId && mr.MarketEventDate == startReadingDate));
            var startReading = ConsumptionRepository.GetEntityByCondition<MeterReading>(
                mr => mr.EanId == _elkEanSmart2Reg.EanId && mr.MarketEventDate == startReadingDate, "RegisterReading", "RegisterReading.TariffType");

            //need to figure out why below method fails 
            TestAssertionHelpers.AssertYearlyUsageCalculation(startReading, readingSetOfPreviousReading, _elkEanSmart2Reg, response);
            TestAssertionHelpers.AssertYearlyUsageByMeterReadingResponse(registerNr, response, readingSetOfPreviousReading);
        }

        [Test, Ignore("unstable test, will investigate in https://budget.atlassian.net/browse/UT-4644")]
        public void GetBalancesYearlyUsageByMeterReadingForTLVMeter_ValidCase(
            [Values(MarketEvent.Switch)] MarketEvent me)
        {
            var startReadingDate = DateTime.Now.AddDays(-1095);

            //create start reading
            GeneralHelpers.CreateReadingForEan(_elkEanNonSmart4Reg2, startReadingDate, me, ReadingSource.Customer_Backoffice);

            var readingSetOfPreviousReading = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanNonSmart4Reg2, me, ReadingSource.Customer_Backoffice, OpenedWindow);
            var registerNr = GeneralHelpers.GetMeteringPointsByDate(_elkEanNonSmart4Reg2.EanId, OpenedWindow).EnergyMeter.NrOfRegisters;

            var response = GeneralHelpers.GetYearlyUsageByMeterReadingResponse(readingSetOfPreviousReading);
            Waiter.Wait(() => ConsumptionRepository.GetEntityByCondition<MeterReading>(
                mr => mr.EanId == _elkEanNonSmart4Reg2.EanId && mr.MarketEventDate == startReadingDate,
                "RegisterReading", "RegisterReading.TariffType",
                "RegisterReading.MeteringDirection"));
            var startReading = ConsumptionRepository.GetEntityByCondition<MeterReading>(
                mr => mr.EanId == _elkEanNonSmart4Reg2.EanId && mr.MarketEventDate == startReadingDate, "RegisterReading", "RegisterReading.TariffType",
                "RegisterReading.MeteringDirection");

            TestAssertionHelpers.AssertYearlyUsageCalculation(startReading, readingSetOfPreviousReading, _elkEanNonSmart4Reg2, response);
            TestAssertionHelpers.AssertYearlyUsageByMeterReadingResponse(registerNr, response, readingSetOfPreviousReading);
        }

        [Test]
        public void GetBalancesYearlyUsageByMeterReadingForRegister(
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg }, CcuMutationDate.AddDays(1));

            var readingSetOfPreviousReading = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanSmart2Reg, me, ReadingSource.Customer_Backoffice, OpenedWindow);
            var registerNr = GeneralHelpers.GetMeteringPointsByDate(_elkEanSmart2Reg.EanId, OpenedWindow).EnergyMeter.NrOfRegisters;

            var response = GeneralHelpers.GetYearlyUsageByMeterReadingResponse(readingSetOfPreviousReading);

            TestAssertionHelpers.AssertYearlyUsageByMeterReadingResponse(registerNr, response, readingSetOfPreviousReading);
        }

        [Test]
        public void GetYearlyUsageByMeterReading_EanWithoutProfileCategory([Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            MduProcessor.SendMdu(new List<EanInfo>() { _elkEanSmart4Reg }, DateTime.Now.AddDays(-5), hasProfileCategory: false);
            var meterReading =
                 MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, OpenedWindow, me,
                     ReadingSource.Customer_Backoffice);
            var readingSetInfo = GeneralHelpers.GetReadingSetInfoFromReading(meterReading);
            var response = new GetYearlyUsageByMeterReadingRequest { ReadingsSetInfo = readingSetInfo }
                .CallWith(MeterReadingClient.Proxy.GetYearlyUsageByMeterReading);

            var errorMessage = string.Format(PatternMessages.NoProfileCategoryForUsageByMeterReading,
                _elkEanSmart4Reg.EanId);
            response.Header.Message.Should().Contain(errorMessage);
        }

        [Test]
        public void GetYearlyUsageByMeterReading_WrongRegisterIsSet(
            [Values(MarketEvent.MoveIn)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg }, CcuMutationDate.AddDays(1));

            var readingSetOfPreviousReading = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanSmart2Reg, me, rs, OpenedWindow);
            readingSetOfPreviousReading.Readings.Add(new Reading
            {
                MeteringDirection = EnergyFlowDirectionCode.TLV,
                TariffType = EnergyTariffTypeCode.N,
                Value = 100
            });

            var registerNr = GeneralHelpers.GetMeteringPointsByDate(_elkEanSmart2Reg.EanId, OpenedWindow).EnergyMeter.NrOfRegisters;
            var response = GeneralHelpers.GetYearlyUsageByMeterReadingResponse(readingSetOfPreviousReading);
            var errorMessage = string.Format(PatternMessages.WrongRegisterSet, EnergyFlowDirectionCode.TLV.ToString(),
                EnergyTariffTypeCode.N.ToString());
            TestAssertionHelpers.AssertYearlyUsageByMeterReadingResponse(registerNr, response, readingSetOfPreviousReading);

            foreach (var register in response.ReadingsSetUsage.RegisterUsages)
            {
                if (register.MeteringDirection == EnergyFlowDirectionCode.TLV)
                    register.ErrorMessage.Should().Contain(errorMessage);

                response.ReadingsSetUsage.RegisterUsages.First().YearlyUsage.ToString().Should().NotBeEmpty();
            }
        }

        [Test]
        public void GetYearlyUsageByMeterReading_WhenMeterIdHasChanged(
            [Values(MarketEvent.Yearly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEan4RegNoReadings);
            const string meterId = "11223344";

            GeneralHelpers.CreateReadingWithNewMeterForEan(_elkEan4RegNoReadings, DateTime.Now.AddYears(-1), me, ReadingSource.Customer_EDSN, meterId);

            var readingSetInfoSentToSut = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEan4RegNoReadings, me, rs, OpenedWindow);
            var actualYearlyUsage = GeneralHelpers.GetYearlyUsageByMeterReadingResponse(readingSetInfoSentToSut);

            actualYearlyUsage.Header.Message.Should().Contain(PatternMessages.NotEnoughDataWhenExchangeOfMeter);
        }

        [Test]
        public void GetYearlyUsageByMeterReading_WhenExchangeOfMeter(
            [Values(MarketEvent.ExchangeOfMeter)] MarketEvent me,
        [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_gasEan);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEan }, CcuMutationDate.AddDays(1));
            const int meterId = 11223344;

            ConsumptionServiceHelpers.CreateEndOfMeterSupportReading(_gasEan, DateTime.Now.AddYears(-1), meterId);
            ConsumptionServiceHelpers.CreateBeginOfMeterSupportReading(_gasEan, DateTime.Now.AddDays(-130), meterId);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_gasEan, DateTime.Now.AddDays(-1), MarketEvent.Spontaneous, ReadingSource.Customer_Backoffice);

            var readingSetInfoSentToSut = ConsumptionServiceHelpers.CreateReadingSetInfo(_gasEan, me, rs, OpenedWindow);
            var actualYearlyUsage = GeneralHelpers.GetYearlyUsageByMeterReadingResponse(readingSetInfoSentToSut);

            actualYearlyUsage.Header.HasException.Should().BeFalse();
            actualYearlyUsage.ReadingsSetUsage.RegisterUsages.First().YearlyUsage.Should().NotBe(null);
        }

        [Test]
        public void GetYearlyUsageByMeterReading_NoReadingsFound(
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEan4RegNoReadings);
            var readingSetInfo = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEan4RegNoReadings, me, rs, OpenedWindow);
            var response = new GetYearlyUsageByMeterReadingRequest { ReadingsSetInfo = readingSetInfo }
                .CallWith(MeterReadingClient.Proxy.GetYearlyUsageByMeterReading);

            var errorMessage = string.Format(PatternMessages.NotEnoughHistoricalData, _elkEan4RegNoReadings.EanId);
            response.Header.Message.Should().Contain(errorMessage);
        }

        [Test]
        public void GetYearlyUsageByMeterReading_NotInDateRange(
            [Values(MarketEvent.Yearly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            GeneralHelpers.CreateReadingForEan(_elkEanNonSmart4Reg, OpenedWindow, me, rs);

            var readingSetInfo = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanNonSmart4Reg, me, rs, OpenedWindow);
            var response = new GetYearlyUsageByMeterReadingRequest { ReadingsSetInfo = readingSetInfo }
                .CallWith(MeterReadingClient.Proxy.GetYearlyUsageByMeterReading);

            var m = string.Format(PatternMessages.NotEnoughHistoricalData, _elkEanNonSmart4Reg.EanId);
            response.Header.Message.Should().Be(m);
        }

        [Test]
        public void GetYearlyUsageByMeterReading_InvalidMarketEvent(
            [Values(MarketEvent.Monthly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            GeneralHelpers.CreateReadingForEan(_elkEanNonSmart4Reg, OpenedWindow.AddMonths(-35), me, rs);

            var readingSetInfo = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanNonSmart4Reg, me, rs, OpenedWindow);
            var response = new GetYearlyUsageByMeterReadingRequest { ReadingsSetInfo = readingSetInfo }
                .CallWith(MeterReadingClient.Proxy.GetYearlyUsageByMeterReading);

            response.ReadingsSetUsage.RegisterUsages.FirstOrDefault().Should().NotBeNull();
        }

        [Test]
        public void GetYearlyUsageByMeterReading_InvalidMarketEvent_CallToTmr(
            [Values(MarketEvent.Monthly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart2Reg }, CcuMutationDate.AddDays(1));

            var readingSetInfo = ConsumptionServiceHelpers.CreateReadingSetInfo(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            var response = new GetYearlyUsageByMeterReadingRequest { ReadingsSetInfo = readingSetInfo }
                .CallWith(MeterReadingClient.Proxy.GetYearlyUsageByMeterReading);

            response.ReadingsSetUsage.RegisterUsages.FirstOrDefault().Should().NotBeNull();
        }
    }
}