﻿using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FakeMeterReadingService;
using FluentAssertions;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;
using StatusHistory = Repositories.Consumption.Sql.ConsumptionModels.StatusHistory;

namespace Autotests.Consumption.Tests
{
    [TestFixture]
    internal class SaveReadingTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;
        private EanInfo _elkEanTfSignal;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            _elkEanTfSignal = EanInfoProvider.GetRandomElkEan(false, 2, profileCode: EnergyUsageProfileCode.E1A);
            MduProcessor.SendMdu(new List<EanInfo>
            {
                _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan,
                _elkEanTfSignal
            });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>
                {_elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan}, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>
            {
                _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan,
                _elkEanTfSignal
            });
            MduProcessor.SendMdu(new List<EanInfo> {_elkEanTfSignal}, DateTime.Today.AddDays(-3),
                true);
        }

        [Test, Pairwise]
        public void SaveReadyToSendReading(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate.AddDays(1));

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, OpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void CheckResponsibleIsConsDomSaveSentToEdsnReading(
            [Values(ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart2Reg }, CcuMutationDate.AddDays(1));

            //save ReadyToSend reading
            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
           .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);

            //send reading to fake edsn
            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            var id = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId).Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == id && mr.StatusId == (int) ProcessStatus.SentToEdsn));
            var mrId = response.SavedMeterReadings.First().MeterReadingId;
            var user = ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == mrId).User;

            //assert reading responsible user field
            user.Should().Be(ConsDomResponsible);
        }

        [Test]
        public void SaveReadingWhenWindowIs30DaysInTheFuture(
            [Values(ReadingSource.Physical_Backoffice, ReadingSource.Customer_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.TwoMonthly, MarketEvent.MoveOut, MarketEvent.Yearly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var dateWindowMoreThan30Days = DateTime.Now.AddDays(+35);
            var readingsSet =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart2Reg, me, rs, dateWindowMoreThan30Days);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.Header.HasException.Should().BeTrue();
        }

        [Test, Pairwise]
        public void SaveTechExcpReading(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateTechExcpReadingSet(_elkEanSmart2Reg, me, rs, OpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.TechExcp);
        }

        [Test, Pairwise]
        public void SaveTechExcpReadingWindowNotOpened(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateTechExcpReadingSet(_elkEanNonSmart2Reg, me, rs, NotOpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.TechExcp);
        }

        [Test, Pairwise]
        public void SaveWaitForCustomerReadingWindowOpened(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEan.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer,
                response.SavedMeterReadings.First().MeterReadingId);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.WaitForCustomer);
        }

        [Test, Pairwise]
        public void SaveWaitForCustomerReadingWindowNotOpened(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs,
                    NotOpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer,
                response.SavedMeterReadings.First().MeterReadingId);
            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.WaitForCustomer);
        }

        [Test, Pairwise]
        public void SaveVerifyByAgentReading(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId, me, rs, OpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.VerifyByAgent);
        }

        [Test]
        public void CheckVerifyByAgentReadingWindowOpenedNotUpdatedByJob(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs, OpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.VerifyByAgent);
            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_DailyStatusCheckJob);
            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.VerifyByAgent);
        }

        [Test, Pairwise]
        public void SaveReadyToSendSmartMeterReadingWindowOpened(
            [Values(ReadingSource.P4_BackOffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs,
                    OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test, Order(1)]
        public void RejectPeriodicReadingIfPreviousBudgetEventReadingExists(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Estimated_Backoffice, ReadingSource.Customer_Mijn)]
            ReadingSource rs)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_gasEanSmart, OpenedWindow, MarketEvent.MoveIn, rs);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, MarketEvent.Yearly, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test]
        public void SendPeriodicReadingIfPreviousPeriodicReadingExists(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Estimated_Backoffice, ReadingSource.Customer_Mijn)]
            ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanSmart2Reg, OpenedWindow, MarketEvent.Spontaneous,
                rs);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart2Reg, MarketEvent.Yearly, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond, true)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            var mrId = response.SavedMeterReadings.First().MeterReadingId;
            Waiter.Wait(() =>
                ConsumptionRepository.EntityIsInDb<MeterReading>(m =>
                    m.Id == mrId && m.StatusId == (int) ProcessStatus.SentToEdsn));

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.SentToEdsn);
        }

        [Test, Order(2)]
        public void RejectPeriodicReadingIfPreviousEdsnEventReadingExists(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Estimated_Backoffice, ReadingSource.Customer_Mijn)]
            ReadingSource rs)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanNonSmart4Reg, OpenedWindow.AddDays(-1), MarketEvent.Spontaneous,
                rs);

            var readingsSet =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart4Reg, MarketEvent.MoveIn, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            var meterReadingId = ConsumptionServiceHelpers
                .GetMeterReadingById(response.SavedMeterReadings.First().MeterReadingId).MeterReading.MeterReadingId;

            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == meterReadingId && mr.StatusId == (int) ProcessStatus.SentToEdsn));

            var readingExternalReference = ConsumptionRepository
                .GetEntityByCondition<MeterReading>(m => m.Id == meterReadingId).ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(readingExternalReference);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == meterReadingId && mr.StatusId == (int) ProcessStatus.Accepted));

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart4Reg, MarketEvent.Yearly, rs, OpenedWindow);
            var response2 = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response2.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void SaveWaitToSendSmartMeterReadingWindowNotOpened(
            [Values(ReadingSource.P4_BackOffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.MoveOut, MarketEvent.Spontaneous)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs,
                    NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test, Pairwise]
        public void SaveVerifyByAgentReadingWindowNotOpened(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs,
                    NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.VerifyByAgent);
        }

        [Test, Pairwise]
        public void SaveVerifyByAgentReadingIfSkipValid(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);

            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet, true)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test, Pairwise]
        public void SaveWaitToSendReading(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart4Reg }, CcuMutationDate.AddDays(1));
            //RemoveAllNonHistoricalTestReadingsFromDb(_elkEanNonSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart4Reg, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test, Pairwise]
        public void SaveTooLateReading(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Switch,
                MarketEvent.Spontaneous, MarketEvent.TwoMonthly, MarketEvent.Yearly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            var readingsSet =
                ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, ClosedWindow.AddDays(-30));
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet, true)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.TooLate);
        }

        [Test, Pairwise]
        public void RejectPreviousReadyToSendAfterReadyToSend(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, OpenedWindow);
            var saveRejectedReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond);
            saveRejectedReadingRequest.MeterReadingsSet
                    .First().Readings.First().Value =
                saveRejectedReadingRequest.MeterReadingsSet.First().Readings.First().Value + 10;
            saveRejectedReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousTechExcpAfterReadyToSend(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart2Reg }, CcuMutationDate.AddDays(1));

            var readingsSet =
                ConsumptionServiceHelpers.CreateTechExcpReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousVerifyByAgentAfterWaitToSend(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs,
                    NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet, true)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart4Reg, me, rs, NotOpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond, true)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var mrId = response.SavedMeterReadings.First().MeterReadingId;
            var actualStatus = ConsumptionRepository.GetEntityByCondition<MeterReading>(m => m.Id == mrId).StatusId;

            actualStatus.Should().Be((int) ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousWaitToSendAfterWaitToSend(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate.AddDays(1));

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            var readingsSetSecond = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, NotOpenedWindow);

            readingsSetSecond.Readings.ForEach(re => re.Value = readingsSetSecond.Readings
                .First(r => r.MeteringDirection == re.MeteringDirection && r.TariffType == re.TariffType).Value + 3);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousVerifyByAgentAfterVerifyByAgent(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var marketEvent = MarketEvent.TwoMonthly;

            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId, marketEvent, rs,
                    OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            Waiter.Wait(() => ConsumptionServiceHelpers.GetMeterReadingStatusById(
                response.SavedMeterReadings.First()) == ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousVerifyByAgentAfterVerifyByAgentForSmartMeters(
            [Values(ReadingSource.Customer_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveOut, MarketEvent.Spontaneous,
                MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart2Reg }, CcuMutationDate.AddDays(1));

            var lastReadingFromDb = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId,
                    "RegisterReading");

            var saveReadingSet = GeneralHelpers.GetReadingSetFromSqlMeterReading(lastReadingFromDb);
            saveReadingSet.ReadingSource = rs;
            saveReadingSet.MarketEvent = me;
            saveReadingSet.DossierId = _elkEanNonSmart2Reg.DossierId;
            saveReadingSet.MarketEventDate = OpenedWindow;
            var saveReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(saveReadingSet, true);

            RequestObjectHelpers.SetUpFirstSaveMeterReadingRequest(saveReadingRequest);
            var saveReadingResponse = saveReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId,
                    "RegisterReading");

            var saveReadingSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            var saveRejectedReadingRequestForSmart =
                ConsumptionServiceHelpers.SaveMeterReadingRequest(saveReadingSetSecond, true);
            RequestObjectHelpers.SetUpSaveRejectedReadingRequestForSmart(saveRejectedReadingRequestForSmart);
            saveRejectedReadingRequestForSmart.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(saveReadingResponse.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousReadyToSendAfterVerifyByAgent(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId, me, rs, OpenedWindow);
            var saveRejectedReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond);
            saveRejectedReadingRequest.MeterReadingsSet
                    .First().Readings.First().Value =
                saveRejectedReadingRequest.MeterReadingsSet.First().Readings.First().Value + 10;
            saveRejectedReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousWaitForCustomerAfterVerifyByAgent(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart2Reg.EanId, me,
                ReadingSource.Customer_Mijn, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer,
                response.SavedMeterReadings.First().MeterReadingId);
            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart2Reg.EanId, me, rs,
                    OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void RejectPreviousWaitForCustomerAfterReadyToSend(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);

            var readingsSet =
                 ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart2Reg.EanId, me, rs,
                    OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer,
                response.SavedMeterReadings.First().MeterReadingId);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test]
        public void SaveReadingWithoutDossier(
            [Values(ReadingSource.Customer_Backoffice, ReadingSource.Customer_Mijn, ReadingSource.Settled_Backoffice,
                ReadingSource.Estimated_Backoffice, ReadingSource.P4_BackOffice, ReadingSource.Physical_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Switch)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var readingsSet =
                ConsumptionServiceHelpers.CreateReadingsSetWithoutDossier(_elkEanSmart2Reg, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            var errorMessage = string.Format(PatternMessages.NoDossierForReading, readingsSet.EanId,
                readingsSet.MarketEvent.ToString());
            response.Header.Message.Should().Be(errorMessage);
        }

        [Test, Pairwise]
        public void SaveSmartMeterReadingWithP4Source(
            [Values(ReadingSource.P4_BackOffice)] ReadingSource rs,
            [Values(MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Switch)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart2Reg.EanId, me, OpenedWindow);
            var estimateDataValue = estimateResponse.EstimationData.FirstOrDefault();
            var meterReading =
                GeneralHelpers.GetDomainMeterReadingFromEstimationResult(estimateDataValue, null, rs, me, OpenedWindow);
            meterReading.RegisterReadings.ForEach(re => re.Value = re.Value + 6000);

            var readingsSet = GeneralHelpers.GetReadingSetFromReading(meterReading);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void SaveReadyToSendReadingResponsibleIsUserName(
            [Values(ReadingSource.Customer_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.EndOfSupply)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() {_gasEanSmart}, CcuMutationDate.AddDays(1));

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, OpenedWindow);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
            var mrId = response.SavedMeterReadings.First().MeterReadingId;

            ConsumptionRepository.GetLastEntity<StatusHistory, DateTime>(s =>
                s.CreatedOn, s => s.MeterReadingId == mrId).User.Should().Be(UserNameResponsible);
        }

        [Test]
        public void CheckResponsibleIsTmrService()
        {
            var tmrServiceUser = "TmrService";
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg }, CcuMutationDate.AddDays(1));
            
            var meterReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart2Reg.EanId,
                    "RegisterReading");

            ConsumptionRepository.GetLastEntity<StatusHistory, DateTime>(s =>
                s.CreatedOn, s => s.MeterReadingId == meterReadingId.Id).User.Should().Be(tmrServiceUser);
        }

        [Test]
        public void CheckResponsibleIsConsumptionServiceRejectPreviousWaitToSend(
            [Values(ReadingSource.Customer_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart4Reg }, CcuMutationDate.AddDays(1));

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart4Reg, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart4Reg, me, rs, NotOpenedWindow);
            readingsSetSecond.Readings.First().Value = readingsSetSecond.Readings.First().Value + 10;
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var mrId = response.SavedMeterReadings.First().MeterReadingId;
            ConsumptionRepository.GetLastEntity<StatusHistory, DateTime>(s =>
                s.CreatedOn, s => s.MeterReadingId == mrId).User.Should().Be(ConsDomResponsible);
        }

        [Test]
        public void CheckResponsibleIsConsumptionServiceRejectPreviousReadyToSendAfterTechExcp(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart4Reg }, CcuMutationDate.AddDays(1));
            var readingsSet =
                ConsumptionServiceHelpers.CreateTechExcpReadingSet(_elkEanNonSmart4Reg, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var readingsSetSecond =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart4Reg, me, rs, OpenedWindow);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            var mrId = response.SavedMeterReadings.First().MeterReadingId;
            ConsumptionRepository.GetLastEntity<StatusHistory, DateTime>(s =>
                s.CreatedOn, s => s.MeterReadingId == mrId).User.Should().Be(ConsDomResponsible);
        }

        [Test]
        public void SaveMaxRegisterValue(
            [Values(ReadingSource.Customer_Backoffice)]
            ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            var readingDate = DateTime.Now.Date.AddDays(-1);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, readingDate, me,
                ReadingSource.Customer_Backoffice);
            meterReading.RegisterReadings.ForEach(re => re.Value = 99999);
            var meterPoint = meterReading.To();

            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(meterPoint, MeterReadingStatus.Accepted);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.EanId == _elkEanNonSmart2Reg.EanId && mr.MarketEventDate == readingDate && mr.StatusId == (int) ProcessStatus.Accepted));

            var readingSet =
                ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, readingDate.Date.AddDays(+1));
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void SaveExchangeOfMeter_ReadingInVerifyByAgentStatus()
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            const string newMeterId = "10293847";

            var reading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart4Reg, OpenedWindow,
                MarketEvent.BeginOfMeterSupport, ReadingSource.Customer_EDSN);
            reading.EdsnMeterId = newMeterId;
            reading.RegisterReadings.ForEach(rr => rr.Value = 6500);
            var mrDom = reading.To();
            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(mrDom, MeterReadingStatus.Accepted);

            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(mr =>
                mr.EanId == _elkEanNonSmart4Reg.EanId && mr.MarketEventId == (int) MarketEvent.BeginOfMeterSupport));
            var beginOfMeterSupportReading =
                ConsumptionRepository.GetLastEntity<MeterReading, DateTime>(mr => mr.MarketEventDate,
                    mr => mr.EanId == _elkEanNonSmart4Reg.EanId);

            beginOfMeterSupportReading.StatusId.Should().Be((int) ProcessStatus.VerifyByAgent);
        }

        [Test]
        public void SaveBeginOfMeterReading_AcceptedReading()
        {
            const string newMeterId = "10293847";
            CleanMeterReadingsForEan(_gasEan);

            var reading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, OpenedWindow,
                MarketEvent.BeginOfMeterSupport, ReadingSource.Customer_EDSN);
            reading.EdsnMeterId = newMeterId;
            reading.RegisterReadings.ForEach(rr => rr.Value = new Random().Next(0, 4999));
            var mrDom = reading.To();
            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(mrDom, MeterReadingStatus.Accepted);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);

            Waiter.Wait(() => ConsumptionRepository.GetLastEntity<MeterReading, DateTime>(mr => mr.MarketEventDate,
                mr => mr.EanId == _gasEan.EanId && mr.StatusId == (int) ProcessStatus.Accepted));
        }

        [Test]
        public void SaveBeginOfMeterReading_VerifyByAgentStatus()
        {
            const string newMeterId = "10293847";
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);

            var reading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, OpenedWindow,
                MarketEvent.BeginOfMeterSupport, ReadingSource.Customer_EDSN);
            reading.EdsnMeterId = newMeterId;
            reading.RegisterReadings.ForEach(rr => rr.Value = new Random().Next(5000, 6000));
            var mrDom = reading.To();
            ConsumptionServiceHelpers.ProcessReadingInFakeEdsn(mrDom, MeterReadingStatus.Accepted);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetAcceptedMeterReadingsFake);

            Waiter.Wait(() => ConsumptionRepository.GetEntityByCondition<MeterReading>(mr =>
                mr.EanId == _elkEanNonSmart2Reg.EanId && mr.MarketEventId == (int) MarketEvent.BeginOfMeterSupport &&
                mr.StatusId == (int) ProcessStatus.VerifyByAgent));
        }

        [Test]
        public void SaveTfSignalReading_NRegisterEqualsSum_ReadyToSendStatus(
            [Values(MarketEvent.Yearly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs)
        {
            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(_elkEanTfSignal, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //Assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == _elkEanTfSignal.EanId && mp.IsTfSignal == true), 15);

            //save reading
            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanTfSignal, me, rs, OpenedWindow, true);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void SaveTfSignalReading_NRegisterEqualsSum_VerifyByAgent([Values(MarketEvent.Yearly)] MarketEvent me,
            [Values(ReadingSource.Customer_Backoffice)]
            ReadingSource rs)
        {
            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(_elkEanTfSignal, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == _elkEanTfSignal.EanId && mp.IsTfSignal == true), 3);

            //save reading
            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanTfSignal.EanId, me, rs, OpenedWindow,
                    true);

            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.VerifyByAgent);
        }
    }
}