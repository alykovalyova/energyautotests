﻿using Autotests.Core.Helpers;
using Repositories.Consumption.Sql.ConsumptionModels;
using NUnit.Framework;
using Autotests.Core.TestDataProviders;
using FluentAssertions;
using Nuts.InterDom.Model.Core.Enums;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class SettlementsTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo>
                {_elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan});
            CcuProcessor.SendMoveInCcu(new List<EanInfo>
                {_elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan}, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>
                {_elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan});
        }

        [Test]
        public void StartSettlementOutOfWindow_Smoke(
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, settlementWindow, me,
                ReadingSource.Customer_EDSN);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            var response = ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);

            response.Header.HasException.Should().BeFalse();
        }

        [Test, Pairwise]
        public void AcceptSettlementStartedByOtherParty(
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.Spontaneous,
                MarketEvent.MoveOut, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading =
                MeteringPointsServiceHelpers.GetMeterReading(_gasEan, settlementWindow, me,
                    ReadingSource.Customer_Backoffice);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByOtherParty(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Settled), 200);

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId,
                    "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.CheckSettlement), 200);

            ConsumptionServiceHelpers.AnswerSettlementByBudget(newReadingId, true);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Rejected), 200);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.Accepted), 200);
        }

        [Test]
        public void CheckResponsibleAcceptSettlementStartedByOtherParty(
            [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart2Reg, settlementWindow, me,
                ReadingSource.Customer_Backoffice);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByOtherParty(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Settled));

            var newReadingId = ConsumptionRepository.GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn,
                m => m.EanId == _elkEanSmart2Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.CheckSettlement));

            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == settledReadingId)
                .User.Should().Be(UserNameResponsible);
            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == newReadingId).User
                .Should().Be(UserNameResponsible);

            ConsumptionServiceHelpers.AnswerSettlementByBudget(newReadingId, true);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Rejected));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.Accepted));

            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == settledReadingId)
                .User.Should().Be(UserNameResponsible);
            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == newReadingId).User
                .Should().Be(UserNameResponsible);
        }

        [Test, Pairwise]
        public void RejectSettlementStartedByOtherParty(
            [Values(MarketEvent.Yearly, MarketEvent.MoveOut, MarketEvent.MoveIn,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, settlementWindow, me,
                ReadingSource.Customer_Backoffice);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByOtherParty(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Settled));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId,
                    "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.CheckSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByBudget(newReadingId, false);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Accepted));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.Rejected));
        }

        [Test, Pairwise]
        public void AcceptSettlementStartedByBudget(
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading =
                MeteringPointsServiceHelpers.GetMeterReading(_gasEan, settlementWindow, me,
                    ReadingSource.Customer_EDSN);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);
            
            ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.InSettlement));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId,
                    "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.WaitForSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByOtherParty(newReadingId, true);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Rejected));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.Accepted));
        }

        [Test, Pairwise]
        public void RejectSettlementStartedByBudget(
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)]
            MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, settlementWindow, me,
                ReadingSource.Customer_EDSN);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.InSettlement));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId,
                    "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.WaitForSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByOtherParty(newReadingId, false);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Accepted));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.Rejected));
        }

        [Test]
        public void CheckResponsibleRejectSettlementStartedByBudget(
            [Values(MarketEvent.Switch)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart2Reg, settlementWindow, me,
                ReadingSource.Customer_EDSN);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.InSettlement));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart2Reg.EanId,
                    "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.WaitForSettlement));

            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == settledReadingId)
                .User.Should().Be(UserNameResponsible);
            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == newReadingId).User
                .Should().Be(UserNameResponsible);

            ConsumptionServiceHelpers.AnswerSettlementByOtherParty(newReadingId, false);

            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int) ProcessStatus.Accepted));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int) ProcessStatus.Rejected));

            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == settledReadingId)
                .User.Should().Be(UserNameResponsible);
            ConsumptionRepository
                .GetLastEntity<StatusHistory, DateTime>(s => s.CreatedOn, s => s.MeterReadingId == newReadingId).User
                .Should().Be(UserNameResponsible);
        }
    }
}