﻿using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionSmoke)]
    internal class SmokeConsumptionTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _gasEan;
        private EanInfo _elkEanWithoutReadings;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanWithoutReadings = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan, _elkEanWithoutReadings });
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan, _elkEanWithoutReadings }, CcuMutationDate);
        }

        [Test]
        public void AcceptDisputeStartedByBudget_Smoke()
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, OpenedWindow, MarketEvent.Yearly, ReadingSource.Customer_EDSN);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, MarketEvent.Yearly);

            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);
            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart4Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForDispute), 200);

            TestAssertionHelpers.ValidateInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart4Reg.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.AcceptDisputeByFakeEdsn(newReadingExternalReference, disputedReadingId);
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(newReadingExternalReference);

            TestAssertionHelpers.ValidateRejectedAndAcceptedStatuses(disputedReadingId, _elkEanSmart4Reg);
        }

        [Test, Order(1)]
        public void RejectDisputeStartedByBudget_Smoke([Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);

            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart2Reg, OpenedWindow, me, ReadingSource.Customer_EDSN);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, me);

            //start dispute on created reading and wait for WaitForDispute status
            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);
            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForDispute));

            TestAssertionHelpers.ValidateInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.RejectDisputeByFakeEdsn(newReadingExternalReference, newReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int)ProcessStatus.Accepted));

        }

        [Test]
        public void AcceptDisputeStartedByOtherParty_Smoke([Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var date = DateTime.Now.AddDays(-16);
            var oldMeterReading = ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanNonSmart4Reg, date, me, ReadingSource.Customer_Backoffice);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanNonSmart4Reg, date, me, ReadingSource.Customer_Backoffice);
            meterReading.RegisterReadings.ForEach(re => re.Value = oldMeterReading.RegisterReadings
                    .Find(r => r.MeteringDirection == EnergyFlowDirectionCode.LVR && r.TariffType == EnergyTariffTypeCode.N).Value + 5);
            var readingInfo = meterReading.To();
            var disputedReadingInfo = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart4Reg.EanId, "RegisterReading");

            GeneralHelpers.SendDisputeFromOtherPartyAndWaitStatus(_elkEanNonSmart4Reg, readingInfo, date, disputedReadingInfo);
            var newReading = TestAssertionHelpers.ValidateCheckDisputeStatus(_elkEanNonSmart4Reg);
            GeneralHelpers.AnswerDisputeByBudgetAndValidateRejectedStatus(newReading, disputedReadingInfo);

            var acceptedReading = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanNonSmart4Reg.EanId, "RegisterReading");
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(acceptedReading.ExternalReference);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == acceptedReading.Id && mr.StatusId == (int)ProcessStatus.Accepted));
        }

        [Test]
        public void RejectDisputeStartedByOtherParty__Smoke(
            [Values(ReadingSource.Settled_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            //RemoveAllTestReadingsFromDbForEan(new List<EanInfo>() { _elkEanSmart2Reg });

            var date = DateTime.Now.AddDays(-16);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanSmart2Reg, date, me, rs);

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart2Reg, date, me, rs);
            var readingInfo = meterReading.To();
            var disputedReadingInfo = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart2Reg.EanId, "RegisterReading");

            GeneralHelpers.SendDisputeFromOtherPartyAndWaitStatus(_elkEanSmart2Reg, readingInfo, date, disputedReadingInfo);
            var newReading = TestAssertionHelpers.ValidateCheckDisputeStatus(_elkEanSmart2Reg);
            GeneralHelpers.AnswerDisputeByBudgetAndValidateAcceptedStatus(newReading, disputedReadingInfo);

            var rejectedReading = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart2Reg.EanId, "RegisterReading");
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == rejectedReading.Id && mr.StatusId == (int)ProcessStatus.Rejected));
        }

        [Test]
        public void EstimateForTheFutureDate_Smoke()
        {
            RemoveAllNonHistoricalTestReadingsFromDb(_gasEanSmart);

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_gasEanSmart.EanId, MarketEvent.Spontaneous, NotOpenedWindow);

            estimationResponse.EstimationData.First().EstimationRegisters.ForEach(re =>
                re.Estimation.EstimationMin.Should().NotBe(re.Estimation.EstimationMax));
        }

        [Test]
        public void EstimateForTodayDateIfAcceptedReadingIsToday_Smoke()
        {
            var marketEvent = MarketEvent.Spontaneous;

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanWithoutReadings, OpenedWindow, marketEvent, ReadingSource.Customer_EDSN);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, marketEvent);

            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanWithoutReadings.EanId, marketEvent, OpenedWindow);

            foreach (var register in estimationResponse.EstimationData.First().EstimationRegisters)
            {
                register.Estimation.Estimation.Should().Be(0);
                register.Estimation.EstimationMin.Should().Be(0);
                register.Estimation.EstimationMax.Should().Be(0);
            }
        }

        [Test]
        public void GetYearlyUsage_ValidCase()
        {
            var marketEvent = MarketEvent.Yearly;
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);

            var endReadingDate = DateTime.Now.AddYears(-2);
            var startReadingDate = endReadingDate.AddDays(-400);
            GeneralHelpers.CreateReadingForEan(_elkEanNonSmart2Reg, startReadingDate, marketEvent, ReadingSource.Customer_Backoffice);
            var startReading =
                ConsumptionRepository.GetEntityByCondition<MeterReading>(mr => mr.EanId == _elkEanNonSmart2Reg.EanId,
                    "RegisterReading");

            GeneralHelpers.CreateReadingForEan(_elkEanNonSmart2Reg, endReadingDate, marketEvent, rs: ReadingSource.Customer_Backoffice);
            var endReading =
                ConsumptionRepository.GetLastEntity<MeterReading, DateTime>(mr => mr.MarketEventDate,
                    mr => mr.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading");
            YearlyUsageCalculator.GetYearlyUsageWhenEndAndStartReadingsExist(_elkEanNonSmart2Reg, endReading, startReading);
        }

        [Test]
        public void CheckSourceToSkipIsNotReturned_Smoke()
        {
            var rs = ReadingSource.Estimated_TMR;
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate.AddDays(1));

            var date = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEanSmart.EanId, "RegisterReading").MarketEventDate;
            var readings = ConsumptionServiceHelpers.GetLastAcceptedReading(_gasEanSmart.EanId, date, rs).MeterReadings;

            readings.ForEach(re => re.ReadingSource.Should().NotBe(rs));
        }

        [Test]
        public void CheckReadingsEarlierThanSetDateAreNotReturned_Smoke()
        {
            var rs = ReadingSource.Customer_Mijn;
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var readingDate = DateTime.Now.AddMonths(-10);
            var requestDate = DateTime.Now;

            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, readingDate, MarketEvent.Yearly, rs);
            ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, MarketEvent.Yearly);
            var lastAcceptedReadingResponse = ConsumptionServiceHelpers.GetLastAcceptedReading(_elkEanSmart4Reg.EanId, requestDate, rs);

            lastAcceptedReadingResponse.MeterReadings.Count.Should().Be(0);
        }

        [Test]
        public void SaveReadyToSendReading_Smoke()
        {
            var rs = ReadingSource.Customer_Mijn;
            var me = MarketEvent.Spontaneous;
            CleanMeterReadingsForEan(_gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate.AddDays(1));

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart2Reg, me, rs, OpenedWindow);
            var saveReadingResponse = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            saveReadingResponse.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void SaveVerifyByAgentReading_Smoke()
        {
            var me = MarketEvent.MoveIn;
            var rs = ReadingSource.Customer_Mijn;
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);

            var saveReadingResponse = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            saveReadingResponse.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.VerifyByAgent);
        }

        [Test]
        public void SaveTechExcpReading_Smoke()
        {
            var me = MarketEvent.Spontaneous;
            var rs = ReadingSource.Customer_Backoffice;
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateTechExcpReadingSet(_elkEanNonSmart4Reg, me, rs, OpenedWindow);

            var saveReadingResponse = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            saveReadingResponse.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.TechExcp);
        }

        [Test]
        public void SaveWaitForCustomerReadingWindowNotOpened_Smoke()
        {
            var me = MarketEvent.Yearly;
            var rs = ReadingSource.Customer_Mijn;
            CleanMeterReadingsForEan(_gasEan);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEan.EanId, me, rs, NotOpenedWindow);
            var saveReadingResponse = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, saveReadingResponse.SavedMeterReadings.First().MeterReadingId);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(saveReadingResponse.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.WaitForCustomer);
        }

        [Test]
        public void SaveWaitForCustomerReadingWindowOpened_Smoke()
        {
            var me = MarketEvent.Yearly;
            var rs = ReadingSource.Customer_Mijn;
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);
            var saveReadingResponse = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, saveReadingResponse.SavedMeterReadings.First().MeterReadingId);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(saveReadingResponse.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.WaitForCustomer);
        }

        [Test]
        public void RejectPreviousWaitToSendAfterWaitToSend_Smoke()
        {
            var me = MarketEvent.Switch;
            var rs = ReadingSource.Estimated_Backoffice;

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart2Reg, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            var readingsSetSecond = ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart2Reg, me, rs, NotOpenedWindow);
            readingsSetSecond.Readings.ForEach(re => re.Value = readingsSetSecond.Readings
                                                                    .First(r => r.MeteringDirection == re.MeteringDirection && r.TariffType == re.TariffType).Value + 3);
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetSecond)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should()
                .Be(ProcessStatus.Rejected);
        }

        [Test]
        public void SaveReadyToSendSmartMeterReadingWindowOpened_Smoke()
        {
            var me = MarketEvent.MoveIn;
            var rs = ReadingSource.P4_BackOffice;
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void SaveVerifyByAgentReadingIfSkipValid_Smoke(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet, true)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void SaveWaitToSendReading_Smoke(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart2Reg }, CcuMutationDate.AddDays(1));

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            response.SavedMeterReadings.First().NewStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test]
        public void AcceptSettlementStartedByOtherParty_Smoke(
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            RemoveAllNonHistoricalTestReadingsFromDb(_gasEanSmart);
            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEanSmart, settlementWindow, me, ReadingSource.Customer_Backoffice);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByOtherParty(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.Settled));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEanSmart.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.CheckSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByBudget(newReadingId, true);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.Rejected));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.Accepted));
        }

        [Test]
        public void RejectSettlementStartedByOtherParty_Smoke(
            [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart4Reg, settlementWindow, me, ReadingSource.Customer_Backoffice);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByOtherParty(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.Settled));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart4Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.CheckSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByBudget(newReadingId, false);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.Accepted));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.Rejected));
        }

        [Test]
        public void AcceptSettlementStartedByBudget_Smoke(
            [Values(MarketEvent.Switch)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);

            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart2Reg, settlementWindow, me, ReadingSource.Customer_EDSN);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.InSettlement));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart2Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByOtherParty(newReadingId, true);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.Rejected));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.Accepted));
        }

        [Test]
        public void RejectSettlementStartedByBudget_Smoke(
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);

            var settlementWindow = OpenedWindow.AddMonths(-4);
            var meterReading = MeteringPointsServiceHelpers.GetMeterReading(_elkEanSmart2Reg, settlementWindow, me, ReadingSource.Customer_EDSN);
            var settledReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(meterReading, me);

            ConsumptionServiceHelpers.StartSettlementByBudget(meterReading, settledReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.InSettlement));

            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _elkEanSmart2Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForSettlement));

            ConsumptionServiceHelpers.AnswerSettlementByOtherParty(newReadingId, false);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == settledReadingId && mr.StatusId == (int)ProcessStatus.Accepted));
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.Rejected));
        }

        [Test]
        public void UpdateFromReadyToSendToWaitForCustomerOpenedWindow_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
          [Values(MarketEvent.MoveOut)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitForCustomer);
        }

        [Test]
        public void UpdateFromVerifyByAgentToWaitForCustomerOpenedWindow_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var readingsSet =
                ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer,
                response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus =
                ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitForCustomer);
        }

        [Test]
        public void UpdateFromVerifyByAgentToReadyToSendOpenedWindow_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.EndOfSupply)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEan.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.ReadyToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void UpdateFromVerifyByAgentToRejectedOpenedWindow_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.Rejected, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.Rejected);
        }

        [Test]
        public void UpdateFromVerifyByAgentToWaitToSendNotOpenedWindow_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test]
        public void UpdateFromWaitForCustomerToReadyToSendOpenedWindow_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.MoveIn)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            GeneralHelpers.UpdateReadingById(ProcessStatus.ReadyToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test]
        public void UpdateFromWaitForCustomerToWaitToSend_Smoke(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.MoveOut)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should().Be(ProcessStatus.WaitForCustomer);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test]
        public void UpdateFromTooLateToRejected_Smoke(
            [Values(ReadingSource.Customer_Backoffice)] ReadingSource rs,
            [Values(MarketEvent.EndOfSupply)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);

            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, DateTime.Now.AddDays(-500));
            ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_DailyStatusCheckJob);
            var id = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(
                    m => m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == id && mr.StatusId == (int)ProcessStatus.Rejected));

            ConsumptionRepository.GetLastEntity<MeterReading, DateTime>(m =>
                    m.CreatedOn, m => m.EanId == _elkEanNonSmart2Reg.EanId, "RegisterReading").StatusId.Should()
                .Be((int)ProcessStatus.Rejected);
        }

        [Test]
        public void ValidateCorrectReading_Smoke()
        {
            var me = MarketEvent.MoveOut;
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart4Reg }, CcuMutationDate.AddDays(1));

            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanNonSmart4Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(Nuts.Consumption.Model.Core.ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateIncorrectReading_Smoke()
        {
            var me = MarketEvent.Settlement;
            CleanMeterReadingsForEan(_gasEan);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEan }, CcuMutationDate.AddDays(1));

            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_gasEan.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            estimationResult.EstimationRegisters.ForEach(re => re.Estimation.Estimation = re.Estimation.EstimationMin - 2);
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(Nuts.Consumption.Model.Core.ValidationStatus.InvalidFunctionality);
            }
        }

        [Test]
        public void ValidateSmartMeterReading_Smoke()
        {
            var me = MarketEvent.Settlement;

            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanNonSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanNonSmart2Reg }, CcuMutationDate.AddDays(1));

            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanNonSmart2Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(Nuts.Consumption.Model.Core.ValidationStatus.Valid);
            }
        }

    }
}
