﻿using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FakeMeterReadingService;
using NUnit.Framework;
using MeteringPointDbModels = Repositories.MeteringPoint.Sql.MeteringPointModels;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class TfSignalMduTests : BaseConsDomTest
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            MduProcessor.SendMdu(TestDataPreparation.TestEans.Values.ToList());
            CcuProcessor.SendMoveInCcu(TestDataPreparation.TestEans.Values.ToList(), CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(TestDataPreparation.TestEans.Values.ToList());   
        }
       
        [Test]
        public void TfSignalMdu_InvalidEan([Values(TestDataPreparation.MeterType.ElkEanTfSignalOneRegister,
            TestDataPreparation.MeterType.ElkEanTfSignalSmartMeter, TestDataPreparation.MeterType.ElkEanTfSignalInvalidProfileCode)] TestDataPreparation.MeterType meterType)
        {
            var ean = TestDataPreparation.TestEans[meterType];
            var mutationDate = DateTime.Today.AddDays(-3);
            MduProcessor.SendMdu(new List<EanInfo>() { ean }, mutationDate, tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(ean, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntitiesByCondition<MeteringPointDbModels.MeteringPointHistory>(
                mp => mp.EanId == ean.EanId).Count == 3);
            Waiter.Wait(() => MeteringPointDb.GetLastEntity<MeteringPointDbModels.MeteringPointHistory, DateTime>(
                mp => mp.CreatedOn, mp => mp.EanId == ean.EanId).IsTfSignal == false);
        }

        [Test]
        public void TfSignalMdu_InvalidReadingMethod()
        {
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];
            var mutationDate = DateTime.Today.AddDays(-3);
            MduProcessor.SendMdu(new List<EanInfo>() { ean }, mutationDate, tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(ean, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item001);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntitiesByCondition<MeteringPointDbModels.MeteringPointHistory>(
                mp => mp.EanId == ean.EanId).Count == 3, 15);
            Waiter.Wait(() => MeteringPointDb.GetLastEntity<MeteringPointDbModels.MeteringPointHistory, DateTime>(
                mp => mp.CreatedOn, mp => mp.EanId == ean.EanId).IsTfSignal == false, 15);
        }
    }
}
