﻿using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;
using DisputeStatus = Nuts.FakeEdsn.Contracts.Enums.MeterReadingDisputeStatus;
using MeterReading = Repositories.Consumption.Sql.ConsumptionModels.MeterReading;
using StatusHistory = Repositories.Consumption.Sql.ConsumptionModels.StatusHistory;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class UpdateReadingTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private EanInfo _elkEanSmart4Reg;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
        }

        //private static readonly string _gasEanId = ConnectionsProvider.EanInfoForTestCases[ConnectionsProvider.MeterType.GasMeter].EanId;
        private MeterReading GetLastMeterReading(string ean)
        {
            return ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(
                    m => m.CreatedOn,
                    m => m.EanId == ean, "RegisterReading");
        }

        [Test, Order(1)]
        public void UpdateFromVerifyByAgentToReadyToSend_ForAddressWithTwoConnections_Smoke(
             [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
             [Values(MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan, _elkEanSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEan, _elkEanSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEan, _elkEanSmart4Reg }, CcuMutationDate.AddDays(1));

            const int processId = 2233;

            //creation of readings sets for Gas address and for Elk address
            var readingsSetGasMeter = ConsumptionServiceHelpers.CreateReadingSet(_gasEan,
                me, rs, OpenedWindow);
            var readingsSetElkMeter = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId,
                me, rs, OpenedWindow);

            //assign to elk reading set the same dossierId as gas readings set has
            readingsSetElkMeter.DossierId = readingsSetGasMeter.DossierId;

            //assigning the same processId for both address             
            readingsSetElkMeter.ProcessId = processId;
            readingsSetGasMeter.ProcessId = processId;
            var saveReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetElkMeter);
            saveReadingRequest.MeterReadingsSet.Add(readingsSetGasMeter);
            saveReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            //assert statuses of saved readings sets
            var gasLastReading = GetLastMeterReading(_gasEan.EanId);
            gasLastReading.StatusId.Should().Be((int)ProcessStatus.ReadyToSend);
            var elkLastReading =GetLastMeterReading(_elkEanSmart4Reg.EanId);
            elkLastReading.StatusId.Should().Be((int)ProcessStatus.VerifyByAgent);

            //update VerifyByAgent reading to ReadyToSend status
            GeneralHelpers.UpdateReadingById(ProcessStatus.ReadyToSend, elkLastReading.Id);

            //call job to speed up sending of readings to FakeEdsn
            var gasLast = GetLastMeterReading(_gasEan.EanId).Id;
            var elkLast = GetLastMeterReading(_elkEanSmart4Reg.EanId).Id;
            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == gasLast && mr.StatusId == (int)ProcessStatus.SentToEdsn
                      || mr.Id == elkLast && mr.StatusId == (int)ProcessStatus.SentToEdsn));

            //assert correct statuses
            GetLastMeterReading(_gasEan.EanId).StatusId.Should().Be((int)ProcessStatus.SentToEdsn);
            GetLastMeterReading(_elkEanSmart4Reg.EanId).StatusId.Should().Be((int)ProcessStatus.SentToEdsn);
        }

        [Test]
        public void UpdateVerifyByAgentToWaitForCustomer_ForAddressWithTwoConnections_Smoke(
           [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
           [Values(MarketEvent.MoveOut)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart, _elkEanSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart, _elkEanSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart, _elkEanSmart4Reg }, CcuMutationDate.AddDays(1));

            const int processId = 1122;
            var readingsSetGasMeter = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId,
                me, rs, OpenedWindow);
            var readingsSetElkMeter = ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart4Reg, me, rs, NotOpenedWindow);

            //assign to elk reading set the same dossierId as gas readings set has
            readingsSetElkMeter.DossierId = readingsSetGasMeter.DossierId;

            //assigning the same processId for both address             
            readingsSetElkMeter.ProcessId = processId;
            readingsSetGasMeter.ProcessId = processId;

            var saveReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetElkMeter);
            saveReadingRequest.MeterReadingsSet.Add(readingsSetGasMeter);
            saveReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            //assert statuses of saved readings sets
            var gasReading = GetLastMeterReading(_gasEanSmart.EanId);
            gasReading.StatusId.Should().Be((int)ProcessStatus.VerifyByAgent);
            var elkReading = GetLastMeterReading(_elkEanSmart4Reg.EanId);
            elkReading.StatusId.Should().Be((int)ProcessStatus.WaitToSend);

            //update VerifyByAgent reading to ReadyToSend status
            RequestObjectHelpers.CreateUpdateRequest(gasReading, elkReading)
                .CallWith(MeterReadingClient.Proxy.UpdateMeterReadingStatus);

            //assert correct statuses
            GetLastMeterReading(_gasEanSmart.EanId).StatusId.Should().Be((int)ProcessStatus.WaitForCustomer);
            GetLastMeterReading(_elkEanSmart4Reg.EanId).StatusId.Should().Be((int)ProcessStatus.WaitToSend);
        }

        [Test, Pairwise]
        public void UpdateFromReadyToSendToWaitForCustomerOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart2Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitForCustomer);
        }

        [Test, Pairwise]
        public void UpdateFromVerifyByAgentToWaitForCustomerOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut,
                MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);

            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());
            expectedStatus.Should().Be(ProcessStatus.WaitForCustomer);
        }

        [Test, Pairwise]
        public void UpdateFromVerifyByAgentToWaitForCustomerNotOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitForCustomer);
        }

        [Test, Pairwise]
        public void UpdateFromVerifyByAgentToReadyToSendOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.ReadyToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test, Pairwise]
        public void UpdateFromVerifyByAgentToRejectedOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanNonSmart4Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.Rejected, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void UpdateFromVerifyByAgentToWaitToSendNotOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanSmart2Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test, Pairwise]
        public void UpdateFromWaitForCustomerToReadyToSendOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanSmart2Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);

            GeneralHelpers.UpdateReadingById(ProcessStatus.ReadyToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.ReadyToSend);
        }

        [Test, Pairwise]
        public void UpdateFromWaitForCustomerToRejectedOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanNonSmart4Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanNonSmart4Reg.EanId, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);

            GeneralHelpers.UpdateReadingById(ProcessStatus.Rejected, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.Rejected);
        }

        [Test, Pairwise]
        public void UpdateFromWaitForCustomerToWaitToSend(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId, me, rs, NotOpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);
            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First()).Should().Be(ProcessStatus.WaitForCustomer);

            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitToSend, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            expectedStatus.Should().Be(ProcessStatus.WaitToSend);
        }

        [Test, Order(2)]
        public void UpdateFromVerifyByAgentToReadyToSend_ForAddressWithTwoConnections(
           [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
           [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.Spontaneous, MarketEvent.Switch)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanSmart2Reg, _gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg, _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg, _gasEanSmart }, CcuMutationDate.AddDays(1));
            const int processId = 2233;

            //creation of readings sets for Gas address and for Elk address
            var readingsSetGasMeter = ConsumptionServiceHelpers.CreateReadingSet(_gasEanSmart,
                me, rs, OpenedWindow);
            var readingsSetElkMeter = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_elkEanSmart2Reg.EanId,
                me, rs, OpenedWindow);

            //assign to elk reading set the same dossierId as gas readings set has
            readingsSetElkMeter.DossierId = readingsSetGasMeter.DossierId;

            //assigning the same processId for both address             
            readingsSetElkMeter.ProcessId = processId;
            readingsSetGasMeter.ProcessId = processId;

            var saveReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetElkMeter);
            saveReadingRequest.MeterReadingsSet.Add(readingsSetGasMeter);
            saveReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            //assert statuses of saved readings sets
            var gasReading = GetLastMeterReading(_gasEanSmart.EanId);
            gasReading.StatusId.Should().Be((int)ProcessStatus.ReadyToSend);
            var elkReading = GetLastMeterReading(_elkEanSmart2Reg.EanId);
            elkReading.StatusId.Should().Be((int)ProcessStatus.VerifyByAgent);

            //update VerifyByAgent reading to ReadyToSend status
            GeneralHelpers.UpdateReadingById(ProcessStatus.ReadyToSend, elkReading.Id);

            //call job to speed up sending of readings to FakeEdsn
            var gasLast = GetLastMeterReading(_gasEanSmart.EanId).Id;
            var elkLast = GetLastMeterReading(_elkEanSmart2Reg.EanId).Id;
            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == gasLast && mr.StatusId == (int)ProcessStatus.SentToEdsn
                      || mr.Id == elkLast && mr.StatusId == (int)ProcessStatus.SentToEdsn));

            //assert correct statuses
            GetLastMeterReading(_gasEanSmart.EanId).StatusId.Should().Be((int)ProcessStatus.SentToEdsn);
            GetLastMeterReading(_elkEanSmart2Reg.EanId).StatusId.Should().Be((int)ProcessStatus.SentToEdsn);
        }

        [Test]
        public void UpdateVerifyByAgentToWaitForCustomer_ForAddressWithTwoConnections(
           [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
           [Values(MarketEvent.Yearly, MarketEvent.MoveIn, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanSmart2Reg, _gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg, _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg, _gasEanSmart }, CcuMutationDate.AddDays(1));
            const int processId = 1122;
            var readingsSetGasMeter = ConsumptionServiceHelpers.CreateVerifyByAgentReadingSet(_gasEanSmart.EanId,
                me, rs, OpenedWindow);
            var readingsSetElkMeter = ConsumptionServiceHelpers.CreateReadingSet(_elkEanSmart2Reg,
                me, rs, NotOpenedWindow);
            //assign to elk reading set the same dossierId as gas readings set has
            readingsSetElkMeter.DossierId = readingsSetGasMeter.DossierId;

            //assigning the same processId for both address             
            readingsSetElkMeter.ProcessId = processId;
            readingsSetGasMeter.ProcessId = processId;

            var saveReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSetElkMeter);
            saveReadingRequest.MeterReadingsSet.Add(readingsSetGasMeter);
            saveReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            //assert statuses of saved readings sets
            var gasReading = GetLastMeterReading(_gasEanSmart.EanId);
            gasReading.StatusId.Should().Be((int)ProcessStatus.VerifyByAgent);
            var elkReading = GetLastMeterReading(_elkEanSmart2Reg.EanId);
            elkReading.StatusId.Should().Be((int)ProcessStatus.WaitToSend);

            //update VerifyByAgent reading to ReadyToSend status
            RequestObjectHelpers.CreateUpdateRequest(gasReading, elkReading)
                .CallWith(MeterReadingClient.Proxy.UpdateMeterReadingStatus);

            //assert correct statuses
            GetLastMeterReading(_gasEanSmart.EanId).StatusId.Should().Be((int)ProcessStatus.WaitForCustomer);
            GetLastMeterReading(_elkEanSmart2Reg.EanId).StatusId.Should().Be((int)ProcessStatus.WaitToSend);
        }

        [Test]
        public void RejectReadingAndResendRequest_ForAddressWithTwoConnections(
           [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
           [Values(MarketEvent.Yearly, MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _gasEan, _elkEanNonSmart4Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEan, _elkEanNonSmart4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEan, _elkEanNonSmart4Reg }, CcuMutationDate.AddDays(1));
            const int processId = 1122;

            GeneralHelpers.SetUpElkAndGasReadingsSets(_gasEan.EanId, _elkEanNonSmart4Reg, rs, me, processId, out _, out var readingsSetElkMeter);

            //assert statuses of saved readings sets
            var gasReading = GetLastMeterReading(_gasEan.EanId);
            gasReading.StatusId.Should().Be((int)ProcessStatus.VerifyByAgent);
            var elkReading = GetLastMeterReading(_elkEanNonSmart4Reg.EanId);
            elkReading.StatusId.Should().Be((int)ProcessStatus.ReadyToSend);

            //update VerifyByAgent reading to Rejected status
            GeneralHelpers.UpdateReadingById(ProcessStatus.Rejected, gasReading.Id);

            //creating new functionally valid readings for Gas address
            var correctReadingsSetGasMeter = ConsumptionServiceHelpers.CreateReadingSet(_gasEan, me, rs, OpenedWindow);

            //assigning the same dossier id and processId
            correctReadingsSetGasMeter.DossierId = readingsSetElkMeter.DossierId;
            //correctReadingsSet.Add(correctReadingsSetGasMeter);
            correctReadingsSetGasMeter.ProcessId = processId;
            readingsSetElkMeter.ProcessId = 1122;

            var saveCorrectReadingRequest = ConsumptionServiceHelpers.SaveMeterReadingRequest(correctReadingsSetGasMeter);
            saveCorrectReadingRequest.MeterReadingsSet.Add(readingsSetElkMeter);
            saveCorrectReadingRequest.CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            //assert readings statuses
            GetLastMeterReading(_gasEan.EanId).StatusId.Should().Be((int)ProcessStatus.ReadyToSend);
            GetLastMeterReading(_elkEanNonSmart4Reg.EanId).StatusId.Should().Be((int)ProcessStatus.ReadyToSend);

            //call job to speed up sending of readings to FakeEdsn
            Scheduler.TriggerJob(QuartzJobName.Energy_Consumption_SendMeterReadingJob);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == GetLastMeterReading(_gasEan.EanId).Id && mr.StatusId == (int)ProcessStatus.SentToEdsn
                || mr.Id == GetLastMeterReading(_elkEanNonSmart4Reg.EanId).Id && mr.StatusId == (int)ProcessStatus.SentToEdsn));

            //assert correct statuses
            GetLastMeterReading(_gasEan.EanId).StatusId.Should().Be((int)ProcessStatus.SentToEdsn);
            GetLastMeterReading(_elkEanNonSmart4Reg.EanId).StatusId.Should().Be((int)ProcessStatus.SentToEdsn);
        }

        [Test]
        public void CheckResponsibleIsUserNameUpdateFromReadyToSendToWaitForCustomerOpenedWindow(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.Switch)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanNonSmart2Reg);
            var readingsSet = ConsumptionServiceHelpers.CreateReadingSet(_elkEanNonSmart2Reg, me, rs, OpenedWindow);
            var response = ConsumptionServiceHelpers.SaveMeterReadingRequest(readingsSet)
                .CallWith(MeterReadingClient.Proxy.SaveMeterReading);

            GeneralHelpers.UpdateReadingById(ProcessStatus.WaitForCustomer, response.SavedMeterReadings.First().MeterReadingId);
            var expectedStatus = ConsumptionServiceHelpers.GetMeterReadingStatusById(response.SavedMeterReadings.First());

            var mrId = response.SavedMeterReadings.First().MeterReadingId;
            expectedStatus.Should().Be(ProcessStatus.WaitForCustomer);
            ConsumptionRepository.GetLastEntity<StatusHistory, DateTime>(s =>
                s.CreatedOn, s => s.MeterReadingId == mrId).User.Should().Be(UserNameResponsible);
        }

        [Test]
        public void UpdateFromDisputeRejectedToRejected([Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_gasEan);
            var disputedReading = MeteringPointsServiceHelpers.GetMeterReading(_gasEan, OpenedWindow, me, ReadingSource.Customer_EDSN);
            var disputedReadingId = ConsumptionServiceHelpers.AcceptOtherPartyMeterReading(disputedReading, me);

            ConsumptionServiceHelpers.StartDisputeByBudget(disputedReading, disputedReadingId);
            var newReadingId = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId, "RegisterReading").Id;
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == newReadingId && mr.StatusId == (int)ProcessStatus.WaitForDispute), 200);

            TestAssertionHelpers.ValidateInDisputeReadingStatus(disputedReadingId);
            GeneralHelpers.WaitForSentToEdsnStatus(newReadingId);

            var newReadingExternalReference = ConsumptionRepository
                .GetLastEntity<MeterReading, DateTime>(m => m.CreatedOn, m => m.EanId == _gasEan.EanId, "RegisterReading").ExternalReference;
            GeneralHelpers.RejectDisputeByFakeEdsn(newReadingExternalReference, newReadingId);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
                mr => mr.Id == disputedReadingId && mr.StatusId == (int)ProcessStatus.Accepted), 200);

            var disputeRejectedReading = ConsumptionRepository.GetEntityByCondition<MeterReading>(mr =>
            mr.EanId == _gasEan.EanId && mr.StatusId == (int)ProcessStatus.DisputeRejected);

            GeneralHelpers.UpdateReadingById(ProcessStatus.Rejected, disputeRejectedReading.Id);
            Waiter.Wait(() => ConsumptionRepository.EntityIsInDb<MeterReading>(
              mr => mr.Id == disputeRejectedReading.Id && mr.StatusId == (int)ProcessStatus.Rejected), 200);
        }

        [Test]
        public void UpdateFromRejectedByEdsnToRejected(
            [Values(ReadingSource.Customer_Mijn)] ReadingSource rs,
            [Values(MarketEvent.MoveOut)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _gasEan);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEan }, CcuMutationDate.AddDays(1));
            var rejectedReading = ConsumptionServiceHelpers.RejectConsDomMeterReading(_gasEan, OpenedWindow, me, rs, MeterReadingStatus.RejectedByEdsn);

            GeneralHelpers.UpdateReadingById(ProcessStatus.Rejected, rejectedReading.MeterReadingId);

            ConsumptionRepository.GetEntityByCondition<MeterReading>(mr => mr.Id == rejectedReading.MeterReadingId).StatusId.Should().Be((int)ProcessStatus.Rejected);
        }
    }
}
