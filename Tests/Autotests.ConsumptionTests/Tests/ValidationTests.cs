﻿using Autotests.Clients;
using Autotests.Core.TestDataProviders;
using NUnit.Framework;
using Nuts.Consumption.Model.Core;
using Nuts.InterDom.Model.Core.Enums;
using Autotests.Core.Helpers;
using FluentAssertions;
using Autotests.Consumption.Base;
using Autotests.Consumption.Framework.Helpers;
using FakeMeterReadingService;

namespace Autotests.Consumption.Tests
{
    [TestFixture, Category(Categories.ConsumptionRegression)]
    internal class ValidationTests : BaseConsDomTest
    {
        private EanInfo _elkEanNonSmart2Reg;
        private EanInfo _elkEanSmart2Reg;
        private EanInfo _elkEanNonSmart4Reg;
        private static EanInfo _elkEanSmart4Reg;
        private static EanInfo _elkEanTfSignal;
        private static EanInfo _elkEanTfSignal4Reg;
        private EanInfo _gasEan;
        private EanInfo _gasEanSmart;

        [OneTimeSetUp]
        public void SetUp()
        {
            _elkEanNonSmart2Reg = EanInfoProvider.GetRandomElkEan(false);
            _elkEanNonSmart4Reg = EanInfoProvider.GetRandomElkEan(false, 4);
            _elkEanTfSignal = EanInfoProvider.GetRandomElkEan(false, 2, profileCode:EnergyUsageProfileCode.E1A);
            _elkEanTfSignal4Reg = EanInfoProvider.GetRandomElkEan(false, 4, profileCode: EnergyUsageProfileCode.E1A);
            _elkEanSmart4Reg = EanInfoProvider.GetRandomElkEan(true, 4);
            _elkEanSmart2Reg = EanInfoProvider.GetRandomElkEan(true);
            _gasEan = EanInfoProvider.GetRandomGasEan(false);
            _gasEanSmart = EanInfoProvider.GetRandomGasEan(true);
            MduProcessor.SendMdu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanTfSignal, _elkEanTfSignal4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan });
            CcuProcessor.SendMoveInCcu(new List<EanInfo> { _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _elkEanSmart4Reg, _gasEanSmart, _gasEan }, CcuMutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo> { _elkEanTfSignal, _elkEanTfSignal4Reg, _elkEanNonSmart4Reg, _elkEanNonSmart2Reg, _elkEanSmart2Reg, _gasEanSmart, _gasEan });
        }

        [Test]
        public void ValidateCorrectReading(
            [Values(MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _gasEanSmart);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _gasEanSmart });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _gasEanSmart }, CcuMutationDate.AddDays(1));

            var estimate = ConsumptionServiceHelpers.EstimateReadings(_gasEanSmart.EanId, me, OpenedWindow);
            var estimationResult = estimate.EstimationData.First();
            var validate = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validate.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateIncorrectReading(
            [Values(MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg }, CcuMutationDate.AddDays(1));
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart2Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();

            estimationResult.EstimationRegisters.ForEach(re => re.Estimation.Estimation = re.Estimation.EstimationMin - 200);
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.InvalidFunctionality);
            }
        }

        [Test]
        public void ValidateTechnicallyIncorrectReading(
           [Values(MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan( _elkEanSmart2Reg);
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart2Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            GeneralHelpers.SetUpEstimationResult(estimationResult);

            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.InvalidTechnical);
            }
        }

        [Test]
        public void ValidateSmartMeterReading(
            [Values(MarketEvent.EndOfSupply, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg }, CcuMutationDate.AddDays(1));
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart2Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();

            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateReading_P4ReadingAlwaysValid(
            [Values(MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart2Reg);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanSmart2Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanSmart2Reg }, CcuMutationDate.AddDays(1));
           
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart2Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();

            estimationResult.EstimationRegisters.ForEach(re => re.Estimation.Estimation = re.Estimation.EstimationMax+100);
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateUsingEstimatedReadingSource()
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanSmart4Reg, DateTime.Today.AddDays(-10),
                MarketEvent.Spontaneous, ReadingSource.Estimated_Backoffice);
           var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId,
                    MarketEvent.Spontaneous, DateTime.Today);
           var validationResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResponse.EstimationData.First(),
                MarketEvent.Spontaneous, DateTime.Today);

            validationResponse.Result.First().RegisterValidationResults.ForEach(vr => 
                vr.ValidationStatus.Should().Be(ValidationStatus.Valid));
        }

        [Test]
        public void ValidateTlvRegisters()
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            ConsumptionServiceHelpers.AcceptConsDomMeterReading(_elkEanSmart4Reg, DateTime.Today.AddDays(-10),
                MarketEvent.Spontaneous, ReadingSource.Estimated_Backoffice);
            var estimationResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId,
                MarketEvent.Spontaneous, DateTime.Today);
            estimationResponse.EstimationData.First().EstimationRegisters.First(er =>
                    er.MeteringDirection == EnergyFlowDirectionCode.LVR && er.TariffType == EnergyTariffTypeCode.N)
                .Estimation.Estimation = 5;
            estimationResponse.EstimationData.First().EstimationRegisters.First(er =>
                    er.MeteringDirection == EnergyFlowDirectionCode.TLV && er.TariffType == EnergyTariffTypeCode.N)
                .Estimation.Estimation = 50;
            var validationResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResponse.EstimationData.First(),
                MarketEvent.Spontaneous, DateTime.Today);

            validationResponse.Result.First().RegisterValidationResults.ForEach(vr => vr.ValidationStatus.Should().Be(ValidationStatus.Valid));
        }

        [Test]
        public void ValidateMoveInSmartMeterReadingEqualToPrevious()
        {
            CleanMeterReadingsForEan(_gasEanSmart);
            const MarketEvent me = MarketEvent.MoveIn;
            const ReadingSource rs = ReadingSource.Customer_Backoffice;
            var meterReading = ConsumptionServiceHelpers.AcceptConsDomMeterReading(_gasEanSmart, OpenedWindow, me, rs);
            var response = RequestObjectHelpers.GenerateValidateMeterReadingRequest(_gasEanSmart.EanId, me, meterReading, OpenedWindow)
                .CallWith(MeterReadingClient.Proxy.ValidateMeterReading);

            foreach (var result in response.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateRegisterReading(
            [Values(MarketEvent.EndOfSupply, MarketEvent.MoveIn, MarketEvent.MoveOut, MarketEvent.Spontaneous, MarketEvent.Switch, MarketEvent.TwoMonthly, MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanSmart4Reg);
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanSmart4Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();

            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow);

            validateResponse.Result.First().RegisterValidationResults.ElementAt(0).RegisterNumber.Should()
                .BeEquivalentTo(
                    MeteringPointsServiceHelpers.GetFirstRegisterNumber(_elkEanSmart4Reg.EanId, NotOpenedWindow));
        }

        [Test]
        public void ValidateTfSignalReadings_Valid([Values(MarketEvent.Yearly)] MarketEvent me)
        {
            CleanMeterReadingsForEan(_elkEanTfSignal);
            
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanTfSignal });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanTfSignal }, CcuMutationDate);
            _elkEanTfSignal.ProfileCategory = EnergyUsageProfileCode.E1A;
            
            MduProcessor.SendMdu(new List<EanInfo>() {_elkEanTfSignal}, CcuMutationDate.AddDays(1), tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(_elkEanTfSignal, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == _elkEanTfSignal.EanId && mp.IsTfSignal == true), 15);

            //validate reading
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanTfSignal.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            var validateResponse =
                ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow, true);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateTfSignal4RegistersReadings_Valid([Values(MarketEvent.Yearly)] MarketEvent me)
        {
            var mutationDate = DateTime.Today.AddDays(-100);
            FakeEdsnAdminClient.ClearAllReadings();
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanTfSignal4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanTfSignal4Reg }, mutationDate);
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanTfSignal4Reg }, mutationDate.AddDays(1));
            _elkEanTfSignal4Reg.ProfileCategory = EnergyUsageProfileCode.E1A;

            MduProcessor.SendMdu(new List<EanInfo>() { _elkEanTfSignal4Reg }, mutationDate.AddDays(2), tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(_elkEanTfSignal4Reg, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == _elkEanTfSignal4Reg.EanId && mp.IsTfSignal == true), 3);

            //validate reading
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanTfSignal4Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            var validateResponse =
                ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me, OpenedWindow, true);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                result.ValidationStatus.Should().Be(ValidationStatus.Valid);
            }
        }

        [Test]
        public void ValidateTfSignalReadings_InvalidFunctionality([Values(MarketEvent.Yearly)] MarketEvent me,
            [Values(TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg, TestDataPreparation.MeterType.ElkEanTfSignalValid4Reg)] TestDataPreparation.MeterType meterType)
        {
            var ean = TestDataPreparation.TestEans[meterType];
            var mutationDate = DateTime.Today.AddDays(-100);
            FakeEdsnAdminClient.ClearAllReadings();
            MduProcessor.SendMdu(new List<EanInfo>(){ean}, mutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { ean });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { ean }, mutationDate.AddDays(1));
            MduProcessor.SendMdu(new List<EanInfo>() { ean }, mutationDate.AddDays(2), tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(ean, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == ean.EanId && mp.IsTfSignal), 15);

            //validate reading
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(ean.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me,
                    OpenedWindow, true, true);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                if (result.MeteringDirection == EnergyFlowDirectionCode.TLV)
                {
                    result.ValidationStatus.Should().Be(ValidationStatus.Valid);
                    result.ValidationMessage.Should().BeNullOrEmpty();
                }

                if (result.MeteringDirection == EnergyFlowDirectionCode.LVR)
                {
                    result.ValidationStatus.Should().Be(ValidationStatus.InvalidFunctionality);
                    result.ValidationMessage.Should().Contain(PatternMessages.InvalidTfSignalReading);
                }
            }
        }

        [Test]
        public void ValidateTfSignal4RegistersReadings_InvalidFunctionality([Values(MarketEvent.Yearly)] MarketEvent me)
        {
            var mutationDate = DateTime.Today.AddDays(-100);
            FakeEdsnAdminClient.ClearAllReadings();
            //MduProcessor.SendMdu(new List<EanInfo>(){_elkEanTfSignal4Reg}, mutationDate);
            GeneralHelpers.CreateInitialReadingsInTmr(new List<EanInfo>() { _elkEanTfSignal4Reg });
            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanTfSignal4Reg }, mutationDate.AddDays(1));

            CcuProcessor.SendMoveInCcu(new List<EanInfo>() { _elkEanTfSignal4Reg }, mutationDate.AddDays(2));
            MduProcessor.SendMdu(new List<EanInfo>() { _elkEanTfSignal4Reg }, mutationDate.AddDays(3), tfSignal: true);

            //create tf signal reading in fake edsn and accept it
            var request = FakeEdsnServiceHelpers.GetMeterReadingRequest(_elkEanTfSignal4Reg, OpenedWindow,
                MeterReadingExchangeNotificationEnvelope_EnergyMeterReadingMethodCode.Item005);
            request.Portaal_Content[0].Portaal_Mutation.MutationReason =
                MeterReadingExchangeNotificationEnvelope_MutationReasonPortaalCode.PERMTR;
            FakeMeterReadingService.MeterReadingExchangeNotificationAsync(request);
            var extRef = request.Portaal_Content[0].Portaal_Mutation.ExternalReference;
            FakeEdsnServiceHelpers.ProcessAcceptedReadingByEdsn(extRef);

            //assert that meter point has IsTfSignal = 1
            Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPoint.Sql.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == _elkEanTfSignal4Reg.EanId && mp.IsTfSignal), 15);

            //validate reading
            var estimateResponse = ConsumptionServiceHelpers.EstimateReadings(_elkEanTfSignal4Reg.EanId, me, OpenedWindow);
            var estimationResult = estimateResponse.EstimationData.First();
            var validateResponse = ConsumptionServiceHelpers.ValidateReadingsByEstimation(estimationResult, me,
                    OpenedWindow, true, true);

            foreach (var result in validateResponse.Result.First().RegisterValidationResults)
            {
                if (result.MeteringDirection == EnergyFlowDirectionCode.TLV)
                {
                    result.ValidationStatus.Should().Be(ValidationStatus.Valid);
                    result.ValidationMessage.Should().BeNullOrEmpty();
                }

                if (result.MeteringDirection == EnergyFlowDirectionCode.LVR)
                {
                    result.ValidationStatus.Should().Be(ValidationStatus.InvalidFunctionality);
                    result.ValidationMessage.Should().Contain(PatternMessages.InvalidTfSignalReading);
                }
            }
        }
    }
}