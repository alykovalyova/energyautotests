﻿using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Repositories.P4DailyReadingsModels;
using NUnit.Framework;
using Nuts.QueueExchange;
using System.Net;

namespace Autotests.EdsnEndpoints.Base
{
    [SetUpFixture, Category(Categories.EdsnEndpoints)]
    internal class BaseEdsnEndpoints: AllureReport
    {
        //clients declaraion
        protected ServiceClient<Nuts.EdsnGateway.Contract.TMRData.ITMRDataService> TMRDataSupplyingClient;
        protected ServiceClient<Nuts.EdsnGateway.Contract.MeterReadings.IMeterReadingService> GatewayMeterReadingClient;
        protected NutsHttpClient MoveInClient;
        protected NutsHttpClient MpPreClient;
        protected NutsHttpClient MpInfoClient;
        protected NutsHttpClient MpSupplyClient;

        //databases declaration
        protected DbHandler<P4DailyReadingsContext> P4DailyReadingsDb;

        //variables initialization
        private ConfigHandler _configHanler = new ConfigHandler();
        protected const string EanId = "111425200000012487";
        protected const string BalanceResponsibleId = "8934547493531";
        protected const string BalanceSupplierId = "8714252018141";
        protected const string GridOperatorId = "2438439739743";
        protected const string MutationDate = "2020-09-29T07:36:51.277Z";
        protected const string ReferenceId = "8ba86baf-3350-4941-8219-639f289c1c8f";
        protected const string KvkNumber = "12435687";
        protected const string BuildingNr = "71";
        protected const string Zipcode = "1212QQ";
        protected const int TimeOfWaiting = 100;
        protected List<HttpStatusCode> AllowedStatusCodes;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            TMRDataSupplyingClient = new ServiceClient<Nuts.EdsnGateway.Contract.TMRData.ITMRDataService>(_configHanler.ServicesEndpoints.EgwDataSupplyingService);
            GatewayMeterReadingClient = new ServiceClient<Nuts.EdsnGateway.Contract.MeterReadings.IMeterReadingService>(_configHanler.ServicesEndpoints.EgwMeterReadingService);
            MoveInClient = new NutsHttpClient(new BaseHttpClient(_configHanler.ApiUrls.MoveIn));
            MpPreClient = new NutsHttpClient(new BaseHttpClient(_configHanler.ApiUrls.MpPre));
            MpInfoClient = new NutsHttpClient(new BaseHttpClient(_configHanler.ApiUrls.MpInfo));
            MpSupplyClient = new NutsHttpClient(new BaseHttpClient(_configHanler.ApiUrls.MpSupply));

            P4DailyReadingsDb = new DbHandler<P4DailyReadingsContext>(_configHanler.GetConnectionString(DbConnectionName.P4DailyReadingsDatabase.ToString()));
            AllowedStatusCodes = new List<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound, HttpStatusCode.PreconditionFailed };
        }

        protected void SetConsumerEvents<TRequest>(Consumer<TRequest> consumer) where TRequest: class
        {
            consumer.OnCallbackException += (o, e) =>
            {
                Assert.IsTrue(false);
            };
            consumer.OnMessageReceivedAsync += async (s, m, a) =>
            {
                var isCorrect = string.IsNullOrWhiteSpace(a.ErrorMessage);
                Assert.IsTrue(isCorrect);
            };
        }
    }
}