﻿using Autotests.Clients;
using Autotests.EdsnEndpoints.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.EdsnGateway.Contract.TMRData;
using System.Net;

namespace Autotests.EdsnEndPoints.Tests.EdsnMethods
{
    [TestFixture]
    internal class GetTmrDataSupplierPhaseTests: BaseEdsnEndpoints
    {
        [Test]
        public void GetTmrDataSupplierPhase_CheckConnection()
        {
            var request = new GetTMRDataSupplierPhaseRequest { Ean = EanId };
            var response = request.CallWith(TMRDataSupplyingClient.Proxy.GetTmrDataSupplierPhase);

            var isCorrectStatusCode = AllowedStatusCodes.Contains((HttpStatusCode)response.Header.StatusCode);
            isCorrectStatusCode.Should().BeTrue();
        }
    }
}
