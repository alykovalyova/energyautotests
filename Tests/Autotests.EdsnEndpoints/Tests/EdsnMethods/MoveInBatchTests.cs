﻿using Autotests.Clients;
using Autotests.EdsnEndpoints.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using static Autotests.Helper.GtwFactory;

namespace Autotests.EdsnEndpoints.Tests.EdsnMethods
{
    [TestFixture]
    internal class MoveInBatchTests : BaseEdsnEndpoints
    {
        [Test]
        public void MoveInBatch_CheckConnection()
        {
            var response = CreateMoveInRequest(EanId, BalanceResponsibleId, BalanceSupplierId, GridOperatorId,
                    ReferenceId, MutationDate, KvkNumber, int.Parse(BuildingNr), Zipcode)
                .CallWith<MoveInRequest, MoveInResponse>(MoveInClient);

            var isCorrectStatusCode = AllowedStatusCodes.Contains(response.Header.StatusCode);
            isCorrectStatusCode.Should().BeTrue();
        }
    }
}
