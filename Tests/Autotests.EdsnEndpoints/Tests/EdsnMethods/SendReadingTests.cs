﻿using Autotests.Clients;
using FluentAssertions;
using NUnit.Framework;
using Nuts.EdsnGateway.Contract.MeterReadings.Headers;
using System.Net;
using Nuts.EdsnGateway.Contract.MeterReadings.MeterReadings;
using Autotests.EdsnEndpoints.Base;

namespace Autotests.EdsnEndpoints.Tests.EdsnMethods
{
    [TestFixture]
    internal class SendReadingTests: BaseEdsnEndpoints
    {
        [Test]
        public void SendMeterReading_CheckConnection()
        {
            var request = new SendMeterReadingsRequest();
            request.MeteringReadings = new List<MeterReading>();
            var response = request.CallWith(GatewayMeterReadingClient.Proxy.SendMeterReadings);

            var isCorrectStatusCode = AllowedStatusCodes.Contains((HttpStatusCode)response.Header.StatusCode);
            isCorrectStatusCode.Should().BeTrue();
        }
    }
}
