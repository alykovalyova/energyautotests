﻿using Autotests.Core.Dispatchers;
using Autotests.Core.Handlers;
using FluentAssertions;
using NUnit.Framework;
using Nuts.QueueExchange;
using Nuts.QueueExchange.ConnectionInfo;
using Nuts.QueueExchange.ConsumerInfo;
using Nuts.EdsnGateway.Contract.Queue.FileExchange;
using WinSCP;
using Autotests.Core.Helpers;
using Autotests.EdsnEndpoints.Base;

namespace Autotests.EdsnEndpoints.Tests.EdsnMethods
{
    [TestFixture]
    internal class UploadCerFileMessageReceivedTests : BaseEdsnEndpoints
    {
        [Test]
        public void UploadCerFileMessageReceived_CheckConnection()
        {
            var cephUploader = new EdsnCephUploader(ConfigHandler.Instance.EdsnCephSettings.CephKey);
            var requestQueue = cephUploader.GetUploadCerFileMesageModel();

            cephUploader.IsCephFileInStorage().Should().BeTrue();
            RemoveSftpFile(ConfigHandler.Instance.EdsnSftpSettings.SFTPFilePath);

            var queueConnectionInfo = new QueueConnectionInfo(ConfigHandler.Instance.QueueSettings.HostNameQueue, int.Parse(ConfigHandler.Instance.QueueSettings.PortQueue),
                ConfigHandler.Instance.QueueSettings.UserQueue, ConfigHandler.Instance.QueueSettings.PasswordQueue, "Autotest");
            var queueConsumerInfo = new QueueConsumerInfo(DispatcherName.EDSNGateway, "FileExchangeConfirmation", "Nuts.EdsnGateway.Contract.Queue.FileExchange.FileExchangeConfirmation");
            var consumer = new Consumer<FileExchangeConfirmation>(queueConnectionInfo, queueConsumerInfo);
            SetConsumerEvents(consumer);

            var dispatcher = new Dispatcher(DispatcherName.CustomerInfo, ConfigHandler.Instance.QueueSettings.HostNameQueue);
            dispatcher.DispatchMessage(requestQueue);

            consumer.StartConsuming();

            Assert.DoesNotThrow(() => Waiter.Wait(() => IsSftpFileExists(ConfigHandler.Instance.EdsnSftpSettings.SFTPFilePath) == true, TimeOfWaiting));
            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, 5));
        }

        private bool IsSftpFileExists(string sftpFilePath)
        {
            using (var session = new Session())
            {
                var sessionOptions = GetSessionOptions();
                session.Open(sessionOptions);
                return session.FileExists(sftpFilePath);
            }
        }

        private void RemoveSftpFile(string sftpFilePath)
        {
            if (!IsSftpFileExists(sftpFilePath))
            {
                return;
            }
            using (var session = new Session())
            {
                var sessionOptions = GetSessionOptions();
                session.Open(sessionOptions);
                session.RemoveFiles(sftpFilePath).Check();
            }
        }

        private SessionOptions GetSessionOptions()
        {
            return new SessionOptions
            {
                Protocol = WinSCP.Protocol.Sftp,
                HostName = ConfigHandler.Instance.EdsnSftpSettings.SFTPHost,
                UserName = ConfigHandler.Instance.EdsnSftpSettings.SFTPUser,
                Password = ConfigHandler.Instance.EdsnSftpSettings.SFTPPass,
                GiveUpSecurityAndAcceptAnySshHostKey = true
            };
        }
    }
}
