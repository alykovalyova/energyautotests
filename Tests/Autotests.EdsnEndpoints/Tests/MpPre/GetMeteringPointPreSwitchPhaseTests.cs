﻿using Autotests.Clients;
using Autotests.EdsnEndpoints.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPointPrePhase.Contract;
using static Autotests.Helper.GtwFactory;

namespace Autotests.EdsnEndpoints.Tests.MpPre
{
    [TestFixture]
    internal class GetMeteringPointPreSwitchPhaseTests : BaseEdsnEndpoints
    {
        [Test]
        public void GetMeteringPointPreSwitchPhase_CheckConnection()
        {
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(EanId, ReferenceId, BalanceSupplierId)
                .CallWith<GetMeteringPointsPreSwitchPhaseRequest, GetMeteringPointsPreSwitchPhaseResponse>(MpPreClient);

            var isCorrectStatusCode = AllowedStatusCodes.Contains(response.Header.StatusCode);
            isCorrectStatusCode.Should().BeTrue();
        }
    }
}
