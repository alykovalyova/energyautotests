﻿using NUnit.Framework;
using static Autotests.Helper.GtwFactory;
using Nuts.MeteringPointInfoPhase.Contract;
using FluentAssertions;
using Autotests.Clients;
using Autotests.EdsnEndpoints.Base;

namespace Autotests.EdsnEndpoints.Tests.MpSearch
{
    [TestFixture]
    internal class GetMeteringPointSearchPhaseTests : BaseEdsnEndpoints
    {
        [Test]
        public void GetMeteringPointSearchPhase_CheckConnection()
        {
            var response = CreateGetMeteringPointInfoRequest(EanId).
                CallWith<GetMeteringPointInfoRequest, GetMeteringPointInfoResponse>(MpInfoClient);

            var isCorrectStatusCode = AllowedStatusCodes.Contains(response.Header.StatusCode);
            isCorrectStatusCode.Should().BeTrue();
        }
    }
}
