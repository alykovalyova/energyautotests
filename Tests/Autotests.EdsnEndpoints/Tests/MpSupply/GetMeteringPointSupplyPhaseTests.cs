﻿using Autotests.Clients;
using Autotests.EdsnEndpoints.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPointSupplyPhase.Contract;
using static Autotests.Helper.GtwFactory;

namespace Autotests.Tests.CheckConnection.MpSupply
{
    [TestFixture]
    internal class GetMeteringPointSupplyPhaseTests : BaseEdsnEndpoints
    {
        [Test]
        public void GetMeteringPointSupply_CheckConnection()
        {
            var response = CreateGetMeteringPointSupplyRequest(EanId)
                .CallWith<GetMeteringPointSupplyRequest, GetMeteringPointSupplyResponse>(MpSupplyClient);

            var isCorrectStatusCode = AllowedStatusCodes.Contains(response.Header.StatusCode);
            isCorrectStatusCode.Should().BeTrue();
        }
    }
}
