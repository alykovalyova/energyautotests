﻿using System.Globalization;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using NUnit.Framework;
using Nuts.EdsnGateway.Contract.PublishCustomerInfo;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture, Category(Categories.GtwServices)]
    internal abstract class BaseTest : AllureReport
    {
        //clients declaration
        protected FakeEdsnAdminClient FakeEdsnAdminClient;
        protected ServiceClient<IPublishCustomerInfo> PublishCustomerClient;
        protected NutsHttpClient EdsnSwitchClient;

        //helpers declaration
        public BuildDirector BuildDirector;

        //variables initialization
        protected const string EanId = "111256123154675001";

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            BuildDirector = new BuildDirector();
            EdsnSwitchClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.EdsnSwitch));
            FakeEdsnAdminClient = new FakeEdsnAdminClient();
            PublishCustomerClient = new ServiceClient<IPublishCustomerInfo>(ConfigHandler.Instance.ServicesEndpoints.PublishCustomerInfoService);
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);
        }
    }
}