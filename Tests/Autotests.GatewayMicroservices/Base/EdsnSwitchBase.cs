﻿using Autotests.Clients;
using Autotests.Core.Handlers;
using Autotests.Helper.DataProviders.FakeEdsn;
using NUnit.Framework;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture]
    internal abstract class EdsnSwitchBase : BaseTest
    {
        protected const string Ean = "000425200000006000";
        protected const string BalanceResponsibleId = TestConstants.BalanceResponsible;
        protected const string BalanceSupplierId = TestConstants.BudgetSupplier;
        protected const string GridOperatorId = TestConstants.GridOperator;
        protected const string ReferenceId = TestConstants.ExternalReference;
        protected const string KvkNumber = TestConstants.KvkNumber;
        protected const string Zipcode = TestConstants.Zipcode;
        protected const string InvalidZipcode = "0000QQ";
        protected const string BuildingNr = TestConstants.BuildingNr;
        protected const string ExBuildingNr = "B";
        protected const string NotExistedEanId = "0001125200000006";
        protected const string MutationDate = "2020-09-29T07:36:51.277Z";
        protected DateTime CurrentDate = DateTime.Today;
        protected NutsHttpClient EdsnSwitchClients;

        [OneTimeSetUp]
        public new void OneTimeSetUp() => EdsnSwitchClients = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.EdsnSwitch));
    }
}