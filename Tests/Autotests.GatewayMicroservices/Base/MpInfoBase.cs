﻿using Autotests.Clients;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper.Builders.FakeEdsn.SearchPhase;
using Autotests.Helper.DataProviders.FakeEdsn;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using FakeSearchMP = Nuts.FakeEdsn.Contracts.SearchMeteringPointsResponseRequestEnvelope;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture]
    internal abstract class MpInfoBase : BaseTest
    {
        //clients declaraion
        protected NutsHttpClient MpInfoClient;

        //variables initialization
        protected new const string EanId = "000425200000006000";
        protected const string Zipcode = "1111QQ";
        protected const int BuildingNr = 71;
        protected const string ExBuildingNr = "B";
        protected const string NotExistedEanId = "000112520000000600";

        [OneTimeSetUp]
        public new void OneTimeSetUp()
        {
            SaveInfoPhaseToFake(EanId);
            MpInfoClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.MpInfo));
        }

        public void SaveInfoPhaseToFake(string eanId)
        {
            var mpInfoPhaseRequest = BuildDirector.Get<SearchPhaseRequestBuilder>()
                .SetEanId(eanId)
                .SetBuildingNr(TestConstants.BuildingNr)
                .SetExBuildingNr(TestConstants.ExBuildingNr)
                .SetZipcode(TestConstants.Zipcode)
                .Build();
            var mpInfoPhaseResponse = BuildDirector.Get<SearchPhaseResponseBuilder>()
                .SetEanId(eanId)
                .SetProductType(TestConstants.GasProductType)
                .SetMarketSegment(TestConstants.MarketSegment)
                .SetAddress(SearchPhaseEntitiesProvider.Address)
                .SetEnergyMeter(new[] {SearchPhaseEntitiesProvider.EnergyMeter})
                .SetPhysicalCharacteristics(SearchPhaseEntitiesProvider.PhysicalCharacteristics)
                .SetGridOperator(SearchPhaseEntitiesProvider.GridOperator)
                .SetGridArea(TestConstants.GridArea)
                .SetMeteringPointGroup(SearchPhaseEntitiesProvider.MpGroup)
                .Build();
            var getMpInfoPhase = new FakeSearchMP
            {
                SoapException = new SoapFaultDetails(),
                Request = mpInfoPhaseRequest,
                Response = new SearchMeteringPointsResponseEnvelope
                {
                    Portaal_Content = new SearchMeteringPointsResponseEnvelope_PC
                    {
                        Item = new SearchMeteringPointsResponseEnvelope_PC_Result
                            {Portaal_MeteringPoint = new []{ mpInfoPhaseResponse }}
                    }
                }
            };

            FakeEdsnAdminClient.ClearAllMpsSearch();
            FakeEdsnAdminClient.SaveSearchPhaseMeteringPoint(new List<SearchMeteringPointsResponseRequestEnvelope>{getMpInfoPhase});
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => FakeEdsnAdminClient.ClearAllMpsSearch();
    }
}