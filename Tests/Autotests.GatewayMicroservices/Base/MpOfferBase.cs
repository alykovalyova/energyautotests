﻿using Autotests.Clients;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper.Builders.FakeEdsn.OfferPhase;
using Autotests.Helper.DataProviders.FakeEdsn;
using NUnit.Framework;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture]
    internal abstract class MpOfferBase : BaseTest
    {
        //clients declaration
        protected NutsHttpClient MpOfferClient;

        //variables initialization
        protected const string _eanId = "000425200000009999";
        protected const string NotExistedEanId = "000425200000007788";
        protected const string NotRecognizedEan = "000111100000007788";
        protected const string ExternalRef = "autotest_external_reference";
        protected const string SupplierEan = "8714252018141";
        protected OfferPhaseEntitiesProvider PcDefaultData;

        [OneTimeSetUp]
        public new void OneTimeSetUp()
        {
            PcDefaultData = new OfferPhaseEntitiesProvider();
            MpOfferClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.MpOffer));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => FakeEdsnAdminClient.ClearAllMpsOfferPhase();

        protected void InitializeMpOfferPhaseResponse(string eanId, bool withEapPeaks = true)
        {
            FakeEdsnAdminClient.ClearAllMpsOfferPhase();
            var physicalCharacteristics = BuildDirector.Get<Helper.Builders.FakeEdsn.MasterData.PhysicalCharacteristicsBuilder>()
                .SetCaptarCode(TestConstants.CapTarCode)
                .SetEnergyFlowDirection(TestConstants.EnergyFlowDirection)
                .SetProfileCategory(TestConstants.ProfileCategory)
                .SetEacPeaks("100")
                .SetEapPeaks(withEapPeaks ? "100" : null)
                .Build();
            var portalContent = BuildDirector.Get<OfferPhaseBuilder>()
                .SetEanId(eanId)
                .SetEnergyMeter(PcDefaultData.EnergyMeter)
                .SetGridArea(TestConstants.GridArea)
                .SetGridOperator(PcDefaultData.GridOperator)
                .SetPortalMutation(PcDefaultData.Mutation)
                .SetAddress(PcDefaultData.EdsnAddress)
                .SetPhysicalCharacteristics(physicalCharacteristics)
                .SetMarketSegment(TestConstants.MarketSegment)
                .SetAdminStatusSmartMeter(PcDefaultData.IsSmartMeter)
                .SetMeteringPointGroup(PcDefaultData.MeteringPointGroup)
                .SetProductType(TestConstants.GasProductType)
                .SetValidFromDate(DateTime.Now)
                .Build();
            var mpOfferPhase = new GetMPInformationResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetMPInformationResponseEnvelope_EDSNBusinessDocumentHeader(),
                Portaal_Content = new GetMPInformationResponseEnvelope_PC() {Item = portalContent}
            };
            FakeEdsnAdminClient.ClearAllMpsOfferPhase();
            FakeEdsnAdminClient.SaveOfferPhaseMeteringPoint(new List<GetMPInformationResponseEnvelope> { mpOfferPhase });
        }
    }
}