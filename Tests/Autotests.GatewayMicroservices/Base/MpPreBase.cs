﻿using Allure.NUnit.Attributes;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Builders.FakeEdsn.PreSwitchPhase;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Repositories.MeteringPointPreModels;
using NUnit.Framework;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture]
    internal abstract class MpPreBase : BaseTest
    {
        //clients declaration
        protected NutsHttpClient MpPreClient;

        //databases declarations
        protected DbHandler<MeteringPointPreContext> MpPreDb;

        //variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        protected BuildDirector BuildDirector;
        protected new const string EanId = "000425200000007777";
        protected string GasProductType = "Gas";
        protected const string NotExistedEanId = "000425200000007788";
        protected const string NotRecognizedEan = "888425200000007788";
        protected const string ExternalRef = "autotest_external_reference";
        protected const string SupplierEan = "8714252018141";
        protected PreswitchPhaseEntitiesProvider PcDefaultValues;

        [OneTimeSetUp]
        public new void OneTimeSetUp()
        {
            MpPreClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.MpPre));
            MpPreDb = new DbHandler<MeteringPointPreContext>(_configHandler.GetConnectionString(DbConnectionName.MeteringPointPreDatabase.ToString()));
            PcDefaultValues = new PreswitchPhaseEntitiesProvider();
            FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();
            BuildDirector = new BuildDirector();
        }

        [TearDown]
        public void TearDown() => FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();

        [AllureStep("SaveValidEanToFake")]
        protected void SaveValidEanToFake(string ean, string productType = "GAS")
        {
            var eanPortalContent = BuildDirector.Get<PreSwitchPhaseBuilder>()
                .SetEanId(ean)
                .SetGridOperator(PcDefaultValues.GridOperatorCompany)
                .SetAddress(PcDefaultValues.Address)
                .SetEnergyMeter(PcDefaultValues.EnergyMeter)
                .SetProductType(productType)
                .SetPhysicalCharacteristics(PcDefaultValues.PhysicalCharacteristics)
                .SetPortalMutation(PcDefaultValues.Mutation)
                .SetMarketSegment(TestConstants.MarketSegment)
                .Build();
            var getMpPreSwitchResponse = new GetSCMPInformationResponseEnvelope
            {
                EDSNBusinessDocumentHeader = new GetSCMPInformationResponseEnvelope_EDSNBusinessDocumentHeader(),
                Portaal_Content = new GetSCMPInformationResponseEnvelope_PC { Item = eanPortalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();
            FakeEdsnAdminClient.SavePreSwitchPhaseMeteringPoint(new List<GetSCMPInformationResponseEnvelope> { getMpPreSwitchResponse });
        }
    }
}