﻿using Amazon.Runtime.Internal;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Builders.FakeEdsn;
using Autotests.Helper.Builders.FakeEdsn.SupplyPhase;
using Autotests.Helper.DataProviders.FakeEdsn;
using NUnit.Framework;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture]
    internal abstract class MpSupplyBase : BaseTest
    {
        //clients declaration
        protected NutsHttpClient MpSupplyClient;

        //variables intialization
        private ConfigHandler _configHandler = new ConfigHandler();
        protected BuildDirector BuildDirector;
        protected new const string EanId = "000425200000007777";
        protected const string WrongEanId = "000425200000008888";

        [OneTimeSetUp]
        public new void OneTimeSetUp()
        {
            SaveEanToFake(EanId);
            MpSupplyClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.MpSupply));
            BuildDirector = new BuildDirector();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => FakeEdsnAdminClient.ClearAllMpsSupplierPhase();

        public void SaveEanToFake(string eanId)
        {
            var portalContent = BuildDirector.Get<SupplyPhaseBuilder>()
                .SetEanId(eanId)
                .SetProductType(TestConstants.GasProductType)
                .SetAddress(SupplyPhaseTestConstants.Address)
                .SetCommercialCharacteristics(SupplyPhaseTestConstants.CommercialCharacteristics)
                .SetPhysicalCharacteristic(SupplyPhaseTestConstants.PhysicalCharacteristics)
                .SetEnergyMeter(SupplyPhaseTestConstants.GetPortalEnergyMeter(SupplyPhaseTestConstants.EnergyMeterId))
                .SetGridArea(TestConstants.GridArea)
                .SetGridOperator(TestConstants.GridOperator)
                .SetMeteringPointGroup(SupplyPhaseTestConstants.MeteringPointGroup)
                .SetMarketSegment(TestConstants.MarketSegment)
                .SetLocationDescription(TestConstants.LocationDescription)
                .SetIsSmartMeterProperty(false)
                .SetValidFromDate(DateTime.Now)
                .Build();
            var mpSupplyPhase = new GetMeteringPointResponseEnvelope
            {
                EDSNBusinessDocumentHeader = new GetMeteringPointResponseEnvelope_EDSNBusinessDocumentHeader
                    {MessageID = "ffec7c54-97bf-49f2-86b7-3a9f4f0db674", CreationTimestamp = DateTime.Now },
                Portaal_Content = new GetMeteringPointResponseEnvelope_PC {Item = portalContent}
            };
            FakeEdsnAdminClient.ClearAllMpsSupplierPhase();
            FakeEdsnAdminClient.SaveSupplierPhaseMeteringPoint(new AutoConstructedList<GetMeteringPointResponseEnvelope>{mpSupplyPhase});
        }
    }
}