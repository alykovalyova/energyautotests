﻿using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using FakeNoticeContractAck = Nuts.FakeEdsn.Contracts.NoticeContract.NoticeContractAcknowledgementEnvelope;

namespace Autotests.GatewayMicroservices.Base
{
    [SetUpFixture]
    internal abstract class PublishCustomerContractBase : BaseTest
    {
        //variables initialization
        protected const string OtherPartyFirstBalanceSupplier = "1982541287235";
        protected const string EanIdGas = "000425200000005595";
        protected const string EanIdElk = "000425200000044466";
        
        [OneTimeTearDown]
        public void OneTimeTearDown() => FakeEdsnAdminClient.ClearAllMpsOfferPhase();

        protected void AddContractNoticeToEdsn(FakeNoticeContractAck request, string reference) => 
            FakeEdsnAdminClient.SavePublishCustomerContractAcknowledgementMPs(reference, request);

        protected void AddCustomerKeysToEdsn(CreateCKResponseEnvelope request, string reference) => 
            FakeEdsnAdminClient.SaveCustomerApprovalKeysMPs(reference, request);
    }
}