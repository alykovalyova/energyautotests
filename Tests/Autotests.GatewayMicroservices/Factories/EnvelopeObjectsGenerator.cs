﻿using System;
using Nuts.FakeEdsn.Contracts;
using Nuts.FakeEdsn.Contracts.NoticeContract;
using Destination1 = Nuts.FakeEdsn.Contracts.NoticeContract.Destination1;
using EDSNBusinessDocumentHeader1 = Nuts.FakeEdsn.Contracts.NoticeContract.EDSNBusinessDocumentHeader1;
using PC1 = Nuts.FakeEdsn.Contracts.NoticeContract.PC1;
using PMP1 = Nuts.FakeEdsn.Contracts.NoticeContract.PMP1;
using Receiver1 = Nuts.FakeEdsn.Contracts.NoticeContract.Receiver1;
using RejectionPortaalType = Nuts.FakeEdsn.Contracts.NoticeContract.RejectionPortaalType;
using Source1 = Nuts.FakeEdsn.Contracts.NoticeContract.Source1;

namespace Autotests.Framework.Factories.Gtw
{
    internal static class EnvelopeObjectsGenerator
    {
        private const string RejectionCode = "202";
        private const string RejectionText = "EAN-code opvragende partij onbekend.";

        internal static PC1 WithNoticeContractPortalContent(string supplier, string externalReference, string eanId)
        {
            return new PC1
            {
                Portaal_MeteringPoint = new PMP1
                {
                    EANID = eanId,
                    MPCommercialCharacteristics = new MPCommercialCharacteristics
                    {
                        BalanceSupplier_Company = new BalanceSupplier_Company1 { ID = supplier }
                    },
                    Portaal_Rejection = new[]
                    {
                        new RejectionPortaalType
                        {
                            RejectionCode = RejectionCode,
                            RejectionText = RejectionText
                        }
                    },
                    Query = new Query1 { ExternalReference = externalReference }
                }
            };
        }

        internal static CreateCKResponseEnvelope_PC WithCustomerKeysPortalContent(string externalReference, string eanId)
        {
            return new CreateCKResponseEnvelope_PC
            {
                Portaal_MeteringPoint = new CreateCKResponseEnvelope_PC_PMP
                {
                    EANID = eanId,
                    Portaal_Mutation = new CreateCKResponseEnvelope_PC_PMP_PM { ExternalReference = externalReference },
                    Portaal_Rejection = new[]
                    {
                        new CreateCKResponseEnvelope_RejectionPortaalType
                        {
                            RejectionCode = RejectionCode,
                            RejectionText = RejectionText
                        }
                    }
                }
            };
        }

        internal static NoticeContractAcknowledgementEnvelope CreateNoticeCustomerContract()
        {
            return new NoticeContractAcknowledgementEnvelope
            {
                EDSNBusinessDocumentHeader = new EDSNBusinessDocumentHeader1
                {
                    MessageID = Guid.NewGuid().ToString(),
                    CreationTimestamp = DateTime.Now,
                    Destination = new Destination1
                    {
                        Receiver = new Receiver1
                        {
                            Authority = "EAN.UCC",
                            ContactTypeIdentifier = "EDSN",
                            ReceiverID = "8712423010208"
                        }
                    },
                    Source = new Source1
                    {
                        Authority = "EAN.UCC",
                        SenderID = "8714252018141",
                        ContactTypeIdentifier = "DDQ_M"
                    }
                },
                Portaal_Content = null
            };
        }

        internal static CreateCKResponseEnvelope CreateCustomerKeysApprovalRequest()
        {
            return new CreateCKResponseEnvelope
            {
                EDSNBusinessDocumentHeader = new CreateCKResponseEnvelope_EDSNBusinessDocumentHeader
                {
                    MessageID = Guid.NewGuid().ToString(),
                    CreationTimestamp = DateTime.Now,
                    Destination = new CreateCKResponseEnvelope_EDSNBusinessDocumentHeader_Destination
                    {
                        Receiver = new CreateCKResponseEnvelope_EDSNBusinessDocumentHeader_Destination_Receiver
                        {
                            ReceiverID = "8712423010208"
                        }
                    },
                    Source = new CreateCKResponseEnvelope_EDSNBusinessDocumentHeader_Source
                    {
                        ContactTypeIdentifier = "DDQ_M",
                        SenderID = "8714252018141"
                    }
                },
                Portaal_Content = null
            };
        }
    }
}