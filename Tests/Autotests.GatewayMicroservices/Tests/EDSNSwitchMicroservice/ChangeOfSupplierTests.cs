﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.GatewayMicroservices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using static Autotests.Helper.GtwFactory;

namespace Autotests.GatewayMicroservices.Tests.EDSNSwitchMicroservice
{
    [TestFixture]
    internal class ChangeOfSupplierTests : EdsnSwitchBase
    {
        [Test]
        public void ChangeOfSupplier_Returns200StatusCode()
        {
            var response = CreateChangeOfSupplierRequest(Ean, BalanceResponsibleId,
                    BalanceSupplierId, GridOperatorId, ReferenceId, int.Parse(BuildingNr),
                    Zipcode, KvkNumber, CurrentDate)
                .CallWith<ChangeOfSupplierRequest, ChangeOfSupplierResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.Rejection.Should().BeNull();
        }

        [Test]
        [
            TestCase(NotExistedEanId, BalanceResponsibleId, BalanceSupplierId, GridOperatorId, BuildingNr, Zipcode,
                PatternMessages.EanIdFieldRegexIsWrong,
                TestName = "ChangeOfSupplier_EanValidation_Returns400StatusCode"),
            TestCase(Ean, "123", BalanceSupplierId, GridOperatorId, BuildingNr, Zipcode,
                PatternMessages.BalanceResponsibleIdFieldRegexIsWrong,
                TestName = "ChangeOfSupplier_BalanceResponsibleIdValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, "123", GridOperatorId, BuildingNr, Zipcode,
                PatternMessages.BalanceSupplierIdFieldRegexIsWrong,
                TestName = "ChangeOfSupplier_BalanceSupplierIdValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, "123", BuildingNr, Zipcode,
                PatternMessages.GridOperatorIdFieldRegexIsWrong,
                TestName = "ChangeOfSupplier_GridOperatorIdValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, GridOperatorId, "0", Zipcode,
                PatternMessages.BuildingNrFieldIsWrong,
                TestName = "ChangeOfSupplier_BuildingNrValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, GridOperatorId, BuildingNr, InvalidZipcode,
                PatternMessages.ZipCodeFieldRegexIsWrong,
                TestName = "ChangeOfSupplier_ZipcodeValidation_Returns400StatusCode")]
        public void ChangeOfSupplier_ReturnsMessage_RegularExpressionValidationTests(
            string ean, string balanceResponsibleId,
            string balanceSupplierId, string gridOperatorId,
            string buildingNr, string zipcode,
            string validationMessage)
        {
            var response = CreateChangeOfSupplierRequest(ean, balanceResponsibleId, balanceSupplierId, gridOperatorId,
                    ReferenceId, int.Parse(buildingNr), zipcode, KvkNumber, CurrentDate)
                .CallWith<ChangeOfSupplierRequest, ChangeOfSupplierResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(validationMessage);
        }

        [Test]
        [
            TestCase("", BalanceResponsibleId, BalanceSupplierId, GridOperatorId, nameof(Ean),
                TestName = "ChangeOfSupplier_EanFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, "", BalanceSupplierId, GridOperatorId, nameof(BalanceResponsibleId),
                TestName = "ChangeOfSupplier_BalanceResponsibleIdFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, "", GridOperatorId, nameof(BalanceSupplierId),
                TestName = "ChangeOfSupplier_BalanceSupplierIdFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, "", nameof(GridOperatorId),
                TestName = "ChangeOfSupplier_GridOperatorIdFieldIsRequired_Returns400StatusCode")]
        public void ChangeOfSupplier_ChangeOfSupplier_ReturnsMessage(
            string ean, string balanceResponsibleId,
            string balanceSupplierId, string gridOperatorId,
            string requiredField)
        {
            var response = CreateChangeOfSupplierRequest(ean, balanceResponsibleId, balanceSupplierId, gridOperatorId,
                    ReferenceId, int.Parse(BuildingNr), Zipcode, KvkNumber, CurrentDate)
                .CallWith<ChangeOfSupplierRequest, ChangeOfSupplierResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.ResponseText.Should().Contain(string.Format(PatternMessages.FieldIsRequired, requiredField));
        }
    }
}