﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.GatewayMicroservices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using static Autotests.Helper.GtwFactory;

namespace Autotests.GatewayMicroservices.Tests.EDSNSwitchMicroservice
{
    [TestFixture]
    internal class MoveInTests : EdsnSwitchBase
    {
        [Test]
        public void MoveIn_Returns200StatusCode()
        {
            var response = CreateMoveInRequest(Ean, BalanceResponsibleId, BalanceSupplierId, GridOperatorId,
                    ReferenceId, MutationDate, KvkNumber, int.Parse(BuildingNr), Zipcode)
                .CallWith<MoveInRequest, MoveInResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.Rejection.Should().BeNull();
        }

        [Test]
        [
            TestCase(NotExistedEanId, BalanceResponsibleId, BalanceSupplierId, GridOperatorId, BuildingNr, Zipcode,
                PatternMessages.EanIdFieldRegexIsWrong, TestName = "MoveIn_EanValidation_Returns400StatusCode"),
            TestCase(Ean, "123", BalanceSupplierId, GridOperatorId, BuildingNr, Zipcode,
                PatternMessages.BalanceResponsibleIdFieldRegexIsWrong,
                TestName = "MoveIn_BalanceResponsibleIdValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, "123", GridOperatorId, BuildingNr, Zipcode,
                PatternMessages.BalanceSupplierIdFieldRegexIsWrong,
                TestName = "MoveIn_BalanceSupplierIdValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, "123", BuildingNr, Zipcode,
                PatternMessages.GridOperatorIdFieldRegexIsWrong,
                TestName = "MoveIn_GridOperatorIdValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, GridOperatorId, "0", Zipcode,
                PatternMessages.BuildingNrFieldIsWrong, TestName = "MoveIn_BuildingNrValidation_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, GridOperatorId, BuildingNr, InvalidZipcode,
                PatternMessages.ZipCodeFieldRegexIsWrong, TestName = "MoveIn_ZipcodeValidation_Returns400StatusCode")]
        public void MoveIn_ReturnsMessage_RegularExpressionValidationTests(
            string ean, string balanceResponsibleId,
            string balanceSupplierId, string gridOperatorId,
            string buildingNr, string zipcode,
            string validationMessage)
        {
            var response = CreateMoveInRequest(ean, balanceResponsibleId, balanceSupplierId, gridOperatorId,
                    ReferenceId, MutationDate, KvkNumber, int.Parse(buildingNr), zipcode)
                .CallWith<MoveInRequest, MoveInResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(validationMessage);
        }

        [Test]
        [
            TestCase("", BalanceResponsibleId, BalanceSupplierId, GridOperatorId, nameof(Ean),
                TestName = "MoveIn_EanFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, "", BalanceSupplierId, GridOperatorId, nameof(BalanceResponsibleId),
                TestName = "MoveIn_BalanceResponsibleIdFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, "", GridOperatorId, nameof(BalanceSupplierId),
                TestName = "MoveIn_BalanceSupplierIdFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, BalanceResponsibleId, BalanceSupplierId, "", nameof(GridOperatorId),
                TestName = "MoveIn_GridOperatorIdFieldIsRequired_Returns400StatusCode")]
        public void MoveIn_ReturnsMessage_RequiredFieldValidationTests(
            string ean, string balanceResponsibleId,
            string balanceSupplierId, string gridOperatorId,
            string requiredField)
        {
            var response = CreateMoveInRequest(ean, balanceResponsibleId, balanceSupplierId, gridOperatorId,
                    ReferenceId, MutationDate, KvkNumber, int.Parse(BuildingNr), Zipcode)
                .CallWith<MoveInRequest, MoveInResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.ResponseText.Should().Contain(string.Format(PatternMessages.FieldIsRequired, requiredField));
        }
    }
}