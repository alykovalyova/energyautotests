﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.GatewayMicroservices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using static Autotests.Helper.GtwFactory;

namespace Autotests.GatewayMicroservices.Tests.EDSNSwitchMicroservice
{
    [TestFixture]
    internal class MoveOutTests : EdsnSwitchBase
    {
        [Test]
        public void MoveOut_SuccessRequest_Return200OK()
        {
            var response = CreateMoveOutRequest(Ean, BalanceSupplierId, GridOperatorId, ReferenceId, CurrentDate)
                .CallWith<MoveOutRequest, MoveOutResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.Rejection.Should().BeNull();
        }

        [Test]
        [
            TestCase(NotExistedEanId, BalanceSupplierId, GridOperatorId, MutationDate,
                PatternMessages.EanIdFieldRegexIsWrong, TestName = "MoveOut_EanFieldRegexIsWrong_Returns400StatusCode"),
            TestCase(Ean, "123", GridOperatorId, MutationDate, PatternMessages.BalanceSupplierIdFieldRegexIsWrong,
                TestName = "MoveOut_BalanceSupplierIdFieldRegexIsWrong_Returns400StatusCode"),
            TestCase(Ean, BalanceSupplierId, "123", MutationDate, PatternMessages.GridOperatorIdFieldRegexIsWrong,
                TestName = "MoveOut_GridOperatorIdFieldRegexIsWrong_Returns400StatusCode"),
            TestCase(Ean, BalanceSupplierId, GridOperatorId, "1111-09-29T07:36:51.277Z",
                PatternMessages.DateTimeRangeRequirement,
                TestName = "MoveOut_DateTimeRangeRequirement_Returns400StatusCode")]
        public void MoveOut_ReturnsMessage_RegularExpressionValidationTests(
            string ean, string balanceSupplierId,
            string gridOperatorId, DateTime mutationDate,
            string validationMessage)
        {
            var response = CreateMoveOutRequest(ean, balanceSupplierId, gridOperatorId, ReferenceId, mutationDate)
                .CallWith<MoveOutRequest, MoveOutResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.ResponseText.Should().Contain(validationMessage);
        }

        [Test]
        [
            TestCase("", BalanceSupplierId, GridOperatorId, ReferenceId, nameof(Ean),
                TestName = "MoveOut_EanFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, "", GridOperatorId, ReferenceId, nameof(BalanceSupplierId),
                TestName = "MoveOut_BalanceSupplierIdFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, BalanceSupplierId, "", ReferenceId, nameof(GridOperatorId),
                TestName = "MoveOut_GridOperatorIdFieldIsRequired_Returns400StatusCode"),
            TestCase(Ean, BalanceSupplierId, GridOperatorId, "", nameof(ReferenceId),
                TestName = "MoveOut_ReferenceIdFieldIsRequired_Returns400StatusCode")]
        public void MoveOut_ReturnsMessage_RequiredFieldValidationTests(
            string ean, string balanceSupplierId,
            string gridOperatorId, string referenceId,
            string requiredField)
        {
            var response = CreateMoveOutRequest(ean, balanceSupplierId, gridOperatorId, referenceId, CurrentDate)
                .CallWith<MoveOutRequest, MoveOutResponse>(EdsnSwitchClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.ResponseText.Should().Contain(string.Format(PatternMessages.FieldIsRequired, requiredField));
        }
    }
}