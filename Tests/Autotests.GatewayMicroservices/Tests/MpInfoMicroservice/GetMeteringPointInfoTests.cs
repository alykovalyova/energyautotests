﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.GatewayMicroservices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPointInfoPhase.Contract;
using static Autotests.Helper.GtwFactory;

namespace Autotests.GatewayMicroservices.Tests.MpInfoMicroservice
{
    [TestFixture]
    internal class GetMeteringPointInfoTests : MpInfoBase
    {
        [Test]
        public void GetMeteringPointInfo_SearchMeteringPointsByEanId_ReturnsMpInfoJson()
        {
            var response = CreateGetMeteringPointInfoRequest(EanId)
                .CallWith<GetMeteringPointInfoRequest, GetMeteringPointInfoResponse>(MpInfoClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var mp = response.Data.MeteringPoints.First();
            mp.EanId.Should().BeEquivalentTo(EanId);
            mp.Address.ZIPCode.Should().BeEquivalentTo(Zipcode);
            mp.Address.BuildingNr.Should().Be(BuildingNr);
            mp.Address.ExBuildingNr.Should().BeEquivalentTo(ExBuildingNr);
        }

        [Test]
        public void GetMeteringPointInfo_SearchMeteringPointsByZipcodeAndBuildNr_ReturnsMpInfoJson()
        {
            var response = CreateGetMeteringPointInfoRequest(zipCode: Zipcode, buildingNr: BuildingNr)
                .CallWith<GetMeteringPointInfoRequest, GetMeteringPointInfoResponse>(MpInfoClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var mp = response.Data.MeteringPoints.First();
            mp.EanId.Should().BeEquivalentTo(EanId);
            mp.Address.ZIPCode.Should().BeEquivalentTo(Zipcode);
            mp.Address.BuildingNr.Should().Be(BuildingNr);
            mp.Address.ExBuildingNr.Should().BeEquivalentTo(ExBuildingNr);
        }

        [Test]
        public void GetMeteringPointInfo_SearchMeteringPointsByZipcodeBuildNrAndExZipcode_ReturnsMpInfoJson()
        {
            var response = CreateGetMeteringPointInfoRequest(zipCode: Zipcode, buildingNr: BuildingNr, exBuildingNr: ExBuildingNr)
                .CallWith<GetMeteringPointInfoRequest, GetMeteringPointInfoResponse>(MpInfoClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var mp = response.Data.MeteringPoints.First();
            mp.EanId.Should().BeEquivalentTo(EanId);
            mp.Address.ZIPCode.Should().BeEquivalentTo(Zipcode);
            mp.Address.BuildingNr.Should().Be(BuildingNr);
            mp.Address.ExBuildingNr.Should().BeEquivalentTo(ExBuildingNr);
        }

        [Test]
        public void GetMeteringPointInfo_SearchNotExistedMeteringPoints_ReturnsEmptyMpJObject()
        {
            var response = CreateGetMeteringPointInfoRequest(NotExistedEanId)
                .CallWith<GetMeteringPointInfoRequest, GetMeteringPointInfoResponse>(MpInfoClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().BeEmpty();
        }

        [Test]
        [TestCase("", "", BuildingNr, "", PatternMessages.NoRequiredParams, TestName = "GetMeteringPointInfo_SearchMeteringPointsByBuildNrOnly_Returns400StatusCode")]
        [TestCase("", "", 0, ExBuildingNr, PatternMessages.NoRequiredParams, TestName = "GetMeteringPointInfo_SearchMeteringPointsByExBuildNrOnly_Returns400StatusCode")]
        [TestCase("", "", 0, "", PatternMessages.NoRequiredParams, TestName = "GetMeteringPointInfo_SearchMeteringPointsWithoutAnyParams_Returns400StatusCode")]
        [TestCase(EanId, Zipcode, BuildingNr, ExBuildingNr, PatternMessages.AreRestrictedParams, TestName = "GetMeteringPointInfo_SearchMeteringPointsByAllParams_Returns400StatusCode")]
        public void GetMeteringPointInfo_Test_Returns400StatusCode(
            string eanId, string Zipcode, int? buildingNr,
            string exBuildingNr, string messagePattern)
        {
            var response = CreateGetMeteringPointInfoRequest(
                string.IsNullOrEmpty(eanId) ? null : eanId,
                buildingNr == 0 ? null : buildingNr,
                string.IsNullOrEmpty(exBuildingNr) ? null : exBuildingNr,
                string.IsNullOrEmpty(Zipcode) ? null : Zipcode).CallWith(MpInfoClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(messagePattern);
        }
    }
}