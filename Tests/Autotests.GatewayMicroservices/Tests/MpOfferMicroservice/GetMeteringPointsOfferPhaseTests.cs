﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.MeteringPointOfferPhase.Contract;
using System.Net;
using Autotests.GatewayMicroservices.Base;
using Autotests.Helper;
using Autotests.Helper.Builders.FakeEdsn.OfferPhase;
using Autotests.Helper.Builders.MeteringPointPhases;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Helper.Enums;
using static Autotests.Helper.GtwFactory;
using Rejection = Autotests.Helper.Models.Rejection;

namespace Autotests.GatewayMicroServices.Tests.MpOfferMicroservice
{
    [TestFixture]
    internal class GetMeteringPointsOfferPhaseTests : MpOfferBase
    {
        [Test]
        public void GetMeteringPointsOfferPhase_DefaultValidTest_Returns200()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = GtwFactory.CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .CallWith<GetMeteringPointsOfferPhaseRequest, GetMeteringPointsOfferPhaseResponse>(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().NotBeEmpty();
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == _eanId);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithoutEapPeaks_Returns200()
        {
            InitializeMpOfferPhaseResponse(_eanId, withEapPeaks: false);
            var response = GtwFactory.CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .CallWith<GetMeteringPointsOfferPhaseRequest, GetMeteringPointsOfferPhaseResponse>(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().NotBeEmpty();
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == _eanId);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithoutAnyConnectionDetails_Returns400()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response =GtwFactory.CreateGetMeteringPointsOfferPhaseRequest().CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WithoutAnyConnectionDetails);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithWrongBirthdayKeyRegex_Returns400(
            [Values("--1-01", "-01", "11-01", "--11-1", "--11-", "--13-01", "--11-32", "--ff-ff")] string birthdayKey)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = GtwFactory.CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .SetBirthdayKey(0, birthdayKey).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongBirthdayKeyRegex);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithEmptyBirthdayKey_Returns200()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .SetBirthdayKey(0, "")
                .CallWith<GetMeteringPointsOfferPhaseRequest, GetMeteringPointsOfferPhaseResponse>(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().NotBeEmpty();
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == _eanId);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithWrongIbanKeyRegex_Returns400(
            [Values("12", "1234", "fff")] string ibanKey)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .SetIbanKey(0, ibanKey).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongIbanKeyRegex);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithEmptyIbanKey_Returns200()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .SetIbanKey(0, "")
                .CallWith<GetMeteringPointsOfferPhaseRequest, GetMeteringPointsOfferPhaseResponse>(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().NotBeEmpty();
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == _eanId);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithEmptyIbanAndBirthdayKeys_Returns400()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .SetIbanKey(0, null).SetBirthdayKey(0, null).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.BothIbanAndBirthDayKeyNotSpecified);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithInvalidPostalCodeRegex_Returns400(
            [Values("111QQ", "11111QQ", "1111Q", "1111QQQ", "QQ1111", "1111##", "QQQQ11")] string postalCode)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest(postalCode: postalCode)
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongPostalCodeRegex);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_EmptyPostalCode_Returns400(
            [Values("", null)] string postalCode)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest(postalCode: postalCode)
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.FieldIsRequired, "PostalCode"));
        }

        [Test]
        [TestCase("00042520000000777", SupplierEan, PatternMessages.InvalidEanField, TestName = "GetMeteringPointsOfferPhase_EanIdShorter18_Returns400")]
        [TestCase("0004252000000077777", SupplierEan, PatternMessages.InvalidEanField, TestName = "GetMeteringPointsOfferPhase_EanIdLonger18_Returns400")]
        [TestCase("ffffffffffffffffff", SupplierEan, PatternMessages.InvalidEanField, TestName = "GetMeteringPointsOfferPhase_EanIdWithChars_Returns400")]
        [TestCase(EanId, "871425201814", PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPointsOfferPhase_SupplierIdShorter13_Returns400")]
        [TestCase(EanId, "87142520181411", PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPointsOfferPhase_SupplierIdLonger13_Returns400")]
        [TestCase(EanId, "fffffffffffff", PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPointsOfferPhase_SupplierIdWithChars_Returns400")]
        public void GetMeteringPointsOfferPhase_InvalidEanAndSupplierIds_Returns400(string eanId, string supplierId, string message)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, eanId, ExternalRef, supplierId).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithEmptyEanField_Returns400()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, string.Empty, ExternalRef, SupplierEan).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "EanId");
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithEmptySupplierField_Returns400()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, EanId, ExternalRef, string.Empty).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "SupplierEan");
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithNotExistedEanId_Returns412()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, NotExistedEanId, ExternalRef, SupplierEan).CallWith(MpOfferClient);
            var rejection = response.ResponseText.GetJTokenAs<Rejection>("rejection");

            response.Header.StatusCode.Should().Be(HttpStatusCode.PreconditionFailed);
            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));
            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.EndsWith("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }

        [Test]
        public void GetMeteringPointsOfferPhase_EanIsNotRecognized_Returns412()
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, NotRecognizedEan, ExternalRef, SupplierEan).CallWith(MpOfferClient);
            var rejection = response.ResponseText.GetJTokenAs<Rejection>("rejection");

            response.Header.StatusCode.Should().Be(HttpStatusCode.PreconditionFailed);
            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));
            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.Equals("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }

        [Test]
        [TestCase(false, "true")]
        [TestCase(true, null)]
        [TestCase(false, null)]
        public void GetMeteringPointsOfferPhase_FaceToFaceWithInvalidCheckboxes_Returns400(bool hasOptedInByCheckbox, string requestOrigin)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .SetPermissionType(0, PermissionType.FaceToFace)
                .SetHasOptedInByCheckbox(0, hasOptedInByCheckbox)
                .SetRequestOrigin(0, requestOrigin).CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ErrorForFaceToFaceType);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_CheckGenderField_Returns200([Values(Gender.Male, Gender.Female)] Gender gender)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var request = CreateGetMeteringPointsOfferPhaseRequest().WithGasConnection(0, _eanId, ExternalRef, SupplierEan);
            request.MeteringPointDetails.First().PermissionGrand.PermissionInfo.Customer.Gender = gender.ToString();

            var response = request.CallWith<GetMeteringPointsOfferPhaseRequest, GetMeteringPointsOfferPhaseResponse>(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().NotBeEmpty();
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == _eanId);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_InvalidGenderField_Returns400([Values("mail", "female", "123", "", "!@#$%^&*()")] string gender)
        {
            InitializeMpOfferPhaseResponse(_eanId);
            var request = CreateGetMeteringPointsOfferPhaseRequest().WithGasConnection(0, _eanId, ExternalRef, SupplierEan);
            request.MeteringPointDetails.First().PermissionGrand.PermissionInfo.Customer.Gender = gender;

            var response = request.CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.InvalidGender);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithoutPhysicalCharacteristics_ReturnsError()
        {
            var portalContent = BuildDirector.Get<OfferPhaseBuilder>()
                .SetEanId(_eanId)
                .SetEnergyMeter(PcDefaultData.EnergyMeter)
                .SetPortalMutation(PcDefaultData.Mutation)
                .SetProductType(TestConstants.GasProductType)
                .SetAddress(PcDefaultData.EdsnAddress)
                .SetValidFromDate(DateTime.Now)
                .Build();
            var mpOfferPhase = new GetMPInformationResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetMPInformationResponseEnvelope_EDSNBusinessDocumentHeader(),
                Portaal_Content = new GetMPInformationResponseEnvelope_PC() { Item = portalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsOfferPhase();
            FakeEdsnAdminClient.SaveOfferPhaseMeteringPoint(new List<GetMPInformationResponseEnvelope> { mpOfferPhase });

            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

        [Test]
        public void GetMeteringPointsOfferPhase_WithoutAddress_ReturnsError()
        {
            var portalContent = BuildDirector.Get<OfferPhaseBuilder>()
                .SetEanId(_eanId)
                .SetEnergyMeter(PcDefaultData.EnergyMeter)
                .SetPortalMutation(PcDefaultData.Mutation)
                .SetProductType(TestConstants.GasProductType)
                .SetPhysicalCharacteristics(PcDefaultData.PhysicalCharacteristics)
                .SetValidFromDate(DateTime.Now)
                .Build();
            var mpOfferPhase = new GetMPInformationResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetMPInformationResponseEnvelope_EDSNBusinessDocumentHeader(),
                Portaal_Content = new GetMPInformationResponseEnvelope_PC() { Item = portalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsOfferPhase();
            FakeEdsnAdminClient.SaveOfferPhaseMeteringPoint(new List<GetMPInformationResponseEnvelope> { mpOfferPhase });

            var response = CreateGetMeteringPointsOfferPhaseRequest()
                .WithGasConnection(0, _eanId, ExternalRef, SupplierEan)
                .CallWith(MpOfferClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }
    }
}