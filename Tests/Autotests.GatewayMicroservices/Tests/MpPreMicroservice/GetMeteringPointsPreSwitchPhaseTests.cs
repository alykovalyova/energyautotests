﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.GatewayMicroservices.Base;
using Autotests.Helper.Builders.FakeEdsn.PreSwitchPhase;
using Autotests.Helper.DataProviders.FakeEdsn;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPointPrePhase.Contract;
using static Autotests.Helper.GtwFactory;
using Rejection = Autotests.Helper.Models.Rejection;

namespace Autotests.GatewayMicroservices.Tests.MpPreMicroservice
{
    [TestFixture, SingleThreaded]
    internal class GetMeteringPointsPreSwitchPhaseTests : MpPreBase
    {
        [Test]
        public void GetMeteringPointsPreSwitchPhase_DefaultValidTest_Returns200()
        {
            SaveValidEanToFake(EanId);
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(EanId, ExternalRef, SupplierEan)
                .CallWith<GetMeteringPointsPreSwitchPhaseRequest, GetMeteringPointsPreSwitchPhaseResponse>(MpPreClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == EanId);
        }

        [Test]
        [TestCase("00042520000000777", SupplierEan, ExternalRef, PatternMessages.InvalidEanField, TestName = "GetMeteringPointsPreSwitchPhase_EanIdShorter18_Returns400")]
        [TestCase("0004252000000077777", SupplierEan, ExternalRef, PatternMessages.InvalidEanField, TestName = "GetMeteringPointsPreSwitchPhase_EanIdLonger18_Returns400")]
        [TestCase("ffffffffffffffffff", SupplierEan, ExternalRef, PatternMessages.InvalidEanField, TestName = "GetMeteringPointsPreSwitchPhase_EanIdWithChars_Returns400")]
        [TestCase(EanId, "871425201814", ExternalRef, PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPointsPreSwitchPhase_SupplierIdShorter13_Returns400")]
        [TestCase(EanId, "87142520181411", ExternalRef, PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPointsPreSwitchPhase_SupplierIdLonger13_Returns400")]
        [TestCase(EanId, "fffffffffffff", ExternalRef, PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPointsPreSwitchPhase_SupplierIdWithChars_Returns400")]
        public void GetMeteringPointsPreSwitchPhase_InvalidFields_Returns400(string eanId, string supplierEan, string externalRef, string message)
        {
            SaveValidEanToFake(EanId);
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(eanId, externalRef, supplierEan)
                .CallWith(MpPreClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointsPreSwitchPhase_WithEmptyEanField_Returns400()
        {
            SaveValidEanToFake(EanId);
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(string.Empty, ExternalRef, SupplierEan)
                .CallWith(MpPreClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "EanId");
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointsPreSwitchPhase_WithEmptySupplierField_Returns400()
        {
            SaveValidEanToFake(EanId);
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(EanId, ExternalRef, string.Empty)
                .CallWith(MpPreClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "SupplierEan");
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointsPreSwitchPhase_WithNotExistedEan_Returns412()
        {
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(NotExistedEanId, ExternalRef, SupplierEan)
                .CallWith(MpPreClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.PreconditionFailed);
            var rejection = response.ResponseText.GetJTokenAs<Rejection>("rejection");

            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));
            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.Equals("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }

        [Test]
        public void GetMeteringPointsPreSwitchPhase_EanIsNotRecognized_Returns412()
        {
            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(NotRecognizedEan, ExternalRef, SupplierEan)
                .CallWith(MpPreClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.PreconditionFailed);
            var rejection = response.ResponseText.GetJTokenAs<Rejection>("rejection");

            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));

            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.Equals("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }

        [Test]
        public void GetMeteringPointsPreSwitchPhase_WithoutEnergyMeter_Returns200()
        {
            var eanPortalContent = BuildDirector.Get<PreSwitchPhaseBuilder>()
                .SetEanId(EanId)
                .SetGridOperator(PcDefaultValues.GridOperatorCompany)
                .SetAddress(PcDefaultValues.Address)
                .SetProductType(GasProductType)
                .SetPhysicalCharacteristics(PcDefaultValues.PhysicalCharacteristics)
                .SetPortalMutation(PcDefaultValues.Mutation)
                .SetMarketSegment(TestConstants.MarketSegment)
                .Build();
            var getMpPreSwitchResponse = new GetSCMPInformationResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetSCMPInformationResponseEnvelope_EDSNBusinessDocumentHeader(),
                Portaal_Content = new GetSCMPInformationResponseEnvelope_PC()
                { Item = eanPortalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();
            FakeEdsnAdminClient.SavePreSwitchPhaseMeteringPoint(new List<GetSCMPInformationResponseEnvelope> { getMpPreSwitchResponse });

            var response = CreateGetMeteringPointsPreSwitchPhaseRequest(EanId, ExternalRef, SupplierEan)
                .CallWith<GetMeteringPointsPreSwitchPhaseRequest, GetMeteringPointsPreSwitchPhaseResponse>(MpPreClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().Contain(mp => mp.EnergyMeter == null);
        }
    }
}