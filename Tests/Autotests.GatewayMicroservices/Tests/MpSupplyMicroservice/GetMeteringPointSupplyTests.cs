﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.GatewayMicroservices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPointSupplyPhase.Contract;
using Rejection = Autotests.Helper.Models.Rejection;
using static Autotests.Helper.GtwFactory;

namespace Autotests.GatewayMicroservices.Tests.MpSupplyMicroservice
{
    [TestFixture]
    internal class GetMeteringPointSupplyTests : MpSupplyBase
    {
        [Test]
        public void GetMeteringPointSupply_DefaultValidTest_Returns200()
        {
            var response = CreateGetMeteringPointSupplyRequest(EanId)
                .CallWith<GetMeteringPointSupplyRequest, GetMeteringPointSupplyResponse>(MpSupplyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.MeteringPoint.EanId.Should().BeEquivalentTo(EanId);
        }

        [Test]
        public void GetMeteringPointSupply_WithoutEanId_Returns404()
        {
            CreateGetMeteringPointSupplyRequest().CallWith(MpSupplyClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
        }

        [Test]
        [TestCase("77777777777777777", PatternMessages.EanRegexIsWrong, TestName = "GetMeteringPointSupply_WithEanLengthLess18_Returns400")]
        [TestCase("7777777777777777777", PatternMessages.EanRegexIsWrong, TestName = "GetMeteringPointSupply_WithEanLengthMore18_Returns400")]
        [TestCase("FFFFFFFFFFFFFFFFFF", PatternMessages.EanRegexIsWrong, TestName = "GetMeteringPointSupply_WithWrongEanRegex_Returns400")]
        public void GetMeteringPointSupply_WithInvalidEanIdRegex_Returns400(string eanId, string message)
        {
            var response = CreateGetMeteringPointSupplyRequest(eanId).CallWith(MpSupplyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPointSupply_WithWrongEanId_Returns412()
        {
            var response = CreateGetMeteringPointSupplyRequest(WrongEanId).CallWith(MpSupplyClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.PreconditionFailed);

            var rejection = response.ResponseText.GetJTokenAs<Rejection>("rejection");
            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                    PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));
            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.Equals("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }
    }
}