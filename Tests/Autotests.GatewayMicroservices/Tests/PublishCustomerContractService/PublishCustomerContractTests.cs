﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Framework.Factories.Gtw;
using Autotests.GatewayMicroservices.Base;
using FluentAssertions;
using NUnit.Framework;
using static Autotests.Helper.GtwFactory;

namespace Autotests.GatewayMicroservices.Tests.PublishCustomerContractService
{
    [TestFixture, SingleThreaded]
    internal class PublishCustomerContractTests : PublishCustomerContractBase
    {
        [Test]
        public void PublishCustomerContract_ValidValues_Returns200()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            var noticeCustomerContract = EnvelopeObjectsGenerator.CreateNoticeCustomerContract();

            AddContractNoticeToEdsn(noticeCustomerContract, externalReference);

            var response = CreatePublishCustomerContractRequest(OtherPartyFirstBalanceSupplier, externalReference, EanIdGas)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Header.Message.Should().BeEmpty();
            response.EdsnRejections.Should().BeEmpty();
        }

        [Test]
        public void PublishCustomerContract_WithRejectedCustomerContract_Returns500()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            var noticeCustomerContract = EnvelopeObjectsGenerator.CreateNoticeCustomerContract();
            noticeCustomerContract.Portaal_Content = EnvelopeObjectsGenerator
                .WithNoticeContractPortalContent(OtherPartyFirstBalanceSupplier, externalReference, EanIdGas);

            AddContractNoticeToEdsn(noticeCustomerContract, externalReference);

            var response = CreatePublishCustomerContractRequest(OtherPartyFirstBalanceSupplier, externalReference, EanIdGas)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.EdsnRejectionName);
        }

        [Test]
        public void PublishCustomerApprovalKey_ValidValues_Returns200()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            var customerKeysApprovalRequest = EnvelopeObjectsGenerator.CreateCustomerKeysApprovalRequest();

            AddCustomerKeysToEdsn(customerKeysApprovalRequest, externalReference);

            var response = PublishCustomerApprovalKey(externalReference, EanIdGas)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerApprovalKey);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Header.Message.Should().BeEmpty();
            response.Rejections.Should().BeEmpty();
        }

        [Test]
        public void PublishCustomerApprovalKey_RejectedCustomerApprovalKeys_Returns500()
        {
            var externalReference = RandomDataProvider.GetRandomNumbersString(10);
            var customerKeysApprovalRequest = EnvelopeObjectsGenerator.CreateCustomerKeysApprovalRequest();
            customerKeysApprovalRequest.Portaal_Content = EnvelopeObjectsGenerator.WithCustomerKeysPortalContent(externalReference, EanIdGas);

            AddCustomerKeysToEdsn(customerKeysApprovalRequest, externalReference);

            var response = PublishCustomerApprovalKey(externalReference, EanIdGas)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerApprovalKey);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.EdsnRejectionName);
        }

        [Test]
        public void PublishCustomerContract_WithEanIsNull_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);

            var response = CreatePublishCustomerContractRequest(OtherPartyFirstBalanceSupplier, externalReference, null)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(string.Format(PatternMessages.FieldIsRequired, "Ean"));
        }

        [Test]
        public void PublishCustomerContract_WithSupplierIsNull_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);

            var response = CreatePublishCustomerContractRequest(null, externalReference, EanIdElk)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should()
                .BeEquivalentTo(string.Format(PatternMessages.FieldIsRequired, "BalanceSupplier"));
        }

        [Test]
        public void PublishCustomerContract_WithWrongSupplier_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomNumbersString(10);
            var balanceSupplier = OtherPartyFirstBalanceSupplier.Remove(12);

            var response = CreatePublishCustomerContractRequest(balanceSupplier, externalReference, EanIdElk)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.BalanceSupplierWrong);
        }

        [Test]
        public void PublishCustomerContract_WithWrongEan_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            var ean = EanIdElk.Remove(17);

            var response = CreatePublishCustomerContractRequest(OtherPartyFirstBalanceSupplier, externalReference, ean)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.EanWrong);
        }

        [Test]
        public void PublishCustomerContract_WithoutExReference_Returns400()
        {
            var response = CreatePublishCustomerContractRequest(OtherPartyFirstBalanceSupplier, null, EanIdElk)
                 .CallWith(PublishCustomerClient.Proxy.PublishCustomerContract);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(string.Format(PatternMessages.FieldIsRequired, "ExternalReference"));
        }

        [Test]
        public void PublishCustomerApprovalKey_WithWrongBirthdayKey_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            const string birthDayKey = "-12";

            var response = PublishCustomerApprovalKey(externalReference, EanIdElk, birthDayKey)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerApprovalKey);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.BirthdayKeyWrong);
        }

        [Test]
        public void PublishCustomerApprovalKey_WithIbanKeyIsEmpty_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            var ibanKey = string.Empty;

            var response = PublishCustomerApprovalKey(externalReference, EanIdElk, ibanKey: ibanKey)
                .CallWith(PublishCustomerClient.Proxy.PublishCustomerApprovalKey);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.IbanWrongNl);
        }

        [Test]
        public void PublishCustomerApprovalKey_WithWrongIbanKey_Returns400()
        {
            var externalReference = RandomDataProvider.GetRandomLettersString(10);
            const string ibanKey = "12";
            var response = PublishCustomerApprovalKey(externalReference, EanIdElk,
                ibanKey: ibanKey).CallWith(PublishCustomerClient.Proxy.PublishCustomerApprovalKey);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.IbanWrong);
        }
    }
}