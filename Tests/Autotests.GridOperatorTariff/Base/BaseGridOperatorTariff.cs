﻿using Allure.Commons;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.GridOperatorTariff.Builders;
using Autotests.GridOperatorTariff.Enums;
using Autotests.Repositories.GridOperatorTariffModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Nuts.GridOperatorTariff.Contract.Rest.Transport;
using System.Globalization;
using System.Net;
using System.Net.Http;
using StatusHistory = Autotests.Repositories.GridOperatorTariffModels.StatusHistory;

namespace Autotests.GridOperatorTariff.Base
{
    [SetUpFixture, Category(Core.Helpers.Categories.GoTariff)]
    internal abstract class BaseGridOperatorTariff : AllureReport
    {
        //clients declaration
        protected NutsHttpClient GOTariffClient;
        protected FakeEdsnAdminClient FakeEdsnClient;
        protected RestSchedulerClient Scheduler;

        //databases declaration
        protected DbHandler<GridOperatorTariffContext> GoTariffDb;
        protected DbHandler<MarketPartyContext> MarketPartyDb;

        //Variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        private readonly Random _random = new Random();
        private readonly List<int> _goTariffIdsToRemove = new List<int>();
        protected BuildDirector BuildDirector;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {          
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);

            GOTariffClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.GOTariff));
            FakeEdsnClient = new FakeEdsnAdminClient();
            Scheduler = new RestSchedulerClient();
            BuildDirector = new BuildDirector();

            GoTariffDb = new DbHandler<GridOperatorTariffContext>(_configHandler.GetConnectionString(DbConnectionName.GridOperatorTariffDatabase.ToString()));
            MarketPartyDb = new DbHandler<MarketPartyContext>(_configHandler.GetConnectionString(DbConnectionName.MarketPartyDatabase.ToString()));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => CleanDb();

        [SetUp]
        public void SetUp() => FakeEdsnClient.ClearAllCapacityTariffs();

        private void CleanDb()
        {
            _goTariffIdsToRemove.ForEach(id =>
            {
                GoTariffDb.DeleteEntitiesByCondition<Tariff>(t => t.GridOperatorTariffId.Equals(id));
                GoTariffDb.DeleteEntitiesByCondition<StatusHistory>(sh => sh.GridOperatorTariffId.Equals(id));
                GoTariffDb.DeleteEntityByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(e => e.Id.Equals(id));
            });
        }

        protected Repositories.GridOperatorTariffModels.GridOperatorTariff AddGoTariffToDb(GoTariffStatus status)
        {
            const long capTarCode = 8710000004442;
            var defaultDate = DateTime.Today.AddDays(-10);
            var gridOperator = MarketPartyDb.GetEntitiesByCondition<MarketParty>(mp => true)
                .RandomItem();
            var goTariff = new Repositories.GridOperatorTariffModels.GridOperatorTariff
            {
                CreatedOn = defaultDate,
                ModifiedOn = defaultDate,
                StatusId = (short)status,
                GridOperator = gridOperator.Ean,
                Currency = "EUR",
                QueryDate = defaultDate,
                DateFrom = defaultDate,
                EdsnGridOperatorTariffId = _random.Next(1, 99).ToString(),
                Tariff = new List<Tariff>
                {
                    new Tariff
                    {
                        CapTarCode = capTarCode.ToString(),
                        CreatedOn = defaultDate,
                        DayTariff = 12.0M,
                        ProductTypeId = 1
                    }
                }
            };
            var res = GoTariffDb.AddEntityToDb(goTariff);
            _goTariffIdsToRemove.Add(res.Id);
            return res;
        }

        protected Repositories.GridOperatorTariffModels.GridOperatorTariff SaveFakeTariff()
        {
            const long capTarCode = 8710000004442;
            var defaultDate = DateTime.Today.AddDays(-10);
            var gridOperator = MarketPartyDb.GetEntityByCondition<MarketParty>(mp => true);
            // create FakeTariff response
            var fakeTariffToSave = BuildDirector.Get<TariffsResponseEnvelopeBuilder>()
                .SetDateFrom(defaultDate).SetQueryDate(defaultDate)
                .SetFinancialCharacteristics(TariffsResponseEnvelope_CurrencyEuroCode.EUR)
                .SetGridOperator_Company(gridOperator.Ean).SetTariffId(_random.Next(1, 99).ToString())
                .SetPortaalMeteringPoint(12.0M, capTarCode.ToString(), TariffsResponseEnvelope_EnergyProductPortaalTypeCode.ELK)
                .Build();
            // save FakeTariff into FakeEdsn and trigger job
            FakeEdsnClient.SaveCapacityTariffs(new List<TariffsResponseEnvelope_Portaal_Content_Query>
                {fakeTariffToSave});
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnGateway_GetCapacityTariffsFakeJob);
            // wait for saved tariff in DB
            var tariffFromDb = Waiter.Wait(() =>
                GoTariffDb.GetEntityByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got =>
                    got.EdsnGridOperatorTariffId.Equals(fakeTariffToSave.ID), "Tariff"));
            _goTariffIdsToRemove.Add(tariffFromDb.Id);

            return tariffFromDb;
        }

        protected void AcceptTariff(int id)
        {
            const string modifiedBy = "Autotest";
            // approve tariff
            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(id).SetStatus(GoTariffStatus.Approved.ToString()).SetModifiedBy(modifiedBy).Build()
            }.CallWith<UpdateGridOperatorTariffStatusRequest, UpdateGridOperatorTariffStatusResponse>(GOTariffClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            // accept tariff
            response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(id).SetStatus(GoTariffStatus.Accepted.ToString()).SetModifiedBy(modifiedBy).Build()
            }.CallWith<UpdateGridOperatorTariffStatusRequest, UpdateGridOperatorTariffStatusResponse>(GOTariffClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
        }
    }
}