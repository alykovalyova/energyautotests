﻿using Autotests.Core.Helpers;
using Nuts.FakeEdsn.Contracts;
using System;

namespace Autotests.GridOperatorTariff.Builders
{
    internal class TariffsResponseEnvelopeBuilder : MainBuilder<TariffsResponseEnvelope_Portaal_Content_Query>
    {
        internal TariffsResponseEnvelopeBuilder SetDateFrom(DateTime date)
        {
            Instance.DateFrom = date;
            return this;
        }

        internal TariffsResponseEnvelopeBuilder SetQueryDate(DateTime date)
        {
            Instance.QueryDate = date;
            return this;
        }

        internal TariffsResponseEnvelopeBuilder SetFinancialCharacteristics(
            TariffsResponseEnvelope_CurrencyEuroCode currency)
        {
            Instance.FinancialCharacteristics =
                new TariffsResponseEnvelope_Portaal_Content_Query_FinancialCharacteristics
                {
                    Currency = currency
                };
            return this;
        }

        internal TariffsResponseEnvelopeBuilder SetGridOperator_Company(string id)
        {
            Instance.GridOperator_Company = new TariffsResponseEnvelope_Portaal_Content_Query_GridOperator_Company
            {
                ID = id
            };
            return this;
        }

        internal TariffsResponseEnvelopeBuilder SetTariffId(string id)
        {
            Instance.ID = id;
            return this;
        }

        internal TariffsResponseEnvelopeBuilder SetPortaalMeteringPoint(
            decimal dayTariff, string capTarCode, TariffsResponseEnvelope_EnergyProductPortaalTypeCode productType)
        {
            Instance.Portaal_MeteringPoint = new[]
            {
                new TariffsResponseEnvelope_Portaal_Content_Query_Portaal_MeteringPoint
                {
                    MPCommercialCharacteristics =
                        new
                            TariffsResponseEnvelope_Portaal_Content_Query_Portaal_MeteringPoint_MPCommercialCharacteristics
                            {
                                DayTariff = dayTariff
                            },
                    MPPhysicalCharacteristics =
                        new
                            TariffsResponseEnvelope_Portaal_Content_Query_Portaal_MeteringPoint_MPPhysicalCharacteristics
                            {
                                CapTarCode = capTarCode
                            },
                    ProductType = productType
                }
            };
            return this;
        }
    }
}