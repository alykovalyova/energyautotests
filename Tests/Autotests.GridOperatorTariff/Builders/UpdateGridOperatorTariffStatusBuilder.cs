﻿using Autotests.Core.Helpers;
using Nuts.GridOperatorTariff.Contract.Rest.Model;

namespace Autotests.GridOperatorTariff.Builders
{
    internal class UpdateGridOperatorTariffStatusBuilder : MainBuilder<UpdateGridOperatorTariffStatus>
    {
        internal UpdateGridOperatorTariffStatusBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal UpdateGridOperatorTariffStatusBuilder SetStatus(string status)
        {
            Instance.Status = status;
            return this;
        }

        internal UpdateGridOperatorTariffStatusBuilder SetModifiedBy(string modifiedBy)
        {
            Instance.ModifiedBy = modifiedBy;
            return this;
        }
    }
}