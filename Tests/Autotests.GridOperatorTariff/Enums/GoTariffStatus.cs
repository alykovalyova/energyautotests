﻿namespace Autotests.GridOperatorTariff.Enums
{
    internal enum GoTariffStatus
    {
        New = 1,
        Approved,
        Rejected,
        Accepted,
    }
}