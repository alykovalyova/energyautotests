﻿using Autotests.Clients;
using Autotests.GridOperatorTariff.Base;
using Autotests.GridOperatorTariff.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.GridOperatorTariff.Contract.Rest.Transport;
using System.Linq;
using System.Net;

namespace Autotests.GridOperatorTariff.Tests
{
    [TestFixture]
    internal class GridOperatorTariffIntegrationTests : BaseGridOperatorTariff
    {
        [Test]
        public void Integration_SearchGridOperatorTariffs_DefaultValidCase_Returns200()
        {
            var savedTariff = SaveFakeTariff();
            var allGoTariffs = GoTariffDb.GetEntitiesByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got =>
                got.GridOperator.Equals(savedTariff.GridOperator) && got.StatusId.Equals(savedTariff.StatusId));

            var response = new SearchGridOperatorTariffsRequest
            {
                GridOperatorEan = savedTariff.GridOperator,
                Status = ((GoTariffStatus)savedTariff.StatusId).ToString()
            }.CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.GridOperatorTariffs.Should().NotBeEmpty().And.Subject.Should().HaveCount(allGoTariffs.Count);
        }

        [Test]
        public void Integration_SearchTariffsByCaptarCode_DefaultValidCase_Returns200()
        {
            var savedTariff = SaveFakeTariff();
            AcceptTariff(savedTariff.Id);
            var tariff = savedTariff.Tariff.FirstOrDefault();

            var response = new SearchTariffsByCaptarCodeRequest
            {
                Code = tariff?.CapTarCode,
                StartDate = tariff?.CreatedOn.AddDays(-10)
            }.CallWith<SearchTariffsByCaptarCodeRequest, SearchTariffsByCaptarCodeResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.TariffInfos.Should().NotBeEmpty();
        }
    }
}