﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.GridOperatorTariff.Base;
using Autotests.GridOperatorTariff.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.GridOperatorTariff.Contract.Rest.Transport;
using System.Net;

namespace Autotests.GridOperatorTariff.Tests
{
    [TestFixture]
    internal class SearchGridOperatorTariffsTests : BaseGridOperatorTariff
    {
        [Test]
        public void SearchGridOperatorTariffs_DefaultValidCase_Returns200()
        {
            const GoTariffStatus status = GoTariffStatus.New;
            var goTariff = AddGoTariffToDb(status);
            var allGoTariffs = GoTariffDb.GetEntitiesByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got =>
                got.GridOperator.Equals(goTariff.GridOperator) && got.StatusId.Equals((short)status));

            var response = new SearchGridOperatorTariffsRequest
            {
                GridOperatorEan = goTariff.GridOperator,
                Status = status.ToString()
            }.CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.GridOperatorTariffs.Should().NotBeEmpty().And.Subject.Should().HaveCount(allGoTariffs.Count);
        }

        [Test]
        public void SearchGridOperatorTariffs_ByGoEanOnly_Returns200()
        {
            const GoTariffStatus status = GoTariffStatus.New;
            var goTariff = AddGoTariffToDb(status);
            var allGoTariffs = GoTariffDb.GetEntitiesByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got =>
                got.GridOperator.Equals(goTariff.GridOperator));

            var response = new SearchGridOperatorTariffsRequest
            {
                GridOperatorEan = goTariff.GridOperator,
            }.CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.GridOperatorTariffs.Should().NotBeEmpty().And.Subject.Should().HaveCount(allGoTariffs.Count);
        }

        [Test]
        public void SearchGridOperatorTariffs_ByStatusOnly_Returns200()
        {
            const GoTariffStatus status = GoTariffStatus.New;
            AddGoTariffToDb(status);
            var allGoTariffs = GoTariffDb.GetEntitiesByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got =>
               got.StatusId.Equals((short)status));

            var response = new SearchGridOperatorTariffsRequest
            {
                Status = status.ToString()
            }.CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.GridOperatorTariffs.Should().NotBeEmpty().And.Subject.Should().HaveCount(allGoTariffs.Count);
        }

        [Test]
        public void SearchGridOperatorTariffs_WithoutAllParams_Returns400()
        {
            var response = new SearchGridOperatorTariffsRequest()
                .CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.NoParamsError);
        }

        [Test]
        public void SearchGridOperatorTariffs_WithInvalidEnum_Returns400()
        {
            const string status = "Invalid_Status";

            var response = new SearchGridOperatorTariffsRequest
            {
                Status = status
            }.CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.InvalidStatus);
        }

        [Test]
        public void SearchGridOperatorTariffs_WithInvalidGoEan_ReturnsEmptyList()
        {
            var response = new SearchGridOperatorTariffsRequest
            {
                GridOperatorEan = "7777777777",
            }.CallWith<SearchGridOperatorTariffsRequest, SearchGridOperatorTariffsResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.GridOperatorTariffs.Should().BeEmpty();
        }
    }
}
