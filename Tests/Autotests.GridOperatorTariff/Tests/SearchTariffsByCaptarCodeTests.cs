﻿using Autotests.Clients;
using Autotests.GridOperatorTariff.Base;
using Autotests.GridOperatorTariff.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.GridOperatorTariff.Contract.Rest.Transport;
using System.Net;

namespace Autotests.GridOperatorTariff.Tests
{
    [TestFixture]
    internal class SearchTariffsByCaptarCodeTests : BaseGridOperatorTariff
    {
        [Test]
        public void SearchTariffsByCaptarCode_DefaultValidCase_Returns200()
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.Accepted);
            var tariff = goTariff.Tariff.FirstOrDefault();

            var response = new SearchTariffsByCaptarCodeRequest
            {
                Code = tariff?.CapTarCode,
                StartDate = tariff?.CreatedOn.AddDays(-3)
            }.CallWith<SearchTariffsByCaptarCodeRequest, SearchTariffsByCaptarCodeResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.TariffInfos.Should().NotBeEmpty();
        }

        [Test]
        public void SearchTariffsByCaptarCode_ByCodeOnly_Returns200()
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.Accepted);
            var tariff = goTariff.Tariff.FirstOrDefault();

            var response = new SearchTariffsByCaptarCodeRequest
            {
                Code = tariff?.CapTarCode
            }.CallWith<SearchTariffsByCaptarCodeRequest, SearchTariffsByCaptarCodeResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.TariffInfos.Should().NotBeEmpty();
        }
    }
}
