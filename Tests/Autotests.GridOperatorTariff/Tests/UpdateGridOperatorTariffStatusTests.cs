﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.GridOperatorTariff.Base;
using Autotests.GridOperatorTariff.Builders;
using Autotests.GridOperatorTariff.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.GridOperatorTariff.Contract.Rest.Transport;
using System.Net;
using static System.String;

namespace Autotests.GridOperatorTariff.Tests
{
    [TestFixture]
    internal class UpdateGridOperatorTariffStatusTests : BaseGridOperatorTariff
    {
        private const string ModifiedBy = "Autotest";

        [Test]
        public void UpdateGridOperatorTariffStatus_FromNewToRejected_UpdatedToRejected()
        {
            const GoTariffStatus status = GoTariffStatus.Rejected;
            var goTariff = AddGoTariffToDb(GoTariffStatus.New);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<UpdateGridOperatorTariffStatusRequest, UpdateGridOperatorTariffStatusResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Id.Should().Be(goTariff.Id);
            var resultedGoTariff =
                GoTariffDb.GetEntityByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got => got.Id.Equals(goTariff.Id));
            resultedGoTariff.StatusId.Should().Be((short)status);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromNewToApproved_UpdatedToApproved()
        {
            const GoTariffStatus status = GoTariffStatus.Approved;
            var goTariff = AddGoTariffToDb(GoTariffStatus.New);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<UpdateGridOperatorTariffStatusRequest, UpdateGridOperatorTariffStatusResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Id.Should().Be(goTariff.Id);
            var resultedGoTariff =
                GoTariffDb.GetEntityByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got => got.Id.Equals(goTariff.Id));
            resultedGoTariff.StatusId.Should().Be((short)status);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromApprovedToAccepted_UpdatedToAccepted()
        {
            const GoTariffStatus status = GoTariffStatus.Accepted;
            var goTariff = AddGoTariffToDb(GoTariffStatus.Approved);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<UpdateGridOperatorTariffStatusRequest, UpdateGridOperatorTariffStatusResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Id.Should().Be(goTariff.Id);
            var resultedGoTariff =
                GoTariffDb.GetEntityByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got => got.Id.Equals(goTariff.Id));
            resultedGoTariff.StatusId.Should().Be((short)status);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromApprovedToRejected_UpdatedToRejected()
        {
            const GoTariffStatus status = GoTariffStatus.Rejected;
            var goTariff = AddGoTariffToDb(GoTariffStatus.Approved);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<UpdateGridOperatorTariffStatusRequest, UpdateGridOperatorTariffStatusResponse>(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Id.Should().Be(goTariff.Id);
            var resultedGoTariff =
                GoTariffDb.GetEntityByCondition<Repositories.GridOperatorTariffModels.GridOperatorTariff>(got => got.Id.Equals(goTariff.Id));
            resultedGoTariff.StatusId.Should().Be((short)status);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromNewToInvalidStats_Returns500([Values(
            GoTariffStatus.New, GoTariffStatus.Accepted)] GoTariffStatus status)
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.New);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.StatusTransitionNotAllowed,
                GoTariffStatus.New.ToString(), status));
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromApprovedToInvalidStats_Returns500([Values(
            GoTariffStatus.Approved, GoTariffStatus.New)] GoTariffStatus status)
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.Approved);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.StatusTransitionNotAllowed,
                GoTariffStatus.Approved.ToString(), status));
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromAcceptedToInvalidStats_Returns500([Values(
            GoTariffStatus.Accepted, GoTariffStatus.New,
            GoTariffStatus.Approved, GoTariffStatus.Rejected)] GoTariffStatus status)
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.Accepted);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.StatusDoesNotHaveTransition,
                GoTariffStatus.Accepted.ToString()));
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_FromRejectedToInvalidStats_Returns500([Values(
            GoTariffStatus.Rejected, GoTariffStatus.New,
            GoTariffStatus.Approved, GoTariffStatus.Accepted)] GoTariffStatus status)
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.Rejected);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(status.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.StatusDoesNotHaveTransition,
                GoTariffStatus.Rejected.ToString()));
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_WithNotExistGoTariffId_Returns404()
        {
            const int id = int.MaxValue - 1;

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(id).SetStatus(GoTariffStatus.Rejected.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.GoTariffNotFound, id));
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_WithInvalidModifiedByLength_Returns400()
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.New);
            var modifiedBy = new string('a', 256);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(GoTariffStatus.Rejected.ToString()).SetModifiedBy(modifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.InvalidModifiedByLength);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_WithInvalidModifiedBy_Returns400([Values("", null)] string modifiedBy)
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.New);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(GoTariffStatus.Rejected.ToString()).SetModifiedBy(modifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(
                PatternMessages.FieldIsRequired, nameof(UpdateGridOperatorTariffStatusRequest.GridOperatorTariff.ModifiedBy)));
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_WithoutStatus_Returns400()
        {
            var goTariff = AddGoTariffToDb(GoTariffStatus.New);

            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(goTariff.Id).SetStatus(Empty).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldIsRequired,
                nameof(UpdateGridOperatorTariffStatusRequest.GridOperatorTariff.Status));
            response.Header.ErrorDetails.Should().Contain(message)
                .And.Subject.Should().Contain(PatternMessages.InvalidStatus);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_WithInvalidRangeForId_Returns400([Values(-1, 0)] int id)
        {
            var response = new UpdateGridOperatorTariffStatusRequest
            {
                GridOperatorTariff = BuildDirector.Get<UpdateGridOperatorTariffStatusBuilder>()
                    .SetId(id).SetStatus(GoTariffStatus.Rejected.ToString()).SetModifiedBy(ModifiedBy).Build()
            }.CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.InvalidRange,
                nameof(UpdateGridOperatorTariffStatusRequest.GridOperatorTariff.Id));
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void UpdateGridOperatorTariffStatus_WithoutGridOperatorTariffField_Returns400()
        {
            var response = new UpdateGridOperatorTariffStatusRequest().CallWith(GOTariffClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldIsRequired,
                nameof(UpdateGridOperatorTariffStatusRequest.GridOperatorTariff));
            response.Header.ErrorDetails.Should().Contain(message);
        }
    }
}
