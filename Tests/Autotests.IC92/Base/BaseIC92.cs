﻿using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.Sql;
using Autotests.Core.Sql.Entities;
using Autotests.IC92.Builders;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Model;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Globalization;
using System.Net;
using Autotests.Helper;
using Autotests.Helper.Enums;
using Register = Autotests.Repositories.IC92SettlementModels.Register;
using Settlement = Autotests.Repositories.IC92SettlementModels.Settlement;
using SettlementStatusHistory = Autotests.Repositories.IC92SettlementModels.SettlementStatusHistory;
using Tariff = Autotests.Repositories.IC92SettlementModels.Tariff;

namespace Autotests.IC92.Base
{
    [SetUpFixture, Category(Categories.IC92)]
    internal abstract class BaseIC92 : AllureReport
    {
        //clients declaration
        protected NutsHttpClient IC92Client;

        //databases declaration
        protected DbHandler<IC92SettlementContext> Ic92Db;
        protected DbHandler<MarketPartyContext> MarketPartyDb;

        //variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        protected BuildDirector BuildDirector;
        protected readonly short Year = (short)DateTime.Today.AddYears(2).Year;
        protected const string ModifiedBy = "Autotest";
        protected const string MimeType = "text/xlsx";
        protected Random random = new Random();
        protected List<EanInfoData> EanInfos;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);
            IC92Client = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.IC92));
            Ic92Db = new DbHandler<IC92SettlementContext>(_configHandler.GetConnectionString(DbConnectionName.IC92SettlementDatabase.ToString()));
            MarketPartyDb = new DbHandler<MarketPartyContext>(_configHandler.GetConnectionString(DbConnectionName.MarketPartyDatabase.ToString()));
            EanInfos = SqlQueryExecutor.Instance.ConnectTo("EnerFreeDb").ExecuteSqlQuery<EanInfoData>("GetEanInfoQuery", 100).ToList();

            BuildDirector = new BuildDirector();
        }

        [SetUp]
        public void SetUp() => CleanSettlementsDb();

        protected List<Nuts.Ic92Settlement.Contract.Rest.Model.Tariff> CreateAndGetTestTariffs()
        {
            var res = new List<Nuts.Ic92Settlement.Contract.Rest.Model.Tariff>();
            for (byte quarter = 1; quarter <= 4; quarter++)
            {
                var tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter(quarter).SetYear(Year).Build();
                res.Add(tariff);
                var response = new UpsertTariffRequest { Tariff = tariff }
                    .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
                response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            }
            return res;
        }

        protected int CreateDefaultSettlement(int marketPartyId, Label label)
        {
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>()
                .SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var insertSettlementResponse = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(label.ToString()).SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketPartyId).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);
            insertSettlementResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            return insertSettlementResponse.Data.Id;
        }

        protected bool GenerateProforma(int settlementId)
        {
            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);
            return response.Header.StatusCode.Equals(HttpStatusCode.OK);
        }

        protected FileModel GenerateProformaAndGetInfo(int settlementId)
        {
            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            if (response.Header.StatusCode != HttpStatusCode.OK)
                throw new AssertionException(response.Header.Message);

            return response.Data.ProformaFile;
        }

        protected void GenerateInvoice(int settlementId)
        {
            new GenerateInvoiceRequest
            {
                InvoiceData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetSettlementIds(settlementId).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
        }

        protected FileModel GenerateInvoiceAndGetInfo(int settlementId)
        {
            var response = new GenerateInvoiceRequest
            {
                InvoiceData = BuildDirector.Get<InvoiceDataBuilder>()
                        .SetSettlementIds(settlementId).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            if (response.Header.StatusCode != HttpStatusCode.OK)
                throw new AssertionException(response.Header.Message);

            return response.Data.InvoiceFile;
        }

        protected void UpdateSettlementStatus(int settlementId, SettlementStatus status)
        {
            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var request = new UpdateSettlementStatusRequest { Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus } };
            var response = request.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
        }

        private void CleanSettlementsDb()
        {
            Ic92Db.GetEntitiesByCondition<Tariff>(t => t.ValidFrom.Year.Equals(Year)).ForEach(t =>
            {
                Ic92Db.GetEntitiesByCondition<Settlement>(s => s.TariffId.Equals(t.Id)).ForEach(s =>
                {
                    Ic92Db.DeleteEntitiesByCondition<Register>(r => r.SettlementId.Equals(s.Id));
                    Ic92Db.DeleteEntitiesByCondition<SettlementStatusHistory>(ssh => ssh.SettlementId.Equals(s.Id));
                    Ic92Db.DeleteEntityByCondition<Settlement>(settlement => settlement.Id.Equals(s.Id));
                });
                Ic92Db.DeleteEntityByCondition<Tariff>(tariff => tariff.Id.Equals(t.Id));
            });
        }
    }
}