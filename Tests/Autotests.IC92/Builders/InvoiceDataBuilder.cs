﻿using Autotests.Core.Helpers;
using Nuts.Ic92Settlement.Contract.Rest.Model;

namespace Autotests.IC92.Builders
{
    internal class InvoiceDataBuilder : MainBuilder<InvoiceData>
    {
        internal InvoiceDataBuilder SetModifiedBy(string modifier)
        {
            Instance.ModifiedBy = modifier;
            return this;
        }

        internal InvoiceDataBuilder SetSettlementIds(params int[] settlementIds)
        {
            Instance.SettlementIds = settlementIds.ToList();
            return this;
        }
    }
}