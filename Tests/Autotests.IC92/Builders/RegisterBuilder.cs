﻿using Autotests.Core.Helpers;
using Register = Nuts.Ic92Settlement.Contract.Rest.Model.Register;

namespace Autotests.IC92.Builders
{
    internal class RegisterBuilder : MainBuilder<Register>
    {
        internal RegisterBuilder SetUsage(int usage)
        {
            Instance.Usage = usage;
            return this;
        }

        internal RegisterBuilder SetOriginalReading(int originalReading)
        {
            Instance.OriginalReading = originalReading;
            return this;
        }

        internal RegisterBuilder SetNewReading(int newReading)
        {
            Instance.NewReading = newReading;
            return this;
        }

        internal RegisterBuilder SetMeteringDirection(string meteringDirection)
        {
            Instance.MeteringDirection = meteringDirection;
            return this;
        }

        internal RegisterBuilder SetTariffType(string tariffType)
        {
            Instance.TariffType = tariffType;
            return this;
        }
    }
}