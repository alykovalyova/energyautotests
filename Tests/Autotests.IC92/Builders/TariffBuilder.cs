﻿using Autotests.Core.Helpers;
using Nuts.Ic92Settlement.Contract.Rest.Model;

namespace Autotests.IC92.Builders
{
    internal class TariffBuilder : MainBuilder<Tariff>
    {
        internal TariffBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal TariffBuilder SetPriceElec(decimal elecPrice)
        {
            Instance.PriceElec = elecPrice;
            return this;
        }

        internal TariffBuilder SetPriceGas(decimal gasPrice)
        {
            Instance.PriceGas = gasPrice;
            return this;
        }

        internal TariffBuilder SetQuarter(byte quarter)
        {
            Instance.Quarter = quarter;
            return this;
        }

        internal TariffBuilder SetYear(short year)
        {
            Instance.Year = year;
            return this;
        }

        internal TariffBuilder SetModifiedBy(string modifiedBy)
        {
            Instance.ModifiedBy = modifiedBy;
            return this;
        }
    }
}