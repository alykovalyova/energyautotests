﻿using Autotests.Core.Helpers;
using Nuts.Ic92Settlement.Contract.Rest.Model;

namespace Autotests.IC92.Builders
{
    internal class UpdateSettlementStatusBuilder : MainBuilder<UpdateSettlementStatus>
    {
        internal UpdateSettlementStatusBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal UpdateSettlementStatusBuilder SetStatus(string status)
        {
            Instance.Status = status;
            return this;
        }
    }
}