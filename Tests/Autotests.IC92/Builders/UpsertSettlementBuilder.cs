﻿using Autotests.Core.Helpers;
using Nuts.Ic92Settlement.Contract.Rest.Model;

namespace Autotests.IC92.Builders
{
    internal class UpsertSettlementBuilder : MainBuilder<UpsertSettlement>
    {
        internal UpsertSettlementBuilder SetTariffId(int tariffId)
        {
            Instance.TariffId = tariffId;
            return this;
        }

        internal UpsertSettlementBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal UpsertSettlementBuilder SetSupplierId(int supplierId)
        {
            Instance.SupplierId = supplierId;
            return this;
        }

        internal UpsertSettlementBuilder SetEan(string ean)
        {
            Instance.Ean = ean;
            return this;
        }

        internal UpsertSettlementBuilder SetProductType(string productType)
        {
            Instance.ProductType = productType;
            return this;
        }

        internal UpsertSettlementBuilder SetLabel(string label)
        {
            Instance.Label = label;
            return this;
        }

        internal UpsertSettlementBuilder SetContractId(string contractId)
        {
            Instance.ContractId = int.Parse(contractId);
            return this;
        }

        internal UpsertSettlementBuilder SetEventDate(DateTime eventDate)
        {
            Instance.EventDate = eventDate;
            return this;
        }

        internal UpsertSettlementBuilder SetRegisters(params Register[] registers)
        {
            Instance.Registers = registers.ToList();
            return this;
        }
    }
}