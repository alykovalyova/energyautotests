﻿namespace Autotests.IC92.Enums
{
    internal enum MeteringDirection
    {
        CMB = 1,
        LVR,
        TLV
    }
}