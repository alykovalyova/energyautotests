﻿using AutoMapper;
using Autotests.Core;
using Autotests.Helper.Enums;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;

namespace Autotests.IC92.MapProfiles
{
    public class IC92ConfigProfile : Profile
    {
        public IC92ConfigProfile()
        {
            CreateMap<Tariff, Nuts.Ic92Settlement.Contract.Rest.Model.Tariff>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Year, opt => opt.MapFrom(srs => (short)srs.ValidFrom.Year))
                .ForMember(dest => dest.Quarter, opt => opt.MapFrom(srs => srs.ValidFrom.GetQuarter()));
            CreateMap<Settlement, Nuts.Ic92Settlement.Contract.Rest.Model.Settlement>()
                .ForMember(dest => dest.ProductType, opt => opt.MapFrom(src => (ProductType) src.ProductTypeId))
                .ForMember(dest => dest.Label, opt => opt.MapFrom(src => (Label) src.LabelId))
                .ForMember(dest => dest.Comment, opt => opt.Ignore())
                .ForMember(dest => dest.Status, opt => opt.Ignore())
                .ForMember(dest => dest.Tariff, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Registers, opt => opt.MapFrom(src => src.Register));
            CreateMap<Register, Nuts.Ic92Settlement.Contract.Rest.Model.Register>()
                .ForMember(dest => dest.MeteringDirection, opt => opt.MapFrom(srs => (MeteringDirection)srs.MeteringDirectionId))
                .ForMember(dest => dest.TariffType, opt => opt.MapFrom(srs => (TariffType)srs.TariffTypeId));
        }
    }
}