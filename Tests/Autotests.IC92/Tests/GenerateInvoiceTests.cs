﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Repositories.IC92SettlementModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Autotests.Core;
using Autotests.Helper.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using Settlement = Autotests.Repositories.IC92SettlementModels.Settlement;
using static System.String;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Autotests.IC92.Base;
using Autotests.IC92.Enums;
using Autotests.IC92.Builders;

namespace Autotests.Tests.IC92
{
    [TestFixture]
    internal class GenerateInvoiceTests : BaseIC92
    {
        [Test]
        public void GenerateInvoice_DefaultValidCase_Returns200()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            Waiter.Wait(() => GenerateProforma(settlementId), 10);

            var response = new GenerateInvoiceRequest
            {
                InvoiceData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetSettlementIds(settlementId).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.InvoiceFile.Should().NotBeNull();
            response.Data.InvoiceFile.MimeType.Should().BeEquivalentTo(MimeType);
            response.Data.InvoiceFile.Content.Should().NotBeEmpty();
            response.Data.InvoiceFile.FileName.Should().Contain(marketParty.MarketPartyName.RemoveSpecChars())
                .And.Subject.Should().Contain("xlsx")
                .And.Subject.Should().StartWith("Invoice");
            Ic92Db.GetEntityByCondition<Settlement>(s => s.Id.Equals(settlementId))
                .StatusId.Should().Be((short)SettlementStatus.Invoiced);
            Ic92Db.GetEntitiesByCondition<SettlementStatusHistory>(ssh => ssh.SettlementId.Equals(settlementId))
                .Should().HaveCount(3)
                .And.Subject.Should().Contain(s => s.StatusId.Equals((short) SettlementStatus.New))
                .And.Subject.Should().Contain(s => s.StatusId.Equals((short) SettlementStatus.InProforma))
                .And.Subject.Should().Contain(s => s.StatusId.Equals((short)SettlementStatus.Invoiced));
        }

        [Test]
        public void GenerateInvoice_FromInvalidStatus_Returns500([Values(
            SettlementStatus.New, SettlementStatus.Invoiced, SettlementStatus.Canceled)] SettlementStatus status)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            if (!status.Equals(SettlementStatus.New))
                Ic92Db.UpdateSingleEntity<Settlement>(s => s.Id.Equals(settlementId), s => s.StatusId = (short) status);
            Ic92Db.GetEntityByCondition<Settlement>(s => s.Id.Equals(settlementId)).StatusId.Should().Be((short) status);

            var response = new GenerateInvoiceRequest
            {
                InvoiceData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetSettlementIds(settlementId).SetModifiedBy(ModifiedBy).Build()
            }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidStatusToInvoice);
        }

        [Test]
        public void GenerateInvoice_WithoutInvoiceDataField_Returns400()
        {
            var response = new GenerateInvoiceRequest()
                .CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(PatternMessages.FieldIsRequired,
                nameof(GenerateInvoiceRequest.InvoiceData)));
        }

        [Test]
        public void GenerateInvoice_WithoutSettlementsField_Returns400()
        {
            var response = new GenerateInvoiceRequest
                {
                    InvoiceData = BuildDirector.Get<InvoiceDataBuilder>().SetModifiedBy(ModifiedBy).Build()
                }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(PatternMessages.FieldIsRequired,
                nameof(GenerateInvoiceRequest.InvoiceData.SettlementIds)));
        }

        [Test]
        public void GenerateInvoice_WithInvalidSettlementIdsLength_Returns400()
        {
            var response = new GenerateInvoiceRequest
            {
                InvoiceData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds().Build()
            }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(PatternMessages.InvalidArrayFieldLength,
                nameof(GenerateInvoiceRequest.InvoiceData.SettlementIds)));
        }

        [Test]
        public void GenerateInvoice_WithInvalidModifiedByLength_Returns400()
        {
            var modifier = new string('f', 256);
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            var response = new GenerateInvoiceRequest
            {
                InvoiceData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(modifier).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateInvoiceRequest, GenerateInvoiceResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongModifiedByLength2);
        }
    }
}
