﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;
using static System.String;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class GenerateProformaTests : BaseIC92
    {
        [Test]
        public void GenerateProforma_DefaultValidCase_Returns200()
        {
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.ProformaFile.Should().NotBeNull();
            response.Data.ProformaFile.MimeType.Should().BeEquivalentTo(MimeType);
            response.Data.ProformaFile.Content.Should().NotBeEmpty();
            response.Data.ProformaFile.FileName.Should().Contain(mpInfo.MarketPartyName.RemoveSpecChars())
                .And.Subject.Should().Contain("xlsx")
                .And.Subject.Should().StartWith("Proforma");
            Ic92Db.GetEntityByCondition<Settlement>(s => s.Id.Equals(settlementId))
                .StatusId.Should().Be((short)SettlementStatus.InProforma);
            Ic92Db.GetEntitiesByCondition<SettlementStatusHistory>(ssh => ssh.SettlementId.Equals(settlementId))
                .Should().HaveCount(2)
                .And.Subject.Should().Contain(s => s.StatusId.Equals((short)SettlementStatus.New))
                .And.Subject.Should().Contain(s => s.StatusId.Equals((short)SettlementStatus.InProforma));
        }

        [Test]
        public void GenerateProforma_WithoutProformaDataField_Returns400()
        {
            var response = new GenerateProformaRequest().CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(PatternMessages.FieldIsRequired,
                nameof(GenerateProformaRequest.ProformaData)));
        }

        [Test]
        public void GenerateProforma_WithoutSettlementsField_Returns400()
        {
            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(PatternMessages.FieldIsRequired,
                nameof(GenerateProformaRequest.ProformaData.SettlementIds)));
        }

        [Test]
        public void GenerateProforma_WithInvalidSettlementIdsLength_Returns400()
        {
            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds().Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(Format(PatternMessages.InvalidArrayFieldLength,
                nameof(GenerateProformaRequest.ProformaData.SettlementIds)));
        }

        [Test]
        public void GenerateProforma_WithInvalidModifiedByLength_Returns400()
        {
            var modifier = new string('f', 256);
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(modifier).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongModifiedByLength2);
        }

        [Test]
        public void GenerateProforma_WithNotExistedMarketPartyInfo_Returns500()
        {
            const int id = int.MaxValue - 1;
            var settlementId = CreateDefaultSettlement(id, Label.BudgetEnergie);

            var response = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.InternalErrorCode.Should().Be((short)HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketPartyNotFound, id));
        }
    }
}
