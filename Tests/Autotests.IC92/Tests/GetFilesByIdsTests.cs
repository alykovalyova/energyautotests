﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;
using File = Autotests.Repositories.IC92SettlementModels.File;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class GetFilesByIdsTests : BaseIC92
    {
        [Test]
        public void GetFilesByIds_GetProformaFile_Returns200()
        {
            // Create Settlement
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            // Generate Proforma file
            var getProformaRes = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);
            getProformaRes.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            var fileName = getProformaRes.Data.ProformaFile.FileName;

            // Get File from DB
            var fileInDb =
                Ic92Db.GetSingleEntityByCondition<File>(f => f.Name == fileName);

            // Get file info by Id
            var response = new GetFilesByIdsRequest { Ids = new[] { fileInDb.Id } }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.Files[0].Name.Should().BeEquivalentTo(fileInDb.Name);
        }

        [Test]
        public void GetFilesByIds_GetInvoiceFile_Returns200()
        {
            // Create Settlement
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            // Generate Proforma file and get InProforma status for settlement
            GenerateProforma(settlementId);

            // Change status to Invoiced
            var invoiceInfo = GenerateInvoiceAndGetInfo(settlementId);
            Ic92Db.GetEntityByCondition<Settlement>(
                s => s.Id.Equals(settlementId)).StatusId.Should().Be((short)SettlementStatus.Invoiced);
            var fileName = invoiceInfo.FileName;

            // Get File from DB
            var fileInDb =
                Ic92Db.GetSingleEntityByCondition<File>(f => f.Name == fileName);

            // Get file info by Id
            var response = new GetFilesByIdsRequest { Ids = new[] { fileInDb.Id } }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.Files[0].Name.Should().BeEquivalentTo(fileInDb.Name);
        }

        [Test]
        public void GetFilesByIds_IdsListIsNull_Returns400()
        {
            var response = new GetFilesByIdsRequest { Ids = null }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, nameof(GetFilesByIdsRequest.Ids));
            response.Header.ErrorDetails.Should().OnlyContain(e => e == m);
        }

        [Test]
        public void GetFilesByIds_WithEmptyIdsList_Returns400()
        {
            var response = new GetFilesByIdsRequest { Ids = Array.Empty<int>() }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, nameof(GetFilesByIdsRequest.Ids));
            response.Header.ErrorDetails.Should().OnlyContain(e => e == m);
        }

        [Test]
        public void GetFilesByIds_WithInvalidIdInTheList_ReturnsEmptyList()
        {
            var response = new GetFilesByIdsRequest { Ids = new[] { -1, 0 } }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.Files.Should().BeEmpty();
        }
    }
}