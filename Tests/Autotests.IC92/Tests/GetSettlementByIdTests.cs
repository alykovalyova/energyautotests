﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.IC92.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;
using Autotests.Helper.Enums;
using static System.String;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class GetSettlementByIdTests : BaseIC92
    {
        [Test]
        public void GetSettlementById_DefaultValidCase_Returns200()
        {
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            var response = new GetSettlementRequest { SettlementId = settlementId }
                .CallWith<GetSettlementRequest, GetSettlementResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Settlement.Should().NotBeNull();
        }

        [Test]
        public void GetSettlementById_WithInvalidId_Returns400()
        {
            const int settlementId = int.MaxValue - 1;
            var response = new GetSettlementRequest { SettlementId = settlementId }
                .CallWith<GetSettlementRequest, GetSettlementResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.SettlementNotFound, settlementId));
        }
    }
}
