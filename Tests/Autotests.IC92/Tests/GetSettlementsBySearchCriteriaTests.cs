﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Autotests.IC92.Base;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class GetSettlementsBySearchCriteriaTests : BaseIC92
    {
        [Test]
        public void GetSettlementsBySearchCriteria_DefaultValidCase_Returns200([Values(Label.BudgetEnergie, Label.NLE)] Label label)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, label);

            var response = new GetSettlementsBySearchCriteriaRequest
            {
                Label = label.ToString(),
                Statuses = new[] { SettlementStatus.New.ToString() },
                Supplier = marketParty.Id,
                From = DateTime.Now.AddMonths(-1).Date,
                To = DateTime.Now.Date
            }.CallWith<GetSettlementsBySearchCriteriaRequest, GetSettlementsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Settlements.Should().NotBeEmpty();
            response.Data.Settlements.Should().Contain(s => s.Id.Equals(settlementId));
        }

        [Test]
        public void GetSettlementsBySearchCriteria_WithoutAnyFields_Returns200()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            var settlementsFromDb = Ic92Db.GetEntitiesByCondition<Settlement>(s => true);

            var response = new GetSettlementsBySearchCriteriaRequest()
                .CallWith<GetSettlementsBySearchCriteriaRequest, GetSettlementsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Settlements.Should().NotBeEmpty()
                .And.Subject.Should().Contain(s => s.Id.Equals(settlementId))
                .And.Subject.Should().HaveCount(settlementsFromDb.Count);
        }

        [Test]
        public void GetSettlementsBySearchCriteria_WithInvalidLabel_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();

            var response = new GetSettlementsBySearchCriteriaRequest
            {
                Label = "InvalidLabel",
                Statuses = new[] { SettlementStatus.New.ToString() },
                Supplier = marketParty.Id,
                From = DateTime.Now.AddMonths(-1).Date,
                To = DateTime.Now.Date
            }.CallWith<GetSettlementsBySearchCriteriaRequest, GetSettlementsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidLabel);
        }

        [Test]
        public void GetSettlementsBySearchCriteria_WithInvalidStatus_Returns400()
        {
            const string invalidStatus = "InvalidStatus";
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();

            var response = new GetSettlementsBySearchCriteriaRequest
            {
                Label = Label.BudgetEnergie.ToString(),
                Statuses = new[] { invalidStatus },
                Supplier = marketParty.Id,
                From = DateTime.Now.AddMonths(-1).Date,
                To = DateTime.Now.Date
            }.CallWith<GetSettlementsBySearchCriteriaRequest, GetSettlementsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            var message = string.Format(PatternMessages.InvalidSettlementStatuses, invalidStatus);
            response.Header.ErrorDetails.Should().Contain(message);
        }
    }
}
