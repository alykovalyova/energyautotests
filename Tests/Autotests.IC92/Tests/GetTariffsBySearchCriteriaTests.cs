﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.Repositories.IC92SettlementModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class GetTariffsBySearchCriteriaTests : BaseIC92
    {
        [Test]
        public void GetTariffsBySearchCriteria_DefaultValidCase_Returns200()
        {
            var tariffs = CreateAndGetTestTariffs();
            var quarter = (byte)random.Next(1, 4);
            var patterTariff = tariffs.Find(t => t.Quarter.Equals(quarter));

            var response = new GetTariffsBySearchCriteriaRequest
            {
                Quarter = quarter,
                Year = Year
            }.CallWith<GetTariffsBySearchCriteriaRequest, GetTariffsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Tariffs.Should().NotBeEmpty().And.Subject.Should().HaveCount(1);
            ObjectComparator.ComparePropsOfTypes(response.Data.Tariffs.First(), patterTariff, false,
                nameof(patterTariff.Id));
        }

        [Test]
        public void GetTariffsBySearchCriteria_ByYearOnly_Returns200()
        {
            var tariffs = CreateAndGetTestTariffs();

            var response = new GetTariffsBySearchCriteriaRequest { Year = Year }
                .CallWith<GetTariffsBySearchCriteriaRequest, GetTariffsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Tariffs.Should().NotBeEmpty().And.Subject.Should().HaveCount(tariffs.Count);
        }

        [Test]
        public void GetTariffsBySearchCriteria_ByQuarterOnly_Returns400()
        {
            var response = new GetTariffsBySearchCriteriaRequest { Quarter = 3 }
                .CallWith<GetTariffsBySearchCriteriaRequest, GetTariffsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.IndicateYearMessage);
        }

        [Test]
        public void GetTariffsBySearchCriteria_ByInvalidQuarterRegex_Returns400([Values(0, 5)] byte quarter)
        {
            var response = new GetTariffsBySearchCriteriaRequest { Quarter = quarter, Year = Year }
                .CallWith<GetTariffsBySearchCriteriaRequest, GetTariffsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidQuarterRegex);
        }

        [Test]
        public void GetTariffsBySearchCriteria_ByInvalidYearRegex_Returns400([Values(0, 5)] short year)
        {
            var response = new GetTariffsBySearchCriteriaRequest { Quarter = 2, Year = year }
                .CallWith<GetTariffsBySearchCriteriaRequest, GetTariffsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidYearRegex);
        }

        [Test]
        public void GetTariffsBySearchCriteria_WithoutAnySearchCriteria_ReturnsAllTariffs()
        {
            var tariffsFromDb = Ic92Db.GetEntitiesByCondition<Tariff>(t => true)
                .Select(t => BuildDirector.Get<TariffBuilder>()
                    .SetId(t.Id)
                    .SetYear((short)t.ValidFrom.Year)
                    .SetQuarter(t.ValidFrom.GetQuarter())
                    .SetPriceElec(t.PriceElec)
                    .SetPriceGas(t.PriceGas).SetModifiedBy(t.ModifiedBy).Build()).ToList();

            var response = new GetTariffsBySearchCriteriaRequest()
                .CallWith<GetTariffsBySearchCriteriaRequest, GetTariffsBySearchCriteriaResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Tariffs
                .Should().HaveCount(tariffsFromDb.Count)
                .And.Subject.Should().BeEquivalentTo(tariffsFromDb);
        }
    }
}
