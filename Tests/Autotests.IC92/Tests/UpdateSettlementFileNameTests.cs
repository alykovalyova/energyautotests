﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.Repositories.IC92SettlementModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;
using Autotests.Helper.Enums;
using File = Autotests.Repositories.IC92SettlementModels.File;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class UpdateSettlementFileNameTests : BaseIC92
    {
        private const string Updated = "_Updated";

        [Test]
        public void UpdateSettlementFileName_WhenSettlementInProformaStatus_FileNameChanged()
        {
            // Create Settlement
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            // Generate Proforma file
            var getProformaRes = new GenerateProformaRequest
            {
                ProformaData = BuildDirector.Get<InvoiceDataBuilder>()
                    .SetModifiedBy(ModifiedBy).SetSettlementIds(settlementId).Build()
            }.CallWith<GenerateProformaRequest, GenerateProformaResponse>(IC92Client);
            getProformaRes.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            var fileName = getProformaRes.Data.ProformaFile.FileName;

            // Get File from DB
            var fileInDb =
                Ic92Db.GetSingleEntityByCondition<File>(f => f.Name == fileName);

            // Update FileName
            var newFileName = fileName + Updated;
            var updateFileNameRes = new UpdateSettlementFileNameRequest
            {
                SettlementId = settlementId,
                FileName = newFileName
            }.CallWith(IC92Client);
            updateFileNameRes.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            // Assertion
            var getFileInfoRes = new GetFilesByIdsRequest { Ids = new[] { fileInDb.Id } }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);
            getFileInfoRes.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            getFileInfoRes.Data.Files[0].Name.Should().EndWithEquivalent(Updated);
        }

        [Test]
        public void UpdateSettlementFileName_WhenSettlementInInvoicedStatus_FileNameChanged()
        {
            // Create Settlement
            var mpInfo = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mpi => true).RandomItem();
            var settlementId = CreateDefaultSettlement(mpInfo.Id, Label.BudgetEnergie);

            // Generate Proforma file and get InProforma status for settlement
            GenerateProformaAndGetInfo(settlementId);

            // Change status to Invoiced
            var invoiceInfo = GenerateInvoiceAndGetInfo(settlementId);
            Ic92Db.GetEntityByCondition<Settlement>(
                s => s.Id.Equals(settlementId)).StatusId.Should().Be((short)SettlementStatus.Invoiced);
            var fileName = invoiceInfo.FileName;

            // Get File from DB
            var fileInDb =
                Ic92Db.GetSingleEntityByCondition<File>(f => f.Name == fileName);

            // Update FileName
            var newFileName = fileName + Updated;
            var updateFileNameRes = new UpdateSettlementFileNameRequest
            {
                SettlementId = settlementId,
                FileName = newFileName
            }.CallWith(IC92Client);
            updateFileNameRes.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            // Assertion
            var getFileInfoRes = new GetFilesByIdsRequest { Ids = new[] { fileInDb.Id } }
                .CallWith<GetFilesByIdsRequest, GetFilesByIdsResponse>(IC92Client);
            getFileInfoRes.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            getFileInfoRes.Data.Files[0].Name.Should().EndWithEquivalent(Updated);
        }

        [Test]
        public void UpdateSettlementFileName_WithInvalidSettlementId_Returns400([Values(0, -1)] int id)
        {
            var response = new UpdateSettlementFileNameRequest
            {
                SettlementId = id,
                FileName = "Update"
            }.CallWith(IC92Client);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.InvalidRange, nameof(UpdateSettlementFileNameRequest.SettlementId));
            response.Header.ErrorDetails.Should().OnlyContain(e => e == m);
        }

        [Test]
        public void UpdateSettlementFileName_WithoutFileNameField_Returns400([Values(null, "")] string fileName)
        {
            var response = new UpdateSettlementFileNameRequest
            {
                SettlementId = 123,
                FileName = fileName
            }.CallWith(IC92Client);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, nameof(UpdateSettlementFileNameRequest.FileName));
            response.Header.ErrorDetails.Should().OnlyContain(e => e == m);
        }
    }
}