﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;
using Autotests.Repositories.MarketPartyModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Model;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using static System.String;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Settlement = Autotests.Repositories.IC92SettlementModels.Settlement;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class UpdateSettlementStatusTests : BaseIC92
    {
        [Test]
        public void UpdateSettlementStatus_FromNewToCancel_Returns200()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(SettlementStatus.Canceled.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Ids.Should().Contain(id => id.Equals(settlementId));
            Ic92Db.GetEntityByCondition<Settlement>(
                s => s.Id.Equals(settlementId)).StatusId.Should().Be((short)SettlementStatus.Canceled);
            Ic92Db.GetEntitiesByCondition<SettlementStatusHistory>(h => h.SettlementId.Equals(settlementId))
                .Should().HaveCount(2)
                .And.Subject.Should().Contain(h => h.StatusId.Equals((short)SettlementStatus.New))
                .And.Subject.Should().Contain(h => h.StatusId.Equals((short)SettlementStatus.Canceled));
        }

        [Test]
        public void UpdateSettlementStatus_WithInvalidUpdateStatusId_Returns400()
        {
            const int settlementId = -1;

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(SettlementStatus.Canceled.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(Format(Core.Helpers.PatternMessages.InvalidIntFieldValue, "Id"));
        }

        [Test]
        public void UpdateSettlementStatus_WithNotExistId_Returns404()
        {
            const int settlementId = int.MaxValue - 1;

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(SettlementStatus.Canceled.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Ids.Should().BeEmpty();
        }

        [Test]
        public void UpdateSettlementStatus_WithInvalidStatusEnum_Returns400()
        {
            const string invalidEnum = "InvalidEnum";
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(invalidEnum).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(Format(PatternMessages.InvalidUpdateStatus));
        }

        [Test]
        public void UpdateSettlementStatus_WithInvalidModifiedByLength_Returns400()
        {
            var modifiedBy = new string('a', 256);
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(SettlementStatus.Canceled.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = modifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(Format(PatternMessages.WrongModifiedByLength2));
        }

        [Test]
        public void UpdateSettlementStatus_WithoutUpsertSettlementField_Returns400()
        {
            var response = new UpdateSettlementStatusRequest()
                .CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, nameof(UpdateSettlementStatusRequest.Settlements)));
        }

        [Test]
        public void UpdateSettlementStatus_FromNewToInvalidStatuses_Returns500([Values(
            SettlementStatus.InProforma, SettlementStatus.Invoiced)] SettlementStatus status)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message
                .Should().BeEquivalentTo(Format(PatternMessages.WrongSettlementStatus, settlementId));
        }

        [Test]
        public void UpdateSettlementStatus_FromNewToNewStatus_Returns500()
        {
            const SettlementStatus status = SettlementStatus.New;
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message
                .Should().BeEquivalentTo(Format(PatternMessages.SettlementAlreadyHasStatus, settlementId));
        }

        [Test]
        public void UpdateSettlementStatus_FromInProformaToCanceledStats_Returns200()
        {
            var status = SettlementStatus.Canceled;

            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            Waiter.Wait(() => GenerateProforma(settlementId), 10);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Ids.Should().Contain(id => id.Equals(settlementId));
            Ic92Db.GetEntityByCondition<Settlement>(
                s => s.Id.Equals(settlementId)).StatusId.Should().Be((short)status);
            Ic92Db.GetEntitiesByCondition<SettlementStatusHistory>(h => h.SettlementId.Equals(settlementId))
                .Should().HaveCount(3)
                .And.Subject.Should().Contain(h => h.StatusId.Equals((short)SettlementStatus.New))
                .And.Subject.Should().Contain(h => h.StatusId.Equals((short)status));
        }

        [Test]
        public void UpdateSettlementStatus_FromInProformaToInvalidStats_Returns500([Values(
            SettlementStatus.New, SettlementStatus.Invoiced)] SettlementStatus status)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            Waiter.Wait(() => GenerateProforma(settlementId), 10);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message
                .Should().BeEquivalentTo(Format(PatternMessages.WrongSettlementStatus, settlementId));
        }

        [Test]
        public void UpdateSettlementStatus_FromInProformaToInProformaStatus_Returns500()
        {
            const SettlementStatus status = SettlementStatus.InProforma;
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            Waiter.Wait(() => GenerateProforma(settlementId), 10);

            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message
                .Should().BeEquivalentTo(Format(PatternMessages.SettlementAlreadyHasStatus, settlementId));
        }

        [Test]
        public void UpdateSettlementStatus_FromInvoiced_Returns500([Values(
            SettlementStatus.New, SettlementStatus.InProforma,
             SettlementStatus.Canceled)] SettlementStatus status)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            Waiter.Wait(() => GenerateProforma(settlementId), 10);

            // Update to Invoiced
            GenerateInvoice(settlementId);

            // Update to target status
            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message
                .Should().BeEquivalentTo(Format(PatternMessages.WrongSettlementStatus, settlementId));
        }

        [Test]
        public void UpdateSettlementStatus_FromCanceled_Returns500([Values(
            SettlementStatus.New, SettlementStatus.InProforma,
            SettlementStatus.Invoiced)] SettlementStatus status)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<MarketPartyInfo>(mp => true).RandomItem();
            var settlementId = CreateDefaultSettlement(marketParty.Id, Label.BudgetEnergie);
            Waiter.Wait(() => GenerateProforma(settlementId), 10);

            // Update to Canceled
            var toCanceled = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(SettlementStatus.Canceled.ToString()).Build();
            var toRejectedResponse = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { toCanceled },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);
            toRejectedResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            // Update to target status
            var updateSettlementStatus = BuildDirector.Get<UpdateSettlementStatusBuilder>()
                .SetId(settlementId).SetStatus(status.ToString()).Build();
            var response = new UpdateSettlementStatusRequest
            {
                Settlements = new List<UpdateSettlementStatus> { updateSettlementStatus },
                ModifiedBy = ModifiedBy
            }.CallWith<UpdateSettlementStatusRequest, UpdateSettlementStatusResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError);
            response.Header.Message
                .Should().BeEquivalentTo(Format(PatternMessages.WrongSettlementStatus, settlementId));
        }
    }
}