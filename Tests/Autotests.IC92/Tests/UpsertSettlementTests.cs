﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.IC92.Enums;
using Autotests.Repositories.IC92SettlementModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Enums;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using static System.String;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Register = Nuts.Ic92Settlement.Contract.Rest.Model.Register;
using Settlement = Autotests.Repositories.IC92SettlementModels.Settlement;

namespace Autotests.IC92.Tests
{
    [TestFixture]
    internal class UpsertSettlementTests : BaseIC92
    {
        [Test, Pairwise]
        public void UpsertSettlement_DefaultValidInsertCase_Returns200(
            [Values(Label.BudgetEnergie, Label.NLE)] Label label,
            [Values(ProductType.ELK, ProductType.GAS)] ProductType productType,
            [Values(MeteringDirection.LVR, MeteringDirection.CMB, MeteringDirection.TLV)] MeteringDirection meteringDirection,
            [Values(TariffType.L, TariffType.N, TariffType.T)] TariffType tariffType)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>()
                .SetUsage(100).SetMeteringDirection(meteringDirection.ToString())
                .SetTariffType(tariffType.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(label.ToString()).SetProductType(productType.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            Ic92Db.GetEntityByCondition<Settlement>(s => s.Id.Equals(response.Data.Id)).Should().NotBeNull();
            Ic92Db.GetEntityByCondition<SettlementStatusHistory>(sh => sh.SettlementId.Equals(response.Data.Id))
                .Should().NotBeNull()
                .And.Subject.As<SettlementStatusHistory>().StatusId.Should().Be((short) SettlementStatus.New);
        }

        [Test]
        public void UpsertSettlement_InsertWithoutTariffId_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.InvalidIntFieldValue, "TariffId"));
        }

        [Test]
        public void UpsertSettlement_InsertWithoutSupplierId_Returns400()
        {
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.InvalidIntFieldValue, "SupplierId"));
        }

        [Test]
        public void UpsertSettlement_InsertWithInvalidEanRegex_Returns400(
            [Values("11111111111111111", "1111111111111111111", "ffffffffffffffffff")]
            string ean)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(ean).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidEanRegex);
        }

        [Test]
        public void UpsertSettlement_InsertWithoutEan_Returns400([Values(null, "")] string ean)
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(ean).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, "Ean"));
        }

        [Test]
        public void UpsertSettlement_InsertWithoutProductType_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, "ProductType"));
        }

        [Test]
        public void UpsertSettlement_InsertWithoutLabel_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, "Label"));
        }

        [Test]
        public void UpsertSettlement_InsertWithInvalidContractId_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId("0")
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.InvalidIntFieldValue, "ContractId"));
        }

        [Test]
        public void UpsertSettlement_InsertWithoutEventDate_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetLabel(Label.BudgetEnergie.ToString()).SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidEventDateValue);
        }

        [Test]
        public void UpsertSettlement_InsertWithoutRegisters_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetLabel(Label.BudgetEnergie.ToString()).SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetEventDate(DateTime.Now).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, "Registers"));
        }

        [Test]
        public void UpsertSettlement_InsertWithEmptyRegisters_Returns400()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetLabel(Label.BudgetEnergie.ToString()).SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(new List<Register>().ToArray())
                    .SetEventDate(DateTime.Now).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(Format(PatternMessages.InvalidArrayFieldLength,
                nameof(UpsertSettlementRequest.Settlement.Registers)));
        }

        [Test]
        public void UpsertSettlement_InsertWithInvalidRegisterUsage_Returns400()
        {
            var eanInfo = EanInfos.RandomItem();
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetUsage(-1).SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.InvalidIntFieldValue, "Usage").Replace("een 1", "een 0"));
        }

        [Test]
        public void UpsertSettlement_InsertWithoutRegisterMeteringDirection_Returns400()
        {
            var eanInfo = EanInfos.RandomItem();
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>()
                .SetUsage(100).SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, "MeteringDirection"));
        }

        [Test]
        public void UpsertSettlement_InsertWithoutRegisterTariffType_Returns400()
        {
            var eanInfo = EanInfos.RandomItem();
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100)
                .SetMeteringDirection(MeteringDirection.LVR.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should()
                .BeEquivalentTo(Format(PatternMessages.FieldIsRequired, "TariffType"));
        }

        [Test, Pairwise]
        public void UpsertSettlement_WithInvalidEnums_Returns400()
        {
            const string invalidEnum = "InvalidEnum";
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetMeteringDirection(invalidEnum)
                .SetTariffType(invalidEnum).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(invalidEnum).SetProductType(invalidEnum)
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.InvalidMeteringDirection)
                .And.Subject.Should().Contain(PatternMessages.InvalidTariffTape)
                .And.Subject.Should().Contain(PatternMessages.InvalidLabel)
                .And.Subject.Should().Contain(PatternMessages.InvalidProductType);
        }

        [Test, Pairwise]
        public void UpsertSettlement_DefaultValidUpdateCase_Returns200()
        {
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            var eanInfo = EanInfos.RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte) random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var registerToInsert = BuildDirector.Get<RegisterBuilder>().SetUsage(100)
                .SetMeteringDirection(MeteringDirection.LVR.ToString())
                .SetTariffType(TariffType.L.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var insertRequest = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(registerToInsert).Build()
            };
            var insertResponse = IC92Client.MakeRequest<UpsertSettlementRequest, UpsertSettlementResponse>(insertRequest);
            insertResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, insertResponse.Header.Message);

            // Update Settlement
            var registerToUpdate = BuildDirector.Get<RegisterBuilder>().SetUsage(200)
                .SetMeteringDirection(MeteringDirection.CMB.ToString())
                .SetTariffType(TariffType.N.ToString()).SetOriginalReading(155).SetNewReading(255).Build();
            var updateRequest = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>()
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(registerToUpdate).Build()
            };
            var updateResponse = IC92Client.MakeRequest<UpsertSettlementRequest, UpsertSettlementResponse>(updateRequest);
            updateResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, insertResponse.Header.Message);

            // Assertion
            var updatedSettlement = Ic92Db.GetEntityByCondition<Settlement>(
                s => s.Id.Equals(updateResponse.Data.Id), "Register.MeteringDirection");
            ObjectComparator.ComparePropsOfTypes(updatedSettlement.Register.First().To<Register>(), registerToUpdate)
                .Should().BeTrue();
        }

        [Test]
        public void UpsertSettlement_UpdateWithInvalidId_Returns404()
        {
            const int id = int.MaxValue - 1;
            var eanInfo = EanInfos.RandomItem();
            var marketParty = MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketParty>(mp => true).RandomItem();
            // Insert Tariff
            var insertTariffResponse = new UpsertTariffRequest
            {
                Tariff = BuildDirector.Get<TariffBuilder>()
                    .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                    .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build()
            }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            insertTariffResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, insertTariffResponse.Header.Message);

            // Insert Settlement
            var register = BuildDirector.Get<RegisterBuilder>().SetUsage(100).SetTariffType(TariffType.L.ToString())
                .SetMeteringDirection(MeteringDirection.LVR.ToString()).SetOriginalReading(100).SetNewReading(150).Build();
            var response = new UpsertSettlementRequest
            {
                Settlement = BuildDirector.Get<UpsertSettlementBuilder>().SetId(id)
                    .SetTariffId(insertTariffResponse.Data.Id).SetContractId(eanInfo.ContractId.ToString())
                    .SetEan(eanInfo.EanId).SetEventDate(DateTime.Now).SetLabel(Label.BudgetEnergie.ToString())
                    .SetProductType(ProductType.ELK.ToString())
                    .SetSupplierId(marketParty.Id).SetRegisters(register).Build()
            }.CallWith<UpsertSettlementRequest, UpsertSettlementResponse>(IC92Client);

            // Assertion
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound, response.Header.Message);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.SettlementNotFound, id));
        }
    }
}