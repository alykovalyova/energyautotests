﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.IC92.Base;
using Autotests.IC92.Builders;
using Autotests.Repositories.IC92SettlementModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Ic92Settlement.Contract.Rest.Transport;
using System.Net;
using static System.String;
using ModelTariff = Nuts.Ic92Settlement.Contract.Rest.Model.Tariff;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Tests.IC92
{
    [TestFixture]
    internal class UpsertTariffTests : BaseIC92
    {
        [Test]
        public void UpsertTariff_InsertValidCase_Returns200()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Id.Should().NotBe(0);
            var resultedTariff = Ic92Db.GetEntityByCondition<Tariff>(t => t.Id.Equals(response.Data.Id));
            resultedTariff.Should().NotBeNull();
            ObjectComparator.ComparePropsOfTypes(resultedTariff.To<ModelTariff>(), tariff, false)
                .Should().BeTrue($"{nameof(resultedTariff)} not equal to arranged {nameof(tariff)}");
        }

        [Test]
        public void UpsertTariff_InsertExistedTariff_Returns412()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();

            var response = new UpsertTariffRequest { Tariff = tariff }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            var tariffId = response.Data.Id;
            var resultedTariff = Ic92Db.GetEntityByCondition<Tariff>(t => t.Id.Equals(tariffId));
            response = new UpsertTariffRequest { Tariff = tariff }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.PreconditionFailed, response.Header.Message);
            var message = Format(PatternMessages.TariffExisted, $"{resultedTariff.ValidFrom:M/d/yyyy hh:mm:ss tt}").Replace('-', '/');
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpsertTariff_WithInvalidYear_Returns400()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidYearRegex);
        }

        [Test]
        public void UpsertTariff_WithInvalidQuarter_Returns400()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.InvalidQuarterRegex);
        }

        [Test]
        public void UpsertTariff_WithModifiedByInvalidLength_Returns400()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).SetModifiedBy(new string('a', 256)).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(PatternMessages.WrongModifiedByLength);
        }

        [Test]
        public void UpsertTariff_WithInvalidElecPrice_Returns400()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(-1).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(Format(PatternMessages.InvalidPrice, nameof(tariff.PriceElec)));
        }

        [Test]
        public void UpsertTariff_WithElecPriceMoreThan99_Returns500()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(100).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError, response.Header.Message);
        }

        [Test]
        public void UpsertTariff_WithInvalidGasPrice_Returns400()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(-1)
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, response.Header.Message);
            response.Header.ErrorDetails.First().Should().BeEquivalentTo(Format(PatternMessages.InvalidPrice, nameof(tariff.PriceGas)));
        }

        [Test]
        public void UpsertTariff_WithGasPriceMoreThan99_Returns500()
        {
            var tariff = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(100)
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.InternalServerError, response.Header.Message);
        }

        [Test]
        public void UpsertTariff_UpdateValidCase_Returns200()
        {
            // Insert tariff
            var tariffToInsert = BuildDirector.Get<TariffBuilder>()
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariffToInsert }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);

            // Update tariff
            var tariffToUpdate = BuildDirector.Get<TariffBuilder>().SetId(response.Data.Id)
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).SetModifiedBy(ModifiedBy).Build();
            response = new UpsertTariffRequest { Tariff = tariffToUpdate }.CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);

            // Assertion
            response.Data.Id.Should().NotBe(0);
            var resultedTariff = Ic92Db.GetEntityByCondition<Tariff>(t => t.Id.Equals(response.Data.Id));
            resultedTariff.Should().NotBeNull();
            ObjectComparator.ComparePropsOfTypes(resultedTariff.To<ModelTariff>(), tariffToUpdate, false, nameof(response.Data.Id))
                .Should().BeTrue($"{nameof(resultedTariff)} not equal to arranged {nameof(tariffToUpdate)}");
        }

        [Test]
        public void UpsertTariff_UpdateWithNotExistedId_Returns404()
        {
            const int id = int.MaxValue - 1;
            var tariff = BuildDirector.Get<TariffBuilder>().SetId(id)
                .SetPriceElec(random.Next(6)).SetPriceGas(random.Next(6))
                .SetQuarter((byte)random.Next(1, 4)).SetYear(Year).Build();
            var response = new UpsertTariffRequest { Tariff = tariff }
                .CallWith<UpsertTariffRequest, UpsertTariffResponse>(IC92Client);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound, response.Header.Message);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.TariffNotFound, id));
        }
    }
}
