﻿using System.Globalization;
using System.Net;
using Allure.Commons;
using Allure.NUnit.Attributes;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Builders.FakeEdsn;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Repositories.CcuModels;
using Autotests.Repositories.ICARModels;
using Autotests.Repositories.MeteringPointModels;
using NUnit.Framework;
using Nuts.EdsnSwitch.Contract.Rest.Transport;
using Nuts.FakeEdsn.Contracts;
using Nuts.ICAR.Contract.MeteringPointService;
using Nuts.InterDom.Model.Core.Enums;
using Address = Autotests.Repositories.ICARModels.Address;
using AddressHistory = Autotests.Repositories.ICARModels.AddressHistory;
using CcuHistory = Autotests.Repositories.ICARModels.CcuHistory;
using CcuStatusHistory = Autotests.Repositories.ICARModels.CcuStatusHistory;
using ChangeAddressRequest = Nuts.ICAR.Contract.MeteringPointService.ChangeAddressRequest;
using IcarAddress = Nuts.ICAR.Contract.MeteringPointService.MeteringPoint.Address;
using ICARContext = Autotests.Repositories.ICARModels.ICARContext;
using Register = Autotests.Repositories.ICARModels.Register;
using static Autotests.Helper.GtwFactory;

namespace Autotests.ICAR.Base
{
    [SetUpFixture, Category(Categories.Icar)]
    internal abstract class BaseIcarTest : AllureReport
    {
        //clients declaration
        protected FakeEdsnAdminClient FakeEdsnAdminClient = new FakeEdsnAdminClient();
        protected NutsHttpClient MpClient;
        protected ServiceClient<IMeteringPointService> MeteringPointClient;
        protected RestSchedulerClient Scheduler;

        //databases declaration
        protected DbHandler<ICARContext> IcarDb;
        protected DbHandler<MeteringPointContext> MeteringPointDb;
        protected DbHandler<CommercialCharacteristicContext> CcuDb;

        //helpers declaration
        protected SupplyPhaseEntitiesProvider SupplyPhaseEntity { get; set; }
        protected MduEntityProvider MduProvider { get; set; }

        //variables declaration
        private ConfigHandler _configHandler;
        protected BuildDirector BuildDirector;
        protected const string EanId = "000425200000007771";
        protected const string InvalidEanId = "000123456789124567";

        protected DateTime CurrentDate { get; set; } = DateTime.Today;
        

        [OneTimeSetUp]
        public void TestsSetUp()
        {
            FakeEdsnAdminClient = new FakeEdsnAdminClient();
            Scheduler = new RestSchedulerClient();
            MpClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.Mp));
            MeteringPointClient = new ServiceClient<IMeteringPointService>(_configHandler.ServicesEndpoints.IcarMeteringPointService);
            IcarDb = new DbHandler<ICARContext>(_configHandler.GetConnectionString(DbConnectionName.ICARDatabase.ToString()));
            
            SupplyPhaseEntity = new SupplyPhaseEntitiesProvider();
            MduProvider = new MduEntityProvider();
            BuildDirector = new BuildDirector();

            MeteringPointDb = new DbHandler<MeteringPointContext>(_configHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
            CcuDb = new DbHandler<CommercialCharacteristicContext>(_configHandler.GetConnectionString(DbConnectionName.CcuDatabase.ToString()));
            CleanCcuDb();
            CleanMeteringPointDb();
            CleanIcarDb(new List<string> { EanId, InvalidEanId });
        }

        [TearDown]
        public void CleanEanData()
        {
            CleanIcarDb(new List<string>() { EanId, InvalidEanId });
            CleanCcuDb();
            CleanMeteringPointDb();
        }

        private void CleanMeteringPointDb()
        {
            var mpEans = MeteringPointDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                mp.EanId == EanId);

            foreach (var meteringPoint in mpEans)
            {
                MeteringPointDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.AddressHistory>(r =>
                    r.AddressId == meteringPoint.AddressId);
                var mpHistories = MeteringPointDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mph =>
                    mph.EanId == meteringPoint.EanId);

                foreach (var mpHistory in mpHistories)
                {
                    MeteringPointDb.DeleteEntitiesByCondition<RegisterHistory>(rh =>
                        rh.MeteringPointHistoryId == mpHistory.Id);
                    MeteringPointDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mph =>
                        mph.Id == mpHistory.Id);
                }

                MeteringPointDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Register>(r => r.MeteringPoint.Id == meteringPoint.Id);
                MeteringPointDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                    mp.Id == meteringPoint.Id);
                MeteringPointDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Address>(a =>
                    a.Id == meteringPoint.AddressId);
            }
            Waiter.Wait(() => MeteringPointDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                mp.EanId == EanId).Count == 0);
        }

        private void CleanCcuDb()
        {
            var ccuHistories = CcuDb.GetEntitiesByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId.Equals(EanId) || ccu.EanId.Equals(InvalidEanId));
            foreach (var id in ccuHistories.Select(ccuHistory => ccuHistory.Id))
            {
                CcuDb.DeleteEntitiesByCondition<Repositories.CcuModels.CcuStatusHistory>(ccus => ccus.CcuHistoryId == id);
            }
            CcuDb.DeleteEntitiesByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId.Equals(EanId) || ccu.EanId.Equals(InvalidEanId));

            Waiter.Wait(() => CcuDb.GetEntitiesByCondition<Repositories.CcuModels.CcuHistory>(ccu =>
                ccu.EanId == EanId).Count == 0);
        }

        protected SearchEansByAddressResponse SearchEansByAddress(Address address)
        {
            var response = new SearchEansByAddressRequest
            {
                BuildingNr = address.BuildingNr,
                ExBuildingNr = address.ExBuildingNr,
                ZIPCode = address.Zipcode
            }.CallWith(MeteringPointClient.Proxy.SearchEansByAddress);
            if (!response.Header.StatusCode.Equals((int)HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);
            return response;
        }

        protected SearchAddressByEanResponse SearchAddressByEan(string ean)
        {
            var response = new SearchAddressByEanRequest { EanId = ean }
                .CallWith(MeteringPointClient.Proxy.SearchAddressByEan);
            if (!response.Header.StatusCode.Equals((int)HttpStatusCode.OK))
                throw new AssertionException(response.Header.Message);
            return response;
        }

        protected CommercialCharacteristic CreateCommercialCharacteristic(
            string eanId, string balanceSupplier = null, DateTime? mutationDate = null,
            string dossierId = null, string exRef = null,
            InitiatedParty? initiatedParty = null, EnergyProductTypeCode? productType = null)
        {
            return new CommercialCharacteristic
            {
                EanId = eanId,
                DossierId = dossierId ?? TestConstants.DossierId,
                ExternalReference = exRef ?? TestConstants.ExternalReference,
                InitiatedParty = initiatedParty ?? InitiatedParty.OtherParty,
                ProductType = productType ?? EnergyProductTypeCode.ELK,
                BalanceSupplier = balanceSupplier,
                MutationDate = mutationDate
            };
        }

        public LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint GetExternalLossRequest(string eanId, string balanceSupplier, string oldBalanceSupplier,
            DateTime mutationDate)
        {
            var request = BuildDirector.Get<ExternalLossBuilder>()
                .SetCommercialCharacteristics(balanceSupplier, oldBalanceSupplier)
                .SetEanId(eanId)
                .SetProductType()
                .SetGridOperator(TestConstants.GridOperator)
                .SetMutationData(mutationDate)
                .Build();
            return request;
        }

        [AllureStep("Call ChangeOfSupplier")]
        protected void CallChangeOfSupplier(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            CreateChangeOfSupplierRequest(eanId, TestConstants.BalanceResponsible, balanceSupplier, TestConstants.GridOperator,
                    TestConstants.ExternalReference_SWITCH, int.Parse(TestConstants.BuildingNr), TestConstants.Zipcode,
                    TestConstants.KvkNumber, mutationDate)
                .CallWith<ChangeOfSupplierRequest, ChangeOfSupplierResponse>(MpClient);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetGainsFake);
        }

        [AllureStep("Call MoveIn")]
        protected void CallMoveIn(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            CreateMoveInRequest(eanId, TestConstants.BalanceResponsible, balanceSupplier,
                    TestConstants.GridOperator,
                    TestConstants.ExternalReference, mutationDate.ToString(CultureInfo.InvariantCulture), TestConstants.KvkNumber,
                    int.Parse(TestConstants.BuildingNr),
                    TestConstants.Zipcode)
                .CallWith<MoveInRequest, MoveInResponse>(MpClient);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetGainsFake);
        }

        [AllureStep("Call MoveOut")]
        protected void CallMoveOut(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            CreateMoveOutRequest(eanId, balanceSupplier, TestConstants.GridOperator,
                    TestConstants.ExternalReference, mutationDate)
                .CallWith<MoveOutRequest, MoveOutResponse>(MpClient);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);
        }

        [AllureStep("Call EndOfSupply")]
        protected void CallEndOfSupply(string eanId, string balanceSupplier, DateTime mutationDate)
        {
            CreateEndOfSupplyRequest(eanId, balanceSupplier, TestConstants.GridOperator,
                    TestConstants.ExternalReference_EOS, mutationDate)
                .CallWith<EndOfSupplyRequest, EndOfSupplyResponse>(MpClient);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);
        }

        protected void CleanIcarDb(IEnumerable<string> eans)
        {
            foreach (var ean in eans)
            {
                var mdus = IcarDb.GetEntitiesByCondition<MduHistory>(mdu => mdu.EanId == ean);
                foreach (var mdu in mdus)
                {
                    IcarDb.DeleteEntitiesByCondition<MduStatusHistory>(msh => msh.MduHistoryId == mdu.Id);
                    IcarDb.DeleteEntitiesByCondition<MduRegister>(mr => mr.MduHistoryId == mdu.Id);
                    IcarDb.DeleteEntitiesByCondition<MduMutation>(mm => mm.MduHistoryId == mdu.Id);
                    IcarDb.DeleteEntitiesByCondition<MduHistory>(mh => mh.Id == mdu.Id);
                }

                var ccus = IcarDb.GetEntitiesByCondition<CcuHistory>(ccu => ccu.EanId == ean);
                foreach (var ccu in ccus)
                {
                    IcarDb.DeleteEntitiesByCondition<CcuStatusHistory>(csh => csh.CcuHistoryId == ccu.Id);
                }

                IcarDb.DeleteEntitiesByCondition<CcuHistory>(ccu => ccu.EanId == ean);

                var meteringPoints =
                    IcarDb.GetEntitiesByCondition<Repositories.ICARModels.MeteringPoint>(mp => mp.EanId == ean);
                foreach (var meteringPoint in meteringPoints)
                {
                    IcarDb.DeleteEntitiesByCondition<Register>(r => r.MeteringPointId == meteringPoint.Id);
                    IcarDb.DeleteEntitiesByCondition<Repositories.ICARModels.MeteringPoint>(mp => mp.EanId == meteringPoint.EanId);
                    IcarDb.DeleteEntitiesByCondition<AddressHistory>(ah => ah.EanId == meteringPoint.EanId);
                    IcarDb.DeleteEntitiesByCondition<Address>(a => a.EanId == meteringPoint.EanId);
                }

                Waiter.Wait(() => IcarDb.GetEntitiesByCondition<Repositories.ICARModels.MeteringPoint>(mp =>
                    mp.EanId == ean).Count == 0);
            }
        }

        protected ChangeAddressRequest CreateChangeAddressRequest(IcarAddress address, string ean)
        {
            return new ChangeAddressRequest
            {
                EanId = ean,
                Address = address,
                UserName = "Autotest_User"
            };
        }

        protected static IcarAddress CreateNewAddress()
        {
            return new IcarAddress
            {
                ExBuildingNr = "B",
                BuildingNr = 71,
                CityName = "Kharkiv",
                Country = "NL",
                StreetName = "Peremoga",
                ZIPCode = "1111QQ"
            };
        }
    }
}