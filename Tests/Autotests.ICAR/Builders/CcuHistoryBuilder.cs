﻿using Autotests.Core.Helpers;

namespace Autotests.ICAR.Builders
{
    internal class CcuHistoryBuilder : MainBuilder<Repositories.CcuModels.CcuHistory>
    {
        internal CcuHistoryBuilder SetBalanceSupplierResponsibleId(short? id)
        {
            Instance.BalanceSupplierResponsibleId = id;
            return this;
        }

        internal CcuHistoryBuilder SetCcuStatusHistories(params Repositories.CcuModels.CcuStatusHistory[] histories)
        {
            Instance.CcuStatusHistory = histories;
            return this;
        }

        internal CcuHistoryBuilder SetDossierId(string id)
        {
            Instance.DossierId = id;
            return this;
        }

        internal CcuHistoryBuilder SetEanId(string id)
        {
            Instance.EanId = id;
            return this;
        }

        internal CcuHistoryBuilder SetExternalReference(string exRef)
        {
            Instance.ExternalReference = exRef;
            return this;
        }

        internal CcuHistoryBuilder SetLastComment(string comment)
        {
            Instance.LastComment = comment;
            return this;
        }

        internal CcuHistoryBuilder SetMessageSourceId(short? id)
        {
            Instance.MessageSourceId = id.GetValueOrDefault();
            return this;
        }

        internal CcuHistoryBuilder SetMutationReason(string mutationReason)
        {
            Instance.MutationReason.Identifier = mutationReason;
            return this;
        }

        internal CcuHistoryBuilder SetStatusId(short? id)
        {
            Instance.StatusId = id.GetValueOrDefault();
            return this;
        }

        internal CcuHistoryBuilder SetXmlHeaderMessageId()
        {
            Instance.XmlHeaderMessageId = Guid.NewGuid();
            return this;
        }

        internal CcuHistoryBuilder SetBalanceSupplierId(short? id)
        {
            Instance.BalanceSupplierId = id;
            return this;
        }

        internal CcuHistoryBuilder SetCreatedOn(DateTime createdOn)
        {
            Instance.CreatedOn = createdOn;
            return this;
        }

        internal CcuHistoryBuilder SetMutationDate(DateTime mutationDate)
        {
            Instance.MutationDate = mutationDate;
            return this;
        }

        internal CcuHistoryBuilder SetModifiedOn(DateTime modifiedOn)
        {
            Instance.ModifiedOn = modifiedOn;
            return this;
        }

        internal CcuHistoryBuilder SetXmlHeaderCreationTs(DateTime xmlHeaderCreationTs)
        {
            Instance.XmlHeaderCreationTs = xmlHeaderCreationTs;
            return this;
        }

        internal CcuHistoryBuilder SetOldBalanceSupplierId(short? id)
        {
            Instance.OldBalanceSupplierId = id;
            return this;
        }
    }
}