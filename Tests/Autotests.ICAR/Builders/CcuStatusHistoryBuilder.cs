﻿using Autotests.Core.Helpers;
using Autotests.Repositories.CcuModels;

namespace Autotests.ICAR.Builders
{
    internal class CcuStatusHistoryBuilder : MainBuilder<CcuStatusHistory>
    {
        internal CcuStatusHistoryBuilder SetCcuHistoryId(int id)
        {
            Instance.CcuHistoryId = id;
            return this;
        }

        internal CcuStatusHistoryBuilder SetComment(string comment)
        {
            Instance.Comment = comment;
            return this;
        }

        internal CcuStatusHistoryBuilder SetCreatedOn(DateTime date)
        {
            Instance.CreatedOn = date;
            return this;
        }

        internal CcuStatusHistoryBuilder SetProcessStatusId(short? id)
        {
            Instance.ProcessStatusId = id.GetValueOrDefault();
            return this;
        }
    }
}