﻿using Autotests.Core.Helpers;
using Autotests.Repositories.MeteringPointModels;

namespace Autotests.ICAR.Builders
{
    internal class DbMeteringPointRegisterBuilder : MainBuilder<Register>
    {
        internal DbMeteringPointRegisterBuilder SetMeteringPointId(int mpId)
        {
            Instance.MeteringPointId = mpId;
            return this;
        }

        internal DbMeteringPointRegisterBuilder SetEdsnId(string edsnId)
        {
            Instance.EdsnId = edsnId;
            return this;
        }

        internal DbMeteringPointRegisterBuilder SetTariffTypeId(short tariffTypeId)
        {
            Instance.TariffTypeId = tariffTypeId;
            return this;
        }

        internal DbMeteringPointRegisterBuilder SetMeteringDirectionId(short meteringDirectionId)
        {
            Instance.MeteringDirectionId = meteringDirectionId;
            return this;
        }

        internal DbMeteringPointRegisterBuilder SetNrOfDigits(short nrOfDigits)
        {
            Instance.NrOfDigits = nrOfDigits;
            return this;
        }

        internal DbMeteringPointRegisterBuilder SetMultiplicationFactor(decimal? multiplicationFactor)
        {
            Instance.MultiplicationFactor = multiplicationFactor;
            return this;
        }
    }
}