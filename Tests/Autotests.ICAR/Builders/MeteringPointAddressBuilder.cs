﻿using Autotests.Core.Helpers;
using Nuts.MeteringPoint.Contract;

namespace Autotests.ICAR.Builders
{
    internal class MeteringPointAddressBuilder : MainBuilder<Address>
    {
        internal MeteringPointAddressBuilder SetBuildingNr(int? buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal MeteringPointAddressBuilder SetExBuildingNr(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }

        internal MeteringPointAddressBuilder SetZipcode(string zipcode)
        {
            Instance.ZipCode = zipcode;
            return this;
        }

        internal MeteringPointAddressBuilder SetCountry(string country)
        {
            Instance.Country = country;
            return this;
        }

        internal MeteringPointAddressBuilder SetCityName(string cityName)
        {
            Instance.CityName = cityName;
            return this;
        }

        internal MeteringPointAddressBuilder SetStreetName(string streetName)
        {
            Instance.StreetName = streetName;
            return this;
        }
    }
}