﻿namespace Autotests.ICAR.Enums
{
    internal enum IcarProcessStatus
    {
        New,
        Updated,
        FutureUpdate,
        Skipped,
        Duplicate,
        AlreadyInDb,
        ScheduledRetry,
        NotExistsInDb
    }
}
