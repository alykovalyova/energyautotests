﻿namespace Autotests.ICAR.Enums
{
    internal enum MessageType
    {
        Loss = 0,
        ChangeOfSupplier,
        MoveIn,
        MoveOut,
        Gain
    }
}