﻿namespace Autotests.ICAR.Enums
{
    public enum PermissionType
    {
        FaceToFace = 1,
        ByPhone,
        OnlineChat,
        OnlineOther
    }
}
