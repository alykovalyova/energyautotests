﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class ChangeAddressTests : BaseIcarTest
    {
        [Test]
        public void ChangeIcarAddress_AgentChangesAddress_IsChangedByAgentPropertyIsTrue()
        {
            var currentBuildingNr = 71;
            const int newBuildingNr = 22;

            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate, currentBuildingNr.ToString());
            var initialAddress = Waiter.Wait(() => IcarDb.GetEntityByCondition<Repositories.ICARModels.Address>(
                a => a.EanId == EanId && a.BuildingNr == currentBuildingNr));
            initialAddress.IsChangedByAgent.Should().BeFalse();

            var newAddress = CreateNewAddress();
            newAddress.BuildingNr = newBuildingNr;
            var changeAddressResponse = CreateChangeAddressRequest(newAddress, EanId)
                .CallWith(MeteringPointClient.Proxy.ChangeAddress);
            changeAddressResponse.Header.StatusCode.Should()
                .Be((int)HttpStatusCode.OK, changeAddressResponse.Header.Message);
            var address = Waiter.Wait(() => IcarDb.GetEntityByCondition<Repositories.ICARModels.Address>(
                a => a.EanId == EanId && a.BuildingNr == newBuildingNr));
            address.IsChangedByAgent.Should().BeTrue();
            address.BuildingNr.Should().Be(newBuildingNr);
        }

        [Test]
        public void ChangeIcarAddress_AddressIsUpdatedByEdsn_IsChangedByAgentIsFalse()
        {
            const int currentBuildingNr = 71;
            const int newBuildingNr = 22;

            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate, currentBuildingNr.ToString());
            var initialAddress = Waiter.Wait(() => IcarDb.GetEntityByCondition<Repositories.ICARModels.Address>(
                a => a.EanId == EanId && a.BuildingNr == currentBuildingNr));
            initialAddress.IsChangedByAgent.Should().BeFalse();

            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate, buildingNr: newBuildingNr.ToString());
            Waiter.Wait(() => IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>
                (a => a.EanId == EanId && a.Address.BuildingNr == newBuildingNr && a.Address.IsChangedByAgent == false));
        }

        [Test, Ignore("https://budget.atlassian.net/browse/UT-4975")]
        public void ChangeIcarAddress_DuplicateAddressIsReceivedFromEdsn_IsChangedByAgentIsFalse()
        {
            const int newBuildingNr = 11;
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);
            Waiter.Wait(() => IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(
                    m => m.EanId == EanId, "Address"))
                .Address.IsChangedByAgent.Should().BeFalse();

            var newAddress = CreateNewAddress();
            newAddress.BuildingNr = newBuildingNr;
            var changeAddressResponse = CreateChangeAddressRequest(newAddress, EanId)
                .CallWith(MeteringPointClient.Proxy.ChangeAddress);
            changeAddressResponse.Header.StatusCode.Should()
                .Be((int)HttpStatusCode.OK, changeAddressResponse.Header.Message);
            var address = Waiter.Wait(() => IcarDb.GetEntityByCondition<Repositories.ICARModels.Address>(
                a => a.EanId == EanId && a.BuildingNr == newBuildingNr));
            address.IsChangedByAgent.Should().BeTrue();
            address.BuildingNr.Should().Be(newBuildingNr);

            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate, buildingNr: newBuildingNr.ToString());
            var updatedAddress = Waiter.Wait(() =>
                IcarDb.GetLastEntity<Repositories.ICARModels.AddressHistory, DateTime>(
                    m => m.CreatedOn, m => m.EanId == EanId));
            updatedAddress.IsChangedByAgent.Should().BeFalse();
            updatedAddress.BuildingNr.Should().Be(newBuildingNr);
        }
    }
}