﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class DoWeSupplyEanTests : BaseIcarTest
    {
        [Test]
        public void DoWeSupplyEan_EanNotSuppliedByBE_Returns200()
        {
            var testEan = IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(mp
                => mp.BalanceSupplierId != 65 && mp.BalanceSupplierId != 66 && mp.BalanceSupplierId != 67).EanId;

            var response = new DoWeSupplyEanRequest { Ean = testEan }
                .CallWith(MeteringPointClient.Proxy.DoWeSupplyEan);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.IsBudgetEnergyEan.Should().BeFalse();
        }

        [Test]
        public void DoWeSupplyEan_EanSuppliedByBE_Returns200()
        {
            var testEan = IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(mp
                => mp.BalanceSupplierId == 65 || mp.BalanceSupplierId == 66 || mp.BalanceSupplierId == 67).EanId;

            var response = new DoWeSupplyEanRequest { Ean = testEan }
                .CallWith(MeteringPointClient.Proxy.DoWeSupplyEan);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.IsBudgetEnergyEan.Should().BeTrue();
        }

        [Test]
        public void DoWeSupplyEan_WrongEanField_Returns200()
        {
            const string wrongEanId = "1111111111";
            const string wrongEanField = "Ean";

            var response = new DoWeSupplyEanRequest { Ean = wrongEanId }
                .CallWith(MeteringPointClient.Proxy.DoWeSupplyEan);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.WrongFieldFormat, wrongEanField);
            response.Header.Message.Should().Contain(errorMessage);
        }

        [Test]
        public void DoWeSupplyEan_EmptyEanField_Returns200()
        {
            var response = new DoWeSupplyEanRequest { Ean = string.Empty }
                .CallWith(MeteringPointClient.Proxy.DoWeSupplyEan);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, nameof(DoWeSupplyEanRequest.Ean));
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }
    }
}