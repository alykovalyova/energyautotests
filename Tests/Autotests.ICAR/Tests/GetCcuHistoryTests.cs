﻿using Autotests.Core.Helpers;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Repositories.ICARModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using Nuts.InterDom.Model.Core.Enums;
using System.Net;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Autotests.Core;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.ICAR.Base;
using Autotests.ICAR.Enums;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class GetCcuHistoryTests : BaseIcarTest
    {
        [Test]
        public void GetCcuHistory_DefaultValidCase_Returns200()
        {
            var ccuHistory = IcarDb.GetEntityByCondition<CcuHistory>(mp => true);
            var response = new GetHistoryCCURequest
            {
                EanId = ccuHistory.EanId,
                FromDate = ccuHistory.MutationDate,
                ToDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetCCUHistory);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.History.Should().NotBeEmpty();
        }

        [Test]
        public void GetCcuHistory_NotExistingEan_Returns200()
        {
            const string notExistingEan = "111111111111111111";
            var response = new GetHistoryCCURequest
            {
                EanId = notExistingEan,
                FromDate = CurrentDate.AddDays(-5),
                ToDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetCCUHistory);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.History.Should().BeEmpty();
        }

        [Test]
        public void SendCcu_InvalidEanId_StatusIsScheduledRetry()
        {
            var ccuRequest =
                CreateCommercialCharacteristic(InvalidEanId, TestConstants.OtherPartyBalanceSupplier,
                    exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CallChangeOfSupplier(InvalidEanId, TestConstants.BudgetSupplier, CurrentDate);

            var ccuHistory = Waiter.Wait(() => IcarDb.GetEntityByCondition<CcuHistory>(c =>
                    c.EanId == InvalidEanId &&
                    c.Status.Name == IcarProcessStatus.ScheduledRetry.ToString(),
                "MessageType"), 15);
            ccuHistory.MessageType.Name.Should().Be(MessageType.MoveIn.ToString());
            ccuHistory.LastComment.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);
        }

        [Test]
        public void SendCcu_UpdateStatus_StatusIsSkippedAsDuplicate()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, CurrentDate.AddDays(-1));
            var ccuRequest = CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                CurrentDate, exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);

            CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, CurrentDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, CurrentDate);

            Waiter.Wait(() =>
                IcarDb.GetLastEntity<CcuHistory, DateTime>(cc => cc.CreatedOn,
                    cc => cc.EanId == EanId && cc.Status.Name == IcarProcessStatus.Duplicate.ToString()));
        }

        [Test]
        public void SendCcu_UpdateStatus_StatusIsScheduledRetry()
        {
            FakeEdsnAdminClient.ClearAllMpsSupplierPhase();
            var ccuRequest = CreateCommercialCharacteristic(EanId,
                TestConstants.OtherPartyBalanceSupplier, CurrentDate,
                exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, CurrentDate);

            var ccuHistory = Waiter.Wait(() => IcarDb.GetLastEntity<CcuHistory, DateTime>(cc =>
                    cc.CreatedOn,
                cc => cc.EanId == EanId &&
                      cc.Status.Name == IcarProcessStatus.ScheduledRetry.ToString()));
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);
        }

        [Test]
        public void AcceptChangeOfSupplierStartedByBudget_ExternalLoss()
        {
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate.AddDays(-1));
            var externalLossRequest = GetExternalLossRequest(EanId, TestConstants.OtherPartyBalanceSupplier,
                TestConstants.BudgetSupplier, CurrentDate);
            FakeEdsnAdminClient.SaveExternalLoss(externalLossRequest);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);

            var ccuHistory = Waiter.Wait(() => IcarDb.GetEntityByCondition<CcuHistory>(
                m => m.EanId == EanId && m.Status.Name == IcarProcessStatus.Updated.ToString(), "Status"), 10);
            ccuHistory.MessageTypeId.Should().Be((int)MessageType.Loss);
            ccuHistory.LastComment.Should().BeEquivalentTo(PatternMessages.CcuUpdated);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);
        }

        [Test]
        public void SendCcu_MoveIn_GainMessageType()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, CurrentDate);
            var ccuRequest = CreateCommercialCharacteristic(EanId,
                TestConstants.OtherPartyBalanceSupplier, CurrentDate,
                exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CallMoveIn(EanId, TestConstants.BudgetSupplier, CurrentDate);

            var ccuHistory = Waiter.Wait(() => IcarDb.GetEntityByCondition<CcuHistory>(
                c => c.EanId == EanId && c.Status.Name == IcarProcessStatus.Updated.ToString(),
                "BalanceSupplier", "MessageType"));
            ccuHistory.BalanceSupplier.Ean.Should().Be(TestConstants.BudgetSupplier);
            ccuHistory.MessageType.Name.Should().Be(EDSNMessageType.MoveIn.ToString());
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.MOVEIN);
        }

        [Test]
        public void SendCcu_ChangeOfSupplier_InternalSwitch()
        {
            CleanIcarDb(new List<string>() { EanId });
            SupplyPhaseEntity.AddSupplyPhase(EanId, CurrentDate);
            var ccu = CreateCommercialCharacteristic(EanId, TestConstants.BudgetSupplier, CurrentDate,
                exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccu);
            CallChangeOfSupplier(EanId, TestConstants.NleSupplier, CurrentDate);

            var ccuHistoryGain = Waiter.Wait(() => IcarDb.GetEntityByCondition<CcuHistory>(m =>
                    m.EanId == EanId && m.Status.Name == IcarProcessStatus.AlreadyInDb.ToString(),
                "BalanceSupplier", "MessageType"), 15);
            ccuHistoryGain.BalanceSupplier.Ean.Should().Be(TestConstants.NleSupplier);
            ccuHistoryGain.MessageType.Name.Should().Be(EDSNMessageType.MoveIn.ToString());
            ccuHistoryGain.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);

            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);
            var ccuHistoryLoss = Waiter.Wait(() => IcarDb.GetLastEntity<CcuHistory, DateTime>
                (cc => cc.CreatedOn, cc => cc.EanId == EanId && cc.MessageTypeId == (int)MessageType.Loss));

            ccuHistoryLoss.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);
        }

        [Test]
        public void ProcessFutureUpdate()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, CurrentDate.AddDays(5));
            var ccu = CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                CurrentDate.AddDays(5), exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccu);

            CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, CurrentDate.AddDays(5));
            var ccuHistory =
                Waiter.Wait(
                    () => IcarDb.GetEntityByCondition<CcuHistory>(m =>
                        m.EanId == EanId &&
                        m.Status.Name == IcarProcessStatus.FutureUpdate.ToString()),
                    30);

            IcarDb.UpdateFirstEntity<CcuHistory>(cc => cc.EanId == ccuHistory.EanId,
                cc => cc.MutationDate = CurrentDate);
            Scheduler.TriggerJob(QuartzJobName.Energy_ICAR_ProcessFutureUpdates);
            var ccuHistoryProcessed = Waiter.Wait(() => IcarDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId && m.Status.Name == IcarProcessStatus.Updated.ToString()));
            ccuHistoryProcessed.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);
            var meteringPoint = IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(cc => cc.EanId == EanId);
            meteringPoint.ValidFrom.Should().Be(CurrentDate);
        }

        [Test]
        public void SendCcu_MoveOut()
        {
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);

            var ccuRequest =
                CreateCommercialCharacteristic(EanId, TestConstants.BudgetSupplier, CurrentDate,
                    exRef: TestConstants.ExternalReference_MOVEIN);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CallEndOfSupply(EanId, TestConstants.BudgetSupplier, CurrentDate);

            var ccuHistory = Waiter.Wait(() => IcarDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.Status.Name == IcarProcessStatus.AlreadyInDb.ToString() &&
                m.MessageType.Name == EDSNMessageType.Loss.ToString(), "BalanceSupplier"));
            ccuHistory.BalanceSupplier.Should().BeNull();
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.EOSUPPLY);
            ccuHistory.LastComment.Should().BeEquivalentTo(PatternMessages.DifferentDataArrived);
        }
    }
}