﻿using Autotests.Clients;
using Autotests.Repositories.ICARModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class GetLastMduHistoryByDateTests : BaseIcarTest
    {
        [Test]
        public void GetLastMduHistoryByDate_ValidCase_Returns200()
        {
            var ccuHistory = IcarDb.GetEntityByCondition<CcuHistory>(mp => true);
            var response = new GetLastHistoryMDUByDateRequest
            {
                EanId = ccuHistory.EanId,
                MinDate = ccuHistory.MutationDate,
                MaxDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetLastMDUHistoryByDate);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.LastHistoryMdu.MeteringPoint.EanId.Should().BeEquivalentTo(ccuHistory.EanId);
        }

        [Test]
        public void GetLastMduHistoryByDate_NotExistingEan_Returns200()
        {
            const string wrongEan = "111111111111111111";
            var response = new GetLastHistoryMDUByDateRequest
            {
                EanId = wrongEan,
                MinDate = CurrentDate.AddDays(-15),
                MaxDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetLastMDUHistoryByDate);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.LastHistoryMdu.Should().BeNull();
        }
    }
}