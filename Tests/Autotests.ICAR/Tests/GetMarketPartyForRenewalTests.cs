﻿using Autotests.Clients;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class GetMarketPartyForRenewalTests : BaseIcarTest
    {
        [Test]
        public void GetMarketPartyForRenewal_DefaultValidCase_Returns200()
        {
            var response = new GetMarketPartyForRenewalRequest()
                .CallWith(MeteringPointClient.Proxy.GetMarketPartiesForRenewal);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.MarketParties.Should().NotBeEmpty();
        }
    }
}