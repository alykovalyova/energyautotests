﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class GetMarketPartyNameTests : BaseIcarTest
    {
        [Test]
        public void GetMarketPartyName_Returns200()
        {
            var marketParty = IcarDb.GetEntityByCondition<Repositories.ICARModels.MarketParty>(mp => true);
            var response = new GetMarketPartyNameRequest
            {
                MarketPartiesEans = new List<string> { marketParty.Ean }
            }.CallWith(MeteringPointClient.Proxy.GetMarketPartyName);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.MarketParties.Should().Contain(mp => mp.Name.EndsWith(marketParty.Name));
        }

        [Test]
        public void GetMarketPartyName_NotExistingName_Returns200()
        {
            const string notExistingName = "0000000000000";
            var response = new GetMarketPartyNameRequest
            {
                MarketPartiesEans = new List<string> { notExistingName }
            }.CallWith(MeteringPointClient.Proxy.GetMarketPartyName);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.MarketParties.Should().BeEmpty();
        }

        [Test]
        public void GetMarketPartyName_WrongEanFormat_Returns400()
        {
            const string wrongEan = "123";
            const string marketPartyField = "MarketPartyEan";
            var response = new GetMarketPartyNameRequest
            {
                MarketPartiesEans = new List<string> { wrongEan }
            }.CallWith(MeteringPointClient.Proxy.GetMarketPartyName);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.WrongFieldFormat2, marketPartyField, wrongEan);
            response.Header.Message.Should().Contain(errorMessage);
        }
    }
}