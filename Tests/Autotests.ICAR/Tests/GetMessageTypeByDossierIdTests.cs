﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Repositories.ICARModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using Nuts.InterDom.Model.Core.Enums;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class GetMessageTypeByDossierIdTests : BaseIcarTest
    {
        [Test]
        public void GetMessageTypeByDossierId_DefaultValidCase_Returns200()
        {
            var dbCcu = IcarDb.GetLastEntity<CcuHistory, DateTime>(ccu => ccu.MutationDate,
                ccu => ccu.DossierId != null);

            var response = new GetMessageTypesByDossierIdRequest { DossierId = dbCcu.DossierId }
                .CallWith(MeteringPointClient.Proxy.GetMessageTypesByDossierId);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.MessageTypes.MessageTypes.Should().Contain(mt =>
                mt.MessageType.ToString().ToLower().Equals(((EDSNMessageType)dbCcu.MessageTypeId).ToString().ToLower()));
        }

        [Test]
        public void GetMessageTypeByDossierId_EmptyDossierIdField_Returns400()
        {
            var response = new GetMessageTypesByDossierIdRequest()
                .CallWith(MeteringPointClient.Proxy.GetMessageTypesByDossierId);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, nameof(GetMessageTypesByDossierIdRequest.DossierId));
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }
    }
}