﻿using Autotests.Clients;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using System.Net;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class GetMeteringPointsByDateTests : BaseIcarTest
    {
        [Test]
        public void GetMeteringPointsByDate_Returns200()
        {
            var eanId = IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(mp => true);
            var response = new GetMeteringPointsByDateRequest
            {
                Eans = new List<string> { eanId.EanId },
                MaxDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetMeteringPointsByDate);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.MeteringPoints.Should().Contain(mp => mp.EanId.Equals(eanId.EanId));
        }

        [Test]
        public void GetMeteringPointsByDate_NoMpFound_Returns200()
        {
            const string eanId = "111111111111111111";
            var response = new GetMeteringPointsByDateRequest
            {
                Eans = new List<string> { eanId },
                MaxDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetMeteringPointsByDate);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.MeteringPoints.Should().BeEmpty();
        }
    }
}