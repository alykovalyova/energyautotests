﻿using Autotests.ICAR.Base;
using Autotests.Repositories.ICARModels;
using FluentAssertions;
using NUnit.Framework;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class SearchMeteringPointTests : BaseIcarTest
    {
        [Test]
        public void SearchAddressByEan()
        {
            var meteringPointFromDb = IcarDb
                .GetEntitiesByCondition<Repositories.ICARModels.MeteringPoint>(d => d.EanId.StartsWith("111"), "Address").FirstOrDefault();
            var expectedZipcode = meteringPointFromDb?.Address.Zipcode;

            SearchAddressByEan(meteringPointFromDb?.EanId).Address.ZIPCode.Should().BeEquivalentTo(expectedZipcode);
        }

        [Test]
        public void SearchEansByAddress()
        {
            var meteringPointFromDb = IcarDb
                .GetEntitiesByCondition<MeteringPoint>(d => d.EanId.StartsWith("111"), "Address").First();
            var address = meteringPointFromDb.Address;

            SearchEansByAddress(address).Eans.Should().Contain(meteringPointFromDb.EanId);
        }
    }
}