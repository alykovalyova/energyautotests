﻿using Autotests.Core.Helpers;
using Autotests.Clients.Enums;
using Autotests.Repositories.ICARModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ICAR.Contract.MeteringPointService;
using System.Net;
using Autotests.Clients;
using Autotests.ICAR.Enums;
using Autotests.Core;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.ICAR.Base;

namespace Autotests.ICAR.Tests
{
    [TestFixture]
    internal class SendMduTests : BaseIcarTest
    {
        [Test]
        public void GetMduHistory_Returns200()
        {
            var mduHistory = IcarDb.GetEntityByCondition<MduHistory>(mp => true);
            var response = new GetHistoryMDURequest
            {
                EanId = mduHistory.EanId,
                FromDate = mduHistory.MutationDate,
                ToDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetMDUHistory);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.History.Should().NotBeEmpty();
        }

        [Test]
        public void GetMduHistory_NotExistingEan_Returns200()
        {
            const string notExistingEan = "111111111111111111";
            var response = new GetHistoryMDURequest
            {
                EanId = notExistingEan,
                FromDate = CurrentDate.AddDays(-5),
                ToDate = CurrentDate
            }.CallWith(MeteringPointClient.Proxy.GetMDUHistory);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.History.Should().BeEmpty();
        }

        [Test]
        public void SendMdu_BalanceSupplierIsChanged()
        {
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);
            var originMdu = Waiter.Wait(() =>
                IcarDb.GetEntityByCondition<MduHistory>(m => m.EanId == EanId));
            var originMp = Waiter.Wait(() =>
                IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(m => m.EanId == EanId));
            Waiter.Wait(() =>
                IcarDb.GetEntityByCondition<Repositories.ICARModels.MarketParty>(
                    m => m.Id == originMdu.BalanceSupplierId && m.Id == originMp.BalanceSupplierId).Ean);

            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate, TestConstants.OtherPartyBalanceSupplier);

            var otherPartySupplier = IcarDb.GetEntityByCondition<Repositories.ICARModels.MarketParty>(m => m.Ean == TestConstants.OtherPartyBalanceSupplier);
            var mdu = Waiter.Wait(() =>
                IcarDb.GetEntityByCondition<MduHistory>(m =>
                    m.EanId == EanId && m.BalanceSupplierId == otherPartySupplier.Id));
            var mp = Waiter.Wait(() =>
                IcarDb.GetEntityByCondition<Repositories.ICARModels.MeteringPoint>(m => m.EanId == EanId));

            otherPartySupplier.Id.Should().Be(mdu.BalanceSupplierId);
            otherPartySupplier.Id.Should().Be(mp.BalanceSupplierId);
        }

        [Test]
        public void SendMdu_StatusUpdate_DuplicatedMdu()
        {
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);
            var acceptedMdu = Waiter.Wait(() => IcarDb.GetLastEntity<MduHistory, DateTime>(mdu =>
                        mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));

            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);
            var dublicatedMdu = Waiter.Wait(() => IcarDb.GetLastEntity<MduHistory, DateTime>(mdu =>
                    mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));

            acceptedMdu.Status.Name.Should().Be(IcarProcessStatus.Updated.ToString());
            dublicatedMdu.Status.Name.Should().Be(IcarProcessStatus.Duplicate.ToString());
            dublicatedMdu.LastComment.Should().BeEquivalentTo("Dublicated MDU");
        }

        [Test]
        public void SendMdu_MutationDateInFuture_MduIsNotChanged()
        {
            const string city = "Kiev";
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);
            Waiter.Wait(() => IcarDb.EntityIsInDb<MduHistory>(m => m.EanId == EanId));

            var validFromDate = DateTime.Now.AddDays(20);
            MduProvider.SendMduToFakeEdsn(EanId, validFromDate, city: city);
            var updatedMdu = Waiter.Wait(() => IcarDb.GetEntityByCondition<MduHistory>(m =>
                m.EanId == EanId, "Address"));

            updatedMdu.Address.CityName.Should().NotBeEquivalentTo(city);
        }

        [Test]
        public void SendMdu_StatusUpdate_SkippedMdu()
        {
            var validFrom = DateTime.Now;
            MduProvider.SendMduToFakeEdsn(EanId, validFrom);
            var acceptedMdu = Waiter.Wait(() => IcarDb.GetLastEntity<MduHistory, DateTime>(mdu =>
                mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));

            var secondValidFrom = DateTime.Now.AddDays(-3);
            MduProvider.SendMduToFakeEdsn(EanId, secondValidFrom);
            var skippedMdu = Waiter.Wait(() => IcarDb.GetLastEntity<MduHistory, DateTime>(mdu =>
                mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));

            acceptedMdu.Status.Name.Should().Be(IcarProcessStatus.Updated.ToString());
            skippedMdu.Status.Name.Should().Be(IcarProcessStatus.Skipped.ToString());
            skippedMdu.LastComment.Should().Contain("skipped as old.");
        }

        [Test]
        public void SendMdu_MduUpdated()
        {
            MduProvider.SendMduToFakeEdsn(EanId, CurrentDate);
            var masterData = Waiter.Wait(() => IcarDb.GetLastEntity<MduHistory, DateTime>(mdu =>
                    mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));

            masterData.LastComment.Should().BeEquivalentTo("No MP was found: Insert in DB");
            masterData.Status.Name.Should().Be(IcarProcessStatus.Updated.ToString());
        }

        [Test]
        public void SendMdu_StatusUpdateByJob()
        {
            var futureMduDate = DateTime.Now.AddDays(5);
            var updateMduDate = DateTime.Now;
            MduProvider.SendMduToFakeEdsn(EanId, futureMduDate);

            Waiter.Wait(() => IcarDb.GetEntityByCondition<MduHistory>
                (mp => mp.EanId == EanId, "Status").Status.Name.Should().Be(IcarProcessStatus.FutureUpdate.ToString()));
            //mduHistory.Dictionary_ProcessStatus.Name.Should().Be(IcarProcessStatus.FutureUpdate.ToString());
            IcarDb.UpdateSingleEntity<MduHistory>(mdu => mdu.EanId == EanId,
                mdu => mdu.MutationDate = updateMduDate);

            Scheduler.TriggerJob(QuartzJobName.Energy_ICAR_ProcessFutureUpdates);

            Waiter.Wait(() => IcarDb.GetEntityByCondition<MduHistory>
                (mp => mp.EanId == EanId, "Status").Status.Name.Should().Be(IcarProcessStatus.Updated.ToString()));
        }
    }
}