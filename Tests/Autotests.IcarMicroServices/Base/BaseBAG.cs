﻿using Autotests.Core.Handlers;
using Autotests.Clients;
using NUnit.Framework;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseBAG : BaseTest
    {
        //clients declaration
        protected NutsHttpClient BagClient;

        //variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        protected const string Zipcode = "1011DD";
        protected const int BuildingNumber = 105;
        protected const string Country = "NL";
        protected const string City = "Amsterdam";
        protected const string StreetName = "Harry Banninkstraat";
        protected const string ExBuildingNr = "";

        [OneTimeSetUp]
        public void OneTimeSetUp() => BagClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.Bag));
    }
}