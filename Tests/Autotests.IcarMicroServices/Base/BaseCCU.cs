﻿using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Repositories.CcuModels;
using NUnit.Framework;
using System.Globalization;
using Autotests.Helper.DataProviders.FakeEdsn;
using Nuts.InterDom.Model.Core.Enums;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseCCU : BaseTest
    {
        //clients declaration
        protected NutsHttpClient EdsnSwitchClient;
        protected NutsHttpClient CcuClient;
        protected RestSchedulerClient Scheduler;

        //helpers declaration
        protected SupplyPhaseEntitiesProvider SupplyPhaseEntity;
        protected CcuEntitiesProvider CcuEntityProvider;

        //variables initialization
        protected const string EanId = "111425200000249444";
        protected const string EanIdFake = "000425200000249444";
        protected new DateTime MutationDate = DateTime.Today;
        protected const string Gain = "Gain";
        protected const string Loss = "Loss";
        

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);
            CleanCcuDb();
            FakeEdsnAdminClient.ClearAllCommercialCharacteristics();
            CcuClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.ApiUrls.Ccu));
            EdsnSwitchClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.ApiUrls.EdsnSwitch));
            Scheduler = new RestSchedulerClient();
            SupplyPhaseEntity = new SupplyPhaseEntitiesProvider();
            CcuEntityProvider = new CcuEntitiesProvider();
        }

        protected void CleanCcuDb()
        {
            var ccuHistories = CcuDb.GetEntitiesByCondition<CcuHistory>(ccu => ccu.EanId.Equals(EanId) || ccu.EanId.Equals(EanIdFake));
            foreach (var id in ccuHistories.Select(ccuHistory => ccuHistory.Id))
            {
                CcuDb.DeleteEntitiesByCondition<CcuStatusHistory>(ccus => ccus.CcuHistoryId == id);
            }

            CcuDb.DeleteEntitiesByCondition<CcuHistory>(ccu => ccu.EanId.Equals(EanId) || ccu.EanId.Equals(EanIdFake));
        }

        public void AddCcuHistory(string eanId = EanIdFake, string dossier = null)
        {
            SupplyPhaseEntity.AddSupplyPhase(eanId, MutationDate);
            var ccu = CcuEntityProvider.CreateCommercialCharacteristic(eanId,
                TestConstants.OtherPartyBalanceSupplier,
                TestConstants.OtherPartyBalanceSupplier,
                MutationDate, dossier);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccu);

            CcuEntityProvider.CallChangeOfSupplier(eanId, TestConstants.BudgetSupplier, MutationDate);
            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetGainsFake);
            Waiter.Wait(() => CcuDb
                .GetEntityByCondition<CcuHistory>(
                    m => m.EanId == eanId && m.Status.Identifier == ProcessStatus.Accepted.ToString()));
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            CleanCcuDb();
        }
    }
}