﻿using System.Globalization;
using Autotests.Core.Helpers;
using Autotests.Clients;
using Autotests.Repositories.ICARModels;
using Autotests.Repositories.MeteringPointModels;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Address = Autotests.Repositories.ICARModels.Address;
using MeteringP = Autotests.Repositories.ICARModels.MeteringPoint;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Helper.Builders.FakeEdsn;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseCcuIntegration : BaseCCU
    {
        //clients declaration
        protected NutsHttpClient CcuClient;

        //variables initialization
        protected const string InvalidEanId = "000123456789124567";
        protected new const string EanId = "000425200000007771";

        [OneTimeSetUp]
        public new void OneTimeSetUp()
        {
            CcuClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.ApiUrls.Ccu));
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);
            CleanTestDataInDb();
        }

        [SetUp]
        public void SetUp()
        {
            FakeEdsnAdminClient.ClearAllMdus();
            FakeEdsnAdminClient.ClearAllCommercialCharacteristics();
            CleanTestDataInDb();
        }

        //add to helper - used in several places
        public LossResultResponseEnvelope_Portaal_Content_Portaal_MeteringPoint GetExternalLossRequest(string eanId, string balanceSupplier, string oldBalanceSupplier,
            DateTime mutationDate)
        {
            var request = BuildDirector.Get<ExternalLossBuilder>()
                .SetCommercialCharacteristics(balanceSupplier, oldBalanceSupplier)
                .SetEanId(eanId)
                .SetProductType()
                .SetGridOperator(TestConstants.GridOperator)
                .SetMutationData(mutationDate)
                .Build();
            return request;
        }

        private void CleanTestDataInDb()
        {
            var repeats = 3;
            while (repeats > 0)
            {
                try
                {
                    CleanIcarDb();
                    CleanMeteringPointDb();
                    CleanCcuDb();
                    CleanMduDb();

                    break;
                }
                catch (DbUpdateException)
                {
                    repeats--;
                }
            }
        }

        private void CleanIcarDb()
        {
            var mduHistories = IcarDb.GetEntitiesByCondition<MduHistory>(mdu => mdu.EanId == EanId);
            foreach (var id in mduHistories.Select(mduHistory => mduHistory.Id))
            {
                IcarDb.DeleteEntitiesByCondition<MduStatusHistory>(mdus => mdus.MduHistoryId == id);
                IcarDb.DeleteEntitiesByCondition<MduRegister>(mdur => mdur.MduHistoryId == id);
                IcarDb.DeleteEntitiesByCondition<MduMutation>(mdum => mdum.MduHistoryId == id);
            }
            IcarDb.DeleteEntitiesByCondition<MduHistory>(mdu => mdu.EanId == EanId);

            var ccuHistories = IcarDb.GetEntitiesByCondition<CcuHistory>(ccu => ccu.EanId == EanId || ccu.EanId == InvalidEanId);
            foreach (var ccuHistory in ccuHistories)
            {
                IcarDb.DeleteEntitiesByCondition<CcuStatusHistory>(csh => csh.CcuHistoryId == ccuHistory.Id);
            }
            IcarDb.DeleteEntitiesByCondition<CcuHistory>(ccu => ccu.EanId == EanId || ccu.EanId == InvalidEanId);

            var meteringPoints = IcarDb.GetEntitiesByCondition<MeteringP>(mp => mp.EanId == EanId);
            foreach (var meteringPoint in meteringPoints)
            {
                IcarDb.DeleteEntitiesByCondition<Repositories.ICARModels.Register>(r => r.MeteringPointId == meteringPoint.Id);
            }
            IcarDb.DeleteEntitiesByCondition<MeteringP>(mp => mp.EanId == EanId);
            IcarDb.DeleteEntitiesByCondition<Repositories.ICARModels.AddressHistory>(ah => ah.EanId == EanId);
            IcarDb.DeleteEntitiesByCondition<Address>(a => a.EanId == EanId);
        }

        private void CleanMduDb()
        {
            var mduHistories = MasterDataDb.GetEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(mdu => mdu.EanId.Equals(EanId));
            foreach (var id in mduHistories.Select(mduHistory => mduHistory.Id))
            {
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduStatusHistory>(mdus => mdus.MduHistoryId == id);
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduRegister>(mdus => mdus.MduHistoryId == id);
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduMutation>(mdus => mdus.MduHistoryId == id);
            }
            MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(mdu => mdu.EanId.Equals(EanId));
            Waiter.Wait(() => MasterDataDb.GetEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(mdu => mdu.EanId == EanId).Count == 0);
        }

        private void CleanMeteringPointDb()
        {
            var mpEans = MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                mp.EanId == EanId);

            foreach (var meteringPoint in mpEans)
            {
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.AddressHistory>(r =>
                    r.AddressId == meteringPoint.AddressId);
                var mpHistories = MpDb.GetEntitiesByCondition<MeteringPointHistory>(mph =>
                    mph.EanId == meteringPoint.EanId);

                foreach (var mpHistory in mpHistories)
                {
                    MpDb.DeleteEntitiesByCondition<RegisterHistory>(rh =>
                        rh.MeteringPointHistoryId == mpHistory.Id);
                    MpDb.DeleteEntitiesByCondition<MeteringPointHistory>(mph =>
                        mph.Id == mpHistory.Id);
                }

                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Register>(r => r.MeteringPoint.Id == meteringPoint.Id);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                    mp.Id == meteringPoint.Id);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Address>(a =>
                    a.Id == meteringPoint.AddressId);
            }

            Waiter.Wait(() =>
                MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId).Count == 0);
        }

        protected new void CleanCcuDb()
        {
            var ccuHistories = CcuDb.GetEntitiesByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId.Equals(EanId) || ccu.EanId.Equals(InvalidEanId));
            foreach (var id in ccuHistories.Select(ccuHistory => ccuHistory.Id))
            {
                CcuDb.DeleteEntitiesByCondition<Repositories.CcuModels.CcuStatusHistory>(ccu => ccu.CcuHistoryId == id);
            }
            CcuDb.DeleteEntitiesByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId.Equals(EanId) || ccu.EanId.Equals(InvalidEanId));
            Waiter.Wait(() => CcuDb.GetEntitiesByCondition<Repositories.CcuModels.CcuHistory>(ccu =>
                    ccu.EanId == EanId).Count == 0);
        }
    }
}