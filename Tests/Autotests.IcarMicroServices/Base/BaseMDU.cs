﻿using Autotests.Core.Handlers;
using Autotests.Clients;
using Autotests.Repositories.MasterDataModels;
using NUnit.Framework;
using System.Globalization;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseMdu : BaseTest
    {
        //clients declaration
        protected NutsHttpClient MduClient;

        //variables initialization
        protected const string EanIdTest = "111425200000242777";
        protected const string EanId = "000425200000007771";

        [OneTimeSetUp]
        public virtual void OneTimeSetUp()
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);
            MduClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.ApiUrls.Mdu));
            CleanMduDb();
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate.AddDays(-1));
        }

        private void CleanMduDb()
        {
            var mduHistories = MasterDataDb.GetEntitiesByCondition<MduHistory>(mdu => mdu.EanId.Equals(EanIdTest) || mdu.EanId.Equals(EanId));
            foreach (var id in mduHistories.Select(mduHistory => mduHistory.Id))
            {
                MasterDataDb.DeleteEntitiesByCondition<MduStatusHistory>(mdus => mdus.MduHistoryId == id);
                MasterDataDb.DeleteEntitiesByCondition<MduRegister>(mdus => mdus.MduHistoryId == id);
                MasterDataDb.DeleteEntitiesByCondition<MduMutation>(mdus => mdus.MduHistoryId == id);
            }
            MasterDataDb.DeleteEntitiesByCondition<MduHistory>(mdu => mdu.EanId.Equals(EanIdTest) || mdu.EanId.Equals(EanId));
        }
    }
}