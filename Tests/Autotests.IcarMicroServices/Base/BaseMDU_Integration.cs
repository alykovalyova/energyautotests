﻿using Autotests.Clients;
using Autotests.Repositories.ICARModels;
using Autotests.Repositories.MeteringPointModels;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Globalization;
using Address = Autotests.Repositories.ICARModels.Address;
using AddressHistory = Autotests.Repositories.ICARModels.AddressHistory;
using Register = Autotests.Repositories.ICARModels.Register;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseMduIntegration : BaseMdu
    {
        //client declaration
        protected NutsHttpClient MduClient;

        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-US", false);
            MduClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.ApiUrls.Mdu));
        }

        [SetUp]
        public void SetUp()
        {
            FakeEdsnAdminClient.ClearAllMdus();
            FakeEdsnAdminClient.ClearAllCommercialCharacteristics();
            CleanTestDataInDb();
        }

        private void CleanTestDataInDb()
        {
            var repeats = 3;
            while (repeats > 0)
            {
                try
                {
                    CleanIcarDb();
                    CleanMasterDataDb();
                    CleanMeteringPointDb();
                    break;
                }
                catch (DbUpdateException)
                {
                    repeats--;
                }
            }
        }

        private void CleanIcarDb()
        {
            var mduHistories = IcarDb.GetEntitiesByCondition<MduHistory>(mdu => mdu.EanId == EanId);
            foreach (var id in mduHistories.Select(mduHistory => mduHistory.Id))
            {
                IcarDb.DeleteEntitiesByCondition<MduStatusHistory>(mdus => mdus.MduHistoryId == id);
                IcarDb.DeleteEntitiesByCondition<MduRegister>(mdus => mdus.MduHistoryId == id);
                IcarDb.DeleteEntitiesByCondition<MduMutation>(mdus => mdus.MduHistoryId == id);
            }
            IcarDb.DeleteEntitiesByCondition<MduHistory>(mdu => mdu.EanId == EanId);

            var meteringPoints = IcarDb.GetEntitiesByCondition<Repositories.ICARModels.MeteringPoint>(mp => mp.EanId == EanId);
            foreach (var meteringPoint in meteringPoints)
            {
                IcarDb.DeleteEntitiesByCondition<Register>(r => r.MeteringPointId == meteringPoint.Id);
            }
            IcarDb.DeleteEntitiesByCondition<Repositories.ICARModels.MeteringPoint>(mp => mp.EanId == EanId);
            IcarDb.DeleteEntitiesByCondition<AddressHistory>(ah => ah.EanId == EanId);
            IcarDb.DeleteEntitiesByCondition<Address>(a => a.EanId == EanId);
        }

        private void CleanMasterDataDb()
        {
            var mduHistories = MasterDataDb.GetEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(mdu =>
                mdu.EanId == EanId);
            foreach (var id in mduHistories.Select(mduHistory => mduHistory.Id))
            {
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduStatusHistory>(mdus => mdus.MduHistoryId == id);
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduRegister>(mdus => mdus.MduHistoryId == id);
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduMutation>(mdus => mdus.MduHistoryId == id);
            }
            MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(mdu => mdu.EanId == EanId);
        }

        private void CleanMeteringPointDb()
        {
            var mpEans = MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                mp.EanId == EanId);

            foreach (var meteringPoint in mpEans)
            {
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.AddressHistory>(r =>
                    r.AddressId == meteringPoint.AddressId);
                var mphistories = MpDb.GetEntitiesByCondition<MeteringPointHistory>(mph =>
                    mph.EanId == meteringPoint.EanId);

                foreach (var mpHistory in mphistories)
                {
                    MpDb.DeleteEntitiesByCondition<RegisterHistory>(rh =>
                        rh.MeteringPointHistoryId == mpHistory.Id);
                    MpDb.DeleteEntitiesByCondition<MeteringPointHistory>(mph =>
                        mph.Id == mpHistory.Id);
                }

                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Register>(r => r.MeteringPoint.Id == meteringPoint.Id);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                    mp.Id == meteringPoint.Id);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Address>(a =>
                    a.Id == meteringPoint.AddressId);
            }
        }
    }
}