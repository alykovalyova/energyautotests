﻿using Allure.NUnit.Attributes;
using Autotests.Core.Handlers;
using Autotests.Clients;
using Autotests.Repositories.MeteringPointModels;
using Newtonsoft.Json;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using Autotests.Helper.Builders.FakeEdsn.PreSwitchPhase;
using Address = Nuts.MeteringPoint.Contract.Address;
using Register = Autotests.Repositories.MeteringPointModels.Register;
using MeteringPointHistory = Autotests.Repositories.MeteringPointModels.MeteringPointHistory;
using Autotests.IcarMicroServices.Builders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Helper;
using Autotests.Core;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseMP : BaseTest
    {
        //clients declaration
        protected NutsHttpClient MpClient;

        //variables initialization       
        private ConfigHandler _configHandler = new ConfigHandler();
        private static readonly string _jsonPath = ConfigHelper.GetFilePath("Icar/MeteringPoint.json");
        protected const string EanId = "111256123154675007";
        protected const string EanIdFake = "000256123154675007";
        protected DateTime CurrentDate = DateTime.Now;
        protected const string EanIdPhaseTest = "000425200000007777";
        protected const string EanIdCacheTest = "000425200000007779";
        protected const string NotExistedEanId = "000425200000007788";
        protected const string NotRecognizedEan = "888425200000007700";
        protected const string ExternalRef = "autotest_external_reference";
        protected const string SupplierEan = "8714252018141";
        protected Repositories.MeteringPointModels.MeteringPoint MeteringPoint = GetMeteringPointObject();
        protected Address AddressToRestoreAfterTest;
        
        //helpers declaration
        protected PreswitchPhaseEntitiesProvider PcDefaultValues;
        protected MduEntityProvider MduProvider;
        

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            MpClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.Mp));
            PcDefaultValues = new PreswitchPhaseEntitiesProvider();
            MduProvider = new MduEntityProvider();
            CleanMpDb();
            FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();
            CreateTestDataInDb();
            AddressToRestoreAfterTest = MakeCopyOfAddress();
            MduProvider.SendMduToFakeEdsn(EanIdPhaseTest, CurrentDate);
        }

        [OneTimeTearDown]
        public void TearDownAfterClass() => CleanMpDb();

        [TearDown]
        public void TearDown()
        {
            var testName = TestContext.CurrentContext.Test.Name;
            if (testName.Contains("ChangeAddress") && testName.Contains("Returns200"))
                RestoreAddress();
            CleanMpDb();
            FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();
        }

        private GetSCMPInformationResponseEnvelope_PC_PMP GetEanPortalContent(string ean, string productType,
            GetSCMPInformationResponseEnvelope_MPAddressResponseType address, bool hasMeter, GetSCMPInformationResponseEnvelope_PC_PMP_PM mutationData)
        {
            return BuildDirector.Get<PreSwitchPhaseBuilder>()
                .SetEanId(ean)
                .SetGridOperator(PcDefaultValues.GridOperatorCompany)
                .SetGridArea(TestConstants.GridArea)
                .SetAddress(address)
                .SetEnergyMeter(hasMeter ? PcDefaultValues.EnergyMeter : null)
                .SetProductType(productType)
                .SetPhysicalCharacteristics(PcDefaultValues.PhysicalCharacteristics)
                .SetPortalMutation(mutationData)
                .SetMarketSegment(TestConstants.MarketSegment)
                .Build();
        }
        private GetSCMPInformationResponseEnvelope_MPAddressResponseType GetAddress(string buildingNr)
        {
            return BuildDirector.Get<PreswitchAddressBuilder>()
                .SetCity(TestConstants.City)
                .SetZipcode(TestConstants.Zipcode)
                .SetStreet(TestConstants.Street)
                .SetExbuildingNr(TestConstants.ExBuildingNr)
                .SetBuildingNr(buildingNr)
                .SetBagAddress()
                .Build();
        }

        private GetSCMPInformationResponseEnvelope_PC_PMP_PM GetMutationData(DateTime mutationDate)
        {
            return BuildDirector.Get<PreSwitchMutationDataBuilder>()
                .SetMutationDate(mutationDate)
                .SetExternalReference(TestConstants.ExternalReference)
                .SetDossier(PcDefaultValues.Dossier)
                .Build();
        }

        [AllureStep("SaveValidEanToFake")]
        protected void SaveValidEanToPreSwitchPhase(string ean, DateTime mutationDate,
            string productType = "GAS", string buildingNr = "71", bool hasMeter = true)
        {
            var address = GetAddress(buildingNr);
            var mutationData = GetMutationData(mutationDate);
            var eanPortalContent = GetEanPortalContent(ean, productType, address, hasMeter, mutationData);
            var getMpPreSwitchResponse = new GetSCMPInformationResponseEnvelope()
            {
                EDSNBusinessDocumentHeader = new GetSCMPInformationResponseEnvelope_EDSNBusinessDocumentHeader(),
                Portaal_Content = new GetSCMPInformationResponseEnvelope_PC()
                { Item = eanPortalContent }
            };
            FakeEdsnAdminClient.ClearAllMpsPreSwitchPhase();
            FakeEdsnAdminClient.SavePreSwitchPhaseMeteringPoint(new List<GetSCMPInformationResponseEnvelope> { getMpPreSwitchResponse });
        }

        protected GetMeteringPointsPreSwitchPhaseRequest GetMeteringPointPreSwitchPhaseRequest(string eanId, string externalReference, string balanceSupplier)
        {
            return new GetMeteringPointsPreSwitchPhaseRequest()
            {
                Connections = new List<ConnectionDetail>()
                {
                    new ConnectionDetail()
                    {
                        EanId = eanId,
                        ExternalReference = externalReference,
                        SupplierEan = balanceSupplier
                    }
                }
            };
        }

        protected void CleanMpDb()
        {
            var masterDatas = MasterDataDb.GetEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(md =>
            md.EanId == EanIdPhaseTest ||
                 md.EanId == EanIdCacheTest ||
                 md.EanId == NotExistedEanId ||
                 md.EanId == NotRecognizedEan);
            foreach (var masterData in masterDatas)
            {
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduStatusHistory>(msh =>
                msh.MduHistoryId == masterData.Id);
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduMutation>(mm =>
                mm.MduHistoryId == masterData.Id);
                MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduRegister>(mr =>
                mr.MduHistoryId == masterData.Id);
            }

            MasterDataDb.DeleteEntitiesByCondition<Repositories.MasterDataModels.MduHistory>(mh =>
                mh.EanId == EanIdPhaseTest ||
                 mh.EanId == EanIdCacheTest ||
                 mh.EanId == NotExistedEanId ||
                 mh.EanId == NotRecognizedEan);

            var mpEans = MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                 mp.EanId == EanIdPhaseTest ||
                 mp.EanId == EanIdCacheTest ||
                 mp.EanId == NotExistedEanId ||
                 mp.EanId == NotRecognizedEan);

            foreach (var meteringPoint in mpEans)
            {
                MpDb.DeleteEntitiesByCondition<AddressHistory>(r =>
                    r.AddressId == meteringPoint.AddressId);
                var mphistories = MpDb.GetEntitiesByCondition<MeteringPointHistory>(mph =>
                    mph.EanId == meteringPoint.EanId);

                foreach (var mpHistory in mphistories)
                {
                    MpDb.DeleteEntitiesByCondition<RegisterHistory>(rh =>
                        rh.MeteringPointHistoryId == mpHistory.Id);
                    MpDb.DeleteEntitiesByCondition<MeteringPointHistory>(mph =>
                        mph.Id == mpHistory.Id);
                }

                MpDb.DeleteEntitiesByCondition<Register>(r => r.MeteringPoint.Id == meteringPoint.Id);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                    mp.Id == meteringPoint.Id);
            }

            var preswitchEans = MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(mp =>
                       mp.EanId == EanIdPhaseTest ||
                       mp.EanId == EanIdCacheTest ||
                       mp.EanId == NotExistedEanId ||
                       mp.EanId == NotRecognizedEan);
            foreach (var ean in preswitchEans)
            {
                var mphistories = MpDb.GetEntitiesByCondition<MeteringPointHistory>(mph =>
                    mph.EanId == ean.EanId);
                MpDb.DeleteEntitiesByCondition<PreSwitchRegister>(r => r.PreSwitchMeteringPoint.EanId == ean.EanId);
                MpDb.DeleteEntitiesByCondition<AddressHistory>(r => r.EanId == ean.EanId);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(r => r.EanId == ean.EanId);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Address>(r => r.EanId == ean.EanId);
                foreach (var mpHistory in mphistories)
                {
                    MpDb.DeleteEntitiesByCondition<RegisterHistory>(rh => rh.MeteringPointHistoryId == mpHistory.Id);
                }

                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(r => r.EanId == ean.EanId);
                MpDb.DeleteEntitiesByCondition<Repositories.MeteringPointModels.Address>(a =>
                    a.Id == ean.AddressId);
            }

        }

        private void RestoreAddress()
        {
            var request = new ChangeAddressRequest { Address = AddressToRestoreAfterTest, Ean = EanId };
            MpClient.MakeRequest<ChangeAddressRequest, ChangeAddressResponse>(request);
        }

        private Address MakeCopyOfAddress()
        {
            var mpAddress = MeteringPoint.Address;
            return BuildDirector.Get<MeteringPointAddressBuilder>()
                .SetBuildingNr(mpAddress.BuildingNr).SetExBuildingNr(mpAddress.ExBuildingNr)
                .SetZipcode(mpAddress.Zipcode).SetCountry(mpAddress.Country)
                .SetCityName(mpAddress.CityName).SetStreetName(mpAddress.StreetName).Build();
        }

        private void CreateTestDataInDb()
        {
            if (MpDb.EntityIsInDb<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)) return;
            try
            {
                var meteringPoint = MpDb.AddEntityToDb(MeteringPoint);
                MpDb.AddEntityToDb(GetRegisterObject(meteringPoint));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private Register GetRegisterObject(Repositories.MeteringPointModels.MeteringPoint meteringPoint)
        {
            return BuildDirector.Get<DbMeteringPointRegisterBuilder>()
                .SetMeteringPointId(meteringPoint.Id).SetEdsnId(1.ToString()).SetTariffTypeId(1)
                .SetMeteringDirectionId(1).SetNrOfDigits(2).SetMultiplicationFactor(1)
                .Build();
        }

        private static Repositories.MeteringPointModels.MeteringPoint GetMeteringPointObject()
        {
            return JsonConvert.DeserializeObject<Repositories.MeteringPointModels.MeteringPoint>(File.ReadAllText(_jsonPath));
        }
    }
}