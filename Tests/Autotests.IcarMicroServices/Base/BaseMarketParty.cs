﻿using Autotests.Core.Handlers;
using Autotests.Clients;
using Newtonsoft.Json;
using NUnit.Framework;
using ConfigHelper = Autotests.Core.ConfigHelper;
using Autotests.Core;

namespace Autotests.IcarMicroServices.Base
{
    internal abstract class BaseMarketParty : BaseTest
    {
        //client declaration
        protected NutsHttpClient MarketPartyClient;

        //variables initialization
        private readonly string _jsonPath = ConfigHelper.GetFilePath("Icar/MarketParties.json");
        protected static MarketPartiesPattern MarketPartiesPattern { get; private set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            MarketPartyClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.ApiUrls.MarketParty));
            MarketPartiesPattern = JsonConvert.DeserializeObject<MarketPartiesPattern>(File.ReadAllText(_jsonPath));
            CleanMarketPartyDb();
        }

        private void CleanMarketPartyDb()
        {
            MarketPartyDb.DeleteEntitiesByCondition<Repositories.MarketPartyModels.MarketPartyInfo>(mp =>
                mp.Other == "Created by autotests");
        }
    }
}