﻿using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.IcarMicroServices.Builders;
using Autotests.Repositories.BAGModels;
using Autotests.Repositories.CcuModels;
using Autotests.Repositories.ICARModels;
using Autotests.Repositories.MarketPartyModels;
using Autotests.Repositories.MasterDataModels;
using Autotests.Repositories.MeteringPointModels;
using NUnit.Framework;
using Nuts.BagAddress.Contract;
using Nuts.CommercialCharacteristic.Contract;
using Nuts.MarketParty.Contract;
using Nuts.MasterData.Contract;
using Nuts.MeteringPoint.Contract;
using CapTarCode = Autotests.Repositories.MasterDataModels.CapTarCode;
using DoWeSupplyEanRequest = Nuts.MeteringPoint.Contract.DoWeSupplyEanRequest;
using EnumEnergyAllocationMethodCode = Autotests.Repositories.MasterDataModels.EnumEnergyAllocationMethodCode;
using EnumEnergyConnectionPhysicalStatusCode = Autotests.Repositories.MasterDataModels.EnumEnergyConnectionPhysicalStatusCode;
using EnumEnergyDeliveryStatusCode = Autotests.Repositories.MasterDataModels.EnumEnergyDeliveryStatusCode;
using EnumEnergyFlowDirectionCode = Autotests.Repositories.MasterDataModels.EnumEnergyFlowDirectionCode;
using EnumEnergyMeteringMethodCode = Autotests.Repositories.MasterDataModels.EnumEnergyMeteringMethodCode;
using EnumEnergyMeterTypeCode = Autotests.Repositories.MasterDataModels.EnumEnergyMeterTypeCode;
using EnumEnergyProductTypeCode = Autotests.Repositories.MasterDataModels.EnumEnergyProductTypeCode;
using EnumEnergyUsageProfileCode = Autotests.Repositories.MasterDataModels.EnumEnergyUsageProfileCode;
using EnumMarketSegmentCode = Autotests.Repositories.MasterDataModels.EnumMarketSegmentCode;
using EnumPhysicalCapacityCode = Autotests.Repositories.MasterDataModels.EnumPhysicalCapacityCode;
using GetMarketPartyForRenewalRequest = Nuts.MarketParty.Contract.GetMarketPartyForRenewalRequest;
using GetMarketPartyNameRequest = Nuts.MarketParty.Contract.GetMarketPartyNameRequest;
using GetMessageTypesByDossierIdRequest = Nuts.CommercialCharacteristic.Contract.GetMessageTypesByDossierIdRequest;
using GridArea = Autotests.Repositories.MasterDataModels.GridArea;

namespace Autotests.IcarMicroServices.Base
{
    [SetUpFixture, Category(Categories.IcarServices)]
    internal abstract class BaseTest : AllureReport
    {
        //client declaration
        protected FakeEdsnAdminClient FakeEdsnAdminClient;

        //databases declaration
        protected DbHandler<BAGContext> BagRepo;
        protected DbHandler<MeteringPointContext> MpDb;
        protected DbHandler<ICARContext> IcarDb;
        protected DbHandler<CommercialCharacteristicContext> CcuDb;
        protected DbHandler<MarketPartyContext> MarketPartyDb;
        protected DbHandler<MasterDataContext> MasterDataDb;

        //helpers declaration
        protected MduEntityProvider MasterDataProvider { get; set; }
        protected BuildDirector BuildDirector;

        //variables initialization
        public ConfigHandler ConfigHandler = new ConfigHandler();
        protected DateTime MutationDate = DateTime.Today;

        [OneTimeSetUp]
        public void MainOneTimeSetUp()
        {
            FakeEdsnAdminClient = new FakeEdsnAdminClient();
            MasterDataProvider = new MduEntityProvider();

            BuildDirector = new BuildDirector();

            BagRepo = new DbHandler<BAGContext>(ConfigHandler.GetConnectionString(DbConnectionName.BagDatabase.ToString()));
            MpDb = new DbHandler<MeteringPointContext>(ConfigHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
            IcarDb = new DbHandler<ICARContext>(ConfigHandler.GetConnectionString(DbConnectionName.ICARDatabase.ToString()));
            CcuDb = new DbHandler<CommercialCharacteristicContext>(ConfigHandler.GetConnectionString(DbConnectionName.CcuDatabase.ToString()));
            MarketPartyDb = new DbHandler<MarketPartyContext>(ConfigHandler.GetConnectionString(DbConnectionName.MarketPartyDatabase.ToString()));
            MasterDataDb = new DbHandler<MasterDataContext>(ConfigHandler.GetConnectionString(DbConnectionName.MasterDataDatabase.ToString()));
        }

        protected GetBagAddressRequest CreateGetBagAddressRequest(int buildingNr, string zipcode)
        {
            return new GetBagAddressRequest
            {
                BuildingNr = buildingNr,
                ZIPCode = zipcode
            };
        }

        protected T CreateRequest<T>(string id = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            if (typeof(T) == typeof(GetHistoryCcuRequest))
                return new GetHistoryCcuRequest { EanId = id, FromDate = fromDate, ToDate = toDate }.TypeTo<T>();
            if (typeof(T) == typeof(GetLastHistoryCcuRequest))
                return new GetLastHistoryCcuRequest { EanId = id, FromDate = fromDate, ToDate = toDate }.TypeTo<T>();
            if (typeof(T) == typeof(GetMessageTypesByDossierIdRequest))
                return new GetMessageTypesByDossierIdRequest { DossierId = id }.TypeTo<T>();

            return default;
        }

        protected T CreateMarketPartyRequest<T>(List<string> marketPartiesEans = null)
        {
            if (typeof(T) == typeof(GetMarketPartyForRenewalRequest))
                return new GetMarketPartyForRenewalRequest().TypeTo<T>();
            if (typeof(T) == typeof(GetMarketPartyNameRequest))
                return new GetMarketPartyNameRequest { MarketPartiesEans = marketPartiesEans }.TypeTo<T>();

            return default(T);
        }

        protected UpsertMarketPartyInfoRequest GetUpsertMarketPartyInfoRequest(
            string marketPartyName = null, string[] marketPartyEans = null,
            string phoneNumber = null, string email = "email.test@blabla.com",
            bool isAvailableForIc92 = true, bool isDeleted = false)
        {
            const string requestType = "RequestType";
            const string other = "Created by autotests";
            return new UpsertMarketPartyInfoRequest
            {
                MarketPartyInfo = BuildDirector.Get<UpsertMarketPartyInfoBuilder>()
                    .SetAvailableForIc92(isAvailableForIc92)
                    .SetEmail(email)
                    .SetIsDeleted(isDeleted)
                    .SetMarketPartyEans(marketPartyEans)
                    .SetMarketPartyName(marketPartyName)
                    .SetPhoneNumber(phoneNumber)
                    .SetRequestType(requestType)
                    .SetOther(other).Build()
            };
        }

        protected static GetMduHistoryRequest GenerateGetMduHistoryRequest(string eanId = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            return new GetMduHistoryRequest
            {
                EanId = eanId,
                FromDate = fromDate,
                ToDate = toDate
            };
        }

        protected bool CheckEnums(MasterData masterData, string eanId)
        {
            var commCharacteristics = masterData.CommercialCharacteristics;
            var physicalCharacteristics = masterData.PhysicalCharacteristics;
            var mduHistoryFromDb = DbHelper.GetMduHistory(eanId);

            var list = new List<bool>
            {
                commCharacteristics.BalanceResponsibleParty == MasterDataDb
                    .GetEntityByCondition<Repositories.MasterDataModels.MarketParty>(mp => mp.Id == mduHistoryFromDb.BalanceResponsiblePartyId).Ean,
                commCharacteristics.BalanceSupplier == MasterDataDb
                    .GetEntityByCondition<Repositories.MasterDataModels.MarketParty>(mp => mp.Id == mduHistoryFromDb.BalanceSupplierId).Ean,
                commCharacteristics.MeteringResponsibleParty == MasterDataDb
                    .GetEntityByCondition<Repositories.MasterDataModels.MarketParty>(mp => mp.Id == mduHistoryFromDb.MeteringResponsiblePartyId).Ean,
                masterData.EnergyMeter.Type == MasterDataDb
                    .GetEntityByCondition<EnumEnergyMeterTypeCode>(e => e.Id == mduHistoryFromDb.TypeId).Identifier,
                masterData.MarketSegment == MasterDataDb
                    .GetEntityByCondition<EnumMarketSegmentCode>(e => e.Id == mduHistoryFromDb.MarketSegmentId).Identifier,
                physicalCharacteristics.AllocationMethod == MasterDataDb
                    .GetEntityByCondition<EnumEnergyAllocationMethodCode>(e => e.Id == mduHistoryFromDb.AllocationMethodId).Identifier,
                physicalCharacteristics.EnergyDeliveryStatus == MasterDataDb
                    .GetEntityByCondition<EnumEnergyDeliveryStatusCode>(e => e.Id == mduHistoryFromDb.EnergyDeliveryStatusId).Identifier,
                physicalCharacteristics.EnergyFlowDirection == MasterDataDb
                    .GetEntityByCondition<EnumEnergyFlowDirectionCode>(e => e.Id == mduHistoryFromDb.EnergyFlowDirectionId).Identifier,
                physicalCharacteristics.MeteringMethod == MasterDataDb
                    .GetEntityByCondition<EnumEnergyMeteringMethodCode>(e => e.Id == mduHistoryFromDb.MeteringMethodId).Identifier,
                physicalCharacteristics.PhysicalCapacity == MasterDataDb
                    .GetEntityByCondition<EnumPhysicalCapacityCode>(e => e.Id == mduHistoryFromDb.PhysicalCapacityId).Identifier,
                physicalCharacteristics.PhysicalStatus == MasterDataDb
                    .GetEntityByCondition<EnumEnergyConnectionPhysicalStatusCode>(e => e.Id == mduHistoryFromDb.PhysicalStatusId).Identifier,
                physicalCharacteristics.ProfileCategory == MasterDataDb
                    .GetEntityByCondition<EnumEnergyUsageProfileCode>(e => e.Id == mduHistoryFromDb.ProfileCategoryId).Identifier,
                masterData.ProductType == MasterDataDb
                    .GetEntityByCondition<EnumEnergyProductTypeCode>(e => e.Id == mduHistoryFromDb.ProductTypeId).Identifier,
                masterData.GridArea == MasterDataDb
                    .GetEntityByCondition<GridArea>(g => g.Id == mduHistoryFromDb.GridAreaId).Ean,
                physicalCharacteristics.CapTarCode == MasterDataDb
                    .GetEntityByCondition<CapTarCode>(cap => cap.Id == mduHistoryFromDb.CapTarCodeId).Ean
            };
            return list.TrueForAll(el => el);
        }

        protected T CreateRequest<T>(
            Nuts.MeteringPoint.Contract.Address address = null, DateTime? maxDate = null,
            string ean = null, List<string> eans = null,
            int? buildingNr = null, string exBuildingNr = null, string zipcode = null)
        {
            if (typeof(T) == typeof(ChangeAddressRequest))
                return new ChangeAddressRequest { Address = address, Ean = ean }.TypeTo<T>();
            if (typeof(T) == typeof(DoWeSupplyEanRequest))
                return new DoWeSupplyEanRequest { Ean = ean }.TypeTo<T>();
            if (typeof(T) == typeof(GetMeteringPointRequest))
                return new GetMeteringPointRequest { Ean = ean }.TypeTo<T>();
            if (typeof(T) == typeof(GetMeteringPointsByDateRequest))
            {
                return maxDate != null
                    ? new GetMeteringPointsByDateRequest { Eans = eans, MaxDate = (DateTime)maxDate }.TypeTo<T>()
                    : new GetMeteringPointsByDateRequest { Eans = eans }.TypeTo<T>();
            }
            if (typeof(T) == typeof(SearchAddressByEanRequest))
                return new SearchAddressByEanRequest { Ean = ean }.TypeTo<T>();
            if (typeof(T) == typeof(SearchEansByAddressRequest))
            {
                return new SearchEansByAddressRequest
                {
                    BuildingNr = buildingNr,
                    ExBuildingNr = exBuildingNr,
                    ZipCode = zipcode
                }.TypeTo<T>();
            }

            return default;
        }

        protected bool CheckMeteringPointEnums(Nuts.MeteringPoint.Contract.MeteringPoint contractMp,
            Repositories.MeteringPointModels.MeteringPoint dbMp)
        {
            var gridOperator = contractMp.GridOperator;
            var commCharacteristics = contractMp.CommercialCharacteristics;
            var physicalCharacteristics = contractMp.PhysicalCharacteristics;

            var list = new List<bool>
            {
                commCharacteristics.BalanceResponsibleParty == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.MarketParty>(mp => mp.Id == dbMp.BalanceResponsiblePartyId).Ean,
                commCharacteristics.BalanceSupplier == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.MarketParty>(mp => mp.Id == dbMp.BalanceSupplierId).Ean,
                commCharacteristics.MeteringResponsibleParty == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.MarketParty>(mp => mp.Id == dbMp.MeteringResponsiblePartyId).Ean,
                contractMp.EnergyMeter.Type == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyMeterTypeCode>(e => e.Id == dbMp.TypeId).Identifier,
                contractMp.GridArea == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.GridArea>(g => g.Id == dbMp.GridAreaId).Ean,
                contractMp.GridOperator == gridOperator,
                contractMp.MarketSegment == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumMarketSegmentCode>(e => e.Id == dbMp.MarketSegmentId).Identifier,
                physicalCharacteristics.AllocationMethod == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyAllocationMethodCode>(e => e.Id == dbMp.AllocationMethodId).Identifier,
                physicalCharacteristics.CapTarCode == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.CapTarCode>(cap => cap.Id == dbMp.CapTarCodeId).Ean,
                physicalCharacteristics.EnergyDeliveryStatus == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyDeliveryStatusCode>(e => e.Id == dbMp.EnergyDeliveryStatusId).Identifier,
                physicalCharacteristics.EnergyFlowDirection == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyFlowDirectionCode>(e => e.Id == dbMp.EnergyFlowDirectionId).Identifier,
                physicalCharacteristics.MeteringMethod == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyMeteringMethodCode>(e => e.Id == dbMp.MeteringMethodId).Identifier,
                physicalCharacteristics.PhysicalCapacity == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumPhysicalCapacityCode>(e => e.Id == dbMp.PhysicalCapacityId).Identifier,
                physicalCharacteristics.PhysicalStatus == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyConnectionPhysicalStatusCode>(e => e.Id == dbMp.PhysicalStatusId).Identifier,
                physicalCharacteristics.ProfileCategory == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyUsageProfileCode>(e => e.Id == dbMp.ProfileCategoryId).Identifier,
                contractMp.ProductType == MpDb
                    .GetEntityByCondition<Repositories.MeteringPointModels.EnumEnergyProductTypeCode>(e => e.Id == dbMp.ProductTypeId).Identifier
            };
            return list.TrueForAll(el => el);
        }
    }
}