﻿using Autotests.Core.Helpers;
using Nuts.MarketParty.Contract;

namespace Autotests.IcarMicroServices.Builders
{
    internal class UpsertMarketPartyInfoBuilder : MainBuilder<UpsertMarketPartyInfo>
    {
        internal UpsertMarketPartyInfoBuilder SetAvailableForIc92(bool isAvailable)
        {
            Instance.AvailableForIc92 = isAvailable;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetEmail(string email)
        {
            Instance.Email = email;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetIsDeleted(bool isDeleted)
        {
            Instance.IsDeleted = isDeleted;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetMarketPartyEans(params string[] marketPartyEans)
        {
            Instance.MarketPartyEans = marketPartyEans;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetMarketPartyName(string marketPartyName)
        {
            Instance.MarketPartyName = marketPartyName;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetPhoneNumber(string phoneNr)
        {
            Instance.PhoneNumber = phoneNr;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetRequestType(string type)
        {
            Instance.RequestType = type;
            return this;
        }

        internal UpsertMarketPartyInfoBuilder SetOther(string other)
        {
            Instance.Other = other;
            return this;
        }
    }
}