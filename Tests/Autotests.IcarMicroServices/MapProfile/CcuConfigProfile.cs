﻿using AutoMapper;
using Autotests.Repositories.CcuModels;

namespace Autotests.IcarMicroServices.MapProfile
{
    public class CcuConfigProfile : Profile
    {
        public CcuConfigProfile()
        {
            CreateMap<CcuHistory, Nuts.CommercialCharacteristic.Contract.CcuHistory>()
                .ForMember(dest => dest.EanId, opt => opt.MapFrom(src => src.EanId))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => src.CreatedOn))
                .ForMember(dest => dest.BalanceSupplier, opt => opt.MapFrom(src => src.BalanceSupplier.Ean))
                .ForMember(dest => dest.BalanceSupplierResponsible, opt => opt.Ignore())
                .ForMember(dest => dest.HeaderCreationTs, opt => opt.MapFrom(src => src.XmlHeaderCreationTs))
                .ForMember(dest => dest.HeaderMessageId, opt => opt.MapFrom(src => src.XmlHeaderMessageId))
                .ForPath(dest => dest.Mutation.MutationReason, opt => opt.MapFrom(src => src.MutationReason.Identifier))
                .ForPath(dest => dest.Mutation.MutationDate, opt => opt.MapFrom(src => src.MutationDate))
                .ForPath(dest => dest.Mutation.DossierId, opt => opt.MapFrom(src => src.DossierId))
                .ForPath(dest => dest.Mutation.ExternalReference, opt => opt.MapFrom(src => src.ExternalReference))
                .ForMember(dest => dest.OldBalanceSupplier, opt => opt.MapFrom(src => src.OldBalanceSupplier.Ean))
                .ForPath(dest => dest.MessageSource, opt => opt.MapFrom(src => src.MessageSource.Identifier));
        }
    }
}