﻿namespace Autotests.IcarMicroServices.Models
{
    internal class RejectionInfo
    {
        public Rejection Rejection { get; set; }
    }

    internal class Rejection
    {
        public List<RejectionData> Rejections { get; set; }
        public string RejectionMessage { get; set; }
    }

    internal class RejectionData
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}