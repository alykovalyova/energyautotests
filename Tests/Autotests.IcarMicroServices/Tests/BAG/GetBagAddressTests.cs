﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using Autotests.Repositories.BAGModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.BagAddress.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.BAG
{
    [TestFixture]
    internal class GetBagAddressTests : BaseBAG
    {
        [Test]
        public void GetBagAddress_WithValidData_Returns200()
        {
            if (!BagRepo.EntityIsInDb<BagData>(bag =>
                (bag.Wijkcode + bag.Lettercombinatie).Equals(Zipcode) && bag.Huisnr == BuildingNumber))
                Assert.Ignore($"Bag DB does not contain info for Zipcode {Zipcode} and buildingNumber {BuildingNumber}");

            var response = CreateGetBagAddressRequest(BuildingNumber, Zipcode)
                .CallWith<GetBagAddressRequest, GetBagAddressResponse>(BagClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            var addressRes = response.Data.Addresses.First();
            addressRes.BuildingNr.Should().Be(BuildingNumber);
            addressRes.ZipCode.Should().BeEquivalentTo(Zipcode);
            addressRes.Country.Should().BeEquivalentTo(Country);
            addressRes.CityName.Should().BeEquivalentTo(City);
            addressRes.StreetName.Should().BeEquivalentTo(StreetName);
            addressRes.ExBuildingNr.Should().BeEquivalentTo(ExBuildingNr);
        }

        [Test]
        public void GetBagAddress_WrongZipcode_Returns400()
        {
            const string wrongZipcode = "1110";

            var response = CreateGetBagAddressRequest(BuildingNumber, wrongZipcode).CallWith(BagClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ZipCodeError);
        }

        [Test]
        public void GetBagAddress_WrongZeroBuildingNumber_Returns400()
        {
            const int zeroBuildingNumber = 0;

            var response = CreateGetBagAddressRequest(zeroBuildingNumber, Zipcode).CallWith(BagClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.BuildingNumberIntError);
        }

        [Test]
        public void GetBagAddress_NotRealData_Returns200AndEmptyList()
        {
            const string unrealZipcode = "1110QQ";

            var response = CreateGetBagAddressRequest(BuildingNumber, unrealZipcode)
                .CallWith<GetBagAddressRequest, GetBagAddressResponse>(BagClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Addresses.Should().BeEmpty();
        }
    }
}