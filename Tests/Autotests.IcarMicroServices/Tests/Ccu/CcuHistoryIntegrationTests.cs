﻿using Autotests.Core.Helpers;
using Autotests.Clients.Enums;
using Autotests.Repositories.CcuModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using ProcessStatus = Autotests.Helper.Enums.ProcessStatus;
using MeterPointAddress = Autotests.Repositories.MeteringPointModels.Address;
using Autotests.IcarMicroServices.Base;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Helper.Enums;

namespace Autotests.IcarMicroServices.Tests.Ccu
{
    [TestFixture]
    internal class CcuHistoryIntegrationTests : BaseCcuIntegration
    {
        [Test]
        public void SendCcu_CcuValidDateMoreThanMpDate()
        {
            const string balanceResponsibleParty = "8717953171467";
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate.AddDays(-3));

            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate, balanceResponsible: balanceResponsibleParty);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId,
                TestConstants.BudgetSupplier, balanceResponsibleParty, MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveOut(EanId, TestConstants.BudgetSupplier, MutationDate);

            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == EanId && mp.BalanceResponsibleParty.Ean == balanceResponsibleParty, "BalanceSupplier"));
            meteringPoint.BalanceSupplier.Should().BeNull();
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2), 60);
        }

        [Test]
        public void SendCcu_CcuValidDateEqualToMpDate()
        {
            const string balanceResponsibleParty = "8717953171467";
            var mutationDate = MutationDate.AddDays(-3);
            MasterDataProvider.SendMduToFakeEdsn(EanId, mutationDate);

            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate, balanceResponsible: balanceResponsibleParty);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId,
                TestConstants.OtherPartyBalanceSupplier, balanceResponsibleParty, mutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveOut(EanId, TestConstants.BudgetSupplier, mutationDate);

            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == EanId && mp.BalanceResponsibleParty.Ean == balanceResponsibleParty, "BalanceSupplier"));
            meteringPoint.BalanceSupplier.Should().BeNull();
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2), 30);
        }

        [Test]
        public void SendCcu_CcuValidDateEqualToMpDate_CcuXmlHeaderDateLessThanMeteringPointXmlHeaderDate()
        {
            const string balanceResponsibleParty = "8717953171467";
            var mutationDate = MutationDate.AddDays(-3);
            MasterDataProvider.SendMduToFakeEdsn(EanId, mutationDate);
            var headerCreationDate = MpDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                mp.EanId == EanId).HeaderCreationDate;
            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.MeteringPoint>(cc =>
                cc.EanId == EanId, cc => cc.HeaderCreationDate = headerCreationDate.AddMinutes(5));

            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate, balanceResponsible: balanceResponsibleParty);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId,
                TestConstants.OtherPartyBalanceSupplier, balanceResponsibleParty, mutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveOut(EanId, TestConstants.BudgetSupplier, mutationDate);

            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == EanId, "BalanceSupplier", "BalanceResponsibleParty"));
            meteringPoint.BalanceSupplier.Ean.Should().Be(TestConstants.BudgetSupplier);
            meteringPoint.BalanceResponsibleParty.Ean.Should().Be(TestConstants.BalanceResponsible);
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2), 60);
        }

        [Test]
        public void SendCcu_CcuValidDateLessThanMpDate()
        {
            const string balanceResponsibleParty = "8717953171467";
            var mutationDate = MutationDate.AddDays(-3);
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate.AddMonths(-1));
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);

            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId,
                TestConstants.BudgetSupplier, balanceResponsibleParty, mutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveOut(EanId, TestConstants.BudgetSupplier, mutationDate);

            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                mp => mp.EanId == EanId, "BalanceSupplier", "BalanceResponsibleParty"));
            meteringPoint.BalanceSupplier.Ean.Should().Be(TestConstants.BudgetSupplier);
            meteringPoint.BalanceResponsibleParty.Ean.Should().Be(TestConstants.BalanceResponsible);
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(3), 60);
        }

        [Test]
        public void SendCcu_UpdateStatus_DuplicatedCcu()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, MutationDate);

            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, MutationDate);

            var ccuHistory = Waiter.Wait(() => CcuDb.GetLastEntity<CcuHistory, DateTime>(cc =>
                cc.CreatedOn, cc => cc.EanId == EanId && cc.Status.Identifier == ProcessStatus.Duplicate.ToString()));
            Waiter.Wait(() => MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
               .Count.Should().Be(1));
            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(1);
            ccuHistory.LastComment.Should().BeEquivalentTo(Core.Helpers.PatternMessages.DublicateCcu);
        }

        [Test]
        public void SendCcu_ChangeOfSupplierEvent_GainMessageSource()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate.AddDays(-3), TestConstants.NleSupplier);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId,
                TestConstants.OtherPartyBalanceSupplier, mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallChangeOfSupplier(ccuRequest.EanId, TestConstants.NleSupplier, MutationDate.AddDays(-3));

            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(cc =>
                cc.EanId == EanId && cc.Status.Identifier == ProcessStatus.Accepted.ToString() &&
                cc.MessageSource.Identifier == Gain), 10);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);

            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
                .Count.Should().Be(1);
            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(1);
        }

        [Test]
        public void SendCcu_ChangeOfSupplierEvent_GainWithAddressChange()
        {
            const string newZipcode = "1212QQ";
            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate.AddDays(-3), TestConstants.NleSupplier);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallChangeOfSupplier(ccuRequest.EanId, TestConstants.NleSupplier, MutationDate.AddDays(-3));

            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate.AddDays(-3), TestConstants.BudgetSupplier,
                zipCode: "1212QQ");
            var ccuRequestUpdated = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.NleSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequestUpdated);
            CcuEntityProvider.CallChangeOfSupplier(ccuRequest.EanId, TestConstants.BudgetSupplier, MutationDate.AddDays(-3));

            Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m => m.EanId == EanId), 10);
            var address = Waiter.Wait(() => MpDb.GetEntityByCondition<MeterPointAddress>(cc => cc.EanId == EanId));
            address.Zipcode.Should().Be(newZipcode);

            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
                .Count.Should().Be(1);
            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2);
        }

        [Test]
        public void SendCcu_MoveInEvent_GainMessageSource()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate);
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId,
                TestConstants.OtherPartyBalanceSupplier, mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveIn(EanId, TestConstants.BudgetSupplier, MutationDate);

            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(c =>
                c.EanId == EanId &&
                c.Status.Identifier == ProcessStatus.Accepted.ToString() &&
                c.MessageSource.Identifier == Gain), 30);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.MOVEIN);

            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
                .Count.Should().Be(1), 30);
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(1), 30);
            var ccuHistoryLoss = Waiter.Wait(() =>
                CcuDb.GetLastEntity<CcuHistory, DateTime>(cc => cc.CreatedOn, cc =>
                    cc.EanId == EanId && cc.MessageSource.Identifier == Gain));
            ccuHistoryLoss.MutationReasonId.Should().Be((int)MutationReasonCode.MOVEIN);
        }

        [Test]
        public void SendCcu_ChangeOfSupplierEvent_InternalSwitch()
        {
            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate);

            var ccu = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.BudgetSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccu);
            CcuEntityProvider.CallChangeOfSupplier(EanId, TestConstants.NleSupplier, MutationDate);

            var ccuHistoryGain = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                    m.EanId == EanId &&
                    m.Status.Identifier == ProcessStatus.Accepted.ToString(),
                "MessageSource", "MutationReason"));
            ccuHistoryGain.MessageSource.Identifier.Should().Be(Gain);
            ccuHistoryGain.MutationReason.Identifier.Should().Be(MutationReasonCode.SWITCHLV.ToString());

            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);

            var ccuHistoryLoss = Waiter.Wait(() =>
                CcuDb.GetLastEntity<CcuHistory, DateTime>(cc => cc.CreatedOn, cc =>
                        cc.EanId == EanId &&
                        cc.MessageSource.Identifier == Loss &&
                        cc.Status.Identifier == ProcessStatus.Accepted.ToString(),
                    "MutationReason"));
            ccuHistoryLoss.MutationReason.Identifier.Should().Be(MutationReasonCode.SWITCHLV.ToString());

            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
                .Count.Should().Be(1), 30);
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2), 30);
            var ccuHistoryLossMeteringPoint = Waiter.Wait(() =>
                CcuDb.GetLastEntity<CcuHistory, DateTime>(cc =>
                        cc.CreatedOn, cc => cc.EanId == EanId && cc.MessageSource.Identifier == Loss,
                    "MutationReason"));
            ccuHistoryLossMeteringPoint.MutationReason.Identifier.Should()
                .Be(MutationReasonCode.SWITCHLV.ToString());
        }

        [Test]
        public void SendCcu_ProcessFutureUpdateCcuByJob()
        {
            var mutationDate = DateTime.Now.AddDays(3);

            SupplyPhaseEntity.AddSupplyPhase(EanId, mutationDate);
            var ccu = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                mutationDate: mutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccu);
            CcuEntityProvider.CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, mutationDate);

            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.Status.Identifier == ProcessStatus.FutureProcessing.ToString()), 20);

            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                mp.EanId == EanId).Count.Should().Be(0);
            MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp =>
                mp.EanId == EanId).Count.Should().Be(0);
            CcuDb.UpdateFirstEntity<CcuHistory>(cc => cc.EanId == ccuHistory.EanId,
                cc => cc.MutationDate = DateTime.Now);
            Scheduler.TriggerJob(QuartzJobName.Energy_CommercialCharacteristic_FutureProcessingCcuJob);
            Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                    m.EanId == EanId && m.Status.Identifier == ProcessStatus.Accepted.ToString()));
            CcuDb.GetEntityByCondition<CcuHistory>(cc => cc.EanId == EanId, "MutationReason")
                .MutationReason.Identifier.Should().Be(MutationReasonCode.SWITCHLV.ToString());
        }

        [Test]
        public void SendCcu_ProcessScheduleRetryCcuByJob()
        {
            FakeEdsnAdminClient.ClearAllMpsSupplierPhase();
            var ccu = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccu);
            CcuEntityProvider.CallChangeOfSupplier(EanId, TestConstants.BudgetSupplier, MutationDate);

            Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.Status.Identifier == ProcessStatus.ScheduledRetry.ToString()), 20);

            SupplyPhaseEntity.AddSupplyPhase(EanId, MutationDate);
            Scheduler.TriggerJob(QuartzJobName
                .Energy_CommercialCharacteristic_ScheduledRetryProcessingCcuJob);

            Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.Status.Identifier == ProcessStatus.Accepted.ToString()));
            CcuDb.GetEntityByCondition<CcuHistory>(cc => cc.EanId == EanId, "MutationReason")
                .MutationReason.Identifier.Should().Be(MutationReasonCode.SWITCHLV.ToString());
        }

        [Test]
        public void SendCcu_MoveOutEvent_LossMessageSource()
        {
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);

            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.BudgetSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveOut(EanId, TestConstants.BudgetSupplier, MutationDate);

            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.MessageSource.Identifier == Loss &&
                m.Status.Identifier == ProcessStatus.Accepted.ToString()), 30);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.MOVEOUT);

            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
                .Count.Should().Be(1), 30);
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2), 30);
            var ccuHistoryLoss = Waiter.Wait(() => CcuDb.GetLastEntity<CcuHistory, DateTime>(cc =>
                    cc.CreatedOn,
                cc => cc.EanId == EanId && cc.MessageSource.Identifier == MessageType.Loss.ToString()));
            ccuHistoryLoss.MutationReasonId.Should().Be((int)MutationReasonCode.MOVEOUT);
        }

        [Test]
        public void SendCcu_EndOfSupplyEvent_LossMessageSource()
        {
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate.AddDays(-3));

            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallEndOfSupply(EanId, TestConstants.BudgetSupplier, MutationDate);

            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                    m.EanId == EanId && m.Status.Identifier == ProcessStatus.Accepted.ToString(),
                "MessageSource"), 30);
            ccuHistory.MessageSource.Identifier.Should().Be(Loss);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.EOSUPPLY);

            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == EanId)
                .Count.Should().Be(1), 30);
            Waiter.Wait(() => MpDb
                .GetEntitiesByCondition<Repositories.MeteringPointModels.MeteringPointHistory>(mp => mp.EanId == EanId)
                .Count.Should().Be(2), 30);
            var ccuHistoryLoss = Waiter.Wait(() =>
                CcuDb.GetLastEntity<CcuHistory, DateTime>(cc =>
                    cc.CreatedOn, cc => cc.EanId == EanId, "MessageSource"));
            ccuHistoryLoss.MutationReasonId.Should().Be((int)MutationReasonCode.EOSUPPLY);
            ccuHistoryLoss.MessageSource.Identifier.Should().Be(Loss);
        }

        [Test]
        public void SendCcu_ChangeOfSupplierEvent_ExternalLoss()
        {
            CleanCcuDb();
            var externalLossRequest = GetExternalLossRequest(EanId, TestConstants.OtherPartyBalanceSupplier,
                TestConstants.BudgetSupplier, MutationDate);
            FakeEdsnAdminClient.SaveExternalLoss(externalLossRequest);

            Scheduler.TriggerJob(QuartzJobName.Energy_EdsnSwitch_GetLossesFake);
            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.Status.Identifier == ProcessStatus.Accepted.ToString() &&
                m.MessageSource.Identifier == Loss), 15);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.SWITCHLV);
        }

        [Test, Category(Core.Helpers.Categories.Icar)]
        public void SendCcu_UpdateStatus_StatusIsRetryScheduled()
        {
            FakeEdsnAdminClient.ClearAllMpsSupplierPhase();
            var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(EanId, TestConstants.OtherPartyBalanceSupplier,
                mutationDate: MutationDate);
            FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
            CcuEntityProvider.CallMoveIn(EanId, TestConstants.BudgetSupplier, MutationDate);

            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<CcuHistory>(m =>
                m.EanId == EanId &&
                m.Status.Identifier == IcarProcessStatus.ScheduledRetry.ToString() &&
                m.MessageSource.Identifier == Gain, "BalanceSupplier"));
            ccuHistory.BalanceSupplier.Ean.Should().Be(TestConstants.BudgetSupplier);
            ccuHistory.MutationReasonId.Should().Be((int)MutationReasonCode.MOVEIN);
        }
    }
}