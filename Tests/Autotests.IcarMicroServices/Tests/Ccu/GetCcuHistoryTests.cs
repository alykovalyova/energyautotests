﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.CommercialCharacteristic.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.Ccu
{
    [TestFixture]
    internal class GetCcuHistoryTests : BaseCCU
    {
        [Test]
        public void GetCcuHistory_DefaultValidTest_Returns200()
        {
            AddCcuHistory();
            var response = CreateRequest<GetHistoryCcuRequest>(EanIdFake, DateTime.Today.Date,
                    DateTime.Today.Date).CallWith<GetHistoryCcuRequest, GetHistoryCcuResponse>(CcuClient);

            var ccuHistory = CcuDb.GetEntityByCondition<Repositories.CcuModels.CcuHistory>(ccu =>
                ccu.EanId == EanIdFake, "BalanceSupplier");
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.History.Should().NotBeEmpty();
            ccuHistory.BalanceSupplier.Ean.Should().Be(response.Data.History.First().BalanceSupplier);
        }

        [Test]
        public void GetCcuHistory_WithNotExistedEanId_Returns404()
        {
            const string invalidEanId = "111000000000000123";

            var response = CreateRequest<GetHistoryCcuRequest>(invalidEanId).CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoHistoryFound, invalidEanId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetCcuHistory_WithWrongFromDatePeriod_Returns404()
        {
            var response = CreateRequest<GetHistoryCcuRequest>(EanIdFake, DateTime.Today.AddDays(11)).CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoHistoryFound, EanIdFake);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetCcuHistory_WithWrongToDatePeriod_Returns404()
        {
            var response = CreateRequest<GetHistoryCcuRequest>(EanIdFake, toDate: MutationDate.AddDays(-1))
                .CallWith<GetHistoryCcuRequest>(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoHistoryFound, EanIdFake);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetCcuHistory_WithToDateLessThanFromDate_Returns400()
        {
            var response = CreateRequest<GetHistoryCcuRequest>(EanIdFake, DateTime.Today, DateTime.Today.AddDays(-1))
                .CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ToDateLessThanFromDate);
        }

        [Test]
        [TestCase("ffffffffffffffffff", PatternMessages.InvalidEanField, TestName = "GetCcuHistory_WithInvalidEanIdExpressionChars_Returns400")]
        [TestCase("1234567891234567899", PatternMessages.InvalidEanField, TestName = "GetCcuHistory_WithInvalidEanIdExpressionLength_Returns400")]
        public void GetCcuHistory_InvalidEanIdValues_Returns400(string eanId, string patternErrorMessage)
        {
            var response = CreateRequest<GetHistoryCcuRequest>(eanId).CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(patternErrorMessage);
        }
    }
}