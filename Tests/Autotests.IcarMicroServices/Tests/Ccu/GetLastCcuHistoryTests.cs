﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.CommercialCharacteristic.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.Ccu
{
    [TestFixture]
    internal class GetLastCcuHistoryTests : BaseCCU
    {
        [Test]
        public void GetLastCcuHistory_DefaultValidTest_Returns200()
        {
            AddCcuHistory();
            var response = CreateRequest<GetLastHistoryCcuRequest>(EanIdFake,
                DateTime.Today.Date, DateTime.Today.Date.AddDays(10))
                .CallWith<GetLastHistoryCcuRequest, GetLastHistoryCcuResponse>(CcuClient);

            var ccuHistory = CcuDb.GetLastEntity<Repositories.CcuModels.CcuHistory, DateTime>(ccu =>
                ccu.CreatedOn, ccu => ccu.EanId == EanIdFake, "BalanceSupplier");
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            ccuHistory.BalanceSupplier.Ean.Should().Be(response.Data.History.BalanceSupplier);
        }

        [Test]
        public void GetLastCcuHistory_WithNotExistedEanId_Returns404()
        {
            const string invalidEnaId = "111000000000000123";
            var response = CreateRequest<GetLastHistoryCcuRequest>(invalidEnaId).CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoHistoryFound, invalidEnaId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetLastCcuHistory_WithWrongFromDatePeriod_Returns404()
        {
            var response = CreateRequest<GetLastHistoryCcuRequest>(EanIdFake, DateTime.Today.AddDays(11))
                .CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoHistoryFound, EanIdFake);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetLastCcuHistory_WithWrongToDatePeriod_Returns404()
        {
            var response =
                CreateRequest<GetLastHistoryCcuRequest>(EanIdFake, toDate: MutationDate.AddDays(-1))
                .CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoHistoryFound, EanIdFake);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetLastCcuHistory_WithToDateLessThanFromDate_Returns404()
        {
            var response =
                CreateRequest<GetLastHistoryCcuRequest>(EanIdFake, DateTime.Today, DateTime.Today.AddDays(-2))
                .CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ToDateLessThanFromDate);
        }

        [Test]
        [TestCase("ffffffffffffffffff", PatternMessages.InvalidEanField, TestName = "GetLastCcuHistory_WithInvalidEanIdExpressionChars_Returns400")]
        [TestCase("1234567891234567899", PatternMessages.InvalidEanField, TestName = "GetLastCcuHistory_WithInvalidEanIdExpressionLength_Returns400")]
        public void GetLastCcuHistory_InvalidEnaIdValues_Returns400(string eanId, string patternErrorMessage)
        {
            var response = CreateRequest<GetLastHistoryCcuRequest>(eanId).CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(patternErrorMessage);
        }
    }
}