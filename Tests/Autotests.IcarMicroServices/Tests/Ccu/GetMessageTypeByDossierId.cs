﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.CommercialCharacteristic.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Tests.IcarMicroservices.CCU
{
    [TestFixture]
    internal class GetMessageTypeByDossierId : BaseCCU
    {
        [Test]
        public void GetMessageTypeByDossierId_ValidDossierId_Returns200()
        {
            var dossier = RandomDataProvider.GetRandomNumbersString(8);
            AddCcuHistory(dossier: dossier);
            var ccuHistory = Waiter.Wait(() => CcuDb.GetEntityByCondition<Repositories.CcuModels.CcuHistory>(
                ccu => ccu.EanId == EanIdFake && ccu.DossierId == dossier, "MessageSource"));
            var response = CreateRequest<GetMessageTypesByDossierIdRequest>(ccuHistory.DossierId)
                .CallWith<GetMessageTypesByDossierIdRequest, GetMessageTypesByDossierIdResponse>(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.MessageTypes.Ean.Should().BeEquivalentTo(EanIdFake);
            response.Data.MessageTypes.MessageTypes.Should()
                .Contain(mt => mt.MutationDate.Equals(ccuHistory.MutationDate))
                .And.Subject.Should().Contain(mt => mt.DossierId.Equals(dossier))
                .And.Subject.Should().Contain(mt => mt.MessageSource.Equals(ccuHistory.MessageSource.Identifier));
        }

        [Test]
        public void GetMessageTypeByDossierId_WithNotExistedDossierId_Returns404()
        {
            var response = CreateRequest<GetMessageTypesByDossierIdRequest>("22222222")
                .CallWith(CcuClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().Contain(PatternMessages.NoMessageTypesFound);
        }
    }
}