﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MarketParty.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.IcarMicroServices.Tests.MarketParty
{
    [TestFixture]
    internal class MarketPartyTests : BaseMarketParty
    {
        [Test]
        public void MarketParty_RetrieveAllMarketParties_RetrievedEntitiesEqualPattern()
        {
            var response = CreateMarketPartyRequest<GetMarketPartyForRenewalRequest>()
                .CallWith<GetMarketPartyForRenewalRequest, GetMarketPartyForRenewalResponse>(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            MarketPartiesPattern.CompareTo(response.Data.MarketParties).Should().Be(0);
        }

        [Test]
        public void MarketParty_RetrieveParticularMarketParty_RetrievedEntityEqualsPattern()
        {
            var marketParties = new List<Nuts.MarketParty.Contract.MarketParty> { MarketPartiesPattern.MarketParties[0] };
            var response =
                CreateMarketPartyRequest<GetMarketPartyNameRequest>(marketParties.Select(el => el.MarketPartyEan).ToList())
                    .CallWith<GetMarketPartyNameRequest, GetMarketPartyNameResponse>(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.MarketParties.Should().HaveCount(marketParties.Count);
            response.Data.MarketParties.Should().Contain(mp => mp.MarketPartyEan.Equals(marketParties[0].MarketPartyEan));
            response.Data.MarketParties.Should().Contain(mp => mp.Name.Equals(marketParties[0].Name));
            response.Data.MarketParties.Should().Contain(mp => mp.Role.Equals(marketParties[0].Role));
        }

        [Test]
        public void MarketParty_RetrieveAllCertainMarketParties_RetrievedEntitiesEqualPattern()
        {
            var response =
                CreateMarketPartyRequest<GetMarketPartyNameRequest>(MarketPartiesPattern.MarketParties
                    .Select(el => el.MarketPartyEan).ToList())
                    .CallWith<GetMarketPartyNameRequest, GetMarketPartyNameResponse>(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            MarketPartiesPattern.CompareTo(response.Data.MarketParties).Should().Be(0);
        }

        [Test]
        [TestCase("", PatternMessages.InvalidMarketPartyEan, TestName = "MarketParty_WithEmptyMarketPartyEan_Returns400")]
        [TestCase("fffffffffffff", PatternMessages.InvalidMarketPartyEan, TestName = "MarketParty_WithInvalidMarketPartyEanChars_Returns400")]
        [TestCase("12345678912345", PatternMessages.InvalidMarketPartyEan, TestName = "MarketParty_WithInvalidMarketPartyEanNums_Returns400")]
        [TestCase("empty", PatternMessages.MarketPartiesListIsEmpty, TestName = "MarketParty_WithEmptyMarketPartyEanList_Returns400")]
        public void MarketParty_InvalidValues_Returns400(string marketPartyEan, string errorMessagePattern)
        {
            var request = CreateMarketPartyRequest<GetMarketPartyNameRequest>(marketPartyEan.Equals("empty") 
                ? new List<string>() 
                : new List<string> { marketPartyEan });

            var response = request.CallWith(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(errorMessagePattern.Replace("value", marketPartyEan));
        }

        [Test]
        public void MarketParty_WithMarketPartiesIsNull_Returns400()
        {
            var response = CreateMarketPartyRequest<GetMarketPartyNameRequest>()
                .CallWith<GetMarketPartyNameRequest, GetMarketPartyNameResponse>(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired,
                nameof(GetMarketPartyNameRequest.MarketPartiesEans));
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        [TestCase(true, TestName = "GetMarketPartyInfo_AvailableForIC92_ValidTest_Returns200")]
        [TestCase(false, TestName = "GetMarketPartyInfo_UnavailableForIC92_ValidTest_Returns200")]
        public void GetMarketPartyInfo_ValidTest(bool isAvailableForIca92)
        {
            var response = new GetMarketPartyInfoRequest {AvailableForIc92 = isAvailableForIca92}
                .CallWith<GetMarketPartyInfoRequest, GetMarketPartyInfoResponse>(MarketPartyClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);


            var actualMarketPartyNames = response.Data.MarketPartyInfo.Select(mp => mp.MarketPartyName).ToList();
            var expectedMarketPartiesNames =
                MarketPartyDb.GetEntitiesByCondition<Repositories.MarketPartyModels.MarketPartyInfo>(mp => mp.AvailableForIc92 == isAvailableForIca92 && mp.LastStatusId != 3)
                    .Select(mp => mp.MarketPartyName).ToList();
            expectedMarketPartiesNames.Should().BeEquivalentTo(actualMarketPartyNames);
        }

        [Test]
        public void UpsertMarketParty_ValidCase_Returns200()
        {
            var marketPartyEans = new[] { "19283746" };
            const string marketPartyName = "marketPartyName";
            const string phoneNumber = "+3876542";
            var response = GetUpsertMarketPartyInfoRequest(marketPartyName, marketPartyEans, phoneNumber)
                .CallWith<UpsertMarketPartyInfoRequest, UpsertMarketPartyInfoResponse>(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Id.Should().BePositive();
        }

        [Test]
        public void UpsertMarketParty_EmptyRequest_Returns400()
        {
            var response = new UpsertMarketPartyInfoRequest {MarketPartyInfo = null}.CallWith(MarketPartyClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired,
                nameof(UpsertMarketPartyInfoRequest.MarketPartyInfo));
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMarketPartyById_ValidCase_Returns200()
        {
            var marketParty = MarketPartyDb.GetEntityByCondition<Repositories.MarketPartyModels.MarketPartyInfo>(mp => true);
            var response = new GetMarketPartyInfoByIdRequest {MarketPartyId = marketParty.Id}
                .CallWith<GetMarketPartyInfoByIdRequest, GetMarketPartyInfoByIdResponse>(MarketPartyClient);
            
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            ObjectComparator.ComparePropsOfTypes(response.Data.MarketPartyInfo, marketParty).Should().BeTrue();
        }

        [Test]
        public void GetMarketPartyById_NotExistingMarketParty_Returns400()
        {
            const int notExistingMarketParty = 0;
            var response = new GetMarketPartyInfoByIdRequest {MarketPartyId = notExistingMarketParty}.CallWith(MarketPartyClient);
            
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.MarketPartyNotFound, notExistingMarketParty);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}