﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MasterData.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.Mdu
{
    [TestFixture]
    internal class GetMasterDataUpdateTests : BaseMdu
    {
        [Test]
        public void GetMasterDataUpdateHistory_ValidValues_Returns200()
        {
            var response = GenerateGetMduHistoryRequest(EanId, MutationDate.AddDays(-1), DateTime.Now)
                .CallWith<GetMduHistoryRequest, GetMduHistoryResponse>(MduClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.MasterData.Should().NotBeEmpty();
            CheckEnums(response.Data.MasterData.First(), EanId).Should().BeTrue();
        }

        [Test]
        public void GetMasterDataUpdateHistory_WithNotExistedEanId_Returns404()
        {
            var response = GenerateGetMduHistoryRequest("111425200000111111").CallWith(MduClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoMduFound);
        }

        [Test]
        public void GetMasterDataUpdateHistory_WithWrongFromDatePeriod_Returns404()
        {
            var response = GenerateGetMduHistoryRequest(EanId, MutationDate.AddDays(3))
                .CallWith(MduClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoMduFound);
        }

        [Test]
        public void GetMasterDataUpdateHistory_WithWrongToDatePeriod_Returns404()
        {
            var response = GenerateGetMduHistoryRequest(EanId, toDate: MutationDate.AddDays(-10))
                .CallWith(MduClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoMduFound);
        }

        [Test]
        public void GetMasterDataUpdateHistory_WithToDateLessThanFromDate_Returns400()
        {
            var response = GenerateGetMduHistoryRequest(EanId, MutationDate, MutationDate.AddDays(-3))
                .CallWith(MduClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongToDate);
        }

        [Test]
        [TestCase("ffffffffffffffffff", PatternMessages.InvalidEanIdExpression2, TestName =
            "GetMasterDataUpdateHistory_WithInvalidEanIdExpressionChars_Returns400")]
        [TestCase("1234567891234567899", PatternMessages.InvalidEanIdExpression2, TestName =
            "GetMasterDataUpdateHistory_WithInvalidEanIdExpressionLength_Returns400")]
        public void GetMasterDataUpdateHistory_InvalidEnaIdValues_Returns400(string eanId, string patternErrorMessage)
        {
            var response = GenerateGetMduHistoryRequest(eanId).CallWith(MduClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(patternErrorMessage);
        }
    }
}