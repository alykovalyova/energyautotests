﻿using Autotests.Core.Helpers;
using FluentAssertions;
using NUnit.Framework;
using MeteringPointDb = Autotests.Repositories.MeteringPointModels.MeteringPoint;
using MeteringPointHistoryDb = Autotests.Repositories.MeteringPointModels.MeteringPointHistory;
using MduHistoryDb = Autotests.Repositories.MasterDataModels.MduHistory;
using Autotests.IcarMicroServices.Base;
using Autotests.Helper.Enums;
using Autotests.Helper.DataProviders.FakeEdsn;

namespace Autotests.IcarMicroServices.Tests.Mdu
{
    [TestFixture]
    internal class MasterDataUpdateTestsIntegration : BaseMduIntegration
    {
        [Test]
        public void SendMdu_UpdateStatus_UpdatedMdu()
        {
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);

            Waiter.Wait(() => MpDb.GetEntitiesByCondition<MeteringPointDb>(mp =>
                mp.EanId == EanId)).Count.Should().Be(1);
            Waiter.Wait(() => MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp =>
                mp.EanId == EanId)).Count.Should().Be(1);
            var masterData = Waiter.Wait(() => MasterDataDb.GetLastEntity<MduHistoryDb, DateTime>(mdu => 
                    mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));

            masterData.LastComment.Should().BeEquivalentTo("Mdu updated");
            masterData.Status.Identifier.Should().Be(ProcessStatus.Accepted.ToString());
        }

        [Test]
        public void SendMdu_IntegrationTest_MduXmlHeaderDateIsLessThanExisting()
        {
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);

            var zipCode = "1212QQ";
            var headerCreationDate = Waiter.Wait(() => MpDb.GetLastEntity<MeteringPointDb, DateTime>(mp =>
                mp.CreatedOn, mdu => mdu.EanId == EanId)).HeaderCreationDate;
            MpDb.UpdateFirstEntity<MeteringPointDb>(mp => mp.EanId == EanId,
                mp => mp.HeaderCreationDate = headerCreationDate.AddMinutes(2));

            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate, zipCode:zipCode);
            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<MeteringPointDb>(mp => 
                mp.EanId == EanId, "Address"));
            meteringPoint.Address.CityName.Should().Be(TestConstants.City);
            Waiter.Wait(() => MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(
                mp => mp.EanId == EanId)).Count.Should().Be(2);
        }

        [Test]
        public void SendMdu_IntegrationTest_MduXmlHeaderDateIsEqualToExisting()
        {
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);

            var zipCode = "1111QQ";
            var headerCreationDate = Waiter.Wait(() => MpDb.GetLastEntity<MeteringPointDb, DateTime>(mp =>
                mp.CreatedOn, mdu => mdu.EanId == EanId)).HeaderCreationDate;
            MpDb.UpdateFirstEntity<MeteringPointDb>(mp => mp.EanId == EanId,
                mp => mp.HeaderCreationDate = headerCreationDate);

            MasterDataProvider.SendMduToFakeEdsn(EanId, headerCreationDate, zipCode: zipCode);
            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<MeteringPointDb>(mp =>
                mp.EanId == EanId, "Address"));
            meteringPoint.Address.CityName.Should().Be(TestConstants.City);
            Waiter.Wait(() =>
                MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp => mp.EanId == EanId)).Count.Should().Be(1);
        }

        [Test]
        public void SendMdu_UpdateStatus_DuplicatedMdu()
        {
          MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);
            Waiter.Wait(() => MasterDataDb.GetLastEntity<MduHistoryDb, DateTime>(mdu => 
                    mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status")).
                Status.Identifier.Should().Be(ProcessStatus.Accepted.ToString());

            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);
            Waiter.Wait(() => MasterDataDb.GetEntityByCondition<MduHistoryDb>(mdu => 
                mdu.EanId == EanId && mdu.Status.Identifier == ProcessStatus.Duplicate.ToString(), "Status"));
                

            MpDb.GetEntitiesByCondition<MeteringPointDb>(mp => mp.EanId == EanId).Count.Should().Be(1);
            MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp => mp.EanId == EanId).Count.Should().Be(1);
        }

        [Test]
        public void SendMdu_IntegrationTest_AddressChange()
        {
            var newZipCode = "1212QQ";
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);
            Waiter.Wait(() => MasterDataDb.GetLastEntity<MduHistoryDb, DateTime>(mdu =>
                mdu.CreatedOn, mdu => mdu.EanId == EanId));
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate, zipCode:newZipCode);

            var updatedMdu = Waiter.Wait(() => MasterDataDb.GetLastEntity<MduHistoryDb, DateTime>(mdu =>
                mdu.CreatedOn, mdu => mdu.EanId == EanId, "Status"));
            updatedMdu.Status.Identifier.Should().Be(ProcessStatus.Accepted.ToString());
            updatedMdu.Zipcode.Should().Be(newZipCode);

            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<MeteringPointDb>(mp => mp.EanId == EanId, "Address"));
            meteringPoint.Address.Zipcode.Should().Be(newZipCode);
            MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp => mp.EanId == EanId).Count.Should().Be(2);
        }

        [Test]
        public void SendMdu_UpdateStatus_FutureUpdate()
        {
            var validFrom = MutationDate.AddDays(10);
            MasterDataProvider.SendMduToFakeEdsn(EanId, validFrom);
            Waiter.Wait(() => !MpDb.EntityIsInDb<MeteringPointDb>(mp => 
                mp.EanId == EanId));
            Waiter.Wait(() => !MpDb.EntityIsInDb<MeteringPointHistoryDb>(mp => 
                mp.EanId == EanId));
            var futureUpdateMdu = Waiter.Wait(() => MasterDataDb
                .GetEntityByCondition<Repositories.MasterDataModels.MduHistory>(mdu => mdu.EanId == EanId, "Status"));

            futureUpdateMdu.Status.Identifier.Should().Be(ProcessStatus.FutureProcessing.ToString());
            futureUpdateMdu.LastComment.Should().BeEquivalentTo("Skipped as new");
        }

        [Test]
        public void SendMdu_UpdateStatus_SkippedMdu()
        {
            var newZipCode = "1212QQ";
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);
            Waiter.Wait(() => MasterDataDb.GetLastEntity<Repositories.MasterDataModels.MduHistory, DateTime>(mdu => mdu.CreatedOn,
                    mdu => mdu.EanId == EanId, "Status")).Status.Identifier.Should().Be(ProcessStatus.Accepted.ToString());

            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate.AddDays(-3), zipCode:newZipCode);
            Waiter.Wait(() => MasterDataDb.GetLastEntity<Repositories.MasterDataModels.MduHistory, DateTime>(mdu => mdu.CreatedOn,
                    mdu => mdu.EanId == EanId, "Status")).Status.Identifier.Should().Be(ProcessStatus.Accepted.ToString());

            var meteringPoint = Waiter.Wait(() =>
                MpDb.GetEntityByCondition<MeteringPointDb>(mp => mp.EanId == EanId, "Address"));
            meteringPoint.Address.Zipcode.Should().Be(TestConstants.Zipcode);
            Waiter.Wait(() =>
                MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp => mp.EanId == EanId)).Count.Should().Be(2);
        }
        
        [Test]
        public void SendMdu_IntegrationTest_ChangeBalanceSupplier()
        {
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);
            var initialMeteringPoint = Waiter.Wait(() =>
                MasterDataDb.GetEntityByCondition<MduHistoryDb>(m => m.EanId == EanId, "BalanceSupplier"));
            initialMeteringPoint.BalanceSupplier.Ean.Should().Be(TestConstants.BudgetSupplier);

            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate, TestConstants.OtherPartyBalanceSupplier);
            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<MeteringPointDb>(mp =>
                mp.EanId == EanId, "BalanceSupplier"));

            meteringPoint.BalanceSupplier.Ean.Should().Be(TestConstants.OtherPartyBalanceSupplier);
            Waiter.Wait(() => MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp
                => mp.EanId == EanId)).Count.Should().Be(2);
        }

        [Test]
        public void SendMdu_IntegrationTest_ValidFromDateInFuture()
        {
            const string city = "Kiev";
            var mutationDate = DateTime.Now.AddDays(20);
            MasterDataProvider.SendMduToFakeEdsn(EanId, MutationDate);
            Waiter.Wait(() => MasterDataDb.EntityIsInDb<MduHistoryDb>(m => m.EanId == EanId));

            MasterDataProvider.SendMduToFakeEdsn(EanId, mutationDate, city: city);
            var updatedMdu = Waiter.Wait(() => MasterDataDb.GetEntityByCondition<MduHistoryDb>(m => m.EanId == EanId));
            updatedMdu.CityName.Should().NotBeEquivalentTo(city);

            var meteringPoint = Waiter.Wait(() => MpDb.GetEntityByCondition<MeteringPointDb>(mp => 
                mp.EanId == EanId, "Address"));
            meteringPoint.Address.CityName.Should().Be(TestConstants.City);
            
            Waiter.Wait(() => MpDb.GetEntitiesByCondition<MeteringPointHistoryDb>(mp => mp.EanId == EanId)).Count.Should().Be(1);
        }
    }
}