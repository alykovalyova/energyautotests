﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture]
    [NonParallelizable]
    internal class ChangeAddressTests : BaseMP
    {
        [Test]
        [TestCaseSource(typeof(DataProvider), nameof(DataProvider.GetValidAddressesForTest))]
        public void ChangeAddress_ValidValues_Returns200(KeyValuePair<string, Address> testData)
        {
            CreateRequest<ChangeAddressRequest>(testData.Value, ean: MeteringPoint.EanId)
                .CallWith<ChangeAddressRequest, ChangeAddressResponse>(MpClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var response = CreateRequest<SearchAddressByEanRequest>(ean: EanId)
                .CallWith<SearchAddressByEanRequest, SearchAddressByEanResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            ObjectComparator.ComparePropsOfTypes(response.Data.Address, testData.Value).Should().BeTrue();
        }

        [Test]
        public void ChangeAddress_InvalidZipcodeExpression_Returns400(
            [Values("1111QQQ", "111111", "QQQQQQ", "111QQ", "1111@#")] string zipcode)
        {
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(zipcode), ean: MeteringPoint.EanId)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ZipCodeRegexWrong);
        }

        [Test]
        public void ChangeAddress_InvalidBuildingNrExpression_Returns400([Values(0, 100000)] int buildNr)
        {
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(buildingNr: buildNr),
                ean: MeteringPoint.EanId).CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.BuildingNrRegexWrong);
        }

        [Test]
        public void ChangeAddress_InvalidExBuildingNrExpression_Returns400()
        {
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(exBuildingNr: "CCCCCCC"),
                ean: MeteringPoint.EanId).CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ExBuildingNrRegexWrong);
        }
    }
}