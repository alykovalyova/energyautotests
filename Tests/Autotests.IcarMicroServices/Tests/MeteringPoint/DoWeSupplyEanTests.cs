﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture]
    internal class DoWeSupplyEanTests : BaseMP
    {
        [Test]
        public void DoWeSupplyEan_WithValidEan_Returns200()
        {
            MduProvider.SendMduToFakeEdsn(EanIdPhaseTest, CurrentDate);
            var response = CreateRequest<DoWeSupplyEanRequest>(ean: EanIdPhaseTest)
                .CallWith<DoWeSupplyEanRequest, DoWeSupplyEanResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.IsBudgetEnergyEan.Should().BeTrue();
        }

        [Test]
        public void DoWeSupplyEan_WithNotExistedEan_Returns200()
        {
            var response = CreateRequest<DoWeSupplyEanRequest>(ean: EanIdPhaseTest.Replace("0", "7"))
                .CallWith<DoWeSupplyEanRequest, DoWeSupplyEanResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.IsBudgetEnergyEan.Should().BeFalse();
            response.Data.BalanceSupplier.Should().BeNull();
        }

        [Test]
        [TestCase("1112561231546750011", TestName = "DoWeSupplyEan_WithInvalidEanLengthMore_Returns400")]
        [TestCase("11125612315467500", TestName = "DoWeSupplyEan_WithInvalidEanLengthLess_Returns400")]
        [TestCase("ffffffffffffffffff", TestName = "DoWeSupplyEan_WithInvalidEanExpression_Returns400")]
        [TestCase("!@#$%^&*()_)+`~;:/?", TestName = "DoWeSupplyEan_WithEanSpecialSymbols_Returns400")]
        public void DoWeSupplyEan_WithInvalidEanExpression_Returns400(string ean)
        {
            if (ean == "!@#$%^&*()_)+`~;:/?") throw new IgnoreException("Bug: UT-4592");

            var response = CreateRequest<DoWeSupplyEanRequest>(ean: ean).CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EanFieldRegexWrong);
        }
    }
}