﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture]
    internal class GetMeteringPointByDateTests : BaseMP
    {
        [Test]
        public void GetMeteringPointsByDate_ValidValues_Returns200()
        {
            var response = CreateRequest<GetMeteringPointsByDateRequest>(maxDate: CurrentDate, eans: new List<string> { EanIdPhaseTest })
                 .CallWith<GetMeteringPointsByDateRequest, GetMeteringPointsByDateResponse>(MpClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
        }

        [Test]
        public void GetMeteringPointsByDate_WithNotExistedEan_Returns404()
        {
            var response = CreateRequest<GetMeteringPointsByDateRequest>(eans: new List<string> { "123456789123456789" })
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoMpFound);
        }

        [Test]
        [TestCase("2019-05-30T00:00:00+03:00", TestName = "GetMeteringPointsByDate_WithMaxDateLess_Returns404")]
        [TestCase("", TestName = "GetMeteringPointsByDate_WithoutMaxDate_Returns404")]
        public void GetMeteringPointsByDate_WithInvalidMaxDate_Returns404(string date)
        {
            var response = string.IsNullOrEmpty(date)
                ? CreateRequest<GetMeteringPointsByDateRequest>(eans: new List<string> { EanId })
                    .CallWith(MpClient)
                : CreateRequest<GetMeteringPointsByDateRequest>(eans: new List<string> { EanId }, maxDate: Convert.ToDateTime(date))
                    .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoMpFound);
        }

        [Test]
        public void GetMeteringPointsByDate_WithEmptyEansList_Returns400()
        {
            var response = CreateRequest<GetMeteringPointsByDateRequest>(eans: new List<string>())
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EansListIsEmpty);
        }

        [Test]
        [NonParallelizable]
        [TestCase("87168711000025151", TestName = "GetMeteringPointsByDate_WithInvalidEanLengthMore_Returns400")]
        [TestCase("871687110000251", TestName = "GetMeteringPointsByDate_WithInvalidEanLengthLess_Returns400")]
        [TestCase("ffffffffffffffffff", TestName = "GetMeteringPointsByDate_WithInvalidEanSymbols_Returns400")]
        [TestCase("", TestName = "GetMeteringPointsByDate_WithEmptyEan_Returns400")]
        public void GetMeteringPointsByDate_WithInvalidEanExpression_Returns400(string ean)
        {
            var response = CreateRequest<GetMeteringPointsByDateRequest>(eans: new List<string> { ean }, maxDate: MeteringPoint.ValidFrom)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.EanRegularExpressionError, ean) + PatternMessages.EanIdRegex;
            response.Header.ErrorDetails.Should().Contain(message);
        }
    }
}