﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture]
    internal class GetMeteringPointByEanTests : BaseMP
    {
        [Test]
        public void GetMeteringPointByEan_WithValidEan_Returns200()
        {
            MduProvider.SendMduToFakeEdsn(EanIdPhaseTest, CurrentDate);
            var response = CreateRequest<GetMeteringPointRequest>(ean: EanIdPhaseTest, maxDate: CurrentDate)
                .CallWith<GetMeteringPointRequest, GetMeteringPointResponse>(MpClient);

            var meteringPointFromDb = MpDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp =>
                    mp.EanId == EanIdFake);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test]
        public void GetMeteringPointByEan_WithNotExistedEan_Returns404()
        {
            var eanId = EanId.Replace("2", "7");
            var response = CreateRequest<GetMeteringPointRequest>(ean: eanId)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.MeteringPointNotFound, eanId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        [TestCase("87168711000025151", TestName = "GetMeteringPointByEan_WithInvalidEanLengthMore_Returns400")]
        [TestCase("871687110000251", TestName = "GetMeteringPointByEan_WithInvalidEanLengthLess_Returns400")]
        [TestCase("ffffffffffffffffff", TestName = "GetMeteringPointByEan_WithInvalidEanSymbols_Returns400")]
        public void GetMeteringPointByEan_WithInvalidEanExpression_Returns400(string ean)
        {
            var response = CreateRequest<GetMeteringPointRequest>(ean: ean).CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EanFieldRegexWrong);
        }
    }
}