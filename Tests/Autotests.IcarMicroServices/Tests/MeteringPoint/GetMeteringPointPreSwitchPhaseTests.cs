﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Repositories.MeteringPointModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using MeteringPointPreSwitchPhaseRequest = Nuts.MeteringPoint.Contract.GetMeteringPointsPreSwitchPhaseRequest;
using MeteringPointPreSwitchPhaseResponse = Nuts.MeteringPoint.Contract.GetMeteringPointsPreSwitchPhaseResponse;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Autotests.IcarMicroServices.Base;
using Autotests.Helper;
using Rejection = Autotests.IcarMicroServices.Models.Rejection;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture, SingleThreaded]
    internal class GetMeteringPointsPreSwitchPhaseTests : BaseMP
    {
        [Test]
        public void GetMeteringPoint_PreSwitchPhase_DefaultValidTest_Returns200()
        {
            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, hasMeter: false);
            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().Contain(mp => mp.EanId == EanIdPhaseTest);
        }

        [Test]
        [TestCase("00042520000000777", SupplierEan, ExternalRef, PatternMessages.InvalidEanField, TestName = "GetMeteringPoint_PreSwitchPhase_EanIdShorter18_Returns400")]
        [TestCase("0004252000000077777", SupplierEan, ExternalRef, PatternMessages.InvalidEanField, TestName = "GetMeteringPoint_PreSwitchPhase_EanIdLonger18_Returns400")]
        [TestCase("ffffffffffffffffff", SupplierEan, ExternalRef, PatternMessages.InvalidEanField, TestName = "GetMeteringPoint_PreSwitchPhase_EanIdWithChars_Returns400")]
        [TestCase(EanIdPhaseTest, "871425201814", ExternalRef, PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPoint_PreSwitchPhase_SupplierIdShorter13_Returns400")]
        [TestCase(EanIdPhaseTest, "87142520181411", ExternalRef, PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPoint_PreSwitchPhase_SupplierIdLonger13_Returns400")]
        [TestCase(EanIdPhaseTest, "fffffffffffff", ExternalRef, PatternMessages.SupplierFieldRegexIsWrong, TestName = "GetMeteringPoint_PreSwitchPhase_SupplierIdWithChars_Returns400")]
        public void GetMeteringPoint_PreSwitchPhase_InvalidFields_Returns400(string eanId, string supplierEan, string externalRef, string message)
        {
            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var response = GetMeteringPointPreSwitchPhaseRequest(eanId, externalRef, supplierEan)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_WithEmptyEanField_Returns400()
        {
            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var response = GetMeteringPointPreSwitchPhaseRequest(string.Empty, ExternalRef, SupplierEan)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "EanId");
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_WithEmptySupplierField_Returns400()
        {
            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, string.Empty)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "SupplierEan");
            response.Header.ErrorDetails.Should().Contain(message);
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_WithNotExistedEan_Returns412()
        {
            var response = GetMeteringPointPreSwitchPhaseRequest(NotExistedEanId, ExternalRef, SupplierEan)
                .CallWith(MpClient);
            response.Header.InternalErrorCode.Should().Be((int)HttpStatusCode.PreconditionFailed);
            var rejection = response.Header.Message.GetJTokenAs<Rejection>("rejection");

            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));
            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.Equals("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_EanIsNotRecognized_Returns412()
        {
            var response = GetMeteringPointPreSwitchPhaseRequest(NotRecognizedEan, ExternalRef, SupplierEan)
                .CallWith(MpClient);
            response.Header.InternalErrorCode.Should().Be((int)HttpStatusCode.PreconditionFailed);
            var rejection = response.Header.Message.GetJTokenAs<Rejection>("rejection");

            var innerRejectionMessage = PatternMessages.Edsn201Rejection.Substring(
                PatternMessages.Edsn201Rejection.LastIndexOf("EAN-code", StringComparison.Ordinal));

            rejection.RejectionMessage.Should().BeEquivalentTo(PatternMessages.Edsn201Rejection);
            rejection.Rejections.Should().Contain(r => r.Code.Equals("Item201"));
            rejection.Rejections.Should().Contain(r => r.Message.Equals(innerRejectionMessage));
        }

        [Test, Order(1)]
        public void GetMeteringPoint_PreSwitchPhase_CachedEanInfoIsReturned_Returns200()
        {
            SaveValidEanToPreSwitchPhase(EanIdCacheTest, CurrentDate);
            const string newProductType = "ELK";
            const string oldProductType = "GAS";
            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdCacheTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);

            Waiter.Wait(() => MpDb.GetEntitiesByCondition<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(mp =>
                mp.EanId == EanIdCacheTest), 5);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().Contain(mp => mp.ProductType.Equals(oldProductType));

            SaveValidEanToPreSwitchPhase(EanIdCacheTest, CurrentDate, newProductType);
            var responseWhenEanIsCached = GetMeteringPointPreSwitchPhaseRequest(EanIdCacheTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);

            responseWhenEanIsCached.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            responseWhenEanIsCached.Data.MeteringPoints.Should().HaveCount(1);
            responseWhenEanIsCached.Data.MeteringPoints.Should().Contain(mp => mp.ProductType.Equals(oldProductType));
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_CachedEanIsNotReturnedIfValidDateIsYesterday_Returns200()
        {
            SaveValidEanToPreSwitchPhase(EanIdCacheTest, CurrentDate);

            var newValidDate = DateTime.Today.AddDays(-1);
            var newProductType = "ELK";
            GetMeteringPointPreSwitchPhaseRequest(EanIdCacheTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(mp =>
                mp.EanId == EanIdCacheTest));
            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(mp => mp.EanId == EanIdCacheTest,
                mp => mp.ValidDate = newValidDate);
            SaveValidEanToPreSwitchPhase(EanIdCacheTest, CurrentDate, newProductType);

            var responseWhenEanCacheIsOutDate = GetMeteringPointPreSwitchPhaseRequest(EanIdCacheTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            responseWhenEanCacheIsOutDate.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            responseWhenEanCacheIsOutDate.Data.MeteringPoints.Should()
                .Contain(mp => mp.ProductType.Equals(newProductType));
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_WithoutEnergyMeter_Returns200()
        {
            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, hasMeter: false);

            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.MeteringPoints.Should().Contain(mp => mp.EnergyMeter == null);
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_CachedEanWithoutEnergyMeter_Returns200()
        {
            var validDate = DateTime.Today.AddDays(-1);

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, hasMeter: false);

            var getMpPreSwitch = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);

            getMpPreSwitch.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            Waiter.Wait(() => MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate));

            var getMpPreSwitchFromEdsn = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchFromEdsn.Data.MeteringPoints.Should().Contain(mp => mp.EnergyMeter == null);
        }

        [Test]
        public void GetMeteringPoint_PreSwitchPhase_ChangeAddressByAgent()
        {
            var newBuildingNr = 125;

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(buildingNr: newBuildingNr),
                ean: EanIdPhaseTest).CallWith(MpClient);
            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);

            var address = Waiter.Wait(() =>
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest));
            address.BuildingNr.Should().Be(newBuildingNr);
            address.IsChangedByAgent.Should().BeTrue();
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_ChangeAddressByAgent_NewMduIsSent()
        {
            CleanMpDb();

            var newBuildingNr = 125;
            var mduBuildingNr = "80";
            var validDate = DateTime.Today.AddDays(-1);

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var getMpPreSwitchPhaseResponse = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchPhaseResponse.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(buildingNr: newBuildingNr),
                ean: EanIdPhaseTest).CallWith(MpClient);
            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);

            var address =
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest);
            address.BuildingNr.Should().Be(newBuildingNr);
            address.IsChangedByAgent.Should().BeTrue();

            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate);
            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == validDate));

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, buildingNr: mduBuildingNr);
            GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            var updatedAddress = Waiter.Wait(() =>
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest));
            updatedAddress.BuildingNr.Should().Be(Int32.Parse(mduBuildingNr));
            updatedAddress.IsChangedByAgent.Should().BeFalse();
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_ChangeAddressByAgent_PreviousMduIsSent()
        {
            CleanMpDb();

            var newBuildingNr = 125;
            var validDate = DateTime.Today.AddDays(-1);

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var getMpPreSwitchPhaseResponse = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchPhaseResponse.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(buildingNr: newBuildingNr),
                ean: EanIdPhaseTest).CallWith(MpClient);
            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);

            var address =
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest);
            address.BuildingNr.Should().Be(newBuildingNr);
            address.IsChangedByAgent.Should().BeTrue();

            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate);
            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == validDate));

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            var updatedAddress = Waiter.Wait(() =>
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest));
            var addressHistoryCount = Waiter.Wait(() => MpDb.GetEntitiesByCondition<AddressHistory>(ah =>
                ah.EanId == EanIdPhaseTest)).Count;
            addressHistoryCount.Should().Be(2);
            updatedAddress.BuildingNr.Should().Be(newBuildingNr);
            updatedAddress.IsChangedByAgent.Should().BeTrue();
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_ChangeAddressByAgent_DublicateMduIsSent()
        {
            var newBuildingNr = 125;
            var validDate = DateTime.Today.AddDays(-1);

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var getMpPreSwitchPhaseResponse = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchPhaseResponse.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var response = CreateRequest<ChangeAddressRequest>(DataProvider.GetNewAddress(buildingNr: newBuildingNr),
                ean: EanIdPhaseTest).CallWith(MpClient);
            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);

            var address =
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest);
            address.BuildingNr.Should().Be(newBuildingNr);
            address.IsChangedByAgent.Should().BeTrue();

            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate);
            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == validDate));

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, DateTime.Today, buildingNr: newBuildingNr.ToString());
            GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            var updatedAddress = Waiter.Wait(() =>
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.Address>(mp => mp.EanId == EanIdPhaseTest));
            updatedAddress.BuildingNr.Should().Be(newBuildingNr);
            updatedAddress.IsChangedByAgent.Should().BeFalse();
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_CachedEnergyMeter()
        {
            var validDate = DateTime.Today.AddDays(-1);

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, hasMeter: false);
            var getMpPreSwitchPhaseResponse = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchPhaseResponse.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            getMpPreSwitchPhaseResponse.Data.MeteringPoints.FirstOrDefault()?.EnergyMeter.Should().BeNull();

            var energyMeter =
                Waiter.Wait(() => MpDb.GetEntityByCondition<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(mp => mp.EanId == EanIdPhaseTest, including: nameof(PreSwitchRegister)));
            energyMeter.PreSwitchRegister.Should().BeEmpty();

            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate);
            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == validDate));

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, hasMeter: true);
            GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            var updatedMeter = Waiter.Wait(() =>
                MpDb.GetEntityByCondition<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(mp => mp.EanId == EanIdPhaseTest, including: nameof(PreSwitchRegister)));
            updatedMeter.MeterEdsnId.Should().BeEquivalentTo(PcDefaultValues.EnergyMeter.ID);
            updatedMeter.NrOfRegisters.Should().Be(short.Parse(PcDefaultValues.EnergyMeter.NrOfRegisters));
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_SupplyPhaseOutDated()
        {
            var validDate = DateTime.Today.AddDays(-1);
            var zipCode = "1111WW";

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);
            var getMpPreSwitchPhaseResponse = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchPhaseResponse.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            getMpPreSwitchPhaseResponse.Data.MeteringPoints.FirstOrDefault().Should().NotBeNull();

            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == CurrentDate));
            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate);
            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == validDate));

            MduProvider.SendMduToFakeEdsn(EanIdPhaseTest, CurrentDate, zipCode: zipCode);
            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            response.Data.MeteringPoints.First().Address.ZipCode.Should().Be(zipCode);
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_PreswitchPhaseExistsWithoutMeter()
        {
            CleanMpDb();
            var validDate = DateTime.Today.AddDays(-1);

            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate, hasMeter: false);
            var getMpPreSwitchPhaseResponse = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            getMpPreSwitchPhaseResponse.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            getMpPreSwitchPhaseResponse.Data.MeteringPoints.FirstOrDefault().Should().NotBeNull();

            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
               pre.EanId == EanIdPhaseTest & pre.ValidDate == CurrentDate));
            MpDb.UpdateFirstEntity<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest, pre => pre.ValidDate = validDate);
            Waiter.Wait(() => MpDb.EntityIsInDb<Repositories.MeteringPointModels.PreSwitchMeteringPoint>(pre =>
                pre.EanId == EanIdPhaseTest & pre.ValidDate == validDate));

            MduProvider.SendMduToFakeEdsn(EanIdPhaseTest, CurrentDate);
            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            response.Data.MeteringPoints.First().EnergyMeter.Should().NotBeNull();
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_SupplyPhaseDataIsReturn()
        {
            CleanMpDb();
            var city = "Kyiv";
            SaveValidEanToPreSwitchPhase(EanIdPhaseTest, CurrentDate);

            MduProvider.SendMduToFakeEdsn(EanIdPhaseTest, CurrentDate, city: city);
            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            response.Data.MeteringPoints.First().Address.CityName.Should().Be(city);
        }

        [Test]
        public void GetMeteringPointPreSwitchPhase_NoPhasesExist()
        {
            CleanMpDb();

            var response = GetMeteringPointPreSwitchPhaseRequest(EanIdPhaseTest, ExternalRef, SupplierEan)
                .CallWith<MeteringPointPreSwitchPhaseRequest, MeteringPointPreSwitchPhaseResponse>(MpClient);
            response.Header.Message.Contains(PatternMessages.EanNotFound);
        }
    }
}