﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture]
    internal class SearchAddressByEanTests : BaseMP
    {
        [Test]
        public void SearchAddressByEan_WithValidEan_Returns200()
        {
            var response = CreateRequest<SearchAddressByEanRequest>(ean: EanId)
                .CallWith<SearchAddressByEanRequest, SearchAddressByEanResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            ObjectComparator.ComparePropsOfTypes(MeteringPoint.Address, response.Data.Address, false).Should().BeTrue();
        }

        [Test]
        public void SearchAddressByEan_WithNotExistedEan_Returns404()
        {
            var response = CreateRequest<SearchAddressByEanRequest>(ean: "111256123154675222")
                .CallWith<SearchAddressByEanRequest, SearchAddressByEanResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Address.Should().BeNull();
        }

        [Test]
        [TestCase("87168711000025151", TestName = "SearchAddressByEan_WithInvalidEanLengthMore_Returns400")]
        [TestCase("871687110000251", TestName = "SearchAddressByEan_WithInvalidEanLengthLess_Returns400")]
        [TestCase("ffffffffffffffffff", TestName = "SearchAddressByEan_WithInvalidEanSymbols_Returns400")]
        public void SearchAddressByEan_WithInvalidEanExpression_Returns400(string ean)
        {
            var response = CreateRequest<SearchAddressByEanRequest>(ean: ean).CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EanFieldRegexWrong);
        }
    }
}