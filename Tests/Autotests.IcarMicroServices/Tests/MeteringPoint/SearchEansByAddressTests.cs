﻿using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.IcarMicroServices.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Contract;
using System.Net;

namespace Autotests.IcarMicroServices.Tests.MeteringPoint
{
    [TestFixture]
    internal class SearchEansByAddressTests : BaseMP
    {
        [Test]
        public void SearchEansByAddress_ValidValues_Returns200()
        {
            var response = CreateRequest<SearchEansByAddressRequest>(zipcode: "1111QQ", buildingNr: 71, exBuildingNr: "B")
                .CallWith<SearchEansByAddressRequest, SearchEansByAddressResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Eans.Should().Contain(EanId);
        }

        [Test]
        [TestCase("2222QQ", 71, "B", TestName = "SearchEansByAddress_WithNotExistedZipcode_Returns200AndEmptyArray")]
        [TestCase("1111QQ", 76, "B", TestName = "SearchEansByAddress_WithNotExistedBuildingNr_Returns200AndEmptyArray")]
        [TestCase("1111QQ", 71, "CCC", TestName = "SearchEansByAddress_WithNotExistedExBuildingNr_Returns200AndEmptyArray")]
        public void SearchEansByAddress_InvalidValues_Returns200AndEmptyArray(string zipcode, int buildingNr, string exBuildingNr)
        {
            var response = CreateRequest<SearchEansByAddressRequest>(zipcode: zipcode, buildingNr: buildingNr,
                exBuildingNr: exBuildingNr)
                .CallWith<SearchEansByAddressRequest, SearchEansByAddressResponse>(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.Eans.Should().BeEmpty();
        }

        [Test]
        public void SearchEansByAddress_InvalidZipcodeExpression_Returns400(
            [Values("1111QQQ", "111111", "QQQQQQ", "111QQ", "1111@#")] string zipcode)
        {
            var response = CreateRequest<SearchEansByAddressRequest>(zipcode: zipcode).CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ZipCodeRegexWrong);
        }

        [Test]
        public void SearchEansByAddress_InvalidBuildingNrExpression_Returns400([Values(0, 100000)] int buildingNr)
        {
            var response = CreateRequest<SearchEansByAddressRequest>(zipcode: "1111QQ", buildingNr: buildingNr)
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.BuildingNrRegexWrong);
        }

        [Test]
        public void SearchEansByAddress_InvalidExBuildingNrExpression_Returns400()
        {
            var response = CreateRequest<SearchEansByAddressRequest>(zipcode: "1111QQ", buildingNr: 71, exBuildingNr: "CCCCCCC")
                .CallWith(MpClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ExBuildingNrRegexWrong);
        }
    }
}