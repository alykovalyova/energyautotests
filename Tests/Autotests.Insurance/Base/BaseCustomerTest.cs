﻿using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.Sql.Entities;
using Newtonsoft.Json;
using CreateCustomerRequest = Nuts.InsuranceApi.Contract.Rest.Transport.CreateCustomerRequest;
using UpdateProductRequest = Nuts.InsuranceApi.Contract.Rest.Transport.UpdateProductRequest;

namespace Autotests.Insurance.Base
{
    internal class BaseCustomerTest : BaseInsuranceTest
    {
        private static readonly string _createCustomerFilePath =
            ConfigHelper.GetFilePath("Insurance/CreateCustomerRequest.json");

        private static readonly string _updateCustomerFilePath =
            ConfigHelper.GetFilePath("Insurance/UpdateCustomerRequest.json");

        protected CreateCustomerRequest CreateCustomerRequestObj => JsonConvert
            .DeserializeObject<CreateCustomerRequest>(File.ReadAllText(_createCustomerFilePath));

        protected UpdateProductRequest UpdateCustomerRequestObj => JsonConvert
            .DeserializeObject<UpdateProductRequest>(File.ReadAllText(_updateCustomerFilePath));

        protected CreateCustomerRequest InitializeCreateCustomerRequest(int relationId, string policyNumber,
            BankCode code = BankCode.INGB)
        {
            var createCustomerRequest = CreateCustomerRequestObj;
            createCustomerRequest.RelationId = relationId;
            createCustomerRequest.Package.Products.ForEach(p => p.PolicyNumber = policyNumber);
            createCustomerRequest.PaymentDetails.Iban = IbanProvider.GetIbanNumber(code);
            return createCustomerRequest;
        }

        protected UpdateProductRequest InitializeUpdateCustomerRequest(ConvertedCustomer customer,
            string productType = "Transport",
            string productSpecification = "specification")
        {
            var updateCustomerRequest = UpdateCustomerRequestObj;
            updateCustomerRequest.RelationId = customer.RelationId;
            updateCustomerRequest.Package.Discount = customer.Discount;
            updateCustomerRequest.Package.GrossPrice = customer.GrossPrice;
            updateCustomerRequest.Package.NetPrice = customer.NetPrice;
            updateCustomerRequest.Package.StartDate = customer.StartDate;
            updateCustomerRequest.Package.EndDate = customer.EndDate;
            updateCustomerRequest.Package.Products.First().PolicyNumber = customer.PolicyNumber;
            updateCustomerRequest.Package.Products.First().StartDate = customer.StartDate;
            updateCustomerRequest.Package.Products.First().EndDate = customer.EndDate;
            updateCustomerRequest.Package.Products.First().IsActive = customer.IsActive;
            updateCustomerRequest.Package.Products.First().Specification = productSpecification;
            updateCustomerRequest.Package.Products.First().SpecificationKey = 1;
            updateCustomerRequest.Package.Products.First().Type = productType;
            updateCustomerRequest.Package.Products.First().Price = customer.Price;
            return updateCustomerRequest;
        }
    }
}