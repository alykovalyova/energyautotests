﻿using System.ComponentModel;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Repositories.InsuranceModels;
using NUnit.Framework;

namespace Autotests.Insurance.Base
{
    [SetUpFixture, NUnit.Framework.Category(Categories.Insurance)]
    internal abstract class BaseInsuranceTest : AllureReport
    {
        //client declaration
        protected NutsHttpClient InsuranceClient;
        protected RestSharpClient RestClient;

        //databases declaration
        protected DbHandler<InsuranceContext> InsuranceDb;

        //variables declaration
        protected ConfigHandler _configHandler = new ConfigHandler();
        protected List<int> RelationIds { get; set; }
        protected int RelationId { get; set; }
        protected RestSharpClient MobileApp { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            RelationIds = new List<int>();
            InsuranceClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.Insurance));
            RestClient = new RestSharpClient(new Uri(_configHandler.ApiUrls.Insurance));
            MobileApp = new RestSharpClient(new Uri(_configHandler.ApiUrls.MobileAppApi));
            InsuranceDb = new DbHandler<InsuranceContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.InsuranceDatabase.ToString()));
        }

        [SetUp]
        public void SetUp()
        {
            RelationId = int.Parse(RandomDataProvider.GetRandomNumbersString(8));
            RelationIds.Add(RelationId);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            foreach (var relationId in RelationIds)
            {
                var products =
                    InsuranceDb.GetEntitiesByCondition<CustomerProduct>(cp => cp.Customer.RelationId == relationId);
                foreach (var product in products)
                    InsuranceDb.DeleteEntitiesByCondition<CustomerProduct>(cp => cp.Id == product.Id);

                InsuranceDb.DeleteEntitiesByCondition<Customer>(c => c.RelationId == relationId);
            }
        }
    }
}