﻿using Autotests.Core;
using Newtonsoft.Json;
using NUnit.Framework;
using Offer = Autotests.Repositories.InsuranceModels.Offer;
using OfferProduct = Autotests.Repositories.InsuranceModels.OfferProduct;
using CreateOfferRequest = Nuts.InsuranceApi.Contract.Rest.Transport.CreateOfferRequest;

namespace Autotests.Insurance.Base
{
    internal class BaseOfferTest : BaseInsuranceTest
    {
        private static readonly string _createOfferFilePath =
            ConfigHelper.GetFilePath("Insurance/CreateOfferRequest.json");

        protected CreateOfferRequest CreateOfferRequestObj => JsonConvert
            .DeserializeObject<CreateOfferRequest>(File.ReadAllText(_createOfferFilePath));

        protected CreateOfferRequest InitializeCreateOfferRequest(string type = "Transport",
            string campaign = "Standard")
        {
            var createOfferRequest = CreateOfferRequestObj;
            createOfferRequest.Offers.First().IncludeInCampaign = campaign;
            createOfferRequest.Offers.First().Package.Products.First().Type = type;
            return createOfferRequest;
        }

        [OneTimeSetUp]
        public void RemoveOffersFromDb()
        {
            var offers = InsuranceDb.GetEntitiesByCondition<Offer>(o => o.Comment.Equals("AutoTests"));
            foreach (var offer in offers)
            {
                InsuranceDb.DeleteEntitiesByCondition<OfferProduct>(op => op.OfferId == offer.Id);
                InsuranceDb.DeleteEntitiesByCondition<Offer>(o => o.Id == offer.Id);
            }
        }
    }
}