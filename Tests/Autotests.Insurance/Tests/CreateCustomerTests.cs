﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Insurance.Base;
using FluentAssertions;
using NUnit.Framework;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Insurance.Tests
{
    [TestFixture]
    internal class CreateCustomerTests : BaseCustomerTest
    {
        [Test]
        public void CreateCustomer_ValidCase_Returns204()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            
            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.ResponseText[0].Should().NotBeNull();
            InsuranceDb.EntityIsInDb<Repositories.InsuranceModels.Customer>(c => c.RelationId == RelationId);
        }

        [Test]
        public void CreateCustomer_WithoutProducts_Returns204()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Package.Products =  new List<Nuts.InsuranceApi.Contract.Rest.Model.ProductDetails>();
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.ResponseText[0].Should().NotBeNull();
            InsuranceDb.EntityIsInDb<Repositories.InsuranceModels.Customer>(c => c.RelationId == RelationId);
        }

        [Test]
        public void CreateCustomer_InvalidRelationId_Returns400()
        {
            int relationId = 0;
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);

            var request = InitializeCreateCustomerRequest(relationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            var m = string.Format(PatternMessages.WrongFieldRange3, nameof(request.RelationId));
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(m);
        }

        [Test]
        public void CreateCustomer_InvalidBirthDate_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var birthdate = DateTime.Now.AddYears(-5);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Person.DateOfBirth = birthdate;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(PatternMessages.InvalidAge);
        }

        [Test]
        public void CreateCustomer_PackageStartDateInvalid_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var startDate = DateTime.Now.AddYears(1);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Package.StartDate = startDate;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(PatternMessages.InvalidStartDate);
        }

        [Test]
        public void CreateCustomer_ProductStartDateInvalid_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var startDate = DateTime.Now.AddYears(1);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Package.Products.First().StartDate = startDate;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(PatternMessages.InvalidStartDate);
        }

        [Test]
        public void CreateCustomer_InvalidIban_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var iban = "blabla";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.PaymentDetails.Iban = iban;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(PatternMessages.InvalidIban2);
        }

        [Test]
        public void CreateCustomer_InvalidProductType_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var product = "notExistingProduct";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Package.Products.First().Type = product;
            var response = request.CallWith(InsuranceClient);

            var m = string.Format(PatternMessages.InvalidProduct, product);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(m);
        }

        [Test]
        public void CreateCustomer_InvalidPostalCode_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var postalCode = "AAAA";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Address.ZipCode = postalCode;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.First().Should().Be(PatternMessages.ODInvalidZipCode);
        }

        [Test]
        public void CreateCustomer_InvalidHomeNumber_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var homeNumber = "AAAA";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Contact.ContactPhoneNumbers.Home = homeNumber;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.InvalidHomeNumber, homeNumber);
            response.Header.ErrorDetails.First().Should().Be(m);
        }

        [Test]
        public void CreateCustomer_InvalidMobileNumber_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var mobileNumber = "0000";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            request.Contact.ContactPhoneNumbers.Mobile = mobileNumber;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.InvalidDutchNumber, mobileNumber);
            response.Header.ErrorDetails.First().Should().Be(m);
        }
    }
}
