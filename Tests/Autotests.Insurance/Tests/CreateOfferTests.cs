﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Insurance.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ApiClient;

namespace Autotests.Insurance.Tests
{
    [TestFixture]
    internal class CreateOfferTests : BaseOfferTest
    {
        [Test]
        public void CreateOffer_ValidTest_Returns204()
        {
            var campaign = "Custom";

            var request = InitializeCreateOfferRequest(campaign:campaign);
            ApiResponse response;
            try
            {
                response = request.CallWith(InsuranceClient);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.ResponseText[0].Should().NotBeNull();
        }

        [Test]
        public void CreateOffer_InvalidCampaign_Returns400()
        {
            var campaign = "Blabla";

            var request = InitializeCreateOfferRequest(campaign:campaign);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsInvalid, "IncludeInCampaign");
            response.Header.ErrorDetails.First().Should().Contain(m);
        }

        [Test]
        public void CreateOffer_InvalidBatchId_Returns400()
        {
            var batchId = 0;

            var request = InitializeCreateOfferRequest();
            request.BatchId = batchId;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.InvalidIntFieldValue, nameof(request.BatchId));
            response.Header.ErrorDetails.First().Should().Contain(m);
        }

        [Test]
        public void CreateOffer_WithoutProducts_Returns200()
        {
            var request = InitializeCreateOfferRequest();
            request.Offers.First().Package.Products = null;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Header.Message.Should().BeNullOrEmpty();
        }

        [Test]
        public void CreateOffer_WithoutIncludeInCampaign_Returns400()
        {
            var request = InitializeCreateOfferRequest();
            request.Offers.First().IncludeInCampaign = null;
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, "IncludeInCampaign");
            response.Header.ErrorDetails[0].Should().Be(m);
        }

        [Test]
        public void CreateOffer_InvalidProductType_Returns400()
        {
            var productType = "bla";
            var request = InitializeCreateOfferRequest(productType);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.InvalidProduct, productType);
            response.Header.ErrorDetails.First().Should().Contain(m);
        }
    }
}
