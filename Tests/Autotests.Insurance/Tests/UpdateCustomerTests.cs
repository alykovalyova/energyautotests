﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.Sql;
using Autotests.Core.Sql.Entities;
using Autotests.Insurance.Base;
using Autotests.Repositories.InsuranceModels;
using FluentAssertions;
using NUnit.Framework;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Insurance.Tests
{
    [TestFixture]
    internal class UpdateCustomerTests : BaseCustomerTest
    {
        [Test]
        public void UpdateCustomer_ValidCase_Returns200()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var productType = "Motorrijtuigen";
            var specification = "mobile";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            var customerAdded = Waiter.Wait(() => InsuranceDb.GetEntityByCondition<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer, productType, specification);
            var updateResponse = updateRequest.CallWith(InsuranceClient);
            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            updateResponse.ResponseText[0].Should().NotBeNull();

            Waiter.Wait(() => InsuranceDb.GetLastEntity<CustomerProduct,DateTime>(cp => cp.CreatedOn, cp =>
            cp.CustomerId == customerAdded.Id && cp.ProductType.Description == productType &&
            cp.ProductSpecification.Description == specification, "ProductType", "ProductSpecification"));
         }

        [Test]
        public void UpdateCustomer_InvalidProductTypeUpdate_Returns200()
        {
            string policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var productType = "BlaBla";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.ResponseText[0].Should().NotBeNull();
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.Package.Products.First().Type = productType;
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            var m = string.Format(PatternMessages.InvalidProduct, productType);
            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            updateResponse.Header.ErrorDetails.First().Should().Contain(m);
        }

        [Test]
        public void UpdateCustomer_UpdateRelationId_Returns200()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var relationId = long.Parse(RandomDataProvider.GetRandomNumbersString(8));

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.RelationId = relationId;
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            updateResponse.Header.ErrorDetails.Should().BeNull();
            Waiter.Wait(() => !InsuranceDb.EntityIsInDb<CustomerProduct>(cp =>
                cp.Customer.RelationId == relationId), 5);
        }

        [Test]
        public void UpdateCustomer_UpdateInvalidRelationId_Returns400()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.RelationId = 0;
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.WrongFieldRange3, nameof(request.RelationId));
            updateResponse.Header.ErrorDetails[0].Should().Be(m);
        }

        [Test]
        public void UpdateCustomer_InvalidStartDate_Returns400()
        {
            string policyNumber = RandomDataProvider.GetRandomNumbersString(5);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.Package.Products.First().StartDate = updateRequest.Package.Products.First().EndDate.AddDays(1);
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            updateResponse.Header.ErrorDetails.First().Should().Contain(PatternMessages.InvalidStartDate);
        }

        [Test]
        public void UpdateCustomer_UpdateChangeReason_Returns200()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var changeReason = $"changeReason123";

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.Package.Products.First().ChangeReason = changeReason;
            updateRequest.Package.Products.First().ChangeReasonKey = int.Parse(RandomDataProvider.GetRandomNumbersString(2));
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            updateResponse.Header.ErrorDetails.Should().BeNull();
        }

        [Test]
        public void UpdateCustomer_UpdateGrossPrice_Returns200()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var grossPrice = 999.999m;

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.Package.GrossPrice = grossPrice;
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            updateResponse.Header.ErrorDetails.Should().BeNull();
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<CustomerProduct>(cp =>
                cp.Customer.RelationId == RelationId && cp.Customer.GrossPrice == grossPrice));
        }

        [Test]
        public void UpdateCustomer_UpdatePrice_Returns204()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);
            var price = 999.999m;

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.Package.Products.First().Price = price;
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            updateResponse.Header.ErrorDetails.Should().BeNull();
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<CustomerProduct>(cp =>
                cp.Customer.RelationId == RelationId && cp.Price == price));
        }

        [Test]
        public void UpdateCustomer_UpdateIsActive_Returns200()
        {
            var policyNumber = RandomDataProvider.GetRandomNumbersString(5);

            var request = InitializeCreateCustomerRequest(RelationId, policyNumber);
            var response = request.CallWith(InsuranceClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<Customer>(c => c.RelationId == RelationId));
            var customer = SqlQueryExecutor.Instance.ConnectTo(DbConnectionName.InsuranceDatabase.ToString())
                .ExecuteSqlQuery<ConvertedCustomer>("GetInsuredCustomer", condition: RelationId.ToString()).First();

            var updateRequest = InitializeUpdateCustomerRequest(customer);
            updateRequest.Package.Products.First().IsActive = false;
            var updateResponse = updateRequest.CallWith(InsuranceClient);

            updateResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            updateResponse.Header.ErrorDetails.Should().BeNull();
            Waiter.Wait(() => InsuranceDb.EntityIsInDb<CustomerProduct>(cp =>
                cp.Customer.RelationId == RelationId && cp.IsActive == false));
        }
    }
}
