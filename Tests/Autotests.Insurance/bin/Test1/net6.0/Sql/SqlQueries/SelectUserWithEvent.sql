﻿select cpk.ContactPersonNumber
from customer.dbo.NutsHomeCustomer c 
join customer.dbo.ProductCustomer pc on pc.NutsHomeCustomerId = c.id
join customer.dbo.CustomerContactPerson ccp on ccp.NutsHomeCustomerId = c.id 
join customer.dbo.ContactPersonKey cpk on cpk.id = ccp.ContactPersonKeyId
join Postoffice.dbo.CustomerEvent ce on ce.NutsHomeCustomerNumber = c.NutsHomeCustomerNumber
where ccp.ValidTo is null and ce.description is not null