﻿using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Repositories.OptInResellerModels;
using Newtonsoft.Json;
using NUnit.Framework;
using Nuts.OptInReseller.Contract.Api.Transport;

namespace Autotests.OptIn.Base
{
    [SetUpFixture, Category(Categories.OptIn)]
    internal abstract class BaseOptIn : AllureReport
    {
        //clients declaration
        protected NutsHttpClient OptInClient;

        //databases declaration
        protected DbHandler<OptInResellerContext> OptInDb;

        //variables initialization
        protected ConfigHandler _configHandler = new ConfigHandler();
        protected readonly string TestMarker = "AutoT_OI";
        private static readonly string _filePath = ConfigHelper.GetFilePath("OptIn/CreateOptInRequest.json");
        protected CreateOptInRequest CreateOptInObj => JsonConvert
            .DeserializeObject<CreateOptInRequest>(File.ReadAllText(_filePath));
        protected const string PostalCode = "1415AC";
        protected const string PhoneNumber = "396502745";
        protected const string OnlineExternalReference = "332f8c2e3ebb40b6-81c54ff55480e2ed.png";
        protected const string Base64File = "iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAIAAADZF8uwAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABFSURBVChThcrBDQAgCEPRbubV4Z3GBRSDQSwRyT/RB7lSWx5G/7uFvm6j3B2UuAu9HCMpOhYaOZ4t73jzmeOBUsffGIAJWI3XiRMioEUAAAAASUVORK5CYII=";
        protected const string VoiceExternalReference = "optinFolder\\optinSubFolder\\41edc283265647cfada7a12a4cd82944.mp3";
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
           OptInClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.OptIn));
           OptInDb = new DbHandler<OptInResellerContext>(_configHandler.GetConnectionString(DbConnectionName.OptInDatabase.ToString()));
        }

        [OneTimeTearDown]
        public void TearDown() => CleanOptInDb();

        [SetUp]
        public void SetUp() => CleanOptInDb();

        private void CleanOptInDb()
        {
            var optIns = OptInDb.GetEntitiesByCondition<Repositories.OptInResellerModels.OptIn>(oi
                => oi.ExternalReference.Contains(TestMarker));
            foreach (var optIn in optIns)
            {
                OptInDb.DeleteEntitiesByCondition<VoiceLog>(vl => vl.OptInId == optIn.Id);
                OptInDb.DeleteEntitiesByCondition<Repositories.OptInResellerModels.OptIn>(oi => oi.Id == optIn.Id);
                OptInDb.DeleteEntitiesByCondition<OnlineLog>(ol => ol.Id == optIn.OnlineLogId);
                OptInDb.DeleteEntitiesByCondition<Customer>(c => c.Id == optIn.CustomerId);
            }
        }

        public string GetOptInExternalReference() => $"{TestMarker}_{RandomDataProvider.GetRandomNumbersString(5)}_{DateTime.Now.Millisecond}";

        public int GenerateRandomNutsHomeCustomerNumber() => int.Parse(RandomDataProvider.GetRandomNumbersString(5));

        public int GenerateRamdonOptInId() => int.Parse(RandomDataProvider.GetRandomNumbersString(7));

        protected CreateOptInRequest InitializeCreateOptInRequest()
        {
            var request = CreateOptInObj;
            request.ExternalReference = GetOptInExternalReference();
            request.OptInDate = DateTime.Today;
            return request;
        }

        public Repositories.OptInResellerModels.OptIn GetOptInObject(string externalReference) =>
            OptInDb.GetEntityByCondition<Repositories.OptInResellerModels.OptIn>(
                oi => oi.ExternalReference == externalReference);

        public Customer GetOptInCustomerObject(int customerId) =>
            OptInDb.GetEntityByCondition<Repositories.OptInResellerModels.Customer>(
                c => c.Id == customerId);

        public VoiceLog GetOptInVoiceLog(int optInId) =>
            OptInDb.GetEntityByCondition<Repositories.OptInResellerModels.VoiceLog>(
                vl => vl.OptInId == optInId);

        public OnlineLog GetOptInOnlineLog(Repositories.OptInResellerModels.OptIn optInObj) =>
             OptInDb.GetEntityByCondition<OnlineLog>(
                ol => ol.Id == optInObj.OnlineLogId);
    }
}
