﻿using Autotests.Core.Helpers;
using Nuts.OptInReseller.Contract.Api.Model.CreateOptIn;
using Nuts.OptInReseller.Contract.Api.Transport;

namespace Autotests.OptIn.Builders
{
    internal class CreateOptInRequestBuilder : MainBuilder<CreateOptInRequest>
    {
        internal CreateOptInRequestBuilder SetExternalReference(string externalReference)
        {
            Instance.ExternalReference = externalReference;
            return this;
        }
        
        internal CreateOptInRequestBuilder SetSalesChannelId(int salesChannelId)
        {
            Instance.SalesChannelId = salesChannelId;
            return this;
        }

        internal CreateOptInRequestBuilder SetOptInDate(DateTime optInDate)
        {
            Instance.OptInDate = optInDate;
            return this;
        }

        internal CreateOptInRequestBuilder SetOptInCustomer(CreateOptInCustomer optInCustomer)
        {
            Instance.Customer = optInCustomer;
            return this;
        }

        internal CreateOptInRequestBuilder SetOnlineLog(CreateOptInOnlineLog onlineLog)
        {
            Instance.OnlineLog = onlineLog;
            return this;
        }

        internal CreateOptInRequestBuilder SetVoiceLog(params CreateOptInVoiceLog[] voiceLogs)
        {
            Instance.VoiceLogs = voiceLogs.ToList();
            return this;
        }

    }
}
