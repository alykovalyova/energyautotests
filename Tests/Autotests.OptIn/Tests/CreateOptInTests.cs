﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.OptIn.Base;
using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using Nuts.OptInReseller.Contract.Api.Enum;
using Nuts.OptInReseller.Contract.Api.Model.CreateOptIn;
using System.Net;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.OptIn.Tests
{
    [TestFixture]
    internal class CreateOptInTests : BaseOptIn
    {
        [Test]
        public void CreateOptIn_ValidCaseWithAllFieldsBothLogs_Returns200(
        [Values(AudioType.AAC, AudioType.FLAC, AudioType.M4A, AudioType.MP3, AudioType.WAV, AudioType.WMA)]
        AudioType audioType)
        {
            var request = InitializeCreateOptInRequest();
            request.VoiceLogs.LastOrDefault().AudioType = audioType.ToString();
            var externalReference = request.ExternalReference;

            request.CallWith(OptInClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var customerId = optInObj.CustomerId;
            var customerObj = GetOptInCustomerObject(customerId);
            var voiceLogObj = GetOptInVoiceLog(optInId);
            var onlineLogObj = GetOptInOnlineLog(optInObj);

            using (new AssertionScope())
            {
                optInObj.ExternalReference.Should().BeEquivalentTo(request.ExternalReference);
                optInObj.OptInDate.Should().Be((DateTime)request.OptInDate);
                optInObj.SalesChannelId.Should().Be(request.SalesChannelId);
                customerObj.NutsHomeCustomerNumber.Should().Be(request.Customer.NutsHomeCustomerNumber);
                customerObj.Initials.Should().BeEquivalentTo(request.Customer.Initials);
                customerObj.SurnamePrefix.Should().BeEquivalentTo(request.Customer.SurnamePrefix);
                customerObj.Surname.Should().BeEquivalentTo(request.Customer.Surname);
                customerObj.Street.Should().BeEquivalentTo(request.Customer.Street);
                customerObj.HouseNumber.Should().Be(request.Customer.HouseNumber);
                customerObj.HouseNumberExtension.Should().BeEquivalentTo(request.Customer.HouseNumberExtension);
                customerObj.PostalCode.Should().BeEquivalentTo(request.Customer.PostalCode);
                customerObj.City.Should().BeEquivalentTo(request.Customer.City);
                customerObj.Country.Should().BeEquivalentTo(request.Customer.Country);
                customerObj.PhoneNumber.Should().BeEquivalentTo(request.Customer.PhoneNumber);
                onlineLogObj.CephReference.Should().NotBeEmpty();
                onlineLogObj.IpAddress.Should().BeEquivalentTo(request.OnlineLog.IpAddress);
                onlineLogObj.OnlineExternalReference.Should().BeEquivalentTo(request.OnlineLog.OnlineExternalReference);
                voiceLogObj.Should().NotBeNull();
            }
        }

        [Test]
        public void CreateOptIn_ValidCaseWithRequiredFieldsBothLogs_Returns200()
        {
            var request = InitializeCreateOptInRequest();
            var customer = new CreateOptInCustomer()
            {
                PostalCode = "1111AZ",
                PhoneNumber = "395826754",
                HouseNumber = 105
            };
            request.Customer = customer;
            request.OnlineLog.IpAddress = null;
            var externalReference = request.ExternalReference;

            request.CallWith(OptInClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var customerId = optInObj.CustomerId;
            var customerObj = GetOptInCustomerObject(customerId);
            var voiceLogObj = GetOptInVoiceLog(optInId);
            var onlineLogObj = GetOptInOnlineLog(optInObj);

            using (new AssertionScope())
            {
                optInObj.ExternalReference.Should().BeEquivalentTo(request.ExternalReference);
                customerObj.NutsHomeCustomerNumber.Should().BeNull();
                customerObj.Initials.Should().BeNull();
                onlineLogObj.Should().NotBeNull();
                onlineLogObj.IpAddress.Should().BeNull();
                voiceLogObj.Should().NotBeNull();
            }
        }

        [Test]
        public void CreateOptIn_ValidCaseWithOnlineLog_Returns200()
        {
            var request = InitializeCreateOptInRequest();
            request.VoiceLogs = null;
            var externalReference = request.ExternalReference;

            request.CallWith(OptInClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var voiceLogObj = GetOptInVoiceLog(optInId);
            var onlineLogObj = GetOptInOnlineLog(optInObj);

            using (new AssertionScope())
            {
                optInObj.ExternalReference.Should().BeEquivalentTo(request.ExternalReference);
                onlineLogObj.Should().NotBeNull();
                voiceLogObj.Should().BeNull();
            }
        }

        [Test]
        public void CreateOptIn_ValidCaseWithVoiceLog_Returns200()
        {
            var request = InitializeCreateOptInRequest();
            request.OnlineLog = null;
            var externalReference = request.ExternalReference;

            request.CallWith(OptInClient).Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var voiceLogObj = GetOptInVoiceLog(optInId);
            var onlineLogObj = GetOptInOnlineLog(optInObj);

            using (new AssertionScope())
            {
                optInObj.ExternalReference.Should().BeEquivalentTo(request.ExternalReference);
                optInObj.OnlineLogId.Should().BeNull();
                voiceLogObj.Should().NotBeNull();
                onlineLogObj.Should().BeNull();
            }
        }

        [Test]
        public void CreateOptIn_NotUniqExternalReferenceValue_Returns400()
        {
            var firstRequest = InitializeCreateOptInRequest();
            var externalReference = firstRequest.ExternalReference;
            firstRequest.CallWith(OptInClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            Waiter.Wait(() => OptInDb.EntityIsInDb<Repositories.OptInResellerModels.OptIn>(
                oi => oi.ExternalReference == externalReference));

            var secondRequestWithNotUniqExternalReference = firstRequest;
            var response = secondRequestWithNotUniqExternalReference.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.NotUniqExternalReferenceProvided, externalReference);
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void CreateOptIn_WithoutBothVoiceOnlineLogs_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.OnlineLog = null;
            request.VoiceLogs = null;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.NoLogsProvided);
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }
        
        [Test]
        public void CreateOptIn_WithInvalidPostalCode_Reterns400()
        {
            var request = InitializeCreateOptInRequest();
            request.Customer.PostalCode = $"{RandomDataProvider.GetRandomNumbersString(6)}";

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.InvalidPostalCode);
            response.Header.ErrorDetails.Contains(errorMessage);
        }

        [Test]
        public void CreateOptIn_MissingRequiredField_ExternalReference_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.ExternalReference = null;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, "ExternalReference");
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void CreateOptIn_MissingRequiredField_OptInDate_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.OptInDate = null;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, "OptInDate");
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void CreateOptIn_MissingRequiredField_SalesChannelId_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.SalesChannelId = 0;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.WrongIntFieldFormat, "SalesChannelId");
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void CreateOptIn_MissingRequiredField_HouseNumber_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.Customer.HouseNumber = 0;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.WrongFieldRange2, "HouseNumber");
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void CreateOptIn_RequiredFieldValueIsEmpty_AudioType_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.VoiceLogs.LastOrDefault().AudioType = "";

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.EmptyAudioType);
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void CreateOptIn_RequiredFieldValueIsNull_AudioType_Returns400()
        {
            var request = InitializeCreateOptInRequest();
            request.VoiceLogs.LastOrDefault().AudioType = null;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, "AudioType");
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test, Ignore("two issues were created - https://budget.atlassian.net/browse/UT-4990," +
            "https://budget.atlassian.net/browse/UT-4989")]
        [
            TestCase("", PhoneNumber, OnlineExternalReference, Base64File, VoiceExternalReference, nameof(PostalCode),
                TestName = "CreateOptIn_RequiredFieldIsEmpty_PostalCode"),
            TestCase(PostalCode, "", OnlineExternalReference, Base64File, VoiceExternalReference, nameof(PhoneNumber),
                TestName = "CreateOptIn_RequiredFieldIsEmpty_PhoneNumber"),
            TestCase(PostalCode, PhoneNumber, "", Base64File, VoiceExternalReference, nameof(OnlineExternalReference),
                TestName = "CreateOptIn_RequiredFieldIsEmpty_OnlineExternalReference"),
            TestCase(PostalCode, PhoneNumber, OnlineExternalReference, "", VoiceExternalReference, nameof(Base64File),
                TestName = "CreateOptIn_RequiredFieldIsEmpty_Base64File"),
            TestCase(PostalCode, PhoneNumber, OnlineExternalReference, Base64File, "", nameof(VoiceExternalReference),
                TestName = "CreateOptIn_RequiredFieldIsEmpty_VoiceExternalReference")
            ]
        public void CreateOptIn_RequiredFieldIsEmptyString_Returns400(
            string postalCode, string phoneNumber, string onlineExternalReference,
            string base64File, string voiceOnlineReference, string requiredField)
        {
            var request = InitializeCreateOptInRequest();
            request.Customer.PostalCode = postalCode;
            request.Customer.PhoneNumber = phoneNumber;
            request.OnlineLog.OnlineExternalReference = onlineExternalReference;
            request.OnlineLog.Base64File = base64File;
            request.VoiceLogs.LastOrDefault().VoiceExternalReference = voiceOnlineReference;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, requiredField);
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        [
            TestCase(null, PhoneNumber, OnlineExternalReference, Base64File, VoiceExternalReference,
                nameof(PostalCode), TestName = "CreateOptIn_RequiredFieldIsNull_PostalCode"),
            TestCase(PostalCode, null, OnlineExternalReference, Base64File, VoiceExternalReference,
                nameof(PhoneNumber), TestName = "CreateOptIn_RequiredFieldIsNull_PhoneNumber"),
            TestCase(PostalCode, PhoneNumber, null, Base64File, VoiceExternalReference,
                nameof(OnlineExternalReference), TestName = "CreateOptIn_RequiredFieldIsNull_OnlineExternalReference"),
            TestCase(PostalCode, PhoneNumber, OnlineExternalReference, null, VoiceExternalReference,
                nameof(Base64File), TestName = "CreateOptIn_RequiredFieldIsNull_Base64File"),
            TestCase(PostalCode, PhoneNumber, OnlineExternalReference, Base64File, null,
                nameof(VoiceExternalReference), TestName = "CreateOptIn_RequiredFieldIsNull_VoiceExternalReference")
            ]
        public void CreateOptIn_RequiredFieldValueIsNull_Returns400(
            string postalCode, string phoneNumber, string onlineExternalReference,
            string base64File, string voiceOnlineReference, string requiredField)
        {
            var request = InitializeCreateOptInRequest();
            request.Customer.PostalCode = postalCode;
            request.Customer.PhoneNumber = phoneNumber;
            request.OnlineLog.OnlineExternalReference = onlineExternalReference;
            request.OnlineLog.Base64File = base64File;
            request.VoiceLogs.LastOrDefault().VoiceExternalReference = voiceOnlineReference;

            var response = request.CallWith(OptInClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, requiredField);
            response.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }
    }
}
