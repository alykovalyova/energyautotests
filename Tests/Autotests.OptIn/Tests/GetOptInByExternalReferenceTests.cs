﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.OptIn.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.OptInReseller.Contract.Api.Transport;
using System.Net;

namespace Autotests.OptIn.Tests
{
    [TestFixture]
    internal class GetOptInByExternalReferenceTests : BaseOptIn
    {
        [Test]
        public void GetOptInByExternalReference_GetExistedOptIn_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            createOptInRequest.CallWith(OptInClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;

            var getOptInResponse = new GetOptInByExternalReferenceRequest { ExternalReference = externalReference }
            .CallWith<GetOptInByExternalReferenceRequest, GetOptInByExternalReferenceResponse>(OptInClient);

            getOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            getOptInResponse.Data.ExternalReference.Should().BeEquivalentTo(externalReference);
            getOptInResponse.Data.OptInId.Should().Be(optInId);
            getOptInResponse.Data.Customer.Should().NotBeNull();
            getOptInResponse.Data.OnlineLog.Should().NotBeNull();
            getOptInResponse.Data.VoiceLogs.Should().NotBeNull();
        }

        [Test]
        public void GetOptInByExternalReference_GetNonExistedOptIn_Returns404()
        {
            var externalReference = GetOptInExternalReference();
            var response = new GetOptInByExternalReferenceRequest { ExternalReference = externalReference }
            .CallWith<GetOptInByExternalReferenceRequest, GetOptInByExternalReferenceResponse>(OptInClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.NotFound);
            var errorMessage = string.Format(PatternMessages.InvalidOptInSearchByExternalReference, externalReference);
            response.Header.ErrorDetails.Contains(errorMessage);
        }

        [Test]
        public void GetOptInByExternalReference_NoSpecifiedExternalReference_Returns400()
        {
            var response = new GetOptInByExternalReferenceRequest { ExternalReference = null }
            .CallWith<GetOptInByExternalReferenceRequest, GetOptInByExternalReferenceResponse>(OptInClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, "ExternalReference");
            response.Header.ErrorDetails.Contains(errorMessage);
        }
    }
}
