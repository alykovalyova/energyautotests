﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.OptIn.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.OptInReseller.Contract.Api.Transport;
using System.Net;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.OptIn.Tests
{
    [TestFixture]
    internal class GetOptInByIdTests : BaseOptIn
    {
        [Test]
        public void GetOptInById_GetExistedOptIn_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            createOptInRequest.CallWith(OptInClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var onlineLogId = optInObj.OnlineLogId;
            var voiceLogId = GetOptInVoiceLog(optInId);

            var getOptInResponse = new GetOptInRequest { OptInId = optInId }
            .CallWith<GetOptInRequest, GetOptInResponse>(OptInClient);

            getOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            getOptInResponse.Data.ExternalReference.Should().BeEquivalentTo(externalReference);
            getOptInResponse.Data.Customer.Should().NotBeNull();
            getOptInResponse.Data.OnlineLog.OnlineLogId.Should().Be(onlineLogId);
            getOptInResponse.Data.VoiceLogs.FirstOrDefault().VoiceLogId.Equals(voiceLogId);
        }

        [Test]
        public void GetOptInById_GetNonExistedOptIn_Returns404()
        {
            var optInId = int.Parse($"{RandomDataProvider.GetRandomNumbersString(3)}{DateTime.Now.Millisecond}");
            var getOptInResponse = new GetOptInRequest { OptInId = optInId }
           .CallWith<GetOptInRequest, GetOptInResponse>(OptInClient);

            getOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.NotFound);
            var errorMessage = string.Format(PatternMessages.InvalidOptInSearchById, optInId);
            getOptInResponse.Header.ErrorDetails.Contains(errorMessage);
        }

        [Test]
        public void GetOptInById_InvalidOptInIDProvided_Returns400()
        {
            int optInId = 0;
            var getOptInResponse = new GetOptInRequest { OptInId = optInId }
           .CallWith<GetOptInRequest, GetOptInResponse>(OptInClient);

            getOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.InvalidRange, "OptInId");
            getOptInResponse.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }
    }
}
