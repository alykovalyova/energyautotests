﻿using Autotests.Clients;
using Autotests.Core;
using Autotests.OptIn.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.OptInReseller.Contract.Api.Model.SearchOptIn;
using Nuts.OptInReseller.Contract.Api.Transport;
using System.Net;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.OptIn.Tests
{
    [TestFixture]
    internal class SearchOptInsTests : BaseOptIn
    {
        [Test]
        public void SearchOptIns_ValidCase_SearchByAllFields_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var nutsHomeCustomerNumber = createOptInRequest.Customer.NutsHomeCustomerNumber;
            var externalReference = createOptInRequest.ExternalReference;
            var salesChannelId = createOptInRequest.SalesChannelId;
            var optInDate = createOptInRequest.OptInDate;
            var surname = createOptInRequest.Customer.Surname;
            var houseNumber = createOptInRequest.Customer.HouseNumber;
            var postalCode = createOptInRequest.Customer.PostalCode;

            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            int optInId = optInObj.Id;

            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    ExternalReference = externalReference,
                    NutsHomeCustomerNumber = nutsHomeCustomerNumber,
                    SalesChannelId = salesChannelId
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.Count().Should().Be(1);
            searchResponse.Data.SearchResult.All(r => r.ExternalReference == externalReference);
            searchResponse.Data.SearchResult.All(r => r.Customer.NutsHomeCustomerNumber == nutsHomeCustomerNumber);
            searchResponse.Data.SearchResult.All(r => r.SalesChannelId == salesChannelId);
            searchResponse.Data.SearchResult.FirstOrDefault().Id.Should().Be(optInId);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInDate.Should().Be((DateTime)optInDate);
            searchResponse.Data.SearchResult.FirstOrDefault().SalesChannelId.Should().Be(salesChannelId);
            searchResponse.Data.SearchResult.FirstOrDefault().Customer.NutsHomeCustomerNumber.Should().Be(nutsHomeCustomerNumber);
            searchResponse.Data.SearchResult.FirstOrDefault().Customer.Surname.Should().Be(surname);
            searchResponse.Data.SearchResult.FirstOrDefault().Customer.HouseNumber.Should().Be(houseNumber);
            searchResponse.Data.SearchResult.FirstOrDefault().Customer.PostalCode.Should().Be(postalCode);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInFilesAvailable.Should().BeTrue();
        }

        [Test]
        public void SearchOptIns_SearchByExternalReference_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    ExternalReference = externalReference
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.Count().Should().Be(1);
            searchResponse.Data.SearchResult.All(r => r.ExternalReference == externalReference);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInFilesAvailable.Should().BeTrue();
        }

        [Test]
        public void SearchOptIns_SearchByNutsHomeCustomerNumber_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            var nutsHomeCustomerNumber = createOptInRequest.Customer.NutsHomeCustomerNumber;
            
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            int optInId = optInObj.Id;

            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    NutsHomeCustomerNumber = nutsHomeCustomerNumber
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.FirstOrDefault(r => r.Id == optInId);
            searchResponse.Data.SearchResult.All(r => r.Customer.NutsHomeCustomerNumber == nutsHomeCustomerNumber);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInFilesAvailable.Should().BeTrue();
        }

        [Test]
        public void SearchOptIns_SearchBySalesChannelId_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            var salesChannelId = createOptInRequest.SalesChannelId;
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            int optInId = optInObj.Id;

            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    SalesChannelId = salesChannelId
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.FirstOrDefault(r => r.Id == optInId);
            searchResponse.Data.SearchResult.All(r => r.SalesChannelId == salesChannelId);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInFilesAvailable.Should().BeTrue();
        }

        [Test, Ignore("untill the voice log storage is configured")]
        public void SearchOptIns_ValidCase_VoiceLogAvailable_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            createOptInRequest.OnlineLog = null;
            var externalReference = createOptInRequest.ExternalReference;
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;

            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    ExternalReference = externalReference
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.FirstOrDefault(r => r.Id == optInId);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInFilesAvailable.Should().BeTrue();
        }

        [Test]
        public void SearchOptIns_ValidCase_OnlineLogAvailable_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            createOptInRequest.VoiceLogs = null;
            var externalReference = createOptInRequest.ExternalReference;
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;

            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    ExternalReference = externalReference
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.FirstOrDefault(r => r.Id == optInId);
            searchResponse.Data.SearchResult.FirstOrDefault().OptInFilesAvailable.Should().BeTrue();
        }

        [Test]
        public void SearchOptIns_NoResultsFoundByExternalReference_Returns200()
        {
            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria
                {
                    ExternalReference = GetOptInExternalReference()
                }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            searchResponse.Data.SearchResult.Count.Should().Be(0);
        }

        [Test]
        public void SearchOptIns_InvalidCase_NoSearchCriteriaProvided_Return400()
        {
            var searchRequest = new SearchOptInRequest
            {
                SearchCriteria = new SearchOptInSearchCriteria { }
            };

            var searchResponse = searchRequest.CallWith<SearchOptInRequest, SearchOptInResponse>(OptInClient);
            searchResponse.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.InvalidSearchCriteria);
            searchResponse.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

    }
}
