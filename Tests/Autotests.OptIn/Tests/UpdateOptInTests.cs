﻿using Autotests.Clients;
using NUnit.Framework;
using System.Net;
using Nuts.OptInReseller.Contract.Api.Transport;
using Nuts.OptInReseller.Contract.Api.Model.UpdateOptIn;
using FluentAssertions;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Autotests.OptIn.Base;

namespace Autotests.OptIn.Tests
{
    [TestFixture]
    internal class UpdateOptInTests : BaseOptIn
    {
        [Test]
        public void UpdateOptIn_ValidCase_UpdateToANewValue_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var customerId = optInObj.CustomerId;
            var customerObj = GetOptInCustomerObject(customerId);
            var nutsHomeCustomerNumberBeforeUpdate = customerObj.NutsHomeCustomerNumber;

            var updateOptInRequest = new UpdateOptInRequest
            {
                OptInId = optInId,
                Customer = new UpdateOptInUpdateCustomer
                {
                    NutsHomeCustomerNumber = GenerateRandomNutsHomeCustomerNumber()
                }
            };
            var updateOptInResponse = updateOptInRequest.CallWith(OptInClient);
            updateOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var customerObjAfterUpdate = GetOptInCustomerObject(customerId);
            var nutsHomeCustomerNumberAfterUpdate = customerObjAfterUpdate.NutsHomeCustomerNumber;
            nutsHomeCustomerNumberAfterUpdate.Should().NotBe(nutsHomeCustomerNumberBeforeUpdate);
        }

        [Test]
        public void UpdateOptIn_ValidCase_UpdateFromNullToValue_Returns200()
        {
            var createOptInRequest = InitializeCreateOptInRequest();
            var externalReference = createOptInRequest.ExternalReference;
            createOptInRequest.Customer.NutsHomeCustomerNumber = null;
            var createOptInResponse = createOptInRequest.CallWith(OptInClient);
            createOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var optInObj = GetOptInObject(externalReference);
            var optInId = optInObj.Id;
            var customerId = optInObj.CustomerId;
            var customerObj = GetOptInCustomerObject(customerId);
            var nutsHomeCustomerNumberBeforeUpdate = customerObj.NutsHomeCustomerNumber;

            var updateOptInRequest = new UpdateOptInRequest
            {
                OptInId = optInId,
                Customer = new UpdateOptInUpdateCustomer
                {
                    NutsHomeCustomerNumber = GenerateRandomNutsHomeCustomerNumber()
                }
            };
            var updateOptInResponse = updateOptInRequest.CallWith(OptInClient);
            updateOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var customerObjAfterUpdate = GetOptInCustomerObject(customerId);
            var nutsHomeCustomerNumberAfterUpdate = customerObjAfterUpdate.NutsHomeCustomerNumber;
            nutsHomeCustomerNumberAfterUpdate.Should().NotBe(nutsHomeCustomerNumberBeforeUpdate);
        }

        [Test]
        public void UpdateOptIn_InalidCase_UpdateNonExistentOptIn_Returns404()
        {
            var updateOptInRequest = new UpdateOptInRequest
            {
                OptInId = GenerateRamdonOptInId(),
                Customer = new UpdateOptInUpdateCustomer
                {
                    NutsHomeCustomerNumber = GenerateRandomNutsHomeCustomerNumber()
                }
            };
            int optInId = updateOptInRequest.OptInId;
            var updateOptInResponse = updateOptInRequest.CallWith(OptInClient);
            updateOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var errorMessage = string.Format(PatternMessages.InvalidOptInToUpdate, optInId);
            updateOptInResponse.Header.Message.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void UpdateOptIn_InalidCase_UpdateNutsHomeCustomerNumberToNull_Returns400()
        {
            var updateOptInRequest = new UpdateOptInRequest
            {
                OptInId = GenerateRamdonOptInId(),
                Customer = new UpdateOptInUpdateCustomer { }
            };
            
            var updateOptInResponse = updateOptInRequest.CallWith(OptInClient);
            updateOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.InvalidNutsHomeCustomerNumberUpdateToNull);
            updateOptInResponse.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void UpdateOptIn_InalidCase_UpdateInvalidOptInId_Returns400()
        {
            var updateOptInRequest = new UpdateOptInRequest
            {
                OptInId = 0,
                Customer = new UpdateOptInUpdateCustomer
                {
                    NutsHomeCustomerNumber = GenerateRandomNutsHomeCustomerNumber()
                }
            };

            var updateOptInResponse = updateOptInRequest.CallWith(OptInClient);
            updateOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.InvalidRange, "OptInId");
            updateOptInResponse.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void UpdateOptIn_InalidCase_NoCustomerDataProvided_Returns400()
        {
            var updateOptInRequest = new UpdateOptInRequest
            {
                OptInId = GenerateRamdonOptInId()
            };

            var updateOptInResponse = updateOptInRequest.CallWith(OptInClient);
            updateOptInResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            var errorMessage = string.Format(PatternMessages.FieldIsRequired, "Customer");
            updateOptInResponse.Header.ErrorDetails.Should().BeEquivalentTo(errorMessage);
        }
    }
}
