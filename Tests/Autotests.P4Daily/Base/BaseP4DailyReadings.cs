﻿using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Builders;
using Autotests.P4Daily.Enums;
using Autotests.Repositories.CcuModels;
using Autotests.Repositories.MeteringPointModels;
using Autotests.Repositories.P4DailyReadingsModels;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.P4DailyReadings.Contract;
using MeterReading = Autotests.Repositories.P4DailyReadingsModels.MeterReading;
using P4MeteringPoint = Nuts.P4DailyReadings.Contract.MeteringPoint;
using P4MeteringPointSql = Autotests.Repositories.P4DailyReadingsModels.MeteringPoint;
using RegisterReading = Autotests.Repositories.P4DailyReadingsModels.RegisterReading;

namespace Autotests.P4Daily.Base
{
    [SetUpFixture, Category(Core.Helpers.Categories.P4DailyReadings)]
    internal abstract class BaseP4DailyReadings : AllureReport
    {
        //clients declaration
        protected NutsHttpClient P4DailyClient;
        protected FakeEdsnAdminClient FakeEdsnAdminClient;
        protected RestSchedulerClient Scheduler;

        //databases declaration
        protected DbHandler<P4DailyReadingsContext> P4DailyReadingsDb;
        protected DbHandler<MeteringPointContext> MeteringPointDb;
        protected DbHandler<CommercialCharacteristicContext> CcuDb;

        //providers declaration
        protected SupplyPhaseEntitiesProvider SupplyPhaseEntitiesProvider;
        protected CcuEntitiesProvider CcuEntitiesProvider;

        //variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        protected const string InvalidGridOperator = "1234567891777";
        protected BuildDirector BuildDirector;
        protected EanInfo GasEan;
        protected EanInfo ElkEan;
        protected DateTime CurrentDate = DateTime.Today;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            FakeEdsnAdminClient = new FakeEdsnAdminClient();
            FakeEdsnAdminClient.ClearAllRequests();
            GasEan = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            ElkEan = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            P4DailyClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.P4Daily));
            Scheduler = new RestSchedulerClient();
            P4DailyReadingsDb = new DbHandler<P4DailyReadingsContext>(_configHandler.GetConnectionString(DbConnectionName.P4DailyReadingsDatabase.ToString()));
            MeteringPointDb = new DbHandler<Repositories.MeteringPointModels.MeteringPointContext>(_configHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
            CcuDb = new DbHandler<Repositories.CcuModels.CommercialCharacteristicContext>(_configHandler.GetConnectionString(DbConnectionName.CcuDatabase.ToString()));
            CleanP4DailyDb();
            BuildDirector = new BuildDirector();
            SupplyPhaseEntitiesProvider = new SupplyPhaseEntitiesProvider();
            CcuEntitiesProvider = new CcuEntitiesProvider();
            AddMeteringPointGain(CurrentDate, GasEan, ElkEan);
        }

        [TearDown]
        public void TearDown()
        {
            CleanP4DailyDb();
        }

        protected P4MeteringPoint GetMeteringPoint(string eanId, string gridOpr, EnergyProductTypeCode? productType, string supplier = "8714252018141")
        {
            return BuildDirector.Get<P4MeteringPointBuilder>()
                .SetEanId(eanId)
                .SetGridOperator(gridOpr).SetBalanceSupplier(supplier)
                .SetProductType(productType).Build();
        }

        protected void AddMeteringPointGain(DateTime date, params EanInfo[] eans)
        {
            foreach (var ean in eans)
            {
                SupplyPhaseEntitiesProvider.AddSupplyPhase(ean.EanId, date, productType: ean.ProductType.ToString(),
                gridOperator: ean.GridOperator, isSmartMeter: true);
                var ccuRequest = CcuEntitiesProvider.CreateCommercialCharacteristic(ean.EanId,
                  TestConstants.OtherPartyBalanceSupplier, mutationDate: date);
                FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
                CcuEntitiesProvider.CallMoveIn(ean.EanId, TestConstants.BudgetSupplier, date);

                Waiter.Wait(() => CcuDb.GetEntityByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId == ean.EanId));
                Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(m => m.EanId == ean.EanId));

            }
        }

        protected void AddMeteringPointLoss(DateTime date, params EanInfo[] eans)
        {
            foreach (var ean in eans)
            {
                SupplyPhaseEntitiesProvider.AddSupplyPhase(ean.EanId, date, productType: ean.ProductType.ToString(),
                    gridOperator: ean.GridOperator, isSmartMeter: true);
                var ccuRequest = CcuEntitiesProvider.CreateCommercialCharacteristic(ean.EanId, TestConstants.BudgetSupplier,
                    mutationDate: date);
                FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
                CcuEntitiesProvider.CallMoveOut(ean.EanId, TestConstants.BudgetSupplier, date);

                Waiter.Wait(() => CcuDb.GetEntityByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId == ean.EanId && ccu.MessageSourceId == 1));
            }
        }

        protected string GetDailyReadingExternalReference(P4MeteringPointSql ean)
        {
            return Waiter.Wait(() => P4DailyReadingsDb.GetLastEntity<CommunicationHistory, DateTime>(cc => cc.CreatedOn,
                cc => cc.MeteringPoint.Id == ean.Id)).ExternalReference;
        }

        protected string GetHistoricalReadingExternalReference(string ean)
        {
            return $"Hist_{ean}";
        }

        protected void UpdateMandate(bool isMandateEnabled = true, params P4MeteringPoint[] mps)
        {
            foreach (var mp in mps)
            {
                new UpsertMandateRequest()
                {
                    MandateEnabled = isMandateEnabled,
                    MeteringPoints = new List<P4MeteringPoint>()
                {
                    new P4MeteringPoint
                    {
                        EanId = mp.EanId,
                        BalanceSupplier = mp.BalanceSupplier,
                        GridOperator = mp.GridOperator,
                        ProductType = mp.ProductType
                    }
                }
                }.CallWith(P4DailyClient);

                Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<P4MeteringPointSql>(mp4 => mp4.EanId == mp.EanId
                && mp4.Mandate == isMandateEnabled));
            }
        }

        protected void CleanP4DailyDb()
        {
            var eans = P4DailyReadingsDb.GetEntitiesByCondition<P4MeteringPointSql>(mp =>
                mp.EanId.StartsWith("000"));
            foreach (var ean in eans)
            {
                var meterReadings = P4DailyReadingsDb.GetEntitiesByCondition<MeterReading>(mr => mr.EanId == ean.EanId);
                foreach (var reading in meterReadings)
                {
                    P4DailyReadingsDb.DeleteEntitiesByCondition<RegisterReading>(mr =>
                        mr.MeterReadingId == reading.Id);
                }
                P4DailyReadingsDb.DeleteEntitiesByCondition<MeterReading>(mr => mr.EanId == ean.EanId);
                P4DailyReadingsDb.DeleteEntitiesByCondition<MeteringPointStatusHistory>(h =>
                    h.MeteringPointId == ean.Id);
                var comHists = P4DailyReadingsDb.GetEntitiesByCondition<CommunicationHistory>(h =>
                    h.MeteringPointId == ean.Id);

                foreach (var ch in comHists)
                {
                    var statusHistories = P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.StatusHistory>(
                            coh => coh.CommunicationHistoryId == ch.Id);
                    foreach (var sh in statusHistories)
                    {
                        var rejections = P4DailyReadingsDb.GetEntitiesByCondition<EdsnRejection>(er => er.StatusHistoryId == sh.Id);
                        P4DailyReadingsDb.DeleteEntities(rejections);
                    }
                    P4DailyReadingsDb.DeleteEntitiesByCondition<StatusHistory>(sh =>
                        sh.CommunicationHistoryId == ch.Id);
                }
                P4DailyReadingsDb.DeleteEntitiesByCondition<CommunicationHistory>(h =>
                    h.MeteringPointId == ean.Id);
                var mandateHistories = P4DailyReadingsDb.GetEntitiesByCondition<MandateHistory>(mbox => mbox.MeteringPointId == ean.Id);
                foreach (var mhis in mandateHistories)
                {
                    P4DailyReadingsDb.DeleteEntityByCondition<MandateHistory>(mh => mh.Id == mhis.Id);
                }
                P4DailyReadingsDb.DeleteEntitiesByCondition<P4MeteringPointSql>(mp => mp.EanId == ean.EanId);
                Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<P4MeteringPointSql>(mp => mp.EanId == ean.EanId).Count == 0);
            }
        }

        protected void MpStatusShouldBe(P4MeteringPointSql mp, P4MeteringPointStatus status)
        {
            if (mp.StatusId != (int)status)
            {
                throw new AssertionException(
                    $"MP with ean {mp.EanId} has {(P4MeteringPointStatus)mp.StatusId} instead {status}");
            }
        }

        protected void MpStatusHistoryShouldBe(int chId, P4ProcessStatus status)
        {
            var mpStatusHistoryDb =
                P4DailyReadingsDb.GetEntitiesByCondition<StatusHistory>(h => h.CommunicationHistoryId == chId)
                    .Last();
            if (mpStatusHistoryDb.StatusId != (int)status)
            {
                throw new AssertionException(
                    $"MpStatusHistory has {(P4ProcessStatus)mpStatusHistoryDb.StatusId} instead {status}");
            }
        }

        protected CommunicationHistory GetCommunicationHistory(int mpId) =>
            P4DailyReadingsDb.GetSingleEntityByCondition<CommunicationHistory>(h => h.MeteringPointId == mpId, "MeteringPoint");

        protected List<StatusHistory> GetStatusHistory(int comHistoryId) =>
            P4DailyReadingsDb.GetEntitiesByCondition<StatusHistory>(h => h.CommunicationHistoryId == comHistoryId);

        protected List<EdsnRejection> GetEdsnRejection(int statusHistoryId) =>
            P4DailyReadingsDb.GetEntitiesByCondition<EdsnRejection>(h => h.StatusHistoryId == statusHistoryId);
    }
}