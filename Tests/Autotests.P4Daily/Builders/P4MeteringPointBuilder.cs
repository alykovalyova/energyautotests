﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Model.Core.Enums;
using P4MeteringPoint = Nuts.P4DailyReadings.Contract.MeteringPoint;

namespace Autotests.P4Daily.Builders
{
    internal class P4MeteringPointBuilder : MainBuilder<P4MeteringPoint>
    {
        internal P4MeteringPointBuilder SetEanId(string eanId)
        {
            Instance.EanId = eanId;
            return this;
        }

        internal P4MeteringPointBuilder SetGridOperator(string gridOperator)
        {
            Instance.GridOperator = gridOperator;
            return this;
        }

        internal P4MeteringPointBuilder SetBalanceSupplier(string balanceSupplier)
        {
            Instance.BalanceSupplier = balanceSupplier;
            return this;
        }

        internal P4MeteringPointBuilder SetProductType(EnergyProductTypeCode? productType)
        {
            Instance.ProductType = productType.ToString();
            return this;
        }
    }
}