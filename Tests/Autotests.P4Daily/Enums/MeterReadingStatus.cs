﻿namespace Autotests.P4Daily.Enums
{
    public enum MeterReadingStatus
    {
        Accepted = 1,
        Corrected,
        Rejected,
        SyncFailure
    }
}