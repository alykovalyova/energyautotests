﻿namespace Autotests.P4Daily.Enums
{
    public enum P4MeteringPointStatus
    {
        New = 1,
        Active,
        InActive,
        EdsnError
    }
}