﻿namespace Autotests.P4Daily.Enums
{
    public enum P4ProcessStatus
    {
        ReadyToSend = 1,
        SentToEdsn,
        NoResponse,
        IncompleteData,
        Accepted,
        RejectedByEdsn,
        SemanticRejectedByEdsn
    }
}