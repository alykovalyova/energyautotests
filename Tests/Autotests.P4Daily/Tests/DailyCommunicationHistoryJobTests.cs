﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Base;
using Autotests.P4Daily.Enums;
using Autotests.Repositories.P4DailyReadingsModels;
using FluentAssertions;
using FluentAssertions.Common;
using NUnit.Framework;
using P4MeteringPointSql = Autotests.Repositories.P4DailyReadingsModels.MeteringPoint;
using P4MeterReadingSql = Autotests.Repositories.P4DailyReadingsModels.MeterReading;

namespace Autotests.P4Daily.Tests
{
    [TestFixture]
    internal class DailyCommunicationHistoryJobTests : BaseP4DailyReadings
    {
        [Test]
        public void DailyCommunicationHistoryJob_SuccessFlow_MeterReadingAndRegisterReadingInsertedPlusAcceptedStatus()
        {
            var mp = GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType);

            UpdateMandate(true, mp);

            // trigger RequestDailyReadingsJob to put CommunicationHistory into DB
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);
            var mp4WithCommHist = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == mp.EanId);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4WithCommHist);
            var readings = P4DataProvider.AddDailyReadingsDetails(GasEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // check that new CommunicationHistory is in DB and MP status is Active
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id));
            comHistory.Should().NotBeNull();
            MpStatusHistoryShouldBe(comHistory.Id, P4ProcessStatus.SentToEdsn);

            // trigger DailyCommunicationHistoryJob
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // check that new StatusHistory with status Accepted is added to DB
            new Action(() => Waiter.Wait(() =>
            {
                var statusHistories = GetStatusHistory(comHistory.Id);
                return statusHistories.Count == 3 &&
                       statusHistories.Any(sh => sh.StatusId.Equals((int)P4ProcessStatus.Accepted));
            })).Should().NotThrow();

            // check that MeterReading with RegisterReadings was added to DB
            new Action(() => Waiter.Wait(() =>
            {
                var mr = P4DailyReadingsDb.GetEntityByCondition<P4MeterReadingSql>(r => r.EanId == mp.EanId);
                return mr != null;
            })).Should().NotThrow();
        }

        [Test]
        public void DailyCommunicationHistoryJob_ReadingDateMoreThan2YearsInPast_CommunicationHistoryRemovedFromDb()
        {
            var mp = GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType);

            // add metering point
            UpdateMandate(true, mp);

            // trigger RequestDailyReadingsJob to put CommunicationHistory into DB
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);
            var mp4CommHist = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == mp.EanId);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4CommHist);
            var readings = P4DataProvider.AddDailyReadingsDetails(GasEan, true);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // check that new CommunicationHistory is in DB and MP status is Active
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id), 20);
            comHistory.Should().NotBeNull();
            MpStatusHistoryShouldBe(comHistory.Id, P4ProcessStatus.SentToEdsn);

            // set CommunicationHistory/ReadingDate on more than 2 years in past in DB
            P4DailyReadingsDb.UpdateSingleEntity<CommunicationHistory>(
                ch => ch.MeteringPointId.Equals(mpDb.Id),
                ch => ch.ReadingDate = DateTime.Today.AddYears(-2).AddDays(-5));

            // trigger DailyCommunicationHistoryJob
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // check that CommunicationHistory is removed from DB
            new Action(() => Waiter.Wait(() => GetCommunicationHistory(mpDb.Id) == null, 20)).Should().NotThrow();
        }

        [Test]
        public void DailyCommunicationHistoryJob_ReadingDateEqualsTodayMinus3Days_NoResponseStatusShouldBeAdded()
        {
            var mp = GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType);
            UpdateMandate(true, mp);

            // trigger RequestDailyReadingsJob to put CommunicationHistory into DB
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);
            var mp4CommHist = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == mp.EanId);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4CommHist);
            var readings = P4DataProvider.AddDailyReadingsDetails(GasEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // check that new CommunicationHistory is in DB and MP status is Active
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            MpStatusHistoryShouldBe(mpDb.Id, P4ProcessStatus.Accepted);
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id));
            comHistory.Should().NotBeNull();

            // set CommunicationHistory/ReadingDate on minus 3 days in past in DB
            P4DailyReadingsDb.UpdateSingleEntity<CommunicationHistory>(
                ch => ch.MeteringPointId.Equals(mpDb.Id),
                ch => ch.ReadingDate = DateTime.Today.AddDays(-3));

            // trigger DailyCommunicationHistoryJob
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // check that new StatusHistory with status NoResponse is added to DB
            new Action(() => Waiter.Wait(() =>
            {
                var statusHistories = GetStatusHistory(comHistory.Id);
                return statusHistories.Count == 4 &&
                       statusHistories.Any(sh => sh.StatusId.Equals((int)P4ProcessStatus.NoResponse));
            }, 20)).Should().NotThrow();
        }

        [Test]
        public void DailyCommunicationHistoryJob_WithRejectedEans_RejectedStatusShouldBe()
        {
            var mp = GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType);

            // add metering point         
            UpdateMandate(true, mp);

            // trigger RequestDailyReadingsJob to put CommunicationHistory into DB
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            var mp4CommHist = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == mp.EanId);
            var externalReference = GetDailyReadingExternalReference(mp4CommHist);
            var readings = P4DataProvider.AddDailyReadingsDetails(GasEan, false);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // check that new CommunicationHistory is in DB and MP status is Active
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id));
            MpStatusHistoryShouldBe(comHistory.Id, P4ProcessStatus.SentToEdsn);
            comHistory.Should().NotBeNull();

            // trigger DailyCommunicationHistoryJob
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // check that new StatusHistory with status Rejected is added to DB
            new Action(() => Waiter.Wait(() =>
            {
                var statusHistories = GetStatusHistory(comHistory.Id);
                return statusHistories.Count == 3 &&
                       statusHistories.Any(sh => sh.StatusId.Equals((int)P4ProcessStatus.RejectedByEdsn));
            })).Should().NotThrow();
        }

        [Test, Ignore("https://budget.atlassian.net/browse/UT-4963")]
        public void DailyCommunicationHistoryJob_WhenMeteringPointIsInActive_MeteringPointStatusMustChangeToActive()
        {
            var mp = GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType);

            UpdateMandate(true, mp);

            //add request for readings to fake edsn
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add test data to fake edsn
            var mpDb = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == mp.EanId);
            var externalReference = GetDailyReadingExternalReference(mpDb);
            var readings = P4DataProvider.AddDailyReadingsDetails(ElkEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());
            UpdateMandate(false, mp);

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // assertion
            var mpDbUpd = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDbUpd.Id), 5);
            comHistory.Should().NotBeNull();
            MpStatusHistoryShouldBe(comHistory.Id, P4ProcessStatus.SentToEdsn);

            Waiter.Wait(() => GetStatusHistory(comHistory.Id).Count == 3, 5);
            var statusHistories = GetStatusHistory(comHistory.Id);
            statusHistories.Should().HaveCount(3);
            statusHistories.Should()
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.ReadyToSend)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.SentToEdsn)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.RejectedByEdsn));

            // trigger DailyCommunicationHistoryJob
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // check metering point is active
            new Action(() =>
            {
                Waiter.Wait(() => P4DailyReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(e =>
                    e.EanId == mp.EanId).StatusId == (short)P4MeteringPointStatus.Active);
            }).Should().NotThrow();
        }

        [Test]
        public void DailyCommunicationHistoryJob_DoubleTriggerRequestDailyReadingsJob_CommunicationHistoryHasSingleDataAndReadingDateIsToday()
        {
            var mp = GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType);

            // add metering point            
            UpdateMandate(true, mp);

            // trigger RequestDailyReadingsJob to put CommunicationHistory into DB
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            // check that new CommunicationHistory is in DB and MP status is Active
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id), 20);
            MpStatusHistoryShouldBe(mpDb.Id, P4ProcessStatus.Accepted);
            comHistory.Should().NotBeNull();
            var comHistoryId = comHistory.Id;
            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<StatusHistory>(sh => sh.CommunicationHistoryId == comHistoryId && sh.StatusId == (short)P4ProcessStatus.SemanticRejectedByEdsn));

            // trigger RequestDailyReadingsJob to put CommunicationHistory into DB
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);
            comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id), 20);
            comHistory.ReadingDate.Should().Be(DateTime.Today);
        }
    }
}