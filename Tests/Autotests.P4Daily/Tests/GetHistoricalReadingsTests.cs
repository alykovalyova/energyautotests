﻿using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Base;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;

namespace Autotests.P4Daily.Tests
{
    [TestFixture]
    internal class GetHistoricalReadingsTests : BaseP4DailyReadings
    {
        [Test]
        public void GetHistoricalReadings_DefaultValidFlow_ReadingReceived()
        {
            var mutationDate = DateTime.Today.AddDays(-5);
            var readingDate = DateTime.Today.AddDays(-4);
            var mp = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(mp.EanId, mp.GridOperator, mp.ProductType);
            AddMeteringPointGain(mutationDate, mp);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(mp.EanId);
            var readings = P4DataProvider.AddHistoricalReadings(mp, externalReference, readingDate);
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>() { readings });
            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                mr.EanId == mp.EanId).Count == 1, 15);
            Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeteringPoint>(mr =>
                mr.EanId == mp.EanId && mr.CallHistoricalData));
        }

        [Test, Ignore("https://budget.atlassian.net/browse/UT-4965")]
        public void GetHistoricalReadings_DuplicateReadingReceived_CorrectedStatus()
        {
            var mutationDate = DateTime.Today.AddDays(-5);
            var readingDate = DateTime.Today.AddDays(-4);
            var mp = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(mp.EanId, mp.GridOperator, mp.ProductType);
            AddMeteringPointGain(mutationDate, mp);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(mp.EanId);
            var readings = P4DataProvider.AddHistoricalReadings(mp, externalReference, readingDate);
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>() { readings });
            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                mr.EanId == mp.EanId && mr.StatusId == (int)Enums.MeterReadingStatus.Accepted).Count == 1, 15);
            Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeteringPoint>(mr =>
                mr.EanId == mp.EanId && mr.CallHistoricalData));

            UpdateMandate(false, mp4);
            var duplicateReadings = P4DataProvider.AddHistoricalReadings(mp, externalReference, readingDate);
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>() { duplicateReadings });
            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                mr.EanId == mp.EanId && mr.StatusId == (int)Enums.MeterReadingStatus.Corrected).Count == 1, 15);
        }

        [Test]
        public void GetHistoricalReadings_ReadingAfterLossNotReceived()
        {
            var gainDate = DateTime.Today.AddDays(-5);
            var lossDate = gainDate.AddDays(2);
            var gainReadingDate = gainDate.AddDays(1);
            var lossReadingDate = lossDate.AddDays(2);
            var ean = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(ean.EanId, ean.GridOperator, ean.ProductType);
            AddMeteringPointGain(gainDate, ean);
            AddMeteringPointLoss(lossDate, ean);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(ean.EanId);
            var gainReadings = P4DataProvider.AddHistoricalReadings(ean, externalReference, gainReadingDate);
            var lossReadings = P4DataProvider.AddHistoricalReadings(ean, externalReference, lossReadingDate);
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(
                new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>() { gainReadings, lossReadings });

            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                mr.EanId == ean.EanId && mr.ReadingDate == gainReadingDate).Count == 1);
            Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeteringPoint>(mr =>
                mr.EanId == ean.EanId && mr.CallHistoricalData));
        }

        [Test]
        public void GetHistoricalReadings_GainAndReadingsAreMoreThan2YearsAgo_ReadingsNotRetrieved()
        {
            var gainDate = DateTime.Today.AddYears(-3);
            var gainReadingDate = gainDate.AddDays(1);
            var ean = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(ean.EanId, ean.GridOperator, ean.ProductType);
            AddMeteringPointGain(gainDate, ean);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(ean.EanId);
            var gainReadings = P4DataProvider.AddHistoricalReadings(ean, externalReference, gainReadingDate);
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(
                new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>() { gainReadings });

            UpdateMandate(true, mp4);

            Waiter.Wait(() => !P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                mr.EanId == ean.EanId), 10);
            Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeteringPoint>(mr =>
                mr.EanId == ean.EanId && mr.CallHistoricalData == false));
        }

        [Test]
        public void GetHistoricalReadings_InvalidReadingsFormatSent_ReadingNotSavedToDb()
        {
            var mutationDate = DateTime.Today.AddDays(-5);
            var mp = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(mp.EanId, mp.GridOperator, mp.ProductType);
            AddMeteringPointGain(mutationDate, mp);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(mp.EanId);
            var readings = new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>();
            readings.Add(P4DataProvider.AddHistoricalReadings(mp, externalReference, mutationDate.AddDays(1)));
            foreach (var register in readings.First().Portaal_Content.Portaal_MeteringPoint.Portaal_EnergyMeter.First().Register)
            {
                register.Reading = new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading[]
                {
                    new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                    {
                        Reading = "1.897R"
                    }
                };
            }

            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(readings);
            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                mr.EanId == mp.EanId).Count == 0, 5);
            Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeteringPoint>(mr =>
                mr.EanId == mp.EanId && mr.CallHistoricalData == false));
        }

        [Test]
        public void GetHistoricalReadings_InvalidReadingsValueSent_LastReadingIsSavedToDb()
        {
            var mutationDate = DateTime.Today.AddDays(-5);
            var mp = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(mp.EanId, mp.GridOperator, mp.ProductType);
            AddMeteringPointGain(mutationDate, mp);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(mp.EanId);
            var historicalReadings = new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>();
            for (var dt = mutationDate.AddDays(1); dt < CurrentDate; dt = dt.AddDays(1))
            {
                historicalReadings.Add(P4DataProvider.AddHistoricalReadings(mp, externalReference, dt));
            }

            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(historicalReadings);
            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(
                mr => mr.EanId == mp.EanId).Count == 1, 10);
            Waiter.Wait(() => P4DailyReadingsDb.EntityIsInDb<Repositories.P4DailyReadingsModels.MeteringPoint>(mr =>
                mr.EanId == mp.EanId && mr.CallHistoricalData));
        }
    }
}
