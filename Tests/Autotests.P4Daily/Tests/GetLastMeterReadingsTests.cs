﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.P4DailyReadings.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.P4Daily.Tests
{
    [TestFixture]
    internal class GetLastMeterReadingsTests : BaseP4DailyReadings
    {
        [Test]
        public void GetLastRegularMeterReadings_DefaultValidCase_Returns200()
        {
            var ean = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(CurrentDate, ean);

            var mp = GetMeteringPoint(ean.EanId, ean.GridOperator, ean.ProductType);
            UpdateMandate(true, mp);

            var mp4 = P4DailyReadingsDb.GetEntityByCondition<Repositories.P4DailyReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == ean.EanId);

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4);
            var readings = P4DataProvider.AddDailyReadingsDetails(ean);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);
            Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<Repositories.P4DailyReadingsModels.MeterReading>(
                mr => mr.EanId == mp4.EanId), 15);
            var response = new GetLastMeterReadingsRequest
            {
                Eans = new List<string> { mp4.EanId }
            }.CallWith<GetLastMeterReadingsRequest, GetLastMeterReadingsResponse>(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.MeterReadings.Should().NotBeEmpty();
            response.Data.MeterReadings.Should()
                .OnlyContain(mr => mr.ProductType.Equals(EnergyProductTypeCode.ELK.ToString()));
        }

        [Test]
        public void GetLastHistoricalMeterReadings_DefaultValidCase_Returns200()
        {
            var mutationDate = DateTime.Today.AddDays(-5);
            var readingDate = DateTime.Today.AddDays(-4);
            var mp = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(mp.EanId, mp.GridOperator, mp.ProductType);
            AddMeteringPointGain(mutationDate, mp);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(mp.EanId);
            var readings = P4DataProvider.AddHistoricalReadings(mp, externalReference, readingDate);
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>() { readings });
            UpdateMandate(true, mp4);
            var response = new GetLastMeterReadingsRequest
            {
                Eans = new List<string> { mp4.EanId }
            }.CallWith<GetLastMeterReadingsRequest, GetLastMeterReadingsResponse>(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.MeterReadings.Should().NotBeEmpty();
            response.Data.MeterReadings.Should()
                .OnlyContain(mr => mr.ProductType.Equals(EnergyProductTypeCode.ELK.ToString()));
        }

        [Test]
        public void GetLastMeterReadings_WithEmptyEansList_Returns400()
        {
            var response = new GetLastMeterReadingsRequest
            {
                Eans = new List<string>()
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(m => m.Equals(PatternMessages.P4MpListIsEmpty));
        }

        [Test]
        public void GetLastMeterReadings_EansListIsNull_Returns400()
        {
            var response = new GetLastMeterReadingsRequest().CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, nameof(GetLastMeterReadingsRequest.Eans));
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(m));
        }

        [Test]
        public void GetLastMeterReadings_VerifyEansRegexValidation_Returns400(
            [Values('1', 'a')] char c,
            [Values(0, 19)] int eanLength)
        {
            var ean = new string(c, eanLength);

            var response = new GetLastMeterReadingsRequest
            {
                Eans = new List<string> { ean }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.EanMustMatchRegex, ean) + PatternMessages.EanIdRegex;
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(m));
        }
    }
}