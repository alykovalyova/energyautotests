﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.FakeEdsn.Contracts;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.P4DailyReadings.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.P4Daily.Tests
{
    [TestFixture]
    internal class GetMeterReadingsByDateTests : BaseP4DailyReadings
    {
        [Test]
        public void GetMeterReadingsByDate_DefaultValidCase_Returns200()
        {
            UpdateMandate(true, GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType));
            var mp4 = P4DailyReadingsDb.GetEntityByCondition<Repositories.P4DailyReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == GasEan.EanId);

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4);
            var readings = P4DataProvider.AddDailyReadingsDetails(GasEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mp4.Id), 5);
            comHistory.Should().NotBeNull();

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(
                mr => mr.EanId == GasEan.EanId).Count == 1);

            // get meter readings
            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { mp4.EanId },
                DateFrom = DateTime.Today.AddYears(-1),
                DateTo = DateTime.Today
            }.CallWith<GetMeterReadingsByDateRequest, GetMeterReadingsByDateResponse>(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.MeterReadings.Should().NotBeEmpty();
            response.Data.MeterReadings.Should()
                .OnlyContain(mr => mr.ProductType.Equals(EnergyProductTypeCode.GAS.ToString()));
        }

        [Test]
        public void GetMeterReadingsByDate_GetAllReadings_Returns200()
        {
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));
            var mp4 = P4DailyReadingsDb.GetEntityByCondition<Repositories.P4DailyReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == ElkEan.EanId);

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add daily readings to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4);
            var readings = P4DataProvider.AddDailyReadingsDetails(ElkEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mp4.Id), 5);
            comHistory.Should().NotBeNull();

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(
                mr => mr.EanId == ElkEan.EanId).Count == 1);

            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { mp4.EanId }
            }.CallWith<GetMeterReadingsByDateRequest, GetMeterReadingsByDateResponse>(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.MeterReadings.Should().NotBeEmpty();
            response.Data.MeterReadings.Should()
                .OnlyContain(mr => mr.ProductType.Equals(EnergyProductTypeCode.ELK.ToString()));
        }

        [Test]
        public void GetMeterReadingsByDate_GetHistoricalReadings_Returns200()
        {
            var mutationDate = DateTime.Today.AddMonths(-1);
            var readingStartDate = DateTime.Today.AddDays(-14);
            var mp = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            var mp4 = GetMeteringPoint(mp.EanId, mp.GridOperator, mp.ProductType);
            AddMeteringPointGain(mutationDate, mp);

            //add test data to fake edsn
            var externalReference = GetHistoricalReadingExternalReference(mp.EanId);
            var historicalReadings = new List<GetHistoricalSmartMeterReadingSeriesResponseEnvelope>();
            for (var dt = readingStartDate; dt < CurrentDate; dt = dt.AddDays(1))
            {
                var reading = P4DataProvider.AddHistoricalReadings(mp, externalReference, dt);
                foreach (var register in reading.Portaal_Content.Portaal_MeteringPoint.Portaal_EnergyMeter.First().Register)
                {
                    register.Reading = new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading[]
                    {
                        new GetHistoricalSmartMeterReadingSeriesResponseEnvelope_Portaal_Content_Portaal_MeteringPoint_Portaal_EnergyMeter_Register_Reading()
                        {
                            ReadingDateTime = dt,
                            Reading = (dt.Day + dt.Minute).ToString()
                        }
                    };
                }
                historicalReadings.Add(reading);
            }
            FakeEdsnAdminClient.SaveHistoricalMeterReadingSeries(historicalReadings);
            UpdateMandate(true, mp4);

            Waiter.Wait(() => P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(
                                  mr => mr.EanId == mp.EanId).Count == 14);

            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { mp4.EanId }
            }.CallWith<GetMeterReadingsByDateRequest, GetMeterReadingsByDateResponse>(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.MeterReadings.Should().NotBeEmpty();
            response.Data.MeterReadings.Should()
                .OnlyContain(mr => mr.ProductType.Equals(EnergyProductTypeCode.ELK.ToString()));
        }

        [Test]
        public void GetMeterReadingsByDate_WithEmptyEansList_Returns400()
        {
            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string>(),
                DateFrom = DateTime.Today.AddMonths(-1),
                DateTo = DateTime.Today
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.P4MpListIsEmpty));
        }

        [Test]
        public void GetMeterReadingsByDate_EansListIsNull_Returns400()
        {
            var response = new GetMeterReadingsByDateRequest
            {
                Eans = null,
                DateFrom = DateTime.Today.AddMonths(-1),
                DateTo = DateTime.Today
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, nameof(GetMeterReadingsByDateRequest.Eans));
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(m));
        }

        [Test]
        public void GetMeterReadingsByDate_DateFromLaterThenDateTo_Returns400()
        {
            UpdateMandate(true, GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType));
            var mp4 = P4DailyReadingsDb.GetEntityByCondition<Repositories.P4DailyReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == GasEan.EanId);

            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { mp4.EanId },
                DateFrom = DateTime.Today.AddMonths(-1),
                DateTo = DateTime.Today.AddMonths(-2)
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidDateFrom));
        }

        [Test]
        public void GetMeterReadingsByDate_DateFromLaterThenToday_Returns200ButEmptyList()
        {
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));

            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { ElkEan.EanId },
                DateFrom = DateTime.Today.AddDays(1),
                DateTo = DateTime.Today.AddDays(5)
            }.CallWith<GetMeterReadingsByDateRequest, GetMeterReadingsByDateResponse>(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.MeterReadings.Should().BeEmpty();
        }
    }
}