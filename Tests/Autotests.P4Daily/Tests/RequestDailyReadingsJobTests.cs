﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Base;
using Autotests.P4Daily.Enums;
using FluentAssertions;
using FluentAssertions.Common;
using NUnit.Framework;
using P4MeteringPointSql = Autotests.Repositories.P4DailyReadingsModels.MeteringPoint;

namespace Autotests.P4Daily.Tests
{
    [TestFixture]
    internal class RequestDailyReadingsJobTests : BaseP4DailyReadings
    {
        [Test]
        public void RequestDailyReadingsJob_SendActiveMeteringPoint_MpIsActiveAndHasHistory()
        {
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));
            var mp = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == ElkEan.EanId);

            //add request for readings to fake edsn
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp);
            var readings = P4DataProvider.AddDailyReadingsDetails(ElkEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // assertion
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == ElkEan.EanId && e.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id), 5);
            MpStatusHistoryShouldBe(comHistory.Id, P4ProcessStatus.SentToEdsn);
            comHistory.Should().NotBeNull();
            Waiter.Wait(() => GetStatusHistory(comHistory.Id).Count == 3, 15);
            var statusHistories = GetStatusHistory(comHistory.Id);
            statusHistories.Should().HaveCount(3);
            statusHistories.Should()
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.ReadyToSend)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.SentToEdsn)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.Accepted));

            var meterReadings =
                P4DailyReadingsDb.GetEntitiesByCondition<Repositories.P4DailyReadingsModels.MeterReading>(mr =>
                    mr.EanId == ElkEan.EanId);
            meterReadings.Count.Should().Be(1);
        }

        [Test]
        public void RequestDailyReadingsJob_TrySendInActiveMp_MpIsInActiveAndNoHistory()
        {
            var mp = GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType);

            //inactivate p4 ean
            UpdateMandate(false, mp);

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            // assertion
            var mpUpd = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(metp =>
                metp.EanId == ElkEan.EanId && metp.StatusId == (int)P4MeteringPointStatus.InActive));
            var action = new Action(() => Waiter.Wait(() => GetCommunicationHistory(mpUpd.Id), 5));
            action.Should().Throw<AssertionException>();
        }

        [Test]
        public void RequestDailyReadingsJob_TrySendWithInvalidGridOperator_HistoryHasSemanticRejectedByEdsnStatus()
        {
            var ean = EanInfoProvider.GetRandomGasEan(true, gridOperator: InvalidGridOperator);
            AddMeteringPointGain(CurrentDate, ean);
            var mp = GetMeteringPoint(ean.EanId, ean.GridOperator, ean.ProductType);
            UpdateMandate(true, mp);
            var mp4 = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == mp.EanId);

            //add request for readings to fake edsn
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4);
            var readings = P4DataProvider.AddDailyReadingsDetails(ean, true);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // assertion
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && mp4.StatusId == (int)P4MeteringPointStatus.Active));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id), 5);
            comHistory.Should().NotBeNull();
            Waiter.Wait(() => GetStatusHistory(comHistory.Id).Count == 2, 5);
            var statusHistories = GetStatusHistory(comHistory.Id);
            statusHistories.Should().HaveCount(2);
            statusHistories.Should()
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.ReadyToSend)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.SemanticRejectedByEdsn));
        }

        [Test]
        public void RequestDailyReadingsJob_TrySendRejectedReadings_HistoryHasRejectedByEdsnStatus()
        {
            var mp = GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType);
            UpdateMandate(true, mp);

            //add request for readings to fake edsn
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);
            var mp4 = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(mpd => mpd.EanId == ElkEan.EanId);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp4);
            var readings = P4DataProvider.AddDailyReadingsDetails(ElkEan, false);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // assertion
            var mpDb = Waiter.Wait(() => P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId && e.StatusId == (int)P4MeteringPointStatus.EdsnError, "CommunicationHistory"));
            var comHistory = Waiter.Wait(() => GetCommunicationHistory(mpDb.Id), 5);
            MpStatusHistoryShouldBe(comHistory.Id, P4ProcessStatus.RejectedByEdsn);
            comHistory.Should().NotBeNull();
            Waiter.Wait(() => GetStatusHistory(comHistory.Id).Count == 3, 5);
            var statusHistories = GetStatusHistory(comHistory.Id);
            statusHistories.Should().HaveCount(3);
            statusHistories.Should()
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.ReadyToSend)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.SentToEdsn)).And
                .Contain(h => h.StatusId.IsSameOrEqualTo(P4ProcessStatus.RejectedByEdsn));

            var rejection = GetEdsnRejection(statusHistories.Last().Id);
            //rejection
        }
    }
}