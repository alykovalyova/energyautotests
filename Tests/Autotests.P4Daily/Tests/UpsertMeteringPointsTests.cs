﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Daily.Base;
using Autotests.P4Daily.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.P4DailyReadings.Contract;
using P4MeteringPoint = Nuts.P4DailyReadings.Contract.MeteringPoint;
using P4MeteringPointSql = Autotests.Repositories.P4DailyReadingsModels.MeteringPoint;

namespace Autotests.P4Daily.Tests
{
    [TestFixture]
    internal class UpsertMeteringPointsTests : BaseP4DailyReadings
    {
        [Test]
        public void UpsertMeteringPoints_DefaultValidCase_Returns200()
        {
            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType),
                    GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType)
                },
                MandateEnabled = true
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            var mpGasDb = P4DailyReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == GasEan.EanId);
            var mpElkDb = P4DailyReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == ElkEan.EanId);
            MpStatusShouldBe(mpGasDb, P4MeteringPointStatus.Active);
            MpStatusHistoryShouldBe(mpGasDb.Id, P4ProcessStatus.Accepted);
            MpStatusShouldBe(mpElkDb, P4MeteringPointStatus.Active);
            MpStatusHistoryShouldBe(mpElkDb.Id, P4ProcessStatus.Accepted);
        }

        [Test]
        public void UpsertMeteringPoints_DeactivateByDates_Returns200()
        {
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));
            var mp4 = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == ElkEan.EanId);
            MpStatusShouldBe(mp4, P4MeteringPointStatus.Active);
            MpStatusHistoryShouldBe(mp4.Id, P4ProcessStatus.Accepted);

            // change StartDate and add same metering point with other StartDate to deactivate it
            P4DailyReadingsDb.UpdateSingleEntity<P4MeteringPointSql>(
                m => m.EanId.Equals(mp4.EanId), m => m.CreatedOn = DateTime.Today.AddDays(-5));
            UpdateMandate(false, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));

            // check metering point is inActive
            var mpDb = P4DailyReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp4.EanId);
            MpStatusShouldBe(mpDb, P4MeteringPointStatus.InActive);
            MpStatusHistoryShouldBe(mpDb.Id, P4ProcessStatus.Accepted);
        }

        [Test]
        public void UpsertMeteringPoints_InActivateMeteringPoint_StatusIsInActive()
        {
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));
            var mp = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == ElkEan.EanId, "GridOperator");

            //add request for readings to fake edsn
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_RequestDailyReadingsJob);

            //add test data to fake edsn
            var externalReference = GetDailyReadingExternalReference(mp);
            var readings = P4DataProvider.AddDailyReadingsDetails(ElkEan);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            // trigger job
            Scheduler.TriggerJob(QuartzJobName.Energy_P4Daily_DailyCommunicationHistoryJob);

            // check metering point is active
            var mpDb = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId);
            MpStatusShouldBe(mpDb, P4MeteringPointStatus.Active);
            MpStatusHistoryShouldBe(mpDb.Id, P4ProcessStatus.Accepted);

            // change StartDate and add same metering point to deactivate it
            P4DailyReadingsDb.UpdateSingleEntity<P4MeteringPointSql>(
                m => m.EanId.Equals(mp.EanId), m => m.CreatedOn = DateTime.Today.AddDays(-5));
            UpdateMandate(false, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));

            // check metering point is inActive
            var mp4 = P4DailyReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(e =>
                e.EanId == mp.EanId);
            MpStatusShouldBe(mp4, P4MeteringPointStatus.InActive);
            MpStatusHistoryShouldBe(mp4.Id, P4ProcessStatus.Accepted);

            //set metering point back to active
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));
        }

        [Test]
        public void UpsertMeteringPoints_WithNotUniqueEan_Returns400()
        {
            UpdateMandate(true, GetMeteringPoint(ElkEan.EanId, ElkEan.GridOperator, ElkEan.ProductType));
            var mp = P4DailyReadingsDb.GetEntityByCondition<P4MeteringPointSql>(m => m.EanId == ElkEan.EanId, "GridOperator");

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(mp.EanId, mp.GridOperator.EanId, (EnergyProductTypeCode)mp.ProductTypeId),
                    GetMeteringPoint(mp.EanId, mp.GridOperator.EanId, (EnergyProductTypeCode)mp.ProductTypeId)
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.EanNotUnique));
        }

        [Test]
        public void UpsertMeteringPoints_WithEmptyMeteringPointsList_Returns400()
        {
            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>()
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.P4MpListIsEmpty));
        }

        [Test]
        public void UpsertMeteringPoints_VerifyAllRequiredValidations_Returns400()
        {
            var errors = new List<string>
            {
                string.Format(PatternMessages.FieldIsRequired, nameof(P4MeteringPoint.EanId)),
                string.Format(PatternMessages.FieldIsRequired, nameof(P4MeteringPoint.GridOperator)),
                string.Format(PatternMessages.FieldIsRequired, nameof(P4MeteringPoint.BalanceSupplier)),
                string.Format(PatternMessages.FieldIsRequired, nameof(P4MeteringPoint.ProductType)),
                PatternMessages.InvalidEnergyProductType
            };

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(null, null, null, null),
                },
                MandateEnabled = true
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().BeEquivalentTo(errors);
        }

        [Test]
        public void UpsertMeteringPoints_WithoutMeteringPointsList_Returns400()
        {
            var response = new UpsertMandateRequest().CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            var m = string.Format(PatternMessages.FieldIsRequired, nameof(UpsertMandateRequest.MeteringPoints));
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(m));
        }

        [Test]
        public void UpsertMeteringPoints_VerifyEanLengthValidation_Returns400()
        {
            var ean = new string('1', 19);

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(ean, ElkEan.GridOperator, ElkEan.ProductType),
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidEanField));
        }

        [Test]
        public void UpsertMeteringPoints_VerifyEanCharValidation_Returns400()
        {
            var ean = new string('a', 18);

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(ean, GasEan.GridOperator, GasEan.ProductType),
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidEanField));
        }

        [Test]
        public void UpsertMeteringPoints_VerifyGridOperatorLength_Returns400()
        {
            var gridOperator = new string('1', 14);

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(GasEan.EanId, gridOperator,  GasEan.ProductType),
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidGridOperatorField));
        }

        [Test]
        public void UpsertMeteringPoints_VerifyGridOperatorCharValidation_Returns400()
        {
            var gridOperator = new string('a', 13);

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(GasEan.EanId, gridOperator, GasEan.ProductType),
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidGridOperatorField));
        }

        [Test]
        public void UpsertMeteringPoints_VerifySupplierLength_Returns400()
        {
            var supplier = new string('1', 14);

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(GasEan.EanId, GasEan.GridOperator, GasEan.ProductType, supplier),
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidSupplierField));
        }

        [Test]
        public void UpsertMeteringPoints_VerifySupplierCharValidation_Returns400()
        {
            var supplier = new string('a', 13);
            var mpGas = GetMeteringPoint("", "", EnergyProductTypeCode.ELK);

            var response = new UpsertMandateRequest
            {
                MeteringPoints = new List<P4MeteringPoint>
                {
                    GetMeteringPoint(mpGas.EanId, mpGas.GridOperator, EnergyProductTypeCode.GAS, supplier),
                }
            }.CallWith(P4DailyClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(e => e.Equals(PatternMessages.InvalidSupplierField));
        }
    }
}