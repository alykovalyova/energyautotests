﻿using System.Net;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Dispatchers;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Enums;
using Autotests.Repositories.CcuModels;
using Autotests.Repositories.ConsumptionModels;
using Autotests.Repositories.MeteringPointModels;
using Autotests.Repositories.P4EventReadingsModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MeteringPoint.Queue.Contract.SmartMeter;
using Nuts.P4EventReadings.Contract;
using CommunicationHistory = Autotests.Repositories.P4EventReadingsModels.CommunicationHistory;
using EdsnRejection = Autotests.Repositories.P4EventReadingsModels.EdsnRejection;
using EdsnRejectionLevel = Autotests.P4Event.Enums.EdsnRejectionLevel;
using EnergyProductTypeCode = Autotests.P4Event.Enums.EnergyProductTypeCode;
using GridOperator = Autotests.Repositories.P4EventReadingsModels.GridOperator;
using MeterReading = Autotests.Repositories.P4EventReadingsModels.MeterReading;
using P4MeteringPointSql = Autotests.Repositories.P4EventReadingsModels.MeteringPoint;
using RegisterReading = Autotests.Repositories.P4EventReadingsModels.RegisterReading;
using StatusHistory = Autotests.Repositories.P4EventReadingsModels.StatusHistory;
using EnumSource = Autotests.P4Event.Enums.EnumSource;
using P4EventMeteringPoint = Autotests.Repositories.P4EventReadingsModels.MeteringPoint;
using P4EventProcessStatus = Autotests.P4Event.Enums.P4EventProcessStatus;
using MarketEvent = Autotests.P4Event.Enums.MarketEvent;

namespace Autotests.P4Event.Base
{
    [SetUpFixture, Category(Core.Helpers.Categories.P4EventReadings)]
    internal abstract class BaseP4EventReadings : AllureReport
    {
        //clients declaration
        protected NutsHttpClient P4EventClient;
        protected RestSchedulerClient Scheduler;
        protected FakeEdsnAdminClient FakeEdsnAdminClient;
        
        //helpers declaration
        protected Dispatcher Dispatcher;
        protected SupplyPhaseEntitiesProvider SupplyPhaseEntitiesProvider;
        protected CcuEntitiesProvider CcuEntityProvider;

        //databases declaration
        protected DbHandler<P4EventReadingsContext> P4EventReadingsDb;
        protected DbHandler<MeteringPointContext> MeteringPointDb;
        protected DbHandler<CommercialCharacteristicContext> CcuDb;
        protected DbHandler<ConsumptionContext> ConsumptionDb;

        //variables declaration
        private ConfigHandler _configHandler = new ConfigHandler();
        protected int countOfMessages;
        protected readonly int timeForDatabaseUpdating = 8;
        protected readonly int value = 1;
        protected readonly string testEan = "111425200000006103";

        protected readonly List<string> testGridOperatorsToDelete = new List<string>()
        {
            TestConstants.GridOperator
        };

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            countOfMessages = 0;
            P4EventClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.P4Event));
            Scheduler = new RestSchedulerClient();
            FakeEdsnAdminClient = new FakeEdsnAdminClient();
            SupplyPhaseEntitiesProvider = new SupplyPhaseEntitiesProvider();
            CcuEntityProvider = new CcuEntitiesProvider();
            FakeEdsnAdminClient.ClearAll();
            Dispatcher = new Dispatcher(DispatcherName.MeteringPoint, ConfigHandler.Instance.QueueSettings.HostNameQueue);
            P4EventReadingsDb = new DbHandler<P4EventReadingsContext>(_configHandler.GetConnectionString(DbConnectionName.P4EventReadingsDatabase.ToString()));
            MeteringPointDb = new DbHandler<MeteringPointContext>(_configHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
            CcuDb = new DbHandler<CommercialCharacteristicContext>(_configHandler.GetConnectionString(DbConnectionName.CcuDatabase.ToString()));
            ConsumptionDb = new DbHandler<ConsumptionContext>(_configHandler.GetConnectionString(DbConnectionName.ConsumptionDatabase.ToString()));
            DeleteTestData();
        }

        [TearDown]
        public void TearDown() => DeleteTestData();

        protected void AddGridOperator(string gridOperator)
        {
            var gridOperatorToAdd = new GridOperator()
            {
                EanId = gridOperator
            };

            P4EventReadingsDb.AddEntityToDb(gridOperatorToAdd);
        }

        protected P4EventMeteringPoint CreateRequest(DateTime date, MarketEvent marketEvent = MarketEvent.Spontaneous,
            EnumSource source = EnumSource.ProcessRunner, params EanInfo[] eans)
        {
            var response = new CreateRequestRequest()
            {
                Eans = eans.Select(e => e.EanId).ToList<string>(),
                MarketEvent = marketEvent.ToString(),
                QueryDate = date,
                RequestDate = date,
                Source = source.ToString()
            }.CallWith(P4EventClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            var mp = Waiter.Wait(() => P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == eans.FirstOrDefault().EanId && p4.StatusId == (int)MeteringPointStatus.Active, "CommunicationHistories"));

            return mp;
        }

        protected void AddMeteringPointGain(DateTime date, params EanInfo[] eans)
        {
            foreach (var ean in eans)
            {
                SupplyPhaseEntitiesProvider.AddSupplyPhase(ean.EanId, date, productType: ean.ProductType.ToString(),
                    gridOperator: ean.GridOperator, isSmartMeter: true, energyMeter: ean.EnergyMeterId);
                var ccuRequest = CcuEntityProvider.CreateCommercialCharacteristic(ean.EanId,
                    TestConstants.OtherPartyBalanceSupplier, mutationDate: date);
                FakeEdsnAdminClient.SendInitialCommercialCharacteristic(ccuRequest);
                CcuEntityProvider.CallMoveIn(ean.EanId, TestConstants.BudgetSupplier, date);

                Waiter.Wait(() => CcuDb.GetEntityByCondition<Repositories.CcuModels.CcuHistory>(ccu => ccu.EanId == ean.EanId));
                Waiter.Wait(() => MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(m => m.EanId == ean.EanId));
            }
        }

        protected string GetP4EventReadingExternalReference(P4MeteringPointSql ean)
        {
            var externalReference = $"P4EID_{ean.CommunicationHistories.Last().Id}";
            Waiter.Wait(() =>
                P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.CommunicationHistory>(ch =>
                    ch.MeteringPointId == ean.Id && ch.ExternalReference == externalReference));
            return externalReference;
        }

        protected void AddMeteringPoint(DateTime creationDate, string gridOperator)
        {
            var meteringPointToAdd = new P4MeteringPointSql
            {
                EanId = testEan,
                CreatedOn = creationDate,
                GridOperatorId = GetGridOperatorId(gridOperator),
                ProductTypeId = (short)Enum.Parse<EnergyProductTypeCode>(TestConstants.ElkProductType),
                StatusId = (short)Enum.Parse<MeteringPointStatus>(TestConstants.MeteringPointStatus)
            };

            P4EventReadingsDb.AddEntityToDb(meteringPointToAdd);
        }

        protected void AddCommunicationHistory(DateTime creationDate)
        {
            var communicationHistoryToAdd = new CommunicationHistory
            {
                MeteringPointId = P4EventReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(mp => mp.EanId == testEan).Id,
                ReadingDate = creationDate.Date,
                RequestDate = creationDate.Date,
                CreatedOn = creationDate,
                BalanceSupplierId = GetBalanceSupplierId(TestConstants.BudgetSupplier),
                ExternalReference = TestConstants.ExternalReference
            };

            P4EventReadingsDb.AddEntityToDb(communicationHistoryToAdd);
        }

        protected void AddStatusHistory(DateTime creationDate, Guid xmlMessageId, P4EventProcessStatus status)
        {
            var statusHistoryToAdd = new StatusHistory
            {
                CreatedOn = creationDate,
                StatusId = (short)status,
                XmlCreationTs = creationDate,
                XmlMessageId = xmlMessageId,
                CommunicationHistoryId = P4EventReadingsDb.GetSingleEntityByCondition<CommunicationHistory>(ch => ch.ExternalReference == TestConstants.ExternalReference).Id
            };

            P4EventReadingsDb.AddEntityToDb(statusHistoryToAdd);
        }

        protected void AddRejection(Guid xmlMessageId, RejectionCode code)
        {
            var rejectionToAdd = new EdsnRejection
            {
                CodeId = (short)code,
                Message = TestConstants.RejectionMessage,
                StatusHistoryId = P4EventReadingsDb.GetSingleEntityByCondition<StatusHistory>(sh => sh.XmlMessageId == xmlMessageId).Id,
                RejectionLevelId = (short)Enum.Parse<EdsnRejectionLevel>(TestConstants.RejectionLevel)
            };

            P4EventReadingsDb.AddEntityToDb(rejectionToAdd);
        }

        protected P4MeteringPointSql GetMeteringPoint()
        {
            return P4EventReadingsDb.GetSingleEntityByCondition<P4MeteringPointSql>(mp => mp.EanId == testEan);
        }

        protected CommunicationHistory GetCommunicationHistory()
        {
            return P4EventReadingsDb.GetSingleEntityByCondition<CommunicationHistory>(ch => ch.ExternalReference == TestConstants.ExternalReference);
        }

        protected List<CommunicationHistory> GetCommunicationHistories(int meteringPointId)
        {
            return P4EventReadingsDb.GetEntitiesByCondition<CommunicationHistory>(ch => ch.MeteringPointId == meteringPointId);
        }

        protected List<StatusHistory> GetStatusHistoriesByCommHistoryId(int commHistoryId)
        {
            return P4EventReadingsDb.GetEntitiesByCondition<StatusHistory>(sh => sh.CommunicationHistoryId == commHistoryId);
        }

        protected List<Repositories.P4EventReadingsModels.Source> GetSourcesByCommHistoryId(int commHistoryId)
        {
            return P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.Source>(s => s.CommunicationHistoryId == commHistoryId);
        }

        protected int GetGridOperatorId(string gridOperator)
        {
            return P4EventReadingsDb.GetSingleEntityByCondition<GridOperator>(g => g.EanId == gridOperator).Id;
        }

        protected int GetBalanceSupplierId(string balanceSupplier)
        {
            return P4EventReadingsDb.GetSingleEntityByCondition<BalanceSupplier>(b => b.EanId == balanceSupplier).Id;
        }

        protected void DispatchSmartMeterInfoRequest(EanInfo eanInfo, DateTime date, MarketEvent marketEvent)
        {
            var request = new SmartMeterInfo()
            {
                MarketEvent = marketEvent.ToString(),
                QueryDate = date,
                SmartMeterData = new List<SmartMeterData>()
                {
                    new SmartMeterData()
                    {
                        BalanceSupplier = TestConstants.BudgetSupplier,
                        Ean = eanInfo.EanId,
                        EdsnMeterId = eanInfo.EnergyMeterId,
                        GridOperator = eanInfo.GridOperator,
                        ProductType = eanInfo.ProductType.ToString()
                    }
                }
            };

            Dispatcher.DispatchMessage(request);
        }

        protected void DeleteTestData()
        {
            var gridOperatorsToDelete = P4EventReadingsDb.GetEntitiesByCondition<GridOperator>(g => testGridOperatorsToDelete.Contains(g.EanId));
            var meteringPointsToDelete = P4EventReadingsDb.GetEntitiesByCondition<P4MeteringPointSql>(mp => mp.EanId == testEan || gridOperatorsToDelete.Select(g => g.Id).Contains(mp.GridOperatorId) || mp.EanId.StartsWith("000"));
            var commHistoriesToDelete = P4EventReadingsDb.GetEntitiesByCondition<CommunicationHistory>(ch => meteringPointsToDelete.Select(mp => mp.Id).Contains(ch.MeteringPointId));
            var meterReadingsToDelete = P4EventReadingsDb.GetEntitiesByCondition<MeterReading>(mr => mr.ExternalReference == TestConstants.ExternalReference);
            var registerReadingsToDelete = P4EventReadingsDb.GetEntitiesByCondition<RegisterReading>(rr => meterReadingsToDelete.Select(mr => mr.Id).Contains(rr.MeterReadingId));
            var statusHistoriesToDelete = P4EventReadingsDb.GetEntitiesByCondition<StatusHistory>(sh => commHistoriesToDelete.Select(ch => ch.Id).Contains(sh.CommunicationHistoryId));
            var rejectionsToDelete = P4EventReadingsDb.GetEntitiesByCondition<EdsnRejection>(r => statusHistoriesToDelete.Select(sh => sh.Id).Contains(r.StatusHistoryId));
            var sourcesToDelete = P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.Source>(s => commHistoriesToDelete.Select(ch => ch.Id).Contains(s.CommunicationHistoryId));

            P4EventReadingsDb.DeleteEntities(rejectionsToDelete);
            P4EventReadingsDb.DeleteEntities(statusHistoriesToDelete);
            P4EventReadingsDb.DeleteEntities(registerReadingsToDelete);
            P4EventReadingsDb.DeleteEntities(meterReadingsToDelete);
            P4EventReadingsDb.DeleteEntities(sourcesToDelete);
            P4EventReadingsDb.DeleteEntities(commHistoriesToDelete);
            P4EventReadingsDb.DeleteEntities(meteringPointsToDelete);
            P4EventReadingsDb.DeleteEntities(gridOperatorsToDelete);
        }
    }
}
