﻿namespace Autotests.P4Event.Enums
{
    public enum EdsnRejectionLevel
    {
        /// <summary>
        /// Edsn
        /// </summary>
        Edsn = 1,

        /// <summary>
        /// GridOperator
        /// </summary>
        GridOperator = 2,

        /// <summary>
        /// Semantic
        /// </summary>
        Semantic = 3
    }
}
