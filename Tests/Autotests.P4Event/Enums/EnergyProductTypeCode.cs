﻿namespace Autotests.P4Event.Enums
{
    public enum EnergyProductTypeCode
    {
        /// <summary>
        /// ELK
        /// </summary>
        ELK = 1,

        /// <summary>
        /// GAS
        /// </summary>
        GAS = 2,

        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 3
    }
}
