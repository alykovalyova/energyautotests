﻿namespace Autotests.P4Event.Enums
{
    public enum EnumSource
    {
        /// <summary>
        /// TariffChange
        /// </summary>
        TariffChange = 1,

        /// <summary>
        /// Monthly
        /// </summary>
        Monthly = 2,

        /// <summary>
        /// ProcessRunner
        /// </summary>
        ProcessRunner = 3
    }
}
