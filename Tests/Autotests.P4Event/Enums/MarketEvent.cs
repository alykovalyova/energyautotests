﻿namespace Autotests.P4Event.Enums
{
    public enum MarketEvent
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// MoveIn
        /// </summary>
        MoveIn = 1,

        /// <summary>
        /// Switch
        /// </summary>
        Switch = 2,

        /// <summary>
        /// MoveOut
        /// </summary>
        MoveOut = 3,

        /// <summary>
        /// EndOfSupply
        /// </summary>
        EndOfSupply = 4,

        /// <summary>
        /// Yearly
        /// </summary>
        Yearly = 5,

        /// <summary>
        /// ExchangeOfMeter
        /// </summary>
        ExchangeOfMeter = 6,

        /// <summary>
        /// TwoMonthly
        /// </summary>
        TwoMonthly = 7,

        /// <summary>
        /// Dispute
        /// </summary>
        Dispute = 8,

        /// <summary>
        /// Historical
        /// </summary>
        Historical = 9,

        /// <summary>
        /// Physical
        /// </summary>
        Physical = 10,

        /// <summary>
        /// Renewal
        /// </summary>
        Renewal = 11,

        /// <summary>
        /// VolumeRegisterReading
        /// </summary>
        VolumeRegisterReading = 12,

        /// <summary>
        /// Settlement
        /// </summary>
        Settlement = 13,

        /// <summary>
        /// Spontaneous
        /// </summary>
        Spontaneous = 14,

        /// <summary>
        /// EndOfMeterSupport
        /// </summary>
        EndOfMeterSupport = 15,

        /// <summary>
        /// BeginOfMeterSupport
        /// </summary>
        BeginOfMeterSupport = 16,

        /// <summary>
        /// TariffChange
        /// </summary>
        TariffChange = 17,

        /// <summary>
        /// Monthly
        /// </summary>
        Monthly = 18
    }
}