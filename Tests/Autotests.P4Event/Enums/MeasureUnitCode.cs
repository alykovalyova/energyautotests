﻿namespace Autotests.P4Event.Enums
{
    internal enum MeasureUnitCode
    {
        /// <summary>
        /// KVR
        /// </summary>
        KVR = 1, // kVAR De voltampère reactief (var) is de eenheid van reactief vermogen (Pr) ook wel bekend als Blind vermogen (Pbl)

        /// <summary>
        /// KWH
        /// </summary>
        KWH = 2, // kilo Watt hour (kWh)

        /// <summary>
        /// KWT
        /// </summary>
        KWT = 3, // kilo Watt (kW)

        /// <summary>
        /// M3N
        /// </summary>
        M3N = 4, // Normal Cubic Metre

        /// <summary>
        /// MTQ
        /// </summary>
        MTQ = 5, // Cubic Metre

        /// <summary>
        /// WH
        /// </summary>
        WH = 6, // Watt hour

        /// <summary>
        /// DM3
        /// </summary>
        DM3 = 7, // Cubic Decimetre

        /// <summary>
        /// MJ
        /// </summary>
        MJ = 8, // MegaJoule

        /// <summary>
        /// M3N3517
        /// </summary>
        M3N3517 = 9, // Groningen-gas equivalent is m3(n;35,17)

        /// <summary>
        /// M3N3517HR
        /// </summary>
        M3N3517HR = 10 // Gasafname in m3(n;35,17)/uur
    }
}
