﻿namespace Autotests.P4Event.Enums
{
    public enum MeteringPointStatus
    {
        /// <summary>
        /// Active
        /// </summary>
        Active = 1,

        /// <summary>
        /// InActive
        /// </summary>
        InActive = 2,

        /// <summary>
        /// Error
        /// </summary>
        EdsnError = 3,

        /// <summary>
        /// Application
        /// </summary>
        Application = 4,

        /// <summary>
        /// MalfunctionKnown
        /// </summary>
        MalfunctionKnown = 5
    }
}
