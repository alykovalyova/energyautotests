﻿namespace Autotests.P4Event.Enums
{
    public enum P4EventProcessStatus
    {
        /// <summary>
        /// ReadyToSend
        /// </summary>
        ReadyToSend = 1,

        /// <summary>
        /// SentToEdsn
        /// </summary>
        SentToEdsn = 2,

        /// <summary>
        /// NoResponse
        /// </summary>
        NoResponse = 3,

        /// <summary>
        /// IncompleteData
        /// </summary>
        IncompleteData = 4,

        /// <summary>
        /// Accepted
        /// </summary>
        Accepted = 5,

        /// <summary>
        /// RejectedByEdsn
        /// </summary>
        RejectedByEdsn = 6,

        /// <summary>
        /// SemanticRejectedByEdsn
        /// </summary>
        SemanticRejectedByEdsn = 7,

        /// <summary>
        /// RetriedToSend
        /// </summary>
        RetriedToSend = 8,

        /// <summary>
        /// Rejected
        /// </summary>
        Rejected = 9
    }
}
