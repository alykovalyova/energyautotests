﻿namespace Autotests.P4Event.Enums
{
    public enum RejectionCode
    {
        /// <summary>
        /// NotAuthorized
        /// </summary>
        NotAuthorized = 0,

        /// <summary>
        /// IncorrectValue
        /// </summary>
        IncorrectValue = 1,

        /// <summary>
        /// AuthenticationError
        /// </summary>
        AuthenticationError = 3,

        /// <summary>
        /// EanUnknownOnRequestDate
        /// </summary>
        EanUnknownOnRequestDate = 6,

        /// <summary>
        /// NoSmartMeterOnRequestDate
        /// </summary>
        NoSmartMeterOnRequestDate = 7,

        /// <summary>
        /// RequestingPartyNotAuthorized
        /// </summary>
        RequestingPartyNotAuthorized = 8,

        /// <summary>
        /// SmartMeterInDeployment
        /// </summary>
        SmartMeterInDeployment = 9,

        /// <summary>
        /// RequestDateInTheFuture
        /// </summary>
        RequestDateInTheFuture = 10,

        /// <summary>
        /// RequestDateToOld
        /// </summary>
        RequestDateToOld = 11,

        /// <summary>
        /// RequestDataNotAvailable
        /// </summary>
        RequestDataNotAvailable = 12,

        /// <summary>
        /// RequestCouldNotBeProcessedByMeter
        /// </summary>
        RequestCouldNotBeProcessedByMeter = 13,

        /// <summary>
        /// MeterIsNotFunctioning
        /// </summary>
        MeterIsNotFunctioning = 14,

        /// <summary>
        /// ProductTypeAndRequestDontMatch
        /// </summary>
        ProductTypeAndRequestDontMatch = 22,

        /// <summary>
        /// ExecutionOfRequestFailedTechnicalProblem
        /// </summary>
        ExecutionOfRequestFailedTechnicalProblem = 23,

        /// <summary>
        /// SwitchingResultUnknown
        /// </summary>
        SwitchingResultUnknown = 25,

        /// <summary>
        /// MeterStatusCodeRed
        /// </summary>
        MeterStatusCodeRed = 26,

        /// <summary>
        /// ResultDataRequestControlCommandUnknown
        /// </summary>
        ResultDataRequestControlCommandUnknown = 31,

        /// <summary>
        /// ThisCodeNotAllowed
        /// </summary>
        ThisCodeNotAllowed = 35,

        /// <summary>
        /// EanSubGridOperatorUnknown
        /// </summary>
        EanSubGridOperatorUnknown = 36,

        /// <summary>
        /// MessageCannotBeDelivered
        /// </summary>
        MessageCannotBeDelivered = 37,

        /// <summary>
        /// SmartMeterSwitchedOff
        /// </summary>
        SmartMeterSwitchedOff = 38,

        /// <summary>
        /// MeterIsNotReachableFromDistance
        /// </summary>
        MeterIsNotReachableFromDistance = 39,

        /// <summary>
        /// WeAreNoLongerMandatedToRetrieveTheMeterreadingsForThisEan
        /// </summary>
        WeAreNoLongerMandatedToRetrieveTheMeterreadingsForThisEan = 40,

        /// <summary>
        /// MeterdataIsntAvailableLargeScaleShutdown
        /// </summary>
        MeterdataIsntAvailableLargeScaleShutdown = 41
    }
}
