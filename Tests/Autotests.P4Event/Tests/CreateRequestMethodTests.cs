﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Base;
using Autotests.P4Event.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.P4EventReadings.Contract;

namespace Autotests.P4Event.Tests
{
    [TestFixture]
    internal class CreateRequestMethodTests : BaseP4EventReadings
    {
        [Test]
        public void CreateRequestMethod_WithInvalidEanId_BadRequest()
        {
            var invalidEanId = "12345";

            var response = new CreateRequestRequest
            {
                Eans = new List<string> { invalidEanId },
                QueryDate = DateTime.Now.AddDays(-3),
                RequestDate = DateTime.Now,
                MarketEvent = MarketEvent.MoveOut.ToString(),
                Source = EnumSource.ProcessRunner.ToString()
            }.CallWith(P4EventClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.EanMustMatchRegex, invalidEanId) + PatternMessages.EanIdRegex);
        }

        [Test]
        public void CreateRequestMethod_WithEmptyListOfEans_BadRequest()
        {
            var response = new CreateRequestRequest
            {
                Eans = new List<string>(),
                QueryDate = DateTime.Now.AddDays(-3),
                RequestDate = DateTime.Now,
                MarketEvent = MarketEvent.EndOfSupply.ToString(),
                Source = EnumSource.ProcessRunner.ToString()
            }.CallWith(P4EventClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EansListIsEmpty);
        }

        [Test]
        public void CreateRequestMethod_WithoutParameters_BadRequest()
        {
            var response = new CreateRequestRequest().CallWith(P4EventClient);

            response.Header.StatusCodeShouldBe(System.Net.HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EansIsRequired);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.SourceIsRequired);
        }

        [Test]
        public void CreateRequestMethod_RequestDateIsEarlierThanToday_BadRequest()
        {
            var response = new CreateRequestRequest
            {
                Eans = new List<string> { testEan },
                QueryDate = DateTime.Now,
                RequestDate = DateTime.Now.AddDays(-3),
                MarketEvent = MarketEvent.EndOfSupply.ToString(),
                Source = EnumSource.ProcessRunner.ToString()
            }.CallWith(P4EventClient);

            response.Header.StatusCodeShouldBe(System.Net.HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.RequestDateIsOld);
        }

        [Test]
        public void CreateRequestMethod_QueryDateIsEarlierThan39Days_BadRequest()
        {
            var response = new CreateRequestRequest
            {
                Eans = new List<string> { testEan },
                QueryDate = DateTime.Now.AddDays(-40),
                RequestDate = DateTime.Today,
                MarketEvent = MarketEvent.Physical.ToString(),
                Source = EnumSource.ProcessRunner.ToString()
            }.CallWith(P4EventClient);

            response.Header.StatusCodeShouldBe(System.Net.HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.QueryDateIsOld);
        }

        [Test]
        public void CreateRequestMethod_DuplicatedEans_BadRequest()
        {
            var date = DateTime.Now.AddHours(-1);
            var ean = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, ean);

            var response = new CreateRequestRequest
            {
                Eans = new List<string> { ean.EanId, ean.EanId },
                QueryDate = date,
                RequestDate = DateTime.Now,
                MarketEvent = MarketEvent.Monthly.ToString(),
                Source = EnumSource.ProcessRunner.ToString()
            }.CallWith(P4EventClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.DuplicatedEans);
        }

        [Test]
        public void CreateRequestMethod_ValidCaseWithAddingOfCommunicationHistory_OK()
        {
            var date = DateTime.Today;
            var eanInfo = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            var mp = CreateRequest(date, eans: eanInfo);

            var commHistories = GetCommunicationHistories(mp.Id);
            commHistories.Count.Should().Be(1);
            var statusHistories = GetStatusHistoriesByCommHistoryId(commHistories.First().Id);
            statusHistories.Count.Should().Be(1);
            statusHistories.First().StatusId.Should().Be((short)P4EventProcessStatus.ReadyToSend);
            var sources = GetSourcesByCommHistoryId(commHistories.First().Id);
            sources.Count.Should().Be(1);
            sources.First().SourceId.Should().Be((short)EnumSource.ProcessRunner);
        }

        [Test]
        public void CreateRequestMethod_ValidCaseWithExistingCommunicationHistory_OK()
        {
            var date = DateTime.Today;
            var source = EnumSource.ProcessRunner;
            var eanInfo = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            CreateRequest(date, source: source, eans: eanInfo);
            var mp = CreateRequest(date, source: source, eans: eanInfo);

            var commHistories = GetCommunicationHistories(mp.Id);
            commHistories.Count.Should().Be(1);
            var statusHistories = GetStatusHistoriesByCommHistoryId(commHistories.First().Id);
            statusHistories.Count.Should().Be(1);
            var sources = GetSourcesByCommHistoryId(commHistories.First().Id);
            sources.Count.Should().Be(1);
            sources.First().SourceId.Should().Be((short)source);
        }
    }
}
