﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.P4EventReadings.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.P4Event.Tests
{
    [TestFixture]
    internal class GetMeterReadingsMethodTests : BaseP4EventReadings
    {
        [Test]
        public void GetMeterReadingsMethod_WithInvalidEanId_BadRequest()
        {
            var invalidEanId = "1122334";
            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { invalidEanId },
                ReadingDate = DateTime.Now
            }.CallWith(P4EventClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.EanMustMatchRegex, invalidEanId) + PatternMessages.EanIdRegex);
        }

        [Test]
        public void GetMeterReadingsMethod_WithEmptyListOfEans_BadRequest()
        {
            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string>(),
                ReadingDate = DateTime.Now
            }.CallWith(P4EventClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EansListIsEmpty);
        }

        [Test]
        public void GetMeterReadingsMethod_WithoutParameters_BadRequest()
        {
            var response = new GetMeterReadingsByDateRequest().CallWith(P4EventClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.EansIsRequired);
        }

        [Test]
        public void GetMeterReadingsMethod_AcceptedReading_OK()
        {
            var date = DateTime.Today;
            var eanInfo = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            var mp = CreateRequest(date, eans: eanInfo);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);
            var externalReference = GetP4EventReadingExternalReference(mp);
            var readings = P4DataProvider.AddDailyReadingsDetails(eanInfo);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RequestingMeterReadingsJob);
            Waiter.Wait(() => P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.MeterReading>(p4 =>
                p4.EanId == eanInfo.EanId).Count == 1, 200);

            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { eanInfo.EanId },
                ReadingDate = date.Date
            }.CallWith<GetMeterReadingsByDateRequest, GetMeterReadingsByDateResponse>(P4EventClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.Should().NotBeNull();
            response.Data.MeterReadings.Should().NotBeNullOrEmpty();

            var meterReading = response.Data.MeterReadings.First();

            meterReading.EanId.Should().Be(eanInfo.EanId);
            meterReading.ProductType.Should().Be(TestConstants.ElkProductType);
            meterReading.ReadingDate.Should().Be(date.Date);
            meterReading.RejectionCode.Should().BeNull();
            meterReading.RejectionMessage.Should().BeNull();
        }

        [Test]
        public void GetMeterReadingsMethod_ValidCaseWithoutMeterReadings_OK()
        {
            var date = DateTime.Today;
            var eanInfo = EanInfoProvider.GetRandomElkEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            CreateRequest(date, eans: eanInfo);

            var response = new GetMeterReadingsByDateRequest
            {
                Eans = new List<string> { eanInfo.EanId },
                ReadingDate = DateTime.Now.Date
            }.CallWith<GetMeterReadingsByDateRequest, GetLastMeterReadingsResponse>(P4EventClient);

            response.Header.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Data.Should().NotBeNull();
            response.Data.MeterReadings.Should().BeNullOrEmpty();
        }
    }
}
