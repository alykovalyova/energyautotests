﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Base;
using Autotests.P4Event.Enums;
using FluentAssertions;
using NUnit.Framework;

namespace Autotests.P4Event.Tests
{
    [TestFixture]
    internal class IntegrationTests : BaseP4EventReadings
    {
        [Test]
        public void P4EventAcceptedReadingIsReceived_ProcessRunnerSource()
        {
            var date = DateTime.Today;
            var eanInfo = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            var mp = CreateRequest(date, eans: eanInfo);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);
            var externalReference = GetP4EventReadingExternalReference(mp);
            var readings = P4DataProvider.AddDailyReadingsDetails(eanInfo);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RequestingMeterReadingsJob);
            Waiter.Wait(() => P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.MeterReading>(p4 =>
                p4.EanId == eanInfo.EanId).Count == 1);
        }

        [Test]
        public void P4EventRejectionIsReceived_ErrorStatusOfMeteringPoint()
        {
            var date = DateTime.Today;
            var rejectionCode = "1";
            var eanInfo = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            var mp = CreateRequest(date, eans: eanInfo);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);
            var externalReference = GetP4EventReadingExternalReference(mp);

            //rejected readings added
            var readings = P4DataProvider.AddDailyReadingsDetails(eanInfo, false, rejectionCode);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RequestingMeterReadingsJob);
            Waiter.Wait(() => P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.StatusHistory>(sh =>
                                  sh.CommunicationHistoryId == mp.CommunicationHistories.Last().Id).Count == 3, 200);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);
            Waiter.Wait(() => P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.StatusHistory>(sh =>
                sh.CommunicationHistoryId == mp.CommunicationHistories.Last().Id && sh.StatusId == (int)P4EventProcessStatus.Rejected));
            var rejectedMp = Waiter.Wait(() => P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == eanInfo.EanId & p4.StatusId == (int)MeteringPointStatus.EdsnError, "CommunicationHistories", "CommunicationHistories.StatusHistories"));
            var rejectedByEdsnShId = rejectedMp.CommunicationHistories.Last().StatusHistories
                .First(sh => sh.StatusId == (int)P4EventProcessStatus.RejectedByEdsn).Id;
            var rejection = P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.EdsnRejection>(er =>
                er.StatusHistoryId == rejectedByEdsnShId);
            rejection.CodeId.Should().Be(short.Parse(rejectionCode));
        }

        [Test]
        public void P4EventReadingsAreSentToConsDom(
            [Values(MarketEvent.TariffChange, MarketEvent.Monthly)] MarketEvent marketEvent)
        {
            var date = DateTime.Today;
            var eanInfo = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            DispatchSmartMeterInfoRequest(eanInfo, date, marketEvent);
            var mp = Waiter.Wait(() => P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == eanInfo.EanId && p4.StatusId == (int)MeteringPointStatus.Active, "CommunicationHistories"));

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);
            var externalReference = GetP4EventReadingExternalReference(mp);
            var readings = P4DataProvider.AddDailyReadingsDetails(eanInfo);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RequestingMeterReadingsJob);
            Waiter.Wait(() => P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.MeterReading>(p4 =>
                p4.EanId == eanInfo.EanId).Count == 1);

            Waiter.Wait(() => ConsumptionDb.GetEntityByCondition<Repositories.ConsumptionModels.MeterReading>(
                mr => mr.EanId == eanInfo.EanId && mr.MarketEventId == (int)marketEvent), 15);
        }

        [Test]
        public void P4EventReadingsAreSentToConsDom_DublicatedReadings()
        {
            var date = DateTime.Today;
            var tariffChangeMarketEvent = MarketEvent.TariffChange;
            var monthlyMarketEvent = MarketEvent.Monthly;
            var eanInfo = EanInfoProvider.GetRandomGasEan(true, gridOperator: TestConstants.GridOperatorFake);
            AddMeteringPointGain(date, eanInfo);

            DispatchSmartMeterInfoRequest(eanInfo, date, tariffChangeMarketEvent);
            DispatchSmartMeterInfoRequest(eanInfo, date, monthlyMarketEvent);

            var mp = Waiter.Wait(() => P4EventReadingsDb.GetEntityByCondition<Repositories.P4EventReadingsModels.MeteringPoint>(p4 =>
                p4.EanId == eanInfo.EanId && p4.StatusId == (int)MeteringPointStatus.Active, "CommunicationHistories"));

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);
            var externalReference = GetP4EventReadingExternalReference(mp);
            var readings = P4DataProvider.AddDailyReadingsDetails(eanInfo);
            FakeEdsnAdminClient.ProcessP4RequestsByExternalReference(externalReference, readings.ToList());

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RequestingMeterReadingsJob);
            Waiter.Wait(() => P4EventReadingsDb.GetEntitiesByCondition<Repositories.P4EventReadingsModels.MeterReading>(p4 =>
                p4.EanId == eanInfo.EanId).Count == 1);

            Waiter.Wait(() => ConsumptionDb.GetEntitiesByCondition<Repositories.ConsumptionModels.MeterReading>(
                mr => mr.EanId == eanInfo.EanId).Count == 2, 15);
        }
    }
}
