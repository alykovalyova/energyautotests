﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Base;
using Autotests.P4Event.Enums;
using FluentAssertions;
using NUnit.Framework;

namespace Autotests.P4Event.Tests
{
    [TestFixture]
    internal class MarkingMeteringPointJobTests : BaseP4EventReadings
    {
        [Test]
        public void MarkingMeteringPointJob_MarkMeteringPointAsError()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.AuthenticationError);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.EdsnError);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.AuthenticationError);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.EdsnError);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);
        }

        [Test]
        public void MarkingMeteringPointJob_MarkMeteringPointAsApplication()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.EanUnknownOnRequestDate);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Application);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.EanUnknownOnRequestDate);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Application);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);
        }

        [Test]
        public void MarkingMeteringPointJob_MarkMeteringPointAsApplicationIfCommunicationHistoryIsExpired()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.RequestCouldNotBeProcessedByMeter);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Application);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.RequestCouldNotBeProcessedByMeter);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Application);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            //case when status history is new
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-1), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.RequestCouldNotBeProcessedByMeter);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Application);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);
        }

        [Test]
        public void MarkingMeteringPointJob_MarkMeteringPointAsMalfunctionIfCommunicationHistoryIsExpired()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotFunctioning);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.MalfunctionKnown);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotFunctioning);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.MalfunctionKnown);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            //case when status history is new
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-1), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotFunctioning);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.MalfunctionKnown);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);
        }

        [Test]
        public void MarkingMeteringPointJob_MeteringPointIsNotMarkingInCaseOfExpiredMalfunctionRejectionDuringPeriodOfRetrying()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotFunctioning);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RetriedToSend);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotFunctioning);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RejectedByEdsn);
        }

        [Test]
        public void MarkingMeteringPointJob_MeteringPointIsNotMarkingInCaseOfExpiredApplicationRejectionDuringPeriodOfRetrying()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.RequestCouldNotBeProcessedByMeter);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RetriedToSend);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.RequestCouldNotBeProcessedByMeter);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RejectedByEdsn);
        }

        [Test]
        public void MarkingMeteringPointJob_MeteringPointIsNotMarkingInCaseOfExpiredNoErrorRejectionDuringPeriodOfRetrying()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.MeterdataIsntAvailableLargeScaleShutdown);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RetriedToSend);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.MeterdataIsntAvailableLargeScaleShutdown);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RejectedByEdsn);
        }

        [Test]
        public void MarkingMeteringPointJob_MeteringPointIsNotMarkingInCaseOfNoErrorRejection()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotReachableFromDistance);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.MeterIsNotReachableFromDistance);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);
        }

        [Test]
        public void MarkingMeteringPointJob_MeteringPointIsNotMarkingInCaseOfExpiredNoErrorRejection()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.MeterdataIsntAvailableLargeScaleShutdown);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-39));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);
            AddRejection(xmlMessageId, RejectionCode.MeterdataIsntAvailableLargeScaleShutdown);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Rejected);
        }

        [Test]
        public void MarkingMeteringPointJob_SkipCommunicationHistoriesWithReadingDateOlderThan39Days()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-40));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);
            AddRejection(xmlMessageId, RejectionCode.AuthenticationError);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RetriedToSend);
        }

        [Test]
        public void MarkingMeteringPointJob_MissCheckOfMeteringPointStatusIfLastStatusHistoryIsNotRetriedOrNotRejectedOrRejectionIsAbsent()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.Accepted);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var addedCommHistoryId = GetCommunicationHistory().Id;
            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Accepted);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.ReadyToSend);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.ReadyToSend);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_MarkingMeteringPointStatusJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            addedCommHistoryId = GetCommunicationHistory().Id;
            lastStatusHistory = GetStatusHistoriesByCommHistoryId(addedCommHistoryId)
                .OrderByDescending(sh => sh.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RejectedByEdsn);
            GetMeteringPoint().StatusId.Should().Be((short)MeteringPointStatus.Active);
        }
    }
}
