﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Base;
using Autotests.P4Event.Enums;
using FluentAssertions;
using NUnit.Framework;

namespace Autotests.P4Event.Tests
{
    [TestFixture]
    internal class RetrySendP4DataJobTests : BaseP4EventReadings
    {
        [Test]
        public void RetrySendP4DataJob_DoNotSendIfLastStatusHistoryWasCreatedDuringThreeDays()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-4));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-3), xmlMessageId, P4EventProcessStatus.RetriedToSend);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            Assert.IsTrue(lastStatusHistory.StatusId == (short)P4EventProcessStatus.RetriedToSend);

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-2), xmlMessageId, P4EventProcessStatus.RejectedByEdsn);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            Assert.IsTrue(lastStatusHistory.StatusId == (short)P4EventProcessStatus.RejectedByEdsn);

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-1), xmlMessageId, P4EventProcessStatus.SentToEdsn);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.SentToEdsn);
        }

        [Test]
        public void RetrySendP4DataJob_DontSendIfLastStatusHistoryIsNotRetriedOrIsNotSentToEdsnOrIsNotRejectedByEdsn()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-10), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-10));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-9), xmlMessageId, P4EventProcessStatus.Accepted);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.Accepted);

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-5), xmlMessageId, P4EventProcessStatus.SemanticRejectedByEdsn);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.SemanticRejectedByEdsn);
        }

        [Test]
        public void RetrySendP4DataJob_SkipCommunicationHistoriesWithReadingDateOlderThan39Days()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-40), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-40));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.SentToEdsn);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.SentToEdsn);

            DeleteTestData();

            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-50), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-48));

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-4), xmlMessageId, P4EventProcessStatus.RetriedToSend);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_RetrySendP4DataJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RetriedToSend);
        }
    }
}
