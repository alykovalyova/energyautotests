﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.P4Event.Base;
using Autotests.P4Event.Enums;
using FluentAssertions;
using NUnit.Framework;

namespace Autotests.P4Event.Tests
{
    [TestFixture]
    internal class SendingRequestsJobTests : BaseP4EventReadings
    {
        [Test]
        public void SendingRequestsJob_DontSendIfCommunicationHistoryWasCreatedEarlierThanToday()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now.AddDays(-3));

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now.AddDays(-3), xmlMessageId, P4EventProcessStatus.ReadyToSend);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.ReadyToSend);
        }

        [Test]
        public void SendingRequestsJob_DontSendIfLastStatusIsNotReadyToSend()
        {
            var gridOperator = TestConstants.GridOperator;
            AddGridOperator(gridOperator);
            AddMeteringPoint(DateTime.Now.AddDays(-4), gridOperator);
            AddCommunicationHistory(DateTime.Now);

            var xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now, xmlMessageId, P4EventProcessStatus.RetriedToSend);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            var lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RetriedToSend);

            xmlMessageId = Guid.NewGuid();
            AddStatusHistory(DateTime.Now, xmlMessageId, P4EventProcessStatus.RejectedByEdsn);

            Scheduler.TriggerJob(QuartzJobName.Energy_P4Event_SendingRequestsJob);

            Assert.Throws<AssertionException>(() => Waiter.Wait(() => { return false; }, timeForDatabaseUpdating));

            lastStatusHistory = GetStatusHistoriesByCommHistoryId(GetCommunicationHistory().Id)
                .OrderByDescending(s => s.CreatedOn).First();

            lastStatusHistory.StatusId.Should().Be((short)P4EventProcessStatus.RejectedByEdsn);
        }
    }
}
