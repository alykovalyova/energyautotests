﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Base
{
    internal class BaseDeletePropositionTest : BaseProdManTest
    {
        protected Guid AddPropositionDraft()
        {
            var marketTo = DateTime.Now.AddYears(2);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            var response = ProdManFactory.CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, 
                    ThreeYearDuration, EnergySources, MarketFrom, marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            if (response.Header.StatusCode != (int)HttpStatusCode.OK)
                throw new AssertionException("Draft proposition hasn't been created./n" + response.Header.Message);

            return response.PropositionId;
        }
    }
}