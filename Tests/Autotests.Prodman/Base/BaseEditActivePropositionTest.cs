﻿using System.Net;
using Autotests.Core.Helpers;
using Autotests.Repositories.ProdManModels;
using NUnit.Framework;
using ElectricityPrice = Nuts.ProdMan.Model.Contract.ProductPricesToSave.ElectricityPrice;
using Status = Nuts.ProdMan.Core.Enums.Status;
using Autotests.Helper;
using Autotests.Clients;

namespace Autotests.Prodman.Base
{
    [SetUpFixture]
    internal class BaseEditActivePropositionTest : BaseProdManTest
    {
        protected const string NewDescription = "changed_by_autotest_description";
        protected Guid ActivePropositionId { get; private set; }
        protected Guid VerifiablePropositionId { get; private set; }
        protected Proposition Proposition { get; private set; }

        [OneTimeSetUp]
        public new void OneTimeSetUp()
        {
            ActivePropositionId = CreateProposition(Status.Active);
            VerifiablePropositionId = CreateProposition(Status.Verifiable);
            Proposition = ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == ActivePropositionId);
        }

        [OneTimeTearDown]
        public new void OneTimeTearDown()
        {
            ProdManDb.DeleteEntityByCondition<Proposition>(p => p.Id == ActivePropositionId);
            ProdManDb.DeleteEntityByCondition<Proposition>(p => p.Id == VerifiablePropositionId);
        }

        private Guid CreateProposition(Status status)
        {
            var marketTo = DateTime.Now.AddDays(+45);
            var nameProposition = $"Autotest_{RandomDataProvider.GetRandomNumCharString(7)}";
            var elkPrices = new List<ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            var response = ProdManFactory.CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            if (response.Header.StatusCode != (int)HttpStatusCode.OK)
                throw new AssertionException("Draft proposition hasn't been created.");

            if (status == Status.Active)
                ActivateProposition(response.PropositionId);
            else
                ChangePropositionStatus(response.PropositionId, status);

            return response.PropositionId;
        }
    }
}