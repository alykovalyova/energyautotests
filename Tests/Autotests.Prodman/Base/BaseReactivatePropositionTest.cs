﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Repositories.ProdManModels;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using ElectricityPrice = Nuts.ProdMan.Model.Contract.ProductPricesToSave.ElectricityPrice;

namespace Autotests.Prodman.Base
{
    [SetUpFixture]
    internal abstract class BaseReactivatePropositionTest : BaseProdManTest
    {
        protected Guid VerifiablePropositionId { get; private set; }

        [OneTimeSetUp]
        public new void OneTimeSetUp() => VerifiablePropositionId = CreateProposition(Status.Verifiable);

        [OneTimeTearDown]
        public new void OneTimeTearDown() => ProdManDb.DeleteEntityByCondition<Proposition>(p => p.Id == VerifiablePropositionId);

        private Guid CreateProposition(Status status)
        {
            var marketTo = DateTime.Now.AddDays(+45);
            var nameProposition = $"Autotest_{RandomDataProvider.GetRandomNumCharString(7)}";
            var elkPrices = new List<ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            var response = ProdManFactory.CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient,
                    ThreeYearDuration, GasEnergySources, MarketFrom, marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            if (response.Header.StatusCode != (int)HttpStatusCode.OK)
                throw new AssertionException("Draft proposition hasn't been created.");

            if (status == Status.Active)
                ActivateProposition(response.PropositionId);
            else
                ChangePropositionStatus(response.PropositionId, status);
            
            return response.PropositionId;
        }
    }
}