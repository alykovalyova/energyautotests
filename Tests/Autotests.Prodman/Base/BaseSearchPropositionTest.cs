﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract.ProductPricesToSave;

namespace Autotests.Prodman.Base
{
    [SetUpFixture]
    internal class BaseSearchPropositionTest : BaseProdManTest
    {
        protected Guid AddProposition(Status? status = null)
        {
            var marketTo = DateTime.Now.AddDays(+45);
            var nameProposition = $"Autotest_{RandomDataProvider.GetRandomNumCharString(7)}";
            var elkPrices = new List<ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            var response = ProdManFactory.CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient,
                    ThreeYearDuration, GasEnergySources, MarketFrom, marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            if (response.Header.StatusCode != (int)HttpStatusCode.OK)
                throw new AssertionException("Draft proposition hasn't been created.");

            if (status == null) return response.PropositionId;

            if (status == Status.Active)
                ActivateProposition(response.PropositionId);
            else
                ChangePropositionStatus(response.PropositionId, status.GetValueOrDefault());

            return response.PropositionId;
        }
    }
}
