﻿using Nuts.InterDom.Model.Core.Enums;
using Nuts.ProdMan.Core.Enums;

namespace Autotests.Prodman.Helpers
{
    public class ProdManEnumsSetter
    {
        public static List<TypeOfClient> GetTypeOfClients =>
            new List<TypeOfClient>
            {
                TypeOfClient.Business, TypeOfClient.Private
            };

        public static List<EnergySource> GetEnergySources =>
            new List<EnergySource>
            {
                EnergySource.GreenG, EnergySource.NlWindE
            };

        public static List<EnergySource> GetElektraEnergySources =>
            new List<EnergySource>
            {
                EnergySource.StandardE, EnergySource.NlWindE
            };

        public static List<EnergySource> GetGasEnergySources =>
            new List<EnergySource>
            {
                EnergySource.GreyG, EnergySource.GreenG
            };

        public static List<EnergyUsageProfileCode> GetElkProfileCategories =>
            new List<EnergyUsageProfileCode>
            {
                EnergyUsageProfileCode.E1A, EnergyUsageProfileCode.E1B, EnergyUsageProfileCode.E1C,EnergyUsageProfileCode.E2A, EnergyUsageProfileCode.E2B
            };

        public static List<EnergyUsageProfileCode> GetElkE1ProfileCategories =>
            new List<EnergyUsageProfileCode>
            {
                EnergyUsageProfileCode.E1A, EnergyUsageProfileCode.E1B, EnergyUsageProfileCode.E1C
            };

        public static List<EnergyUsageProfileCode> GetElkE2ProfileCategories =>
            new List<EnergyUsageProfileCode>
            {
                EnergyUsageProfileCode.E2A, EnergyUsageProfileCode.E2B
            };

        public static List<EnergyUsageProfileCode> GetGasProfileCategories =>
            new List<EnergyUsageProfileCode>
            {
                EnergyUsageProfileCode.G1A, EnergyUsageProfileCode.G2A
            };

        public static List<EnergyUsageProfileCode> GetGasG1AProfileCategories =>
            new List<EnergyUsageProfileCode>
            {
                EnergyUsageProfileCode.G1A
            };

        public static List<EnergyUsageProfileCode> GetGasG2AProfileCategories =>
            new List<EnergyUsageProfileCode>
            {
                EnergyUsageProfileCode.G2A
            };
    }
}