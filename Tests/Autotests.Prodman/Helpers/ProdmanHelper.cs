﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using Nuts.ProdMan.Model.Contract.Propositions;
using Nuts.ProdMan.Model.Contract.Transport;
using Proposition = Nuts.ProdMan.Model.Contract.PropositionsToSave.Proposition;

namespace Autotests.Prodman.Helpers
{
    internal static class ProdmanHelper
    {
        private static ServiceClient<IProdManService> ProdManClient =
            new ServiceClient<IProdManService>(ConfigHandler.Instance.ServicesEndpoints.ProdManService);
        private const decimal ElkRetailPrice = 0.05m;
        private const decimal ElkAbsoluteDiscount = -0.05m;
        private const decimal GasRetailPrice = 0.3m;

        private static readonly List<EnergyUsageProfileCode> _elkProfileCategories = new List<EnergyUsageProfileCode>()
        {
            EnergyUsageProfileCode.E1A, EnergyUsageProfileCode.E1B, EnergyUsageProfileCode.E1C,
            EnergyUsageProfileCode.E2A, EnergyUsageProfileCode.E2B
        };

        private static readonly List<EnergyUsageProfileCode> _gasProfileCategories = new List<EnergyUsageProfileCode>()
        {
            EnergyUsageProfileCode.G1A, EnergyUsageProfileCode.G2A
        };

        private static List<ElectricityPrice> GenerateElkProductPricesList(string propositionName, int duration,
            DateTime marketFrom, decimal retailPrice,
            List<EnergyUsageProfileCode> profileCategories, PriceDiscountType type, decimal value)
        {
            return new List<ElectricityPrice>
            {
                ProdManFactory.GetElectricityPrice(propositionName, duration, marketFrom, retailPrice,
                    profileCategories, type, value)
            };
        }

        private static List<GasPrice> GenerateGasProductPricesList(string propositionName, int duration,
            DateTime marketFrom,
            List<EnergyUsageProfileCode> profileCategories)
        {
            return new List<GasPrice>
            {
                ProdManFactory.GetGasPrice(propositionName, duration, GasRetailPrice, marketFrom, profileCategories)
            };
        }

        private static Guid AddNewProposition(Labels label, string propositionName, bool hasVoucher, bool hasCashBack)
        {
            var addPropositionRequest = new AddPropositionRequest
            {
                PropositionInfo = new Proposition
                {
                    Name = propositionName,
                    Description = "Proposition generated by autotests",
                    Bna = false,
                    IsEligibleAsRenewal = true,
                    Label = label,
                    EnergySources = new List<EnergySource>()
                    {
                        EnergySource.EuropeseWind,
                        EnergySource.GreenG
                    },
                    ClientType = new List<TypeOfClient>()
                    {
                        TypeOfClient.Private
                    },
                    StandingChargePeriod = StandingChargePeriodType.PerDay,
                    ContractStartWindow = 10,
                    CoolDownPeriod = 5,
                    Duration = 12,
                    MarketFrom = DateTime.Now,
                    MargeTypeN = MargeTypeN.Auction,
                    MargeTypeR = MargeTypeR.Passing,
                    ElectricityPrices = GenerateElkProductPricesList(propositionName, 12, DateTime.Now,
                        ElkRetailPrice,
                        _elkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount),
                    GasPrices = GenerateGasProductPricesList(propositionName, 12, DateTime.Now, _gasProfileCategories)
                }
            };
            if (hasVoucher)
            {
                addPropositionRequest.PropositionInfo.Voucher = new Voucher
                {
                    VoucherAmount = 100
                };
            }

            if (hasCashBack)
            {
                addPropositionRequest.PropositionInfo.CashBack = new CashBack
                {
                    MaxCashback = 100
                };
            }

            var response = addPropositionRequest.CallWith(ProdManClient.Proxy.AddProposition);
            Assert.That(response.Header.StatusCode, Is.EqualTo((int) HttpStatusCode.OK));
            return response.PropositionId;
        }

        internal static PropositionInfo GetPropositionDetails(Guid propositionId)
        {
            var response = new GetPropositionInfoRequest {PropositionId = propositionId}
                .CallWith(ProdManClient.Proxy.GetPropositionInfo);

            return response.PropositionInfo;
        }

        private static void ActivateProposition(Guid propositionId)
        {
            new UpdatePropositionStatusRequest
            {
                PropositionId = propositionId,
                NewStatus = Status.Verifiable
            }.CallWith(ProdManClient.Proxy.UpdatePropositionStatus);

            new UpdatePropositionStatusRequest
            {
                PropositionId = propositionId,
                NewStatus = Status.Approved
            }.CallWith(ProdManClient.Proxy.UpdatePropositionStatus);

            new UpdatePropositionStatusRequest
            {
                PropositionId = propositionId,
                NewStatus = Status.Active
            }.CallWith(ProdManClient.Proxy.UpdatePropositionStatus);

            var activeProposition = new GetPropositionInfoRequest {PropositionId = propositionId};

            Waiter.Wait(() =>
                activeProposition.CallWith(ProdManClient.Proxy.GetPropositionInfo).PropositionInfo
                    .PropositionStatus ==
                Status.Active);
        }

        internal static Guid GetActivePropositionForTest(Labels label, string propositionName, bool hasVoucher, bool hasCashBack)
        {
            var propositionId = AddNewProposition(label, propositionName, hasVoucher, hasCashBack);
            ActivateProposition(propositionId);
            return propositionId;
        }
    }
}