﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static System.String;
using ElectricityPrice = Autotests.Repositories.ProdManModels.ElectricityPrice;
using GasPrice = Autotests.Repositories.ProdManModels.GasPrice;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class AddProductPrices : BaseProdManTest
    {
        private static readonly DateTime _marketTo = DateTime.Now.AddYears(+1);

        [Test]
        public void AddProductPrice_AddElkProductPriceToExistingProposition_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, OneYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, OneYearDuration,
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newElkPrice = GetElectricityPrice(nameProposition, OneYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage,
                PercentageDiscount);
            AddProductPriceToProposition(response.PropositionId, newElkPrice, PriceMarketFrom)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep =>
                ep.Code == newElkPrice.Code, "Proposition").Proposition.Name.Should().BeEquivalentTo(nameProposition);
        }

        [Test]
        public void AddProductPrice_AddGasProductPriceToExistingProposition_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, OneYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, OneYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, OneYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<GasPrice>(ep => ep.Code == newGasPrice.Code, "Proposition").Proposition.Name
                .Should().BeEquivalentTo(nameProposition);
        }

        [Test]
        public void AddProductPrice_AddProductPriceWithMarketFromLaterThanToday_Returns200()
        {
            //arrange
            var newMarketFrom = DateTime.Now.AddDays(+1);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                newMarketFrom, GasProfileCategories);
            var responseAddNewPrice = AddProductPriceToProposition(response.PropositionId, newGasPrice, newMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        public void AddProductPrice_AddElkProductPriceWithMarketFromEarlierThanActivePriceHas_Returns500()
        {
            //arrange
            var newPriceMarketFrom = PriceMarketFrom.AddDays(-2);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, OneYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, OneYearDuration,
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, newPriceMarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, -ElkRetailPrice);
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newElkPrice, newPriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                DateTime.Now.AddDays(1).ToString("dd-MM-yyyy")));
        }

        [Test]
        public void AddProductPrice_AddGasProductPriceWithMarketFromEarlierThanActivePriceHas_Returns500()
        {
            //arrange
            var newMarketFrom = PriceMarketFrom.AddDays(-1);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var sourcesOfEnergy = new List<EnergySource> { EnergySource.GreenG };
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    sourcesOfEnergy, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                newMarketFrom, GasProfileCategories);
            var responseAddNewPrice = AddProductPriceToProposition(response.PropositionId, newGasPrice, newMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                DateTime.Now.AddDays(+1).ToString("dd-MM-yyyy")));
        }

        [Test]
        public void AddProductPrice_AddProductPriceWithoutMarketFromDate_Returns500()
        {
            //arrange
            var marketFromDateForNewPrice = new DateTime();
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                marketFromDateForNewPrice, GasProfileCategories);
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newGasPrice, marketFromDateForNewPrice);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                DateTime.Now.AddDays(+1).ToString("dd-MM-yyyy")));
        }

        [Test]
        public void AddProductPrice_AddProductPriceToInactiveProposition_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            request.PropositionInfo.Id = response.PropositionId;
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);
            ChangePropositionStatus(response.PropositionId, Status.Inactive);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        public void AddProductPrice_AddProductPriceToVerifiableProposition_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ChangePropositionStatus(response.PropositionId, Status.Verifiable);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice, PriceMarketFrom,
                GasProfileCategories);
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(
                Format(PatternMessages.NotPossibleToAddPrice, Status.Active, Status.Inactive));
        }

        [Test]
        public void AddProductPrice_AddProductPriceToApprovedProposition_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ChangePropositionStatus(response.PropositionId, Status.Approved);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(
                Format(PatternMessages.NotPossibleToAddPrice, Status.Active, Status.Inactive));
        }

        [Test]
        public void AddProductPrice_AddProductPriceWithoutElkTariffsToExistingProposition_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories,
                PriceDiscountType.Absolute, -ElkRetailPrice);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom,
                ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            newElkPrice.ElektraTariffs.RemoveRange(0, 3);
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newElkPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkPriceMustHaveTariffs);
        }

        [Test]
        public void AddProductPrice_AddElkProductPriceWithWrongRetailPrice_Returns500([Values(0.01, 1.6)] decimal newRetailPrice)
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, -ElkRetailPrice);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var elektraTariffs = GetElektraTariffSet(newRetailPrice, ThreeYearDuration, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom,
                newRetailPrice, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            newElkPrice.ElektraTariffs = elektraTariffs;
            var responseAddNewPrice = AddProductPriceToProposition(response.PropositionId, newElkPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkRetailPriceWrong);
        }

        [Test]
        public void AddProductPrice_AddElkProductPriceWithWrongStandingCharge_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, -ElkRetailPrice);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    ElektraEnergySources,
                    MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            newElkPrice.StandingCharge = 0;
            var responseAddNewPrice = AddProductPriceToProposition(response.PropositionId, newElkPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(PatternMessages.StandingChargeWrong);
        }

        [Test]
        public void AddProductPrice_AddGasProductPriceWithWrongRetailPrice_Returns500()
        {
            //arrange
            const decimal newRetailPrice = 0.01m;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var gasTariffs = GetGasTariffsSet(newRetailPrice, ThreeYearDuration, GasProfileCategories);
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, newRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            newGasPrice.GasTariffs = gasTariffs;
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(PatternMessages.GasRetailPriceWrong);
        }

        [Test]
        public void AddProductPrice_AddGasProductPriceWithWrongStandingCharge_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act: create proposition
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //activate proposition
            ActivateProposition(response.PropositionId);

            //add product prices
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            newGasPrice.StandingCharge = 0;
            var responseAddNewPrice =
                AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom);
            responseAddNewPrice.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseAddNewPrice.Header.Message.Should().BeEquivalentTo(PatternMessages.StandingChargeWrong);
        }

        [Test]
        public void AddProductPrice_AddSecondGasProductPrice_LatestProductPriceIsActiveAllPreviousAreInactive()
        {
            //arrange
            var propositionName = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(propositionName, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, GasEnergySources, MarketFrom, _marketTo, propositionName);

            //add one more product price and activate it
            var gasPrice1 = GetGasPrice(propositionName, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            var addPriceResponse1 =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, gasPrice1, PriceMarketFrom);
            addPriceResponse1.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            gasPrice1.Id = addPriceResponse1.ProductPriceId;
            UpdateProductPriceStatus(Status.Verifiable, gasPrice1);
            UpdateProductPriceStatus(Status.Approved, gasPrice1);
            var date = DateTime.Now.AddDays(-2);
            ProdManDb.UpdateSingleEntity<GasPrice>(p => p.Id.Equals(gasPrice1.Id), p => p.MarketFrom = date);
            UpdateProductPriceStatus(Status.Active, gasPrice1);

            //assert that latest product price is active and all previous are inactive
            ProdManDb.GetEntitiesByCondition<GasPrice>(g => g.PropositionId.Equals(addPropositionResponse.PropositionId))
                .Should().HaveCount(2).And.Subject
                .Should().ContainSingle(p => p.StatusId.Equals((int)Status.Active));
        }

        [Test]
        public void AddProductPrice_AddSecondElkProductPrice_LatestProductPriceIsActiveAllPreviousAreInactive()
        {
            //arrange
            var propositionName = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(propositionName, OneYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, ElektraEnergySources, MarketFrom, _marketTo, propositionName);

            //add one more product price and activate it
            var newElkPrice = GetElectricityPrice(propositionName, OneYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage,
                PercentageDiscount);
            var addPriceResponse1 =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            addPriceResponse1.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            newElkPrice.Id = addPriceResponse1.ProductPriceId;
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);
            var date = DateTime.Now.AddDays(-2);
            ProdManDb.UpdateSingleEntity<ElectricityPrice>(p => p.Id.Equals(newElkPrice.Id), p => p.MarketFrom = date);
            UpdateProductPriceStatus(Status.Active, newElkPrice);

            //assert that latest product price is active and all previous are inactive
            ProdManDb.GetEntitiesByCondition<ElectricityPrice>(g => g.PropositionId.Equals(addPropositionResponse.PropositionId))
                .Should().HaveCount(2).And.Subject
                .Should().ContainSingle(p => p.StatusId.Equals((int)Status.Active));
        }
    }
}