﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using GasTariff = Nuts.ProdMan.Model.Contract.GasTariff;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using static System.String;
using EnergySource = Nuts.ProdMan.Core.Enums.EnergySource;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal partial class AddProposition : BaseProdManTest
    {
        private static readonly DateTime _marketTo = DateTime.Now.AddYears(2);

        [Test]
        public void AddProposition_WithTwoProductPrices_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, EnergySources,
                MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.EntityIsInDb<Proposition>(p => p.Name == nameProposition).Should().BeTrue();
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId.Should().Be((byte)Status.Draft);
        }

        [Test]
        public void AddProposition_WithElektraPriceOnly_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.EntityIsInDb<Proposition>(p => p.Name == nameProposition).Should().BeTrue();
        }

        [Test]
        public void AddProposition_WithElkTariffsDifferentiation_Returns200()
        {
            //arrange
            var elkE1TariffSet = GetElektraTariffSet(ElkRetailPrice, ThreeYearDuration,
                ElkE1ProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var elkE2TariffSet = GetElektraTariffSet(ElkRetailPrice, ThreeYearDuration,
                ElkE2ProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var tariffSet = new List<ElektraTariff>();
            tariffSet.AddRange(elkE1TariffSet);
            tariffSet.AddRange(elkE2TariffSet);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>
            {
                GetElectricityPriceWithTariffsSet(
                    nameProposition, ThreeYearDuration, MarketFrom, tariffSet)
            };
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, ElektraEnergySources,
                MarketFrom, _marketTo, nameProposition).CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.EntityIsInDb<Proposition>(p => p.Name == nameProposition).Should().BeTrue();
        }

        [Test]
        public void AddProposition_WithGasTariffsDifferentiation_Returns200()
        {
            //arrange
            var gasG1TariffSet = GetGasTariffsSet(GasRetailPrice, ThreeYearDuration, GasG1AProfileCategories);
            var gasG2TariffSet = GetGasTariffsSet(GasRetailPrice, ThreeYearDuration, GasG2AProfileCategories);
            var tariffSet = new List<GasTariff>();
            tariffSet.AddRange(gasG1TariffSet);
            tariffSet.AddRange(gasG2TariffSet);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var gasPrices = new List<ProductPricesToSave.GasPrice>
            {
                GetGasPriceWithTariffsSet(nameProposition, ThreeYearDuration, MarketFrom, tariffSet)
            };
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.EntityIsInDb<Proposition>(p => p.Name == nameProposition).Should().BeTrue();
        }

        [Test]
        public void AddProposition_WithGasPriceOnly_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, GasEnergySources,
                MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.EntityIsInDb<Proposition>(p => p.Name == nameProposition).Should().BeTrue();
        }

        //fails
        [Test]
        public void AddProposition_WithSingleTariffTypeOnly_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();
            elkPrices.First().ElektraTariffs.RemoveRange(1, 2);

            //act
            CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, ElektraEnergySources,
                MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        public void AddProposition_WithCashBeckAndIsEligableAsRenewal_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, EnergySources,
                MarketFrom, _marketTo, nameProposition, incentiveType:IncentiveType.CashBack)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.WithCashBeckAndEligableAsRenewal);
        }

        [Test]
        public void AddProposition_WithHighTariffTypeOnly_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            elkPrices.First().ElektraTariffs.RemoveRange(0, 2);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkTariffTypeWrong);
        }

        [Test]
        public void AddProposition_WithoutPrices_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should()
                .BeEquivalentTo(Format(PatternMessages.PropositionHasNoPrices, nameProposition));
        }

        [Test]
        public void AddProposition_WithoutElektraTariffs_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elektraPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                MarketFrom, ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice> {elektraPrice};
            var gasPrices = new List<ProductPricesToSave.GasPrice>();
            elektraPrice.ElektraTariffs.RemoveRange(0, 3);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    ElektraEnergySources, MarketFrom, MarketFrom.AddDays(+55), nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkPriceMustHaveTariffs);
        }

        [Test]
        public void AddProposition_WithoutGasTariffs_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);

            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);

            //add new gas price without tariffs
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            newGasPrice.GasTariffs.RemoveRange(0, 10);
            var addPriceResponse = AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom);
            addPriceResponse.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            addPriceResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.GasPriceMustHaveTariffs);
        }

        [Test]
        public void AddProposition_WithProductPriceCodeMoreThanAllowed_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(75);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    EnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            //assert
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.CodeLengthWrong);
        }

        [Test]
        public void AddProposition_WithoutEnergySources_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var sourcesOfEnergy = new List<EnergySource>();
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    sourcesOfEnergy, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            //assert
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoEnergySources);
        }

        [Test]
        public void AddProposition_WithWrongSetOfEnergySourcesForElektraPrice_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            //assert
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should()
                .BeEquivalentTo(Format(PatternMessages.NoEnergySourcesForElkPrice, nameProposition));
        }

        [Test]
        public void AddProposition_WithWrongSetOfEnergySourcesForGasPrice_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);

            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            //assert
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should()
                .BeEquivalentTo(Format(PatternMessages.NoEnergySourcesForGasPrice, nameProposition));
        }

        [Test]
        public void AddProposition_WithoutTypeOfClient_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var typeOfClients = new List<TypeOfClient>();
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, typeOfClients, ThreeYearDuration, 
                    EnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            //assert
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoClientType);
        }

        [Test]
        public void AddProposition_WithoutElkTariffsCapacity_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>
            {
                GetElectricityPrice(nameProposition, ThreeYearDuration, MarketFrom,
                    ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount)
            };
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            elkPrices.First().ElektraTariffs.ForEach(elkTariff => elkTariff.ProfileCategories = null);
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    ElektraEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);

            //assert           
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.TariffMustHaveCapacity);
        }

        [Test]
        public void AddProposition_WithTheExistingName_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                EnergySources, MarketFrom, _marketTo, nameProposition);
            request.CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should()
                .BeEquivalentTo(Format(PatternMessages.PropositionAlreadyExist, nameProposition));
        }

        [Test]
        public void AddProposition_WithVariableDuration_Returns500()
        {
            //arrange
            var duration = ThreeYearDuration;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, duration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, duration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, duration, EnergySources,
                MarketFrom, _marketTo, nameProposition);
            request.CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should()
                .BeEquivalentTo(Format(PatternMessages.PropositionAlreadyExist, nameProposition));
        }

        [Test]
        public void AddProposition_WithWrongElkRetailPrice_Returns500()
        {
            //arrange
            const decimal retailPrice = 0.01m;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, retailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, ElektraEnergySources,
                MarketFrom, _marketTo, nameProposition);
            request.CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);

            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkRetailPriceWrong);
        }

        [Test]
        public void AddProposition_WithWrongGasRetailPrice_Returns500()
        {
            //arrange
            const decimal retailPrice = 0.01m;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, retailPrice,
                GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, GasEnergySources,
                MarketFrom, _marketTo, nameProposition);
            request.CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);

            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.GasRetailPriceWrong);
        }

        [Test]
        public void AddProposition_WithWrongStandingCharge_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, GasEnergySources,
                MarketFrom, _marketTo, nameProposition);
            request.PropositionInfo.GasPrices.First().StandingCharge = 0;
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.StandingChargeWrong);
        }

        [Test]
        public void AddProposition_WithWrongMarketFrom_Returns500()
        {
            //arrange
            var fromDate = DateTime.Now.AddDays(-17);
            var correctMarketFrom = DateTime.Now.AddDays(-14);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, fromDate, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, fromDate, GasRetailPrice,
                GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, EnergySources,
                fromDate, _marketTo, nameProposition);
            request.CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);

            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                correctMarketFrom.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void AddProposition_WithoutMarketFrom_Returns500()
        {
            //arrange
            var fromDate = new DateTime();
            var correctMarketFrom = DateTime.Now.AddDays(-14);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, fromDate, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, fromDate, GasRetailPrice,
                GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, EnergySources,
                fromDate, _marketTo, nameProposition);
            request.CallWith(ProdmanClient.Proxy.AddProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);

            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                correctMarketFrom.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void AddProposition_CheckElectraTariffsForE1CEnergyUsageProfileWithFiveTariffs_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount, true);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    EnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            GetPropositionDetails(response.PropositionId).PropositionDetails.Should().NotBeNull();
            ActivateProposition(response.PropositionId);
            var updatedProposition = GetPropositionDetails(response.PropositionId).PropositionDetails;
            updatedProposition.PropositionStatus.Should().BeEquivalentTo(Status.Active);

            //assert
            updatedProposition.GetElectricityTariff(EnergyUsageProfileCode.E1C, DateTime.Today.AddDays(50)).ElektraTariffs
                .Should().HaveCount(2).And.Subject
                .Should().Contain(x => x.TariffType == ElektraTariffType.Peak).And.Subject
                .Should().Contain(x => x.TariffType == ElektraTariffType.OffPeak);
        }

        [Test]
        public void AddProposition_CheckElectraTariffsForE1CEnergyUsageProfileWithThreeTariffs_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);

            //act
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    EnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            GetPropositionDetails(response.PropositionId).PropositionDetails.Should().NotBeNull();
            ActivateProposition(response.PropositionId);
            var updatedProposition = GetPropositionDetails(response.PropositionId).PropositionDetails;
            updatedProposition.PropositionStatus.Should().BeEquivalentTo(Status.Active);

            //assert
            updatedProposition.GetElectricityTariff(EnergyUsageProfileCode.E1C, DateTime.Today.AddDays(50))
                .ElektraTariffs
                .Should().HaveCount(2).And.Subject
                .Should().Contain(x => x.TariffType == ElektraTariffType.High).And.Subject
                .Should().Contain(x => x.TariffType == ElektraTariffType.Low);
        }

        [Test]
        public void AddProposition_GetActiveElectricityPrice_Returns200()
        {
            //arrange
            var marketFromDate = DateTime.Today.AddDays(-13);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, marketFromDate,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, marketFromDate,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    EnergySources, marketFromDate, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ActivateProposition(response.PropositionId);
            var propositionDetails = GetPropositionDetails(response.PropositionId).PropositionDetails;
            propositionDetails.GetElectricityTariff(EnergyUsageProfileCode.E1A, MarketFrom.AddDays(1).Date)
                .ProductPriceStatus.Should().BeEquivalentTo(Status.Active);
        }

        [Test]
        public void AddProposition_GetApprovedGasPrice_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, 
                    ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);

            var newGasPrice = GetGasPrice(nameProposition, OneYearDuration, GasRetailPrice,
                MarketFrom, GasProfileCategories);
            var addPriceResponse = AddProductPriceToProposition(response.PropositionId, newGasPrice, PriceMarketFrom);
            newGasPrice.Id = addPriceResponse.ProductPriceId;
            UpdateProductPriceStatus(Status.Verifiable, newGasPrice);
            UpdateProductPriceStatus(Status.Approved, newGasPrice);

            var propositionDetails = GetPropositionDetails(response.PropositionId);
            propositionDetails.PropositionDetails
                .GetGasTariff(EnergyUsageProfileCode.G1A, GasRegion.Region1, _marketTo)
                .ProductPriceStatus.Should().BeEquivalentTo(Status.Approved);
        }
    }
}