﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using ElectricityPrice = Autotests.Repositories.ProdManModels.ElectricityPrice;
using GasPrice = Autotests.Repositories.ProdManModels.GasPrice;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal partial class AddProposition
    {
        private static readonly DateTime _toDate = DateTime.Now.AddDays(+99);
        
        [Test]
        public void AddProposition_AddLoyalCustomerDiscountToElkPrice_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, VariableDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, -ElkRetailPrice);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                    ElektraEnergySources, MarketFrom, _toDate, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            
            //assert
            var code = elkPrices.First().Code;
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => 
                ep.Code == code, "ElectricityTariff").ElectricityTariff.First().LoyalCustomerDiscount.Should().Be(-1.5m);
        }

        [Test]
        public void AddProposition_AddLoyalCustomerDiscountToGasPrice_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, VariableDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _toDate, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            var code = gasPrices.First().Code;
            ProdManDb.GetEntityByCondition<GasPrice>(gp => gp.Code == code, "GasTariff").GasTariff.First()
                .LoyalCustomerDiscount.Should().Be(-0.7m);
        }

        [Test]
        public void AddProposition_AddAbsoluteDiscountToElkPrice_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    EnergySources, MarketFrom, _toDate, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);

            //assert
            var code = elkPrices.First().Code;
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == code, "ElectricityTariff").ElectricityTariff.First()
                .PriceDiscountValue.GetValueOrDefault().Should().Be(ElkAbsoluteDiscount);
        }

        [Test]
        public void AddProposition_AddPercentageDiscountToElkPrice_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    EnergySources, MarketFrom, _toDate, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);

            //assert
            var code = elkPrices.First().Code;
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == code, "ElectricityTariff").ElectricityTariff.First()
                .PriceDiscountValue.GetValueOrDefault().Should().Be(PercentageDiscount);
        }

        [Test]
        public void AddProposition_CheckPercentageDiscountValidation_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, WrongPercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    EnergySources, MarketFrom, _toDate, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PercentDiscountWrong);
        }

        [Test]
        public void AddProposition_CheckAbsoluteDiscountValidation_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, WrongAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                EnergySources, MarketFrom, _toDate, nameProposition);
            request.PropositionInfo.ElectricityPrices.First().ElektraTariffs.First().PriceDiscount.Value = 55;
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.AbsoluteDiscountWrong);
        }

        [Test, Pairwise]
        public void AddProposition_DifferentMargeType_Returns200
        ([Values(MargeTypeN.Business, MargeTypeN.Auction, MargeTypeN.Comparer, MargeTypeN.Employee,
                MargeTypeN.Regular, MargeTypeN.Variable)] MargeTypeN margeTypeN, 
          [Values(MargeTypeR.Passing, MargeTypeR.RenewalEmployee, MargeTypeR.RenewalAction, MargeTypeR.RenewalAuction,
              MargeTypeR.RenewalBusiness, MargeTypeR.RenewalRegular, MargeTypeR.RenewalSharp, MargeTypeR.RenewalSuperSharp)] MargeTypeR margeTypeR  )
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, VariableDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, -ElkRetailPrice);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                ElektraEnergySources, MarketFrom, _toDate, nameProposition);
            request.PropositionInfo.MargeTypeR = margeTypeR;
            request.PropositionInfo.MargeTypeN = margeTypeN;
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //need to deactivate propositions with new margeTypes till XTeam start supporting them 
            ActivateProposition(response.PropositionId);
            ChangePropositionStatus(response.PropositionId, Status.Inactive);
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == response.PropositionId).StatusId
                .Should().Be((int)Status.Inactive);
        }
    }
}