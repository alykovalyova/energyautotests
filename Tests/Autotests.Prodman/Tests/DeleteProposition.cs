﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static System.String;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class DeleteProposition : BaseDeletePropositionTest
    {
        [Test]
        public void DeleteProposition_WhenPropositionIsDraft_Returns200()
        {
            var propositionId = AddPropositionDraft();

            var response = CreateDeletePropositionRequest(propositionId)
                .CallWith(ProdmanClient.Proxy.DeleteProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ProdManDb.EntityIsInDb<Proposition>(p => p.Id == propositionId).Should().BeFalse();
        }

        [Test]
        public void DeleteProposition_WhenPropositionIsNotDraft_Returns500(
            [Values(Status.Approved, Status.Verifiable)] Status status)
        {
            var propositionId = AddPropositionDraft();
            ChangePropositionStatus(propositionId, status);

            var response = CreateDeletePropositionRequest(propositionId)
                .CallWith(ProdmanClient.Proxy.DeleteProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotEditOrDeleteProposition, status);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void DeleteProposition_WhenPropositionIsActive_Returns500()
        {
            var propositionId = AddPropositionDraft();
            ActivateProposition(propositionId);

            var response = CreateDeletePropositionRequest(propositionId)
                .CallWith(ProdmanClient.Proxy.DeleteProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotEditOrDeleteProposition, Status.Active);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        [TestCase("empty", TestName = "DeleteProposition_WithEmptyPropositionId_Returns500")]
        [TestCase("notExisted", TestName = "DeleteProposition_WithNotExistedPropositionId_Returns500")]
        public void DeleteProposition_WithInvalidPropositionId_Returns500(string testCase)
        {
            var request = testCase == "empty"
                ? CreateDeletePropositionRequest(Guid.Empty)
                : CreateDeletePropositionRequest(Guid.NewGuid());
            var response = request.CallWith(ProdmanClient.Proxy.DeleteProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotFindProposition, request.PropositionId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}