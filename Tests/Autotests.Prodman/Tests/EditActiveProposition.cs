﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static System.String;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class EditActiveProposition : BaseEditActivePropositionTest
    {
        [Test]
        [TestCase(NewDescription, null, null, TestName = "EditActiveProposition_ChangeDescription_Returns200")]
        [TestCase(null, false, null, TestName = "EditActiveProposition_ChangeIsSalesEligible_Returns200")]
        [TestCase(null, null, 777, TestName = "EditActiveProposition_ChangeReimburseFine_Returns200")]
        public void EditActiveProposition_DefaultValidTests_Returns200(string description, bool? isSalesEligible, decimal? reimburseFine)
        {
            CreateEditActivePropositionRequest(
                    Proposition, description:description, isSalesEligible:isSalesEligible, reimburseFine:reimburseFine)
                .CallWith(ProdmanClient.Proxy.EditActiveProposition)
                .Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var proposition = ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == ActivePropositionId);
            (description != null).IfTrue(() => proposition.Description.Should().BeEquivalentTo(NewDescription));
            (isSalesEligible != null).IfTrue(() => proposition.IsSalesEligible.Should().BeFalse());
            (reimburseFine != null).IfTrue(() =>  proposition.ReimburseFine.Should().Be(reimburseFine));
            proposition.MarketTo.Should().Be(Proposition.MarketTo);
        }

        [Test]
        public void EditActiveProposition_ChangeMarketToDate_Returns200()
        {
            var request = CreateEditActivePropositionRequest(Proposition, marketTo:Proposition.MarketTo.GetValueOrDefault().AddDays(10));
            var response = request.CallWith(ProdmanClient.Proxy.EditActiveProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var proposition = ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == ActivePropositionId);
            proposition.Description.Should().BeEquivalentTo(request.Description);
            proposition.IsSalesEligible.Should().Be(request.IsSalesEligible);
            proposition.ReimburseFine.Should().Be(request.ReimburseFine);
            proposition.MarketTo.Should().Be(request.MarketTo);
        }

        [Test]
        public void EditActiveProposition_WhenMarketToLessThanTodayDate_Returns500()
        {
            var invalidDate = DateTime.Now.AddDays(-1);
            var response = CreateEditActivePropositionRequest(Proposition, marketTo: invalidDate)
                .CallWith(ProdmanClient.Proxy.EditActiveProposition);

            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotChangeMarketToDate, invalidDate.Date.ToString("dd-MM-yyyy"));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void EditActiveProposition_WithNotExistedPropositionId_Returns500()
        {
            var propositionId = Guid.NewGuid();
            var response = CreateEditActivePropositionRequest(Proposition, propositionId)
                .CallWith(ProdmanClient.Proxy.EditActiveProposition);

            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotFindProposition, propositionId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void EditActiveProposition_InvalidPropositionStatus_Returns500(
            [Values(Status.Draft, Status.Verifiable, Status.Inactive)] Status status)
        {
            var proposition = ProdManDb.GetEntityByCondition<Proposition>(p => p.StatusId == (int)status);
            var response = CreateEditActivePropositionRequest(proposition)
                .CallWith(ProdmanClient.Proxy.EditActiveProposition);

            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotEditNotActiveProposition, status.ToString());
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}