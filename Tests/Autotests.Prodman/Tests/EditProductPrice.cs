﻿using System.Net;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using ElectricityPrice = Autotests.Repositories.ProdManModels.ElectricityPrice;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using static System.String;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class EditProductPrice : BaseProdManTest
    {
        [Test]
        public void EditProductPrice_WhenStatusDraft_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var marketToDate = DateTime.Now.AddDays(+40);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient, 
                ThreeYearDuration, ElektraEnergySources, MarketFrom, marketToDate, nameProposition);

            //add draft product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var addProductPriceResponse = AddProductPriceToProposition(addPropositionResponse.PropositionId, 
                newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addProductPriceResponse.ProductPriceId;

            //edit new product prices
            newElkPrice.MeasureUnit = Nuts.InterDom.Model.Core.Enums.MeasureUnitCode.KWT;
            EditProductPrice(PriceMarketFrom, newElkPrice).Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => 
                ep.Code == newElkPrice.Code, "MeasureUnitCode").MeasureUnitCode.Name
                .Should().BeEquivalentTo(newElkPrice.MeasureUnit.ToString());
        }

        [Test]
        public void EditProductPrice_WhenStatusVerifiable_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice, ElkProfileCategories, 
                PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();
            var marketToDate = DateTime.Now.AddDays(+100);
            
            //act: create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient, 
                ThreeYearDuration, ElektraEnergySources, MarketFrom, marketToDate, nameProposition);

            //add verifiable product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            var addProductPriceResponse = AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addProductPriceResponse.ProductPriceId;
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);

            //edit new product prices
            newElkPrice.MeasureUnit = Nuts.InterDom.Model.Core.Enums.MeasureUnitCode.KWT;
            var response = EditProductPrice(PriceMarketFrom, newElkPrice);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should()
                .BeEquivalentTo(Format(PatternMessages.NotPossibleToEditPrice, Status.Verifiable));
        }

        [Test]
        public void EditProductPrice_WhenStatusApproved_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var marketToDate = DateTime.Now.AddDays(+90);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, ElektraEnergySources, MarketFrom, marketToDate, nameProposition);

            //add verifiable product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addProductPriceResponse = AddProductPriceToProposition(addPropositionResponse.PropositionId,
                newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addProductPriceResponse.ProductPriceId;
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);

            //edit new product prices
            newElkPrice.StandingCharge = elkPrices.First().StandingCharge + 1;
            var response = EditProductPrice(PriceMarketFrom, newElkPrice);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.NotPossibleToEditPrice,
                Status.Approved));
        }

        [Test]
        public void EditElkProductPrice_WithWrongRetailPrice_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            const decimal newRetailPrice = 0.01m;
            var marketToDate = DateTime.Now.AddDays(+140);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient, 
                ThreeYearDuration, EnergySources, MarketFrom, marketToDate, nameProposition);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var addProductPriceResponse = AddProductPriceToProposition(addPropositionResponse.PropositionId, 
                newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addProductPriceResponse.ProductPriceId;

            //edit new product prices
            newElkPrice.ElektraTariffs.First().RetailPrice = newRetailPrice;
            var response = EditProductPrice(PriceMarketFrom, newElkPrice);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkRetailPriceWrong);
        }

        [Test]
        public void EditGasProductPrice_WithWrongRetailPrice_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            const decimal newRetailPrice = 0.01m;
            var marketToDate = DateTime.Now.AddDays(+50);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //create active proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient, 
                ThreeYearDuration, EnergySources, MarketFrom, marketToDate, nameProposition);

            //add product price
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice, 
                PriceMarketFrom, GasProfileCategories);
            var addProductPriceResponse = AddProductPriceToProposition(addPropositionResponse.PropositionId, 
                newGasPrice, PriceMarketFrom);
            newGasPrice.Id = addProductPriceResponse.ProductPriceId;

            //edit new product prices
            newGasPrice.GasTariffs.First().RetailPrice = newRetailPrice;
            var response = EditProductPrice(PriceMarketFrom, newGasPrice);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.GasRetailPriceWrong);
        }
    }
}