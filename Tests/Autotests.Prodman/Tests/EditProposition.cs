﻿using System.Net;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;
using Autotests.Clients;
using static System.String;
using Microsoft.Data.SqlClient.Server;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class EditProposition : BaseProdManTest
    {
        private static readonly DateTime _marketTo = DateTime.Now.AddDays(+45);
        
        [Test]
        public void EditProposition_MarketTo_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration,
                MarketFrom, GasRetailPrice, GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            
            ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var editResponse = EditActiveProposition(request.PropositionInfo.IsSalesEligible, 
                _marketTo.AddDays(1), response.PropositionId);
            editResponse.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition)
                .MarketTo.GetValueOrDefault().Date.Should().Be(_marketTo.AddDays(1).Date);
        }

        [Test]
        public void EditProposition_Description_Returns200()
        {
            //arrange
            const string newDescription = "new_description";
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, 
                MarketFrom, GasRetailPrice, GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            request.PropositionInfo.Description = newDescription;
            var responseEdit = EditActiveProposition(request.PropositionInfo.IsFineApplicable, 
                request.PropositionInfo.MarketTo.GetValueOrDefault(), request.PropositionInfo.Id, newDescription);
            responseEdit.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition)
                .Description.Should().BeEquivalentTo(newDescription);
        }

        [Test]
        public void EditProposition_EligibleForSales_Returns200()
        {
            //arrange
            const bool newEligible = false;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
                        response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            request.PropositionInfo.IsSalesEligible = newEligible;
            var responseEdit = EditActiveProposition(newEligible, request.PropositionInfo.MarketTo.GetValueOrDefault(), 
                request.PropositionInfo.Id);
            responseEdit.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => 
                p.Name == nameProposition).IsSalesEligible.Should().Be(newEligible);
        }

        [Test]
        public void EditProposition_ReimbursementField_Returns200()
        {
            //arrange
            const int newReimburseFine = 5000;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration,
                MarketFrom, GasRetailPrice, GasProfileCategories);

            //create activated proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            request.PropositionInfo.ReimburseFine = newReimburseFine;
            var responseEdit = EditActiveProposition(request.PropositionInfo.IsSalesEligible,
                request.PropositionInfo.MarketTo.GetValueOrDefault(),
                request.PropositionInfo.Id, request.PropositionInfo.Description, newReimburseFine);
            responseEdit.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition)
                .ReimburseFine.Should().Be(newReimburseFine);
        }

        [Test]
        public void EditProposition_WithCorrectMarketFromField_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var newMarketFrom = MarketFrom.AddDays(+5);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act: create activated proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var newCode = CreateCodeForElkPrice(nameProposition, newMarketFrom, ThreeYearDuration);
            request.PropositionInfo.GasPrices.First().Code = newCode;
            request.PropositionInfo.MarketFrom = newMarketFrom;
            var responseEdit = EditProposition(request.PropositionInfo);
            responseEdit.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition)
                .MarketFrom.Should().Be(newMarketFrom.Date);
        }

        [Test]
        public void EditProposition_WithWrongMarketFromField_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var newMarketFrom = MarketFrom.AddDays(-5);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act: create activated proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            //ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var newCode = CreateCodeForElkPrice(nameProposition, newMarketFrom, ThreeYearDuration);
            request.PropositionInfo.GasPrices.First().Code = newCode;
            request.PropositionInfo.MarketFrom = newMarketFrom;
            var responseEdit = EditProposition(request.PropositionInfo);
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                MarketFrom.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void EditProposition_WhenStatusIsVerifiable_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var newMarketFrom = MarketFrom.AddDays(-5);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //create activated proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ChangePropositionStatus(response.PropositionId, Status.Verifiable);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var newCode = CreateCodeForGasPrice(nameProposition, newMarketFrom, ThreeYearDuration);
            request.PropositionInfo.GasPrices.First().Code = newCode;
            request.PropositionInfo.MarketFrom = newMarketFrom;
            var responseEdit = EditProposition(request.PropositionInfo);

            //assert
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                MarketFrom.Date.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void EditProposition_WhenStatusIsApproved_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var newMarketFrom = MarketFrom.AddDays(-30);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act (create activated proposition)
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ChangePropositionStatus(response.PropositionId, Status.Approved);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var newCode = CreateCodeForGasPrice(nameProposition, newMarketFrom, ThreeYearDuration);
            request.PropositionInfo.GasPrices.First().Code = newCode;
            request.PropositionInfo.MarketFrom = newMarketFrom;
            var responseEdit = EditProposition(request.PropositionInfo);

            //assert
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                MarketFrom.Date.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void EditProposition_WhenStatusIsActive_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var newMarketFrom = MarketFrom.AddDays(-30);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice, 
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create activated proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                ElektraEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ActivateProposition(response.PropositionId);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var newCode = CreateCodeForElkPrice(nameProposition, newMarketFrom, ThreeYearDuration);
            request.PropositionInfo.ElectricityPrices.First().Code = newCode;
            request.PropositionInfo.MarketFrom = newMarketFrom;
            var responseEdit = EditProposition(request.PropositionInfo);

            //assert
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                MarketFrom.Date.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void EditProposition_WhenStatusIsInactive_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var newMarketFrom = MarketFrom.AddDays(-30);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice, 
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create activated proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration, 
                ElektraEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            ActivateProposition(response.PropositionId);
            ChangePropositionStatus(response.PropositionId, Status.Inactive);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            var newCode = CreateCodeForElkPrice(nameProposition, newMarketFrom, ThreeYearDuration);
            request.PropositionInfo.ElectricityPrices.First().Code = newCode;
            request.PropositionInfo.MarketFrom = newMarketFrom;
            var responseEdit = EditProposition(request.PropositionInfo);

            //assert
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.MarketFromDateWrong,
                MarketFrom.Date.ToString("dd-MM-yyyy")));
        }

        [Test]
        public void EditProposition_WithWrongElkRetailPrice_Returns500()
        {
            //arrange
            const decimal newRetailPrice = 0.01m;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                ElektraEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            request.PropositionInfo.ElectricityPrices.First().ElektraTariffs.First().RetailPrice = newRetailPrice;
            var responseEdit = EditProposition(request.PropositionInfo);

            //assert
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(PatternMessages.ElkRetailPriceWrong);
        }

        [Test]
        public void EditProposition_WithWrongGasRetailPrice_Returns500()
        {
            //arrange
            const decimal newRetailPrice = 0.01m;
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            //act: create proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            request.PropositionInfo.Id = response.PropositionId;

            //edit proposition
            request.PropositionInfo.GasPrices.First().GasTariffs.First().RetailPrice = newRetailPrice;
            var responseEdit = EditProposition(request.PropositionInfo);

            //assert
            responseEdit.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            responseEdit.Header.Message.Should().BeEquivalentTo(PatternMessages.GasRetailPriceWrong);
        }
    }
}