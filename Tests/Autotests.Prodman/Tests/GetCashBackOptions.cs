﻿using System.Linq.Expressions;
using System.Net;
using Autotests.Clients;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using static System.String;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class GetCashBackOptions : BaseProdManTest
    {
        [Test]
        public void GetCashBackOptions_ValidTest_Returns200()
        {
            Expression<Func<Proposition, GasPrice, JoinSelector<Proposition, GasPrice>>> resultSelector =
                (o, i) => new JoinSelector<Proposition, GasPrice> { Outer = o, Inner = i };

            var proposition = ProdManDb.GetEntityByConditionWithJoin(
                    p => p.Id,
                    gp => gp.PropositionId, resultSelector,
                    res => res.Inner.CashBack > 50).Outer;
            var response = CreateGetCashBackOptionsRequest(proposition.Id, proposition.Name)
                .CallWith(ProdmanClient.Proxy.GetCashBackOptions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.CashBacks.Should().NotBeEmpty();
        }

        [Test]
        public void GetCashBackOptions_WithEmptyPropositionId_Returns500()
        {
            var response = CreateGetCashBackOptionsRequest(Guid.Empty)
                .CallWith(ProdmanClient.Proxy.GetCashBackOptions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.GetCashBackWithInvalidIdOrName);
        }

        [Test]
        public void GetCashBackOptions_WithNotExistedPropositionId_Returns500()
        {
            var response = CreateGetCashBackOptionsRequest(Guid.NewGuid())
                .CallWith(ProdmanClient.Proxy.GetCashBackOptions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.CashBacks.Should().BeNull();
        }

        [Test]
        public void GetCashBackOptions_WithEmptyPropositionName_Returns500()
        {
            var response = CreateGetCashBackOptionsRequest(name: Empty)
                .CallWith(ProdmanClient.Proxy.GetCashBackOptions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.GetCashBackWithInvalidIdOrName);
        }
    }
}