﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class GetProposition : BaseProdManTest
    {
        [Test]
        public void GetProposition_DefaultTest_Returns200()
        {
            //arrange
            var marketTo = DateTime.Now.AddYears(2);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    EnergySources, MarketFrom, marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            GetPropositionDetails(response.PropositionId).PropositionDetails.Should().NotBeNull();
        }
    }
}