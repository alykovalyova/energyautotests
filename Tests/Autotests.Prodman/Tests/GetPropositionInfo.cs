﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class GetPropositionInfo : BaseProdManTest
    {
        [Test]
        public void GetPropositionInfo_DefaultValidTest_Returns200()
        {
            var proposition = ProdManDb.GetEntityByCondition<Proposition>(p => p.StatusId == (int)Status.Active);
            var response = CreateGetPropositionInfoRequest(proposition.Id)
                .CallWith(ProdmanClient.Proxy.GetPropositionInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            var info = response.PropositionInfo;
            ObjectComparator.ComparePropsOfTypes(info, proposition).Should().BeTrue();
        }

        [Test]
        public void GetPropositionInfo_WithEmptyPropositionId_Returns500()
        {
            var response = CreateGetPropositionInfoRequest(Guid.Empty)
                .CallWith(ProdmanClient.Proxy.GetPropositionInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfo.Should().BeNull();
        }

        [Test]
        public void GetPropositionInfo_WithNotExistedPropositionId_Returns500()
        {
            var response = CreateGetPropositionInfoRequest(Guid.NewGuid())
                .CallWith(ProdmanClient.Proxy.GetPropositionInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfo.Should().BeNull();
        }
    }
}