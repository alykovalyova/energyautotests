﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class GetPropositionInfos : BaseProdManTest
    {
        [Test]
        public void GetPropositionInfos_ValidTest()
        {
            var proposition = ProdManDb.GetEntityByCondition<Repositories.ProdManModels.Proposition>(p => p.StatusId == (int)Status.Active);
            var response = CreateGetPropositionInfosRequest(proposition.Id)
                .CallWith(ProdmanClient.Proxy.GetPropositionInfos);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            var info = response.Propositions;
            ObjectComparator.ComparePropsOfTypes(info, proposition).Should().BeTrue();
        }

        [Test]
        public void GetPropositionInfos_WithEmptyPropositionId_Returns200()
        {
            var response = CreateGetPropositionInfosRequest(Guid.Empty)
                .CallWith(ProdmanClient.Proxy.GetPropositionInfos);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Propositions.Should().BeEmpty();
        }

        [Test]
        public void GetPropositionInfos_WithNotExistedPropositionId_Returns200()
        {
            var response = CreateGetPropositionInfosRequest(Guid.NewGuid())
                .CallWith(ProdmanClient.Proxy.GetPropositionInfos);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Propositions.Should().BeEmpty();
        }
    }
}
