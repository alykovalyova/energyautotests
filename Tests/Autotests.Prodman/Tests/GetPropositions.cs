﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class GetPropositions : BaseProdManTest
    {
        [Test]
        public void GetPropositions_DefaultValidTest_Returns200()
        {
            var propositions = new List<Proposition>();
            for (var i = 0; i < 5; i++)
            {
                var i1 = i;
                propositions.Add(ProdManDb.GetEntityByCondition<Proposition>(p => p.StatusId == i1));
            }

            var response = CreateGetPropositionsRequest(propositions.Where(p => p != null).Select(p => p.Id).ToList())
                .CallWith(ProdmanClient.Proxy.GetPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Propositions.Should().HaveCount(propositions.Where(p => p != null).ToList().Count);
        }

        [Test]
        public void GetPropositions_WhenPropositionsListIsEmpty_Returns500()
        {
            var response = CreateGetPropositionsRequest(new List<Guid>())
                .CallWith(ProdmanClient.Proxy.GetPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PropositionIdsIsEmpty);
        }

        [Test]
        public void GetPropositions_WithNotExistedIdInList_Returns500()
        {
            CreateGetPropositionsRequest(new List<Guid> { Guid.NewGuid() })
                .CallWith(ProdmanClient.Proxy.GetPropositions)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }
    }
}