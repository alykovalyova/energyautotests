﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Prodman.Base;
using static Autotests.Helper.ProdManFactory;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract.Transport;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class GetPropositionsCsv : BaseProdManTest
    {
        [Test]
        public void GetPropositionsCsv_ForOneProposition_Returns200()
        {
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, 
                    ThreeYearDuration, EnergySources, MarketFrom, DateTime.Now.AddYears(2), nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);

            var guidId = response.PropositionId;
            var getPropositionsCsvResponse = new GetPropositionsCsvRequest { PropositionIds = new List<Guid> { guidId } }
                .CallWith(ProdmanClient.Proxy.GetPropositionsCsv);

            getPropositionsCsvResponse.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            getPropositionsCsvResponse.CsvBytes.Should().NotBeEmpty();
        }

        [Test]
        public void GetPropositionsCsv_ForThreePropositions_Returns200()
        {
            var testPropositions = new List<AddPropositionResponse>();
            AddPropositionsToList(testPropositions);

            var guids = new List<Guid>();
            testPropositions.ForEach(p => guids.Add(p.PropositionId));
            
            var getPropositionsCsvResponse = new GetPropositionsCsvRequest{ PropositionIds = guids}
                .CallWith(ProdmanClient.Proxy.GetPropositionsCsv);

            getPropositionsCsvResponse.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            getPropositionsCsvResponse.CsvBytes.Should().NotBeEmpty();
        }
    }
}