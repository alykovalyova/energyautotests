﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static System.String;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class ReactivateProposition : BaseReactivatePropositionTest
    {
        private const string Comment = "reactivated_by_autotest";

        [Test, Ignore("no proper data exists")]
        public void ReactivateProposition_DefaultValidTest_Returns200()
        {
            var propositionToReactivate = ProdManDb.GetEntityByCondition<Proposition>(p =>
                p.StatusId == 3 && p.MarketTo < DateTime.Today);

            var response = CreateReactivatePropositionRequest(Comment, propositionToReactivate.Id, DateTime.Now.AddDays(45))
                .CallWith(ProdmanClient.Proxy.ReactivateProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == propositionToReactivate.Id).StatusId.Should().Be((byte)Status.Active);
        }

        [Test, Ignore("no proper data exists")]
        public void ReactivateProposition_WithInvalidMarketToDate_Returns500()
        {
            var marketToDate = DateTime.Now.AddDays(-1);
            var propositionToReactivate = ProdManDb.GetEntityByCondition<Proposition>(p =>
                p.StatusId == 3 && p.MarketTo < DateTime.Today);

            var response = CreateReactivatePropositionRequest(Comment, propositionToReactivate.Id, marketToDate)
                .CallWith(ProdmanClient.Proxy.ReactivateProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotReactivatePropWithInvalidDate, marketToDate.ToString("dd-MM-yyyy"));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void ReactivateProposition_WithInvalidPropositionStatus_Returns500([Values(0, 1, 4)] byte statusId)
        {
            var propositionToReactivate = ProdManDb.GetEntityByCondition<Proposition>(p => p.StatusId == statusId);

            var response = CreateReactivatePropositionRequest(Comment, propositionToReactivate.Id, DateTime.Now.AddDays(45))
                .CallWith(ProdmanClient.Proxy.ReactivateProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotReactivatePropWithInvalidStatus, (Status)statusId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void ReactivateProposition_WithMarketToLaterThanToday_Returns500()
        {
            var futureDate = DateTime.Now.AddDays(1);
            var propositionToReactivate = ProdManDb.GetEntityByCondition<Proposition>(p => p.StatusId == 3 && p.MarketTo > futureDate);

            var response = CreateReactivatePropositionRequest(Comment, propositionToReactivate.Id, DateTime.Now.AddDays(45))
                .CallWith(ProdmanClient.Proxy.ReactivateProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotReactivatePropWhenDateLater,
                propositionToReactivate.MarketTo.GetValueOrDefault().ToString("dd-MM-yyyy"));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void ReactivateProposition_WithInvalidPropositionId_Returns500()
        {
            var propositionId = Guid.NewGuid();
            var response = CreateReactivatePropositionRequest(Comment, propositionId, DateTime.Now.AddDays(45))
                .CallWith(ProdmanClient.Proxy.ReactivateProposition);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.CannotFindProposition, propositionId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}