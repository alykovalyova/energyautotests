﻿using System.Net;
using Autotests.Clients;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract.Transport;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class SearchProductPrices : BaseProdManTest
    {
        [Test, Pairwise]
        public void SearchProductPrices_DefaultValidTest_Returns200(
            [Values(TypeOfClient.Private, TypeOfClient.Business)] TypeOfClient type,
            [Values(Labels.BudgetEnergie, Labels.NLE)] Labels label,
            [Values(true, false)] bool isEligibleAsRenewal,
            [Values(true, false)] bool isSalesEligible,
            [Values(Status.Active, Status.Draft)] Status status)
        {
            var durations = new List<int> { 12, 36, 60 };

            var response = CreateSearchRequest<SearchProductPricesRequest>(type, label, durations,
                    isEligibleAsRenewal, isSalesEligible, status, DateTime.Now.AddYears(-2), DateTime.Now)
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.ProductPrices.Should().HaveCountGreaterOrEqualTo(0);
        }

        [Test]
        public void SearchProductPrices_ByClientTypeOnly_Returns200(
            [Values(TypeOfClient.Private, TypeOfClient.Business)] TypeOfClient type)
        {
            var response = CreateSearchRequest<SearchProductPricesRequest>(type, @from: DateTime.Now.AddMonths(-3))
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.ProductPrices.Should().HaveCountGreaterOrEqualTo(0);
        }

        [Test]
        public void SearchProductPrices_ByLabelOnly_Returns200(
            [Values(Labels.BudgetEnergie, Labels.NLE)] Labels label)
        {
            var response = CreateSearchRequest<SearchProductPricesRequest>(label: label, @from: DateTime.Now.AddMonths(-3))
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);
            var propositionId = response.ProductPrices.First().PropositionId;
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            if (response.ProductPrices.Count <= 0) return;
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == propositionId).LabelCode.Should().Be((byte)label);
        }

        [Test]
        public void SearchProductPrices_ByIsEligibleAsRenewalOnly_Returns200(
            [Values(true, false)] bool isEligibleAsRenewal)
        {
            var response = CreateSearchRequest<SearchProductPricesRequest>(isEligibleAsRenewal: isEligibleAsRenewal,
                    @from: DateTime.Now.AddMonths(-3))
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            if (response.ProductPrices.Count <= 0) return;

            var id = response.ProductPrices.First().PropositionId;
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == id).IsEligibleAsRenewal.Should().Be(isEligibleAsRenewal);
        }

        [Test]
        public void SearchProductPrices_ByIsSalesEligibleOnly_Returns200([Values(true, false)] bool isSalesEligible)
        {
            var response = CreateSearchRequest<SearchProductPricesRequest>(isSalesEligible: isSalesEligible,
                    @from: DateTime.Now.AddMonths(-3))
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            if (response.ProductPrices.Count <= 0) return;

            var id = response.ProductPrices.First().PropositionId;
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == id).IsSalesEligible.Should().Be(isSalesEligible);
        }

        [Test]
        public void SearchProductPrices_ByStatusOnly_Returns200(
            [Values(Status.Active, Status.Draft, Status.Approved, Status.Inactive, Status.Verifiable)] Status status)
        {
            var response = CreateSearchRequest<SearchProductPricesRequest>(status: status,
                    @from: DateTime.Now.AddMonths(-3))
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            if (response.ProductPrices.Count <= 0) return;

            var price = response.ProductPrices.First();
            (price.ElectricityPrices.FirstOrDefault()?.ProductPriceStatus == status
             || price.GasPrices.FirstOrDefault()?.ProductPriceStatus == status).Should().BeTrue();
        }

        [Test]
        public void SearchProductPrices_ByDateOnly_Returns200()
        {
            var from = DateTime.Now.AddMonths(-2);
            var to = DateTime.Now;
            var response = CreateSearchRequest<SearchProductPricesRequest>(@from: from, to: to)
                .CallWith(ProdmanClient.Proxy.SearchProductPrices);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            if (response.ProductPrices.Count <= 0) return;

            var id = response.ProductPrices.First().PropositionId;
            var propositionInfo = ProdManDb.GetEntityByCondition<Proposition>(p => p.Id == id);
            (propositionInfo.MarketFrom >= from && propositionInfo.MarketTo <= to).Should().BeTrue();
        }
    }
}