﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract;
using Nuts.ProdMan.Model.Contract.Transport;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class SearchPropositionInfo : BaseProdManTest
    {
        [Test]
        public void SearchPropositionInfo_ByClientType_Returns200()
        {
            const TypeOfClient testClientType = TypeOfClient.Private;
            const int duration = 12;
            const string searchText = "";
            const Status status = Status.Active;
            const bool isSalesEligible = false;
            const bool isEligibleRenewal = true;
            var fromDate = DateTime.Today.AddYears(-1);
            var toDate = DateTime.Today.AddYears(1);
            var label = Labels.BudgetEnergie;

            SearchPropositionInfo(testClientType, duration, fromDate, toDate, searchText, status,
                isSalesEligible, isEligibleRenewal, label).PropositionInfos
                .Should().Contain(i => i.ClientType.Contains(testClientType));
        }

        [Test]
        public void SearchPropositionInfo_ByDuration_Returns200()
        {
            const TypeOfClient typeOfClient = TypeOfClient.Business;
            const int testDuration = 36;
            var marketTo = DateTime.Today.AddYears(1);
            var nameProposition = "AutotestProposition " + RandomDataProvider.GetRandomLettersString(5);
            var elkPrices = GenerateElkProductPricesList(nameProposition, testDuration, MarketFrom, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, testDuration, MarketFrom, 
                GasRetailPrice, GasProfileCategories);

            CreateActivatedProposition(elkPrices, gasPrices, new List<TypeOfClient> {typeOfClient}, 
                testDuration, EnergySources, MarketFrom, marketTo, nameProposition);

            var response = new SearchPropositionInfosRequest
            {
                SearchInfo = new SearchCriteria {Durations = new List<int> {testDuration}}
            }.CallWith(ProdmanClient.Proxy.SearchPropositionInfos);

            response.Header.StatusCode.Should().Be((int) HttpStatusCode.OK);
            response.PropositionInfos.Should().Contain(i => i.Duration.Equals(testDuration));
        }

        [Test]
        public void SearchPropositionInfo_ByFromDate_Returns200()
        {
            const TypeOfClient clientType = TypeOfClient.Business;
            const int testDuration = 36;
            const Status propositionStatus = Status.Active;
            var testFromDate = DateTime.Now.AddDays(-5);

            var marketTo = DateTime.Now.AddYears(1);
            var nameProposition = "AutotestProposition " + RandomDataProvider.GetRandomLettersString(5);
            var elkPrices = GenerateElkProductPricesList(nameProposition, testDuration, testFromDate, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, testDuration, testFromDate, GasRetailPrice,
                GasProfileCategories);

            var propositionId = CreateActivatedProposition(
                elkPrices, gasPrices, new List<TypeOfClient> {clientType}, testDuration, 
                EnergySources, testFromDate, marketTo, nameProposition).PropositionId;
            var label = Labels.BudgetEnergie;

            var searchResponse = SearchPropositionInfo(clientType, testDuration, testFromDate.AddDays(-1), 
                marketTo.AddDays(1), "", propositionStatus, true, true, label);
            var result = searchResponse.PropositionInfos.Find(p => p.Id.Equals(propositionId));

            result.Should().NotBeNull();
            result.ClientType.Should().Contain(clientType);
            result.MarketTo.Should().Be(marketTo.Date);
            result.MarketFrom.Should().Be(testFromDate.Date);
            result.Duration.Should().Be(testDuration);
        }

        [Test]
        public void SearchPropositionInfo_ByToDate_Returns200()
        {
            const TypeOfClient clientType = TypeOfClient.Private;
            const int testDuration = 12;
            const Status propositionStatus = Status.Active;
            var testFromDate = DateTime.Today.AddYears(-2);
            var testToDate = DateTime.Today.AddYears(1);
            var marketTo = DateTime.Today.AddYears(1);
            var nameProposition = "AutotestProposition " + RandomDataProvider.GetRandomLettersString(5);
            var label = Labels.BudgetEnergie;

            var elkPrices = GenerateElkProductPricesList(nameProposition, testDuration, testFromDate, 
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, testDuration, testFromDate, 
                GasRetailPrice, GasProfileCategories);

            // Create a proposition before Search
            CreateActivatedProposition(
                elkPrices, gasPrices, new List<TypeOfClient> {clientType}, testDuration, 
                EnergySources, MarketFrom, marketTo, nameProposition);

            // Search for the proposition
            SearchPropositionInfo(clientType, testDuration, testFromDate, marketTo, 
                    "", propositionStatus, false, false, label)
                .PropositionInfos.First().MarketTo.GetValueOrDefault().CompareTo(testToDate).Should().BeLessThan(0);
        }

        [Test]
        public void SearchPropositionInfo_BySearchText_Returns200()
        {
            const string searchText = "Renewal";
            const TypeOfClient clientType = TypeOfClient.Private;
            const int duration = 12;
            const Status status = Status.Active;
            const bool isSalesEligible = false;
            const bool isEligibleAsRenewal = true;
            var fromDate = DateTime.Today.AddMonths(-7);
            var toDate = DateTime.Today.AddYears(1); 
            var label = Labels.BudgetEnergie;

            SearchPropositionInfo(clientType, duration, fromDate, toDate, 
                searchText, status, isSalesEligible, isEligibleAsRenewal, label).PropositionInfos
                .Should().Contain(i => i.Name.Contains(searchText));
        }

        [Test]
        public void SearchPropositionInfo_ByStatus_Returns200()
        {
            const TypeOfClient clientType = TypeOfClient.Private;
            const Status status = Status.Inactive;
            const int duration = 36;
            const string searchText = "";
            const bool isSalesEligible = false;
            const bool isEligibleAsRenewal = false;
            var fromDate = DateTime.Today.AddMonths(-7);
            var toDate = DateTime.Today.AddYears(1);
            var label = Labels.BudgetEnergie;

            SearchPropositionInfo(
                clientType, duration, fromDate, toDate, 
                searchText, status, isSalesEligible, isEligibleAsRenewal, label).PropositionInfos
                .Should().Contain(i => i.PropositionStatus.Equals(status));
        }

        [Test]
        public void SearchPropositionInfo_ByIsSalesEligible_Returns200()
        {
            const TypeOfClient clientType = TypeOfClient.Private;
            const int duration = 12;
            const string searchText = "";
            const Status status = Status.Active;
            const bool isSalesEligible = true;
            const bool isEligibleAsRenewal = true;
            var fromDate = DateTime.Today.AddYears(-1);
            var toDate = DateTime.Today.AddYears(1);
            var label = Labels.BudgetEnergie;           

            SearchPropositionInfo(
                clientType, duration, fromDate, toDate, searchText, status,
                isSalesEligible, isEligibleAsRenewal, label).PropositionInfos
                .Should().Contain(i => i.IsSalesEligible.Equals(isSalesEligible));
        }

        [Test]
        public void SearchPropositionInfo_NoResult_Returns200WIthEmptyList()
        {
            const TypeOfClient clientType = TypeOfClient.Private;
            const int duration = 12;
            const string searchText = "sldfjks";
            const Status status = Status.Active;
            const bool isSalesEligible = true;
            const bool isEligibleAsRenewal = true;
            var fromDate = DateTime.Today.AddYears(-1);
            var toDate = DateTime.Now.AddYears(1);
            var label = Labels.BudgetEnergie;

            SearchPropositionInfo(
                clientType, duration, fromDate, toDate, searchText, 
                status, isSalesEligible, isEligibleAsRenewal, label).PropositionInfos
                .Should().BeEmpty();
        }
    }
}