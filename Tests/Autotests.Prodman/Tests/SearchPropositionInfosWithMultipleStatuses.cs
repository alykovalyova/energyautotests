﻿using System.Net;
using Autotests.Clients;
using Autotests.Prodman.Base;
using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class SearchPropositionInfosWithMultipleStatuses : BaseProdManTest
    {
        [Test, Pairwise]
        public void SearchPropositionInfosWithMultipleStatuses_DefaultValidTest_Returns200(
            [Values(TypeOfClient.Private, TypeOfClient.Business)] TypeOfClient type,
            [Values(Labels.BudgetEnergie, Labels.NLE)] Labels label,
            [Values(true, false)] bool isEligibleAsRenewal,
            [Values(true, false)] bool isSalesEligible)
        {
            var durations = new List<int> { 12, 36, 60 };
            var statuses = new List<Status>
            {
                Status.Active, Status.Draft, Status.Approved, Status.Inactive, Status.Verifiable
            };
            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(type, label, durations,
                    isEligibleAsRenewal, isSalesEligible, statuses)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            using (new AssertionScope())
            {
                response.PropositionInfos.Should().NotBeEmpty();
                response.PropositionInfos
                    .Should().Contain(i => i.ClientType.Contains(type)).And.Subject
                    .Should().Contain(i => i.Label.Equals(label)).And.Subject
                    .Should().Contain(i => i.IsEligibleAsRenewal.Equals(isEligibleAsRenewal)).And.Subject
                    .Should().Contain(i => i.IsSalesEligible.Equals(isSalesEligible)).And.Subject
                    .Should().Contain(i => durations.Contains(i.Duration)).And.Subject
                    .Should().Contain(i => statuses.Contains(i.PropositionStatus));
            }
        }

        [Test]
        public void SearchPropositionInfosWithMultipleStatuses_ByLabelOnly_Returns200(
            [Values(Labels.BudgetEnergie, Labels.NLE)] Labels label)
        {
            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(label: label)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfos.Should().Contain(i => i.Label.Equals(label));
        }

        [Test]
        public void SearchPropositionInfosWithMultipleStatuses_ByClientTypeOnly_Returns200(
            [Values(TypeOfClient.Business, TypeOfClient.Private)] TypeOfClient type)
        {
            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(type)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfos.Should().Contain(i => i.ClientType.Contains(type));
        }

        [Test]
        public void SearchPropositionInfosWithMultipleStatuses_ByIsEligibleAsRenewalOnly_Returns200(
            [Values(true, false)] bool isEligibleAsRenewal)
        {
            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(isEligibleAsRenewal: isEligibleAsRenewal)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfos.Should().Contain(i => i.IsEligibleAsRenewal.Equals(isEligibleAsRenewal));
        }

        [Test]
        public void SearchPropositionInfosWithMultipleStatuses_ByIsSalesEligibleOnly_Returns200(
            [Values(true, false)] bool isSalesEligible)
        {
            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(isSalesEligible: isSalesEligible)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfos.Should().Contain(i => i.IsSalesEligible.Equals(isSalesEligible));
        }

        [Test]
        public void SearchPropositionInfosWithMultipleStatuses_ByStatusOnly_Returns200()
        {
            var statuses = new List<Status> { Status.Active, Status.Draft };
            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(statuses: statuses)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.PropositionInfos.Should().Contain(i => statuses.Contains(i.PropositionStatus));
        }

        [Test]
        public void SearchPropositionInfosWithMultipleStatuses_ByDateOnly_Returns200()
        {
            var from = DateTime.Now.AddMonths(-3);

            var response = CreateSearchPropositionInfosWithMultipleStatusesRequest(@from: from)
                .CallWith(ProdmanClient.Proxy.SearchPropositionInfosWithMultipleStatuses);

            var info = response.PropositionInfos.First();
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            (info.MarketFrom >= from).Should().BeTrue();
        }
    }
}