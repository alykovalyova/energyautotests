﻿using System.Net;
using Autotests.Clients;
using Autotests.Prodman.Base;
using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract.Transport;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class SearchPropositions : BaseSearchPropositionTest
    {
        [Test, Pairwise]
        public void SearchPropositions_DefaultValidTest_Returns200(
            [Values(TypeOfClient.Private, TypeOfClient.Business)] TypeOfClient type,
            [Values(Labels.BudgetEnergie)] Labels label,
            [Values(Status.Active, Status.Draft, Status.Inactive)] Status status)
        {
            AddProposition();
            var response = CreateSearchRequest<SearchPropositionsRequest>(type, label, status: status)
                .CallWith(ProdmanClient.Proxy.SearchPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            using (new AssertionScope())
            {
                response.Propositions
                    .Should().NotBeEmpty().And.Subject
                    .Should().Contain(p => p.ClientType.Contains(type)).And.Subject
                    .Should().Contain(p => p.Label.Equals(label)).And.Subject
                    .Should().Contain(p => p.PropositionStatus.Equals(status));
            }
        }

        [Test]
        public void SearchPropositions_ByClientTypeOnly_Returns200(
            [Values(TypeOfClient.Private, TypeOfClient.Business)] TypeOfClient type)
        {
            var from = DateTime.Now.AddMonths(-6);

            var response = CreateSearchRequest<SearchPropositionsRequest>(type, @from: from)
                .CallWith(ProdmanClient.Proxy.SearchPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Propositions.Should().Contain(p => p.ClientType.Contains(type));
        }

        [Test]
        public void SearchPropositions_ByLabelOnly_Returns200(
            [Values(Labels.BudgetEnergie)] Labels label)
        {
            var from = DateTime.Now.AddMonths(-6);

            var response = CreateSearchRequest<SearchPropositionsRequest>(label: label, @from: from)
                .CallWith(ProdmanClient.Proxy.SearchPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Propositions.Should().Contain(p => p.Label.Equals(label));
        }

        [Test]
        public void SearchPropositions_ByStatusOnly_Returns200(
                [Values(Status.Active, Status.Draft)] Status status)
        {
            var response = CreateSearchRequest<SearchPropositionsRequest>(status: status)
                .CallWith(ProdmanClient.Proxy.SearchPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.Propositions.Should().Contain(p => p.PropositionStatus.Equals(status));
        }

        [Test]
        public void SearchPropositions_ByDateOnly_Returns200()
        {
            var from = DateTime.Now.AddMonths(-3);

            var response = CreateSearchRequest<SearchPropositionsRequest>(@from: from)
                .CallWith(ProdmanClient.Proxy.SearchPropositions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            var p = response.Propositions.First();
            (p.MarketFrom >= from).Should().BeTrue();
        }
    }
}