﻿using System.Net;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using static System.String;
using ElectricityPrice = Autotests.Repositories.ProdManModels.ElectricityPrice;
using GasPrice = Autotests.Repositories.ProdManModels.GasPrice;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;
using Autotests.Clients;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class UpdateProductPriceStatus : BaseProdManTest
    {
        private static readonly DateTime _marketTo = DateTime.Now.AddDays(+75);

        [Test]
        public void UpdateProductPriceStatus_FromDraftToVerifiableStatusIfPropositionIsActive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act: create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, GasEnergySources, MarketFrom, _marketTo, nameProposition);

            //add active product price
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            var addPriceResponse = AddProductPriceToProposition(addPropositionResponse.PropositionId, newGasPrice, PriceMarketFrom);
            newGasPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newGasPrice);

            //assert
            ProdManDb.GetEntityByCondition<GasPrice>(gp => gp.Code == newGasPrice.Code).StatusId
                .Should().Be((byte)Status.Verifiable);
        }

        [Test]
        public void UpdateProductPriceStatus_FromDraftToVerifiableStatusIfPropositionIsInactive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //act: create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, ElektraEnergySources, MarketFrom, _marketTo, nameProposition);
            CreateUpdatePropositionStatusRequest(Status.Inactive, addPropositionResponse.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addPriceResponse = AddProductPriceToProposition(
                addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .Should().Be((byte)Status.Verifiable);
        }

        [Test]
        public void UpdateProductPriceStatus_FromDraftToApprovedStatus_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration,
                GasEnergySources, MarketFrom, _marketTo, nameProposition);
            CreateUpdatePropositionStatusRequest(Status.Inactive, addPropositionResponse.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //add product price
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newGasPrice, PriceMarketFrom);
            newGasPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            var message = Format(PatternMessages.NotPossibleToChangePriceStatus, Status.Draft, Status.Approved,
                PriceMarketFrom.ToString("dd-MM-yyyy"));
            UpdateProductPriceStatus(Status.Approved, newGasPrice, HttpStatusCode.InternalServerError, message);
        }

        [Test]
        public void UpdateProductPriceStatus_FromDraftToActiveStatus_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration,
                GasEnergySources, MarketFrom, _marketTo, nameProposition);

            //add product price
            var newGasPrice = GetGasPrice(nameProposition, ThreeYearDuration, GasRetailPrice,
                PriceMarketFrom, GasProfileCategories);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newGasPrice, PriceMarketFrom);
            newGasPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            var message = Format(PatternMessages.NotPossibleToChangePriceStatus, Status.Draft, Status.Active,
                PriceMarketFrom.ToString("dd-MM-yyyy"));
            UpdateProductPriceStatus(Status.Active, newGasPrice, HttpStatusCode.InternalServerError, message);
        }

        [Test]
        public void UpdateProductPriceStatus_FromVerifiableToDraftStatusIfPropositionIsActive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);

            //add active product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Draft, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .Should().Be((byte)Status.Draft);
        }

        [Test]
        public void UpdateProductPriceStatus_FromVerifiableToDraftStatusIfPropositionIsInactive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);
            CreateUpdatePropositionStatusRequest(Status.Inactive, addPropositionResponse.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Draft, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .Should().Be((byte)Status.Draft);
        }

        [Test]
        public void UpdateProductPriceStatus_FromVerifiableToApprovedStatusIfPropositionIsActive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);

            //add active product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .Should().Be((byte)Status.Approved);
        }

        [Test]
        public void UpdateProductPriceStatus_FromVerifiableToApprovedStatusIfPropositionIsInactive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);
            CreateUpdatePropositionStatusRequest(Status.Inactive, addPropositionResponse.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //add active product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .Should().Be((byte)Status.Approved);
        }

        [Test]
        public void UpdateProductPriceStatus_FromVerifiableToActiveStatusIfPropositionIsActive_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            var message = Format(PatternMessages.NotPossibleToChangePriceStatus, Status.Verifiable, Status.Active,
                PriceMarketFrom.ToString("dd-MM-yyyy"));
            UpdateProductPriceStatus(Status.Active, newElkPrice, HttpStatusCode.InternalServerError, message);
        }

        [Test]
        public void UpdateProductPriceStatus_FromApprovedToDraftStatusIfPropositionIsInactive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, ElektraEnergySources, MarketFrom, _marketTo, nameProposition);
            CreateUpdatePropositionStatusRequest(Status.Inactive, addPropositionResponse.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Absolute, ElkAbsoluteDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            addPriceResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);
            UpdateProductPriceStatus(Status.Draft, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .As<Status>().Should().BeEquivalentTo(Status.Draft);
        }

        [Test]
        public void UpdateProductPriceStatus_FromApprovedToDraftStatusIfPropositionIsActive_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration,
                PriceMarketFrom, ElkRetailPrice, ElkProfileCategories,
                PriceDiscountType.Percentage, PercentageDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            addPriceResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Inactive status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);
            UpdateProductPriceStatus(Status.Draft, newElkPrice);

            //assert
            ProdManDb.GetEntityByCondition<ElectricityPrice>(ep => ep.Code == newElkPrice.Code).StatusId
                .As<Status>().Should().BeEquivalentTo(Status.Draft);
        }

        [Test]
        public void UpdateProductPriceStatus_FromApprovedToActiveStatusIfPropositionIsActive_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            addPriceResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Approved status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);
            var message = Format(PatternMessages.NotPossibleToChangePriceStatus, Status.Approved,
                Status.Active, PriceMarketFrom.ToString("dd-MM-yyyy"));
            UpdateProductPriceStatus(Status.Active, newElkPrice, HttpStatusCode.InternalServerError, message);
        }

        [Test]
        public void UpdateProductPriceStatus_FromApprovedToActiveStatusIfPropositionIsInactive_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, GasRetailPrice,
                GasProfileCategories);

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, EnergySources, MarketFrom, _marketTo, nameProposition);
            CreateUpdatePropositionStatusRequest(Status.Inactive, addPropositionResponse.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //add product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;

            //update price to Approved status
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);
            var message = Format(PatternMessages.NotPossibleToChangePriceStatus, Status.Approved,
                Status.Active, PriceMarketFrom.ToString("dd-MM-yyyy"));
            UpdateProductPriceStatus(Status.Active, newElkPrice, HttpStatusCode.InternalServerError, message);
        }

        [Test]
        public void UpdateProductPriceStatus_FromApprovedToInactiveStatus_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom, ElkRetailPrice,
                ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = new List<ProductPricesToSave.GasPrice>();

            //create proposition
            var addPropositionResponse = CreateActivatedProposition(elkPrices, gasPrices, TypesOfClient,
                ThreeYearDuration, ElektraEnergySources, MarketFrom, _marketTo, nameProposition);

            //add approved product price
            var newElkPrice = GetElectricityPrice(nameProposition, ThreeYearDuration, PriceMarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var addPriceResponse =
                AddProductPriceToProposition(addPropositionResponse.PropositionId, newElkPrice, PriceMarketFrom);
            newElkPrice.Id = addPriceResponse.ProductPriceId;
            UpdateProductPriceStatus(Status.Verifiable, newElkPrice);
            UpdateProductPriceStatus(Status.Approved, newElkPrice);

            //update price to Inactive status
            var message = Format(PatternMessages.NotPossibleToChangePriceStatus, Status.Approved,
                Status.Inactive, PriceMarketFrom.ToString("dd-MM-yyyy"));
            UpdateProductPriceStatus(Status.Inactive, newElkPrice, HttpStatusCode.InternalServerError, message);
        }
    }
}