﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prodman.Base;
using Autotests.Repositories.ProdManModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract.Transport;
using static System.String;
using ProductPricesToSave = Nuts.ProdMan.Model.Contract.ProductPricesToSave;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using static Autotests.Helper.ProdManFactory;

namespace Autotests.Prodman.Tests
{
    [TestFixture]
    internal class UpdatePropositionStatus : BaseProdManTest
    {
        private static readonly DateTime _marketTo = DateTime.Today.AddDays(+35);

        [Test]
        public void UpdatePropositionStatus_ToActiveStatus_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, VariableDuration,
                MarketFrom, GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, VariableDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);

            var getPropositionDetailsResponse = new GetPropositionDetailsRequest
            {
                PropositionId = response.PropositionId
            }.CallWith(ProdmanClient.Proxy.GetProposition);

            //assert
            getPropositionDetailsResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            getPropositionDetailsResponse.PropositionDetails.PropositionStatus.Should().BeEquivalentTo(Status.Active);
        }

        [Test]
        public void UpdatePropositionStatus_FromActiveToInactiveStatus_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);
            ChangePropositionStatus(response.PropositionId, Status.Inactive);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId
                .Should().Be((byte)Status.Inactive);
        }

        [Test]
        public void UpdatePropositionStatus_FromInactiveToActiveStatus_Returns200()
        {
            //arrange
            var marketFromDate = DateTime.Today.AddDays(-13);
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, marketFromDate,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, marketFromDate, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ActivateProposition(response.PropositionId);
            ChangePropositionStatus(response.PropositionId, Status.Inactive);
            ChangePropositionStatus(response.PropositionId, Status.Active);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId
                .Should().Be((byte)Status.Active);
        }

        [Test]
        public void UpdatePropositionStatus_FromVerifiableToDraftStatus_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            ChangePropositionStatus(response.PropositionId, Status.Verifiable);
            CreateUpdatePropositionStatusRequest(Status.Draft, response.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId
                .Should().Be((byte)Status.Draft);
        }

        [Test]
        public void UpdatePropositionStatus_FromVerifiableToApprovedStatus_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ChangePropositionStatus(response.PropositionId, Status.Approved);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId
                .Should().Be((byte)Status.Approved);
        }

        [Test]
        public void UpdatePropositionStatus_FromApprovedToDraftStatus_Returns200()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            ChangePropositionStatus(response.PropositionId, Status.Approved);
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId
                .Should().Be((byte)Status.Approved);

            CreateUpdatePropositionStatusRequest(Status.Draft, response.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            //assert
            ProdManDb.GetEntityByCondition<Proposition>(p => p.Name == nameProposition).StatusId
                .Should().Be((byte)Status.Draft);
        }

        [Test]
        public void UpdatePropositionStatus_FromDraftToApprovedStatus_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            var updateResponse = CreateUpdatePropositionStatusRequest(Status.Approved, response.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus);

            updateResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.NotPossibleToChangePropositionStatus, Status.Draft, Status.Approved,
                MarketFrom.ToString("dd-MM-yyyy"));
            updateResponse.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdatePropositionStatus_FromApprovedToVerifiableStatus_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            ChangePropositionStatus(response.PropositionId, Status.Approved);
            var updateResponse = CreateUpdatePropositionStatusRequest(Status.Verifiable, response.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus);
            updateResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.NotPossibleToChangePropositionStatus, Status.Approved,
                Status.Verifiable, MarketFrom.ToString("dd-MM-yyyy"));
            updateResponse.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdatePropositionStatus_FromApprovedToActiveStatusIfMarketToIsLessThanToday_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = GenerateElkProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                ElkRetailPrice, ElkProfileCategories, PriceDiscountType.Percentage, PercentageDiscount);
            var gasPrices = GenerateGasProductPricesList(nameProposition, ThreeYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //create proposition
            var request = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, ThreeYearDuration,
                EnergySources, MarketFrom, _marketTo, nameProposition);
            request.PropositionInfo.MarketTo = _marketTo.AddDays(-41);
            var response = request.CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            request.PropositionInfo.Id = response.PropositionId;

            ChangePropositionStatus(response.PropositionId, Status.Approved);
            var updateResponse = CreateUpdatePropositionStatusRequest(Status.Active, response.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus);
            updateResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.NotPossibleToChangePropositionStatus, Status.Approved, Status.Active,
                MarketFrom.ToString("dd-MM-yyyy"));
            updateResponse.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdatePropositionStatus_FromActiveToApprovedStatus_Returns500()
        {
            //arrange
            var nameProposition = RandomDataProvider.GetRandomNumCharString(25);
            var elkPrices = new List<ProductPricesToSave.ElectricityPrice>();
            var gasPrices = GenerateGasProductPricesList(nameProposition, OneYearDuration, MarketFrom,
                GasRetailPrice, GasProfileCategories);

            //act
            var response = CreateAddPropositionRequest(elkPrices, gasPrices, TypesOfClient, OneYearDuration,
                    GasEnergySources, MarketFrom, _marketTo, nameProposition)
                .CallWith(ProdmanClient.Proxy.AddProposition);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            ActivateProposition(response.PropositionId);
            var updateResponse = CreateUpdatePropositionStatusRequest(Status.Approved, response.PropositionId)
                .CallWith(ProdmanClient.Proxy.UpdatePropositionStatus);
            updateResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = Format(PatternMessages.NotPossibleToChangePropositionStatus, Status.Active, Status.Approved,
                MarketFrom.ToString("dd-MM-yyyy"));
            updateResponse.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}