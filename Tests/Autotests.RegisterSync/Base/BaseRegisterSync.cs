﻿using System.Net;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.RegisterSync.Enums;
using Autotests.Repositories.RegisterSyncModels;
using NUnit.Framework;
using Nuts.RegisterSynchronization.Contract;
using EnumLabel = Autotests.Repositories.RegisterSyncModels.EnumLabel;
using Report = Autotests.Repositories.RegisterSyncModels.Report;
using ReportData = Nuts.RegisterSynchronization.Contract.CreateReport;


namespace Autotests.RegisterSync.Base
{
    [SetUpFixture, Category(Categories.RegisterSync)]
    internal abstract class BaseRegisterSync : AllureReport
    {
        //clients declaration
        protected NutsHttpClient RegisterSyncClient;

        //databases declaration
        protected DbHandler<RegisterSyncContext> RegisterSyncDb;

        //variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        protected const string AutotestMarker = "Autotest_";
        protected const string DefaultUpdateAssignedTo = "Updated_AssignedTo";
        protected const MismatchStatus DefaultUpdateMismatchStatus = MismatchStatus.Fixed;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            RegisterSyncClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.RegisterSync));
            RegisterSyncDb = new DbHandler<RegisterSyncContext>(_configHandler.GetConnectionString(DbConnectionName.RegisterSyncDatabase.ToString()));
        }

        protected static CreateReportRequest GetCreateReportRequest(string comment = null, string createdBy = null)
        {
            return new CreateReportRequest
            {
                ReportData = new ReportData
                {
                    Comment = comment,
                    CreatedBy = createdBy
                }
            };
        }

        protected void CreateReport(string comment, string createdBy)
        {
            var createReportRequest = GetCreateReportRequest(comment, createdBy);
            var createReportResponse = RegisterSyncClient.MakeRequest(createReportRequest);
            Assert.That(createReportResponse.Header.StatusCode, Is.EqualTo(HttpStatusCode.OK),
                createReportResponse.Header.Message);
        }

        protected static UpdateMismatchStatusRequest CreateUpdateMismatchStatusRequest(
            string status = null, string assignedTo = null, int? eanMismatchId = null)
        {
            var request = new UpdateMismatchStatusRequest
            {
                MismatchData = new UpdateMismatch
                {
                    AssignedTo = assignedTo,
                    Status = status
                }
            };
            if (eanMismatchId != null)
                request.MismatchData.MismatchId = eanMismatchId.GetValueOrDefault();

            return request;
        }

        protected EanMismatch AddMismatchToDb(MismatchStatus status)
        {
            var statusId = RegisterSyncDb
                .GetEntityByCondition<EnumMismatchStatus>(ems => ems.Identifier == status.ToString()).Id;
            var random = new Random();
            var check = RegisterSyncDb.EntityIsInDb<EanMismatch>(em => em.StatusId == statusId);
            var checkStatuses = RegisterSyncDb.EntityIsInDb<EnumMismatchStatus>(ems => true);
            var checkLabels = RegisterSyncDb.EntityIsInDb<EnumLabel>(el => true);

            if (!checkStatuses || !checkLabels)
                throw new IgnoreException("No statuses or labels in DB.");
            if (check)
                return RegisterSyncDb.GetEntitiesByCondition<EanMismatch>(em => em.StatusId == statusId).RandomItem();

            var mismatch = new EanMismatch
            {
                Ean = $"000{random.Next(000000000, 999999999)}{random.Next(000000, 999999)}",
                StatusId = statusId,
                AssignedTo = "Autotest_User",
                ReportId = RegisterSyncDb.GetEntitiesByCondition<Report>(r => true).RandomItem().Id,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                EdsnLabelCodeId = RegisterSyncDb
                    .GetEntityByCondition<EnumLabel>(el => el.Identifier == Label.BudgetEnergie.ToString()).Id
            };
            return RegisterSyncDb.AddEntityToDb(mismatch);
        }

        protected static UpdateReportRequest GetUpdateReportRequest(int reportId, string assignedTo, int mismatchId,
            MismatchStatus status)
        {
            return new UpdateReportRequest
            {
                MismatchesData = new List<UpdateMismatch>
                {
                    new UpdateMismatch
                    {
                        AssignedTo = assignedTo,
                        MismatchId = mismatchId,
                        Status = status.ToString()
                    }
                },
                ReportId = reportId
            };
        }
    }
}