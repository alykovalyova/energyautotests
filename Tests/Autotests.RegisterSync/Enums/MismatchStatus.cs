﻿namespace Autotests.RegisterSync.Enums
{
    internal enum MismatchStatus
    {
        New = 1,
        InProgress,
        Fixed
    }
}