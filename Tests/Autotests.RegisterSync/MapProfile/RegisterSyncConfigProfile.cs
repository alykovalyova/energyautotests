﻿using AutoMapper;
using Autotests.Repositories.RegisterSyncModels;

namespace Autotests.RegisterSync.MapProfile
{
    public class RegisterSyncConfigProfile : Profile
    {
        public RegisterSyncConfigProfile()
        {
            CreateMap<Report, Nuts.RegisterSynchronization.Contract.Report>()
                .ForMember(dest => dest.MismatchesCount, opt => opt.Ignore())
                .ForMember(dest => dest.FoundMismatchesCount, opt => opt.Ignore())
                .ForMember(dest => dest.Labels, opt => opt.Ignore())
                .ForMember(dest => dest.Mismatches, opt => opt.Ignore());
        }
    }
}