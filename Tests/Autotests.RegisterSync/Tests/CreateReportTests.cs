﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.RegisterSync.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.RegisterSynchronization.Contract;
using Report = Autotests.Repositories.RegisterSyncModels.Report;

namespace Autotests.RegisterSync.Tests
{
    [TestFixture]
    internal class CreateReportTests : BaseRegisterSync
    {
        [Test, Order(1), Ignore("Operation runs too long.")]
        public void CreateReport_DefaultPositiveCase_Returns200()
        {
            var comment = $"{AutotestMarker}{Guid.NewGuid().ToString().Remove(5)}";
            var createdBy = $"{AutotestMarker}{Guid.NewGuid().ToString().Remove(5)}";
            CreateReport(comment, createdBy);

            var report = RegisterSyncDb.GetEntityByCondition<Report>(r => r.CreatedBy == createdBy);
            report.Should().NotBeNull();

            new GetReportRequest { ReportId = report.Id }.CallWith(RegisterSyncClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
        }

        [Test]
        [TestCase("", TestName = "CreateReport_WithEmptyCreatedByField_Returns400")]
        [TestCase(null, TestName = "CreateReport_WithNullCreatedByField_Returns400")]
        public void CreateReport_WithInvalidCreatedByField_Returns400(string createdBy)
        {
            var comment = $"{AutotestMarker}{Guid.NewGuid().ToString().Remove(5)}";
            var response = GetCreateReportRequest(comment, createdBy).CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.CreatedByIsRequired);
        }

        [Test]
        [TestCase("", TestName = "CreateReport_WithEmptyCommentField_Returns400")]
        [TestCase(null, TestName = "CreateReport_WithNullCommentField_Returns400")]
        public void CreateReport_WithInvalidCommentField_Returns400(string comment)
        {
            var createdBy = $"{AutotestMarker}{Guid.NewGuid().ToString().Remove(5)}";
            var response = GetCreateReportRequest(comment, createdBy).CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.CommentIsRequired);
        }

        [Test]
        public void CreateReport_WithoutAnyFields_Returns400()
        {
            var response = GetCreateReportRequest().CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.CommentIsRequired);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.CreatedByIsRequired);
        }

        [Test]
        public void CreateReport_WithEmptyReportObject_Returns400()
        {
            var request = GetCreateReportRequest();
            request.ReportData = null;

            var response = request.CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ReportDataIsRequired);
        }
    }
}