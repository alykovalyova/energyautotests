﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.RegisterSync.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.RegisterSynchronization.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Report = Autotests.Repositories.RegisterSyncModels.Report;

namespace Autotests.RegisterSync.Tests
{
    [TestFixture]
    internal class GetReportTests : BaseRegisterSync
    {
        [Test]
        public void GetReport_DefaultPositiveCase_Returns200()
        {
            var report = RegisterSyncDb.GetEntityByCondition<Report>(r => r.CreatedManually);
            report.Should().NotBeNull();

            var response = new GetReportRequest { ReportId = report.Id }
                .CallWith<GetReportRequest, GetReportResponse>(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Report.Should().NotBeNull();
            response.Data.Report.CreatedManually.Should().BeTrue();
            ObjectComparator
                .ComparePropsOfTypes(response.Data.Report, report.To<Nuts.RegisterSynchronization.Contract.Report>())
                .Should().BeTrue();
        }

        [Test]
        [TestCase(0, TestName = "GetReport_WithNotExistedReportId_Returns404")]
        [TestCase(null, TestName = "GetReport_WithNullReportId_Returns400")]
        public void GetReport_WithInvalidReportId_Returns400(int? reportId)
        {
            var request = reportId != null
                ? new GetReportRequest { ReportId = reportId.GetValueOrDefault() }
                : new GetReportRequest();
            var response = request.CallWith<GetReportRequest, GetReportResponse>(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.NoReportFound.Replace("{0}", ""));
        }
    }
}