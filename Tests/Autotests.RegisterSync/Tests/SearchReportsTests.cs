﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.RegisterSync.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.RegisterSynchronization.Contract;
using Report = Autotests.Repositories.RegisterSyncModels.Report;

namespace Autotests.RegisterSync.Tests
{
    [TestFixture]
    internal class SearchReportsTests : BaseRegisterSync
    {
        [Test]
        public void SearchReports_DefaultPositiveCase_Returns200()
        {
            var reportFromDb = RegisterSyncDb.GetEntityByCondition<Report>(r => true);
            var response = new SearchReportsRequest
            {
                FromDate = reportFromDb.ExtractionDate,
                ToDate = DateTime.Now
            }.CallWith<SearchReportsRequest, SearchReportsResponse>(RegisterSyncClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Reports.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void SearchReports_ToDateLessThanFromDate_Returns400()
        {
            var response = new SearchReportsRequest
            {
                FromDate = DateTime.Now,
                ToDate = DateTime.Now.AddDays(-1)
            }.CallWith<SearchReportsRequest, SearchReportsResponse>(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.ToDateLessThanFromDate);
        }

        [Test]
        public void SearchReports_WithInvalidDates_ReturnsEmptyList()
        {
            var response = new SearchReportsRequest
            {
                FromDate = DateTime.Now,
                ToDate = DateTime.Now
            }.CallWith<SearchReportsRequest, SearchReportsResponse>(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Reports.Should().BeEmpty();
        }

        [Test]
        public void SearchReports_WithDateOnly_Returns200()
        {
            var response = new SearchReportsRequest
            {
                ToDate = DateTime.Now
            }.CallWith<SearchReportsRequest, SearchReportsResponse>(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Reports.Should().NotBeEmpty();
        }
    }
}