﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.RegisterSync.Base;
using Autotests.RegisterSync.Enums;
using Autotests.Repositories.RegisterSyncModels;
using FluentAssertions;
using NUnit.Framework;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using Report = Autotests.Repositories.RegisterSyncModels.Report;

namespace Autotests.RegisterSync.Tests
{
    [TestFixture]
    internal class UpdateReportTests : BaseRegisterSync
    {
        [Test, Ignore("not working currently")]
        public void UpdateReport_DefaultValidCase_Returns200([Values(MismatchStatus.Fixed, MismatchStatus.InProgress)]
            MismatchStatus status)
        {
            const string assignedTo = DefaultUpdateAssignedTo;
            var reportFromDb = RegisterSyncDb.GetEntityByCondition<Report>(
                r => r.EanMismatch.Count > 0, "EanMismatch");
            var mismatchId = reportFromDb.EanMismatch.RandomItem().Id;

            GetUpdateReportRequest(reportFromDb.Id, assignedTo, mismatchId, status)
                .CallWith(RegisterSyncClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            
            var updatedMismatch = Waiter.Wait(() => 
                RegisterSyncDb.GetEntityByCondition<EanMismatch>(em => em.Id == mismatchId, "Status"), 10);
            updatedMismatch.Status.Identifier.Should().BeEquivalentTo(status.ToString());
            updatedMismatch.AssignedTo.Should().BeEquivalentTo(assignedTo);
        }

        [Test]
        public void UpdateReport_WithNotExistedReportId_Returns404()
        {
            const int reportId = int.MaxValue - 1;
            const string assignedTo = DefaultUpdateAssignedTo;
            const MismatchStatus status = DefaultUpdateMismatchStatus;
            const int mismatchId = 12345;

            var response = GetUpdateReportRequest(reportId, assignedTo, mismatchId, status).CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.NoReportFound, $" {reportId}");
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdateReport_WithNullMismatchDataList_Returns400()
        {
            var reportId = RegisterSyncDb.GetEntityByCondition<Report>(r => true).Id;
            const string assignedTo = DefaultUpdateAssignedTo;
            const MismatchStatus status = DefaultUpdateMismatchStatus;
            const int mismatchId = 12345;

            var request = GetUpdateReportRequest(reportId, assignedTo, mismatchId, status);
            request.MismatchesData = null;
            var response = request.CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.MismatchesDataIsRequired);
        }

        [Test]
        public void UpdateReport_WithInvalidMismatchId_Returns400()
        {
            var reportId = RegisterSyncDb.GetEntityByCondition<Report>(r => true).Id;
            const string assignedTo = DefaultUpdateAssignedTo;
            const MismatchStatus status = DefaultUpdateMismatchStatus;
            const int mismatchId = 0;

            var response = GetUpdateReportRequest(reportId, assignedTo, mismatchId, status).CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.WrongMismatchId);
        }

        [Test]
        public void UpdateReport_WithNotExistedMismatchId_Returns404()
        {
            var reportId = RegisterSyncDb.GetEntityByCondition<Report>(r => true).Id;
            const string assignedTo = DefaultUpdateAssignedTo;
            const MismatchStatus status = DefaultUpdateMismatchStatus;
            const int mismatchId = int.MaxValue - 1;

            var response = GetUpdateReportRequest(reportId, assignedTo, mismatchId, status).CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.MismatchNotFound, mismatchId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdateReport_WithEmptyStatus_Returns400([Values("", null)] string newStatus)
        {
            var reportId = RegisterSyncDb.GetEntityByCondition<Report>(r => true).Id;
            const string assignedTo = DefaultUpdateAssignedTo;
            const MismatchStatus status = DefaultUpdateMismatchStatus;
            const int mismatchId = 1234;

            var request = GetUpdateReportRequest(reportId, assignedTo, mismatchId, status);
            request.MismatchesData[0].Status = newStatus;
            var response = request.CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.StatusIsRequired);
        }

        [Test]
        public void UpdateReport_WithEmptyAssignedTo_Returns400([Values("", null)] string newAssignedTo)
        {
            var reportId = RegisterSyncDb.GetEntityByCondition<Report>(r => true).Id;
            const string assignedTo = DefaultUpdateAssignedTo;
            const MismatchStatus status = DefaultUpdateMismatchStatus;
            const int mismatchId = 1234;

            var request = GetUpdateReportRequest(reportId, assignedTo, mismatchId, status);
            request.MismatchesData[0].AssignedTo = newAssignedTo;
            var response = request.CallWith(RegisterSyncClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.AssignedToRequired);
        }
    }
}