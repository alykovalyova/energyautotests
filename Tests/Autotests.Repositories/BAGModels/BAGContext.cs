﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.BAGModels
{
    public partial class BAGContext : DbContext
    {
        private readonly string _connectionString;

        public BAGContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public BAGContext(DbContextOptions<BAGContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BagData> BagData { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BagData>(entity =>
            {
                entity.HasKey(e => e.Perceelid)
                    .HasName("PK__bag_data__C7B7434B235AB04E");

                entity.ToTable("bag_data", "dbo");

                entity.HasIndex(e => new { e.Huisnr, e.Wijkcode });

                entity.HasIndex(e => new { e.Huisnr, e.HuisnrBagLetter, e.HuisnrBagToevoeging, e.Wijkcode, e.Lettercombinatie })
                    .HasName("IX_bag_data_huisnr_toevoegingen");

                entity.Property(e => e.Perceelid)
                    .HasColumnName("perceelid")
                    .ValueGeneratedNever();

                entity.Property(e => e.BagAdresseerbaarobjectid)
                    .HasColumnName("bag_adresseerbaarobjectid")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BagNummeraanduidingid)
                    .HasColumnName("bag_nummeraanduidingid")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Breedtegraad)
                    .HasColumnName("breedtegraad")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Cebucocode).HasColumnName("cebucocode");

                entity.Property(e => e.Gebruiksdoel)
                    .HasColumnName("gebruiksdoel")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gemeentecode).HasColumnName("gemeentecode");

                entity.Property(e => e.Gemeenteid).HasColumnName("gemeenteid");

                entity.Property(e => e.Gemeentenaam)
                    .HasColumnName("gemeentenaam")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.GemeentenaamNen)
                    .HasColumnName("gemeentenaam_nen")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.Huisnr).HasColumnName("huisnr");

                entity.Property(e => e.HuisnrBagLetter)
                    .IsRequired()
                    .HasColumnName("huisnr_bag_letter")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.HuisnrBagToevoeging)
                    .HasColumnName("huisnr_bag_toevoeging")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HuisnrTm).HasColumnName("huisnr_tm");

                entity.Property(e => e.HuisnrVan).HasColumnName("huisnr_van");

                entity.Property(e => e.Lengtegraad)
                    .HasColumnName("lengtegraad")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Lettercombinatie)
                    .HasColumnName("lettercombinatie")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Oppervlakte).HasColumnName("oppervlakte");

                entity.Property(e => e.Perceeltype)
                    .HasColumnName("perceeltype")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Plaatscode).HasColumnName("plaatscode");

                entity.Property(e => e.Plaatsid).HasColumnName("plaatsid");

                entity.Property(e => e.Plaatsnaam)
                    .IsRequired()
                    .HasColumnName("plaatsnaam")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.PlaatsnaamNen)
                    .IsRequired()
                    .HasColumnName("plaatsnaam_nen")
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.Provinciecode)
                    .IsRequired()
                    .HasColumnName("provinciecode")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Provincienaam)
                    .IsRequired()
                    .HasColumnName("provincienaam")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Rdx)
                    .HasColumnName("rdx")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Rdy)
                    .HasColumnName("rdy")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Reeksid).HasColumnName("reeksid");

                entity.Property(e => e.Reeksindicatie).HasColumnName("reeksindicatie");

                entity.Property(e => e.Straatid).HasColumnName("straatid");

                entity.Property(e => e.Straatnaam)
                    .HasColumnName("straatnaam")
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.StraatnaamNen)
                    .HasColumnName("straatnaam_nen")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Wijkcode).HasColumnName("wijkcode");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
