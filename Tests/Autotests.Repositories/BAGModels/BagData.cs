﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.BAGModels
{
    public partial class BagData
    {
        public int Perceelid { get; set; }
        public string Perceeltype { get; set; }
        public int? Huisnr { get; set; }
        public string HuisnrBagLetter { get; set; }
        public string HuisnrBagToevoeging { get; set; }
        public string BagNummeraanduidingid { get; set; }
        public string BagAdresseerbaarobjectid { get; set; }
        public decimal Breedtegraad { get; set; }
        public decimal Lengtegraad { get; set; }
        public decimal Rdx { get; set; }
        public decimal Rdy { get; set; }
        public int? Oppervlakte { get; set; }
        public string Gebruiksdoel { get; set; }
        public int? Reeksid { get; set; }
        public int? Wijkcode { get; set; }
        public string Lettercombinatie { get; set; }
        public int? HuisnrVan { get; set; }
        public int? HuisnrTm { get; set; }
        public int? Reeksindicatie { get; set; }
        public int? Straatid { get; set; }
        public string Straatnaam { get; set; }
        public string StraatnaamNen { get; set; }
        public int? Plaatsid { get; set; }
        public string Plaatsnaam { get; set; }
        public string PlaatsnaamNen { get; set; }
        public int? Plaatscode { get; set; }
        public int? Gemeenteid { get; set; }
        public string Gemeentenaam { get; set; }
        public string GemeentenaamNen { get; set; }
        public int? Gemeentecode { get; set; }
        public int? Cebucocode { get; set; }
        public string Provinciecode { get; set; }
        public string Provincienaam { get; set; }
    }
}
