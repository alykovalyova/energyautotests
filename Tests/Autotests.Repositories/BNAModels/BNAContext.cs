﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.BNAModels
{
    public partial class BNAContext : DbContext
    {
        private readonly string _connectionString;

        public BNAContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public BNAContext(DbContextOptions<BNAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BnaCampaign> BnaCampaign { get; set; }
        public virtual DbSet<BnaCampaignStatusHistory> BnaCampaignStatusHistory { get; set; }
        public virtual DbSet<DictionaryEventType> DictionaryEventType { get; set; }
        public virtual DbSet<DictionaryLabels> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryProcessingStatus> DictionaryProcessingStatus { get; set; }
        public virtual DbSet<DictionaryRelationCategory> DictionaryRelationCategory { get; set; }
        public virtual DbSet<PlannedEvent> PlannedEvent { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BnaCampaign>(entity =>
            {
                entity.ToTable("BnaCampaign", "dbo");

                entity.Property(e => e.CampaignName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.LastComment).HasMaxLength(255);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.BnaCampaign)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BnaCampaign_Dictionary_Labels");

                entity.HasOne(d => d.ProcessingStatus)
                    .WithMany(p => p.BnaCampaign)
                    .HasForeignKey(d => d.ProcessingStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BnaCampaign_Dictionary_CampaignStatus");

                entity.HasOne(d => d.RelationType)
                    .WithMany(p => p.BnaCampaign)
                    .HasForeignKey(d => d.RelationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BnaCampaign_Dictionary_RelationCategory");
            });

            modelBuilder.Entity<BnaCampaignStatusHistory>(entity =>
            {
                entity.ToTable("BnaCampaignStatusHistory", "dbo");

                entity.Property(e => e.Comment).HasMaxLength(255);

                entity.HasOne(d => d.BnaCampaign)
                    .WithMany(p => p.BnaCampaignStatusHistory)
                    .HasForeignKey(d => d.BnaCampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BnaCampaignStatusHistory_BnaCampaign");

                entity.HasOne(d => d.ProcessingStatus)
                    .WithMany(p => p.BnaCampaignStatusHistory)
                    .HasForeignKey(d => d.ProcessingStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BnaCampaignStatusHistory_Dictionary_ProcessingStatus");
            });

            modelBuilder.Entity<DictionaryEventType>(entity =>
            {
                entity.ToTable("Dictionary_EventType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryLabels>(entity =>
            {
                entity.ToTable("Dictionary_Labels", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryProcessingStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessingStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryRelationCategory>(entity =>
            {
                entity.ToTable("Dictionary_RelationCategory", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<PlannedEvent>(entity =>
            {
                entity.ToTable("PlannedEvent", "dbo");

                entity.Property(e => e.BnaId).HasMaxLength(50);

                entity.HasOne(d => d.BnaCampaign)
                    .WithMany(p => p.PlannedEvent)
                    .HasForeignKey(d => d.BnaCampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlannedEvent_BnaCampaign");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.PlannedEvent)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlannedEvent_Dictionary_EventType");
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.ToTable("Settings", "dbo");

                entity.Property(e => e.LastBiupdate).HasColumnName("LastBIUpdate");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
