﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.BNAModels
{
    public partial class BnaCampaign
    {
        public BnaCampaign()
        {
            BnaCampaignStatusHistory = new HashSet<BnaCampaignStatusHistory>();
            PlannedEvent = new HashSet<PlannedEvent>();
        }

        public int Id { get; set; }
        public int RenewableContractId { get; set; }
        public int EnerFreeContractId { get; set; }
        public string CampaignName { get; set; }
        public int ProcessingStatusId { get; set; }
        public int LabelCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int RenewalPageIdFromOnline { get; set; }
        public int RelationTypeId { get; set; }
        public double Probability { get; set; }
        public DateTime CreatedOn { get; set; }
        public int SalesChannelId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string LastComment { get; set; }

        public virtual DictionaryLabels LabelCodeNavigation { get; set; }
        public virtual DictionaryProcessingStatus ProcessingStatus { get; set; }
        public virtual DictionaryRelationCategory RelationType { get; set; }
        public virtual ICollection<BnaCampaignStatusHistory> BnaCampaignStatusHistory { get; set; }
        public virtual ICollection<PlannedEvent> PlannedEvent { get; set; }
    }
}
