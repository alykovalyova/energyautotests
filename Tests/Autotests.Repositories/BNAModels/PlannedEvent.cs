﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.BNAModels
{
    public partial class PlannedEvent
    {
        public int Id { get; set; }
        public DateTime? EventDateTime { get; set; }
        public int? EmailTemplateId { get; set; }
        public int BnaCampaignId { get; set; }
        public int EventTypeId { get; set; }
        public int PropositionBundleId { get; set; }
        public double Probability { get; set; }
        public string BnaId { get; set; }

        public virtual BnaCampaign BnaCampaign { get; set; }
        public virtual DictionaryEventType EventType { get; set; }
    }
}
