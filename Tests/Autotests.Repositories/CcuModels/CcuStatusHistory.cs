﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CcuModels
{
    public partial class CcuStatusHistory
    {
        public int Id { get; set; }
        public int CcuHistoryId { get; set; }
        public short ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Comment { get; set; }

        public virtual CcuHistory CcuHistory { get; set; }
        public virtual EnumProcessStatus ProcessStatus { get; set; }
    }
}
