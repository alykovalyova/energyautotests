﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CcuModels
{
    public partial class CommercialCharacteristicContext : DbContext
    {
        private readonly string _connectionString;

        public CommercialCharacteristicContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CommercialCharacteristicContext(DbContextOptions<CommercialCharacteristicContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CcuHistory> CcuHistory { get; set; }
        public virtual DbSet<CcuStatusHistory> CcuStatusHistory { get; set; }
        public virtual DbSet<EnumMessageSource> EnumMessageSource { get; set; }
        public virtual DbSet<EnumMutationReason> EnumMutationReason { get; set; }
        public virtual DbSet<EnumProcessStatus> EnumProcessStatus { get; set; }
        public virtual DbSet<MarketParty> MarketParty { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CcuHistory>(entity =>
            {
                entity.ToTable("CcuHistory", "dbo");

                entity.HasIndex(e => e.BalanceSupplierId);

                entity.HasIndex(e => e.BalanceSupplierResponsibleId);

                entity.HasIndex(e => e.DossierId)
                    .HasName("IX_DossierId");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId");

                entity.HasIndex(e => e.MessageSourceId);

                entity.HasIndex(e => e.MutationReasonId);

                entity.HasIndex(e => e.OldBalanceSupplierId);

                entity.HasIndex(e => e.StatusId)
                    .HasName("IX_StatusId");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.CcuHistoryBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.BalanceSupplierResponsible)
                    .WithMany(p => p.CcuHistoryBalanceSupplierResponsible)
                    .HasForeignKey(d => d.BalanceSupplierResponsibleId);

                entity.HasOne(d => d.MessageSource)
                    .WithMany(p => p.CcuHistory)
                    .HasForeignKey(d => d.MessageSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.CcuHistory)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.OldBalanceSupplier)
                    .WithMany(p => p.CcuHistoryOldBalanceSupplier)
                    .HasForeignKey(d => d.OldBalanceSupplierId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CcuHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CcuStatusHistory>(entity =>
            {
                entity.ToTable("CcuStatusHistory", "dbo");

                entity.HasIndex(e => e.CcuHistoryId)
                    .HasName("IX_CcuHistoryId");

                entity.HasIndex(e => e.ProcessStatusId);

                entity.HasOne(d => d.CcuHistory)
                    .WithMany(p => p.CcuStatusHistory)
                    .HasForeignKey(d => d.CcuHistoryId);

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.CcuStatusHistory)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EnumMessageSource>(entity =>
            {
                entity.ToTable("EnumMessageSource", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMutationReason>(entity =>
            {
                entity.ToTable("EnumMutationReason", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumProcessStatus>(entity =>
            {
                entity.ToTable("EnumProcessStatus", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(13);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
