﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CcuModels
{
    public partial class EnumMessageSource
    {
        public EnumMessageSource()
        {
            CcuHistory = new HashSet<CcuHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }

        public virtual ICollection<CcuHistory> CcuHistory { get; set; }
    }
}
