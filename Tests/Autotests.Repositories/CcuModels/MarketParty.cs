﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CcuModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            CcuHistoryBalanceSupplier = new HashSet<CcuHistory>();
            CcuHistoryBalanceSupplierResponsible = new HashSet<CcuHistory>();
            CcuHistoryOldBalanceSupplier = new HashSet<CcuHistory>();
        }

        public short Id { get; set; }
        public string Ean { get; set; }

        public virtual ICollection<CcuHistory> CcuHistoryBalanceSupplier { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryBalanceSupplierResponsible { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryOldBalanceSupplier { get; set; }
    }
}
