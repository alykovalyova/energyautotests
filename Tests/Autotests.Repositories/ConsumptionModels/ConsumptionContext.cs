﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class ConsumptionContext : DbContext
    {
        private readonly string _connectionString;

        public ConsumptionContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ConsumptionContext(DbContextOptions<ConsumptionContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryAditionalWindowStatus> DictionaryAditionalWindowStatus { get; set; }
        public virtual DbSet<DictionaryBusinessEventType> DictionaryBusinessEventType { get; set; }
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevel { get; set; }
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCode { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCode { get; set; }
        public virtual DbSet<DictionaryInitiatedParty> DictionaryInitiatedParty { get; set; }
        public virtual DbSet<DictionaryMarketEvent> DictionaryMarketEvent { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCode { get; set; }
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatus { get; set; }
        public virtual DbSet<DictionaryReadingSource> DictionaryReadingSource { get; set; }
        public virtual DbSet<DictionaryStatusReason> DictionaryStatusReason { get; set; }
        public virtual DbSet<DictionaryValidationStatus> DictionaryValidationStatus { get; set; }
        public virtual DbSet<EdsnFraction> EdsnFraction { get; set; }
        public virtual DbSet<EdsnRejection> EdsnRejection { get; set; }
        public virtual DbSet<MeterReading> MeterReading { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<RegisterReading> RegisterReading { get; set; }
        public virtual DbSet<RegisterVolume> RegisterVolume { get; set; }
        public virtual DbSet<StatusHistory> StatusHistory { get; set; }
        public virtual DbSet<TransitionEventAction> TransitionEventAction { get; set; }
        public virtual DbSet<TransitionInitiatedPartyInStatusCondition> TransitionInitiatedPartyInStatusCondition { get; set; }
        public virtual DbSet<TransitionMarketEventInStatusCondition> TransitionMarketEventInStatusCondition { get; set; }
        public virtual DbSet<TransitionReadingSourceInStatusCondition> TransitionReadingSourceInStatusCondition { get; set; }
        public virtual DbSet<TransitionStatusCondition> TransitionStatusCondition { get; set; }
        public virtual DbSet<TransitionStatusTransition> TransitionStatusTransition { get; set; }
        public virtual DbSet<ValidationResultLog> ValidationResultLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryAditionalWindowStatus>(entity =>
            {
                entity.ToTable("Dictionary_AditionalWindowStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryBusinessEventType>(entity =>
            {
                entity.ToTable("Dictionary_BusinessEventType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryInitiatedParty>(entity =>
            {
                entity.ToTable("Dictionary_InitiatedParty", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMarketEvent>(entity =>
            {
                entity.ToTable("Dictionary_MarketEvent", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryReadingSource>(entity =>
            {
                entity.ToTable("Dictionary_ReadingSource", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStatusReason>(entity =>
            {
                entity.ToTable("Dictionary_StatusReason", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryValidationStatus>(entity =>
            {
                entity.ToTable("Dictionary_ValidationStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EdsnFraction>(entity =>
            {
                entity.ToTable("EdsnFraction", "dbo");

                entity.HasIndex(e => new { e.FractionValue, e.ProfileCodeId, e.FractionDate })
                    .HasName("IX_ProfileCode_FractionDate_incl_FractionValue");

                entity.Property(e => e.FractionDate).HasColumnType("datetime");

                entity.Property(e => e.FractionValue).HasColumnType("decimal(18, 9)");

                entity.HasOne(d => d.ProfileCode)
                    .WithMany(p => p.EdsnFraction)
                    .HasForeignKey(d => d.ProfileCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EdsnFraction_dbo.Dictionary_EnergyUsageProfileCode_ProfileCodeId");
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.ToTable("EdsnRejection", "dbo");

                entity.HasIndex(e => e.StatusHistoryId)
                    .HasName("IX_StatusHistoryId");

                entity.Property(e => e.Code).HasMaxLength(10);

                entity.HasOne(d => d.RejectionLevel)
                    .WithMany(p => p.EdsnRejection)
                    .HasForeignKey(d => d.RejectionLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EdsnRejection_dbo.Dictionary_EdsnRejectionLevel_RejectionLevelId");

                entity.HasOne(d => d.StatusHistory)
                    .WithMany(p => p.EdsnRejection)
                    .HasForeignKey(d => d.StatusHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EdsnRejection_dbo.StatusHistory_StatusHistoryId");
            });

            modelBuilder.Entity<MeterReading>(entity =>
            {
                entity.ToTable("MeterReading", "dbo");

                entity.HasIndex(e => new { e.EanId, e.EdsnMeterId })
                    .HasName("IX_Ean_MeterId");

                entity.HasIndex(e => new { e.MarketEventDate, e.EanId });

                entity.HasIndex(e => new { e.MarketEventId, e.ProcessId, e.StatusId })
                    .HasName("IX_ProcessId");

                entity.HasIndex(e => new { e.StatusId, e.MarketEventId, e.MarketEventDate })
                    .HasName("IX_StatusCount");

                entity.HasIndex(e => new { e.StatusId, e.MarketEventId, e.ModifiedOn })
                    .HasName("IX_StatusCountByModifiedOn");

                entity.HasIndex(e => new { e.StatusId, e.ReadingSourceId, e.MarketEventDate })
                    .HasName("IX_Search_By_Criteria");

                entity.Property(e => e.ClientReadingDate).HasColumnType("date");

                entity.Property(e => e.Consumer).HasMaxLength(13);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.EdsnMeterId).HasMaxLength(50);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.Initiator).HasMaxLength(13);

                entity.Property(e => e.MarketEventDate).HasColumnType("date");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.EnergyProductType)
                    .WithMany(p => p.MeterReading)
                    .HasForeignKey(d => d.EnergyProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_EnergyProductTypeCode_EnergyProductTypeId");

                entity.HasOne(d => d.InitiatedParty)
                    .WithMany(p => p.MeterReading)
                    .HasForeignKey(d => d.InitiatedPartyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_InitiatedParty_InitiatedPartyId");

                entity.HasOne(d => d.MarketEvent)
                    .WithMany(p => p.MeterReading)
                    .HasForeignKey(d => d.MarketEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_MarketEvent_MarketEventId");

                entity.HasOne(d => d.ReadingSource)
                    .WithMany(p => p.MeterReading)
                    .HasForeignKey(d => d.ReadingSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_ReadingSource_ReadingSourceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MeterReading)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeterReading_dbo.Dictionary_ProcessStatus_StatusId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<RegisterReading>(entity =>
            {
                entity.ToTable("RegisterReading", "dbo");

                entity.HasIndex(e => new { e.MeterReadingId, e.MeteringDirectionId, e.TariffTypeId })
                    .HasName("IX_TariffType")
                    .IsUnique();

                entity.Property(e => e.RegisterNo)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.MeasureUnit)
                    .WithMany(p => p.RegisterReading)
                    .HasForeignKey(d => d.MeasureUnitId)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.Dictionary_MeasureUnitCode_MeasureUnitId");

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.RegisterReading)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.MeterReading_MeterReadingId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.RegisterReading)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.Dictionary_EnergyFlowDirectionCode_MeteringDirectionId");

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.RegisterReading)
                    .HasForeignKey(d => d.TariffTypeId)
                    .HasConstraintName("FK_dbo.RegisterReading_dbo.Dictionary_EnergyTariffTypeCode_TariffTypeId");
            });

            modelBuilder.Entity<RegisterVolume>(entity =>
            {
                entity.ToTable("RegisterVolume", "dbo");

                entity.HasIndex(e => e.BeginReadingId)
                    .HasName("IX_BeginReadingId");

                entity.HasIndex(e => e.EndReadingId)
                    .HasName("IX_EndReadingId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.BeginReading)
                    .WithMany(p => p.RegisterVolumeBeginReading)
                    .HasForeignKey(d => d.BeginReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RegisterVolume_dbo.RegisterReading_BeginReadingId");

                entity.HasOne(d => d.EndReading)
                    .WithMany(p => p.RegisterVolumeEndReading)
                    .HasForeignKey(d => d.EndReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RegisterVolume_dbo.RegisterReading_EndReadingId");
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory", "dbo");

                entity.HasIndex(e => new { e.MeterReadingId, e.CreatedOn })
                    .HasName("IX_MeterReadingIdCreatedOn");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.XmlHeaderCreationTs)
                    .HasColumnName("XmlHeaderCreationTS")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.MeterReading_MeterReadingId");

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.Dictionary_ProcessStatus_ProcessStatusId");

                entity.HasOne(d => d.Reason)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.ReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.Dictionary_StatusReason_ReasonId");

                entity.HasOne(d => d.StatusTransition)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.StatusTransitionId)
                    .HasConstraintName("FK_dbo.StatusHistory_dbo.Transition_StatusTransition_StatusTransitionId");
            });

            modelBuilder.Entity<TransitionEventAction>(entity =>
            {
                entity.ToTable("Transition_EventAction", "dbo");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.TransitionEventAction)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_EventAction_dbo.Dictionary_BusinessEventType_EventTypeId");
            });

            modelBuilder.Entity<TransitionInitiatedPartyInStatusCondition>(entity =>
            {
                entity.ToTable("Transition_InitiatedPartyInStatusCondition", "dbo");

                entity.HasIndex(e => e.ConditionId)
                    .HasName("IX_ConditionId");

                entity.HasOne(d => d.Condition)
                    .WithMany(p => p.TransitionInitiatedPartyInStatusCondition)
                    .HasForeignKey(d => d.ConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_InitiatedPartyInStatusCondition_dbo.Transition_StatusCondition_ConditionId");

                entity.HasOne(d => d.InitiatedParty)
                    .WithMany(p => p.TransitionInitiatedPartyInStatusCondition)
                    .HasForeignKey(d => d.InitiatedPartyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_InitiatedPartyInStatusCondition_dbo.Dictionary_InitiatedParty_InitiatedPartyId");
            });

            modelBuilder.Entity<TransitionMarketEventInStatusCondition>(entity =>
            {
                entity.ToTable("Transition_MarketEventInStatusCondition", "dbo");

                entity.HasIndex(e => e.ConditionId)
                    .HasName("IX_ConditionId");

                entity.HasOne(d => d.Condition)
                    .WithMany(p => p.TransitionMarketEventInStatusCondition)
                    .HasForeignKey(d => d.ConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_MarketEventInStatusCondition_dbo.Transition_StatusCondition_ConditionId");

                entity.HasOne(d => d.MarketEvent)
                    .WithMany(p => p.TransitionMarketEventInStatusCondition)
                    .HasForeignKey(d => d.MarketEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_MarketEventInStatusCondition_dbo.Dictionary_MarketEvent_MarketEventId");
            });

            modelBuilder.Entity<TransitionReadingSourceInStatusCondition>(entity =>
            {
                entity.ToTable("Transition_ReadingSourceInStatusCondition", "dbo");

                entity.HasIndex(e => e.ConditionId)
                    .HasName("IX_ConditionId");

                entity.HasOne(d => d.Condition)
                    .WithMany(p => p.TransitionReadingSourceInStatusCondition)
                    .HasForeignKey(d => d.ConditionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_ReadingSourceInStatusCondition_dbo.Transition_StatusCondition_ConditionId");

                entity.HasOne(d => d.ReadingSource)
                    .WithMany(p => p.TransitionReadingSourceInStatusCondition)
                    .HasForeignKey(d => d.ReadingSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_ReadingSourceInStatusCondition_dbo.Dictionary_ReadingSource_ReadingSourceId");
            });

            modelBuilder.Entity<TransitionStatusCondition>(entity =>
            {
                entity.ToTable("Transition_StatusCondition", "dbo");

                entity.HasIndex(e => e.TransitionId)
                    .HasName("IX_TransitionId");

                entity.HasOne(d => d.DisputeWindowStatus)
                    .WithMany(p => p.TransitionStatusConditionDisputeWindowStatus)
                    .HasForeignKey(d => d.DisputeWindowStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_AditionalWindowStatus_DisputeWindowStatusId");

                entity.HasOne(d => d.ExchangeWindowStatus)
                    .WithMany(p => p.TransitionStatusConditionExchangeWindowStatus)
                    .HasForeignKey(d => d.ExchangeWindowStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_AditionalWindowStatus_ExchangeWindowStatusId");

                entity.HasOne(d => d.SettlementWindowStatus)
                    .WithMany(p => p.TransitionStatusConditionSettlementWindowStatus)
                    .HasForeignKey(d => d.SettlementWindowStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_AditionalWindowStatus_SettlementWindowStatusId");

                entity.HasOne(d => d.Transition)
                    .WithMany(p => p.TransitionStatusCondition)
                    .HasForeignKey(d => d.TransitionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Transition_StatusTransition_TransitionId");

                entity.HasOne(d => d.ValidationStatus)
                    .WithMany(p => p.TransitionStatusCondition)
                    .HasForeignKey(d => d.ValidationStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusCondition_dbo.Dictionary_ValidationStatus_ValidationStatusId");
            });

            modelBuilder.Entity<TransitionStatusTransition>(entity =>
            {
                entity.ToTable("Transition_StatusTransition", "dbo");

                entity.HasIndex(e => e.ActionId)
                    .HasName("IX_ActionId");

                entity.HasIndex(e => e.ParentId)
                    .HasName("IX_ParentId");

                entity.Property(e => e.IsEnabled)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Action)
                    .WithMany(p => p.TransitionStatusTransition)
                    .HasForeignKey(d => d.ActionId)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Transition_EventAction_ActionId");

                entity.HasOne(d => d.CurrentReadingStatus)
                    .WithMany(p => p.TransitionStatusTransitionCurrentReadingStatus)
                    .HasForeignKey(d => d.CurrentReadingStatusId)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Dictionary_ProcessStatus_CurrentReadingStatusId");

                entity.HasOne(d => d.NewReadingStatus)
                    .WithMany(p => p.TransitionStatusTransitionNewReadingStatus)
                    .HasForeignKey(d => d.NewReadingStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Dictionary_ProcessStatus_NewReadingStatusId");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_dbo.Transition_StatusTransition_dbo.Transition_StatusTransition_ParentId");
            });

            modelBuilder.Entity<ValidationResultLog>(entity =>
            {
                entity.ToTable("ValidationResultLog", "dbo");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.EventDate).HasColumnType("date");

                entity.Property(e => e.RegisterNo)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
