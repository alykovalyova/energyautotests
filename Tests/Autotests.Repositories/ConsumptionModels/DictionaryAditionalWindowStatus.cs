﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class DictionaryAditionalWindowStatus
    {
        public DictionaryAditionalWindowStatus()
        {
            TransitionStatusConditionDisputeWindowStatus = new HashSet<TransitionStatusCondition>();
            TransitionStatusConditionExchangeWindowStatus = new HashSet<TransitionStatusCondition>();
            TransitionStatusConditionSettlementWindowStatus = new HashSet<TransitionStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditionDisputeWindowStatus { get; set; }
        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditionExchangeWindowStatus { get; set; }
        public virtual ICollection<TransitionStatusCondition> TransitionStatusConditionSettlementWindowStatus { get; set; }
    }
}
