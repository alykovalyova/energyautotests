﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class DictionaryInitiatedParty
    {
        public DictionaryInitiatedParty()
        {
            MeterReading = new HashSet<MeterReading>();
            TransitionInitiatedPartyInStatusCondition = new HashSet<TransitionInitiatedPartyInStatusCondition>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MeterReading> MeterReading { get; set; }
        public virtual ICollection<TransitionInitiatedPartyInStatusCondition> TransitionInitiatedPartyInStatusCondition { get; set; }
    }
}
