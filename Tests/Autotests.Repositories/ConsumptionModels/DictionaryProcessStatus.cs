﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class DictionaryProcessStatus
    {
        public DictionaryProcessStatus()
        {
            MeterReading = new HashSet<MeterReading>();
            StatusHistory = new HashSet<StatusHistory>();
            TransitionStatusTransitionCurrentReadingStatus = new HashSet<TransitionStatusTransition>();
            TransitionStatusTransitionNewReadingStatus = new HashSet<TransitionStatusTransition>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MeterReading> MeterReading { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
        public virtual ICollection<TransitionStatusTransition> TransitionStatusTransitionCurrentReadingStatus { get; set; }
        public virtual ICollection<TransitionStatusTransition> TransitionStatusTransitionNewReadingStatus { get; set; }
    }
}
