﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class EdsnFraction
    {
        public int Id { get; set; }
        public DateTime FractionDate { get; set; }
        public decimal FractionValue { get; set; }
        public int ProfileCodeId { get; set; }

        public virtual DictionaryEnergyUsageProfileCode ProfileCode { get; set; }
    }
}
