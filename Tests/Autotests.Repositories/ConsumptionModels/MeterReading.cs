﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class MeterReading
    {
        public MeterReading()
        {
            RegisterReading = new HashSet<RegisterReading>();
            StatusHistory = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public string EdsnMeterId { get; set; }
        public string ExternalReference { get; set; }
        public int EnergyProductTypeId { get; set; }
        public int NrOfRegisters { get; set; }
        public DateTime MarketEventDate { get; set; }
        public DateTime? ClientReadingDate { get; set; }
        public int ReadingSourceId { get; set; }
        public int StatusId { get; set; }
        public string Consumer { get; set; }
        public string Initiator { get; set; }
        public string DossierId { get; set; }
        public int MarketEventId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? ProcessId { get; set; }
        public int InitiatedPartyId { get; set; }

        public virtual DictionaryEnergyProductTypeCode EnergyProductType { get; set; }
        public virtual DictionaryInitiatedParty InitiatedParty { get; set; }
        public virtual DictionaryMarketEvent MarketEvent { get; set; }
        public virtual DictionaryReadingSource ReadingSource { get; set; }
        public virtual DictionaryProcessStatus Status { get; set; }
        public virtual ICollection<RegisterReading> RegisterReading { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
    }
}
