﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class RegisterReading
    {
        public RegisterReading()
        {
            RegisterVolumeBeginReading = new HashSet<RegisterVolume>();
            RegisterVolumeEndReading = new HashSet<RegisterVolume>();
        }

        public int Id { get; set; }
        public int MeterReadingId { get; set; }
        public int? MeasureUnitId { get; set; }
        public int? MeteringDirectionId { get; set; }
        public int NrOfDigits { get; set; }
        public int? TariffTypeId { get; set; }
        public int Value { get; set; }
        public string RegisterNo { get; set; }

        public virtual DictionaryMeasureUnitCode MeasureUnit { get; set; }
        public virtual MeterReading MeterReading { get; set; }
        public virtual DictionaryEnergyFlowDirectionCode MeteringDirection { get; set; }
        public virtual DictionaryEnergyTariffTypeCode TariffType { get; set; }
        public virtual ICollection<RegisterVolume> RegisterVolumeBeginReading { get; set; }
        public virtual ICollection<RegisterVolume> RegisterVolumeEndReading { get; set; }
    }
}
