﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class RegisterVolume
    {
        public int Id { get; set; }
        public int? CalorificCorrectedVolume { get; set; }
        public int Volume { get; set; }
        public int BeginReadingId { get; set; }
        public int EndReadingId { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual RegisterReading BeginReading { get; set; }
        public virtual RegisterReading EndReading { get; set; }
    }
}
