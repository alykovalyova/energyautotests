﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class StatusHistory
    {
        public StatusHistory()
        {
            EdsnRejection = new HashSet<EdsnRejection>();
        }

        public int Id { get; set; }
        public int MeterReadingId { get; set; }
        public Guid? XmlHeaderMessageId { get; set; }
        public DateTime? XmlHeaderCreationTs { get; set; }
        public int ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? StatusTransitionId { get; set; }
        public int ReasonId { get; set; }
        public string Comment { get; set; }
        public string User { get; set; }

        public virtual MeterReading MeterReading { get; set; }
        public virtual DictionaryProcessStatus ProcessStatus { get; set; }
        public virtual DictionaryStatusReason Reason { get; set; }
        public virtual TransitionStatusTransition StatusTransition { get; set; }
        public virtual ICollection<EdsnRejection> EdsnRejection { get; set; }
    }
}
