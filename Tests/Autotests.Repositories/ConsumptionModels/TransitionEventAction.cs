﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class TransitionEventAction
    {
        public TransitionEventAction()
        {
            TransitionStatusTransition = new HashSet<TransitionStatusTransition>();
        }

        public int Id { get; set; }
        public int EventTypeId { get; set; }

        public virtual DictionaryBusinessEventType EventType { get; set; }
        public virtual ICollection<TransitionStatusTransition> TransitionStatusTransition { get; set; }
    }
}
