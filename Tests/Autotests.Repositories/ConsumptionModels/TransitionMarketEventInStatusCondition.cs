﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class TransitionMarketEventInStatusCondition
    {
        public int Id { get; set; }
        public int ConditionId { get; set; }
        public int MarketEventId { get; set; }

        public virtual TransitionStatusCondition Condition { get; set; }
        public virtual DictionaryMarketEvent MarketEvent { get; set; }
    }
}
