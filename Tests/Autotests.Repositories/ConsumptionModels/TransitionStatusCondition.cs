﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class TransitionStatusCondition
    {
        public TransitionStatusCondition()
        {
            TransitionInitiatedPartyInStatusCondition = new HashSet<TransitionInitiatedPartyInStatusCondition>();
            TransitionMarketEventInStatusCondition = new HashSet<TransitionMarketEventInStatusCondition>();
            TransitionReadingSourceInStatusCondition = new HashSet<TransitionReadingSourceInStatusCondition>();
        }

        public int Id { get; set; }
        public int TransitionId { get; set; }
        public int? ValidationStatusId { get; set; }
        public int? ExchangeWindowStatusId { get; set; }
        public int? DisputeWindowStatusId { get; set; }
        public int? SettlementWindowStatusId { get; set; }

        public virtual DictionaryAditionalWindowStatus DisputeWindowStatus { get; set; }
        public virtual DictionaryAditionalWindowStatus ExchangeWindowStatus { get; set; }
        public virtual DictionaryAditionalWindowStatus SettlementWindowStatus { get; set; }
        public virtual TransitionStatusTransition Transition { get; set; }
        public virtual DictionaryValidationStatus ValidationStatus { get; set; }
        public virtual ICollection<TransitionInitiatedPartyInStatusCondition> TransitionInitiatedPartyInStatusCondition { get; set; }
        public virtual ICollection<TransitionMarketEventInStatusCondition> TransitionMarketEventInStatusCondition { get; set; }
        public virtual ICollection<TransitionReadingSourceInStatusCondition> TransitionReadingSourceInStatusCondition { get; set; }
    }
}
