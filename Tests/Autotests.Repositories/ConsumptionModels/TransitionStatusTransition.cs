﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class TransitionStatusTransition
    {
        public TransitionStatusTransition()
        {
            InverseParent = new HashSet<TransitionStatusTransition>();
            StatusHistory = new HashSet<StatusHistory>();
            TransitionStatusCondition = new HashSet<TransitionStatusCondition>();
        }

        public int Id { get; set; }
        public int? ActionId { get; set; }
        public int? ParentId { get; set; }
        public int? CurrentReadingStatusId { get; set; }
        public int NewReadingStatusId { get; set; }
        public bool? IsEnabled { get; set; }

        public virtual TransitionEventAction Action { get; set; }
        public virtual DictionaryProcessStatus CurrentReadingStatus { get; set; }
        public virtual DictionaryProcessStatus NewReadingStatus { get; set; }
        public virtual TransitionStatusTransition Parent { get; set; }
        public virtual ICollection<TransitionStatusTransition> InverseParent { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
        public virtual ICollection<TransitionStatusCondition> TransitionStatusCondition { get; set; }
    }
}
