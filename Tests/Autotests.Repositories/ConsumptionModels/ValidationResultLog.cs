﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ConsumptionModels
{
    public partial class ValidationResultLog
    {
        public int Id { get; set; }
        public string Ean { get; set; }
        public DateTime EventDate { get; set; }
        public int Event { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public int ReadingValue { get; set; }
        public int? MeasureUnitId { get; set; }
        public int? MeterReadingDirectionId { get; set; }
        public int? TariffTypeId { get; set; }
        public string RegisterNo { get; set; }
        public int ValidationResult { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
