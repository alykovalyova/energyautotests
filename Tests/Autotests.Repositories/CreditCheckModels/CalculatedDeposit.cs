﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class CalculatedDeposit
    {
        public int Id { get; set; }
        public int DepositRequestId { get; set; }
        public decimal? DepositAmount { get; set; }
        public string Description { get; set; }
        public bool IsCreditWorthy { get; set; }
        public int BasedOnCreditAgencyResponseId { get; set; }

        public virtual CreditAgencyResponse BasedOnCreditAgencyResponse { get; set; }
        public virtual DepositRequest DepositRequest { get; set; }
    }
}
