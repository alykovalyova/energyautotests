﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class CreditAgencyResponse
    {
        public CreditAgencyResponse()
        {
            CalculatedDeposit = new HashSet<CalculatedDeposit>();
        }

        public int Id { get; set; }
        public int DepositRequestId { get; set; }
        public decimal? Score { get; set; }
        public string Description { get; set; }
        public string RawFinancialResponse { get; set; }
        public bool HasSupportedScore { get; set; }
        public string RawCompanyResponse { get; set; }
        public string RawLegalResponse { get; set; }
        public int? CreditAdvice { get; set; }
        public decimal? PdPercentage { get; set; }
        public decimal? PaymentRating { get; set; }
        public string SectorOfIndustry { get; set; }
        public int ScoreBasedOnId { get; set; }
        public int CreditCheckAgencyId { get; set; }
        public int? BestMatchRiskCustomerId { get; set; }

        public virtual RiskCustomer BestMatchRiskCustomer { get; set; }
        public virtual CreditCheckAgency CreditCheckAgency { get; set; }
        public virtual DepositRequest DepositRequest { get; set; }
        public virtual ScoreBasedOn ScoreBasedOn { get; set; }
        public virtual ICollection<CalculatedDeposit> CalculatedDeposit { get; set; }
    }
}
