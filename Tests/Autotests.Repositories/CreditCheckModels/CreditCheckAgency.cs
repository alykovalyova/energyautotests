﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class CreditCheckAgency
    {
        public CreditCheckAgency()
        {
            CreditAgencyResponse = new HashSet<CreditAgencyResponse>();
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public virtual ICollection<CreditAgencyResponse> CreditAgencyResponse { get; set; }
    }
}
