﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class CreditCheckContext : DbContext
    {
        private readonly string _connectionString;

        public CreditCheckContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CreditCheckContext(DbContextOptions<CreditCheckContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CalculatedDeposit> CalculatedDeposit { get; set; }
        public virtual DbSet<CheckKind> CheckKind { get; set; }
        public virtual DbSet<CreditAgencyResponse> CreditAgencyResponse { get; set; }
        public virtual DbSet<CreditCheckAgency> CreditCheckAgency { get; set; }
        public virtual DbSet<DepositRequest> DepositRequest { get; set; }
        public virtual DbSet<DepositRequestEan> DepositRequestEan { get; set; }
        public virtual DbSet<DepositRules> DepositRules { get; set; }
        public virtual DbSet<RiskCustomer> RiskCustomer { get; set; }
        public virtual DbSet<RiskCustomerConnection> RiskCustomerConnection { get; set; }
        public virtual DbSet<RiskCustomerReason> RiskCustomerReason { get; set; }
        public virtual DbSet<RiskReason> RiskReason { get; set; }
        public virtual DbSet<SalesChannelScore> SalesChannelScore { get; set; }
        public virtual DbSet<ScoreBasedOn> ScoreBasedOn { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CalculatedDeposit>(entity =>
            {
                entity.ToTable("CalculatedDeposit", "dbo");

                entity.HasIndex(e => e.DepositRequestId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DepositAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DepositRequestId).HasColumnName("DepositRequestID");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.HasOne(d => d.BasedOnCreditAgencyResponse)
                    .WithMany(p => p.CalculatedDeposit)
                    .HasForeignKey(d => d.BasedOnCreditAgencyResponseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Calculate__Based__75A278F5");

                entity.HasOne(d => d.DepositRequest)
                    .WithMany(p => p.CalculatedDeposit)
                    .HasForeignKey(d => d.DepositRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CalculatedDeposit_DepositRequest");
            });

            modelBuilder.Entity<CheckKind>(entity =>
            {
                entity.ToTable("CheckKind", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CreditAgencyResponse>(entity =>
            {
                entity.ToTable("CreditAgencyResponse", "dbo");

                entity.HasIndex(e => e.CreditCheckAgencyId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DepositRequestId).HasColumnName("DepositRequestID");

                entity.Property(e => e.Description).HasMaxLength(999);

                entity.Property(e => e.PaymentRating).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PdPercentage).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Score).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SectorOfIndustry).HasMaxLength(64);

                entity.HasOne(d => d.BestMatchRiskCustomer)
                    .WithMany(p => p.CreditAgencyResponse)
                    .HasForeignKey(d => d.BestMatchRiskCustomerId)
                    .HasConstraintName("FK_CreditAgencyResponse_RiskCustomer");

                entity.HasOne(d => d.CreditCheckAgency)
                    .WithMany(p => p.CreditAgencyResponse)
                    .HasForeignKey(d => d.CreditCheckAgencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CreditAge__Credi__74AE54BC");

                entity.HasOne(d => d.DepositRequest)
                    .WithMany(p => p.CreditAgencyResponse)
                    .HasForeignKey(d => d.DepositRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CreditAgencyResponse_PrivateDepositRequest");

                entity.HasOne(d => d.ScoreBasedOn)
                    .WithMany(p => p.CreditAgencyResponse)
                    .HasForeignKey(d => d.ScoreBasedOnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CreditAge__Score__73BA3083");
            });

            modelBuilder.Entity<CreditCheckAgency>(entity =>
            {
                entity.ToTable("CreditCheckAgency", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DepositRequest>(entity =>
            {
                entity.ToTable("DepositRequest", "dbo");

                entity.HasIndex(e => e.ContractId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChamberOfCommerceNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DepositRulesId).HasColumnName("DepositRulesID");

                entity.Property(e => e.HouseNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.HouseNumberSuffix).HasMaxLength(50);

                entity.Property(e => e.Initials)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PostalCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.RelationId).HasColumnName("RelationID");

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.SurnamePrefix).HasMaxLength(50);

                entity.HasOne(d => d.CheckKind)
                    .WithMany(p => p.DepositRequest)
                    .HasForeignKey(d => d.CheckKindId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DepositRequest_CheckKind");

                entity.HasOne(d => d.DepositRules)
                    .WithMany(p => p.DepositRequest)
                    .HasForeignKey(d => d.DepositRulesId)
                    .HasConstraintName("FK_DepositRequest_DepositRules");
            });

            modelBuilder.Entity<DepositRequestEan>(entity =>
            {
                entity.ToTable("DepositRequestEAN", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DepositRequestId).HasColumnName("DepositRequestID");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasColumnName("EAN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DepositRequest)
                    .WithMany(p => p.DepositRequestEan)
                    .HasForeignKey(d => d.DepositRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DepositRequestEAN_DepositRequest");
            });

            modelBuilder.Entity<DepositRules>(entity =>
            {
                entity.ToTable("DepositRules", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreateOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.KeyValuePairs).IsRequired();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RiskCustomer>(entity =>
            {
                entity.ToTable("RiskCustomer", "dbo");

                entity.HasIndex(e => e.T3riskRelationId);

                entity.HasIndex(e => new { e.CreationDateTime, e.DeletionReason, e.DeletionDateTime, e.RelationId, e.IbanHash, e.EmailAddressHash, e.Surname, e.DateOfBirth, e.ChamberOfCommerceNumber, e.PrimaryPhoneNumberHash, e.AlternativePhoneNumberHash })
                    .HasName("IX_RiskCustomer_AllColumnsForSearch");

                entity.Property(e => e.AlternativePhoneNumberHash)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ChamberOfCommerceNumber)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreationDateTime).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DeletionDateTime).HasColumnType("datetime");

                entity.Property(e => e.DeletionReason).HasMaxLength(50);

                entity.Property(e => e.EmailAddressHash)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.IbanHash)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PrimaryPhoneNumberHash)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Surname).HasMaxLength(50);

                entity.Property(e => e.T3riskRelationId).HasColumnName("T3RiskRelationId");
            });

            modelBuilder.Entity<RiskCustomerConnection>(entity =>
            {
                entity.ToTable("RiskCustomerConnection", "dbo");

                entity.HasIndex(e => e.RiskCustomerId);

                entity.HasIndex(e => new { e.RiskCustomerId, e.DeletionDateTime, e.Ean })
                    .HasName("IX_RiskCustomerConnection_DeletionDateTime_Ean_include_RiskCustomerId");

                entity.Property(e => e.CreationDateTime).HasColumnType("datetime");

                entity.Property(e => e.DeletionDateTime).HasColumnType("datetime");

                entity.Property(e => e.DeletionReason).HasMaxLength(50);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.RiskCustomer)
                    .WithMany(p => p.RiskCustomerConnection)
                    .HasForeignKey(d => d.RiskCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RiskCustomerConnection_RiskCustomer");
            });

            modelBuilder.Entity<RiskCustomerReason>(entity =>
            {
                entity.ToTable("RiskCustomerReason", "dbo");

                entity.HasIndex(e => e.RiskCustomerId);

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.CreationDateTime).HasColumnType("datetime");

                entity.Property(e => e.DeletionDateTime).HasColumnType("datetime");

                entity.Property(e => e.DeletionReason).HasMaxLength(50);

                entity.HasOne(d => d.RiskCustomer)
                    .WithMany(p => p.RiskCustomerReason)
                    .HasForeignKey(d => d.RiskCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RiskCustomerReason_RiskCustomer");

                entity.HasOne(d => d.RiskReason)
                    .WithMany(p => p.RiskCustomerReason)
                    .HasForeignKey(d => d.RiskReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RiskCustomerReason_RiskReason");
            });

            modelBuilder.Entity<RiskReason>(entity =>
            {
                entity.ToTable("RiskReason", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SalesChannelScore>(entity =>
            {
                entity.HasKey(e => e.SalesChannelId);

                entity.ToTable("SalesChannelScore", "dbo");

                entity.Property(e => e.SalesChannelId).ValueGeneratedNever();
            });

            modelBuilder.Entity<ScoreBasedOn>(entity =>
            {
                entity.ToTable("ScoreBasedOn", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
