﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class DepositRequest
    {
        public DepositRequest()
        {
            CalculatedDeposit = new HashSet<CalculatedDeposit>();
            CreditAgencyResponse = new HashSet<CreditAgencyResponse>();
            DepositRequestEan = new HashSet<DepositRequestEan>();
        }

        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int? RelationId { get; set; }
        public int? ContractId { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string PostalCode { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberSuffix { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int? SalesChannelId { get; set; }
        public int CheckKindId { get; set; }
        public string ChamberOfCommerceNumber { get; set; }
        public int? DepositRulesId { get; set; }
        public string SurnamePrefix { get; set; }

        public virtual CheckKind CheckKind { get; set; }
        public virtual DepositRules DepositRules { get; set; }
        public virtual ICollection<CalculatedDeposit> CalculatedDeposit { get; set; }
        public virtual ICollection<CreditAgencyResponse> CreditAgencyResponse { get; set; }
        public virtual ICollection<DepositRequestEan> DepositRequestEan { get; set; }
    }
}
