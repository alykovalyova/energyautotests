﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class DepositRequestEan
    {
        public int Id { get; set; }
        public int DepositRequestId { get; set; }
        public string Ean { get; set; }
        public int ProductType { get; set; }

        public virtual DepositRequest DepositRequest { get; set; }
    }
}
