﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class DepositRules
    {
        public DepositRules()
        {
            DepositRequest = new HashSet<DepositRequest>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateOn { get; set; }
        public string KeyValuePairs { get; set; }

        public virtual ICollection<DepositRequest> DepositRequest { get; set; }
    }
}
