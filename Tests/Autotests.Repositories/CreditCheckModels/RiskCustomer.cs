﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class RiskCustomer
    {
        public RiskCustomer()
        {
            CreditAgencyResponse = new HashSet<CreditAgencyResponse>();
            RiskCustomerConnection = new HashSet<RiskCustomerConnection>();
            RiskCustomerReason = new HashSet<RiskCustomerReason>();
        }

        public int Id { get; set; }
        public int? RelationId { get; set; }
        public string Surname { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public string DeletionReason { get; set; }
        public string ChamberOfCommerceNumber { get; set; }
        public long? T3riskRelationId { get; set; }
        public string EmailAddressHash { get; set; }
        public string IbanHash { get; set; }
        public string PrimaryPhoneNumberHash { get; set; }
        public string AlternativePhoneNumberHash { get; set; }

        public virtual ICollection<CreditAgencyResponse> CreditAgencyResponse { get; set; }
        public virtual ICollection<RiskCustomerConnection> RiskCustomerConnection { get; set; }
        public virtual ICollection<RiskCustomerReason> RiskCustomerReason { get; set; }
    }
}
