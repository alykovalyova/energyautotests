﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class RiskCustomerReason
    {
        public int Id { get; set; }
        public int RiskCustomerId { get; set; }
        public int RiskReasonId { get; set; }
        public DateTime CreationDateTime { get; set; }
        public DateTime? DeletionDateTime { get; set; }
        public string DeletionReason { get; set; }
        public decimal? Amount { get; set; }

        public virtual RiskCustomer RiskCustomer { get; set; }
        public virtual RiskReason RiskReason { get; set; }
    }
}
