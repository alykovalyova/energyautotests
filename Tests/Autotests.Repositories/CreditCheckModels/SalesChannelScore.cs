﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CreditCheckModels
{
    public partial class SalesChannelScore
    {
        public int SalesChannelId { get; set; }
        public int? MaxScoreForCredit { get; set; }
    }
}
