﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class ConsentPdfs
    {
        public int Id { get; set; }
        public DateTime SignDate { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public string ZipCode { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string PdfMd5Hash { get; set; }
        public string CephId { get; set; }
        public DateTime CreationTime { get; set; }
        public byte[] TimeStamp { get; set; }
    }
}
