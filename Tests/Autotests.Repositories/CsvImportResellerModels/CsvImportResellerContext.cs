﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class CsvImportResellerContext : DbContext
    {
        private readonly string _connectionString;

        public CsvImportResellerContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CsvImportResellerContext(DbContextOptions<CsvImportResellerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ConsentPdfs> ConsentPdfs { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerValidationModel> CustomerValidationModel { get; set; }
        public virtual DbSet<DictionaryPaymentMethod> DictionaryPaymentMethod { get; set; }
        public virtual DbSet<FileBase> FileBase { get; set; }
        public virtual DbSet<FileValidationMessages> FileValidationMessages { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<MmleadsRegistry> MmleadsRegistry { get; set; }
        public virtual DbSet<RowValidationMessages> RowValidationMessages { get; set; }
        public virtual DbSet<ViewTotalsPerDay> ViewTotalsPerDay { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConsentPdfs>(entity =>
            {
                entity.ToTable("ConsentPdfs", "dbo");

                entity.Property(e => e.CreationTime).HasColumnType("datetime");

                entity.Property(e => e.SignDate).HasColumnType("datetime");

                entity.Property(e => e.TimeStamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer", "dbo");

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.City).HasMaxLength(100);

                entity.Property(e => e.CorrespondenceCity).HasMaxLength(100);

                entity.Property(e => e.CorrespondenceHouseNumberExt).HasMaxLength(50);

                entity.Property(e => e.CorrespondencePostalCode).HasMaxLength(50);

                entity.Property(e => e.CorrespondenceStreetName).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EancodeElectricity)
                    .HasColumnName("Eancode_Electricity")
                    .HasMaxLength(18);

                entity.Property(e => e.EancodeGas)
                    .HasColumnName("Eancode_Gas")
                    .HasMaxLength(18);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.HouseNumberExt).HasMaxLength(50);

                entity.Property(e => e.MmagentIdentification).HasColumnName("MMAgentIdentification");

                entity.Property(e => e.MmleadId).HasColumnName("MMLeadId");

                entity.Property(e => e.PaymentMethodId).HasDefaultValueSql("((1))");

                entity.Property(e => e.PostalCode).HasMaxLength(50);

                entity.Property(e => e.PreferredStartDate).HasColumnType("datetime");

                entity.Property(e => e.SignupDate).HasColumnType("datetime");

                entity.Property(e => e.SjvDal).HasColumnName("Sjv_Dal");

                entity.Property(e => e.SjvGas).HasColumnName("Sjv_Gas");

                entity.Property(e => e.SjvPiek).HasColumnName("Sjv_Piek");

                entity.Property(e => e.SjvProduction).HasColumnName("Sjv_Production");

                entity.Property(e => e.SjvProductionDal).HasColumnName("Sjv_Production_Dal");

                entity.Property(e => e.SjvProductionPiek).HasColumnName("Sjv_Production_Piek");

                entity.Property(e => e.StreetName).HasMaxLength(100);

                entity.HasOne(d => d.PaymentMethod)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.PaymentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Customer_dbo.Dictionary_PaymentMethod_PaymentMethodId");
            });

            modelBuilder.Entity<CustomerValidationModel>(entity =>
            {
                entity.HasKey(e => e.CustomerValidationId)
                    .HasName("PK_dbo.CustomerValidationModel");

                entity.ToTable("CustomerValidationModel", "dbo");

                entity.HasIndex(e => e.FileId);

                entity.HasIndex(e => e.StatusCode);

                entity.HasIndex(e => new { e.Huisnummer, e.CreationDate });

                entity.HasIndex(e => new { e.SalesChannelId, e.CreationDate, e.ExternalOrderId })
                    .HasName("IX_Source_ExternalOrderId");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.EancodeGas).HasColumnName("Eancode_Gas");

                entity.Property(e => e.ExternalOrderId).HasMaxLength(150);

                entity.Property(e => e.FactuurHuisnummer).HasColumnName("Factuur_Huisnummer");

                entity.Property(e => e.FactuurPostcode).HasColumnName("Factuur_Postcode");

                entity.Property(e => e.FactuurStraatnaam).HasColumnName("Factuur_Straatnaam");

                entity.Property(e => e.FactuurToevoeging).HasColumnName("Factuur_Toevoeging");

                entity.Property(e => e.FactuurWoonplaats).HasColumnName("Factuur_Woonplaats");

                entity.Property(e => e.FactuuradresAnders).HasColumnName("Factuuradres_Anders");

                entity.Property(e => e.GeboorteDatum).HasColumnName("Geboorte_Datum");

                entity.Property(e => e.IngangsDatum).HasColumnType("datetime");

                entity.Property(e => e.MmagentIdentification).HasColumnName("MMAgentIdentification");

                entity.Property(e => e.MmleadId).HasColumnName("MMLeadId");

                entity.Property(e => e.PaymentMethodId).HasDefaultValueSql("((1))");

                entity.Property(e => e.ReportedDate).HasColumnType("datetime");

                entity.Property(e => e.SjvDal).HasColumnName("Sjv_Dal");

                entity.Property(e => e.SjvGas).HasColumnName("Sjv_Gas");

                entity.Property(e => e.SjvPiek).HasColumnName("Sjv_Piek");

                entity.Property(e => e.SjvProduction).HasColumnName("Sjv_Production");

                entity.Property(e => e.SjvProductionDal).HasColumnName("Sjv_Production_Dal");

                entity.Property(e => e.SjvProductionPiek).HasColumnName("Sjv_Production_Piek");

                entity.Property(e => e.TekenDatum).HasColumnName("Teken_Datum");

                entity.Property(e => e.TenaamstellingAnders).HasColumnName("Tenaamstelling_Anders");

                entity.Property(e => e.TenaamstellingRekening).HasColumnName("Tenaamstelling_Rekening");

                entity.HasOne(d => d.PaymentMethod)
                    .WithMany(p => p.CustomerValidationModel)
                    .HasForeignKey(d => d.PaymentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CustomerValidationModel_dbo.Dictionary_PaymentMethod_PaymentMethodId");
            });

            modelBuilder.Entity<DictionaryPaymentMethod>(entity =>
            {
                entity.ToTable("Dictionary_PaymentMethod", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<FileBase>(entity =>
            {
                entity.HasKey(e => e.FileId)
                    .HasName("PK_dbo.FileBase");

                entity.ToTable("FileBase", "dbo");

                entity.HasIndex(e => e.StatusCode);

                entity.Property(e => e.CreationTime).HasColumnType("datetime");

                entity.Property(e => e.Delimiter).HasMaxLength(1);

                entity.Property(e => e.FileName).HasMaxLength(150);

                entity.Property(e => e.FullName).HasMaxLength(255);

                entity.Property(e => e.TimeStamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<FileValidationMessages>(entity =>
            {
                entity.HasKey(e => e.MessageId)
                    .HasName("PK_dbo.FileValidationMessages");

                entity.ToTable("FileValidationMessages", "dbo");

                entity.HasIndex(e => e.FileFileId)
                    .HasName("IX_File_FileId");

                entity.Property(e => e.FileFileId).HasColumnName("File_FileId");

                entity.Property(e => e.Message).HasMaxLength(255);

                entity.HasOne(d => d.FileFile)
                    .WithMany(p => p.FileValidationMessages)
                    .HasForeignKey(d => d.FileFileId)
                    .HasConstraintName("FK_dbo.FileValidationMessages_dbo.FileBase_File_FileId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<MmleadsRegistry>(entity =>
            {
                entity.ToTable("MMLeadsRegistry", "dbo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Department).HasMaxLength(50);

                entity.Property(e => e.EmployeeBe)
                    .IsRequired()
                    .HasColumnName("EmployeeBE")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.EmployeeMm)
                    .HasColumnName("EmployeeMM")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<RowValidationMessages>(entity =>
            {
                entity.HasKey(e => e.MessageId)
                    .HasName("PK_dbo.RowValidationMessages");

                entity.ToTable("RowValidationMessages", "dbo");

                entity.HasIndex(e => e.CustomerValidationModelCustomerValidationId)
                    .HasName("IX_CustomerValidationModel_CustomerValidationId");

                entity.Property(e => e.CustomerValidationModelCustomerValidationId).HasColumnName("CustomerValidationModel_CustomerValidationId");

                entity.Property(e => e.Message).HasMaxLength(255);

                entity.HasOne(d => d.CustomerValidationModelCustomerValidation)
                    .WithMany(p => p.RowValidationMessages)
                    .HasForeignKey(d => d.CustomerValidationModelCustomerValidationId)
                    .HasConstraintName("FK_dbo.RowValidationMessages_dbo.CustomerValidationModel_CustomerValidationModel_CustomerValidationId");
            });

            modelBuilder.Entity<ViewTotalsPerDay>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_TotalsPerDay", "dbo");

                entity.Property(e => e.Datum)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SourceNaam)
                    .IsRequired()
                    .HasMaxLength(64);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
