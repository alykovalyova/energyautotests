﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class Customer
    {
        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public int? ImportId { get; set; }
        public string Gender { get; set; }
        public string Surname { get; set; }
        public string Initials { get; set; }
        public string SurnamePrefix { get; set; }
        public string PhoneNumber { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountName { get; set; }
        public bool HasCompany { get; set; }
        public string JobPosition { get; set; }
        public string CompanyName { get; set; }
        public string ChamberOfCommerceNumber { get; set; }
        public string EancodeElectricity { get; set; }
        public string EancodeGas { get; set; }
        public string StreetName { get; set; }
        public int? HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public int ProductId { get; set; }
        public string Gift { get; set; }
        public DateTime? PreferredStartDate { get; set; }
        public bool HasMovedInRecently { get; set; }
        public int Sjv { get; set; }
        public int SjvPiek { get; set; }
        public int SjvDal { get; set; }
        public int SjvGas { get; set; }
        public string CephId { get; set; }
        public string ContractPdfMd5Hash { get; set; }
        public DateTime? SignupDate { get; set; }
        public bool HasCorrespondenceAddress { get; set; }
        public string CorrespondenceStreetName { get; set; }
        public int? CorrespondenceHouseNumber { get; set; }
        public string CorrespondenceHouseNumberExt { get; set; }
        public string CorrespondencePostalCode { get; set; }
        public string CorrespondenceCity { get; set; }
        public DateTime CreatedOn { get; set; }
        public string AgentIdentification { get; set; }
        public string PropositionId { get; set; }
        public bool ShouldGetVoucher { get; set; }
        public bool HasGas { get; set; }
        public bool HasElec { get; set; }
        public string MmagentIdentification { get; set; }
        public int? MmleadId { get; set; }
        public int PaymentMethodId { get; set; }
        public int? SjvProductionPiek { get; set; }
        public int? SjvProductionDal { get; set; }
        public int? SjvProduction { get; set; }
        public int SalesChannelId { get; set; }

        public virtual DictionaryPaymentMethod PaymentMethod { get; set; }
    }
}
