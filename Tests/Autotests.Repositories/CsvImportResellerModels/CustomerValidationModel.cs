﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class CustomerValidationModel
    {
        public CustomerValidationModel()
        {
            RowValidationMessages = new HashSet<RowValidationMessages>();
        }

        public int CustomerValidationId { get; set; }
        public string PlainContractRow { get; set; }
        public string ContractRowHash { get; set; }
        public string ExternalOrderId { get; set; }
        public string Aanhef { get; set; }
        public string Achternaam { get; set; }
        public string Voorletters { get; set; }
        public string Tussenvoegsel { get; set; }
        public string Bedrijfsnaam { get; set; }
        public string KvkNummer { get; set; }
        public string Straatnaam { get; set; }
        public int Huisnummer { get; set; }
        public string Toevoeging { get; set; }
        public string Postcode { get; set; }
        public string Woonplaats { get; set; }
        public string Telefoon { get; set; }
        public string Mobiel { get; set; }
        public string Email { get; set; }
        public string Functie { get; set; }
        public int Product { get; set; }
        public string ProductName { get; set; }
        public bool PerDirect { get; set; }
        public DateTime IngangsDatum { get; set; }
        public bool AdresRecent { get; set; }
        public int Sjv { get; set; }
        public int SjvPiek { get; set; }
        public int SjvDal { get; set; }
        public int SjvGas { get; set; }
        public string Rekeningnummer { get; set; }
        public bool TenaamstellingAnders { get; set; }
        public string TenaamstellingRekening { get; set; }
        public bool FactuuradresAnders { get; set; }
        public string FactuurPostcode { get; set; }
        public int FactuurHuisnummer { get; set; }
        public string FactuurToevoeging { get; set; }
        public string FactuurStraatnaam { get; set; }
        public string FactuurWoonplaats { get; set; }
        public string Geschenk { get; set; }
        public string Eancode { get; set; }
        public string EancodeGas { get; set; }
        public string TekenDatum { get; set; }
        public string GeboorteDatum { get; set; }
        public int StatusCode { get; set; }
        public bool IsNormalized { get; set; }
        public bool IsReported { get; set; }
        public DateTime ReportedDate { get; set; }
        public bool HasError { get; set; }
        public int FileId { get; set; }
        public Guid Guid { get; set; }
        public DateTime CreationDate { get; set; }
        public string ContractPdfMd5Hash { get; set; }
        public string AgentIdentification { get; set; }
        public string PropositionId { get; set; }
        public int? VoucherId { get; set; }
        public bool HasGas { get; set; }
        public bool HasElec { get; set; }
        public bool ShouldGetVoucher { get; set; }
        public string MmagentIdentification { get; set; }
        public int? MmleadId { get; set; }
        public int? LegacyImportContractId { get; set; }
        public int PaymentMethodId { get; set; }
        public int? SjvProductionPiek { get; set; }
        public int? SjvProductionDal { get; set; }
        public int? SjvProduction { get; set; }
        public int SalesChannelId { get; set; }

        public virtual DictionaryPaymentMethod PaymentMethod { get; set; }
        public virtual ICollection<RowValidationMessages> RowValidationMessages { get; set; }
    }
}
