﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class FileBase
    {
        public FileBase()
        {
            FileValidationMessages = new HashSet<FileValidationMessages>();
        }

        public int FileId { get; set; }
        public long Size { get; set; }
        public string FullName { get; set; }
        public string FileName { get; set; }
        public string FileHash { get; set; }
        public string RawFileContent { get; set; }
        public string SourceDirectory { get; set; }
        public int StatusCode { get; set; }
        public string Delimiter { get; set; }
        public DateTime CreationTime { get; set; }
        public byte[] TimeStamp { get; set; }
        public int SalesChannelId { get; set; }

        public virtual ICollection<FileValidationMessages> FileValidationMessages { get; set; }
    }
}
