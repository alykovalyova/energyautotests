﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class MmleadsRegistry
    {
        public int Id { get; set; }
        public bool IsWarmLead { get; set; }
        public DateTime CreatedOn { get; set; }
        public string EmployeeMm { get; set; }
        public string EmployeeBe { get; set; }
        public string Department { get; set; }
        public int SalesChannelId { get; set; }
    }
}
