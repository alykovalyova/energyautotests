﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class RowValidationMessages
    {
        public int MessageId { get; set; }
        public int MessageType { get; set; }
        public string Message { get; set; }
        public bool IsResolved { get; set; }
        public int CustomerValidationModelCustomerValidationId { get; set; }

        public virtual CustomerValidationModel CustomerValidationModelCustomerValidation { get; set; }
    }
}
