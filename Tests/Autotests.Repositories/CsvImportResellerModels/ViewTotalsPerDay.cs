﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CsvImportResellerModels
{
    public partial class ViewTotalsPerDay
    {
        public string Datum { get; set; }
        public string SourceNaam { get; set; }
        public int? Aantal { get; set; }
    }
}
