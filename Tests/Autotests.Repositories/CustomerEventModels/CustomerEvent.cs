﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerEventModels
{
    public partial class CustomerEvent
    {
        public CustomerEvent()
        {
            CustomerSurvey = new HashSet<CustomerSurvey>();
        }

        public long Id { get; set; }
        public long CustomerId { get; set; }
        public short EventTypeId { get; set; }
        public short EventStatusId { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string SourceId { get; set; }
        public short ComponentId { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public long? EmployeeId { get; set; }

        public virtual DictionaryComponent Component { get; set; }
        public virtual DictionaryEventStatus EventStatus { get; set; }
        public virtual DictionaryEventType EventType { get; set; }
        public virtual ICollection<CustomerSurvey> CustomerSurvey { get; set; }
    }
}
