﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerEventModels
{
    public partial class CustomerEventContext : DbContext
    {
        private readonly string _connectionString;

        public CustomerEventContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CustomerEventContext(DbContextOptions<CustomerEventContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CustomerEvent> CustomerEvent { get; set; }
        public virtual DbSet<CustomerSurvey> CustomerSurvey { get; set; }
        public virtual DbSet<DictionaryComponent> DictionaryComponent { get; set; }
        public virtual DbSet<DictionaryEventStatus> DictionaryEventStatus { get; set; }
        public virtual DbSet<DictionaryEventType> DictionaryEventType { get; set; }
        public virtual DbSet<DictionarySurveyStatus> DictionarySurveyStatus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerEvent>(entity =>
            {
                entity.ToTable("CustomerEvent", "dbo");

                entity.HasIndex(e => e.ComponentId);

                entity.HasIndex(e => e.CustomerId);

                entity.HasIndex(e => e.EventStatusId);

                entity.HasIndex(e => e.EventTypeId);

                entity.HasOne(d => d.Component)
                    .WithMany(p => p.CustomerEvent)
                    .HasForeignKey(d => d.ComponentId);

                entity.HasOne(d => d.EventStatus)
                    .WithMany(p => p.CustomerEvent)
                    .HasForeignKey(d => d.EventStatusId);

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.CustomerEvent)
                    .HasForeignKey(d => d.EventTypeId);
            });

            modelBuilder.Entity<CustomerSurvey>(entity =>
            {
                entity.ToTable("CustomerSurvey", "dbo");

                entity.HasIndex(e => e.CustomerEventId);

                entity.HasIndex(e => e.CustomerId);

                entity.HasOne(d => d.CustomerEvent)
                    .WithMany(p => p.CustomerSurvey)
                    .HasForeignKey(d => d.CustomerEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<DictionaryComponent>(entity =>
            {
                entity.ToTable("Dictionary_Component", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEventStatus>(entity =>
            {
                entity.ToTable("Dictionary_EventStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEventType>(entity =>
            {
                entity.ToTable("Dictionary_EventType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionarySurveyStatus>(entity =>
            {
                entity.ToTable("Dictionary_SurveyStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
