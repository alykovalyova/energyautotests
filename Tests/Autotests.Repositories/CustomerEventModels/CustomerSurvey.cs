﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerEventModels
{
    public partial class CustomerSurvey
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string CustomerEmail { get; set; }
        public string SendgridTemplateName { get; set; }
        public DateTime? SendOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public short StatusId { get; set; }
        public long CustomerEventId { get; set; }
        public string Comment { get; set; }
        public DateTime? ExpiredOn { get; set; }

        public virtual CustomerEvent CustomerEvent { get; set; }
    }
}
