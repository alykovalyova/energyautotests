﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class CerUpload
    {
        public CerUpload()
        {
            CerUploadHistory = new HashSet<CerUploadHistory>();
        }

        public int Id { get; set; }
        public byte LabelCode { get; set; }
        public int WeekStamp { get; set; }
        public int CurrentStatusId { get; set; }
        public Guid ExternalReference { get; set; }
        public string CephBucket { get; set; }
        public string CephKey { get; set; }
        public string FileHash { get; set; }
        public string EdsnFileId { get; set; }
        public DateTime Created { get; set; }

        public virtual CerUploadStatus CurrentStatus { get; set; }
        public virtual ICollection<CerUploadHistory> CerUploadHistory { get; set; }
    }
}
