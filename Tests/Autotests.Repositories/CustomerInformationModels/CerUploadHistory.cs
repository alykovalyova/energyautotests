﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class CerUploadHistory
    {
        public int Id { get; set; }
        public int CerUploadId { get; set; }
        public int OldStatusId { get; set; }
        public int NewStatusId { get; set; }
        public bool IsSuccess { get; set; }
        public string Comment { get; set; }
        public DateTime Created { get; set; }

        public virtual CerUpload CerUpload { get; set; }
        public virtual CerUploadStatus NewStatus { get; set; }
        public virtual CerUploadStatus OldStatus { get; set; }
    }
}
