﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class CerUploadStatus
    {
        public CerUploadStatus()
        {
            CerUpload = new HashSet<CerUpload>();
            CerUploadHistoryNewStatus = new HashSet<CerUploadHistory>();
            CerUploadHistoryOldStatus = new HashSet<CerUploadHistory>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CerUpload> CerUpload { get; set; }
        public virtual ICollection<CerUploadHistory> CerUploadHistoryNewStatus { get; set; }
        public virtual ICollection<CerUploadHistory> CerUploadHistoryOldStatus { get; set; }
    }
}
