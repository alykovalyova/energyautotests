﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class ContractDocument
    {
        public ContractDocument()
        {
            ReimbursePenaltyRequest = new HashSet<ReimbursePenaltyRequest>();
            ReimbursePenaltyRequestDocument = new HashSet<ReimbursePenaltyRequestDocument>();
            UrgentSignUpRequestDocument = new HashSet<UrgentSignUpRequestDocument>();
        }

        public int Id { get; set; }
        public Guid ReferenceId { get; set; }
        public int ContractId { get; set; }
        public string DocumentStorageDocumentId { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<ReimbursePenaltyRequest> ReimbursePenaltyRequest { get; set; }
        public virtual ICollection<ReimbursePenaltyRequestDocument> ReimbursePenaltyRequestDocument { get; set; }
        public virtual ICollection<UrgentSignUpRequestDocument> UrgentSignUpRequestDocument { get; set; }
    }
}
