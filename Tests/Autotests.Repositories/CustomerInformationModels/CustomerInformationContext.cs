﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class CustomerInformationContext : DbContext
    {
        private readonly string _connectionString;

        public CustomerInformationContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CustomerInformationContext(DbContextOptions<CustomerInformationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CerUpload> CerUpload { get; set; }
        public virtual DbSet<CerUploadHistory> CerUploadHistory { get; set; }
        public virtual DbSet<CerUploadStatus> CerUploadStatus { get; set; }
        public virtual DbSet<ContractDocument> ContractDocument { get; set; }
        public virtual DbSet<CustomerTimeline> CustomerTimeline { get; set; }
        public virtual DbSet<CustomerTimelineConnection> CustomerTimelineConnection { get; set; }
        public virtual DbSet<ExchangeOfMeter> ExchangeOfMeter { get; set; }
        public virtual DbSet<JobRequestReadingsForUsageEmail> JobRequestReadingsForUsageEmail { get; set; }
        public virtual DbSet<JobSendRenewableContract> JobSendRenewableContract { get; set; }
        public virtual DbSet<MeterReadingEmailEvent> MeterReadingEmailEvent { get; set; }
        public virtual DbSet<MeterReadingEmailProcess> MeterReadingEmailProcess { get; set; }
        public virtual DbSet<NleLegacyCustomerIdentifier> NleLegacyCustomerIdentifier { get; set; }
        public virtual DbSet<RegisterSynchronization> RegisterSynchronization { get; set; }
        public virtual DbSet<RegisterSynchronizationHistory> RegisterSynchronizationHistory { get; set; }
        public virtual DbSet<RegisterSynchronizationStatus> RegisterSynchronizationStatus { get; set; }
        public virtual DbSet<ReimbursePenaltyRequest> ReimbursePenaltyRequest { get; set; }
        public virtual DbSet<ReimbursePenaltyRequestDocument> ReimbursePenaltyRequestDocument { get; set; }
        public virtual DbSet<ReimbursePenaltyRequestStatus> ReimbursePenaltyRequestStatus { get; set; }
        public virtual DbSet<SendEmailAcceptReimbursePenalty> SendEmailAcceptReimbursePenalty { get; set; }
        public virtual DbSet<SendEmailConfirmCancel> SendEmailConfirmCancel { get; set; }
        public virtual DbSet<SendEmailCustomerData> SendEmailCustomerData { get; set; }
        public virtual DbSet<SendEmailCustomerSatisfaction> SendEmailCustomerSatisfaction { get; set; }
        public virtual DbSet<SendEmailExchangeOfMeter> SendEmailExchangeOfMeter { get; set; }
        public virtual DbSet<SendEmailMeterReading> SendEmailMeterReading { get; set; }
        public virtual DbSet<SendEmailSmartMeterUsage> SendEmailSmartMeterUsage { get; set; }
        public virtual DbSet<SendEmailVariableTariff> SendEmailVariableTariff { get; set; }
        public virtual DbSet<SendEmailVariableTariffEan> SendEmailVariableTariffEan { get; set; }
        public virtual DbSet<SendEmailWelcome> SendEmailWelcome { get; set; }
        public virtual DbSet<SendYearlyMeterReadingReminder> SendYearlyMeterReadingReminder { get; set; }
        public virtual DbSet<SmartMeterPeriod> SmartMeterPeriod { get; set; }
        public virtual DbSet<SmartMeterReadingRequest> SmartMeterReadingRequest { get; set; }
        public virtual DbSet<SmartMeterRegister> SmartMeterRegister { get; set; }
        public virtual DbSet<SupplyAddress> SupplyAddress { get; set; }
        public virtual DbSet<UrgentSignUpRequest> UrgentSignUpRequest { get; set; }
        public virtual DbSet<UrgentSignUpRequestDocument> UrgentSignUpRequestDocument { get; set; }
        public virtual DbSet<UrgentSignUpRequestStatus> UrgentSignUpRequestStatus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CerUpload>(entity =>
            {
                entity.ToTable("CerUpload", "dbo");

                entity.Property(e => e.CephBucket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CephKey)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.EdsnFileId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FileHash)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.CurrentStatus)
                    .WithMany(p => p.CerUpload)
                    .HasForeignKey(d => d.CurrentStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CerUpload_CerUploadStatus");
            });

            modelBuilder.Entity<CerUploadHistory>(entity =>
            {
                entity.ToTable("CerUploadHistory", "dbo");

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.CerUpload)
                    .WithMany(p => p.CerUploadHistory)
                    .HasForeignKey(d => d.CerUploadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CerUploadHistory_CerUpload");

                entity.HasOne(d => d.NewStatus)
                    .WithMany(p => p.CerUploadHistoryNewStatus)
                    .HasForeignKey(d => d.NewStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NewStatus_CerUploadStatus");

                entity.HasOne(d => d.OldStatus)
                    .WithMany(p => p.CerUploadHistoryOldStatus)
                    .HasForeignKey(d => d.OldStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OldStatus_CerUploadStatus");
            });

            modelBuilder.Entity<CerUploadStatus>(entity =>
            {
                entity.ToTable("CerUploadStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContractDocument>(entity =>
            {
                entity.ToTable("ContractDocument", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_ContractDocument_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.DocumentStorageDocumentId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<CustomerTimeline>(entity =>
            {
                entity.ToTable("CustomerTimeline", "dbo");

                entity.HasIndex(e => e.EnerfreeRelationId)
                    .HasName("IX_EnerfreeRelation");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.SupplyAddress)
                    .WithMany(p => p.CustomerTimeline)
                    .HasForeignKey(d => d.SupplyAddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTimeLine_SupplyAddress");
            });

            modelBuilder.Entity<CustomerTimelineConnection>(entity =>
            {
                entity.ToTable("CustomerTimelineConnection", "dbo");

                entity.HasIndex(e => e.CustomerTimelineId)
                    .HasName("IX_CustomerTimelineConnection_CustomerTimeline");

                entity.HasIndex(e => e.Ean);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasColumnName("EAN")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.HasOne(d => d.CustomerTimeline)
                    .WithMany(p => p.CustomerTimelineConnection)
                    .HasForeignKey(d => d.CustomerTimelineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomerTimelineConnection_CustomerTimeline");
            });

            modelBuilder.Entity<ExchangeOfMeter>(entity =>
            {
                entity.ToTable("ExchangeOfMeter", "dbo");

                entity.HasIndex(e => e.SendEmailExchangeOfMeterId);

                entity.HasIndex(e => new { e.MutationDate, e.Ean });

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.NewCity)
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.NewHousenumberSuffix)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.NewMeterNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NewStreet)
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.OldCity)
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.OldHousenumberSuffix)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.OldMeterNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OldStreet)
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.HasOne(d => d.SendEmailExchangeOfMeter)
                    .WithMany(p => p.ExchangeOfMeter)
                    .HasForeignKey(d => d.SendEmailExchangeOfMeterId)
                    .HasConstraintName("FK_ExchangeOfMeter_SendEmailExchangeOfMeter");
            });

            modelBuilder.Entity<JobRequestReadingsForUsageEmail>(entity =>
            {
                entity.ToTable("JobRequestReadingsForUsageEmail", "dbo");

                entity.Property(e => e.Finished).HasColumnType("datetime");

                entity.Property(e => e.RequestDate).HasColumnType("date");

                entity.Property(e => e.Start).HasColumnType("datetime");
            });

            modelBuilder.Entity<JobSendRenewableContract>(entity =>
            {
                entity.ToTable("JobSendRenewableContract", "dbo");

                entity.Property(e => e.Finished).HasColumnType("datetime");

                entity.Property(e => e.JobRunDate).HasColumnType("datetime");

                entity.Property(e => e.Started).HasColumnType("datetime");
            });

            modelBuilder.Entity<MeterReadingEmailEvent>(entity =>
            {
                entity.ToTable("MeterReadingEmailEvent", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MeterReadingEmailProcess>(entity =>
            {
                entity.ToTable("MeterReadingEmailProcess", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NleLegacyCustomerIdentifier>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("NleLegacyCustomerIdentifier", "dbo");

                entity.Property(e => e.BankAccount)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DebtorId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HouseNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HouseNumberExtension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Initials)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NutsRelationId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RegisterSynchronization>(entity =>
            {
                entity.ToTable("RegisterSynchronization", "dbo");

                entity.Property(e => e.CephBucket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CephKey)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.ReferenceDate).HasColumnType("date");

                entity.HasOne(d => d.CurrentStatus)
                    .WithMany(p => p.RegisterSynchronization)
                    .HasForeignKey(d => d.CurrentStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RegisterSynchronization_RegisterSynchronizationStatus");
            });

            modelBuilder.Entity<RegisterSynchronizationHistory>(entity =>
            {
                entity.ToTable("RegisterSynchronizationHistory", "dbo");

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.NewStatus)
                    .WithMany(p => p.RegisterSynchronizationHistoryNewStatus)
                    .HasForeignKey(d => d.NewStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NewStatus_RegisterSynchronizationStatus");

                entity.HasOne(d => d.OldStatus)
                    .WithMany(p => p.RegisterSynchronizationHistoryOldStatus)
                    .HasForeignKey(d => d.OldStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OldStatus_RegisterSynchronizationStatus");

                entity.HasOne(d => d.RegisterSynchronization)
                    .WithMany(p => p.RegisterSynchronizationHistory)
                    .HasForeignKey(d => d.RegisterSynchronizationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RegisterSynchronizationHistory_RegisterSynchronization");
            });

            modelBuilder.Entity<RegisterSynchronizationStatus>(entity =>
            {
                entity.ToTable("RegisterSynchronizationStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReimbursePenaltyRequest>(entity =>
            {
                entity.ToTable("ReimbursePenaltyRequest", "dbo");

                entity.HasIndex(e => e.ContractId);

                entity.HasIndex(e => e.ProofContractDocumentId);

                entity.Property(e => e.AcceptedAmount).HasColumnType("money");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.RequestedAmount).HasColumnType("money");

                entity.HasOne(d => d.ProofContractDocument)
                    .WithMany(p => p.ReimbursePenaltyRequest)
                    .HasForeignKey(d => d.ProofContractDocumentId)
                    .HasConstraintName("FK_ReimbursePenaltyRequest_ContractDocument");

                entity.HasOne(d => d.ReimbursePenaltyRequestStatus)
                    .WithMany(p => p.ReimbursePenaltyRequest)
                    .HasForeignKey(d => d.ReimbursePenaltyRequestStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReimbursePenaltyRequest_ReimbursePenaltyRequestStatus");
            });

            modelBuilder.Entity<ReimbursePenaltyRequestDocument>(entity =>
            {
                entity.ToTable("ReimbursePenaltyRequestDocument", "dbo");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.ProofContractDocument)
                    .WithMany(p => p.ReimbursePenaltyRequestDocument)
                    .HasForeignKey(d => d.ProofContractDocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReimbursePenaltyRequestDocument_ContractDocument");

                entity.HasOne(d => d.ReimbursePenaltyRequest)
                    .WithMany(p => p.ReimbursePenaltyRequestDocument)
                    .HasForeignKey(d => d.ReimbursePenaltyRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReimbursePenaltyRequestDocument_ReimbursePenaltyRequest");
            });

            modelBuilder.Entity<ReimbursePenaltyRequestStatus>(entity =>
            {
                entity.ToTable("ReimbursePenaltyRequestStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SendEmailAcceptReimbursePenalty>(entity =>
            {
                entity.ToTable("SendEmailAcceptReimbursePenalty", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailAcceptReimbursePenalty")
                    .IsUnique();

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.Created).HasColumnType("datetime");
            });

            modelBuilder.Entity<SendEmailConfirmCancel>(entity =>
            {
                entity.ToTable("SendEmailConfirmCancel", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailConfirmCancel_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");
            });

            modelBuilder.Entity<SendEmailCustomerData>(entity =>
            {
                entity.ToTable("SendEmailCustomerData", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailCustomerData_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");
            });

            modelBuilder.Entity<SendEmailCustomerSatisfaction>(entity =>
            {
                entity.ToTable("SendEmailCustomerSatisfaction", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailCustomerSatisfaction")
                    .IsUnique();

                entity.Property(e => e.CatCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.EmployeeFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCatCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TypeCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SendEmailExchangeOfMeter>(entity =>
            {
                entity.ToTable("SendEmailExchangeOfMeter", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailExchangeOfMeter_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");
            });

            modelBuilder.Entity<SendEmailMeterReading>(entity =>
            {
                entity.ToTable("SendEmailMeterReading", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailMeterReading_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.EanElectricity)
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.EanGas)
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.HasOne(d => d.MeterReadingEmailEvent)
                    .WithMany(p => p.SendEmailMeterReading)
                    .HasForeignKey(d => d.MeterReadingEmailEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SendEmailMeterReading_MeterReadingEmailEvent");

                entity.HasOne(d => d.MeterReadingEmailProcess)
                    .WithMany(p => p.SendEmailMeterReading)
                    .HasForeignKey(d => d.MeterReadingEmailProcessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SendEmailMeterReading_MeterReadingEmailProcess");
            });

            modelBuilder.Entity<SendEmailSmartMeterUsage>(entity =>
            {
                entity.ToTable("SendEmailSmartMeterUsage", "dbo");

                entity.HasIndex(e => e.CustomerTimelineId)
                    .HasName("UX_SendEmailSmartMeterUsage_CustomerTimelineId");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailSmartMeterUsage_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.PeriodFrom).HasColumnType("date");

                entity.Property(e => e.PeriodTo).HasColumnType("date");

                entity.HasOne(d => d.CustomerTimeline)
                    .WithMany(p => p.SendEmailSmartMeterUsage)
                    .HasForeignKey(d => d.CustomerTimelineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SendEmailSmartMeterUsage_CustomerTimeline");
            });

            modelBuilder.Entity<SendEmailVariableTariff>(entity =>
            {
                entity.ToTable("SendEmailVariableTariff", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailVariableTariff_ReferenceId")
                    .IsUnique();

                entity.HasIndex(e => new { e.ContractId, e.TariffStartDate, e.ZipCode, e.HouseNumber, e.HouseNumberSuffix })
                    .HasName("UX_ContractDateAddress")
                    .IsUnique();

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.HouseNumberSuffix)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.TariffStartDate).HasColumnType("date");

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SendEmailVariableTariffEan>(entity =>
            {
                entity.ToTable("SendEmailVariableTariffEan", "dbo");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasColumnName("EAN")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.HasOne(d => d.SendEmailVariableTariff)
                    .WithMany(p => p.SendEmailVariableTariffEan)
                    .HasForeignKey(d => d.SendEmailVariableTariffId)
                    .HasConstraintName("FK_SendEmailVariableTariffEan_SendEmailVariableTariff");
            });

            modelBuilder.Entity<SendEmailWelcome>(entity =>
            {
                entity.ToTable("SendEmailWelcome", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_SendEmailWelcome_ReferenceId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");
            });

            modelBuilder.Entity<SendYearlyMeterReadingReminder>(entity =>
            {
                entity.ToTable("SendYearlyMeterReadingReminder", "dbo");

                entity.Property(e => e.Created).HasColumnType("datetime");
            });

            modelBuilder.Entity<SmartMeterPeriod>(entity =>
            {
                entity.ToTable("SmartMeterPeriod", "dbo");

                entity.HasIndex(e => e.CustomerTimelineConnectionId)
                    .HasName("IX_SmartMeterPeriod_CustomerTimelineConnectionID");

                entity.HasIndex(e => e.SendEmailSmartMeterUsageId);

                entity.Property(e => e.CostsStandingCharge).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.CostsTransport).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.PeriodFrom).HasColumnType("date");

                entity.Property(e => e.PeriodTo).HasColumnType("date");

                entity.HasOne(d => d.CustomerTimelineConnection)
                    .WithMany(p => p.SmartMeterPeriod)
                    .HasForeignKey(d => d.CustomerTimelineConnectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SmartMeterPeriod_CustomerTimelineConnection");

                entity.HasOne(d => d.SendEmailSmartMeterUsage)
                    .WithMany(p => p.SmartMeterPeriod)
                    .HasForeignKey(d => d.SendEmailSmartMeterUsageId)
                    .HasConstraintName("FK_SmartMeterPeriod_SendEmailSmartMeterUsage");
            });

            modelBuilder.Entity<SmartMeterReadingRequest>(entity =>
            {
                entity.ToTable("SmartMeterReadingRequest", "dbo");

                entity.HasIndex(e => e.ContractId)
                    .HasName("UX_SmartMeterReadingRequest")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.LastRequestDate).HasColumnType("date");
            });

            modelBuilder.Entity<SmartMeterRegister>(entity =>
            {
                entity.ToTable("SmartMeterRegister", "dbo");

                entity.HasIndex(e => e.SmartMeterPeriodId)
                    .HasName("IX_SmartMeterRegister_SmartMeterPeriodID");

                entity.Property(e => e.Cost).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Tariff).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.SmartMeterPeriod)
                    .WithMany(p => p.SmartMeterRegister)
                    .HasForeignKey(d => d.SmartMeterPeriodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SmartMeterRegister_SmartMeterPeriod");
            });

            modelBuilder.Entity<SupplyAddress>(entity =>
            {
                entity.ToTable("SupplyAddress", "dbo");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.HouseNumberExtension)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UrgentSignUpRequest>(entity =>
            {
                entity.ToTable("UrgentSignUpRequest", "dbo");

                entity.HasIndex(e => e.ContractId);

                entity.HasIndex(e => e.ProofContractDocumentId);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.RequestedStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.UrgentSignUpRequestStatus)
                    .WithMany(p => p.UrgentSignUpRequest)
                    .HasForeignKey(d => d.UrgentSignUpRequestStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UrgentSignUpRequest_UrgentSignUpRequestStatus");
            });

            modelBuilder.Entity<UrgentSignUpRequestDocument>(entity =>
            {
                entity.ToTable("UrgentSignUpRequestDocument", "dbo");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.ProofContractDocument)
                    .WithMany(p => p.UrgentSignUpRequestDocument)
                    .HasForeignKey(d => d.ProofContractDocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UrgentSignUpRequestDocument_ContractDocument");

                entity.HasOne(d => d.UrgentSignUpRequest)
                    .WithMany(p => p.UrgentSignUpRequestDocument)
                    .HasForeignKey(d => d.UrgentSignUpRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UrgentSignUpRequestDocument_UrgentSignUpRequest");
            });

            modelBuilder.Entity<UrgentSignUpRequestStatus>(entity =>
            {
                entity.ToTable("UrgentSignUpRequestStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
