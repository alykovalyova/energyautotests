﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class CustomerTimeline
    {
        public CustomerTimeline()
        {
            CustomerTimelineConnection = new HashSet<CustomerTimelineConnection>();
            SendEmailSmartMeterUsage = new HashSet<SendEmailSmartMeterUsage>();
        }

        public int Id { get; set; }
        public int EnerfreeRelationId { get; set; }
        public DateTime Created { get; set; }
        public int SupplyAddressId { get; set; }

        public virtual SupplyAddress SupplyAddress { get; set; }
        public virtual ICollection<CustomerTimelineConnection> CustomerTimelineConnection { get; set; }
        public virtual ICollection<SendEmailSmartMeterUsage> SendEmailSmartMeterUsage { get; set; }
    }
}
