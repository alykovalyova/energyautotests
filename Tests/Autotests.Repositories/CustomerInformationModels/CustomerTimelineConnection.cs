﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class CustomerTimelineConnection
    {
        public CustomerTimelineConnection()
        {
            SmartMeterPeriod = new HashSet<SmartMeterPeriod>();
        }

        public int Id { get; set; }
        public int CustomerTimelineId { get; set; }
        public int ProductType { get; set; }
        public string Ean { get; set; }
        public DateTime Created { get; set; }

        public virtual CustomerTimeline CustomerTimeline { get; set; }
        public virtual ICollection<SmartMeterPeriod> SmartMeterPeriod { get; set; }
    }
}
