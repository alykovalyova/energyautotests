﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class ExchangeOfMeter
    {
        public int Id { get; set; }
        public DateTime MutationDate { get; set; }
        public string NewStreet { get; set; }
        public int? NewHousenumber { get; set; }
        public string NewHousenumberSuffix { get; set; }
        public string NewCity { get; set; }
        public string NewMeterNumber { get; set; }
        public int? NewNumberOfRegisters { get; set; }
        public int? NewEnergyFlowDirection { get; set; }
        public bool? NewAdministrativeStatusSmartMeter { get; set; }
        public string OldStreet { get; set; }
        public int? OldHousenumber { get; set; }
        public string OldHousenumberSuffix { get; set; }
        public string OldCity { get; set; }
        public string OldMeterNumber { get; set; }
        public int? OldNumberofRegisters { get; set; }
        public int? OldEnergyFlowDirection { get; set; }
        public bool? OldAdministrativeStatusSmartMeter { get; set; }
        public DateTime Created { get; set; }
        public string Ean { get; set; }
        public int ProductType { get; set; }
        public int EnerfreeRelationId { get; set; }
        public int? SendEmailExchangeOfMeterId { get; set; }

        public virtual SendEmailExchangeOfMeter SendEmailExchangeOfMeter { get; set; }
    }
}
