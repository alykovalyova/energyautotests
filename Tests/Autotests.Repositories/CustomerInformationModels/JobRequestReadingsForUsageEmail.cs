﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class JobRequestReadingsForUsageEmail
    {
        public int Id { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime Start { get; set; }
        public DateTime? Finished { get; set; }
    }
}
