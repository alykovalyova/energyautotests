﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class MeterReadingEmailProcess
    {
        public MeterReadingEmailProcess()
        {
            SendEmailMeterReading = new HashSet<SendEmailMeterReading>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SendEmailMeterReading> SendEmailMeterReading { get; set; }
    }
}
