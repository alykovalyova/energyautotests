﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class NleLegacyCustomerIdentifier
    {
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public string DebtorId { get; set; }
        public string NutsRelationId { get; set; }
        public string CustomerType { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public string BankAccount { get; set; }
        public string ZipCode { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
    }
}
