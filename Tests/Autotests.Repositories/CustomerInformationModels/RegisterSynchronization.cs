﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class RegisterSynchronization
    {
        public RegisterSynchronization()
        {
            RegisterSynchronizationHistory = new HashSet<RegisterSynchronizationHistory>();
        }

        public int Id { get; set; }
        public byte LabelCode { get; set; }
        public DateTime ReferenceDate { get; set; }
        public int? NumberOfEntries { get; set; }
        public int CurrentStatusId { get; set; }
        public Guid ExternalReference { get; set; }
        public string CephBucket { get; set; }
        public string CephKey { get; set; }
        public DateTime Created { get; set; }

        public virtual RegisterSynchronizationStatus CurrentStatus { get; set; }
        public virtual ICollection<RegisterSynchronizationHistory> RegisterSynchronizationHistory { get; set; }
    }
}
