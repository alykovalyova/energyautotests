﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class RegisterSynchronizationHistory
    {
        public int Id { get; set; }
        public int RegisterSynchronizationId { get; set; }
        public int OldStatusId { get; set; }
        public int NewStatusId { get; set; }
        public bool IsSuccess { get; set; }
        public string Comment { get; set; }
        public DateTime Created { get; set; }

        public virtual RegisterSynchronizationStatus NewStatus { get; set; }
        public virtual RegisterSynchronizationStatus OldStatus { get; set; }
        public virtual RegisterSynchronization RegisterSynchronization { get; set; }
    }
}
