﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class RegisterSynchronizationStatus
    {
        public RegisterSynchronizationStatus()
        {
            RegisterSynchronization = new HashSet<RegisterSynchronization>();
            RegisterSynchronizationHistoryNewStatus = new HashSet<RegisterSynchronizationHistory>();
            RegisterSynchronizationHistoryOldStatus = new HashSet<RegisterSynchronizationHistory>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RegisterSynchronization> RegisterSynchronization { get; set; }
        public virtual ICollection<RegisterSynchronizationHistory> RegisterSynchronizationHistoryNewStatus { get; set; }
        public virtual ICollection<RegisterSynchronizationHistory> RegisterSynchronizationHistoryOldStatus { get; set; }
    }
}
