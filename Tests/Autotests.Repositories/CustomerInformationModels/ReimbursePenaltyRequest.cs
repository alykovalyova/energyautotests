﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class ReimbursePenaltyRequest
    {
        public ReimbursePenaltyRequest()
        {
            ReimbursePenaltyRequestDocument = new HashSet<ReimbursePenaltyRequestDocument>();
        }

        public int Id { get; set; }
        public int ContractId { get; set; }
        public decimal RequestedAmount { get; set; }
        public decimal? AcceptedAmount { get; set; }
        public DateTime Created { get; set; }
        public int? ProofContractDocumentId { get; set; }
        public int ReimbursePenaltyRequestStatusId { get; set; }

        public virtual ContractDocument ProofContractDocument { get; set; }
        public virtual ReimbursePenaltyRequestStatus ReimbursePenaltyRequestStatus { get; set; }
        public virtual ICollection<ReimbursePenaltyRequestDocument> ReimbursePenaltyRequestDocument { get; set; }
    }
}
