﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class ReimbursePenaltyRequestDocument
    {
        public int Id { get; set; }
        public int ReimbursePenaltyRequestId { get; set; }
        public int ProofContractDocumentId { get; set; }
        public DateTime Created { get; set; }

        public virtual ContractDocument ProofContractDocument { get; set; }
        public virtual ReimbursePenaltyRequest ReimbursePenaltyRequest { get; set; }
    }
}
