﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SendEmailConfirmCancel
    {
        public int Id { get; set; }
        public int EnerFreeRelationId { get; set; }
        public int EnerFreeContractId { get; set; }
        public Guid ReferenceId { get; set; }
        public DateTime Created { get; set; }
    }
}
