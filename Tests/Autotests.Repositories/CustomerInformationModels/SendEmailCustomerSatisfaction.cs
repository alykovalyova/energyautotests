﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SendEmailCustomerSatisfaction
    {
        public int Id { get; set; }
        public int EnerFreeRelationId { get; set; }
        public string TypeCode { get; set; }
        public string CatCode { get; set; }
        public string SubCatCode { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public Guid ReferenceId { get; set; }
        public DateTime Created { get; set; }
    }
}
