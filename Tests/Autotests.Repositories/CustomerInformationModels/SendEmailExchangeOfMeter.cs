﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SendEmailExchangeOfMeter
    {
        public SendEmailExchangeOfMeter()
        {
            ExchangeOfMeter = new HashSet<ExchangeOfMeter>();
        }

        public int Id { get; set; }
        public Guid ReferenceId { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<ExchangeOfMeter> ExchangeOfMeter { get; set; }
    }
}
