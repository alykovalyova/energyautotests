﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SendEmailMeterReading
    {
        public int Id { get; set; }
        public int EnerFreeRelationId { get; set; }
        public int EnerfreeContractId { get; set; }
        public int MeterReadingEmailProcessId { get; set; }
        public int MeterReadingEmailEventId { get; set; }
        public long ProcessRunnerProcessId { get; set; }
        public string EanElectricity { get; set; }
        public string EanGas { get; set; }
        public int? ConsumptionMeterReadingIdElectricity { get; set; }
        public int? ConsumptionMeterReadingIdGas { get; set; }
        public Guid ReferenceId { get; set; }
        public DateTime Created { get; set; }

        public virtual MeterReadingEmailEvent MeterReadingEmailEvent { get; set; }
        public virtual MeterReadingEmailProcess MeterReadingEmailProcess { get; set; }
    }
}
