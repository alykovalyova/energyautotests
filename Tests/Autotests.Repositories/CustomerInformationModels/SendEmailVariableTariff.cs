﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SendEmailVariableTariff
    {
        public SendEmailVariableTariff()
        {
            SendEmailVariableTariffEan = new HashSet<SendEmailVariableTariffEan>();
        }

        public int Id { get; set; }
        public Guid ReferenceId { get; set; }
        public int ContractId { get; set; }
        public string Street { get; set; }
        public int? HouseNumber { get; set; }
        public string HouseNumberSuffix { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public DateTime TariffStartDate { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<SendEmailVariableTariffEan> SendEmailVariableTariffEan { get; set; }
    }
}
