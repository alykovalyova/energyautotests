﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SendEmailWelcome
    {
        public int Id { get; set; }
        public bool? ModifiedDuringCooldown { get; set; }
        public DateTime Created { get; set; }
        public bool? IsMoveIn { get; set; }
        public int EnerfreeContractId { get; set; }
        public int EnerFreeRelationId { get; set; }
        public Guid ReferenceId { get; set; }
    }
}
