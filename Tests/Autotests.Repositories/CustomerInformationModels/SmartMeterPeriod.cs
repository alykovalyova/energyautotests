﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SmartMeterPeriod
    {
        public SmartMeterPeriod()
        {
            SmartMeterRegister = new HashSet<SmartMeterRegister>();
        }

        public int Id { get; set; }
        public int CustomerTimelineConnectionId { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public DateTime Created { get; set; }
        public int? SendEmailSmartMeterUsageId { get; set; }
        public decimal? CostsStandingCharge { get; set; }
        public decimal? CostsTransport { get; set; }

        public virtual CustomerTimelineConnection CustomerTimelineConnection { get; set; }
        public virtual SendEmailSmartMeterUsage SendEmailSmartMeterUsage { get; set; }
        public virtual ICollection<SmartMeterRegister> SmartMeterRegister { get; set; }
    }
}
