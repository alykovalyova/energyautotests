﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SmartMeterRegister
    {
        public int Id { get; set; }
        public int SmartMeterPeriodId { get; set; }
        public int MeteringDirectionId { get; set; }
        public int TariffTypeId { get; set; }
        public int? Start { get; set; }
        public int? End { get; set; }
        public int? Usage { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Tariff { get; set; }
        public DateTime Created { get; set; }
        public int? Sjv { get; set; }
        public int? ProductionSjv { get; set; }

        public virtual SmartMeterPeriod SmartMeterPeriod { get; set; }
    }
}
