﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class SupplyAddress
    {
        public SupplyAddress()
        {
            CustomerTimeline = new HashSet<CustomerTimeline>();
        }

        public int Id { get; set; }
        public string Street { get; set; }
        public int? HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<CustomerTimeline> CustomerTimeline { get; set; }
    }
}
