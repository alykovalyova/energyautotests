﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class UrgentSignUpRequest
    {
        public UrgentSignUpRequest()
        {
            UrgentSignUpRequestDocument = new HashSet<UrgentSignUpRequestDocument>();
        }

        public int Id { get; set; }
        public int ContractId { get; set; }
        public DateTime RequestedStartDate { get; set; }
        public DateTime Created { get; set; }
        public int? ProofContractDocumentId { get; set; }
        public int UrgentSignUpRequestStatusId { get; set; }

        public virtual UrgentSignUpRequestStatus UrgentSignUpRequestStatus { get; set; }
        public virtual ICollection<UrgentSignUpRequestDocument> UrgentSignUpRequestDocument { get; set; }
    }
}
