﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerInformationModels
{
    public partial class UrgentSignUpRequestStatus
    {
        public UrgentSignUpRequestStatus()
        {
            UrgentSignUpRequest = new HashSet<UrgentSignUpRequest>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UrgentSignUpRequest> UrgentSignUpRequest { get; set; }
    }
}
