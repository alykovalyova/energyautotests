﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class AppNotificationDevice
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int ContactPersonKeyId { get; set; }
        public string AppNotificationToken { get; set; }
        public int AppNotificationMessageTypeId { get; set; }
        public DateTimeOffset? ValidTo { get; set; }
        public int ChangeSourceSystemId { get; set; }
        public string Comment { get; set; }
        public string CreatedByUser { get; set; }

        public virtual DictionaryAppNotificationMessageType AppNotificationMessageType { get; set; }
        public virtual DictionaryChangeSourceSystem ChangeSourceSystem { get; set; }
        public virtual ContactPersonKey ContactPersonKey { get; set; }
    }
}
