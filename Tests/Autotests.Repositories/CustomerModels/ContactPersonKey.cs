﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class ContactPersonKey
    {
        public ContactPersonKey()
        {
            AppNotificationDevice = new HashSet<AppNotificationDevice>();
            ContactPersonEmailRequest = new HashSet<ContactPersonEmailRequest>();
            CustomerContactPerson = new HashSet<CustomerContactPerson>();
        }

        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int ContactPersonNumber { get; set; }
        public string Email { get; set; }
        public int? LabelId { get; set; }

        public virtual ContactPerson ContactPerson { get; set; }
        public virtual ICollection<AppNotificationDevice> AppNotificationDevice { get; set; }
        public virtual ICollection<ContactPersonEmailRequest> ContactPersonEmailRequest { get; set; }
        public virtual ICollection<CustomerContactPerson> CustomerContactPerson { get; set; }
    }
}
