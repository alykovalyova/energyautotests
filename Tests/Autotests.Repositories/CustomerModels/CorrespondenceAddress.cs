﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class CorrespondenceAddress
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int PersonKeyId { get; set; }
        public int CountryId { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string PostalCode { get; set; }
        public int ChangeSourceSystemId { get; set; }
        public string CreatedByUser { get; set; }
        public string Comment { get; set; }
        public DateTimeOffset? ValidTo { get; set; }

        public virtual DictionaryChangeSourceSystem ChangeSourceSystem { get; set; }
        public virtual DictionaryCountry Country { get; set; }
        public virtual PersonKey PersonKey { get; set; }
    }
}
