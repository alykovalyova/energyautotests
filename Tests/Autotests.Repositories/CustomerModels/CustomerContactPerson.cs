﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class CustomerContactPerson
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int NutsHomeCustomerId { get; set; }
        public int ContactPersonKeyId { get; set; }
        public int ChangeSourceSystemId { get; set; }
        public string CreatedByUser { get; set; }
        public string Comment { get; set; }
        public DateTimeOffset? ValidTo { get; set; }
        public int RoleId { get; set; }

        public virtual DictionaryChangeSourceSystem ChangeSourceSystem { get; set; }
        public virtual ContactPersonKey ContactPersonKey { get; set; }
        public virtual NutsHomeCustomer NutsHomeCustomer { get; set; }
        public virtual DictionaryCustomerContactPersonRole Role { get; set; }
    }
}
