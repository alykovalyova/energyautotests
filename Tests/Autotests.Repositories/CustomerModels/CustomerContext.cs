﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class CustomerContext : DbContext
    {
        private readonly string _connectionString;

        public CustomerContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CustomerContext(DbContextOptions<CustomerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppNotificationDevice> AppNotificationDevice { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<ContactPerson> ContactPerson { get; set; }
        public virtual DbSet<ContactPersonEmailRequest> ContactPersonEmailRequest { get; set; }
        public virtual DbSet<ContactPersonKey> ContactPersonKey { get; set; }
        public virtual DbSet<CorrespondenceAddress> CorrespondenceAddress { get; set; }
        public virtual DbSet<CustomerContactPerson> CustomerContactPerson { get; set; }
        public virtual DbSet<CustomerInvoice> CustomerInvoice { get; set; }
        public virtual DbSet<DictionaryAppNotificationMessageType> DictionaryAppNotificationMessageType { get; set; }
        public virtual DbSet<DictionaryChangeSourceSystem> DictionaryChangeSourceSystem { get; set; }
        public virtual DbSet<DictionaryCountry> DictionaryCountry { get; set; }
        public virtual DbSet<DictionaryCustomerContactPersonRole> DictionaryCustomerContactPersonRole { get; set; }
        public virtual DbSet<DictionaryCustomerProductType> DictionaryCustomerProductType { get; set; }
        public virtual DbSet<DictionaryGender> DictionaryGender { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabel { get; set; }
        public virtual DbSet<DictionaryPaymentMethod> DictionaryPaymentMethod { get; set; }
        public virtual DbSet<DictionaryPersonType> DictionaryPersonType { get; set; }
        public virtual DbSet<DictionaryPhoneNumberType> DictionaryPhoneNumberType { get; set; }
        public virtual DbSet<EmailAddress> EmailAddress { get; set; }
        public virtual DbSet<EmailAddressRequest> EmailAddressRequest { get; set; }
        public virtual DbSet<NutsHomeCustomer> NutsHomeCustomer { get; set; }
        public virtual DbSet<PaymentDetails> PaymentDetails { get; set; }
        public virtual DbSet<PaymentDetailsRequest> PaymentDetailsRequest { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<PersonKey> PersonKey { get; set; }
        public virtual DbSet<PhoneNumber> PhoneNumber { get; set; }
        public virtual DbSet<PhoneNumbersRequest> PhoneNumbersRequest { get; set; }
        public virtual DbSet<ProductCustomer> ProductCustomer { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppNotificationDevice>(entity =>
            {
                entity.HasIndex(e => e.AppNotificationMessageTypeId);

                entity.HasIndex(e => e.AppNotificationToken)
                    .HasName("UX_AppNotificationDevice_AppNotificationToken")
                    .IsUnique();

                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.ContactPersonKeyId);

                entity.Property(e => e.AppNotificationToken)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.AppNotificationMessageType)
                    .WithMany(p => p.AppNotificationDevice)
                    .HasForeignKey(d => d.AppNotificationMessageTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.AppNotificationDevice)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ContactPersonKey)
                    .WithMany(p => p.AppNotificationDevice)
                    .HasForeignKey(d => d.ContactPersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.NutsHomeCustomerId)
                    .HasName("UX_Company_NutsHomeCustomerId")
                    .IsUnique();

                entity.HasIndex(e => new { e.ChamberOfCommerceNumber, e.ValidTo, e.NutsHomeCustomerId })
                    .HasName("IX_Company_ChamberOfCommerceNumber");

                entity.HasIndex(e => new { e.CompanyName, e.ValidTo, e.NutsHomeCustomerId })
                    .HasName("IX_Company_CompanyName");

                entity.Property(e => e.ChamberOfCommerceNumber).HasMaxLength(8);

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CompanyName).HasMaxLength(100);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.Company)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithOne(p => p.Company)
                    .HasForeignKey<Company>(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ContactPerson>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.ContactPersonKeyId)
                    .HasName("UX_ContactPerson_ContactPersonKeyId")
                    .IsUnique();

                entity.HasIndex(e => e.LabelId);

                entity.HasIndex(e => new { e.Email, e.LabelId })
                    .HasName("UX_ContactPerson_Email_LabelId")
                    .IsUnique();

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(254);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.ContactPerson)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ContactPersonKey)
                    .WithOne(p => p.ContactPerson)
                    .HasForeignKey<ContactPerson>(d => d.ContactPersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.ContactPerson)
                    .HasForeignKey(d => d.LabelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ContactPersonEmailRequest>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.ContactPersonKeyId);

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.ContactPersonEmailRequest)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ContactPersonKey)
                    .WithMany(p => p.ContactPersonEmailRequest)
                    .HasForeignKey(d => d.ContactPersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ContactPersonKey>(entity =>
            {
                entity.HasIndex(e => e.ContactPersonNumber)
                    .HasName("UX_ContactPersonKey_ContactPersonNumber")
                    .IsUnique();

                entity.Property(e => e.Email).HasMaxLength(254);
            });

            modelBuilder.Entity<CorrespondenceAddress>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.CountryId);

                entity.HasIndex(e => e.PersonKeyId)
                    .HasName("UX_CorrespondenceAddress_PersonKeyId")
                    .IsUnique();

                entity.HasIndex(e => new { e.PostalCode, e.ValidTo, e.HouseNumber, e.PersonKeyId })
                    .HasName("IX_CorrespondenceAddress_PostalCode_HouseNumber");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.HouseNumberExtension).HasMaxLength(50);

                entity.Property(e => e.PostalCode)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.Property(e => e.Street).HasMaxLength(128);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.CorrespondenceAddress)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CorrespondenceAddress)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonKey)
                    .WithOne(p => p.CorrespondenceAddress)
                    .HasForeignKey<CorrespondenceAddress>(d => d.PersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CustomerContactPerson>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.ContactPersonKeyId);

                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasIndex(e => e.RoleId);

                entity.HasIndex(e => new { e.NutsHomeCustomerId, e.ContactPersonKeyId, e.RoleId })
                    .HasName("UX_CustomerContractPerson_NutsHomeCustomerId_ContractPersonKeyId_RoleId")
                    .IsUnique();

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.CustomerContactPerson)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ContactPersonKey)
                    .WithMany(p => p.CustomerContactPerson)
                    .HasForeignKey(d => d.ContactPersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.CustomerContactPerson)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.CustomerContactPerson)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CustomerInvoice>(entity =>
            {
                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.CustomerInvoice)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<DictionaryAppNotificationMessageType>(entity =>
            {
                entity.ToTable("Dictionary_AppNotificationMessageType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryChangeSourceSystem>(entity =>
            {
                entity.ToTable("Dictionary_ChangeSourceSystem");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryCountry>(entity =>
            {
                entity.ToTable("Dictionary_Country");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryCustomerContactPersonRole>(entity =>
            {
                entity.ToTable("Dictionary_CustomerContactPersonRole");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryCustomerProductType>(entity =>
            {
                entity.ToTable("Dictionary_CustomerProductType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryGender>(entity =>
            {
                entity.ToTable("Dictionary_Gender");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryPaymentMethod>(entity =>
            {
                entity.ToTable("Dictionary_PaymentMethod");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryPersonType>(entity =>
            {
                entity.ToTable("Dictionary_PersonType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryPhoneNumberType>(entity =>
            {
                entity.ToTable("Dictionary_PhoneNumberType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EmailAddress>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.PersonKeyId)
                    .HasName("UX_EmailAddress_PersonKeyId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Email, e.ValidTo, e.PersonKeyId })
                    .HasName("IX_EmailAddress_Email");

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Email).HasMaxLength(254);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.EmailAddress)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonKey)
                    .WithOne(p => p.EmailAddress)
                    .HasForeignKey<EmailAddress>(d => d.PersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EmailAddressRequest>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasIndex(e => e.PersonTypeId);

                entity.HasIndex(e => new { e.CreatedByUser, e.Created });

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.EmailAddressRequest)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.EmailAddressRequest)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonType)
                    .WithMany(p => p.EmailAddressRequest)
                    .HasForeignKey(d => d.PersonTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<NutsHomeCustomer>(entity =>
            {
                entity.HasIndex(e => e.LabelId);

                entity.HasIndex(e => e.NutsHomeCustomerNumber)
                    .HasName("UX_NutsHomeCustomer_NutsHomeCustomerNumber")
                    .IsUnique();

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.NutsHomeCustomer)
                    .HasForeignKey(d => d.LabelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PaymentDetails>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.CustomerProductTypeId);

                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasIndex(e => e.PaymentMethodId);

                entity.HasIndex(e => new { e.NutsHomeCustomerId, e.CustomerProductTypeId })
                    .HasName("UX_PaymentDetails_NutsHomeCustomerId_CustomerProductTypeId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Iban, e.ValidTo, e.NutsHomeCustomerId })
                    .HasName("IX_PaymentDetails_Iban");

                entity.Property(e => e.AccountHolder).HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Iban).HasMaxLength(34);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.PaymentDetails)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CustomerProductType)
                    .WithMany(p => p.PaymentDetails)
                    .HasForeignKey(d => d.CustomerProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.PaymentDetails)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PaymentMethod)
                    .WithMany(p => p.PaymentDetails)
                    .HasForeignKey(d => d.PaymentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PaymentDetailsRequest>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.CustomerProductTypeId);

                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasIndex(e => new { e.CreatedByUser, e.Created });

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.PaymentDetailsRequest)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CustomerProductType)
                    .WithMany(p => p.PaymentDetailsRequest)
                    .HasForeignKey(d => d.CustomerProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.PaymentDetailsRequest)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.GenderId);

                entity.HasIndex(e => e.PersonKeyId)
                    .HasName("UX_Person_PersonKeyId")
                    .IsUnique();

                entity.HasIndex(e => new { e.DateOfBirth, e.ValidTo, e.PersonKeyId })
                    .HasName("IX_Person_DateOfBirth");

                entity.HasIndex(e => new { e.Surname, e.ValidTo, e.PersonKeyId })
                    .HasName("IX_Person_Surname");

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.Initials).HasMaxLength(50);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SurnamePrefix).HasMaxLength(50);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.Person)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.Person)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonKey)
                    .WithOne(p => p.Person)
                    .HasForeignKey<Person>(d => d.PersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PersonKey>(entity =>
            {
                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasIndex(e => e.PersonTypeId);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.PersonKey)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonType)
                    .WithMany(p => p.PersonKey)
                    .HasForeignKey(d => d.PersonTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PhoneNumber>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.PersonKeyId);

                entity.HasIndex(e => e.PhoneNumberTypeId);

                entity.HasIndex(e => new { e.PersonKeyId, e.IsPreferredForCalls })
                    .HasName("UX_PhoneNumber_PersonKeyId_IsPreferredForCalls")
                    .IsUnique();

                entity.HasIndex(e => new { e.PersonKeyId, e.IsPreferredForTextMessages })
                    .HasName("UX_PhoneNumber_PersonKeyId_IsPreferredForTextMessages")
                    .IsUnique();

                entity.HasIndex(e => new { e.PhoneNr, e.ValidTo, e.PersonKeyId })
                    .HasName("IX_PhoneNumber_PhoneNr");

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PhoneNr)
                    .IsRequired()
                    .HasMaxLength(16);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.PhoneNumber)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonKey)
                    .WithMany(p => p.PhoneNumber)
                    .HasForeignKey(d => d.PersonKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PhoneNumberType)
                    .WithMany(p => p.PhoneNumber)
                    .HasForeignKey(d => d.PhoneNumberTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PhoneNumbersRequest>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.NutsHomeCustomerId);

                entity.HasIndex(e => e.PersonTypeId);

                entity.HasIndex(e => new { e.CreatedByUser, e.Created });

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.PhoneNumbersRequest)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.PhoneNumbersRequest)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PersonType)
                    .WithMany(p => p.PhoneNumbersRequest)
                    .HasForeignKey(d => d.PersonTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProductCustomer>(entity =>
            {
                entity.HasIndex(e => e.ChangeSourceSystemId);

                entity.HasIndex(e => e.CustomerProductTypeId);

                entity.HasIndex(e => new { e.NutsHomeCustomerId, e.CustomerProductTypeId });

                entity.HasIndex(e => new { e.NutsHomeCustomerId, e.ProductCustomerId, e.CustomerProductTypeId })
                    .HasName("UX_ProductCustomer_ProductCustomerId_CustomerProductTypeId")
                    .IsUnique();

                entity.Property(e => e.Comment).HasMaxLength(512);

                entity.Property(e => e.CreatedByUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.ChangeSourceSystem)
                    .WithMany(p => p.ProductCustomer)
                    .HasForeignKey(d => d.ChangeSourceSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CustomerProductType)
                    .WithMany(p => p.ProductCustomer)
                    .HasForeignKey(d => d.CustomerProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.NutsHomeCustomer)
                    .WithMany(p => p.ProductCustomer)
                    .HasForeignKey(d => d.NutsHomeCustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
