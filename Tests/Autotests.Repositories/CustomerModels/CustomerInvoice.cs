﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class CustomerInvoice
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int NutsHomeCustomerId { get; set; }
        public DateTimeOffset LastInvoiceDate { get; set; }

        public virtual NutsHomeCustomer NutsHomeCustomer { get; set; }
    }
}
