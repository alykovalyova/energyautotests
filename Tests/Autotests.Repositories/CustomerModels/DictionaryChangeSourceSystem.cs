﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class DictionaryChangeSourceSystem
    {
        public DictionaryChangeSourceSystem()
        {
            AppNotificationDevice = new HashSet<AppNotificationDevice>();
            Company = new HashSet<Company>();
            ContactPerson = new HashSet<ContactPerson>();
            ContactPersonEmailRequest = new HashSet<ContactPersonEmailRequest>();
            CorrespondenceAddress = new HashSet<CorrespondenceAddress>();
            CustomerContactPerson = new HashSet<CustomerContactPerson>();
            EmailAddress = new HashSet<EmailAddress>();
            EmailAddressRequest = new HashSet<EmailAddressRequest>();
            PaymentDetails = new HashSet<PaymentDetails>();
            PaymentDetailsRequest = new HashSet<PaymentDetailsRequest>();
            Person = new HashSet<Person>();
            PhoneNumber = new HashSet<PhoneNumber>();
            PhoneNumbersRequest = new HashSet<PhoneNumbersRequest>();
            ProductCustomer = new HashSet<ProductCustomer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<AppNotificationDevice> AppNotificationDevice { get; set; }
        public virtual ICollection<Company> Company { get; set; }
        public virtual ICollection<ContactPerson> ContactPerson { get; set; }
        public virtual ICollection<ContactPersonEmailRequest> ContactPersonEmailRequest { get; set; }
        public virtual ICollection<CorrespondenceAddress> CorrespondenceAddress { get; set; }
        public virtual ICollection<CustomerContactPerson> CustomerContactPerson { get; set; }
        public virtual ICollection<EmailAddress> EmailAddress { get; set; }
        public virtual ICollection<EmailAddressRequest> EmailAddressRequest { get; set; }
        public virtual ICollection<PaymentDetails> PaymentDetails { get; set; }
        public virtual ICollection<PaymentDetailsRequest> PaymentDetailsRequest { get; set; }
        public virtual ICollection<Person> Person { get; set; }
        public virtual ICollection<PhoneNumber> PhoneNumber { get; set; }
        public virtual ICollection<PhoneNumbersRequest> PhoneNumbersRequest { get; set; }
        public virtual ICollection<ProductCustomer> ProductCustomer { get; set; }
    }
}
