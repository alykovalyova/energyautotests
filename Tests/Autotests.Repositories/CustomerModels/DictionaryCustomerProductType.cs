﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class DictionaryCustomerProductType
    {
        public DictionaryCustomerProductType()
        {
            PaymentDetails = new HashSet<PaymentDetails>();
            PaymentDetailsRequest = new HashSet<PaymentDetailsRequest>();
            ProductCustomer = new HashSet<ProductCustomer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PaymentDetails> PaymentDetails { get; set; }
        public virtual ICollection<PaymentDetailsRequest> PaymentDetailsRequest { get; set; }
        public virtual ICollection<ProductCustomer> ProductCustomer { get; set; }
    }
}
