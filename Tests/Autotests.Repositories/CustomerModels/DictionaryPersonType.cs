﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class DictionaryPersonType
    {
        public DictionaryPersonType()
        {
            EmailAddressRequest = new HashSet<EmailAddressRequest>();
            PersonKey = new HashSet<PersonKey>();
            PhoneNumbersRequest = new HashSet<PhoneNumbersRequest>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<EmailAddressRequest> EmailAddressRequest { get; set; }
        public virtual ICollection<PersonKey> PersonKey { get; set; }
        public virtual ICollection<PhoneNumbersRequest> PhoneNumbersRequest { get; set; }
    }
}
