﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class NutsHomeCustomer
    {
        public NutsHomeCustomer()
        {
            CustomerContactPerson = new HashSet<CustomerContactPerson>();
            CustomerInvoice = new HashSet<CustomerInvoice>();
            EmailAddressRequest = new HashSet<EmailAddressRequest>();
            PaymentDetails = new HashSet<PaymentDetails>();
            PaymentDetailsRequest = new HashSet<PaymentDetailsRequest>();
            PersonKey = new HashSet<PersonKey>();
            PhoneNumbersRequest = new HashSet<PhoneNumbersRequest>();
            ProductCustomer = new HashSet<ProductCustomer>();
        }

        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int LabelId { get; set; }
        public int NutsHomeCustomerNumber { get; set; }
        public DateTimeOffset? AnonymizedOn { get; set; }

        public virtual DictionaryLabel Label { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<CustomerContactPerson> CustomerContactPerson { get; set; }
        public virtual ICollection<CustomerInvoice> CustomerInvoice { get; set; }
        public virtual ICollection<EmailAddressRequest> EmailAddressRequest { get; set; }
        public virtual ICollection<PaymentDetails> PaymentDetails { get; set; }
        public virtual ICollection<PaymentDetailsRequest> PaymentDetailsRequest { get; set; }
        public virtual ICollection<PersonKey> PersonKey { get; set; }
        public virtual ICollection<PhoneNumbersRequest> PhoneNumbersRequest { get; set; }
        public virtual ICollection<ProductCustomer> ProductCustomer { get; set; }
    }
}
