﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class Person
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int PersonKeyId { get; set; }
        public int GenderId { get; set; }
        public string Initials { get; set; }
        public string SurnamePrefix { get; set; }
        public string Surname { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int ChangeSourceSystemId { get; set; }
        public string CreatedByUser { get; set; }
        public string Comment { get; set; }
        public DateTimeOffset? ValidTo { get; set; }

        public virtual DictionaryChangeSourceSystem ChangeSourceSystem { get; set; }
        public virtual DictionaryGender Gender { get; set; }
        public virtual PersonKey PersonKey { get; set; }
    }
}
