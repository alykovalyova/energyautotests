﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class PersonKey
    {
        public PersonKey()
        {
            PhoneNumber = new HashSet<PhoneNumber>();
        }

        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int NutsHomeCustomerId { get; set; }
        public int PersonTypeId { get; set; }

        public virtual NutsHomeCustomer NutsHomeCustomer { get; set; }
        public virtual DictionaryPersonType PersonType { get; set; }
        public virtual CorrespondenceAddress CorrespondenceAddress { get; set; }
        public virtual EmailAddress EmailAddress { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<PhoneNumber> PhoneNumber { get; set; }
    }
}
