﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.CustomerModels
{
    public partial class PhoneNumber
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public int PersonKeyId { get; set; }
        public string PhoneNr { get; set; }
        public int PhoneNumberTypeId { get; set; }
        public bool IsPreferredForCalls { get; set; }
        public bool IsPreferredForTextMessages { get; set; }
        public int ChangeSourceSystemId { get; set; }
        public string CreatedByUser { get; set; }
        public string Comment { get; set; }
        public DateTimeOffset? ValidTo { get; set; }

        public virtual DictionaryChangeSourceSystem ChangeSourceSystem { get; set; }
        public virtual PersonKey PersonKey { get; set; }
        public virtual DictionaryPhoneNumberType PhoneNumberType { get; set; }
    }
}
