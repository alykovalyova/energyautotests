﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Adressen
    {
        public Adressen()
        {
            AdressenLogs = new HashSet<AdressenLogs>();
            Contracten = new HashSet<Contracten>();
            EanBoek = new HashSet<EanBoek>();
            Netbeheerders = new HashSet<Netbeheerders>();
        }

        public int AdresId { get; set; }
        public string Straatnaam { get; set; }
        public int Huisnummer { get; set; }
        public string Toevoeging { get; set; }
        public string Postcode { get; set; }
        public string Woonplaats { get; set; }
        public DateTime CreatieDatum { get; set; }
        public DateTime? LastMeterReadingAcquisitionDate { get; set; }
        public DateTime? AfrekenMomentDatum { get; set; }

        public virtual ICollection<AdressenLogs> AdressenLogs { get; set; }
        public virtual ICollection<Contracten> Contracten { get; set; }
        public virtual ICollection<EanBoek> EanBoek { get; set; }
        public virtual ICollection<Netbeheerders> Netbeheerders { get; set; }
    }
}
