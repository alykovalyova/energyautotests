﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class AdressenLogs
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public string User { get; set; }
        public string ChangeType { get; set; }
        public int AdresId { get; set; }
        public string StraatnaamOld { get; set; }
        public int HuisnummerOld { get; set; }
        public string ToevoegingOld { get; set; }
        public string PostcodeOld { get; set; }
        public string WoonplaatsOld { get; set; }
        public string StraatnaamNew { get; set; }
        public int HuisnummerNew { get; set; }
        public string ToevoegingNew { get; set; }
        public string PostcodeNew { get; set; }
        public string WoonplaatsNew { get; set; }

        public virtual Adressen Adres { get; set; }
    }
}
