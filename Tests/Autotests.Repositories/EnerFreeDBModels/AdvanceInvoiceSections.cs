﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class AdvanceInvoiceSections
    {
        public AdvanceInvoiceSections()
        {
            AdvanceInvoiceLines = new HashSet<AdvanceInvoiceLines>();
            Reconciliations = new HashSet<Reconciliations>();
        }

        public int Id { get; set; }
        public int AdvanceInvoiceId { get; set; }
        public int? ContractRegelId { get; set; }
        public decimal AmountExcl { get; set; }
        public decimal AmountVat { get; set; }
        public decimal AmountIncl { get; set; }

        public virtual AdvanceInvoices AdvanceInvoice { get; set; }
        public virtual ICollection<AdvanceInvoiceLines> AdvanceInvoiceLines { get; set; }
        public virtual ICollection<Reconciliations> Reconciliations { get; set; }
    }
}
