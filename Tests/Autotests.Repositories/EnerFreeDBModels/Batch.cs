﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Batch
    {
        public int BatchId { get; set; }
        public DateTime? StartDate { get; set; }
        public byte InvoiceTypeId { get; set; }
        public int? RunByUserId { get; set; }
        public byte StatusId { get; set; }

        public virtual InvoiceType InvoiceType { get; set; }
        public virtual Users RunByUser { get; set; }
    }
}
