﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaBatch
    {
        public BbaBatch()
        {
            BbaBatchContract = new HashSet<BbaBatchContract>();
            BbaBatchContractRegel = new HashSet<BbaBatchContractRegel>();
        }

        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public DateTime Created { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? RunDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int Status { get; set; }
        public int BbaInvoiceRunId { get; set; }
        public int? ApprovedByUserId { get; set; }
        public int? RunByUserId { get; set; }

        public virtual Users ApprovedByUser { get; set; }
        public virtual BbaInvoiceRunConfig BbaInvoiceRun { get; set; }
        public virtual Users RunByUser { get; set; }
        public virtual ICollection<BbaBatchContract> BbaBatchContract { get; set; }
        public virtual ICollection<BbaBatchContractRegel> BbaBatchContractRegel { get; set; }
    }
}
