﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaBatchContract
    {
        public int BbaBatchId { get; set; }
        public int ContractId { get; set; }
        public bool IsBusiness { get; set; }
        public bool Specificatie { get; set; }
        public string VerzendWijze { get; set; }
        public decimal BedragExcl { get; set; }
        public decimal BedragBtw { get; set; }
        public decimal BedragIncl { get; set; }
        public decimal AdminExcl { get; set; }
        public decimal AdminBtw { get; set; }
        public decimal AdminIncl { get; set; }
        public int? Sequence { get; set; }
        public DateTime? DirectdebitDate { get; set; }
        public bool? Success { get; set; }
        public int? FactuurId { get; set; }
        public string ErrorMessage { get; set; }
        public byte PaymentMethodId { get; set; }
        public byte LabelCode { get; set; }

        public virtual BbaBatch BbaBatch { get; set; }
    }
}
