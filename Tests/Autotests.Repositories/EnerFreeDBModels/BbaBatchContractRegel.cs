﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaBatchContractRegel
    {
        public int BbaBatchId { get; set; }
        public int ContractRegelId { get; set; }
        public int SegmentId { get; set; }
        public byte LabelCode { get; set; }

        public virtual BbaBatch BbaBatch { get; set; }
    }
}
