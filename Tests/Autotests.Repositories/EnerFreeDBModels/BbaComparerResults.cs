﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaComparerResults
    {
        public BbaComparerResults()
        {
            BbaUpdateMail = new HashSet<BbaUpdateMail>();
        }

        public byte LabelCode { get; set; }
        public int ContractId { get; set; }
        public decimal CurrentBba { get; set; }
        public decimal EstimatedBba { get; set; }
        public decimal AdvisoryAmount { get; set; }
        public decimal NeutralAmount { get; set; }
        public DateTime BbaStartDate { get; set; }
        public int TermsPaid { get; set; }
        public DateTime RunDate { get; set; }
        public decimal? Difference { get; set; }
        public decimal? Percentage { get; set; }
        public int? InvoiceRunId { get; set; }
        public bool? IsMeterExchangeRecent { get; set; }
        public bool IsSmartMeter { get; set; }
        public bool IsSingleFuelGas { get; set; }
        public int YearlyGasUsage { get; set; }
        public decimal? FractionsGas { get; set; }
        public decimal? FractionsProduction { get; set; }
        public decimal? PaymentRisk { get; set; }

        public virtual ICollection<BbaUpdateMail> BbaUpdateMail { get; set; }
    }
}
