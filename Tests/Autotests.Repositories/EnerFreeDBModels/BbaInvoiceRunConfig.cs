﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaInvoiceRunConfig
    {
        public BbaInvoiceRunConfig()
        {
            BbaBatch = new HashSet<BbaBatch>();
            BbaInvoiceRunContract = new HashSet<BbaInvoiceRunContract>();
        }

        public int Id { get; set; }
        public byte DayOfMonth { get; set; }

        public virtual ICollection<BbaBatch> BbaBatch { get; set; }
        public virtual ICollection<BbaInvoiceRunContract> BbaInvoiceRunContract { get; set; }
    }
}
