﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaInvoiceRunContractLog
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int BbaInvoiceRunIdOld { get; set; }
        public int BbaInvoiceRunIdNew { get; set; }
        public DateTime Modified { get; set; }
        public int ModifiedByUserId { get; set; }

        public virtual Users ModifiedByUser { get; set; }
    }
}
