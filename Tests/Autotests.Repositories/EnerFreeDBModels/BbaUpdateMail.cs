﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaUpdateMail
    {
        public int ContractId { get; set; }
        public int BbaUpdateMailHeadId { get; set; }
        public DateTime? MailNotificationDate { get; set; }
        public decimal CurrentBba { get; set; }
        public decimal NewBbaAmount { get; set; }
        public DateTime? IsOptedOutDate { get; set; }
        public DateTime? BbaUpdateDate { get; set; }
        public DateTime? InvoiceReceivedDate { get; set; }
        public string SourceReferenceId { get; set; }

        public virtual BbaUpdateMailHead BbaUpdateMailHead { get; set; }
        public virtual BbaComparerResults Contract { get; set; }
        public virtual Contracten ContractNavigation { get; set; }
    }
}
