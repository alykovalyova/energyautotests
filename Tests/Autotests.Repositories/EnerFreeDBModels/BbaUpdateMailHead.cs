﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaUpdateMailHead
    {
        public BbaUpdateMailHead()
        {
            BbaUpdateMail = new HashSet<BbaUpdateMail>();
        }

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual BbaUpdateMailSearchFilter BbaUpdateMailSearchFilter { get; set; }
        public virtual ICollection<BbaUpdateMail> BbaUpdateMail { get; set; }
    }
}
