﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BbaUpdateMailSearchFilter
    {
        public int BbaUpdateMailHeadId { get; set; }
        public int? MinPercentage { get; set; }
        public int? MaxPercentage { get; set; }
        public int? MinDifference { get; set; }
        public int? MaxDifference { get; set; }
        public int? MinAdvisoryAmount { get; set; }
        public int? MaxAdvisoryAmount { get; set; }
        public int? MinInvoiceRunId { get; set; }
        public int? MaxInvoiceRunId { get; set; }
        public int? MinYearlyGas { get; set; }
        public int? MaxYearlyGas { get; set; }
        public int? MinTermsInvoiced { get; set; }
        public int? MaxTermsInvoiced { get; set; }
        public bool? SmartMeter { get; set; }
        public bool? ExchangeOfMeter { get; set; }
        public bool? GasOnly { get; set; }
        public decimal? MinFractionsGas { get; set; }
        public decimal? MaxFractionsGas { get; set; }
        public decimal? MinFractionsProduction { get; set; }
        public decimal? MaxFractionsProduction { get; set; }
        public int? MinPaymentRisk { get; set; }
        public int? MaxPaymentRisk { get; set; }

        public virtual BbaUpdateMailHead BbaUpdateMailHead { get; set; }
    }
}
