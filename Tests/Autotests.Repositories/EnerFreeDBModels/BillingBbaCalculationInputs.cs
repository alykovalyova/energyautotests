﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BillingBbaCalculationInputs
    {
        public int ContractId { get; set; }
        public int ContractRegelId { get; set; }
        public string ProfielId { get; set; }
        public bool? IsHorticulture { get; set; }
        public string EanGebruiker { get; set; }
        public int? AddressId { get; set; }
        public int? SegmentId { get; set; }
        public bool? HasMasterdata { get; set; }
        public bool? IsOperational { get; set; }
        public int? Eacpeak { get; set; }
        public int? EacoffPeak { get; set; }
        public int? Eappeak { get; set; }
        public int? EapoffPeak { get; set; }
        public bool? Voorschot { get; set; }
        public string EnergyFlowDirection { get; set; }
        public DateTime? StartDate { get; set; }
        public string PropositionId { get; set; }
        public int GasRegion { get; set; }
        public string CapTarCode { get; set; }
        public bool? IsResidential { get; set; }
        public int? LeadSourceId { get; set; }
        public string Categorie { get; set; }
        public bool ForceSingleTariffForElectricity { get; set; }
        public string GridOperator { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
    }
}
