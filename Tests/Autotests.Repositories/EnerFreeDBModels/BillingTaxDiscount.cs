﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BillingTaxDiscount
    {
        public byte TaxTariffKeyId { get; set; }
        public bool Residential { get; set; }
        public decimal Amount { get; set; }

        public virtual BillingTaxTariffKey TaxTariffKey { get; set; }
    }
}
