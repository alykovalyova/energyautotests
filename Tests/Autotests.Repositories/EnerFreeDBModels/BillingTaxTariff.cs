﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BillingTaxTariff
    {
        public byte TaxTariffKeyId { get; set; }
        public byte SegmentId { get; set; }
        public bool IsHorticulture { get; set; }
        public decimal Eb1 { get; set; }
        public decimal Eb2 { get; set; }
        public decimal Eb3 { get; set; }
        public decimal Eb4 { get; set; }
        public decimal Eb5 { get; set; }
        public decimal Ode1 { get; set; }
        public decimal Ode2 { get; set; }
        public decimal Ode3 { get; set; }
        public decimal Ode4 { get; set; }
        public decimal Ode5 { get; set; }

        public virtual BillingTaxTariffKey TaxTariffKey { get; set; }
    }
}
