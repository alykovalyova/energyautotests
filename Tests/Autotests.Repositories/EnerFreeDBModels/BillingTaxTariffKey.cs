﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class BillingTaxTariffKey
    {
        public BillingTaxTariffKey()
        {
            AdvanceInvoiceLines = new HashSet<AdvanceInvoiceLines>();
            BillingTaxDiscount = new HashSet<BillingTaxDiscount>();
            BillingTaxTariff = new HashSet<BillingTaxTariff>();
            ContractRegelSpecificaties = new HashSet<ContractRegelSpecificaties>();
            InvoiceLines = new HashSet<InvoiceLines>();
        }

        public byte Id { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public virtual ICollection<AdvanceInvoiceLines> AdvanceInvoiceLines { get; set; }
        public virtual ICollection<BillingTaxDiscount> BillingTaxDiscount { get; set; }
        public virtual ICollection<BillingTaxTariff> BillingTaxTariff { get; set; }
        public virtual ICollection<ContractRegelSpecificaties> ContractRegelSpecificaties { get; set; }
        public virtual ICollection<InvoiceLines> InvoiceLines { get; set; }
    }
}
