﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Btw
    {
        public int Btwcode { get; set; }
        public string Omschrijving { get; set; }
        public double? Percentage { get; set; }
        public string Categorie { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] TimeStamp { get; set; }
        public DateTime BeginDatum { get; set; }
        public DateTime EindDatum { get; set; }
        public byte Type { get; set; }
    }
}
