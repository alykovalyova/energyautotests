﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class CalGosRegels2
    {
        public string EanGos { get; set; }
        public short Jaar { get; set; }
        public short Maand { get; set; }
        public decimal CalorFactor { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }
    }
}
