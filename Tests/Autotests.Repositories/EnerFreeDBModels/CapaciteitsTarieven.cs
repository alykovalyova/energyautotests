﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class CapaciteitsTarieven
    {
        public string EanCapaciteit { get; set; }
        public DateTime Start { get; set; }
        public decimal? Vastrecht { get; set; }
        public decimal? AansluitDienst { get; set; }
        public decimal? TransportDienst { get; set; }
        public decimal? SysteemDienst { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }
        public decimal? MeetDienst { get; set; }
        public int CapaciteitsTarievenId { get; set; }
    }
}
