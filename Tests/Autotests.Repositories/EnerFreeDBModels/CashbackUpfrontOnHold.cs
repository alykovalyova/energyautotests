﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class CashbackUpfrontOnHold
    {
        public int ContractId { get; set; }
        public DateTime Inserted { get; set; }

        public virtual Contracten Contract { get; set; }
    }
}
