﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class CerHistory
    {
        public int Id { get; set; }
        public string EanId { get; set; }
        public string RequestXml { get; set; }
        public string ResponseXml { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? EndDateContract { get; set; }
        public int? NoticePeriod { get; set; }
        public string BalanceSupplierCompany { get; set; }
    }
}
