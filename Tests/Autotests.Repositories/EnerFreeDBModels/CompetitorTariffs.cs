﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class CompetitorTariffs
    {
        public int Id { get; set; }
        public string Competitor { get; set; }
        public string Description { get; set; }
        public decimal Normal { get; set; }
        public decimal Low { get; set; }
        public decimal Single { get; set; }
        public decimal StandingChargeMonthlyElectricity { get; set; }
        public decimal Gas { get; set; }
        public decimal StandingChargeMonthlyGas { get; set; }
        public int Order { get; set; }
        public DateTime CreationDate { get; set; }
        public string InsertedBy { get; set; }
    }
}
