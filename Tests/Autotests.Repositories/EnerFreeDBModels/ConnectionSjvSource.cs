﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ConnectionSjvSource
    {
        public int Id { get; set; }
        public int ContractRegelId { get; set; }
        public int Jvh { get; set; }
        public int Jvl { get; set; }
        public int Source { get; set; }
        public DateTime CreationDate { get; set; }
        public int? YearlyProductionNormal { get; set; }
        public int? YearlyProductionLow { get; set; }

        public virtual ContractRegels ContractRegel { get; set; }
    }
}
