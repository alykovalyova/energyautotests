﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractAgreements
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int PenaltyCategoryId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string DeletionBy { get; set; }
        public string DeletionReason { get; set; }
        public bool ForceSingleTariffForElectricity { get; set; }
        public int? IncentivePaymentTypeId { get; set; }
        public decimal? IncentivePaymentAmount { get; set; }
        public decimal CustomerPaysPaymentCostsExclVat { get; set; }
        public DateTime? RenewableFromDate { get; set; }

        public virtual Contracten Contract { get; set; }
        public virtual PenaltyCategory PenaltyCategory { get; set; }
    }
}
