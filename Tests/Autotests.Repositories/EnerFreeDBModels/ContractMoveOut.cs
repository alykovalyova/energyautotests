﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractMoveOut
    {
        public int Id { get; set; }
        public string Eanid { get; set; }
        public DateTime MutationDate { get; set; }
        public string DossierId { get; set; }
        public string OldBalanceSupplier { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
