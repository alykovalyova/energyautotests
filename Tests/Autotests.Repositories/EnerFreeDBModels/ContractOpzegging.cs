﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractOpzegging
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public string RedenOpzegging { get; set; }
        public DateTime DatumUithuizing { get; set; }
        public DateTime? OpzeggingGeannuleerd { get; set; }
        public DateTime Created { get; set; }

        public virtual Contracten Contract { get; set; }
    }
}
