﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractRegelAgreements
    {
        public int ContractRegelAgreementsId { get; set; }
        public int ContractRegelId { get; set; }
        public int GasRegion { get; set; }
        public string ProdmanPropositionId { get; set; }
        public int Profile { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string DeletionBy { get; set; }
        public string DeletionReason { get; set; }

        public virtual ContractRegels ContractRegel { get; set; }
        public virtual Profielen ProfileNavigation { get; set; }
    }
}
