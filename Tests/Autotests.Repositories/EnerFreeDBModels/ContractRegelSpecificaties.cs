﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractRegelSpecificaties
    {
        public int CrspecId { get; set; }
        public int ContractRegelId { get; set; }
        public double? Hoeveelheid { get; set; }
        public double? Prijs { get; set; }
        public short Btwcode { get; set; }
        public decimal BedragExcl { get; set; }
        public decimal BedragBtw { get; set; }
        public decimal BedragIncl { get; set; }
        public DateTime? CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }
        public short? KostensoortId { get; set; }
        public byte? TaxTariffKeyId { get; set; }

        public virtual ContractRegels ContractRegel { get; set; }
        public virtual KostenSoort Kostensoort { get; set; }
        public virtual BillingTaxTariffKey TaxTariffKey { get; set; }
    }
}
