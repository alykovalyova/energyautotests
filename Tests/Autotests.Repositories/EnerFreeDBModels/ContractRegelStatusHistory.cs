﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractRegelStatusHistory
    {
        public long Id { get; set; }
        public int ContractRegelId { get; set; }
        public int StatusId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? ExpectedStartSupply { get; set; }
        public DateTime? StartSupply { get; set; }
        public DateTime? EndSupply { get; set; }
        public DateTime? CooldownEndDate { get; set; }
        public bool IsMoveIn { get; set; }

        public virtual ContractRegels ContractRegel { get; set; }
        public virtual DictionaryContractRegelsDetailStatus Status { get; set; }
    }
}
