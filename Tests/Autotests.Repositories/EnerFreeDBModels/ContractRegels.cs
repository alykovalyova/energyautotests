﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractRegels
    {
        public ContractRegels()
        {
            ConnectionSjvSource = new HashSet<ConnectionSjvSource>();
            ContractRegelAgreements = new HashSet<ContractRegelAgreements>();
            ContractRegelSpecificaties = new HashSet<ContractRegelSpecificaties>();
            LogContractMutations = new HashSet<LogContractMutations>();
            SwitchRegelsLogs = new HashSet<SwitchRegelsLogs>();
            Switches = new HashSet<Switches>();
        }

        public int ContractRegelId { get; set; }
        public int ContractId { get; set; }
        public int EanId { get; set; }
        public DateTime? Ingangsdatum { get; set; }
        public bool Actief { get; set; }
        public DateTime? ContractStart { get; set; }
        public DateTime? ContractEind { get; set; }
        public bool Geannuleerd { get; set; }
        public DateTime? GeannuleerdDatum { get; set; }
        public DateTime? CreatieDatum { get; set; }

        public virtual Contracten Contract { get; set; }
        public virtual EanBoek Ean { get; set; }
        public virtual ContractRegelStatusHistory ContractRegelStatusHistory { get; set; }
        public virtual ContractRegelsHorticulture ContractRegelsHorticulture { get; set; }
        public virtual MonthlyAmountHistory MonthlyAmountHistory { get; set; }
        public virtual ICollection<ConnectionSjvSource> ConnectionSjvSource { get; set; }
        public virtual ICollection<ContractRegelAgreements> ContractRegelAgreements { get; set; }
        public virtual ICollection<ContractRegelSpecificaties> ContractRegelSpecificaties { get; set; }
        public virtual ICollection<LogContractMutations> LogContractMutations { get; set; }
        public virtual ICollection<SwitchRegelsLogs> SwitchRegelsLogs { get; set; }
        public virtual ICollection<Switches> Switches { get; set; }
    }
}
