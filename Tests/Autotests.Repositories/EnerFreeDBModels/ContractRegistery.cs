﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ContractRegistery
    {
        public int Id { get; set; }
        public string Eanid { get; set; }
        public DateTime CancellationDate { get; set; }
        public string BalanceSupplierId { get; set; }
        public string DossierId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? AddressId { get; set; }
        public int? ContractId { get; set; }
        public int? CustomerId { get; set; }
        public bool? Prnotified { get; set; }
    }
}
