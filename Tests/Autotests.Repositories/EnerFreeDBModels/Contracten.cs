﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Contracten
    {
        public Contracten()
        {
            AdvanceInvoices = new HashSet<AdvanceInvoices>();
            BbaUpdateMail = new HashSet<BbaUpdateMail>();
            ContractDocumenten = new HashSet<ContractDocumenten>();
            ContractOpzegging = new HashSet<ContractOpzegging>();
            ContractRegels = new HashSet<ContractRegels>();
            ExceptionQueue = new HashSet<ExceptionQueue>();
            InverseMoveFrom = new HashSet<Contracten>();
            InverseRenewalOf = new HashSet<Contracten>();
            InvoiceQueue = new HashSet<InvoiceQueue>();
            LogContractMutations = new HashSet<LogContractMutations>();
            SendRenewableContract = new HashSet<SendRenewableContract>();
            Smslog = new HashSet<Smslog>();
        }

        public int ContractId { get; set; }
        public int? ContractantId { get; set; }
        public int? CadresId { get; set; }
        public bool Specificatie { get; set; }
        public DateTime? IngevoerdDatum { get; set; }
        public bool Geannuleerd { get; set; }
        public DateTime? GeannuleerdDatum { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }
        public DateTime? AfrekenMomentDatum { get; set; }
        public DateTime? LastMeterReadingAcquisitionDate { get; set; }
        public int? RenewalOfId { get; set; }
        public int? MoveFromId { get; set; }
        public int? LeadSourceId { get; set; }
        public DateTime? SendMonthlyUsageEmailFrom { get; set; }
        public byte LabelCode { get; set; }
        public DateTime SignUpDate { get; set; }

        public virtual Adressen Cadres { get; set; }
        public virtual Relaties Contractant { get; set; }
        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual Contracten MoveFrom { get; set; }
        public virtual Contracten RenewalOf { get; set; }
        public virtual BbaInvoiceRunContract BbaInvoiceRunContract { get; set; }
        public virtual CashbackUpfrontOnHold CashbackUpfrontOnHold { get; set; }
        public virtual ContractAgreements ContractAgreements { get; set; }
        public virtual ContractVoucher ContractVoucher { get; set; }
        public virtual ExcludeFromBbaCompare2019 ExcludeFromBbaCompare2019 { get; set; }
        public virtual ICollection<AdvanceInvoices> AdvanceInvoices { get; set; }
        public virtual ICollection<BbaUpdateMail> BbaUpdateMail { get; set; }
        public virtual ICollection<ContractDocumenten> ContractDocumenten { get; set; }
        public virtual ICollection<ContractOpzegging> ContractOpzegging { get; set; }
        public virtual ICollection<ContractRegels> ContractRegels { get; set; }
        public virtual ICollection<ExceptionQueue> ExceptionQueue { get; set; }
        public virtual ICollection<Contracten> InverseMoveFrom { get; set; }
        public virtual ICollection<Contracten> InverseRenewalOf { get; set; }
        public virtual ICollection<InvoiceQueue> InvoiceQueue { get; set; }
        public virtual ICollection<LogContractMutations> LogContractMutations { get; set; }
        public virtual ICollection<SendRenewableContract> SendRenewableContract { get; set; }
        public virtual ICollection<Smslog> Smslog { get; set; }
    }
}
