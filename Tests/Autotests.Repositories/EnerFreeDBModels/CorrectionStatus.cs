﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class CorrectionStatus
    {
        public CorrectionStatus()
        {
            AdvanceInvoices = new HashSet<AdvanceInvoices>();
            Invoices = new HashSet<Invoices>();
        }

        public byte Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<AdvanceInvoices> AdvanceInvoices { get; set; }
        public virtual ICollection<Invoices> Invoices { get; set; }
    }
}
