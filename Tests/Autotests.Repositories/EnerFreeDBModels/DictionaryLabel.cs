﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class DictionaryLabel
    {
        public DictionaryLabel()
        {
            Contracten = new HashSet<Contracten>();
            InvoiceTemplate = new HashSet<InvoiceTemplate>();
            Relaties = new HashSet<Relaties>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Contracten> Contracten { get; set; }
        public virtual ICollection<InvoiceTemplate> InvoiceTemplate { get; set; }
        public virtual ICollection<Relaties> Relaties { get; set; }
    }
}
