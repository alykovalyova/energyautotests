﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class EanBoek
    {
        public EanBoek()
        {
            ContractRegels = new HashSet<ContractRegels>();
            Meters = new HashSet<Meters>();
            StamdataRequest = new HashSet<StamdataRequest>();
        }

        public int EanId { get; set; }
        public string EanGebruiker { get; set; }
        public string EanNb { get; set; }
        public int? SegmentId { get; set; }
        public int VadresId { get; set; }
        public DateTime CreatieDatum { get; set; }

        public virtual Netbeheerders EanNbNavigation { get; set; }
        public virtual Segmenten Segment { get; set; }
        public virtual Adressen Vadres { get; set; }
        public virtual ICollection<ContractRegels> ContractRegels { get; set; }
        public virtual ICollection<Meters> Meters { get; set; }
        public virtual ICollection<StamdataRequest> StamdataRequest { get; set; }
    }
}
