﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class EdsnFraction
    {
        public DateTime? FractionDate { get; set; }
        public decimal? FractionValue { get; set; }
        public string Profile { get; set; }
        public int Id { get; set; }
        public DateTime? FractionInsertedDate { get; set; }
        public decimal? NormValue { get; set; }
        public DateTime? NormInsertedDate { get; set; }
    }
}
