﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class EnerFreeDBContext : DbContext
    {
        private readonly string _connectionString;

        public EnerFreeDBContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public EnerFreeDBContext(DbContextOptions<EnerFreeDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Adressen> Adressen { get; set; }
        public virtual DbSet<AdressenLogs> AdressenLogs { get; set; }
        public virtual DbSet<AdvanceInvoiceLines> AdvanceInvoiceLines { get; set; }
        public virtual DbSet<AdvanceInvoiceSections> AdvanceInvoiceSections { get; set; }
        public virtual DbSet<AdvanceInvoices> AdvanceInvoices { get; set; }
        public virtual DbSet<Batch> Batch { get; set; }
        public virtual DbSet<BbaBatch> BbaBatch { get; set; }
        public virtual DbSet<BbaBatchContract> BbaBatchContract { get; set; }
        public virtual DbSet<BbaBatchContractRegel> BbaBatchContractRegel { get; set; }
        public virtual DbSet<BbaComparerResults> BbaComparerResults { get; set; }
        public virtual DbSet<BbaInvoiceRunConfig> BbaInvoiceRunConfig { get; set; }
        public virtual DbSet<BbaInvoiceRunContract> BbaInvoiceRunContract { get; set; }
        public virtual DbSet<BbaInvoiceRunContractLog> BbaInvoiceRunContractLog { get; set; }
        public virtual DbSet<BbaUpdateMail> BbaUpdateMail { get; set; }
        public virtual DbSet<BbaUpdateMailHead> BbaUpdateMailHead { get; set; }
        public virtual DbSet<BbaUpdateMailSearchFilter> BbaUpdateMailSearchFilter { get; set; }
        public virtual DbSet<BillingBbaCalculationInputs> BillingBbaCalculationInputs { get; set; }
        public virtual DbSet<BillingDocumentInfo> BillingDocumentInfo { get; set; }
        public virtual DbSet<BillingTaxDiscount> BillingTaxDiscount { get; set; }
        public virtual DbSet<BillingTaxTariff> BillingTaxTariff { get; set; }
        public virtual DbSet<BillingTaxTariffKey> BillingTaxTariffKey { get; set; }
        public virtual DbSet<Bronnen> Bronnen { get; set; }
        public virtual DbSet<Btw> Btw { get; set; }
        public virtual DbSet<CalGosRegels2> CalGosRegels2 { get; set; }
        public virtual DbSet<CapaciteitenUitvalteam> CapaciteitenUitvalteam { get; set; }
        public virtual DbSet<CapaciteitsTarieven> CapaciteitsTarieven { get; set; }
        public virtual DbSet<CashbackUpfrontOnHold> CashbackUpfrontOnHold { get; set; }
        public virtual DbSet<CerHistory> CerHistory { get; set; }
        public virtual DbSet<CompetitorTariffs> CompetitorTariffs { get; set; }
        public virtual DbSet<ConnectionSjvSource> ConnectionSjvSource { get; set; }
        public virtual DbSet<ContractAgreements> ContractAgreements { get; set; }
        public virtual DbSet<ContractDocumenten> ContractDocumenten { get; set; }
        public virtual DbSet<ContractMoveOut> ContractMoveOut { get; set; }
        public virtual DbSet<ContractOpzegging> ContractOpzegging { get; set; }
        public virtual DbSet<ContractRegelAgreements> ContractRegelAgreements { get; set; }
        public virtual DbSet<ContractRegelSpecificaties> ContractRegelSpecificaties { get; set; }
        public virtual DbSet<ContractRegelStatusHistory> ContractRegelStatusHistory { get; set; }
        public virtual DbSet<ContractRegels> ContractRegels { get; set; }
        public virtual DbSet<ContractRegelsHorticulture> ContractRegelsHorticulture { get; set; }
        public virtual DbSet<ContractRegistery> ContractRegistery { get; set; }
        public virtual DbSet<ContractSignupStatus> ContractSignupStatus { get; set; }
        public virtual DbSet<ContractVoucher> ContractVoucher { get; set; }
        public virtual DbSet<Contracten> Contracten { get; set; }
        public virtual DbSet<CorrectionStatus> CorrectionStatus { get; set; }
        public virtual DbSet<CoulanceType> CoulanceType { get; set; }
        public virtual DbSet<DefaultCaptar> DefaultCaptar { get; set; }
        public virtual DbSet<DictionaryContractRegelsDetailStatus> DictionaryContractRegelsDetailStatus { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabel { get; set; }
        public virtual DbSet<EanBoek> EanBoek { get; set; }
        public virtual DbSet<EditStatus> EditStatus { get; set; }
        public virtual DbSet<EdsnFraction> EdsnFraction { get; set; }
        public virtual DbSet<EdsnFractionProduction> EdsnFractionProduction { get; set; }
        public virtual DbSet<Eosdata> Eosdata { get; set; }
        public virtual DbSet<ExceptionQueue> ExceptionQueue { get; set; }
        public virtual DbSet<ExchangeOfMeter> ExchangeOfMeter { get; set; }
        public virtual DbSet<ExcludeFromBbaCompare2019> ExcludeFromBbaCompare2019 { get; set; }
        public virtual DbSet<FactuurRegelsOpmerking> FactuurRegelsOpmerking { get; set; }
        public virtual DbSet<GridareaGasregion> GridareaGasregion { get; set; }
        public virtual DbSet<InstallmentType> InstallmentType { get; set; }
        public virtual DbSet<InvoiceCreatedTask> InvoiceCreatedTask { get; set; }
        public virtual DbSet<InvoiceDecision> InvoiceDecision { get; set; }
        public virtual DbSet<InvoiceDecisionChoice> InvoiceDecisionChoice { get; set; }
        public virtual DbSet<InvoiceDecisionType> InvoiceDecisionType { get; set; }
        public virtual DbSet<InvoiceExceptions> InvoiceExceptions { get; set; }
        public virtual DbSet<InvoiceLines> InvoiceLines { get; set; }
        public virtual DbSet<InvoiceMail> InvoiceMail { get; set; }
        public virtual DbSet<InvoiceMailStatus> InvoiceMailStatus { get; set; }
        public virtual DbSet<InvoiceQueue> InvoiceQueue { get; set; }
        public virtual DbSet<InvoiceSectionDetails> InvoiceSectionDetails { get; set; }
        public virtual DbSet<InvoiceSections> InvoiceSections { get; set; }
        public virtual DbSet<InvoiceTemplate> InvoiceTemplate { get; set; }
        public virtual DbSet<InvoiceTemplatePage> InvoiceTemplatePage { get; set; }
        public virtual DbSet<InvoiceType> InvoiceType { get; set; }
        public virtual DbSet<InvoicedDecisions> InvoicedDecisions { get; set; }
        public virtual DbSet<InvoicedPeriods> InvoicedPeriods { get; set; }
        public virtual DbSet<InvoicedReadings> InvoicedReadings { get; set; }
        public virtual DbSet<Invoices> Invoices { get; set; }
        public virtual DbSet<InvoicingOnHold> InvoicingOnHold { get; set; }
        public virtual DbSet<Job> Job { get; set; }
        public virtual DbSet<JobQueue> JobQueue { get; set; }
        public virtual DbSet<JobQueueStatus> JobQueueStatus { get; set; }
        public virtual DbSet<KostenSoort> KostenSoort { get; set; }
        public virtual DbSet<Kwalificaties> Kwalificaties { get; set; }
        public virtual DbSet<LogContractMutations> LogContractMutations { get; set; }
        public virtual DbSet<LoggedProcedureCall> LoggedProcedureCall { get; set; }
        public virtual DbSet<Mep> Mep { get; set; }
        public virtual DbSet<MeterdataSelection> MeterdataSelection { get; set; }
        public virtual DbSet<Meters> Meters { get; set; }
        public virtual DbSet<MonthlyAmountHistory> MonthlyAmountHistory { get; set; }
        public virtual DbSet<MonthlyAmountModeratorType> MonthlyAmountModeratorType { get; set; }
        public virtual DbSet<Netbeheerders> Netbeheerders { get; set; }
        public virtual DbSet<OnHoldModeratorType> OnHoldModeratorType { get; set; }
        public virtual DbSet<OtherSuppliersInfo> OtherSuppliersInfo { get; set; }
        public virtual DbSet<PaymentDetails> PaymentDetails { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethod { get; set; }
        public virtual DbSet<Penalties> Penalties { get; set; }
        public virtual DbSet<PenaltyCategory> PenaltyCategory { get; set; }
        public virtual DbSet<PrConnectionView> PrConnectionView { get; set; }
        public virtual DbSet<PrSwitchRegelView> PrSwitchRegelView { get; set; }
        public virtual DbSet<PrSwitchesViewBeginSwitchRegels> PrSwitchesViewBeginSwitchRegels { get; set; }
        public virtual DbSet<PrSwitchesViewEindSwitchRegels> PrSwitchesViewEindSwitchRegels { get; set; }
        public virtual DbSet<ProcLog> ProcLog { get; set; }
        public virtual DbSet<ProcedureCall> ProcedureCall { get; set; }
        public virtual DbSet<Profielen> Profielen { get; set; }
        public virtual DbSet<ReclaimVoucherCancellationMoment> ReclaimVoucherCancellationMoment { get; set; }
        public virtual DbSet<Reconciliations> Reconciliations { get; set; }
        public virtual DbSet<Redenen> Redenen { get; set; }
        public virtual DbSet<RegistersType> RegistersType { get; set; }
        public virtual DbSet<RelatieDocumenten> RelatieDocumenten { get; set; }
        public virtual DbSet<Relaties> Relaties { get; set; }
        public virtual DbSet<ScheduledIncentivePaymentTask> ScheduledIncentivePaymentTask { get; set; }
        public virtual DbSet<Segmenten> Segmenten { get; set; }
        public virtual DbSet<SendEmailReclaimVoucher> SendEmailReclaimVoucher { get; set; }
        public virtual DbSet<SendRenewableContract> SendRenewableContract { get; set; }
        public virtual DbSet<SingleTransactionConfiguration> SingleTransactionConfiguration { get; set; }
        public virtual DbSet<Smslog> Smslog { get; set; }
        public virtual DbSet<StamdataRequest> StamdataRequest { get; set; }
        public virtual DbSet<StartOfUninvoicePeriod> StartOfUninvoicePeriod { get; set; }
        public virtual DbSet<SwitchCorrectieTypes> SwitchCorrectieTypes { get; set; }
        public virtual DbSet<SwitchRegels> SwitchRegels { get; set; }
        public virtual DbSet<SwitchRegelsLogs> SwitchRegelsLogs { get; set; }
        public virtual DbSet<Switches> Switches { get; set; }
        public virtual DbSet<TempAcquisitionInfo> TempAcquisitionInfo { get; set; }
        public virtual DbSet<TempAcquisitionInfoConnection> TempAcquisitionInfoConnection { get; set; }
        public virtual DbSet<TempAcquisitionInfoRegister> TempAcquisitionInfoRegister { get; set; }
        public virtual DbSet<TimeslicesSourceMasterdata> TimeslicesSourceMasterdata { get; set; }
        public virtual DbSet<ToBeRemovedTimeslicesSourceMasterdata> ToBeRemovedTimeslicesSourceMasterdata { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<VolumeDeterminationFactors> VolumeDeterminationFactors { get; set; }
        public virtual DbSet<VwInvoiceKeys> VwInvoiceKeys { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Adressen>(entity =>
            {
                entity.HasKey(e => e.AdresId);

                entity.ToTable("Adressen", "dbo");

                entity.HasIndex(e => e.LastMeterReadingAcquisitionDate);

                entity.HasIndex(e => e.Postcode);

                entity.HasIndex(e => new { e.Huisnummer, e.Postcode });

                entity.HasIndex(e => new { e.AdresId, e.Huisnummer, e.Postcode });

                entity.HasIndex(e => new { e.Postcode, e.Huisnummer, e.AdresId })
                    .HasName("IX_Adressen_Huisnummer_AdresID");

                entity.Property(e => e.AdresId).HasColumnName("AdresID");

                entity.Property(e => e.AfrekenMomentDatum).HasColumnType("datetime");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.LastMeterReadingAcquisitionDate).HasColumnType("date");

                entity.Property(e => e.Postcode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Straatnaam).HasMaxLength(50);

                entity.Property(e => e.Toevoeging).HasMaxLength(50);

                entity.Property(e => e.Woonplaats).HasMaxLength(50);
            });

            modelBuilder.Entity<AdressenLogs>(entity =>
            {
                entity.ToTable("AdressenLogs", "dbo");

                entity.HasIndex(e => e.AdresId);

                entity.Property(e => e.AdresId).HasColumnName("AdresID");

                entity.Property(e => e.ChangeType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.HuisnummerNew).HasColumnName("Huisnummer_new");

                entity.Property(e => e.HuisnummerOld).HasColumnName("Huisnummer_old");

                entity.Property(e => e.PostcodeNew)
                    .IsRequired()
                    .HasColumnName("Postcode_new")
                    .HasMaxLength(50);

                entity.Property(e => e.PostcodeOld)
                    .IsRequired()
                    .HasColumnName("Postcode_old")
                    .HasMaxLength(50);

                entity.Property(e => e.StraatnaamNew)
                    .HasColumnName("Straatnaam_new")
                    .HasMaxLength(50);

                entity.Property(e => e.StraatnaamOld)
                    .HasColumnName("Straatnaam_old")
                    .HasMaxLength(50);

                entity.Property(e => e.ToevoegingNew)
                    .HasColumnName("Toevoeging_new")
                    .HasMaxLength(50);

                entity.Property(e => e.ToevoegingOld)
                    .HasColumnName("Toevoeging_old")
                    .HasMaxLength(50);

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.WoonplaatsNew)
                    .HasColumnName("Woonplaats_new")
                    .HasMaxLength(50);

                entity.Property(e => e.WoonplaatsOld)
                    .HasColumnName("Woonplaats_old")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Adres)
                    .WithMany(p => p.AdressenLogs)
                    .HasForeignKey(d => d.AdresId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdressenLogs_Adressen");
            });

            modelBuilder.Entity<AdvanceInvoiceLines>(entity =>
            {
                entity.ToTable("AdvanceInvoiceLines", "dbo");

                entity.HasIndex(e => e.AdvanceInvoiceSectionId);

                entity.Property(e => e.AmountExcl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountIncl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountVat).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.Btwcode).HasColumnName("BTWCode");

                entity.Property(e => e.Price).HasColumnType("decimal(19, 6)");

                entity.Property(e => e.Quantity).HasColumnType("decimal(19, 6)");

                entity.HasOne(d => d.AdvanceInvoiceSection)
                    .WithMany(p => p.AdvanceInvoiceLines)
                    .HasForeignKey(d => d.AdvanceInvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvanceInvoiceLines_AdvanceInvoiceSection");

                entity.HasOne(d => d.Kostensoort)
                    .WithMany(p => p.AdvanceInvoiceLines)
                    .HasForeignKey(d => d.KostensoortId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvanceInvoiceLines_KostenSoort");

                entity.HasOne(d => d.TaxTariffKey)
                    .WithMany(p => p.AdvanceInvoiceLines)
                    .HasForeignKey(d => d.TaxTariffKeyId)
                    .HasConstraintName("FK_AdvanceInvoiceLines_TaxTariffKey");
            });

            modelBuilder.Entity<AdvanceInvoiceSections>(entity =>
            {
                entity.ToTable("AdvanceInvoiceSections", "dbo");

                entity.HasIndex(e => e.AdvanceInvoiceId)
                    .HasName("IX_AdvanceInvoiceSections_InvoiceId");

                entity.HasIndex(e => new { e.ContractRegelId, e.Id })
                    .HasName("IX_AdvanceInvoiceSections_ContractRegelId");

                entity.Property(e => e.AmountExcl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountIncl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountVat).HasColumnType("decimal(15, 4)");

                entity.HasOne(d => d.AdvanceInvoice)
                    .WithMany(p => p.AdvanceInvoiceSections)
                    .HasForeignKey(d => d.AdvanceInvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvanceInvoiceSections_AdvanceInvoices");
            });

            modelBuilder.Entity<AdvanceInvoices>(entity =>
            {
                entity.ToTable("AdvanceInvoices", "dbo");

                entity.HasIndex(e => new { e.Id, e.BillingDocumentInfoId })
                    .HasName("IX_AdvanceInvoices_Billing_Document_Info_Id");

                entity.HasIndex(e => new { e.Number, e.Id })
                    .HasName("IX_AdvanceInvoices_Number");

                entity.HasIndex(e => new { e.ReferencedInvoiceId, e.Id, e.CorrectionStatusId })
                    .HasName("IX_AdvanceInvoices_ReferencedInvoiceId");

                entity.HasIndex(e => new { e.ContractId, e.Id, e.CorrectionStatusId, e.Year, e.Month })
                    .HasName("IX_AdvanceInvoices_ContractID");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountNumber).HasMaxLength(50);

                entity.Property(e => e.AmountExcl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountIncl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountVat).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.BillingDocumentInfoId).HasColumnName("Billing_Document_Info_Id");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.DirectDebitDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentReference)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks).HasMaxLength(50);

                entity.HasOne(d => d.BillingDocumentInfo)
                    .WithMany(p => p.AdvanceInvoices)
                    .HasForeignKey(d => d.BillingDocumentInfoId)
                    .HasConstraintName("FK_AdvanceInvoices_Document_Info");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.AdvanceInvoices)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvanceInvoices_Contracten");

                entity.HasOne(d => d.CorrectionStatus)
                    .WithMany(p => p.AdvanceInvoices)
                    .HasForeignKey(d => d.CorrectionStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvanceInvoices_CorrectionStatus");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.AdvanceInvoices)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK_AdvanceInvoices_Users");

                entity.HasOne(d => d.PaymentMethod)
                    .WithMany(p => p.AdvanceInvoices)
                    .HasForeignKey(d => d.PaymentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdvanceInvoices_PaymentMethod");
            });

            modelBuilder.Entity<Batch>(entity =>
            {
                entity.ToTable("Batch", "dbo");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.InvoiceType)
                    .WithMany(p => p.Batch)
                    .HasForeignKey(d => d.InvoiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Batch_InvoiceType");

                entity.HasOne(d => d.RunByUser)
                    .WithMany(p => p.Batch)
                    .HasForeignKey(d => d.RunByUserId)
                    .HasConstraintName("FK_Batch_Users_Run");
            });

            modelBuilder.Entity<BbaBatch>(entity =>
            {
                entity.ToTable("BbaBatch", "dbo");

                entity.Property(e => e.ApprovedDate).HasColumnType("datetime");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RunDate).HasColumnType("datetime");

                entity.HasOne(d => d.ApprovedByUser)
                    .WithMany(p => p.BbaBatchApprovedByUser)
                    .HasForeignKey(d => d.ApprovedByUserId)
                    .HasConstraintName("FK_BbaBatch_BbaBatch_Approved");

                entity.HasOne(d => d.BbaInvoiceRun)
                    .WithMany(p => p.BbaBatch)
                    .HasForeignKey(d => d.BbaInvoiceRunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BbaBatch_BbaInvoiceRunId");

                entity.HasOne(d => d.RunByUser)
                    .WithMany(p => p.BbaBatchRunByUser)
                    .HasForeignKey(d => d.RunByUserId)
                    .HasConstraintName("FK_BbaBatch_BbaBatch_Run");
            });

            modelBuilder.Entity<BbaBatchContract>(entity =>
            {
                entity.HasKey(e => new { e.BbaBatchId, e.ContractId });

                entity.ToTable("BbaBatchContract", "dbo");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.AdminBtw).HasColumnType("decimal(11, 4)");

                entity.Property(e => e.AdminExcl).HasColumnType("decimal(11, 4)");

                entity.Property(e => e.AdminIncl).HasColumnType("decimal(11, 4)");

                entity.Property(e => e.BedragBtw).HasColumnType("decimal(11, 4)");

                entity.Property(e => e.BedragExcl).HasColumnType("decimal(11, 4)");

                entity.Property(e => e.BedragIncl).HasColumnType("decimal(11, 4)");

                entity.Property(e => e.DirectdebitDate).HasColumnType("date");

                entity.Property(e => e.ErrorMessage)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VerzendWijze)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.BbaBatch)
                    .WithMany(p => p.BbaBatchContract)
                    .HasForeignKey(d => d.BbaBatchId)
                    .HasConstraintName("FK_BbaBatchContract_BbaBatch");
            });

            modelBuilder.Entity<BbaBatchContractRegel>(entity =>
            {
                entity.HasKey(e => new { e.BbaBatchId, e.ContractRegelId });

                entity.ToTable("BbaBatchContractRegel", "dbo");

                entity.HasOne(d => d.BbaBatch)
                    .WithMany(p => p.BbaBatchContractRegel)
                    .HasForeignKey(d => d.BbaBatchId)
                    .HasConstraintName("FK_BbaBatchContractRegel_BbaBatch");
            });

            modelBuilder.Entity<BbaComparerResults>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.ToTable("BbaComparerResults", "dbo");

                entity.HasIndex(e => new { e.LabelCode, e.Difference })
                    .HasName("IX_BbaCompareResults_Label_Difference");

                entity.Property(e => e.ContractId).ValueGeneratedNever();

                entity.Property(e => e.AdvisoryAmount).HasColumnType("money");

                entity.Property(e => e.BbaStartDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentBba).HasColumnType("money");

                entity.Property(e => e.Difference).HasColumnType("money");

                entity.Property(e => e.EstimatedBba).HasColumnType("money");

                entity.Property(e => e.FractionsGas).HasColumnType("decimal(9, 5)");

                entity.Property(e => e.FractionsProduction).HasColumnType("decimal(9, 5)");

                entity.Property(e => e.NeutralAmount).HasColumnType("money");

                entity.Property(e => e.PaymentRisk).HasColumnType("money");

                entity.Property(e => e.Percentage).HasColumnType("money");

                entity.Property(e => e.RunDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BbaInvoiceRunConfig>(entity =>
            {
                entity.ToTable("BbaInvoice_Run_Config", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<BbaInvoiceRunContract>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.ToTable("BbaInvoice_Run_Contract", "dbo");

                entity.Property(e => e.ContractId).ValueGeneratedNever();

                entity.HasOne(d => d.BbaInvoiceRun)
                    .WithMany(p => p.BbaInvoiceRunContract)
                    .HasForeignKey(d => d.BbaInvoiceRunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contract_BbaInvoiceRunId");

                entity.HasOne(d => d.Contract)
                    .WithOne(p => p.BbaInvoiceRunContract)
                    .HasForeignKey<BbaInvoiceRunContract>(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BbaInvoice_Contracten");
            });

            modelBuilder.Entity<BbaInvoiceRunContractLog>(entity =>
            {
                entity.ToTable("BbaInvoice_Run_Contract_Log", "dbo");

                entity.Property(e => e.BbaInvoiceRunIdNew).HasColumnName("BbaInvoiceRunId_new");

                entity.Property(e => e.BbaInvoiceRunIdOld).HasColumnName("BbaInvoiceRunId_old");

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.HasOne(d => d.ModifiedByUser)
                    .WithMany(p => p.BbaInvoiceRunContractLog)
                    .HasForeignKey(d => d.ModifiedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BbaInvoiceRunContractLog_Users");
            });

            modelBuilder.Entity<BbaUpdateMail>(entity =>
            {
                entity.HasKey(e => new { e.BbaUpdateMailHeadId, e.ContractId })
                    .IsClustered(false);

                entity.ToTable("BbaUpdateMail", "dbo");

                entity.Property(e => e.BbaUpdateDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentBba).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.InvoiceReceivedDate).HasColumnType("datetime");

                entity.Property(e => e.IsOptedOutDate).HasColumnType("datetime");

                entity.Property(e => e.MailNotificationDate).HasColumnType("datetime");

                entity.Property(e => e.NewBbaAmount).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.SourceReferenceId)
                    .HasMaxLength(36)
                    .IsUnicode(false);

                entity.HasOne(d => d.BbaUpdateMailHead)
                    .WithMany(p => p.BbaUpdateMail)
                    .HasForeignKey(d => d.BbaUpdateMailHeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BbaUpdateMail_BbaUpdateMailHead");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.BbaUpdateMail)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BbaCompareResults_BbaUpdateMail");

                entity.HasOne(d => d.ContractNavigation)
                    .WithMany(p => p.BbaUpdateMail)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contracten_BbaUpdateMail");
            });

            modelBuilder.Entity<BbaUpdateMailHead>(entity =>
            {
                entity.ToTable("BbaUpdateMailHead", "dbo");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BbaUpdateMailSearchFilter>(entity =>
            {
                entity.HasKey(e => e.BbaUpdateMailHeadId);

                entity.ToTable("BbaUpdateMailSearchFilter", "dbo");

                entity.Property(e => e.BbaUpdateMailHeadId).ValueGeneratedNever();

                entity.Property(e => e.MaxFractionsGas).HasColumnType("decimal(9, 5)");

                entity.Property(e => e.MaxFractionsProduction).HasColumnType("decimal(9, 5)");

                entity.Property(e => e.MinFractionsGas).HasColumnType("decimal(9, 5)");

                entity.Property(e => e.MinFractionsProduction).HasColumnType("decimal(9, 5)");

                entity.HasOne(d => d.BbaUpdateMailHead)
                    .WithOne(p => p.BbaUpdateMailSearchFilter)
                    .HasForeignKey<BbaUpdateMailSearchFilter>(d => d.BbaUpdateMailHeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BbaUpdateMailSearchFilter_BbaUpdateMailHead");
            });

            modelBuilder.Entity<BillingBbaCalculationInputs>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Billing_BbaCalculationInputs", "dbo");

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.CapTarCode)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Categorie)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanGebruiker)
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.EnergyFlowDirection)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GridOperator)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.ProfielId)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PropositionId)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.Property(e => e.SegmentId).HasColumnName("SegmentID");

                entity.Property(e => e.StartDate).HasColumnType("date");
            });

            modelBuilder.Entity<BillingDocumentInfo>(entity =>
            {
                entity.ToTable("Billing_Document_Info", "dbo");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.ProviderKey)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BillingTaxDiscount>(entity =>
            {
                entity.HasKey(e => new { e.TaxTariffKeyId, e.Residential })
                    .HasName("PK_TaxDiscount");

                entity.ToTable("Billing_TaxDiscount", "dbo");

                entity.Property(e => e.TaxTariffKeyId).HasColumnName("TaxTariffKey_Id");

                entity.Property(e => e.Amount).HasColumnType("decimal(6, 2)");

                entity.HasOne(d => d.TaxTariffKey)
                    .WithMany(p => p.BillingTaxDiscount)
                    .HasForeignKey(d => d.TaxTariffKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Billing_TaxDiscount_TaxTariffKey");
            });

            modelBuilder.Entity<BillingTaxTariff>(entity =>
            {
                entity.HasKey(e => new { e.TaxTariffKeyId, e.SegmentId, e.IsHorticulture });

                entity.ToTable("Billing_TaxTariff", "dbo");

                entity.Property(e => e.TaxTariffKeyId).HasColumnName("TaxTariffKey_Id");

                entity.Property(e => e.Eb1)
                    .HasColumnName("EB_1")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Eb2)
                    .HasColumnName("EB_2")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Eb3)
                    .HasColumnName("EB_3")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Eb4)
                    .HasColumnName("EB_4")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Eb5)
                    .HasColumnName("EB_5")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Ode1)
                    .HasColumnName("ODE_1")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Ode2)
                    .HasColumnName("ODE_2")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Ode3)
                    .HasColumnName("ODE_3")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Ode4)
                    .HasColumnName("ODE_4")
                    .HasColumnType("decimal(7, 6)");

                entity.Property(e => e.Ode5)
                    .HasColumnName("ODE_5")
                    .HasColumnType("decimal(7, 6)");

                entity.HasOne(d => d.TaxTariffKey)
                    .WithMany(p => p.BillingTaxTariff)
                    .HasForeignKey(d => d.TaxTariffKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TaxTariff_TaxTariffKey");
            });

            modelBuilder.Entity<BillingTaxTariffKey>(entity =>
            {
                entity.ToTable("Billing_TaxTariffKey", "dbo");

                entity.HasIndex(e => new { e.FromDate, e.ToDate })
                    .HasName("AK_TaxTariff_Period")
                    .IsUnique();

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<Bronnen>(entity =>
            {
                entity.HasKey(e => e.BronId)
                    .HasName("PK_Bron");

                entity.ToTable("Bronnen", "dbo");

                entity.Property(e => e.BronId)
                    .HasColumnName("BronID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Omschrijving)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<Btw>(entity =>
            {
                entity.HasKey(e => e.Btwcode);

                entity.ToTable("BTW", "dbo");

                entity.Property(e => e.Btwcode)
                    .HasColumnName("BTWCode")
                    .ValueGeneratedNever();

                entity.Property(e => e.BeginDatum).HasColumnType("date");

                entity.Property(e => e.Categorie).HasMaxLength(50);

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EindDatum).HasColumnType("date");

                entity.Property(e => e.Omschrijving).HasMaxLength(50);

                entity.Property(e => e.TimeStamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<CalGosRegels2>(entity =>
            {
                entity.HasKey(e => new { e.EanGos, e.Jaar, e.Maand });

                entity.ToTable("CalGosRegels2", "dbo");

                entity.Property(e => e.EanGos)
                    .HasColumnName("EanGOS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CalorFactor).HasColumnType("decimal(10, 9)");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<CapaciteitenUitvalteam>(entity =>
            {
                entity.HasKey(e => e.EanCapaciteit)
                    .HasName("PK_Capaciteiten");

                entity.ToTable("Capaciteiten_Uitvalteam", "dbo");

                entity.Property(e => e.EanCapaciteit)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.NaamNb)
                    .HasColumnName("NaamNB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Omschrijving)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<CapaciteitsTarieven>(entity =>
            {
                entity.ToTable("CapaciteitsTarieven", "dbo");

                entity.HasIndex(e => new { e.EanCapaciteit, e.Start })
                    .HasName("AK_CapaciteitsTarieven")
                    .IsUnique();

                entity.Property(e => e.CapaciteitsTarievenId).HasColumnName("CapaciteitsTarievenID");

                entity.Property(e => e.AansluitDienst).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EanCapaciteit)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MeetDienst).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Start).HasColumnType("datetime");

                entity.Property(e => e.SysteemDienst).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.TransportDienst).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.Vastrecht).HasColumnType("decimal(18, 4)");
            });

            modelBuilder.Entity<CashbackUpfrontOnHold>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.ToTable("CashbackUpfrontOnHold", "dbo");

                entity.Property(e => e.ContractId).ValueGeneratedNever();

                entity.Property(e => e.Inserted).HasColumnType("date");

                entity.HasOne(d => d.Contract)
                    .WithOne(p => p.CashbackUpfrontOnHold)
                    .HasForeignKey<CashbackUpfrontOnHold>(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CashbackUpfrontOnHold_Contracten");
            });

            modelBuilder.Entity<CerHistory>(entity =>
            {
                entity.ToTable("CerHistory", "dbo");

                entity.HasIndex(e => e.EanId);

                entity.Property(e => e.BalanceSupplierCompany)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EndDateContract).HasColumnType("date");

                entity.Property(e => e.RequestDate).HasColumnType("datetime");

                entity.Property(e => e.RequestXml).HasColumnName("RequestXML");

                entity.Property(e => e.ResponseXml).HasColumnName("ResponseXML");
            });

            modelBuilder.Entity<CompetitorTariffs>(entity =>
            {
                entity.ToTable("CompetitorTariffs", "dbo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Competitor).HasMaxLength(20);

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Gas).HasColumnType("decimal(10, 9)");

                entity.Property(e => e.InsertedBy)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Low).HasColumnType("decimal(10, 9)");

                entity.Property(e => e.Normal).HasColumnType("decimal(10, 9)");

                entity.Property(e => e.Single).HasColumnType("decimal(10, 9)");

                entity.Property(e => e.StandingChargeMonthlyElectricity).HasColumnType("decimal(11, 9)");

                entity.Property(e => e.StandingChargeMonthlyGas).HasColumnType("decimal(11, 9)");
            });

            modelBuilder.Entity<ConnectionSjvSource>(entity =>
            {
                entity.ToTable("ConnectionSjvSource", "dbo");

                entity.HasIndex(e => e.ContractRegelId);

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.HasOne(d => d.ContractRegel)
                    .WithMany(p => p.ConnectionSjvSource)
                    .HasForeignKey(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ConnectionSjvSourceData_ContractRegels");
            });

            modelBuilder.Entity<ContractAgreements>(entity =>
            {
                entity.ToTable("ContractAgreements", "dbo");

                entity.HasIndex(e => e.ContractId)
                    .HasName("UX_ContractAgreements_ContractId")
                    .IsUnique();

                entity.Property(e => e.CreationDate).HasColumnType("date");

                entity.Property(e => e.CustomerPaysPaymentCostsExclVat).HasColumnType("money");

                entity.Property(e => e.DeletionBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DeletionDate).HasColumnType("date");

                entity.Property(e => e.DeletionReason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IncentivePaymentAmount).HasColumnType("money");

                entity.Property(e => e.RenewableFromDate).HasColumnType("date");

                entity.HasOne(d => d.Contract)
                    .WithOne(p => p.ContractAgreements)
                    .HasForeignKey<ContractAgreements>(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractAgreements_Contracten");

                entity.HasOne(d => d.PenaltyCategory)
                    .WithMany(p => p.ContractAgreements)
                    .HasForeignKey(d => d.PenaltyCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractAgreements_PenaltyCategory");
            });

            modelBuilder.Entity<ContractDocumenten>(entity =>
            {
                entity.ToTable("ContractDocumenten", "dbo");

                entity.HasIndex(e => new { e.ExternalContractReference, e.ContractId })
                    .HasName("IX_ContractDocumenten_ContractId");

                entity.Property(e => e.ExternalContractReference)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.ContractDocumenten)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractDocumenten_Contracten");
            });

            modelBuilder.Entity<ContractMoveOut>(entity =>
            {
                entity.ToTable("ContractMoveOut", "dbo");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DossierId)
                    .IsRequired()
                    .HasColumnName("DossierID")
                    .IsUnicode(false);

                entity.Property(e => e.Eanid)
                    .IsRequired()
                    .HasColumnName("EANID")
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.OldBalanceSupplier)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsFixedLength();
            });

            modelBuilder.Entity<ContractOpzegging>(entity =>
            {
                entity.ToTable("ContractOpzegging", "dbo");

                entity.HasIndex(e => e.ContractId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.DatumUithuizing).HasColumnType("datetime");

                entity.Property(e => e.OpzeggingGeannuleerd).HasColumnType("datetime");

                entity.Property(e => e.RedenOpzegging)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.ContractOpzegging)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contracten_ContractOpzegging");
            });

            modelBuilder.Entity<ContractRegelAgreements>(entity =>
            {
                entity.ToTable("ContractRegelAgreements", "dbo");

                entity.HasIndex(e => e.ContractRegelId)
                    .HasName("IX_ContractRegelAgreements_ContractRegel");

                entity.HasIndex(e => new { e.DeletionDate, e.PeriodFrom, e.PeriodTo, e.ProdmanPropositionId })
                    .HasName("IX_ContractRegelAgreementsDeletionDateFromToProposition");

                entity.HasIndex(e => new { e.PeriodTo, e.ContractRegelId, e.DeletionDate, e.PeriodFrom })
                    .HasName("IX_ContractRegelAgreements_ContractRegelId_DeletionDate_PeriodFrom_incl_PeriodTo");

                entity.Property(e => e.CreationDate).HasColumnType("date");

                entity.Property(e => e.DeletionBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DeletionDate).HasColumnType("date");

                entity.Property(e => e.DeletionReason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodFrom).HasColumnType("date");

                entity.Property(e => e.PeriodTo).HasColumnType("date");

                entity.Property(e => e.ProdmanPropositionId)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.HasOne(d => d.ContractRegel)
                    .WithMany(p => p.ContractRegelAgreements)
                    .HasForeignKey(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegelAgreements_ContractRegel");

                entity.HasOne(d => d.ProfileNavigation)
                    .WithMany(p => p.ContractRegelAgreements)
                    .HasPrincipalKey(p => p.ProfileCode)
                    .HasForeignKey(d => d.Profile)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegelAgreements_ProfileCode");
            });

            modelBuilder.Entity<ContractRegelSpecificaties>(entity =>
            {
                entity.HasKey(e => e.CrspecId);

                entity.ToTable("ContractRegelSpecificaties", "dbo");

                entity.HasIndex(e => e.ContractRegelId);

                entity.HasIndex(e => new { e.Hoeveelheid, e.Prijs, e.Btwcode, e.BedragExcl, e.BedragBtw, e.BedragIncl, e.CreatieDatum, e.Timestamp, e.KostensoortId, e.TaxTariffKeyId, e.ContractRegelId })
                    .HasName("idxContractRegelSpec2");

                entity.Property(e => e.CrspecId).HasColumnName("CRSpecID");

                entity.Property(e => e.BedragBtw)
                    .HasColumnName("BedragBTW")
                    .HasColumnType("decimal(19, 4)");

                entity.Property(e => e.BedragExcl).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.BedragIncl).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.Btwcode).HasColumnName("BTWCode");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.TaxTariffKeyId).HasColumnName("TaxTariffKey_Id");

                entity.Property(e => e.Timestamp)
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.ContractRegel)
                    .WithMany(p => p.ContractRegelSpecificaties)
                    .HasForeignKey(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegelSpecificaties_ContractRegels");

                entity.HasOne(d => d.Kostensoort)
                    .WithMany(p => p.ContractRegelSpecificaties)
                    .HasForeignKey(d => d.KostensoortId)
                    .HasConstraintName("FK_ContractRegelSpecificaties_KostenSoort");

                entity.HasOne(d => d.TaxTariffKey)
                    .WithMany(p => p.ContractRegelSpecificaties)
                    .HasForeignKey(d => d.TaxTariffKeyId)
                    .HasConstraintName("FK_ContractRegelSpecificaties_TaxTariffKey");
            });

            modelBuilder.Entity<ContractRegelStatusHistory>(entity =>
            {
                entity.ToTable("ContractRegelStatusHistory", "dbo");

                entity.HasIndex(e => e.ContractRegelId)
                    .HasName("UX_ContractRegelStatusHistory_ContractRegelId")
                    .IsUnique();

                entity.HasIndex(e => new { e.ContractRegelId, e.ToDate })
                    .HasName("ContractRegelStatusHistory_ToDate_IX");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CooldownEndDate).HasColumnType("date");

                entity.Property(e => e.EndSupply).HasColumnType("date");

                entity.Property(e => e.ExpectedStartSupply).HasColumnType("date");

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.StartSupply).HasColumnType("date");

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.HasOne(d => d.ContractRegel)
                    .WithOne(p => p.ContractRegelStatusHistory)
                    .HasForeignKey<ContractRegelStatusHistory>(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegelStatusHistory_ContractRegels");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ContractRegelStatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegelStatusHistory_Dictionary_ContractRegelsDetailStatus");
            });

            modelBuilder.Entity<ContractRegels>(entity =>
            {
                entity.HasKey(e => e.ContractRegelId);

                entity.ToTable("ContractRegels", "dbo");

                entity.HasIndex(e => e.ContractId);

                entity.HasIndex(e => new { e.ContractRegelId, e.ContractId, e.EanId, e.Actief })
                    .HasName("IX_ContractRegels_Actief");

                entity.HasIndex(e => new { e.Geannuleerd, e.EanId, e.ContractId, e.ContractRegelId })
                    .HasName("IX_ContractRegels_EanID_ContractID_ContractRegelID");

                entity.HasIndex(e => new { e.ContractRegelId, e.ContractId, e.EanId, e.Ingangsdatum, e.Actief, e.ContractStart, e.Geannuleerd })
                    .HasName("IX_ContractRegels_Geannuleerd_incl_ContractId_EanID_Ingangsdatum_Actief_ContractStart2");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.ContractEind).HasColumnType("date");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ContractStart).HasColumnType("date");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasColumnName("EanID");

                entity.Property(e => e.GeannuleerdDatum).HasColumnType("datetime");

                entity.Property(e => e.Ingangsdatum).HasColumnType("date");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.ContractRegels)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegels_Contracten");

                entity.HasOne(d => d.Ean)
                    .WithMany(p => p.ContractRegels)
                    .HasForeignKey(d => d.EanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegels_EanBoek");
            });

            modelBuilder.Entity<ContractRegelsHorticulture>(entity =>
            {
                entity.HasKey(e => e.ContractRegelId);

                entity.ToTable("ContractRegelsHorticulture", "dbo");

                entity.Property(e => e.ContractRegelId).ValueGeneratedNever();

                entity.HasOne(d => d.ContractRegel)
                    .WithOne(p => p.ContractRegelsHorticulture)
                    .HasForeignKey<ContractRegelsHorticulture>(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractRegelsHorticulture_ContractRegels");
            });

            modelBuilder.Entity<ContractRegistery>(entity =>
            {
                entity.ToTable("ContractRegistery", "dbo");

                entity.HasIndex(e => new { e.Eanid, e.CancellationDate });

                entity.HasIndex(e => new { e.Eanid, e.CreatedOn })
                    .HasName("IX_ContractRegistery_EANID_CreatedOn_DESC");

                entity.HasIndex(e => new { e.Id, e.CancellationDate, e.BalanceSupplierId })
                    .HasName("IX_ContractRegistery_BalanceSupplierID_incl_Id_CancellationDate");

                entity.HasIndex(e => new { e.Id, e.Eanid, e.ContractId })
                    .HasName("IX_ContractRegistery_EANID_ContractId");

                entity.HasIndex(e => new { e.AddressId, e.ContractId, e.CustomerId, e.Prnotified })
                    .HasName("IX_ContractRegistery_AddressContractCustomerId");

                entity.Property(e => e.BalanceSupplierId)
                    .IsRequired()
                    .HasColumnName("BalanceSupplierID")
                    .HasMaxLength(13)
                    .IsFixedLength();

                entity.Property(e => e.CancellationDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DossierId)
                    .IsRequired()
                    .HasColumnName("DossierID")
                    .IsUnicode(false);

                entity.Property(e => e.Eanid)
                    .IsRequired()
                    .HasColumnName("EANID")
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Prnotified).HasColumnName("PRNotified");
            });

            modelBuilder.Entity<ContractSignupStatus>(entity =>
            {
                entity.ToTable("ContractSignupStatus", "dbo");

                entity.HasIndex(e => e.ContractId)
                    .HasName("IX_ContractSignupStatus_ContractId");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.SignupStatusCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SignupStatusCodeExt)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContractVoucher>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.ToTable("ContractVoucher", "dbo");

                entity.HasIndex(e => e.VoucherId)
                    .HasName("UC_ContractVoucher")
                    .IsUnique();

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .ValueGeneratedNever();

                entity.Property(e => e.FinalAmountSpent).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.VoucherId).HasColumnName("VoucherID");

                entity.HasOne(d => d.Contract)
                    .WithOne(p => p.ContractVoucher)
                    .HasForeignKey<ContractVoucher>(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContractVoucher_Contracten");
            });

            modelBuilder.Entity<Contracten>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.ToTable("Contracten", "dbo");

                entity.HasIndex(e => e.CadresId);

                entity.HasIndex(e => e.ContractantId);

                entity.HasIndex(e => e.SendMonthlyUsageEmailFrom);

                entity.HasIndex(e => new { e.ContractId, e.RenewalOfId })
                    .HasName("IX_Contracten_RenewalOfId");

                entity.HasIndex(e => new { e.ContractantId, e.CadresId })
                    .HasName("IX_Contracten_CAdresID_ContractantId");

                entity.HasIndex(e => new { e.ContractantId, e.Geannuleerd })
                    .HasName("IX_Contracten_incl_ContractantID");

                entity.HasIndex(e => new { e.Geannuleerd, e.RenewalOfId });

                entity.HasIndex(e => new { e.ContractId, e.ContractantId, e.CadresId })
                    .HasName("IX_Contracten");

                entity.HasIndex(e => new { e.ContractId, e.Geannuleerd, e.MoveFromId })
                    .HasName("IX_Contracten_MoveFromId");

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AfrekenMomentDatum).HasColumnType("date");

                entity.Property(e => e.CadresId).HasColumnName("CAdresID");

                entity.Property(e => e.ContractantId).HasColumnName("ContractantID");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.GeannuleerdDatum).HasColumnType("datetime");

                entity.Property(e => e.IngevoerdDatum).HasColumnType("datetime");

                entity.Property(e => e.LastMeterReadingAcquisitionDate).HasColumnType("date");

                entity.Property(e => e.SendMonthlyUsageEmailFrom).HasColumnType("date");

                entity.Property(e => e.SignUpDate).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Cadres)
                    .WithMany(p => p.Contracten)
                    .HasForeignKey(d => d.CadresId)
                    .HasConstraintName("FK_Contracten_Adressen");

                entity.HasOne(d => d.Contractant)
                    .WithMany(p => p.Contracten)
                    .HasForeignKey(d => d.ContractantId)
                    .HasConstraintName("FK_Contracten_Relaties");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.Contracten)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MoveFrom)
                    .WithMany(p => p.InverseMoveFrom)
                    .HasForeignKey(d => d.MoveFromId)
                    .HasConstraintName("FK_Contracten_Contracten1");

                entity.HasOne(d => d.RenewalOf)
                    .WithMany(p => p.InverseRenewalOf)
                    .HasForeignKey(d => d.RenewalOfId)
                    .HasConstraintName("FK_Contracten_Contracten");
            });

            modelBuilder.Entity<CorrectionStatus>(entity =>
            {
                entity.ToTable("CorrectionStatus", "dbo");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CoulanceType>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("CoulanceType", "dbo");

                entity.Property(e => e.CoulanceTypeId)
                    .HasColumnName("CoulanceTypeID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DefaultCaptar>(entity =>
            {
                entity.HasKey(e => new { e.EanGridOperator, e.PhysicalCapacityId })
                    .IsClustered(false);

                entity.ToTable("DefaultCaptar", "dbo");

                entity.Property(e => e.EanGridOperator)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EanCapaciteit)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.NameGridOperator).HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryContractRegelsDetailStatus>(entity =>
            {
                entity.ToTable("Dictionary_ContractRegelsDetailStatus", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EanBoek>(entity =>
            {
                entity.HasKey(e => e.EanId);

                entity.ToTable("EanBoek", "dbo");

                entity.HasIndex(e => e.EanGebruiker)
                    .HasName("IX_EanBoek")
                    .IsUnique();

                entity.HasIndex(e => e.VadresId)
                    .HasName("IX_EanBoek_3");

                entity.HasIndex(e => new { e.EanGebruiker, e.EanId })
                    .HasName("IX_EanBoek_EanID_EanGebruiker");

                entity.HasIndex(e => new { e.EanId, e.EanGebruiker, e.EanNb, e.SegmentId })
                    .HasName("IX_EanBoek_2");

                entity.Property(e => e.EanId).HasColumnName("EanID");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EanGebruiker)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EanNb)
                    .HasColumnName("EanNB")
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.SegmentId).HasColumnName("SegmentID");

                entity.Property(e => e.VadresId).HasColumnName("VAdresID");

                entity.HasOne(d => d.EanNbNavigation)
                    .WithMany(p => p.EanBoek)
                    .HasForeignKey(d => d.EanNb)
                    .HasConstraintName("FK_EanBoek_Netbeheerders");

                entity.HasOne(d => d.Segment)
                    .WithMany(p => p.EanBoek)
                    .HasForeignKey(d => d.SegmentId)
                    .HasConstraintName("FK_EanBoek_Segmenten");

                entity.HasOne(d => d.Vadres)
                    .WithMany(p => p.EanBoek)
                    .HasForeignKey(d => d.VadresId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EanBoek_Adressen");
            });

            modelBuilder.Entity<EditStatus>(entity =>
            {
                entity.ToTable("EditStatus", "dbo");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EdsnFraction>(entity =>
            {
                entity.ToTable("EdsnFraction", "dbo");

                entity.HasIndex(e => new { e.FractionValue, e.NormValue, e.Profile, e.FractionDate })
                    .HasName("IX_EdsnFraction_Profile_FractionDate_INCL_FractionValue_NormValue");

                entity.Property(e => e.FractionDate).HasColumnType("date");

                entity.Property(e => e.FractionInsertedDate).HasColumnType("date");

                entity.Property(e => e.FractionValue).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.NormInsertedDate).HasColumnType("date");

                entity.Property(e => e.NormValue).HasColumnType("decimal(18, 9)");

                entity.Property(e => e.Profile)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EdsnFractionProduction>(entity =>
            {
                entity.HasKey(e => e.Date);

                entity.ToTable("EdsnFractionProduction", "dbo");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Fraction).HasColumnType("decimal(9, 8)");
            });

            modelBuilder.Entity<Eosdata>(entity =>
            {
                entity.ToTable("EOSData", "dbo");

                entity.HasIndex(e => new { e.IsUploaded, e.IsRejected, e.CreatedOn })
                    .HasName("IX_EOSData_IsUploaded_CreatedOn");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Eosdate)
                    .HasColumnName("EOSDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Rejection).HasMaxLength(100);

                entity.Property(e => e.UploadedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ExceptionQueue>(entity =>
            {
                entity.ToTable("ExceptionQueue", "dbo");

                entity.HasIndex(e => new { e.ContractId, e.ExceptionNr });

                entity.Property(e => e.ContextInfo).IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ExceptionMessage)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IgnoreDate).HasColumnType("datetime");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.ExceptionQueue)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExceptionQueue_Contracten");

                entity.HasOne(d => d.ExceptionNrNavigation)
                    .WithMany(p => p.ExceptionQueue)
                    .HasForeignKey(d => d.ExceptionNr)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExceptionQueue_InvoiceExceptions");

                entity.HasOne(d => d.InvoiceType)
                    .WithMany(p => p.ExceptionQueue)
                    .HasForeignKey(d => d.InvoiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExceptionQueue_InvoiceType");
            });

            modelBuilder.Entity<ExchangeOfMeter>(entity =>
            {
                entity.ToTable("ExchangeOfMeter", "dbo");

                entity.HasIndex(e => new { e.Eanid, e.MutationDate });

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Eanid)
                    .IsRequired()
                    .HasColumnName("EANID")
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MutationDate).HasColumnType("datetime");

                entity.Property(e => e.NewMeterNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NewProfileCategory)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.OldMeterNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldProfileCategory)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ExcludeFromBbaCompare2019>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.ToTable("ExcludeFromBbaCompare2019", "dbo");

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .ValueGeneratedNever();

                entity.HasOne(d => d.Contract)
                    .WithOne(p => p.ExcludeFromBbaCompare2019)
                    .HasForeignKey<ExcludeFromBbaCompare2019>(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ExcludeFromBbaCompare2019_Contracten");
            });

            modelBuilder.Entity<FactuurRegelsOpmerking>(entity =>
            {
                entity.HasKey(e => e.FactuurRegelId);

                entity.ToTable("FactuurRegelsOpmerking", "dbo");

                entity.Property(e => e.FactuurRegelId)
                    .HasColumnName("FactuurRegelID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Opmerking)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<GridareaGasregion>(entity =>
            {
                entity.HasKey(e => e.GridArea)
                    .HasName("PK_GridareaGasregion");

                entity.ToTable("Gridarea_Gasregion", "dbo");

                entity.Property(e => e.GridArea)
                    .HasMaxLength(18)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InstallmentType>(entity =>
            {
                entity.ToTable("InstallmentType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(127);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<InvoiceCreatedTask>(entity =>
            {
                entity.HasKey(e => e.InvoiceId)
                    .HasName("PK__InvoiceC__D796AAB5688F92CB");

                entity.ToTable("InvoiceCreatedTask", "dbo");

                entity.HasIndex(e => new { e.Status, e.ScheduledDate })
                    .HasName("IX_InvoiceCreatedTask_Scheduled_Date");

                entity.Property(e => e.InvoiceId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ScheduledDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<InvoiceDecision>(entity =>
            {
                entity.ToTable("InvoiceDecision", "dbo");

                entity.HasIndex(e => e.ContractRegelId);

                entity.HasIndex(e => new { e.DecisionChoiceId, e.ContractRegelId });

                entity.Property(e => e.EnergymeterId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastModified).HasColumnType("datetime");
            });

            modelBuilder.Entity<InvoiceDecisionChoice>(entity =>
            {
                entity.HasKey(e => e.DecisionChoiceId)
                    .HasName("PK_InvoiceDecisionChoice1");

                entity.ToTable("InvoiceDecisionChoice", "dbo");

                entity.Property(e => e.DecisionChoiceId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.DecisionType)
                    .WithMany(p => p.InvoiceDecisionChoice)
                    .HasForeignKey(d => d.DecisionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceDecisionChoice_InvoiceDecisionType");
            });

            modelBuilder.Entity<InvoiceDecisionType>(entity =>
            {
                entity.HasKey(e => e.DecisionTypeId);

                entity.ToTable("InvoiceDecisionType", "dbo");

                entity.Property(e => e.DecisionTypeId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceExceptions>(entity =>
            {
                entity.HasKey(e => e.ExceptionNr);

                entity.ToTable("InvoiceExceptions", "dbo");

                entity.Property(e => e.ExceptionNr).ValueGeneratedNever();

                entity.Property(e => e.ExceptionDateCreated).HasColumnType("datetime");

                entity.Property(e => e.ExceptionMessage)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceLines>(entity =>
            {
                entity.ToTable("InvoiceLines", "dbo");

                entity.HasIndex(e => new { e.InvoiceSectionId, e.KostensoortId })
                    .HasName("IX_InvoiceLines_InvoiceSectionId_KostensoortID");

                entity.Property(e => e.AmountExcl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountIncl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountVat).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.Btwcode).HasColumnName("BTWCode");

                entity.Property(e => e.Price).HasColumnType("decimal(19, 6)");

                entity.Property(e => e.Quantity).HasColumnType("decimal(19, 6)");

                entity.HasOne(d => d.InstallmentType)
                    .WithMany(p => p.InvoiceLines)
                    .HasForeignKey(d => d.InstallmentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceLines_InstallmentType");

                entity.HasOne(d => d.InvoiceSection)
                    .WithMany(p => p.InvoiceLines)
                    .HasForeignKey(d => d.InvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceLines_InvoiceSection");

                entity.HasOne(d => d.InvoicedPeriod)
                    .WithMany(p => p.InvoiceLines)
                    .HasForeignKey(d => d.InvoicedPeriodId)
                    .HasConstraintName("FK_InvoiceLines_InvoicedPeriods");

                entity.HasOne(d => d.Kostensoort)
                    .WithMany(p => p.InvoiceLines)
                    .HasForeignKey(d => d.KostensoortId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceLines_KostenSoort");

                entity.HasOne(d => d.TaxTariffKey)
                    .WithMany(p => p.InvoiceLines)
                    .HasForeignKey(d => d.TaxTariffKeyId)
                    .HasConstraintName("FK_InvoiceLines_TaxTariffKey");
            });

            modelBuilder.Entity<InvoiceMail>(entity =>
            {
                entity.ToTable("InvoiceMail", "dbo");

                entity.HasIndex(e => e.FactuurId)
                    .HasName("UC_InvoiceMail_FactuurId")
                    .IsUnique();

                entity.HasIndex(e => new { e.FactuurId, e.StatusId })
                    .HasName("IX_InvoiceMail_StatusId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.InvoiceMail)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceMail_InvoiceMailStatus");
            });

            modelBuilder.Entity<InvoiceMailStatus>(entity =>
            {
                entity.ToTable("InvoiceMailStatus", "dbo");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceQueue>(entity =>
            {
                entity.ToTable("InvoiceQueue", "dbo");

                entity.HasIndex(e => e.BatchId);

                entity.HasIndex(e => e.ContractId);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateInvoiceCreated).HasColumnType("datetime");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.InvoiceQueue)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceQueue_Contracten");

                entity.HasOne(d => d.InvoiceType)
                    .WithMany(p => p.InvoiceQueue)
                    .HasForeignKey(d => d.InvoiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceQueue_InvoiceType");
            });

            modelBuilder.Entity<InvoiceSectionDetails>(entity =>
            {
                entity.HasKey(e => e.InvoiceSectionId);

                entity.ToTable("InvoiceSectionDetails", "dbo");

                entity.Property(e => e.InvoiceSectionId).ValueGeneratedNever();

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.BeginReadingTl).HasColumnName("BeginReadingTL");

                entity.Property(e => e.BeginReadingTn).HasColumnName("BeginReadingTN");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.EndReadingTl).HasColumnName("EndReadingTL");

                entity.Property(e => e.EndReadingTn).HasColumnName("EndReadingTN");

                entity.HasOne(d => d.InvoiceSection)
                    .WithOne(p => p.InvoiceSectionDetails)
                    .HasForeignKey<InvoiceSectionDetails>(d => d.InvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceSectionDetails_InvoiceSection");
            });

            modelBuilder.Entity<InvoiceSections>(entity =>
            {
                entity.ToTable("InvoiceSections", "dbo");

                entity.HasIndex(e => e.InvoiceId);

                entity.HasIndex(e => new { e.ContractRegelId, e.Id })
                    .HasName("IX_InvoiceSections_ContractRegelId");

                entity.Property(e => e.AmountExcl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountIncl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountVat).HasColumnType("decimal(15, 4)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.InvoiceSections)
                    .HasForeignKey(d => d.InvoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceSections_Invoices");
            });

            modelBuilder.Entity<InvoiceTemplate>(entity =>
            {
                entity.ToTable("InvoiceTemplate", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BackgroundPdfFileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CssFileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.RelationType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.InvoiceType)
                    .WithMany(p => p.InvoiceTemplate)
                    .HasForeignKey(d => d.InvoiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceTemplate_InvoiceType");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.InvoiceTemplate)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__InvoiceTe__Label__23CDFA62");
            });

            modelBuilder.Entity<InvoiceTemplatePage>(entity =>
            {
                entity.ToTable("InvoiceTemplatePage", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.XsltFileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.InvoiceTemplate)
                    .WithMany(p => p.InvoiceTemplatePage)
                    .HasForeignKey(d => d.InvoiceTemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoiceTemplatePage_InvoiceTemplate");
            });

            modelBuilder.Entity<InvoiceType>(entity =>
            {
                entity.ToTable("InvoiceType", "dbo");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionNl)
                    .IsRequired()
                    .HasColumnName("DescriptionNL")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoicedDecisions>(entity =>
            {
                entity.ToTable("InvoicedDecisions", "dbo");

                entity.Property(e => e.EnergymeterId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.DecisionChoice)
                    .WithMany(p => p.InvoicedDecisions)
                    .HasForeignKey(d => d.DecisionChoiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoicedDecisions_InvoiceDecisionChoice");

                entity.HasOne(d => d.InvoiceSection)
                    .WithMany(p => p.InvoicedDecisions)
                    .HasForeignKey(d => d.InvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoicedDecisions_InvoiceSections");

                entity.HasOne(d => d.LastModifiedByUser)
                    .WithMany(p => p.InvoicedDecisions)
                    .HasForeignKey(d => d.LastModifiedByUserId)
                    .HasConstraintName("FK_InvoicedDecisions_Users");
            });

            modelBuilder.Entity<InvoicedPeriods>(entity =>
            {
                entity.ToTable("InvoicedPeriods", "dbo");

                entity.HasIndex(e => e.InvoiceSectionId);

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.Enddate).HasColumnType("date");

                entity.Property(e => e.ProdmanPropositionId).HasMaxLength(36);

                entity.HasOne(d => d.InvoiceSection)
                    .WithMany(p => p.InvoicedPeriods)
                    .HasForeignKey(d => d.InvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoicedPeriods_InvoiceSections");
            });

            modelBuilder.Entity<InvoicedReadings>(entity =>
            {
                entity.ToTable("InvoicedReadings", "dbo");

                entity.HasIndex(e => e.InvoiceSectionId);

                entity.Property(e => e.BeginReadingDate).HasColumnType("date");

                entity.Property(e => e.EndReadingDate).HasColumnType("date");

                entity.Property(e => e.EnergyMeterId)
                    .IsRequired()
                    .HasColumnName("EnergyMeterID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedVolume).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.InvoiceSection)
                    .WithMany(p => p.InvoicedReadings)
                    .HasForeignKey(d => d.InvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoicedReadings_InvoiceSections");
            });

            modelBuilder.Entity<Invoices>(entity =>
            {
                entity.ToTable("Invoices", "dbo");

                entity.HasIndex(e => new { e.Id, e.BillingDocumentInfoId })
                    .HasName("IX_Invoices_Billing_Document_Info_Id");

                entity.HasIndex(e => new { e.Number, e.Id })
                    .HasName("IX_Invoices_Number");

                entity.HasIndex(e => new { e.ContractId, e.Id, e.CorrectionStatusId })
                    .HasName("IX_Invoices_ContractID");

                entity.HasIndex(e => new { e.ReferencedInvoiceId, e.Id, e.CorrectionStatusId })
                    .HasName("IX_Invoices_ReferencedInvoiceId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AccountNumber).HasMaxLength(50);

                entity.Property(e => e.AmountExcl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountIncl).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.AmountVat).HasColumnType("decimal(15, 4)");

                entity.Property(e => e.BillingDocumentInfoId).HasColumnName("Billing_Document_Info_Id");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.DirectDebitDate).HasColumnType("date");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentReference)
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks).HasMaxLength(50);

                entity.HasOne(d => d.BillingDocumentInfo)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.BillingDocumentInfoId)
                    .HasConstraintName("FK_Invoices_Document_Info");

                entity.HasOne(d => d.CorrectionStatus)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.CorrectionStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Invoices_CorrectionStatus");

                entity.HasOne(d => d.CreatedByUser)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.CreatedByUserId)
                    .HasConstraintName("FK_Invoices_Users");

                entity.HasOne(d => d.InvoiceType)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.InvoiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Invoices_InvoiceType");

                entity.HasOne(d => d.PaymentMethod)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.PaymentMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Invoices_PaymentMethod");
            });

            modelBuilder.Entity<InvoicingOnHold>(entity =>
            {
                entity.HasKey(e => new { e.RelatieId, e.ModeratorTypeId });

                entity.ToTable("InvoicingOnHold", "dbo");

                entity.Property(e => e.Inserted).HasColumnType("datetime");

                entity.Property(e => e.Reason).HasMaxLength(1000);

                entity.HasOne(d => d.ModeratorType)
                    .WithMany(p => p.InvoicingOnHold)
                    .HasForeignKey(d => d.ModeratorTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoicingOnHold_OnHoldModeratorType");

                entity.HasOne(d => d.Relatie)
                    .WithMany(p => p.InvoicingOnHold)
                    .HasForeignKey(d => d.RelatieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_InvoicingOnHold_Relaties");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("Job", "dbo");

                entity.HasIndex(e => e.ClassName)
                    .HasName("UK_ClassName")
                    .IsUnique();

                entity.Property(e => e.ClassName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DevEmail)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.DevEmailLastSent).HasColumnType("datetime");

                entity.Property(e => e.DevEmailSendFrequency)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastRun).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<JobQueue>(entity =>
            {
                entity.ToTable("JobQueue", "dbo");

                entity.HasIndex(e => new { e.JobId, e.StatusId })
                    .HasName("IX_JobQueue_StatusId");

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.JobQueue)
                    .HasForeignKey(d => d.JobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobQueue_Job");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.JobQueue)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobQueue_JobQueueStatus");
            });

            modelBuilder.Entity<JobQueueStatus>(entity =>
            {
                entity.ToTable("JobQueueStatus", "dbo");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<KostenSoort>(entity =>
            {
                entity.ToTable("KostenSoort", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Eenheid).HasMaxLength(15);

                entity.Property(e => e.IsPartOfBur).HasColumnName("IsPartOfBUR");

                entity.Property(e => e.Kenmerk)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Omschrijving).HasMaxLength(100);
            });

            modelBuilder.Entity<Kwalificaties>(entity =>
            {
                entity.HasKey(e => e.KwalificatieId)
                    .HasName("PK_Qualificaties");

                entity.ToTable("Kwalificaties", "dbo");

                entity.Property(e => e.KwalificatieId)
                    .HasColumnName("KwalificatieID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Omschrijving)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<LogContractMutations>(entity =>
            {
                entity.ToTable("LogContractMutations", "dbo");

                entity.Property(e => e.FieldName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.NewValue).HasMaxLength(255);

                entity.Property(e => e.OldValue).HasMaxLength(255);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.LogContractMutations)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contracten_LogContractMutations");

                entity.HasOne(d => d.ContractRegel)
                    .WithMany(p => p.LogContractMutations)
                    .HasForeignKey(d => d.ContractRegelId)
                    .HasConstraintName("FK_ContractRegels_LogContractMutations");
            });

            modelBuilder.Entity<LoggedProcedureCall>(entity =>
            {
                entity.HasKey(e => e.Procname);

                entity.ToTable("LoggedProcedureCall", "dbo");

                entity.Property(e => e.Procname)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApplicationName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LastUsed).HasColumnType("datetime");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Mep>(entity =>
            {
                entity.HasKey(e => e.Mep1);

                entity.ToTable("MEP", "dbo");

                entity.Property(e => e.Mep1)
                    .HasColumnName("MEP")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Creatiedatum).HasColumnType("datetime");

                entity.Property(e => e.Omschrijving)
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MeterdataSelection>(entity =>
            {
                entity.ToTable("Meterdata_Selection", "dbo");

                entity.HasIndex(e => e.MeterId);

                entity.HasIndex(e => new { e.ContractRegelId, e.Metertype, e.MeetDatum, e.RejectionStatus });

                entity.HasIndex(e => new { e.EanGebruiker, e.ReadingType, e.MeetDatum, e.RejectionStatus, e.Invoiced, e.Corrected })
                    .HasName("IX_Meterdata_Selection_EanGebruiker_ReadingType_MeetDatum_more");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BronId)
                    .IsRequired()
                    .HasColumnName("BronID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ClassNm)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.DateTimeInserted).HasColumnType("datetime");

                entity.Property(e => e.DateTimeModified).HasColumnType("datetime");

                entity.Property(e => e.EanGebruiker)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.KwalificatieId).HasColumnName("KwalificatieID");

                entity.Property(e => e.MeetDatum).HasColumnType("datetime");

                entity.Property(e => e.MeterId).HasColumnName("MeterID");

                entity.Property(e => e.Metertype)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ReadingType)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RedenId)
                    .HasColumnName("RedenID")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RejectionStatus).HasColumnName("Rejection_Status");

                entity.Property(e => e.RelateId).HasColumnName("RelateID");

                entity.Property(e => e.RequestId).HasColumnName("RequestID");

                entity.HasOne(d => d.Meter)
                    .WithMany(p => p.MeterdataSelection)
                    .HasForeignKey(d => d.MeterId)
                    .HasConstraintName("FK_Meterdata_Selection_Meters");

                entity.HasOne(d => d.Reden)
                    .WithMany(p => p.MeterdataSelection)
                    .HasForeignKey(d => d.RedenId)
                    .HasConstraintName("FK_Meterdata_Selection_Redenen");
            });

            modelBuilder.Entity<Meters>(entity =>
            {
                entity.HasKey(e => e.MeterId);

                entity.ToTable("Meters", "dbo");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_Meters");

                entity.HasIndex(e => e.Meternummer);

                entity.HasIndex(e => new { e.EanId, e.Meternummer })
                    .HasName("UC_Meters_ean_nummer")
                    .IsUnique();

                entity.Property(e => e.MeterId).HasColumnName("MeterID");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasColumnName("EanID");

                entity.Property(e => e.EindDatum).HasColumnType("datetime");

                entity.Property(e => e.Meternummer)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Opmerking)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDatum).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Ean)
                    .WithMany(p => p.Meters)
                    .HasForeignKey(d => d.EanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Meters_EanBoek");
            });

            modelBuilder.Entity<MonthlyAmountHistory>(entity =>
            {
                entity.ToTable("MonthlyAmountHistory", "dbo");

                entity.HasIndex(e => e.ContractRegelId)
                    .HasName("UX_MonthlyAmountHistory_ContractRegelId")
                    .IsUnique();

                entity.HasIndex(e => new { e.ModeratorTypeId, e.IsActive, e.ContractRegelId })
                    .HasName("IX_MonthlyAmountHistory_ContractRegelId");

                entity.HasIndex(e => new { e.ContractRegelId, e.BedragIncl, e.IsActive, e.ModifiedOn })
                    .HasName("IX_MonthlyAmountHistory_IsActive_ModifiedOn_incl");

                entity.Property(e => e.BedragBtw).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.BedragExcl).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.BedragIncl).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.ModeratorTypeId).HasColumnName("ModeratorTypeID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ContractRegel)
                    .WithOne(p => p.MonthlyAmountHistory)
                    .HasForeignKey<MonthlyAmountHistory>(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MonthlyAmountHistory_ContractRegels");

                entity.HasOne(d => d.ModeratorType)
                    .WithMany(p => p.MonthlyAmountHistory)
                    .HasForeignKey(d => d.ModeratorTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MonthlyAmountHistory_Moderator");
            });

            modelBuilder.Entity<MonthlyAmountModeratorType>(entity =>
            {
                entity.ToTable("MonthlyAmountModeratorType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Netbeheerders>(entity =>
            {
                entity.HasKey(e => e.EanNb);

                entity.ToTable("Netbeheerders", "dbo");

                entity.Property(e => e.EanNb)
                    .HasColumnName("EanNB")
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.AdresId).HasColumnName("AdresID");

                entity.Property(e => e.BtwNummer)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPersoon)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EmailAlgemeen)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailLeverancier)
                    .HasColumnName("EMailLeverancier")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailNb)
                    .HasColumnName("EmailNB")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailOverige)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailTerugswitch)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Naam)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opmerking)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RekeningNaam)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RekeningNummer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RekeningPlaats)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shortname)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TelKlantenservice)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TelService)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TelStoring)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Telefoon)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.Website).HasMaxLength(50);

                entity.HasOne(d => d.Adres)
                    .WithMany(p => p.Netbeheerders)
                    .HasForeignKey(d => d.AdresId)
                    .HasConstraintName("FK_Netbeheerders_Adressen");
            });

            modelBuilder.Entity<OnHoldModeratorType>(entity =>
            {
                entity.ToTable("OnHoldModeratorType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<OtherSuppliersInfo>(entity =>
            {
                entity.ToTable("OtherSuppliersInfo", "dbo");

                entity.Property(e => e.Marktpartij)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.MarktpartijEancode)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<PaymentDetails>(entity =>
            {
                entity.ToTable("PaymentDetails", "dbo");

                entity.HasIndex(e => new { e.RelatieId, e.ActiveTo });

                entity.HasIndex(e => new { e.RelatieId, e.Iban })
                    .HasName("IX_PaymentDetails_IBAN_incl_RelatieId");

                entity.HasIndex(e => new { e.RelatieId, e.Iban, e.ActiveTo })
                    .HasName("IX_PaymentDetails_ActiveTo_Iban_RelatieId");

                entity.Property(e => e.AccountHolderName).HasMaxLength(50);

                entity.Property(e => e.ActiveTo).HasColumnType("date");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.Iban)
                    .HasColumnName("IBAN")
                    .HasMaxLength(34);

                entity.HasOne(d => d.Relatie)
                    .WithMany(p => p.PaymentDetails)
                    .HasForeignKey(d => d.RelatieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PaymentDetails_Relaties");
            });

            modelBuilder.Entity<PaymentMethod>(entity =>
            {
                entity.ToTable("PaymentMethod", "dbo");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Penalties>(entity =>
            {
                entity.HasKey(e => e.PenaltyId);

                entity.ToTable("Penalties", "dbo");

                entity.Property(e => e.PenaltyAmount).HasColumnType("money");
            });

            modelBuilder.Entity<PenaltyCategory>(entity =>
            {
                entity.ToTable("PenaltyCategory", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreationDate).HasColumnType("date");

                entity.Property(e => e.PenaltyCategoryName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PrConnectionView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PR_ConnectionView", "dbo");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ContractRegelEind).HasColumnType("date");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.ContractRegelStart).HasColumnType("date");

                entity.Property(e => e.ContractStart).HasColumnType("datetime");

                entity.Property(e => e.EanGebruiker)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EanId).HasColumnName("EanID");

                entity.Property(e => e.Jvh).HasColumnName("JVH");

                entity.Property(e => e.Jvl).HasColumnName("JVL");

                entity.Property(e => e.Naam)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Omschrijving).HasMaxLength(50);

                entity.Property(e => e.SegmentId).HasColumnName("SegmentID");

                entity.Property(e => e.SegmentNaam).HasMaxLength(30);

                entity.Property(e => e.VadresId).HasColumnName("VAdresID");
            });

            modelBuilder.Entity<PrSwitchRegelView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PR_SwitchRegel_View", "dbo");

                entity.Property(e => e.Bericht)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.DossierId)
                    .HasColumnName("DossierID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IngediendDatum).HasColumnType("datetime");

                entity.Property(e => e.Regel)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasMaxLength(30);

                entity.Property(e => e.SwitchRegelId).HasColumnName("SwitchRegelID");

                entity.Property(e => e.Switchdatum).HasColumnType("date");

                entity.Property(e => e.Voorsteldatum).HasColumnType("date");
            });

            modelBuilder.Entity<PrSwitchesViewBeginSwitchRegels>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PR_Switches_View_BeginSwitchRegels", "dbo");

                entity.Property(e => e.BeginStatus).HasMaxLength(30);

                entity.Property(e => e.BeginSwitchRegelId).HasColumnName("BeginSwitchRegelID");

                entity.Property(e => e.BeginSwitchdatum).HasColumnType("date");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.SwitchId).HasColumnName("SwitchID");
            });

            modelBuilder.Entity<PrSwitchesViewEindSwitchRegels>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PR_Switches_View_EindSwitchRegels", "dbo");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.EindStatus).HasMaxLength(30);

                entity.Property(e => e.EindSwitchRegelId).HasColumnName("EindSwitchRegelID");

                entity.Property(e => e.EindSwitchdatum).HasColumnType("date");

                entity.Property(e => e.SwitchId).HasColumnName("SwitchID");
            });

            modelBuilder.Entity<ProcLog>(entity =>
            {
                entity.ToTable("ProcLog", "dbo");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Errno).HasColumnName("errno");

                entity.Property(e => e.Msg)
                    .HasColumnName("msg")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Objname)
                    .HasColumnName("objname")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Rowcnt).HasColumnName("rowcnt");

                entity.Property(e => e.Rundate).HasColumnName("rundate");

                entity.Property(e => e.Runuser)
                    .HasColumnName("runuser")
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Servername)
                    .IsRequired()
                    .HasColumnName("servername")
                    .HasMaxLength(128);

                entity.Property(e => e.Stepname)
                    .HasColumnName("stepname")
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProcedureCall>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ProcedureCall", "dbo");

                entity.Property(e => e.ApplicationName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastUsed).HasColumnType("datetime");

                entity.Property(e => e.LoginName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Procname)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Profielen>(entity =>
            {
                entity.HasKey(e => e.ProfielId);

                entity.ToTable("Profielen", "dbo");

                entity.HasIndex(e => e.ProfileCode)
                    .HasName("UC_ProfileCode")
                    .IsUnique();

                entity.HasIndex(e => e.SegmentId)
                    .HasName("IX_Profielen");

                entity.Property(e => e.ProfielId)
                    .HasColumnName("ProfielID")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Omschrijving).HasMaxLength(50);

                entity.Property(e => e.SegmentId).HasColumnName("SegmentID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Segment)
                    .WithMany(p => p.Profielen)
                    .HasForeignKey(d => d.SegmentId)
                    .HasConstraintName("FK_Profielen_Segmenten");
            });

            modelBuilder.Entity<ReclaimVoucherCancellationMoment>(entity =>
            {
                entity.ToTable("ReclaimVoucherCancellationMoment", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Reconciliations>(entity =>
            {
                entity.ToTable("Reconciliations", "dbo");

                entity.HasIndex(e => e.InvoiceSectionId);

                entity.HasIndex(e => new { e.Id, e.AdvanceInvoiceSectionId, e.CounteredByInvoiceId })
                    .HasName("IX_Reconciliations_AdvanceInvoiceSectionId");

                entity.HasOne(d => d.AdvanceInvoiceSection)
                    .WithMany(p => p.Reconciliations)
                    .HasForeignKey(d => d.AdvanceInvoiceSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Reconciliations_AdvanceInvoiceSections");

                entity.HasOne(d => d.InvoiceSection)
                    .WithMany(p => p.Reconciliations)
                    .HasForeignKey(d => d.InvoiceSectionId)
                    .HasConstraintName("FK_Reconciliations_InvoiceSections");
            });

            modelBuilder.Entity<Redenen>(entity =>
            {
                entity.HasKey(e => e.RedenId);

                entity.ToTable("Redenen", "dbo");

                entity.Property(e => e.RedenId)
                    .HasColumnName("RedenID")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Omschrijving)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<RegistersType>(entity =>
            {
                entity.ToTable("RegistersType", "dbo");
            });

            modelBuilder.Entity<RelatieDocumenten>(entity =>
            {
                entity.ToTable("RelatieDocumenten", "dbo");

                entity.HasIndex(e => e.RelatieId);

                entity.Property(e => e.ExternalReference)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ExternalReferenceType)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.FileDate).HasColumnType("datetime");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.TypeDescription)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Relatie)
                    .WithMany(p => p.RelatieDocumenten)
                    .HasForeignKey(d => d.RelatieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RelatieDocumenten_RelatieDocumenten");
            });

            modelBuilder.Entity<Relaties>(entity =>
            {
                entity.HasKey(e => e.RelatieId);

                entity.ToTable("Relaties", "dbo");

                entity.HasIndex(e => e.Categorie)
                    .HasName("IX_Relaties");

                entity.HasIndex(e => e.Email);

                entity.HasIndex(e => e.Gsm);

                entity.HasIndex(e => e.Kvk);

                entity.HasIndex(e => e.LeadRelatieId);

                entity.HasIndex(e => e.NutsHomeCustomerNumber);

                entity.HasIndex(e => e.Telefoon)
                    .HasName("IX_Relaties_Phone");

                entity.HasIndex(e => new { e.Categorie, e.Bedrijfsnaam });

                entity.HasIndex(e => new { e.Categorie, e.Naam });

                entity.HasIndex(e => new { e.LeadRelatieId, e.RelatieId })
                    .HasName("IX_Relaties_RelatieID_LeadRelatieID");

                entity.HasIndex(e => new { e.Bedrijfsnaam, e.Naam, e.LabelCode });

                entity.Property(e => e.RelatieId).HasColumnName("RelatieID");

                entity.Property(e => e.Aaid).HasColumnName("AAid");

                entity.Property(e => e.Bedrijfsnaam).HasMaxLength(50);

                entity.Property(e => e.Btwnummer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Categorie)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Geboortedatum).HasColumnType("datetime");

                entity.Property(e => e.Geslacht)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Gsm)
                    .HasColumnName("GSM")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Kvk)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastKto)
                    .HasColumnName("LastKTO")
                    .HasColumnType("date");

                entity.Property(e => e.LastKto1)
                    .HasColumnName("Last_KTO")
                    .HasColumnType("date");

                entity.Property(e => e.LeadRelatieId).HasColumnName("LeadRelatieID");

                entity.Property(e => e.Memo).IsUnicode(false);

                entity.Property(e => e.Naam)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opmerking)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Telefoon)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.Tussenvoegsel)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Voorletters)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.Relaties)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ScheduledIncentivePaymentTask>(entity =>
            {
                entity.HasKey(e => new { e.ContractId, e.ScheduledFor })
                    .HasName("PK_CScheduledIncentivePaymentTask");

                entity.ToTable("ScheduledIncentivePaymentTask", "dbo");

                entity.Property(e => e.ScheduledFor).HasColumnType("date");
            });

            modelBuilder.Entity<Segmenten>(entity =>
            {
                entity.HasKey(e => e.SegmentId);

                entity.ToTable("Segmenten", "dbo");

                entity.Property(e => e.SegmentId)
                    .HasColumnName("SegmentID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.SegmentNaam)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<SendEmailReclaimVoucher>(entity =>
            {
                entity.ToTable("SendEmailReclaimVoucher", "dbo");

                entity.HasIndex(e => e.ReferenceId)
                    .HasName("UX_ReferenceId")
                    .IsUnique();

                entity.HasIndex(e => e.VoucherId)
                    .HasName("UX_VoucherId")
                    .IsUnique();

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.CancellationMoment)
                    .WithMany(p => p.SendEmailReclaimVoucher)
                    .HasForeignKey(d => d.CancellationMomentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SendEmailReclaimVoucher_ReclaimVoucherCancellationMoment");
            });

            modelBuilder.Entity<SendRenewableContract>(entity =>
            {
                entity.ToTable("SendRenewableContract", "dbo");

                entity.HasIndex(e => e.ContractId)
                    .HasName("IX_FK_SendRenewableContract_Contracten");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.SendRenewableContract)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SendRenewableContract_Contracten");
            });

            modelBuilder.Entity<SingleTransactionConfiguration>(entity =>
            {
                entity.HasKey(e => e.StconfigurationId);

                entity.ToTable("SingleTransactionConfiguration", "dbo");

                entity.HasIndex(e => e.InvoiceTypeId)
                    .HasName("UIX_SingleTransactionConfiguration_InvoiceTypeId")
                    .IsUnique();

                entity.Property(e => e.StconfigurationId)
                    .HasColumnName("STConfigurationID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.InvoiceType)
                    .WithOne(p => p.SingleTransactionConfiguration)
                    .HasForeignKey<SingleTransactionConfiguration>(d => d.InvoiceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SingleTransactionConfiguration_InvoiceType");
            });

            modelBuilder.Entity<Smslog>(entity =>
            {
                entity.HasKey(e => e.SmsId);

                entity.ToTable("SMSlog", "dbo");

                entity.HasIndex(e => new { e.DateTimeSent, e.ResultMessage })
                    .HasName("IX_SMSlog_ResultMessage");

                entity.Property(e => e.SmsId).HasColumnName("SMS_ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.DateTimeSent).HasColumnType("smalldatetime");

                entity.Property(e => e.Originator)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.PhonenumberUsed)
                    .IsRequired()
                    .HasColumnName("Phonenumber_used")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RelatieId).HasColumnName("RelatieID");

                entity.Property(e => e.ResultMessage)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.SmsMessage)
                    .HasColumnName("SMS_Message")
                    .HasMaxLength(160)
                    .IsUnicode(false);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.Smslog)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK_SMSlog_Contracten");

                entity.HasOne(d => d.Relatie)
                    .WithMany(p => p.Smslog)
                    .HasForeignKey(d => d.RelatieId)
                    .HasConstraintName("FK_SMSlog_Relaties");
            });

            modelBuilder.Entity<StamdataRequest>(entity =>
            {
                entity.ToTable("StamdataRequest", "dbo");

                entity.HasIndex(e => new { e.Afgewezen, e.AfgewezenDatum });

                entity.HasIndex(e => new { e.Ingediend, e.CreatieDatum })
                    .HasName("IX_StamdataRequest_Ingediend_Creatiedatum");

                entity.HasIndex(e => new { e.Ingediend, e.IngediendDatum });

                entity.Property(e => e.StamdataRequestId).HasColumnName("StamdataRequestID");

                entity.Property(e => e.AfgewezenDatum).HasColumnType("datetime");

                entity.Property(e => e.AfwijsReden)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasColumnName("EanID");

                entity.Property(e => e.IngediendDatum).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.HasOne(d => d.Ean)
                    .WithMany(p => p.StamdataRequest)
                    .HasForeignKey(d => d.EanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StamdataRequest_EanBoek");
            });

            modelBuilder.Entity<StartOfUninvoicePeriod>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("StartOfUninvoicePeriod", "dbo");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.EndSupply).HasColumnType("date");

                entity.Property(e => e.StartOfPeriod).HasColumnType("date");
            });

            modelBuilder.Entity<SwitchCorrectieTypes>(entity =>
            {
                entity.ToTable("SwitchCorrectieTypes", "dbo");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Beschrijving)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.Naam)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();
            });

            modelBuilder.Entity<SwitchRegels>(entity =>
            {
                entity.HasKey(e => e.SwitchRegelId);

                entity.ToTable("SwitchRegels", "dbo");

                entity.HasIndex(e => new { e.Voorsteldatum, e.Status })
                    .HasName("IX_SwitchRegels_Status_incl_Voorsteldatum");

                entity.HasIndex(e => new { e.SwitchRegelId, e.Correctie, e.Switchdatum })
                    .HasName("IX_SwitchRegels_Correctie_SwitchDatum");

                entity.HasIndex(e => new { e.SwitchRegelId, e.Bericht, e.Status, e.Switchdatum })
                    .HasName("IX_SwitchRegels_Bericht_Status_SwitchDatum_incl_SwitchRegelId");

                entity.HasIndex(e => new { e.SwitchRegelId, e.Status, e.Correctie, e.Switchdatum })
                    .HasName("IX_SwitchRegels_Status_Correctie");

                entity.HasIndex(e => new { e.SwitchRegelId, e.Switchdatum, e.Correctie, e.Opmerking, e.Bericht, e.Status, e.Voorsteldatum })
                    .HasName("IX_SwitchRegels_Bericht_Status");

                entity.Property(e => e.SwitchRegelId).HasColumnName("SwitchRegelID");

                entity.Property(e => e.Bericht)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Contractregelid).HasColumnName("contractregelid");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.DossierId)
                    .HasColumnName("DossierID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EchId).HasColumnName("EchID");

                entity.Property(e => e.EdsnrejectionCount).HasColumnName("EDSNRejectionCount");

                entity.Property(e => e.IngediendDatum).HasColumnType("datetime");

                entity.Property(e => e.Opmerking).HasColumnType("nvarchar(max)");

                entity.Property(e => e.Status).HasMaxLength(30);

                entity.Property(e => e.Switchdatum).HasColumnType("date");

                entity.Property(e => e.Voorsteldatum).HasColumnType("date");

                entity.HasOne(d => d.CorrectieNavigation)
                    .WithMany(p => p.SwitchRegels)
                    .HasForeignKey(d => d.Correctie)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SwitchRegels_SwitchCorrectieTypes");
            });

            modelBuilder.Entity<SwitchRegelsLogs>(entity =>
            {
                entity.ToTable("SwitchRegelsLogs", "dbo");

                entity.HasIndex(e => e.ContractRegelId);

                entity.HasIndex(e => e.SwitchRegelId);

                entity.Property(e => e.BerichtNew)
                    .HasColumnName("Bericht_new")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.BerichtOld)
                    .HasColumnName("Bericht_old")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ChangeType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.CorrectieNew).HasColumnName("Correctie_new");

                entity.Property(e => e.CorrectieOld).HasColumnName("Correctie_old");

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.EchIdNew).HasColumnName("EchID_new");

                entity.Property(e => e.EchIdOld).HasColumnName("EchID_old");

                entity.Property(e => e.IngediendNew).HasColumnName("Ingediend_new");

                entity.Property(e => e.IngediendOld).HasColumnName("Ingediend_old");

                entity.Property(e => e.OpmerkingNew).HasColumnName("Opmerking_new");

                entity.Property(e => e.OpmerkingOld).HasColumnName("Opmerking_old");

                entity.Property(e => e.StatusNew)
                    .HasColumnName("Status_new")
                    .HasMaxLength(30);

                entity.Property(e => e.StatusOld)
                    .HasColumnName("Status_old")
                    .HasMaxLength(30);

                entity.Property(e => e.SwitchId).HasColumnName("SwitchID");

                entity.Property(e => e.SwitchRegelId).HasColumnName("SwitchRegelID");

                entity.Property(e => e.SwitchdatumNew)
                    .HasColumnName("Switchdatum_new")
                    .HasColumnType("datetime");

                entity.Property(e => e.SwitchdatumOld)
                    .HasColumnName("Switchdatum_old")
                    .HasColumnType("datetime");

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.VoorsteldatumNew)
                    .HasColumnName("Voorsteldatum_new")
                    .HasColumnType("datetime");

                entity.Property(e => e.VoorsteldatumOld)
                    .HasColumnName("Voorsteldatum_old")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.ContractRegel)
                    .WithMany(p => p.SwitchRegelsLogs)
                    .HasForeignKey(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SwitchRegelsLogs_ContractRegels");
            });

            modelBuilder.Entity<Switches>(entity =>
            {
                entity.HasKey(e => e.SwitchId);

                entity.ToTable("Switches", "dbo");

                entity.HasIndex(e => new { e.ContractRegelId, e.BeginSwitchregelId, e.EindSwitchregelId })
                    .HasName("IX_Switch_ContractRegelID_SwitchregelsIDs");

                entity.HasIndex(e => new { e.SwitchId, e.ContractRegelId, e.BeginSwitchregelId })
                    .HasName("IX_Switches_BeginSwitchregelID");

                entity.HasIndex(e => new { e.SwitchId, e.ContractRegelId, e.EindSwitchregelId })
                    .HasName("IX_Switches_EindSwitchregelID");

                entity.Property(e => e.SwitchId).HasColumnName("SwitchID");

                entity.Property(e => e.BeginSwitchregelId).HasColumnName("BeginSwitchregelID");

                entity.Property(e => e.ContractRegelId).HasColumnName("ContractRegelID");

                entity.Property(e => e.CreatieDatum).HasColumnType("datetime");

                entity.Property(e => e.EindSwitchregelId).HasColumnName("EindSwitchregelID");

                entity.Property(e => e.Opmerking)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.BeginSwitchregel)
                    .WithMany(p => p.SwitchesBeginSwitchregel)
                    .HasForeignKey(d => d.BeginSwitchregelId)
                    .HasConstraintName("FK_Switches_SwitchRegels");

                entity.HasOne(d => d.ContractRegel)
                    .WithMany(p => p.Switches)
                    .HasForeignKey(d => d.ContractRegelId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Switches_ContractRegels");

                entity.HasOne(d => d.EindSwitchregel)
                    .WithMany(p => p.SwitchesEindSwitchregel)
                    .HasForeignKey(d => d.EindSwitchregelId)
                    .HasConstraintName("FK_Switches_SwitchRegels1");
            });

            modelBuilder.Entity<TempAcquisitionInfo>(entity =>
            {
                entity.ToTable("TEMP_AcquisitionInfo", "dbo");

                entity.Property(e => e.LastMeterReadingAcquisitionDate).HasColumnType("datetime");

                entity.Property(e => e.YearlyBillingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TempAcquisitionInfoConnection>(entity =>
            {
                entity.ToTable("TEMP_AcquisitionInfo_Connection", "dbo");

                entity.HasIndex(e => e.AcquisitionInfoId);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.PhysicalStatus)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ProductType)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.AcquisitionInfo)
                    .WithMany(p => p.TempAcquisitionInfoConnection)
                    .HasForeignKey(d => d.AcquisitionInfoId)
                    .HasConstraintName("FK_TEMP_AcquisitionInfo_Connection_TEMP_AcquisitionInfo");
            });

            modelBuilder.Entity<TempAcquisitionInfoRegister>(entity =>
            {
                entity.ToTable("TEMP_AcquisitionInfo_Register", "dbo");

                entity.HasIndex(e => e.ConnectionId);

                entity.Property(e => e.MeteringDirection)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.RegisterNr)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.TariffType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.HasOne(d => d.Connection)
                    .WithMany(p => p.TempAcquisitionInfoRegister)
                    .HasForeignKey(d => d.ConnectionId)
                    .HasConstraintName("FK_TEMP_AcquisitionInfo_Register_TEMP_AcquisitionInfo_Connection");
            });

            modelBuilder.Entity<TimeslicesSourceMasterdata>(entity =>
            {
                entity.ToTable("Timeslices_Source_Masterdata", "dbo");

                entity.HasIndex(e => new { e.ConnectEan, e.BeginDate })
                    .HasName("IX_TimeslicesSourceMasterdata_Begindate_Connectean");

                entity.HasIndex(e => new { e.CapTarCode, e.GridArea, e.EdsnaddressZipcode, e.SegmentId, e.BeginDate })
                    .HasName("IX_TimeslicesSourceMasterdata_SegmentID_BeginDate");

                entity.HasIndex(e => new { e.EndDate, e.MutationDate, e.RelateId, e.ConnectEan, e.ProfileCategory })
                    .HasName("IX_TimeslicesSourceMasterdata_ConnectEAN_ProfileID");

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.CapTarCode)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ClassName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ConnectEan)
                    .IsRequired()
                    .HasColumnName("ConnectEAN")
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.EdsnaddressBuildingNr).HasColumnName("EDSNAddress_BuildingNr");

                entity.Property(e => e.EdsnaddressCityName)
                    .HasColumnName("EDSNAddress_CityName")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressCountry)
                    .HasColumnName("EDSNAddress_Country")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressExBuildingNr)
                    .HasColumnName("EDSNAddress_ExBuildingNr")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressStreetName)
                    .HasColumnName("EDSNAddress_StreetName")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressZipcode)
                    .HasColumnName("EDSNAddress_ZIPCode")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.EnergyFlowDirection)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GridArea)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.GridOperator)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.MarketSegment)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MeterEnergymeterId)
                    .HasColumnName("Meter_EnergymeterId")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MeterMultiplicationFactor).HasColumnName("Meter_MultiplicationFactor");

                entity.Property(e => e.MeterNrOfDigits).HasColumnName("Meter_NrOfDigits");

                entity.Property(e => e.MeteringMethod)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MutationDate).HasColumnType("datetime");

                entity.Property(e => e.PhysicalStatus)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ProfileCategory)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SegmentId).HasColumnName("SegmentID");
            });

            modelBuilder.Entity<ToBeRemovedTimeslicesSourceMasterdata>(entity =>
            {
                entity.ToTable("_TO_BE_REMOVED_Timeslices_Source_Masterdata", "dbo");

                entity.Property(e => e.BeginDate).HasColumnType("date");

                entity.Property(e => e.CapTarCode)
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ClassName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ConnectEan)
                    .IsRequired()
                    .HasColumnName("ConnectEAN")
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.EdsnaddressBuildingNr).HasColumnName("EDSNAddress_BuildingNr");

                entity.Property(e => e.EdsnaddressCityName)
                    .HasColumnName("EDSNAddress_CityName")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressCountry)
                    .HasColumnName("EDSNAddress_Country")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressExBuildingNr)
                    .HasColumnName("EDSNAddress_ExBuildingNr")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressStreetName)
                    .HasColumnName("EDSNAddress_StreetName")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.EdsnaddressZipcode)
                    .HasColumnName("EDSNAddress_ZIPCode")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.EnergyFlowDirection)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.GridArea)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.GridOperator)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.MarketSegment)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MeterEnergymeterId)
                    .HasColumnName("Meter_EnergymeterId")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MeterHasTariffTypeT).HasColumnName("Meter_HasTariffTypeT");

                entity.Property(e => e.MeterMultiplicationFactor).HasColumnName("Meter_MultiplicationFactor");

                entity.Property(e => e.MeterNrOfDigits).HasColumnName("Meter_NrOfDigits");

                entity.Property(e => e.MeterNrOfRegisters).HasColumnName("Meter_NrOfRegisters");

                entity.Property(e => e.MeteringMethod)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.MutationDate).HasColumnType("datetime");

                entity.Property(e => e.PhysicalStatus)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ProfileCategory)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SegmentId).HasColumnName("SegmentID");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("Users", "dbo");

                entity.HasIndex(e => e.TechnicalKey)
                    .HasName("IX_Users_TechnicalName");

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.TechnicalKey)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VolumeDeterminationFactors>(entity =>
            {
                entity.ToTable("VolumeDeterminationFactors", "dbo");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.EnergyMeterTemperatureCorrection)
                    .IsRequired()
                    .HasColumnName("EnergyMeter_TemperatureCorrection")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Factor).HasColumnType("decimal(8, 6)");

                entity.Property(e => e.StartDate).HasColumnType("date");
            });

            modelBuilder.Entity<VwInvoiceKeys>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwInvoiceKeys", "dbo");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ContractantId).HasColumnName("ContractantID");

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);
            });

            modelBuilder.HasSequence<int>("ContractIdSequence", "dbo")
                .StartsAt(9628201)
                .IncrementsBy(2);

            modelBuilder.HasSequence("InvoiceId", "dbo").StartsAt(59575808);

            modelBuilder.HasSequence("InvoiceNumber", "dbo").StartsAt(15388487);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
