﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Eosdata
    {
        public int Id { get; set; }
        public string Ean { get; set; }
        public DateTime Eosdate { get; set; }
        public bool IsUploaded { get; set; }
        public DateTime? UploadedDate { get; set; }
        public bool IsRejected { get; set; }
        public string Rejection { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
