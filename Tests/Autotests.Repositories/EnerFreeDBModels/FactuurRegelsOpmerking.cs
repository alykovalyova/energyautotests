﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class FactuurRegelsOpmerking
    {
        public int FactuurRegelId { get; set; }
        public string Opmerking { get; set; }
    }
}
