﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceDecision
    {
        public int Id { get; set; }
        public int DecisionChoiceId { get; set; }
        public int ContractRegelId { get; set; }
        public DateTime? LastModified { get; set; }
        public string EnergymeterId { get; set; }
        public int? LastModifiedByUserId { get; set; }
    }
}
