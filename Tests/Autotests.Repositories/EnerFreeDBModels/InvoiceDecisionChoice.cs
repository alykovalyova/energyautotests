﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceDecisionChoice
    {
        public InvoiceDecisionChoice()
        {
            InvoicedDecisions = new HashSet<InvoicedDecisions>();
        }

        public int DecisionChoiceId { get; set; }
        public int DecisionTypeId { get; set; }
        public string Name { get; set; }
        public bool SingleInvoiceOnly { get; set; }

        public virtual InvoiceDecisionType DecisionType { get; set; }
        public virtual ICollection<InvoicedDecisions> InvoicedDecisions { get; set; }
    }
}
