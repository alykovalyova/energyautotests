﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceExceptions
    {
        public InvoiceExceptions()
        {
            ExceptionQueue = new HashSet<ExceptionQueue>();
        }

        public int ExceptionNr { get; set; }
        public string ExceptionMessage { get; set; }
        public bool? ExceptionIgnorable { get; set; }
        public DateTime? ExceptionDateCreated { get; set; }

        public virtual ICollection<ExceptionQueue> ExceptionQueue { get; set; }
    }
}
