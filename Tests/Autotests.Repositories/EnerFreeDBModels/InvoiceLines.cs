﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceLines
    {
        public long Id { get; set; }
        public int InvoiceSectionId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal AmountExcl { get; set; }
        public decimal AmountVat { get; set; }
        public decimal AmountIncl { get; set; }
        public short? PerformanceYear { get; set; }
        public short KostensoortId { get; set; }
        public byte? TaxTariffKeyId { get; set; }
        public byte InstallmentTypeId { get; set; }
        public byte Btwcode { get; set; }
        public int? InvoicedPeriodId { get; set; }

        public virtual InstallmentType InstallmentType { get; set; }
        public virtual InvoiceSections InvoiceSection { get; set; }
        public virtual InvoicedPeriods InvoicedPeriod { get; set; }
        public virtual KostenSoort Kostensoort { get; set; }
        public virtual BillingTaxTariffKey TaxTariffKey { get; set; }
    }
}
