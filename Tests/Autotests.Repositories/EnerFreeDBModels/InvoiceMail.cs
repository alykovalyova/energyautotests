﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceMail
    {
        public Guid Id { get; set; }
        public int FactuurId { get; set; }
        public byte StatusId { get; set; }
        public DateTime Modified { get; set; }
        public byte InvoiceTypeId { get; set; }
        public int NutsHomeCustomerNumber { get; set; }
        public bool SendMail { get; set; }
        public int ContractId { get; set; }

        public virtual InvoiceMailStatus Status { get; set; }
    }
}
