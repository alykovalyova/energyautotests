﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceQueue
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int BatchId { get; set; }
        public int? FactuurId { get; set; }
        public byte InvoiceTypeId { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateInvoiceCreated { get; set; }

        public virtual Contracten Contract { get; set; }
        public virtual InvoiceType InvoiceType { get; set; }
    }
}
