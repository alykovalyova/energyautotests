﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceSectionDetails
    {
        public int InvoiceSectionId { get; set; }
        public long? Ean { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? BeginReadingN { get; set; }
        public int? BeginReadingL { get; set; }
        public int? BeginReadingTn { get; set; }
        public int? BeginReadingTl { get; set; }
        public int? EndReadingN { get; set; }
        public int? EndReadingL { get; set; }
        public int? EndReadingTn { get; set; }
        public int? EndReadingTl { get; set; }

        public virtual InvoiceSections InvoiceSection { get; set; }
    }
}
