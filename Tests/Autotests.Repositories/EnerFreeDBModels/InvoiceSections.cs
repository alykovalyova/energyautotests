﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceSections
    {
        public InvoiceSections()
        {
            InvoiceLines = new HashSet<InvoiceLines>();
            InvoicedDecisions = new HashSet<InvoicedDecisions>();
            InvoicedPeriods = new HashSet<InvoicedPeriods>();
            InvoicedReadings = new HashSet<InvoicedReadings>();
            Reconciliations = new HashSet<Reconciliations>();
        }

        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int? ContractRegelId { get; set; }
        public decimal AmountExcl { get; set; }
        public decimal AmountVat { get; set; }
        public decimal AmountIncl { get; set; }

        public virtual Invoices Invoice { get; set; }
        public virtual InvoiceSectionDetails InvoiceSectionDetails { get; set; }
        public virtual ICollection<InvoiceLines> InvoiceLines { get; set; }
        public virtual ICollection<InvoicedDecisions> InvoicedDecisions { get; set; }
        public virtual ICollection<InvoicedPeriods> InvoicedPeriods { get; set; }
        public virtual ICollection<InvoicedReadings> InvoicedReadings { get; set; }
        public virtual ICollection<Reconciliations> Reconciliations { get; set; }
    }
}
