﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceTemplate
    {
        public InvoiceTemplate()
        {
            InvoiceTemplatePage = new HashSet<InvoiceTemplatePage>();
        }

        public int Id { get; set; }
        public byte InvoiceTypeId { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public string CssFileName { get; set; }
        public string BackgroundPdfFileName { get; set; }
        public string RelationType { get; set; }
        public byte LabelCode { get; set; }

        public virtual InvoiceType InvoiceType { get; set; }
        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual ICollection<InvoiceTemplatePage> InvoiceTemplatePage { get; set; }
    }
}
