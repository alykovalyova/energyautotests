﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceTemplatePage
    {
        public int Id { get; set; }
        public int InvoiceTemplateId { get; set; }
        public int Sequence { get; set; }
        public string XsltFileName { get; set; }

        public virtual InvoiceTemplate InvoiceTemplate { get; set; }
    }
}
