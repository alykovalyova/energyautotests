﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoiceType
    {
        public InvoiceType()
        {
            Batch = new HashSet<Batch>();
            ExceptionQueue = new HashSet<ExceptionQueue>();
            InvoiceQueue = new HashSet<InvoiceQueue>();
            InvoiceTemplate = new HashSet<InvoiceTemplate>();
            Invoices = new HashSet<Invoices>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DescriptionNl { get; set; }

        public virtual SingleTransactionConfiguration SingleTransactionConfiguration { get; set; }
        public virtual ICollection<Batch> Batch { get; set; }
        public virtual ICollection<ExceptionQueue> ExceptionQueue { get; set; }
        public virtual ICollection<InvoiceQueue> InvoiceQueue { get; set; }
        public virtual ICollection<InvoiceTemplate> InvoiceTemplate { get; set; }
        public virtual ICollection<Invoices> Invoices { get; set; }
    }
}
