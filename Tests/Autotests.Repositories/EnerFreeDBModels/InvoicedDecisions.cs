﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoicedDecisions
    {
        public int Id { get; set; }
        public int InvoiceSectionId { get; set; }
        public int DecisionChoiceId { get; set; }
        public string EnergymeterId { get; set; }
        public int? LastModifiedByUserId { get; set; }

        public virtual InvoiceDecisionChoice DecisionChoice { get; set; }
        public virtual InvoiceSections InvoiceSection { get; set; }
        public virtual Users LastModifiedByUser { get; set; }
    }
}
