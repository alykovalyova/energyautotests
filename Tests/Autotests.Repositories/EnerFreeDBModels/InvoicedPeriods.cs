﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoicedPeriods
    {
        public InvoicedPeriods()
        {
            InvoiceLines = new HashSet<InvoiceLines>();
        }

        public int Id { get; set; }
        public int InvoiceSectionId { get; set; }
        public byte ProfileCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Enddate { get; set; }
        public double UsageNormal { get; set; }
        public double UsageLow { get; set; }
        public long? EanCapacityTariff { get; set; }
        public string ProdmanPropositionId { get; set; }
        public byte? GasRegion { get; set; }

        public virtual InvoiceSections InvoiceSection { get; set; }
        public virtual ICollection<InvoiceLines> InvoiceLines { get; set; }
    }
}
