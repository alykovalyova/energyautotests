﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoicedReadings
    {
        public int Id { get; set; }
        public int InvoiceSectionId { get; set; }
        public string EnergyMeterId { get; set; }
        public long Ean { get; set; }
        public byte RegisterType { get; set; }
        public byte EnergyFlowDirection { get; set; }
        public int? BeginRegisterReadingId { get; set; }
        public DateTime BeginReadingDate { get; set; }
        public short BeginReadingQualification { get; set; }
        public int BeginReading { get; set; }
        public int? EndRegisterReadingId { get; set; }
        public DateTime EndReadingDate { get; set; }
        public short EndReadingQualification { get; set; }
        public int EndReading { get; set; }
        public int Volume { get; set; }
        public decimal InvoicedVolume { get; set; }
        public byte VolumeSource { get; set; }

        public virtual InvoiceSections InvoiceSection { get; set; }
    }
}
