﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Invoices
    {
        public Invoices()
        {
            InvoiceSections = new HashSet<InvoiceSections>();
        }

        public int Id { get; set; }
        public int? ReferencedInvoiceId { get; set; }
        public string Number { get; set; }
        public byte InvoiceTypeId { get; set; }
        public byte CorrectionStatusId { get; set; }
        public byte LabelCode { get; set; }
        public int ContractId { get; set; }
        public decimal AmountExcl { get; set; }
        public decimal AmountVat { get; set; }
        public decimal AmountIncl { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime? DirectDebitDate { get; set; }
        public string AccountNumber { get; set; }
        public byte PaymentMethodId { get; set; }
        public string PaymentReference { get; set; }
        public int? BillingDocumentInfoId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CreatedByUserId { get; set; }
        public string Remarks { get; set; }

        public virtual BillingDocumentInfo BillingDocumentInfo { get; set; }
        public virtual CorrectionStatus CorrectionStatus { get; set; }
        public virtual Users CreatedByUser { get; set; }
        public virtual InvoiceType InvoiceType { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }
        public virtual ICollection<InvoiceSections> InvoiceSections { get; set; }
    }
}
