﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class InvoicingOnHold
    {
        public int RelatieId { get; set; }
        public int ModeratorTypeId { get; set; }
        public DateTime Inserted { get; set; }
        public string Reason { get; set; }

        public virtual OnHoldModeratorType ModeratorType { get; set; }
        public virtual Relaties Relatie { get; set; }
    }
}
