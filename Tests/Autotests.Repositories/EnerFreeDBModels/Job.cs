﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Job
    {
        public Job()
        {
            JobQueue = new HashSet<JobQueue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsOn { get; set; }
        public DateTime? LastRun { get; set; }
        public int IntervalMinutes { get; set; }
        public string ClassName { get; set; }
        public int? TakeTop { get; set; }
        public bool SendDevDail { get; set; }
        public string DevEmail { get; set; }
        public string DevEmailSendFrequency { get; set; }
        public DateTime? DevEmailLastSent { get; set; }

        public virtual ICollection<JobQueue> JobQueue { get; set; }
    }
}
