﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class KostenSoort
    {
        public KostenSoort()
        {
            AdvanceInvoiceLines = new HashSet<AdvanceInvoiceLines>();
            ContractRegelSpecificaties = new HashSet<ContractRegelSpecificaties>();
            InvoiceLines = new HashSet<InvoiceLines>();
        }

        public short Id { get; set; }
        public string Kenmerk { get; set; }
        public int? Grootboek { get; set; }
        public string Omschrijving { get; set; }
        public string Eenheid { get; set; }
        public bool IsPartOfBur { get; set; }

        public virtual ICollection<AdvanceInvoiceLines> AdvanceInvoiceLines { get; set; }
        public virtual ICollection<ContractRegelSpecificaties> ContractRegelSpecificaties { get; set; }
        public virtual ICollection<InvoiceLines> InvoiceLines { get; set; }
    }
}
