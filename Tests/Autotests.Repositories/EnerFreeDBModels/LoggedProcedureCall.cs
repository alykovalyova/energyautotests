﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class LoggedProcedureCall
    {
        public string Procname { get; set; }
        public DateTime LastUsed { get; set; }
        public string LoginName { get; set; }
        public string ClientName { get; set; }
        public string ApplicationName { get; set; }
        public int TimesCalled { get; set; }
    }
}
