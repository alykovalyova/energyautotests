﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class MeterdataSelection
    {
        public int Id { get; set; }
        public int ContractRegelId { get; set; }
        public int? MeterId { get; set; }
        public string EanGebruiker { get; set; }
        public string Metertype { get; set; }
        public int? Meterstand { get; set; }
        public DateTime MeetDatum { get; set; }
        public string RedenId { get; set; }
        public string BronId { get; set; }
        public int KwalificatieId { get; set; }
        public string ReadingType { get; set; }
        public int? RequestId { get; set; }
        public int RelateId { get; set; }
        public string ClassNm { get; set; }
        public bool? RejectionStatus { get; set; }
        public bool TriggerForUsage { get; set; }
        public bool? Invoiced { get; set; }
        public bool? Corrected { get; set; }
        public DateTime DateTimeInserted { get; set; }
        public DateTime? DateTimeModified { get; set; }
        public int? ModifiedByUserId { get; set; }

        public virtual Meters Meter { get; set; }
        public virtual Redenen Reden { get; set; }
    }
}
