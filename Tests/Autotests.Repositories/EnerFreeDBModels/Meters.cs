﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Meters
    {
        public Meters()
        {
            MeterdataSelection = new HashSet<MeterdataSelection>();
        }

        public int MeterId { get; set; }
        public int EanId { get; set; }
        public string Meternummer { get; set; }
        public int? Telwerken { get; set; }
        public int? Factor { get; set; }
        public DateTime? StartDatum { get; set; }
        public DateTime? EindDatum { get; set; }
        public string Opmerking { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }

        public virtual EanBoek Ean { get; set; }
        public virtual ICollection<MeterdataSelection> MeterdataSelection { get; set; }
    }
}
