﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class MonthlyAmountHistory
    {
        public int Id { get; set; }
        public int ContractRegelId { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int ModeratorTypeId { get; set; }
        public int? UsageNormal { get; set; }
        public int? UsageLow { get; set; }
        public decimal? BedragExcl { get; set; }
        public decimal? BedragBtw { get; set; }
        public decimal? BedragIncl { get; set; }
        public bool IsActive { get; set; }
        public int? ProductionNormal { get; set; }
        public int? ProductionLow { get; set; }

        public virtual ContractRegels ContractRegel { get; set; }
        public virtual MonthlyAmountModeratorType ModeratorType { get; set; }
    }
}
