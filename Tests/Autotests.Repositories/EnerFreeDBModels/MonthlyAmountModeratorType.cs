﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class MonthlyAmountModeratorType
    {
        public MonthlyAmountModeratorType()
        {
            MonthlyAmountHistory = new HashSet<MonthlyAmountHistory>();
        }

        public int Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MonthlyAmountHistory> MonthlyAmountHistory { get; set; }
    }
}
