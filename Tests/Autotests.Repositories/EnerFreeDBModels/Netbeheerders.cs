﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Netbeheerders
    {
        public Netbeheerders()
        {
            EanBoek = new HashSet<EanBoek>();
        }

        public string EanNb { get; set; }
        public string Naam { get; set; }
        public string Shortname { get; set; }
        public bool EchDeelnemer { get; set; }
        public bool Elektra { get; set; }
        public bool Gas { get; set; }
        public int? GasRegio { get; set; }
        public int? AdresId { get; set; }
        public string TelStoring { get; set; }
        public string TelService { get; set; }
        public string TelKlantenservice { get; set; }
        public string Website { get; set; }
        public string ContactPersoon { get; set; }
        public string Telefoon { get; set; }
        public string EmailAlgemeen { get; set; }
        public string EmailTerugswitch { get; set; }
        public string EmailLeverancier { get; set; }
        public string EmailNb { get; set; }
        public string EmailOverige { get; set; }
        public string Opmerking { get; set; }
        public string RekeningNummer { get; set; }
        public string RekeningNaam { get; set; }
        public string RekeningPlaats { get; set; }
        public DateTime? CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }
        public string BtwNummer { get; set; }

        public virtual Adressen Adres { get; set; }
        public virtual ICollection<EanBoek> EanBoek { get; set; }
    }
}
