﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class PaymentDetails
    {
        public int Id { get; set; }
        public int RelatieId { get; set; }
        public long? MandateId { get; set; }
        public string Iban { get; set; }
        public string AccountHolderName { get; set; }
        public byte PaymentMethodId { get; set; }
        public DateTime? ActiveTo { get; set; }
        public DateTime Created { get; set; }

        public virtual Relaties Relatie { get; set; }
    }
}
