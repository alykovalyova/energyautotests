﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class PenaltyCategory
    {
        public PenaltyCategory()
        {
            ContractAgreements = new HashSet<ContractAgreements>();
        }

        public int Id { get; set; }
        public string PenaltyCategoryName { get; set; }
        public DateTime CreationDate { get; set; }

        public virtual ICollection<ContractAgreements> ContractAgreements { get; set; }
    }
}
