﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class PrConnectionView
    {
        public int EanId { get; set; }
        public int VadresId { get; set; }
        public DateTime? ContractStart { get; set; }
        public string EanGebruiker { get; set; }
        public string SegmentNaam { get; set; }
        public int ContractId { get; set; }
        public bool Actief { get; set; }
        public string Naam { get; set; }
        public string Omschrijving { get; set; }
        public bool GrootVerbruik { get; set; }
        public int ContractRegelId { get; set; }
        public int? GasRegio { get; set; }
        public bool Geannuleerd { get; set; }
        public bool Opgezegd { get; set; }
        public int? SegmentId { get; set; }
        public int? Jvh { get; set; }
        public int? Jvl { get; set; }
        public DateTime? ContractRegelStart { get; set; }
        public DateTime? ContractRegelEind { get; set; }
    }
}
