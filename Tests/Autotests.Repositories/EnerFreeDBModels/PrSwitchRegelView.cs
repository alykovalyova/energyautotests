﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class PrSwitchRegelView
    {
        public int ContractId { get; set; }
        public int ContractRegelId { get; set; }
        public int SwitchRegelId { get; set; }
        public string Bericht { get; set; }
        public string Status { get; set; }
        public DateTime? Voorsteldatum { get; set; }
        public DateTime? IngediendDatum { get; set; }
        public DateTime? Switchdatum { get; set; }
        public int Correctie { get; set; }
        public string DossierId { get; set; }
        public string Opmerking { get; set; }
        public string Regel { get; set; }
    }
}
