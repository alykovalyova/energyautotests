﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class PrSwitchesViewBeginSwitchRegels
    {
        public int ContractId { get; set; }
        public int ContractRegelId { get; set; }
        public int SwitchId { get; set; }
        public int BeginSwitchRegelId { get; set; }
        public DateTime? BeginSwitchdatum { get; set; }
        public string BeginStatus { get; set; }
    }
}
