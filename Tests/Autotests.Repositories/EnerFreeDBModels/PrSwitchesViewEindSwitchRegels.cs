﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class PrSwitchesViewEindSwitchRegels
    {
        public int ContractId { get; set; }
        public int ContractRegelId { get; set; }
        public int SwitchId { get; set; }
        public int EindSwitchRegelId { get; set; }
        public DateTime? EindSwitchdatum { get; set; }
        public string EindStatus { get; set; }
    }
}
