﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ProcLog
    {
        public int Id { get; set; }
        public string Servername { get; set; }
        public DateTime? Rundate { get; set; }
        public string Stepname { get; set; }
        public string Objname { get; set; }
        public string Msg { get; set; }
        public long? Rowcnt { get; set; }
        public int? Errno { get; set; }
        public string Runuser { get; set; }
    }
}
