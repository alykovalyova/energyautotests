﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ReclaimVoucherCancellationMoment
    {
        public ReclaimVoucherCancellationMoment()
        {
            SendEmailReclaimVoucher = new HashSet<SendEmailReclaimVoucher>();
        }

        public int Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SendEmailReclaimVoucher> SendEmailReclaimVoucher { get; set; }
    }
}
