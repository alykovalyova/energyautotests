﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Reconciliations
    {
        public int Id { get; set; }
        public int AdvanceInvoiceSectionId { get; set; }
        public int? InvoiceSectionId { get; set; }
        public int? CounteredByInvoiceId { get; set; }

        public virtual AdvanceInvoiceSections AdvanceInvoiceSection { get; set; }
        public virtual InvoiceSections InvoiceSection { get; set; }
    }
}
