﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class RegistersType
    {
        public byte Id { get; set; }
        public bool HasNormal { get; set; }
        public bool HasLow { get; set; }
        public bool HasProductionNormal { get; set; }
        public bool HasProductionLow { get; set; }
        public bool HasTotal { get; set; }
        public bool HasProductionTotal { get; set; }
    }
}
