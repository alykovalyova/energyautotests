﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class RelatieDocumenten
    {
        public long Id { get; set; }
        public int RelatieId { get; set; }
        public string FileName { get; set; }
        public DateTime FileDate { get; set; }
        public string TypeDescription { get; set; }
        public string ExternalReference { get; set; }
        public string ExternalReferenceType { get; set; }

        public virtual Relaties Relatie { get; set; }
    }
}
