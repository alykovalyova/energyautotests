﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Relaties
    {
        public Relaties()
        {
            Contracten = new HashSet<Contracten>();
            InvoicingOnHold = new HashSet<InvoicingOnHold>();
            PaymentDetails = new HashSet<PaymentDetails>();
            RelatieDocumenten = new HashSet<RelatieDocumenten>();
            Smslog = new HashSet<Smslog>();
        }

        public int RelatieId { get; set; }
        public string Categorie { get; set; }
        public string Bedrijfsnaam { get; set; }
        public string Kvk { get; set; }
        public string Btwnummer { get; set; }
        public string Geslacht { get; set; }
        public string Voorletters { get; set; }
        public string Tussenvoegsel { get; set; }
        public string Naam { get; set; }
        public DateTime? Geboortedatum { get; set; }
        public string Telefoon { get; set; }
        public string Gsm { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Opmerking { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }
        public int? Aaid { get; set; }
        public int? LeadRelatieId { get; set; }
        public bool ReceiveUsageEmail { get; set; }
        public string Password { get; set; }
        public DateTime? LastKto { get; set; }
        public DateTime? LastKto1 { get; set; }
        public string Memo { get; set; }
        public byte LabelCode { get; set; }
        public int NutsHomeCustomerNumber { get; set; }

        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual ICollection<Contracten> Contracten { get; set; }
        public virtual ICollection<InvoicingOnHold> InvoicingOnHold { get; set; }
        public virtual ICollection<PaymentDetails> PaymentDetails { get; set; }
        public virtual ICollection<RelatieDocumenten> RelatieDocumenten { get; set; }
        public virtual ICollection<Smslog> Smslog { get; set; }
    }
}
