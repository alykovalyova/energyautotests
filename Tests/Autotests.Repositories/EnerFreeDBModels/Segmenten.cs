﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Segmenten
    {
        public Segmenten()
        {
            EanBoek = new HashSet<EanBoek>();
            Profielen = new HashSet<Profielen>();
        }

        public int SegmentId { get; set; }
        public string SegmentNaam { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }

        public virtual ICollection<EanBoek> EanBoek { get; set; }
        public virtual ICollection<Profielen> Profielen { get; set; }
    }
}
