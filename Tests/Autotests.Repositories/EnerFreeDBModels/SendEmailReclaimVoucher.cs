﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class SendEmailReclaimVoucher
    {
        public int Id { get; set; }
        public Guid ReferenceId { get; set; }
        public int VoucherId { get; set; }
        public int CancellationMomentId { get; set; }
        public int? InvoiceId { get; set; }
        public DateTime Created { get; set; }

        public virtual ReclaimVoucherCancellationMoment CancellationMoment { get; set; }
    }
}
