﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class SingleTransactionConfiguration
    {
        public int StconfigurationId { get; set; }
        public string Description { get; set; }
        public double MinimumAmount { get; set; }
        public double MaximumAmount { get; set; }
        public byte InvoiceTypeId { get; set; }
        public bool Active { get; set; }

        public virtual InvoiceType InvoiceType { get; set; }
    }
}
