﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Smslog
    {
        public int SmsId { get; set; }
        public int? RelatieId { get; set; }
        public int? ContractId { get; set; }
        public string PhonenumberUsed { get; set; }
        public string SmsMessage { get; set; }
        public DateTime DateTimeSent { get; set; }
        public string Originator { get; set; }
        public string ResultMessage { get; set; }

        public virtual Contracten Contract { get; set; }
        public virtual Relaties Relatie { get; set; }
    }
}
