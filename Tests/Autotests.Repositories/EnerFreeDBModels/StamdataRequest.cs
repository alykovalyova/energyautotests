﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class StamdataRequest
    {
        public int StamdataRequestId { get; set; }
        public int EanId { get; set; }
        public bool Ingediend { get; set; }
        public DateTime? IngediendDatum { get; set; }
        public bool Afgewezen { get; set; }
        public DateTime? AfgewezenDatum { get; set; }
        public string AfwijsReden { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }

        public virtual EanBoek Ean { get; set; }
    }
}
