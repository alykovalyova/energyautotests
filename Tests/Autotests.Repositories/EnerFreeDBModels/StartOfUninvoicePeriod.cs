﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class StartOfUninvoicePeriod
    {
        public int ContractRegelId { get; set; }
        public DateTime? StartOfPeriod { get; set; }
        public DateTime? EndSupply { get; set; }
    }
}
