﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class SwitchCorrectieTypes
    {
        public SwitchCorrectieTypes()
        {
            SwitchRegels = new HashSet<SwitchRegels>();
        }

        public int Id { get; set; }
        public string Naam { get; set; }
        public string Beschrijving { get; set; }
        public DateTime CreatieDatum { get; set; }
        public byte[] Timestamp { get; set; }

        public virtual ICollection<SwitchRegels> SwitchRegels { get; set; }
    }
}
