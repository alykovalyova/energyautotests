﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class SwitchRegels
    {
        public SwitchRegels()
        {
            SwitchesBeginSwitchregel = new HashSet<Switches>();
            SwitchesEindSwitchregel = new HashSet<Switches>();
        }

        public int SwitchRegelId { get; set; }
        public string Bericht { get; set; }
        public bool Ingediend { get; set; }
        public string Status { get; set; }
        public DateTime? Voorsteldatum { get; set; }
        public DateTime? Switchdatum { get; set; }
        public int? EchId { get; set; }
        public DateTime? IngediendDatum { get; set; }
        public string Opmerking { get; set; }
        public bool Geforceerd { get; set; }
        public int Correctie { get; set; }
        public DateTime CreatieDatum { get; set; }
        public int? Contractregelid { get; set; }
        public string DossierId { get; set; }
        public byte EdsnrejectionCount { get; set; }

        public virtual SwitchCorrectieTypes CorrectieNavigation { get; set; }
        public virtual ICollection<Switches> SwitchesBeginSwitchregel { get; set; }
        public virtual ICollection<Switches> SwitchesEindSwitchregel { get; set; }
    }
}
