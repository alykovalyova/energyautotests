﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class SwitchRegelsLogs
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public string User { get; set; }
        public string ChangeType { get; set; }
        public int ContractRegelId { get; set; }
        public int SwitchId { get; set; }
        public bool IsBeginSwitchRegel { get; set; }
        public int SwitchRegelId { get; set; }
        public string BerichtOld { get; set; }
        public bool? IngediendOld { get; set; }
        public string StatusOld { get; set; }
        public DateTime? VoorsteldatumOld { get; set; }
        public DateTime? SwitchdatumOld { get; set; }
        public int? EchIdOld { get; set; }
        public string OpmerkingOld { get; set; }
        public int? CorrectieOld { get; set; }
        public string BerichtNew { get; set; }
        public bool? IngediendNew { get; set; }
        public string StatusNew { get; set; }
        public DateTime? VoorsteldatumNew { get; set; }
        public DateTime? SwitchdatumNew { get; set; }
        public int? EchIdNew { get; set; }
        public string OpmerkingNew { get; set; }
        public int? CorrectieNew { get; set; }

        public virtual ContractRegels ContractRegel { get; set; }
    }
}
