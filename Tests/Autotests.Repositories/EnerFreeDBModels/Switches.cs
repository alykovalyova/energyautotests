﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Switches
    {
        public int SwitchId { get; set; }
        public int? ContractRegelId { get; set; }
        public string Opmerking { get; set; }
        public int? BeginSwitchregelId { get; set; }
        public int? EindSwitchregelId { get; set; }
        public DateTime CreatieDatum { get; set; }

        public virtual SwitchRegels BeginSwitchregel { get; set; }
        public virtual ContractRegels ContractRegel { get; set; }
        public virtual SwitchRegels EindSwitchregel { get; set; }
    }
}
