﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class TempAcquisitionInfo
    {
        public TempAcquisitionInfo()
        {
            TempAcquisitionInfoConnection = new HashSet<TempAcquisitionInfoConnection>();
        }

        public int Id { get; set; }
        public int ContractId { get; set; }
        public int CustomerId { get; set; }
        public int AdressId { get; set; }
        public DateTime? YearlyBillingDate { get; set; }
        public DateTime LastMeterReadingAcquisitionDate { get; set; }

        public virtual ICollection<TempAcquisitionInfoConnection> TempAcquisitionInfoConnection { get; set; }
    }
}
