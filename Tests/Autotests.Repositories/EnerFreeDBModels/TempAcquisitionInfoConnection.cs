﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class TempAcquisitionInfoConnection
    {
        public TempAcquisitionInfoConnection()
        {
            TempAcquisitionInfoRegister = new HashSet<TempAcquisitionInfoRegister>();
        }

        public int Id { get; set; }
        public string Ean { get; set; }
        public string ProductType { get; set; }
        public byte NrOfRegisters { get; set; }
        public string PhysicalStatus { get; set; }
        public bool ActiveSmartMeter { get; set; }
        public int AcquisitionInfoId { get; set; }

        public virtual TempAcquisitionInfo AcquisitionInfo { get; set; }
        public virtual ICollection<TempAcquisitionInfoRegister> TempAcquisitionInfoRegister { get; set; }
    }
}
