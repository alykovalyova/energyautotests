﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class TempAcquisitionInfoRegister
    {
        public int Id { get; set; }
        public string RegisterNr { get; set; }
        public byte NrOfDigits { get; set; }
        public string TariffType { get; set; }
        public string MeteringDirection { get; set; }
        public int ConnectionId { get; set; }

        public virtual TempAcquisitionInfoConnection Connection { get; set; }
    }
}
