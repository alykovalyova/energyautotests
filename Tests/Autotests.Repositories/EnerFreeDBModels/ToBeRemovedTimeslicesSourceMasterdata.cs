﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class ToBeRemovedTimeslicesSourceMasterdata
    {
        public int Id { get; set; }
        public string ConnectEan { get; set; }
        public string CapTarCode { get; set; }
        public string ProfileCategory { get; set; }
        public string GridArea { get; set; }
        public DateTime MutationDate { get; set; }
        public int RelateId { get; set; }
        public string ClassName { get; set; }
        public string PhysicalStatus { get; set; }
        public string MeteringMethod { get; set; }
        public string GridOperator { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EnergyFlowDirection { get; set; }
        public int SegmentId { get; set; }
        public string MarketSegment { get; set; }
        public int? Eacpeak { get; set; }
        public int? EacoffPeak { get; set; }
        public string EdsnaddressStreetName { get; set; }
        public int? EdsnaddressBuildingNr { get; set; }
        public string EdsnaddressExBuildingNr { get; set; }
        public string EdsnaddressZipcode { get; set; }
        public string EdsnaddressCityName { get; set; }
        public string EdsnaddressCountry { get; set; }
        public string MeterEnergymeterId { get; set; }
        public int? MeterNrOfRegisters { get; set; }
        public int? MeterNrOfDigits { get; set; }
        public bool MeterHasTariffTypeT { get; set; }
        public float? MeterMultiplicationFactor { get; set; }
        public bool AdministrativeStatusSmartMeter { get; set; }
        public int? Eappeak { get; set; }
        public int? EapoffPeak { get; set; }
        public bool HasTemperatureCorrection { get; set; }
        public bool? IsResidential { get; set; }
        public byte RegistersTypeId { get; set; }
    }
}
