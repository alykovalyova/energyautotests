﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class Users
    {
        public Users()
        {
            AdvanceInvoices = new HashSet<AdvanceInvoices>();
            Batch = new HashSet<Batch>();
            BbaBatchApprovedByUser = new HashSet<BbaBatch>();
            BbaBatchRunByUser = new HashSet<BbaBatch>();
            BbaInvoiceRunContractLog = new HashSet<BbaInvoiceRunContractLog>();
            InvoicedDecisions = new HashSet<InvoicedDecisions>();
            Invoices = new HashSet<Invoices>();
        }

        public int Id { get; set; }
        public string TechnicalKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<AdvanceInvoices> AdvanceInvoices { get; set; }
        public virtual ICollection<Batch> Batch { get; set; }
        public virtual ICollection<BbaBatch> BbaBatchApprovedByUser { get; set; }
        public virtual ICollection<BbaBatch> BbaBatchRunByUser { get; set; }
        public virtual ICollection<BbaInvoiceRunContractLog> BbaInvoiceRunContractLog { get; set; }
        public virtual ICollection<InvoicedDecisions> InvoicedDecisions { get; set; }
        public virtual ICollection<Invoices> Invoices { get; set; }
    }
}
