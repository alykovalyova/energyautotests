﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.EnerFreeDBModels
{
    public partial class VwInvoiceKeys
    {
        public byte LabelCode { get; set; }
        public int? ContractantId { get; set; }
        public int ContractId { get; set; }
        public int InvoiceId { get; set; }
        public string Number { get; set; }
        public DateTime InvoiceDate { get; set; }
        public byte CorrectionStatusId { get; set; }
        public byte InvoiceTypeId { get; set; }
        public int InvoiceSectionId { get; set; }
        public int? ContractRegelId { get; set; }
        public long InvoiceLineId { get; set; }
        public byte InstallmentTypeId { get; set; }
        public short KostensoortId { get; set; }
    }
}
