﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.GridOperatorTariffModels
{
    public partial class DictionaryProductType
    {
        public DictionaryProductType()
        {
            Tariff = new HashSet<Tariff>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Tariff> Tariff { get; set; }
    }
}
