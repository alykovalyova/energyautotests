﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.GridOperatorTariffModels
{
    public partial class GridOperatorTariff
    {
        public GridOperatorTariff()
        {
            StatusHistory = new HashSet<StatusHistory>();
            Tariff = new HashSet<Tariff>();
        }

        public int Id { get; set; }
        public string EdsnGridOperatorTariffId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime QueryDate { get; set; }
        public string Currency { get; set; }
        public string GridOperator { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual DictionaryStatus Status { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
        public virtual ICollection<Tariff> Tariff { get; set; }
    }
}
