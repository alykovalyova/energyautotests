﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.GridOperatorTariffModels
{
    public partial class GridOperatorTariffContext : DbContext
    {
        private readonly string _connectionString;

        public GridOperatorTariffContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public GridOperatorTariffContext(DbContextOptions<GridOperatorTariffContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryProductType> DictionaryProductType { get; set; }
        public virtual DbSet<DictionaryStatus> DictionaryStatus { get; set; }
        public virtual DbSet<GridOperatorTariff> GridOperatorTariff { get; set; }
        public virtual DbSet<StatusHistory> StatusHistory { get; set; }
        public virtual DbSet<Tariff> Tariff { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryProductType>(entity =>
            {
                entity.ToTable("Dictionary_ProductType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryStatus>(entity =>
            {
                entity.ToTable("Dictionary_Status", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<GridOperatorTariff>(entity =>
            {
                entity.ToTable("GridOperatorTariff", "dbo");

                entity.HasIndex(e => e.StatusId);

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.DateFrom).HasColumnType("date");

                entity.Property(e => e.EdsnGridOperatorTariffId).HasMaxLength(50);

                entity.Property(e => e.GridOperator)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.QueryDate).HasColumnType("date");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GridOperatorTariff)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory", "dbo");

                entity.HasIndex(e => e.GridOperatorTariffId);

                entity.HasIndex(e => e.StatusId);

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.HasOne(d => d.GridOperatorTariff)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.GridOperatorTariffId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<Tariff>(entity =>
            {
                entity.ToTable("Tariff", "dbo");

                entity.HasIndex(e => e.GridOperatorTariffId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.Property(e => e.CapTarCode)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.DayTariff).HasColumnType("decimal(12, 4)");

                entity.HasOne(d => d.GridOperatorTariff)
                    .WithMany(p => p.Tariff)
                    .HasForeignKey(d => d.GridOperatorTariffId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Tariff)
                    .HasForeignKey(d => d.ProductTypeId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
