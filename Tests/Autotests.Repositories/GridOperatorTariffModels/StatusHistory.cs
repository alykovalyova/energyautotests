﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.GridOperatorTariffModels
{
    public partial class StatusHistory
    {
        public int Id { get; set; }
        public string ModifiedBy { get; set; }
        public int GridOperatorTariffId { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual GridOperatorTariff GridOperatorTariff { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
