﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.GridOperatorTariffModels
{
    public partial class Tariff
    {
        public int Id { get; set; }
        public short ProductTypeId { get; set; }
        public decimal DayTariff { get; set; }
        public string CapTarCode { get; set; }
        public int GridOperatorTariffId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual GridOperatorTariff GridOperatorTariff { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
    }
}
