﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.IC92SettlementModels
{
    public partial class File
    {
        public File()
        {
            SettlementInvoice = new HashSet<Settlement>();
            SettlementProforma = new HashSet<Settlement>();
            SettlementStatusHistory = new HashSet<SettlementStatusHistory>();
        }

        public int Id { get; set; }
        public string FullPath { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Settlement> SettlementInvoice { get; set; }
        public virtual ICollection<Settlement> SettlementProforma { get; set; }
        public virtual ICollection<SettlementStatusHistory> SettlementStatusHistory { get; set; }
    }
}
