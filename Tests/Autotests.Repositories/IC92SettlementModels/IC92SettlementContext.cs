﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.IC92SettlementModels
{
    public partial class IC92SettlementContext : DbContext
    {
        private readonly string _connectionString;

        public IC92SettlementContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IC92SettlementContext(DbContextOptions<IC92SettlementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryLabel> DictionaryLabel { get; set; }
        public virtual DbSet<DictionaryMeteringDirection> DictionaryMeteringDirection { get; set; }
        public virtual DbSet<DictionaryProductType> DictionaryProductType { get; set; }
        public virtual DbSet<DictionaryStatus> DictionaryStatus { get; set; }
        public virtual DbSet<DictionaryTariffType> DictionaryTariffType { get; set; }
        public virtual DbSet<File> File { get; set; }
        public virtual DbSet<Register> Register { get; set; }
        public virtual DbSet<Settlement> Settlement { get; set; }
        public virtual DbSet<SettlementStatusHistory> SettlementStatusHistory { get; set; }
        public virtual DbSet<Tariff> Tariff { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeteringDirection>(entity =>
            {
                entity.ToTable("Dictionary_MeteringDirection", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProductType>(entity =>
            {
                entity.ToTable("Dictionary_ProductType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryStatus>(entity =>
            {
                entity.ToTable("Dictionary_Status", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryTariffType>(entity =>
            {
                entity.ToTable("Dictionary_TariffType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<File>(entity =>
            {
                entity.ToTable("File", "dbo");

                entity.Property(e => e.FullPath).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.ToTable("Register", "dbo");

                entity.HasIndex(e => e.MeteringDirectionId);

                entity.HasIndex(e => e.SettlementId);

                entity.HasIndex(e => e.TariffTypeId);

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringDirectionId);

                entity.HasOne(d => d.Settlement)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.SettlementId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.TariffTypeId);
            });

            modelBuilder.Entity<Settlement>(entity =>
            {
                entity.ToTable("Settlement", "dbo");

                entity.HasIndex(e => e.InvoiceId);

                entity.HasIndex(e => e.LabelId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.HasIndex(e => e.ProformaId);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.TariffId);

                entity.HasIndex(e => new { e.SupplierId, e.LabelId, e.StatusId, e.EventDate });

                entity.Property(e => e.Comment).HasMaxLength(250);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.EventDate).HasColumnType("date");

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(12, 4)");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.SettlementInvoice)
                    .HasForeignKey(d => d.InvoiceId);

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.Settlement)
                    .HasForeignKey(d => d.LabelId);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.Settlement)
                    .HasForeignKey(d => d.ProductTypeId);

                entity.HasOne(d => d.Proforma)
                    .WithMany(p => p.SettlementProforma)
                    .HasForeignKey(d => d.ProformaId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Settlement)
                    .HasForeignKey(d => d.StatusId);

                entity.HasOne(d => d.Tariff)
                    .WithMany(p => p.Settlement)
                    .HasForeignKey(d => d.TariffId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<SettlementStatusHistory>(entity =>
            {
                entity.ToTable("SettlementStatusHistory", "dbo");

                entity.HasIndex(e => e.FileId);

                entity.HasIndex(e => e.SettlementId);

                entity.HasIndex(e => e.StatusId);

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.HasOne(d => d.File)
                    .WithMany(p => p.SettlementStatusHistory)
                    .HasForeignKey(d => d.FileId);

                entity.HasOne(d => d.Settlement)
                    .WithMany(p => p.SettlementStatusHistory)
                    .HasForeignKey(d => d.SettlementId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SettlementStatusHistory)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<Tariff>(entity =>
            {
                entity.ToTable("Tariff", "dbo");

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.Property(e => e.PriceElec).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.PriceGas).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.ValidFrom).HasColumnType("date");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
