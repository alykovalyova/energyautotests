﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.IC92SettlementModels
{
    public partial class Register
    {
        public int Id { get; set; }
        public int Usage { get; set; }
        public int? OriginalReading { get; set; }
        public int? NewReading { get; set; }
        public int SettlementId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short TariffTypeId { get; set; }

        public virtual DictionaryMeteringDirection MeteringDirection { get; set; }
        public virtual Settlement Settlement { get; set; }
        public virtual DictionaryTariffType TariffType { get; set; }
    }
}
