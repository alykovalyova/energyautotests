﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.IC92SettlementModels
{
    public partial class Settlement
    {
        public Settlement()
        {
            Register = new HashSet<Register>();
            SettlementStatusHistory = new HashSet<SettlementStatusHistory>();
        }

        public int Id { get; set; }
        public int SupplierId { get; set; }
        public string Ean { get; set; }
        public DateTime EventDate { get; set; }
        public int TotalUsage { get; set; }
        public decimal TotalAmount { get; set; }
        public int TariffId { get; set; }
        public int ContractId { get; set; }
        public short ProductTypeId { get; set; }
        public short StatusId { get; set; }
        public short LabelId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? InvoiceId { get; set; }
        public int? ProformaId { get; set; }
        public string Comment { get; set; }

        public virtual File Invoice { get; set; }
        public virtual DictionaryLabel Label { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
        public virtual File Proforma { get; set; }
        public virtual DictionaryStatus Status { get; set; }
        public virtual Tariff Tariff { get; set; }
        public virtual ICollection<Register> Register { get; set; }
        public virtual ICollection<SettlementStatusHistory> SettlementStatusHistory { get; set; }
    }
}
