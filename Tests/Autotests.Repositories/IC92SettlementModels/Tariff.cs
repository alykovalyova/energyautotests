﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.IC92SettlementModels
{
    public partial class Tariff
    {
        public Tariff()
        {
            Settlement = new HashSet<Settlement>();
        }

        public int Id { get; set; }
        public decimal PriceElec { get; set; }
        public decimal PriceGas { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        public virtual ICollection<Settlement> Settlement { get; set; }
    }
}
