﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class AddressHistory
    {
        public int Id { get; set; }
        public string EanId { get; set; }
        public int AddressId { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Zipcode { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public DateTime MutationDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsChangedByAgent { get; set; }
        public string Comment { get; set; }
        public string User { get; set; }

        public virtual Address Address { get; set; }
    }
}
