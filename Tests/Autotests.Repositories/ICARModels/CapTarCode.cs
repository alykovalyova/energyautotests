﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class CapTarCode
    {
        public CapTarCode()
        {
            FallbackMeteringPoint = new HashSet<FallbackMeteringPoint>();
            MduHistory = new HashSet<MduHistory>();
            MeteringPoint = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoint = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string Ean { get; set; }

        public virtual ICollection<FallbackMeteringPoint> FallbackMeteringPoint { get; set; }
        public virtual ICollection<MduHistory> MduHistory { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
    }
}
