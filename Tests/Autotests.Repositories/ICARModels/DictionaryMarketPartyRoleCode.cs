﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class DictionaryMarketPartyRoleCode
    {
        public DictionaryMarketPartyRoleCode()
        {
            MarketParty = new HashSet<MarketParty>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MarketParty> MarketParty { get; set; }
    }
}
