﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class DictionaryMarketSegmentCode
    {
        public DictionaryMarketSegmentCode()
        {
            FallbackMeteringPoint = new HashSet<FallbackMeteringPoint>();
            MduHistory = new HashSet<MduHistory>();
            MeteringPoint = new HashSet<MeteringPoint>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<FallbackMeteringPoint> FallbackMeteringPoint { get; set; }
        public virtual ICollection<MduHistory> MduHistory { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
    }
}
