﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class DictionaryMutationReasonCode
    {
        public DictionaryMutationReasonCode()
        {
            CcuHistory = new HashSet<CcuHistory>();
            MduMutation = new HashSet<MduMutation>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CcuHistory> CcuHistory { get; set; }
        public virtual ICollection<MduMutation> MduMutation { get; set; }
    }
}
