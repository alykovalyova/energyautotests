﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class DictionaryProcessStatus
    {
        public DictionaryProcessStatus()
        {
            CcuHistory = new HashSet<CcuHistory>();
            CcuStatusHistory = new HashSet<CcuStatusHistory>();
            MduHistory = new HashSet<MduHistory>();
            MduStatusHistory = new HashSet<MduStatusHistory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CcuHistory> CcuHistory { get; set; }
        public virtual ICollection<CcuStatusHistory> CcuStatusHistory { get; set; }
        public virtual ICollection<MduHistory> MduHistory { get; set; }
        public virtual ICollection<MduStatusHistory> MduStatusHistory { get; set; }
    }
}
