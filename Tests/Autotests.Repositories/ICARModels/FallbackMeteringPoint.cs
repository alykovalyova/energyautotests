﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class FallbackMeteringPoint
    {
        public int Id { get; set; }
        public string EanId { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Zipcode { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public string InvoiceMonth { get; set; }
        public byte ProductTypeId { get; set; }
        public short? GridOperatorId { get; set; }
        public short? GridAreaId { get; set; }
        public short? CapTarCodeId { get; set; }
        public bool? DeterminationComplex { get; set; }
        public bool? Residential { get; set; }
        public byte? ProfileCategoryId { get; set; }
        public byte? EnergyFlowDirectionId { get; set; }
        public byte? PhysicalCapacityId { get; set; }
        public byte MarketSegmentId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual CapTarCode CapTarCode { get; set; }
        public virtual DictionaryEnergyFlowDirectionCode EnergyFlowDirection { get; set; }
        public virtual GridArea GridArea { get; set; }
        public virtual MarketParty GridOperator { get; set; }
        public virtual DictionaryMarketSegmentCode MarketSegment { get; set; }
        public virtual DictionaryPhysicalCapacityCode PhysicalCapacity { get; set; }
        public virtual DictionaryEnergyProductTypeCode ProductType { get; set; }
        public virtual DictionaryEnergyUsageProfileCode ProfileCategory { get; set; }
    }
}
