﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class ICARContext : DbContext
    {
        private readonly string _connectionString;

        public ICARContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ICARContext(DbContextOptions<ICARContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<AddressHistory> AddressHistory { get; set; }
        public virtual DbSet<CapTarCode> CapTarCode { get; set; }
        public virtual DbSet<CcuHistory> CcuHistory { get; set; }
        public virtual DbSet<CcuStatusHistory> CcuStatusHistory { get; set; }
        public virtual DbSet<DictionaryCommunicationStatusCode> DictionaryCommunicationStatusCode { get; set; }
        public virtual DbSet<DictionaryEdsnmessageType> DictionaryEdsnmessageType { get; set; }
        public virtual DbSet<DictionaryEnergyAllocationMethodCode> DictionaryEnergyAllocationMethodCode { get; set; }
        public virtual DbSet<DictionaryEnergyConnectionPhysicalStatusCode> DictionaryEnergyConnectionPhysicalStatusCode { get; set; }
        public virtual DbSet<DictionaryEnergyDeliveryStatusCode> DictionaryEnergyDeliveryStatusCode { get; set; }
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCode { get; set; }
        public virtual DbSet<DictionaryEnergyMeterTypeCode> DictionaryEnergyMeterTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyMeteringMethodCode> DictionaryEnergyMeteringMethodCode { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCode { get; set; }
        public virtual DbSet<DictionaryMarketPartyRoleCode> DictionaryMarketPartyRoleCode { get; set; }
        public virtual DbSet<DictionaryMarketSegmentCode> DictionaryMarketSegmentCode { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCode { get; set; }
        public virtual DbSet<DictionaryMutationReasonCode> DictionaryMutationReasonCode { get; set; }
        public virtual DbSet<DictionaryPhysicalCapacityCode> DictionaryPhysicalCapacityCode { get; set; }
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatus { get; set; }
        public virtual DbSet<FallbackMeteringPoint> FallbackMeteringPoint { get; set; }
        public virtual DbSet<GridArea> GridArea { get; set; }
        public virtual DbSet<MarketParty> MarketParty { get; set; }
        public virtual DbSet<MduHistory> MduHistory { get; set; }
        public virtual DbSet<MduMutation> MduMutation { get; set; }
        public virtual DbSet<MduRegister> MduRegister { get; set; }
        public virtual DbSet<MduStatusHistory> MduStatusHistory { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoint { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<NleXmls> NleXmls { get; set; }
        public virtual DbSet<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
        public virtual DbSet<PreSwitchRegister> PreSwitchRegister { get; set; }
        public virtual DbSet<Register> Register { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address", "dbo");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Zipcode, e.BuildingNr, e.ExBuildingNr })
                    .HasName("Search_ZipCode_BuildingNr");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<AddressHistory>(entity =>
            {
                entity.ToTable("AddressHistory", "dbo");

                entity.HasIndex(e => e.AddressId)
                    .HasName("IX_AddressId");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.User).HasMaxLength(100);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.AddressHistory)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.AddressChange_dbo.Address_AddressId");
            });

            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.ToTable("CapTarCode", "dbo");

                entity.HasIndex(e => e.Ean)
                    .HasName("IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(13);
            });

            modelBuilder.Entity<CcuHistory>(entity =>
            {
                entity.ToTable("CcuHistory", "dbo");

                entity.HasIndex(e => e.DossierId)
                    .HasName("IX_DossierId");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId");

                entity.HasIndex(e => e.StatusId)
                    .HasName("IX_StatusId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.XmlHeaderCreationTs)
                    .HasColumnName("XmlHeaderCreationTS")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.CcuHistoryBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.MarketParty_BalanceSupplierId");

                entity.HasOne(d => d.BalanceSupplierResponsible)
                    .WithMany(p => p.CcuHistoryBalanceSupplierResponsible)
                    .HasForeignKey(d => d.BalanceSupplierResponsibleId)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.MarketParty_BalanceSupplierResponsibleId");

                entity.HasOne(d => d.MessageType)
                    .WithMany(p => p.CcuHistory)
                    .HasForeignKey(d => d.MessageTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.Dictionary_EDSNMessageType_MessageTypeId");

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.CcuHistory)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.Dictionary_MutationReasonCode_MutationReasonId");

                entity.HasOne(d => d.OldBalanceSupplier)
                    .WithMany(p => p.CcuHistoryOldBalanceSupplier)
                    .HasForeignKey(d => d.OldBalanceSupplierId)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.MarketParty_OldBalanceSupplierId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CcuHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuHistory_dbo.Dictionary_ProcessStatus_StatusId");
            });

            modelBuilder.Entity<CcuStatusHistory>(entity =>
            {
                entity.ToTable("CcuStatusHistory", "dbo");

                entity.HasIndex(e => e.CcuHistoryId)
                    .HasName("IX_CcuHistoryId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.User).HasMaxLength(100);

                entity.HasOne(d => d.CcuHistory)
                    .WithMany(p => p.CcuStatusHistory)
                    .HasForeignKey(d => d.CcuHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuStatusHistory_dbo.CcuHistory_CcuHistoryId");

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.CcuStatusHistory)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.CcuStatusHistory_dbo.Dictionary_ProcessStatus_ProcessStatusId");
            });

            modelBuilder.Entity<DictionaryCommunicationStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_CommunicationStatusCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEdsnmessageType>(entity =>
            {
                entity.ToTable("Dictionary_EDSNMessageType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyAllocationMethodCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyAllocationMethodCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyConnectionPhysicalStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyConnectionPhysicalStatusCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyDeliveryStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyDeliveryStatusCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyMeterTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyMeterTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyMeteringMethodCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyMeteringMethodCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMarketPartyRoleCode>(entity =>
            {
                entity.ToTable("Dictionary_MarketPartyRoleCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMarketSegmentCode>(entity =>
            {
                entity.ToTable("Dictionary_MarketSegmentCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMutationReasonCode>(entity =>
            {
                entity.ToTable("Dictionary_MutationReasonCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryPhysicalCapacityCode>(entity =>
            {
                entity.ToTable("Dictionary_PhysicalCapacityCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<FallbackMeteringPoint>(entity =>
            {
                entity.ToTable("FallbackMeteringPoint", "dbo");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Zipcode, e.BuildingNr, e.ExBuildingNr })
                    .HasName("Search_ZipCode_BuildingNr");

                entity.Property(e => e.CityName).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(50);

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StreetName).HasMaxLength(100);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(50);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.EnergyFlowDirectionId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_EnergyFlowDirectionCode_EnergyFlowDirectionId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.MarketParty_GridOperatorId");

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_MarketSegmentCode_MarketSegmentId");

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.PhysicalCapacityId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_PhysicalCapacityCode_PhysicalCapacityId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.FallbackMeteringPoint)
                    .HasForeignKey(d => d.ProfileCategoryId)
                    .HasConstraintName("FK_dbo.FallbackMeteringPoint_dbo.Dictionary_EnergyUsageProfileCode_ProfileCategoryId");
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.ToTable("GridArea", "dbo");

                entity.HasIndex(e => e.Ean)
                    .HasName("IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean)
                    .HasName("IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.MarketParty)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.MarketParty_dbo.Dictionary_MarketPartyRoleCode_RoleId");
            });

            modelBuilder.Entity<MduHistory>(entity =>
            {
                entity.ToTable("MduHistory", "dbo");

                entity.HasIndex(e => e.AddressId)
                    .HasName("IX_AddressId");

                entity.HasIndex(e => new { e.EanId, e.MutationDate });

                entity.HasIndex(e => new { e.StatusId, e.MutationDate })
                    .HasName("IX_StatusId_MutationDate");

                entity.HasIndex(e => new { e.EanId, e.BalanceSupplierId, e.StatusId })
                    .HasName("IDX_ICAR_MduHistory_EANID_adhoc");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.Papean)
                    .HasColumnName("PAPEan")
                    .HasMaxLength(18);

                entity.Property(e => e.Sapeans)
                    .HasColumnName("SAPEans")
                    .HasMaxLength(189);

                entity.Property(e => e.XmlHeaderCreationTs)
                    .HasColumnName("XmlHeaderCreationTS")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Address_AddressId");

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.AllocationMethodId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyAllocationMethodCode_AllocationMethodId");

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MduHistoryBalanceResponsibleParty)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_BalanceResponsiblePartyId");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MduHistoryBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_BalanceSupplierId");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.CommunicationStatusCode)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.CommunicationStatusCodeId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_CommunicationStatusCode_CommunicationStatusCodeId");

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyDeliveryStatusCode_EnergyDeliveryStatusId");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.EnergyFlowDirectionId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyFlowDirectionCode_EnergyFlowDirectionId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MduHistoryGridOperator)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_GridOperatorId");

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_MarketSegmentCode_MarketSegmentId");

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyMeteringMethodCode_MeteringMethodId");

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MduHistoryMeteringResponsibleParty)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.MarketParty_MeteringResponsiblePartyId");

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.PhysicalCapacityId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_PhysicalCapacityCode_PhysicalCapacityId");

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyConnectionPhysicalStatusCode_PhysicalStatusId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.ProfileCategoryId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyUsageProfileCode_ProfileCategoryId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_ProcessStatus_StatusId");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_dbo.MduHistory_dbo.Dictionary_EnergyMeterTypeCode_TypeId");
            });

            modelBuilder.Entity<MduMutation>(entity =>
            {
                entity.ToTable("MduMutation", "dbo");

                entity.HasIndex(e => e.MduHistoryId)
                    .HasName("IX_MduHistoryId");

                entity.HasIndex(e => new { e.MutationReasonId, e.MduHistoryId })
                    .HasName("IX_ReasonId_MduHistoryId");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduMutation)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduMutation_dbo.MduHistory_MduHistoryId");

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.MduMutation)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduMutation_dbo.Dictionary_MutationReasonCode_MutationReasonId");
            });

            modelBuilder.Entity<MduRegister>(entity =>
            {
                entity.ToTable("MduRegister", "dbo");

                entity.HasIndex(e => e.MduHistoryId)
                    .HasName("IX_MduHistoryId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.MduHistory_MduHistoryId");

                entity.HasOne(d => d.MeasureUnit)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.MeasureUnitId)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.Dictionary_MeasureUnitCode_MeasureUnitId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.Dictionary_EnergyFlowDirectionCode_MeteringDirectionId");

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduRegister_dbo.Dictionary_EnergyTariffTypeCode_TariffTypeId");
            });

            modelBuilder.Entity<MduStatusHistory>(entity =>
            {
                entity.ToTable("MduStatusHistory", "dbo");

                entity.HasIndex(e => e.MduHistoryId)
                    .HasName("IX_MduHistoryId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.User).HasMaxLength(100);

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduStatusHistory)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduStatusHistory_dbo.MduHistory_MduHistoryId");

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.MduStatusHistory)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MduStatusHistory_dbo.Dictionary_ProcessStatus_ProcessStatusId");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint", "dbo");

                entity.HasIndex(e => e.AddressId)
                    .HasName("IX_AddressId");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.BalanceSupplierId, e.PhysicalStatusId })
                    .HasName("IX_BalanceSupplierId_PhysicalStatusId");

                entity.Property(e => e.CcModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CcValidFrom).HasColumnType("date");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MduValidFrom).HasColumnType("date");

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Papean)
                    .HasColumnName("PAPEan")
                    .HasMaxLength(18);

                entity.Property(e => e.Sapeans)
                    .HasColumnName("SAPEans")
                    .HasMaxLength(189);

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Address_AddressId");

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.AllocationMethodId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyAllocationMethodCode_AllocationMethodId");

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MeteringPointBalanceResponsibleParty)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_BalanceResponsiblePartyId");

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MeteringPointBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_BalanceSupplierId");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.CommunicationStatusCode)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.CommunicationStatusCodeId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_CommunicationStatusCode_CommunicationStatusCodeId");

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyDeliveryStatusCode_EnergyDeliveryStatusId");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.EnergyFlowDirectionId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyFlowDirectionCode_EnergyFlowDirectionId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPointGridOperator)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_GridOperatorId");

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_MarketSegmentCode_MarketSegmentId");

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyMeteringMethodCode_MeteringMethodId");

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MeteringPointMeteringResponsibleParty)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.MarketParty_MeteringResponsiblePartyId");

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.PhysicalCapacityId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_PhysicalCapacityCode_PhysicalCapacityId");

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyConnectionPhysicalStatusCode_PhysicalStatusId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProfileCategoryId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyUsageProfileCode_ProfileCategoryId");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyMeterTypeCode_TypeId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<NleXmls>(entity =>
            {
                entity.ToTable("NleXmls", "dbo");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(3);

                entity.Property(e => e.XmlName).IsRequired();
            });

            modelBuilder.Entity<PreSwitchMeteringPoint>(entity =>
            {
                entity.ToTable("PreSwitchMeteringPoint", "dbo");

                entity.HasIndex(e => e.AddressId)
                    .HasName("IX_AddressId");

                entity.HasIndex(e => e.CapTarCodeId)
                    .HasName("IX_CapTarCodeId");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => e.GridAreaId)
                    .HasName("IX_GridAreaId");

                entity.HasIndex(e => e.GridOperatorId)
                    .HasName("IX_GridOperatorId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.EapoffPeak).HasColumnName("EAPOffPeak");

                entity.Property(e => e.Eappeak).HasColumnName("EAPPeak");

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.ValidDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("('1900-01-01T00:00:00.000')");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.Address_AddressId");

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.CapTarCodeId)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.CapTarCode_CapTarCodeId");

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.GridAreaId)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.GridArea_GridAreaId");

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.GridOperatorId)
                    .HasConstraintName("FK_dbo.PreSwitchMeteringPoint_dbo.MarketParty_GridOperatorId");
            });

            modelBuilder.Entity<PreSwitchRegister>(entity =>
            {
                entity.ToTable("PreSwitchRegister", "dbo");

                entity.HasIndex(e => e.MeteringPointPreSwitchId)
                    .HasName("IX_MeteringPointPreSwitch");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringPointPreSwitch)
                    .WithMany(p => p.PreSwitchRegister)
                    .HasForeignKey(d => d.MeteringPointPreSwitchId)
                    .HasConstraintName("FK_dbo.PreSwitchRegister_dbo.PreSwitchMeteringPoint_MeteringPointPreSwitchId");
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.ToTable("Register", "dbo");

                entity.HasIndex(e => e.MeteringPointId)
                    .HasName("IX_MeteringPointId");

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeasureUnit)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeasureUnitId)
                    .HasConstraintName("FK_dbo.Register_dbo.Dictionary_MeasureUnitCode_MeasureUnitId");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Register_dbo.Dictionary_EnergyFlowDirectionCode_MeteringDirectionId");

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Register_dbo.MeteringPoint_MeteringPointId");

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Register_dbo.Dictionary_EnergyTariffTypeCode_TariffTypeId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
