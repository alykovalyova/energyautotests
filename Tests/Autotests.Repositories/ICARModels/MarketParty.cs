﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            CcuHistoryBalanceSupplier = new HashSet<CcuHistory>();
            CcuHistoryBalanceSupplierResponsible = new HashSet<CcuHistory>();
            CcuHistoryOldBalanceSupplier = new HashSet<CcuHistory>();
            FallbackMeteringPoint = new HashSet<FallbackMeteringPoint>();
            MduHistoryBalanceResponsibleParty = new HashSet<MduHistory>();
            MduHistoryBalanceSupplier = new HashSet<MduHistory>();
            MduHistoryGridOperator = new HashSet<MduHistory>();
            MduHistoryMeteringResponsibleParty = new HashSet<MduHistory>();
            MeteringPointBalanceResponsibleParty = new HashSet<MeteringPoint>();
            MeteringPointBalanceSupplier = new HashSet<MeteringPoint>();
            MeteringPointGridOperator = new HashSet<MeteringPoint>();
            MeteringPointMeteringResponsibleParty = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoint = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string Ean { get; set; }
        public string Name { get; set; }
        public byte? RoleId { get; set; }

        public virtual DictionaryMarketPartyRoleCode Role { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryBalanceSupplier { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryBalanceSupplierResponsible { get; set; }
        public virtual ICollection<CcuHistory> CcuHistoryOldBalanceSupplier { get; set; }
        public virtual ICollection<FallbackMeteringPoint> FallbackMeteringPoint { get; set; }
        public virtual ICollection<MduHistory> MduHistoryBalanceResponsibleParty { get; set; }
        public virtual ICollection<MduHistory> MduHistoryBalanceSupplier { get; set; }
        public virtual ICollection<MduHistory> MduHistoryGridOperator { get; set; }
        public virtual ICollection<MduHistory> MduHistoryMeteringResponsibleParty { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointBalanceResponsibleParty { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointBalanceSupplier { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointGridOperator { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointMeteringResponsibleParty { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
    }
}
