﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class MduMutation
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public string ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public byte MutationReasonId { get; set; }
        public string DossierId { get; set; }

        public virtual MduHistory MduHistory { get; set; }
        public virtual DictionaryMutationReasonCode MutationReason { get; set; }
    }
}
