﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class MduStatusHistory
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public byte ProcessStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Comment { get; set; }
        public string User { get; set; }

        public virtual MduHistory MduHistory { get; set; }
        public virtual DictionaryProcessStatus ProcessStatus { get; set; }
    }
}
