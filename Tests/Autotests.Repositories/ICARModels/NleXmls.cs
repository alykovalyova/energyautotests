﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class NleXmls
    {
        public int Id { get; set; }
        public string XmlName { get; set; }
        public string Status { get; set; }
    }
}
