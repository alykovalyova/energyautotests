﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ICARModels
{
    public partial class PreSwitchRegister
    {
        public int Id { get; set; }
        public int MeteringPointPreSwitchId { get; set; }
        public string EdsnId { get; set; }
        public byte? TariffTypeId { get; set; }
        public byte? MeteringDirectionId { get; set; }
        public byte NrOfDigits { get; set; }
        public decimal? MultiplicationFactor { get; set; }

        public virtual PreSwitchMeteringPoint MeteringPointPreSwitch { get; set; }
    }
}
