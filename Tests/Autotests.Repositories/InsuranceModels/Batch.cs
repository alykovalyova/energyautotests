﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class Batch
    {
        public Batch()
        {
            Lead = new HashSet<Lead>();
        }

        public int Id { get; set; }
        public string Comment { get; set; }
        public short BatchStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual DictionaryBatchStatus BatchStatus { get; set; }
        public virtual ICollection<Lead> Lead { get; set; }
    }
}
