﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class CustomerProduct
    {
        public int Id { get; set; }
        public string PolicyNumber { get; set; }
        public int CustomerId { get; set; }
        public short ProductTypeId { get; set; }
        public decimal Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public short? ProductChangeReasonId { get; set; }
        public short ProductSpecificationId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ProductChangeReason ProductChangeReason { get; set; }
        public virtual ProductSpecification ProductSpecification { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
    }
}
