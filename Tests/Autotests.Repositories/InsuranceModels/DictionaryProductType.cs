﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class DictionaryProductType
    {
        public DictionaryProductType()
        {
            CustomerProduct = new HashSet<CustomerProduct>();
            OfferProduct = new HashSet<OfferProduct>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProduct { get; set; }
        public virtual ICollection<OfferProduct> OfferProduct { get; set; }
    }
}
