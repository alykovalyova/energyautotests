﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class InsuranceContext : DbContext
    {
        private readonly string _connectionString;

        public InsuranceContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public InsuranceContext(DbContextOptions<InsuranceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Batch> Batch { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerProduct> CustomerProduct { get; set; }
        public virtual DbSet<DictionaryBatchStatus> DictionaryBatchStatus { get; set; }
        public virtual DbSet<DictionaryCampaignType> DictionaryCampaignType { get; set; }
        public virtual DbSet<DictionaryFamilySize> DictionaryFamilySize { get; set; }
        public virtual DbSet<DictionaryGender> DictionaryGender { get; set; }
        public virtual DbSet<DictionaryOwnership> DictionaryOwnership { get; set; }
        public virtual DbSet<DictionaryProductType> DictionaryProductType { get; set; }
        public virtual DbSet<Lead> Lead { get; set; }
        public virtual DbSet<LeadEnergyProduct> LeadEnergyProduct { get; set; }
        public virtual DbSet<Offer> Offer { get; set; }
        public virtual DbSet<OfferProduct> OfferProduct { get; set; }
        public virtual DbSet<ProductChangeReason> ProductChangeReason { get; set; }
        public virtual DbSet<ProductSpecification> ProductSpecification { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Batch>(entity =>
            {
                entity.HasIndex(e => e.BatchStatusId);

                entity.Property(e => e.Comment).HasMaxLength(255);

                entity.HasOne(d => d.BatchStatus)
                    .WithMany(p => p.Batch)
                    .HasForeignKey(d => d.BatchStatusId);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.FamilySizeId);

                entity.HasIndex(e => e.LeadId);

                entity.HasIndex(e => e.OwnershipId);

                entity.HasIndex(e => e.RelationId)
                    .IsUnique();

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.GrossPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.HouseNumberExtension).HasMaxLength(50);

                entity.Property(e => e.NetPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.ZipCode).HasMaxLength(6);

                entity.HasOne(d => d.FamilySize)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.FamilySizeId);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Ownership)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.OwnershipId);
            });

            modelBuilder.Entity<CustomerProduct>(entity =>
            {
                entity.HasIndex(e => e.CustomerId);

                entity.HasIndex(e => e.PolicyNumber)
                    .IsUnique();

                entity.HasIndex(e => e.ProductChangeReasonId);

                entity.HasIndex(e => e.ProductSpecificationId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.Property(e => e.PolicyNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductChangeReason)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.ProductChangeReasonId);

                entity.HasOne(d => d.ProductSpecification)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.ProductSpecificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.CustomerProduct)
                    .HasForeignKey(d => d.ProductTypeId);
            });

            modelBuilder.Entity<DictionaryBatchStatus>(entity =>
            {
                entity.ToTable("Dictionary_BatchStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryCampaignType>(entity =>
            {
                entity.ToTable("Dictionary_CampaignType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryFamilySize>(entity =>
            {
                entity.ToTable("Dictionary_FamilySize");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryGender>(entity =>
            {
                entity.ToTable("Dictionary_Gender");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryOwnership>(entity =>
            {
                entity.ToTable("Dictionary_Ownership");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProductType>(entity =>
            {
                entity.ToTable("Dictionary_ProductType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<Lead>(entity =>
            {
                entity.HasIndex(e => e.BatchId);

                entity.HasIndex(e => e.EnergyProductId);

                entity.HasIndex(e => e.FamilySizeId);

                entity.HasIndex(e => e.GenderId);

                entity.HasIndex(e => e.OwnershipId);

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.HouseNumberExtensionLetter).HasMaxLength(10);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.HasOne(d => d.Batch)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.BatchId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyProduct)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.EnergyProductId);

                entity.HasOne(d => d.FamilySize)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.FamilySizeId);

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.GenderId);

                entity.HasOne(d => d.Ownership)
                    .WithMany(p => p.Lead)
                    .HasForeignKey(d => d.OwnershipId);
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.HasIndex(e => e.CampaignTypeId);

                entity.HasIndex(e => e.LeadId);

                entity.Property(e => e.Comment).HasMaxLength(255);

                entity.Property(e => e.Discount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.GrossPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.NetPrice).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.TrackingId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CampaignType)
                    .WithMany(p => p.Offer)
                    .HasForeignKey(d => d.CampaignTypeId);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.Offer)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<OfferProduct>(entity =>
            {
                entity.HasIndex(e => e.OfferId);

                entity.HasIndex(e => e.ProductSpecificationId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 4)");

                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.OfferProduct)
                    .HasForeignKey(d => d.OfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductSpecification)
                    .WithMany(p => p.OfferProduct)
                    .HasForeignKey(d => d.ProductSpecificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.OfferProduct)
                    .HasForeignKey(d => d.ProductTypeId);
            });

            modelBuilder.Entity<ProductChangeReason>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(100);
            });

            modelBuilder.Entity<ProductSpecification>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
