﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class LeadEnergyProduct
    {
        public LeadEnergyProduct()
        {
            Lead = new HashSet<Lead>();
        }

        public int Id { get; set; }
        public int? ElkUsageTotal { get; set; }
        public int? GasUsageTotal { get; set; }

        public virtual ICollection<Lead> Lead { get; set; }
    }
}
