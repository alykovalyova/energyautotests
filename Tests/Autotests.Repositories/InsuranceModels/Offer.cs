﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class Offer
    {
        public Offer()
        {
            OfferProduct = new HashSet<OfferProduct>();
        }

        public int Id { get; set; }
        public int LeadId { get; set; }
        public short CampaignTypeId { get; set; }
        public string TrackingId { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal NetPrice { get; set; }
        public decimal Discount { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual DictionaryCampaignType CampaignType { get; set; }
        public virtual Lead Lead { get; set; }
        public virtual ICollection<OfferProduct> OfferProduct { get; set; }
    }
}
