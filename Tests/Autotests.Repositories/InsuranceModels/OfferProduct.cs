﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class OfferProduct
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public short ProductTypeId { get; set; }
        public short ProductSpecificationId { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal Price { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual ProductSpecification ProductSpecification { get; set; }
        public virtual DictionaryProductType ProductType { get; set; }
    }
}
