﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.InsuranceModels
{
    public partial class ProductChangeReason
    {
        public ProductChangeReason()
        {
            CustomerProduct = new HashSet<CustomerProduct>();
        }

        public short Id { get; set; }
        public int ExternalKey { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProduct { get; set; }
    }
}
