﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MMVDBModels
{
    public partial class MMVDBContext : DbContext
    {
        private readonly string _connectionString;

        public MMVDBContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MMVDBContext(DbContextOptions<MMVDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<Voucher> Voucher { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.ToTable("Voucher", "dbo");

                entity.HasIndex(e => e.CustomerSurname)
                    .HasName("IX_CustomerSurname");

                entity.HasIndex(e => e.FtpFileName)
                    .HasName("IX_FtpFileName")
                    .IsUnique();

                entity.HasIndex(e => e.ReservedOn);

                entity.HasIndex(e => new { e.CanceledOn, e.RestValue })
                    .HasName("IX_GetCanceled");

                entity.HasIndex(e => new { e.VoucherCode, e.ReservedOn, e.CanceledOn, e.VoucherAmount })
                    .HasName("IX_ReserveVoucher");

                entity.Property(e => e.CanceledOn).HasColumnType("datetime");

                entity.Property(e => e.CephKey).IsRequired();

                entity.Property(e => e.ClosedOn).HasColumnType("datetime");

                entity.Property(e => e.ContractExBuildingNr).HasMaxLength(50);

                entity.Property(e => e.ContractZipcode)
                    .HasColumnName("ContractZIPCode")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerBirthDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerIban).HasMaxLength(20);

                entity.Property(e => e.CustomerInitials).HasMaxLength(20);

                entity.Property(e => e.CustomerSurname).HasMaxLength(50);

                entity.Property(e => e.DistributedOn).HasColumnType("datetime");

                entity.Property(e => e.FtpFileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ReservedOn).HasColumnType("datetime");

                entity.Property(e => e.RestValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.SentToMediaMarketOn).HasColumnType("datetime");

                entity.Property(e => e.VoucherAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VoucherCode)
                    .IsRequired()
                    .HasMaxLength(13);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
