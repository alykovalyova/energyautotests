﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MMVDBModels
{
    public partial class Voucher
    {
        public int Id { get; set; }
        public string FtpFileName { get; set; }
        public string CephKey { get; set; }
        public decimal VoucherAmount { get; set; }
        public string VoucherCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ReservedOn { get; set; }
        public DateTime? DistributedOn { get; set; }
        public DateTime? CanceledOn { get; set; }
        public decimal? RestValue { get; set; }
        public DateTime? SentToMediaMarketOn { get; set; }
        public string RestValueFileName { get; set; }
        public string ReceivedRestValueCephKey { get; set; }
        public DateTime? ClosedOn { get; set; }
        public string CustomerSurname { get; set; }
        public string CustomerInitials { get; set; }
        public DateTime? CustomerBirthDate { get; set; }
        public string CustomerIban { get; set; }
        public int? ContractBuildingNr { get; set; }
        public string ContractExBuildingNr { get; set; }
        public string ContractZipcode { get; set; }
        public bool? CanceledManually { get; set; }
        public string SentRestValueCephKey { get; set; }
    }
}
