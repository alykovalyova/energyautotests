﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MarketPartyModels
{
    public partial class DictionaryStatusCode
    {
        public DictionaryStatusCode()
        {
            MarketPartyInfo = new HashSet<MarketPartyInfo>();
            StatusHistory = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MarketPartyInfo> MarketPartyInfo { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
    }
}
