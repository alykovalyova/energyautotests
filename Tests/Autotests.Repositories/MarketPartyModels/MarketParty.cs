﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MarketPartyModels
{
    public partial class MarketParty
    {
        public short Id { get; set; }
        public string Ean { get; set; }
        public string Name { get; set; }
        public byte? RoleId { get; set; }
    }
}
