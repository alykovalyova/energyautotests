﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MarketPartyModels
{
    public partial class MarketPartyContext : DbContext
    {
        private readonly string _connectionString;

        public MarketPartyContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MarketPartyContext(DbContextOptions<MarketPartyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryMarketPartyRoleCode> DictionaryMarketPartyRoleCode { get; set; }
        public virtual DbSet<DictionaryStatusCode> DictionaryStatusCode { get; set; }
        public virtual DbSet<MarketParty> MarketParty { get; set; }
        public virtual DbSet<MarketPartyEan> MarketPartyEan { get; set; }
        public virtual DbSet<MarketPartyInfo> MarketPartyInfo { get; set; }
        public virtual DbSet<StatusHistory> StatusHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryMarketPartyRoleCode>(entity =>
            {
                entity.ToTable("Dictionary_MarketPartyRoleCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_StatusCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<MarketPartyEan>(entity =>
            {
                entity.ToTable("MarketPartyEan", "dbo");

                entity.HasIndex(e => e.MarketPartyInfoId);

                entity.HasOne(d => d.MarketPartyInfo)
                    .WithMany(p => p.MarketPartyEan)
                    .HasForeignKey(d => d.MarketPartyInfoId);
            });

            modelBuilder.Entity<MarketPartyInfo>(entity =>
            {
                entity.ToTable("MarketPartyInfo", "dbo");

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.HasOne(d => d.LastStatus)
                    .WithMany(p => p.MarketPartyInfo)
                    .HasForeignKey(d => d.LastStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MarketPartyInfo_Dictionary_StatusCode_Id");
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.ToTable("StatusHistory", "dbo");

                entity.HasIndex(e => e.MarketPartyInfoId);

                entity.HasOne(d => d.MarketPartyInfo)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.MarketPartyInfoId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StatusHistory_Dictionary_StatusCode_Id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
