﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MarketPartyModels
{
    public partial class MarketPartyEan
    {
        public int Id { get; set; }
        public int MarketPartyInfoId { get; set; }
        public string Ean { get; set; }

        public virtual MarketPartyInfo MarketPartyInfo { get; set; }
    }
}
