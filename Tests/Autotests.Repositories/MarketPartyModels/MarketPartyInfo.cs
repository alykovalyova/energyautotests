﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MarketPartyModels
{
    public partial class MarketPartyInfo
    {
        public MarketPartyInfo()
        {
            MarketPartyEan = new HashSet<MarketPartyEan>();
            StatusHistory = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string MarketPartyName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string RequestType { get; set; }
        public string Other { get; set; }
        public int LastStatusId { get; set; }
        public bool AvailableForIc92 { get; set; }

        public virtual DictionaryStatusCode LastStatus { get; set; }
        public virtual ICollection<MarketPartyEan> MarketPartyEan { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
    }
}
