﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class DictionaryMasterDataBatchStatus
    {
        public DictionaryMasterDataBatchStatus()
        {
            MasterDataBatch = new HashSet<MasterDataBatch>();
            MasterDataBatchStatusHistory = new HashSet<MasterDataBatchStatusHistory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MasterDataBatch> MasterDataBatch { get; set; }
        public virtual ICollection<MasterDataBatchStatusHistory> MasterDataBatchStatusHistory { get; set; }
    }
}
