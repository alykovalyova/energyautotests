﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class EdsnRejection
    {
        public int Id { get; set; }
        public byte? RejectionLevelId { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }

        public virtual MasterDataBatchStatusHistory IdNavigation { get; set; }
        public virtual DictionaryEdsnRejectionLevel RejectionLevel { get; set; }
    }
}
