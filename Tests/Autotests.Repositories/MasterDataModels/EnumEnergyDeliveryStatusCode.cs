﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class EnumEnergyDeliveryStatusCode
    {
        public EnumEnergyDeliveryStatusCode()
        {
            MduHistory = new HashSet<MduHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }

        public virtual ICollection<MduHistory> MduHistory { get; set; }
    }
}
