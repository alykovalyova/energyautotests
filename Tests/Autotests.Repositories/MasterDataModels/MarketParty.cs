﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            MduHistoryBalanceResponsibleParty = new HashSet<MduHistory>();
            MduHistoryBalanceSupplier = new HashSet<MduHistory>();
            MduHistoryGridOperator = new HashSet<MduHistory>();
            MduHistoryMeteringResponsibleParty = new HashSet<MduHistory>();
        }

        public short Id { get; set; }
        public string Ean { get; set; }

        public virtual ICollection<MduHistory> MduHistoryBalanceResponsibleParty { get; set; }
        public virtual ICollection<MduHistory> MduHistoryBalanceSupplier { get; set; }
        public virtual ICollection<MduHistory> MduHistoryGridOperator { get; set; }
        public virtual ICollection<MduHistory> MduHistoryMeteringResponsibleParty { get; set; }
    }
}
