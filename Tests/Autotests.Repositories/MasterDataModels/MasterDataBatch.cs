﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class MasterDataBatch
    {
        public MasterDataBatch()
        {
            MasterDataBatchStatusHistory = new HashSet<MasterDataBatchStatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public byte StatusId { get; set; }
        public string GridOperatorEan { get; set; }
        public string BalanceSupplierEan { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual DictionaryMasterDataBatchStatus Status { get; set; }
        public virtual ICollection<MasterDataBatchStatusHistory> MasterDataBatchStatusHistory { get; set; }
    }
}
