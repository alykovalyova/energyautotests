﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class MasterDataBatchStatusHistory
    {
        public int Id { get; set; }
        public int MasterDataBatchId { get; set; }
        public byte StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Comment { get; set; }

        public virtual MasterDataBatch MasterDataBatch { get; set; }
        public virtual DictionaryMasterDataBatchStatus Status { get; set; }
        public virtual EdsnRejection EdsnRejection { get; set; }
    }
}
