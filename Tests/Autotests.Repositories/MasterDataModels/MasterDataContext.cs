﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class MasterDataContext : DbContext
    {
        private readonly string _connectionString;

        public MasterDataContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MasterDataContext(DbContextOptions<MasterDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CapTarCode> CapTarCode { get; set; }
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevel { get; set; }
        public virtual DbSet<DictionaryMasterDataBatchStatus> DictionaryMasterDataBatchStatus { get; set; }
        public virtual DbSet<EdsnRejection> EdsnRejection { get; set; }
        public virtual DbSet<EnumCommunicationStatusCode> EnumCommunicationStatusCode { get; set; }
        public virtual DbSet<EnumEnergyAllocationMethodCode> EnumEnergyAllocationMethodCode { get; set; }
        public virtual DbSet<EnumEnergyConnectionPhysicalStatusCode> EnumEnergyConnectionPhysicalStatusCode { get; set; }
        public virtual DbSet<EnumEnergyDeliveryStatusCode> EnumEnergyDeliveryStatusCode { get; set; }
        public virtual DbSet<EnumEnergyFlowDirectionCode> EnumEnergyFlowDirectionCode { get; set; }
        public virtual DbSet<EnumEnergyMeterTypeCode> EnumEnergyMeterTypeCode { get; set; }
        public virtual DbSet<EnumEnergyMeteringMethodCode> EnumEnergyMeteringMethodCode { get; set; }
        public virtual DbSet<EnumEnergyProductTypeCode> EnumEnergyProductTypeCode { get; set; }
        public virtual DbSet<EnumEnergyTariffTypeCode> EnumEnergyTariffTypeCode { get; set; }
        public virtual DbSet<EnumEnergyUsageProfileCode> EnumEnergyUsageProfileCode { get; set; }
        public virtual DbSet<EnumMarketSegmentCode> EnumMarketSegmentCode { get; set; }
        public virtual DbSet<EnumMutationReasonCode> EnumMutationReasonCode { get; set; }
        public virtual DbSet<EnumPhysicalCapacityCode> EnumPhysicalCapacityCode { get; set; }
        public virtual DbSet<EnumProcessStatus> EnumProcessStatus { get; set; }
        public virtual DbSet<GridArea> GridArea { get; set; }
        public virtual DbSet<MarketParty> MarketParty { get; set; }
        public virtual DbSet<MasterDataBatch> MasterDataBatch { get; set; }
        public virtual DbSet<MasterDataBatchStatusHistory> MasterDataBatchStatusHistory { get; set; }
        public virtual DbSet<MduHistory> MduHistory { get; set; }
        public virtual DbSet<MduMutation> MduMutation { get; set; }
        public virtual DbSet<MduRegister> MduRegister { get; set; }
        public virtual DbSet<MduStatusHistory> MduStatusHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.ToTable("CapTarCode", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(13);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMasterDataBatchStatus>(entity =>
            {
                entity.ToTable("Dictionary_MasterDataBatchStatus", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.ToTable("EdsnRejection", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Code).HasMaxLength(10);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.EdsnRejection)
                    .HasForeignKey<EdsnRejection>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.RejectionLevel)
                    .WithMany(p => p.EdsnRejection)
                    .HasForeignKey(d => d.RejectionLevelId)
                    .HasConstraintName("FK_dbo.EdsnRejection_dbo.Dictionary_EdsnRejectionLevel_RejectionLevelId");
            });

            modelBuilder.Entity<EnumCommunicationStatusCode>(entity =>
            {
                entity.ToTable("EnumCommunicationStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyAllocationMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyAllocationMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyConnectionPhysicalStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyConnectionPhysicalStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyDeliveryStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyDeliveryStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("EnumEnergyFlowDirectionCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeterTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeterTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeteringMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeteringMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyProductTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyTariffTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("EnumEnergyUsageProfileCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMarketSegmentCode>(entity =>
            {
                entity.ToTable("EnumMarketSegmentCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMutationReasonCode>(entity =>
            {
                entity.ToTable("EnumMutationReasonCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumPhysicalCapacityCode>(entity =>
            {
                entity.ToTable("EnumPhysicalCapacityCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumProcessStatus>(entity =>
            {
                entity.ToTable("EnumProcessStatus", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.ToTable("GridArea", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique();

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(13);
            });

            modelBuilder.Entity<MasterDataBatch>(entity =>
            {
                entity.ToTable("MasterDataBatch", "dbo");

                entity.Property(e => e.BalanceSupplierEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.GridOperatorEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MasterDataBatch)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MasterDataBatchStatus_dbo.Dictionary_MasterDataBatchStatus_StatusId");
            });

            modelBuilder.Entity<MasterDataBatchStatusHistory>(entity =>
            {
                entity.ToTable("MasterDataBatchStatusHistory", "dbo");

                entity.HasIndex(e => e.MasterDataBatchId);

                entity.HasOne(d => d.MasterDataBatch)
                    .WithMany(p => p.MasterDataBatchStatusHistory)
                    .HasForeignKey(d => d.MasterDataBatchId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MasterDataBatchStatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MasterDataBatchStatusHistory_dbo.Dictionary_MasterDataBatchStatus_StatusId");
            });

            modelBuilder.Entity<MduHistory>(entity =>
            {
                entity.ToTable("MduHistory", "dbo");

                entity.HasIndex(e => e.AllocationMethodId);

                entity.HasIndex(e => e.BalanceResponsiblePartyId);

                entity.HasIndex(e => e.BalanceSupplierId);

                entity.HasIndex(e => e.CapTarCodeId);

                entity.HasIndex(e => e.CommunicationStatusCodeId);

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId");

                entity.HasIndex(e => e.EnergyDeliveryStatusId);

                entity.HasIndex(e => e.EnergyFlowDirectionId);

                entity.HasIndex(e => e.GridAreaId);

                entity.HasIndex(e => e.GridOperatorId);

                entity.HasIndex(e => e.MarketSegmentId);

                entity.HasIndex(e => e.MeteringMethodId);

                entity.HasIndex(e => e.MeteringResponsiblePartyId);

                entity.HasIndex(e => e.PhysicalCapacityId);

                entity.HasIndex(e => e.PhysicalStatusId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.HasIndex(e => e.ProfileCategoryId);

                entity.HasIndex(e => e.TypeId);

                entity.HasIndex(e => new { e.StatusId, e.MutationDate })
                    .HasName("IX_StatusId_MutationDate");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.Papean)
                    .HasColumnName("PAPEan")
                    .HasMaxLength(18);

                entity.Property(e => e.Sapeans)
                    .HasColumnName("SAPEans")
                    .HasMaxLength(189);

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.AllocationMethodId);

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MduHistoryBalanceResponsibleParty)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId);

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MduHistoryBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatusCode)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.CommunicationStatusCodeId);

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MduHistoryGridOperator)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MduHistoryMeteringResponsibleParty)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId);

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.PhysicalCapacityId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.ProfileCategoryId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MduHistory)
                    .HasForeignKey(d => d.TypeId);
            });

            modelBuilder.Entity<MduMutation>(entity =>
            {
                entity.ToTable("MduMutation", "dbo");

                entity.HasIndex(e => e.MduHistoryId)
                    .HasName("IX_MduHistoryId");

                entity.HasIndex(e => new { e.MutationReasonId, e.MduHistoryId })
                    .HasName("IX_ReasonId_MduHistoryId");

                entity.Property(e => e.DossierId).HasMaxLength(50);

                entity.Property(e => e.ExternalReference).HasMaxLength(100);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduMutation)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MutationReason)
                    .WithMany(p => p.MduMutation)
                    .HasForeignKey(d => d.MutationReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MduRegister>(entity =>
            {
                entity.ToTable("MduRegister", "dbo");

                entity.HasIndex(e => e.MduHistoryId)
                    .HasName("IX_MduHistoryId");

                entity.HasIndex(e => e.MeteringDirectionId);

                entity.HasIndex(e => e.TariffTypeId);

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.MduRegister)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MduStatusHistory>(entity =>
            {
                entity.ToTable("MduStatusHistory", "dbo");

                entity.HasIndex(e => e.MduHistoryId)
                    .HasName("IX_MduHistoryId");

                entity.HasIndex(e => e.ProcessStatusId);

                entity.Property(e => e.User).HasMaxLength(100);

                entity.HasOne(d => d.MduHistory)
                    .WithMany(p => p.MduStatusHistory)
                    .HasForeignKey(d => d.MduHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProcessStatus)
                    .WithMany(p => p.MduStatusHistory)
                    .HasForeignKey(d => d.ProcessStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
