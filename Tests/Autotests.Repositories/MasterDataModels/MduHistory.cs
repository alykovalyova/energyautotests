﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class MduHistory
    {
        public MduHistory()
        {
            MduMutation = new HashSet<MduMutation>();
            MduRegister = new HashSet<MduRegister>();
            MduStatusHistory = new HashSet<MduStatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
        public short? GridOperatorId { get; set; }
        public short? GridAreaId { get; set; }
        public string LocationDescription { get; set; }
        public short MarketSegmentId { get; set; }
        public short ProductTypeId { get; set; }
        public short? BalanceSupplierId { get; set; }
        public short? BalanceResponsiblePartyId { get; set; }
        public short? MeteringResponsiblePartyId { get; set; }
        public bool HasCommercialCharacteristics { get; set; }
        public short? AllocationMethodId { get; set; }
        public short? CapTarCodeId { get; set; }
        public string ContractedCapacity { get; set; }
        public int? EacPeak { get; set; }
        public int? EacOffPeak { get; set; }
        public short EnergyDeliveryStatusId { get; set; }
        public short? EnergyFlowDirectionId { get; set; }
        public short MeteringMethodId { get; set; }
        public short? PhysicalCapacityId { get; set; }
        public short PhysicalStatusId { get; set; }
        public short? ProfileCategoryId { get; set; }
        public string InvoiceMonth { get; set; }
        public string MaxConsumption { get; set; }
        public string MeterEdsnId { get; set; }
        public short? NrOfRegisters { get; set; }
        public short? TypeId { get; set; }
        public bool? TemperatureCorrection { get; set; }
        public short? CommunicationStatusCodeId { get; set; }
        public short StatusId { get; set; }
        public string LastComment { get; set; }
        public Guid XmlHeaderMessageId { get; set; }
        public DateTime XmlHeaderCreationTs { get; set; }
        public string Papean { get; set; }
        public string Sapeans { get; set; }
        public DateTime MutationDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Zipcode { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public int? EapOffPeak { get; set; }
        public int? EapPeak { get; set; }

        public virtual EnumEnergyAllocationMethodCode AllocationMethod { get; set; }
        public virtual MarketParty BalanceResponsibleParty { get; set; }
        public virtual MarketParty BalanceSupplier { get; set; }
        public virtual CapTarCode CapTarCode { get; set; }
        public virtual EnumCommunicationStatusCode CommunicationStatusCode { get; set; }
        public virtual EnumEnergyDeliveryStatusCode EnergyDeliveryStatus { get; set; }
        public virtual EnumEnergyFlowDirectionCode EnergyFlowDirection { get; set; }
        public virtual GridArea GridArea { get; set; }
        public virtual MarketParty GridOperator { get; set; }
        public virtual EnumMarketSegmentCode MarketSegment { get; set; }
        public virtual EnumEnergyMeteringMethodCode MeteringMethod { get; set; }
        public virtual MarketParty MeteringResponsibleParty { get; set; }
        public virtual EnumPhysicalCapacityCode PhysicalCapacity { get; set; }
        public virtual EnumEnergyConnectionPhysicalStatusCode PhysicalStatus { get; set; }
        public virtual EnumEnergyProductTypeCode ProductType { get; set; }
        public virtual EnumEnergyUsageProfileCode ProfileCategory { get; set; }
        public virtual EnumProcessStatus Status { get; set; }
        public virtual EnumEnergyMeterTypeCode Type { get; set; }
        public virtual ICollection<MduMutation> MduMutation { get; set; }
        public virtual ICollection<MduRegister> MduRegister { get; set; }
        public virtual ICollection<MduStatusHistory> MduStatusHistory { get; set; }
    }
}
