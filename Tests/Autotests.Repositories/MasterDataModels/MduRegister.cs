﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MasterDataModels
{
    public partial class MduRegister
    {
        public int Id { get; set; }
        public int MduHistoryId { get; set; }
        public string EdsnId { get; set; }
        public short TariffTypeId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short NrOfDigits { get; set; }
        public decimal? MultiplicationFactor { get; set; }

        public virtual MduHistory MduHistory { get; set; }
        public virtual EnumEnergyFlowDirectionCode MeteringDirection { get; set; }
        public virtual EnumEnergyTariffTypeCode TariffType { get; set; }
    }
}
