﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class Address
    {
        public Address()
        {
            AddressHistory = new HashSet<AddressHistory>();
            MeteringPoint = new HashSet<MeteringPoint>();
            MeteringPointHistory = new HashSet<MeteringPointHistory>();
            PreSwitchMeteringPoint = new HashSet<PreSwitchMeteringPoint>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Zipcode { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool IsChangedByAgent { get; set; }
        public string BagBuildingId { get; set; }
        public string BagId { get; set; }

        public virtual ICollection<AddressHistory> AddressHistory { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistory { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
    }
}
