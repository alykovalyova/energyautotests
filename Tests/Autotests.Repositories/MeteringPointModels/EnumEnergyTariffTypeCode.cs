﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class EnumEnergyTariffTypeCode
    {
        public EnumEnergyTariffTypeCode()
        {
            PreSwitchRegister = new HashSet<PreSwitchRegister>();
            Register = new HashSet<Register>();
            RegisterHistory = new HashSet<RegisterHistory>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }

        public virtual ICollection<PreSwitchRegister> PreSwitchRegister { get; set; }
        public virtual ICollection<Register> Register { get; set; }
        public virtual ICollection<RegisterHistory> RegisterHistory { get; set; }
    }
}
