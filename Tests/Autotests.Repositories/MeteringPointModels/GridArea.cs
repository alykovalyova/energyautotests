﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class GridArea
    {
        public GridArea()
        {
            MeteringPoint = new HashSet<MeteringPoint>();
            MeteringPointHistory = new HashSet<MeteringPointHistory>();
            PreSwitchMeteringPoint = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string Ean { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistory { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
    }
}
