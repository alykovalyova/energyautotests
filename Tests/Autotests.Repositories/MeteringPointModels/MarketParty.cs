﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class MarketParty
    {
        public MarketParty()
        {
            MeteringPointBalanceResponsibleParty = new HashSet<MeteringPoint>();
            MeteringPointBalanceSupplier = new HashSet<MeteringPoint>();
            MeteringPointGridOperator = new HashSet<MeteringPoint>();
            MeteringPointHistoryBalanceResponsibleParty = new HashSet<MeteringPointHistory>();
            MeteringPointHistoryBalanceSupplier = new HashSet<MeteringPointHistory>();
            MeteringPointHistoryGridOperator = new HashSet<MeteringPointHistory>();
            MeteringPointHistoryMeteringResponsibleParty = new HashSet<MeteringPointHistory>();
            MeteringPointMeteringResponsibleParty = new HashSet<MeteringPoint>();
            PreSwitchMeteringPoint = new HashSet<PreSwitchMeteringPoint>();
        }

        public short Id { get; set; }
        public string Ean { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPointBalanceResponsibleParty { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointBalanceSupplier { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointGridOperator { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryBalanceResponsibleParty { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryBalanceSupplier { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryGridOperator { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistoryMeteringResponsibleParty { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPointMeteringResponsibleParty { get; set; }
        public virtual ICollection<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
    }
}
