﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class MeteringPoint
    {
        public MeteringPoint()
        {
            MeteringPointHistory = new HashSet<MeteringPointHistory>();
            Register = new HashSet<Register>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
        public short? GridOperatorId { get; set; }
        public short? GridAreaId { get; set; }
        public string LocationDescription { get; set; }
        public short MarketSegmentId { get; set; }
        public short ProductTypeId { get; set; }
        public short? BalanceSupplierId { get; set; }
        public short? BalanceResponsiblePartyId { get; set; }
        public short? MeteringResponsiblePartyId { get; set; }
        public bool HasCommercialCharacteristics { get; set; }
        public short? AllocationMethodId { get; set; }
        public short? CapTarCodeId { get; set; }
        public string ContractedCapacity { get; set; }
        public int? EacPeak { get; set; }
        public int? EacOffPeak { get; set; }
        public short EnergyDeliveryStatusId { get; set; }
        public short? EnergyFlowDirectionId { get; set; }
        public short MeteringMethodId { get; set; }
        public short? PhysicalCapacityId { get; set; }
        public short PhysicalStatusId { get; set; }
        public short? ProfileCategoryId { get; set; }
        public string InvoiceMonth { get; set; }
        public string MaxConsumption { get; set; }
        public int AddressId { get; set; }
        public string MeterEdsnId { get; set; }
        public short? NrOfRegisters { get; set; }
        public short? TypeId { get; set; }
        public bool? TemperatureCorrection { get; set; }
        public short? CommunicationStatusId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Papean { get; set; }
        public string Sapeans { get; set; }
        public int? EapOffPeak { get; set; }
        public int? EapPeak { get; set; }
        public DateTime HeaderCreationDate { get; set; }
        public bool IsTfSignal { get; set; }

        public virtual Address Address { get; set; }
        public virtual EnumEnergyAllocationMethodCode AllocationMethod { get; set; }
        public virtual MarketParty BalanceResponsibleParty { get; set; }
        public virtual MarketParty BalanceSupplier { get; set; }
        public virtual CapTarCode CapTarCode { get; set; }
        public virtual EnumCommunicationStatusCode CommunicationStatus { get; set; }
        public virtual EnumEnergyDeliveryStatusCode EnergyDeliveryStatus { get; set; }
        public virtual EnumEnergyFlowDirectionCode EnergyFlowDirection { get; set; }
        public virtual GridArea GridArea { get; set; }
        public virtual MarketParty GridOperator { get; set; }
        public virtual EnumMarketSegmentCode MarketSegment { get; set; }
        public virtual EnumEnergyMeteringMethodCode MeteringMethod { get; set; }
        public virtual MarketParty MeteringResponsibleParty { get; set; }
        public virtual EnumPhysicalCapacityCode PhysicalCapacity { get; set; }
        public virtual EnumEnergyConnectionPhysicalStatusCode PhysicalStatus { get; set; }
        public virtual EnumEnergyProductTypeCode ProductType { get; set; }
        public virtual EnumEnergyUsageProfileCode ProfileCategory { get; set; }
        public virtual EnumEnergyMeterTypeCode Type { get; set; }
        public virtual ICollection<MeteringPointHistory> MeteringPointHistory { get; set; }
        public virtual ICollection<Register> Register { get; set; }
    }
}
