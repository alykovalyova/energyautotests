﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class MeteringPointContext : DbContext
    {
        private readonly string _connectionString;

        public MeteringPointContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MeteringPointContext(DbContextOptions<MeteringPointContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<AddressHistory> AddressHistory { get; set; }
        public virtual DbSet<CapTarCode> CapTarCode { get; set; }
        public virtual DbSet<EnumCommunicationStatusCode> EnumCommunicationStatusCode { get; set; }
        public virtual DbSet<EnumEnergyAllocationMethodCode> EnumEnergyAllocationMethodCode { get; set; }
        public virtual DbSet<EnumEnergyConnectionPhysicalStatusCode> EnumEnergyConnectionPhysicalStatusCode { get; set; }
        public virtual DbSet<EnumEnergyDeliveryStatusCode> EnumEnergyDeliveryStatusCode { get; set; }
        public virtual DbSet<EnumEnergyFlowDirectionCode> EnumEnergyFlowDirectionCode { get; set; }
        public virtual DbSet<EnumEnergyMeterTypeCode> EnumEnergyMeterTypeCode { get; set; }
        public virtual DbSet<EnumEnergyMeteringMethodCode> EnumEnergyMeteringMethodCode { get; set; }
        public virtual DbSet<EnumEnergyProductTypeCode> EnumEnergyProductTypeCode { get; set; }
        public virtual DbSet<EnumEnergyTariffTypeCode> EnumEnergyTariffTypeCode { get; set; }
        public virtual DbSet<EnumEnergyUsageProfileCode> EnumEnergyUsageProfileCode { get; set; }
        public virtual DbSet<EnumMarketSegmentCode> EnumMarketSegmentCode { get; set; }
        public virtual DbSet<EnumPhysicalCapacityCode> EnumPhysicalCapacityCode { get; set; }
        public virtual DbSet<GridArea> GridArea { get; set; }
        public virtual DbSet<MarketParty> MarketParty { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoint { get; set; }
        public virtual DbSet<MeteringPointHistory> MeteringPointHistory { get; set; }
        public virtual DbSet<PreSwitchMeteringPoint> PreSwitchMeteringPoint { get; set; }
        public virtual DbSet<PreSwitchRegister> PreSwitchRegister { get; set; }
        public virtual DbSet<Register> Register { get; set; }
        public virtual DbSet<RegisterHistory> RegisterHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address", "dbo");

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => new { e.Zipcode, e.BuildingNr, e.ExBuildingNr })
                    .HasName("Search_ZipCode_BuildingNr");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<AddressHistory>(entity =>
            {
                entity.ToTable("AddressHistory", "dbo");

                entity.HasIndex(e => e.AddressId)
                    .HasName("IX_AddressId");

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Comment).HasMaxLength(100);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EanId)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.User).HasMaxLength(100);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.AddressHistory)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.ToTable("CapTarCode", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique()
                    .HasFilter("([Ean] IS NOT NULL)");
            });

            modelBuilder.Entity<EnumCommunicationStatusCode>(entity =>
            {
                entity.ToTable("EnumCommunicationStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyAllocationMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyAllocationMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyConnectionPhysicalStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyConnectionPhysicalStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyDeliveryStatusCode>(entity =>
            {
                entity.ToTable("EnumEnergyDeliveryStatusCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("EnumEnergyFlowDirectionCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeterTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeterTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeteringMethodCode>(entity =>
            {
                entity.ToTable("EnumEnergyMeteringMethodCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyProductTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("EnumEnergyTariffTypeCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("EnumEnergyUsageProfileCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMarketSegmentCode>(entity =>
            {
                entity.ToTable("EnumMarketSegmentCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumPhysicalCapacityCode>(entity =>
            {
                entity.ToTable("EnumPhysicalCapacityCode", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.ToTable("GridArea", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique()
                    .HasFilter("([Ean] IS NOT NULL)");
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.ToTable("MarketParty", "dbo");

                entity.HasIndex(e => e.Ean)
                    .IsUnique()
                    .HasFilter("([Ean] IS NOT NULL)");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint", "dbo");

                entity.HasIndex(e => e.AddressId);

                entity.HasIndex(e => e.AllocationMethodId);

                entity.HasIndex(e => e.BalanceResponsiblePartyId);

                entity.HasIndex(e => e.CapTarCodeId);

                entity.HasIndex(e => e.CommunicationStatusId);

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique()
                    .HasFilter("([EanId] IS NOT NULL)");

                entity.HasIndex(e => e.EnergyDeliveryStatusId);

                entity.HasIndex(e => e.EnergyFlowDirectionId);

                entity.HasIndex(e => e.GridAreaId);

                entity.HasIndex(e => e.GridOperatorId);

                entity.HasIndex(e => e.MarketSegmentId);

                entity.HasIndex(e => e.MeteringMethodId);

                entity.HasIndex(e => e.MeteringResponsiblePartyId);

                entity.HasIndex(e => e.PhysicalCapacityId);

                entity.HasIndex(e => e.PhysicalStatusId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.HasIndex(e => e.ProfileCategoryId);

                entity.HasIndex(e => e.TypeId);

                entity.HasIndex(e => new { e.BalanceSupplierId, e.PhysicalStatusId })
                    .HasName("IX_BalanceSupplierId_PhysicalStatusId");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.HeaderCreationDate).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.Papean)
                    .HasColumnName("PAPEan")
                    .HasMaxLength(18);

                entity.Property(e => e.Sapeans)
                    .HasColumnName("SAPEans")
                    .HasMaxLength(189);

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.AllocationMethodId);

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MeteringPointBalanceResponsibleParty)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId);

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MeteringPointBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPointGridOperator)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MeteringPointMeteringResponsibleParty)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId);

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.PhysicalCapacityId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProfileCategoryId);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.TypeId);
            });

            modelBuilder.Entity<MeteringPointHistory>(entity =>
            {
                entity.ToTable("MeteringPointHistory", "dbo");

                entity.HasIndex(e => e.AddressId);

                entity.HasIndex(e => e.AllocationMethodId);

                entity.HasIndex(e => e.BalanceResponsiblePartyId);

                entity.HasIndex(e => e.BalanceSupplierId);

                entity.HasIndex(e => e.CapTarCodeId);

                entity.HasIndex(e => e.CommunicationStatusId);

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId");

                entity.HasIndex(e => e.EnergyDeliveryStatusId);

                entity.HasIndex(e => e.EnergyFlowDirectionId);

                entity.HasIndex(e => e.GridAreaId);

                entity.HasIndex(e => e.GridOperatorId);

                entity.HasIndex(e => e.MarketSegmentId);

                entity.HasIndex(e => e.MeteringMethodId);

                entity.HasIndex(e => e.MeteringPointId)
                    .HasName("IX_MeteringPointId");

                entity.HasIndex(e => e.MeteringResponsiblePartyId);

                entity.HasIndex(e => e.PhysicalCapacityId);

                entity.HasIndex(e => e.PhysicalStatusId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.HasIndex(e => e.ProfileCategoryId);

                entity.HasIndex(e => e.TypeId);

                entity.HasIndex(e => new { e.MeteringPointId, e.ValidFrom })
                    .HasName("IX_MeteringPointId_ValidFrom");

                entity.Property(e => e.ContractedCapacity).HasMaxLength(100);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.HeaderCreationDate).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");

                entity.Property(e => e.InvoiceMonth).HasMaxLength(2);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MaxConsumption).HasMaxLength(10);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.Papean)
                    .HasColumnName("PAPEan")
                    .HasMaxLength(18);

                entity.Property(e => e.Sapeans)
                    .HasColumnName("SAPEans")
                    .HasMaxLength(189);

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.AllocationMethod)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.AllocationMethodId);

                entity.HasOne(d => d.BalanceResponsibleParty)
                    .WithMany(p => p.MeteringPointHistoryBalanceResponsibleParty)
                    .HasForeignKey(d => d.BalanceResponsiblePartyId);

                entity.HasOne(d => d.BalanceSupplier)
                    .WithMany(p => p.MeteringPointHistoryBalanceSupplier)
                    .HasForeignKey(d => d.BalanceSupplierId);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyDeliveryStatus)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.EnergyDeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPointHistoryGridOperator)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.MarketSegmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.MeteringMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringResponsibleParty)
                    .WithMany(p => p.MeteringPointHistoryMeteringResponsibleParty)
                    .HasForeignKey(d => d.MeteringResponsiblePartyId);

                entity.HasOne(d => d.PhysicalCapacity)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.PhysicalCapacityId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.PhysicalStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.ProfileCategoryId);

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.MeteringPointHistory)
                    .HasForeignKey(d => d.TypeId);
            });

            modelBuilder.Entity<PreSwitchMeteringPoint>(entity =>
            {
                entity.ToTable("PreSwitchMeteringPoint", "dbo");

                entity.HasIndex(e => e.AddressId);

                entity.HasIndex(e => e.CapTarCodeId);

                entity.HasIndex(e => e.CommunicationStatusId);

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique()
                    .HasFilter("([EanId] IS NOT NULL)");

                entity.HasIndex(e => e.EnergyFlowDirectionId);

                entity.HasIndex(e => e.GridAreaId);

                entity.HasIndex(e => e.GridOperatorId);

                entity.HasIndex(e => e.MarketSegmentId);

                entity.HasIndex(e => e.MeteringMethodId);

                entity.HasIndex(e => e.PhysicalStatusId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.HasIndex(e => e.ProfileCategoryId);

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.ValidDate).HasColumnType("date");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.AddressId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.MarketSegmentId);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.MeteringMethodId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.PhysicalStatusId);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategory)
                    .WithMany(p => p.PreSwitchMeteringPoint)
                    .HasForeignKey(d => d.ProfileCategoryId);
            });

            modelBuilder.Entity<PreSwitchRegister>(entity =>
            {
                entity.ToTable("PreSwitchRegister", "dbo");

                entity.HasIndex(e => e.MeteringDirectionId);

                entity.HasIndex(e => e.PreSwitchMeteringPointId);

                entity.HasIndex(e => e.TariffTypeId);

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.PreSwitchRegister)
                    .HasForeignKey(d => d.MeteringDirectionId);

                entity.HasOne(d => d.PreSwitchMeteringPoint)
                    .WithMany(p => p.PreSwitchRegister)
                    .HasForeignKey(d => d.PreSwitchMeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.PreSwitchRegister)
                    .HasForeignKey(d => d.TariffTypeId);
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.ToTable("Register", "dbo");

                entity.HasIndex(e => e.MeteringDirectionId);

                entity.HasIndex(e => e.MeteringPointId)
                    .HasName("IX_MeteringPointId");

                entity.HasIndex(e => e.TariffTypeId);

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RegisterHistory>(entity =>
            {
                entity.ToTable("RegisterHistory", "dbo");

                entity.HasIndex(e => e.MeteringDirectionId);

                entity.HasIndex(e => e.MeteringPointHistoryId)
                    .HasName("IX_MeteringPointHistoryId");

                entity.HasIndex(e => e.TariffTypeId);

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.RegisterHistory)
                    .HasForeignKey(d => d.MeteringDirectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringPointHistory)
                    .WithMany(p => p.RegisterHistory)
                    .HasForeignKey(d => d.MeteringPointHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.RegisterHistory)
                    .HasForeignKey(d => d.TariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
