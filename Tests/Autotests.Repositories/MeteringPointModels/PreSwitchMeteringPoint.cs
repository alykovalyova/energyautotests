﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointModels
{
    public partial class PreSwitchMeteringPoint
    {
        public PreSwitchMeteringPoint()
        {
            PreSwitchRegister = new HashSet<PreSwitchRegister>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
        public int AddressId { get; set; }
        public short? GridAreaId { get; set; }
        public string LocationDescription { get; set; }
        public short? MarketSegmentId { get; set; }
        public short ProductTypeId { get; set; }
        public short? GridOperatorId { get; set; }
        public string MeterEdsnId { get; set; }
        public short? EnergyMeterTypeId { get; set; }
        public short? CommunicationStatusId { get; set; }
        public short? NrOfRegisters { get; set; }
        public bool? TemperatureCorrection { get; set; }
        public short? CapTarCodeId { get; set; }
        public int? EacOffPeak { get; set; }
        public int? EacPeak { get; set; }
        public short? EnergyFlowDirectionId { get; set; }
        public short? MeteringMethodId { get; set; }
        public short? PhysicalStatusId { get; set; }
        public short? ProfileCategoryId { get; set; }
        public string DossierId { get; set; }
        public string ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime ValidDate { get; set; }
        public string PapEan { get; set; }
        public string SapEans { get; set; }
        public int? EapOffPeak { get; set; }
        public int? EapPeak { get; set; }

        public virtual Address Address { get; set; }
        public virtual CapTarCode CapTarCode { get; set; }
        public virtual EnumCommunicationStatusCode CommunicationStatus { get; set; }
        public virtual EnumEnergyFlowDirectionCode EnergyFlowDirection { get; set; }
        public virtual GridArea GridArea { get; set; }
        public virtual MarketParty GridOperator { get; set; }
        public virtual EnumMarketSegmentCode MarketSegment { get; set; }
        public virtual EnumEnergyMeteringMethodCode MeteringMethod { get; set; }
        public virtual EnumEnergyConnectionPhysicalStatusCode PhysicalStatus { get; set; }
        public virtual EnumEnergyProductTypeCode ProductType { get; set; }
        public virtual EnumEnergyUsageProfileCode ProfileCategory { get; set; }
        public virtual ICollection<PreSwitchRegister> PreSwitchRegister { get; set; }
    }
}
