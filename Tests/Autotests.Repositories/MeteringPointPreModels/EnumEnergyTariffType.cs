﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointPreModels
{
    public partial class EnumEnergyTariffType
    {
        public EnumEnergyTariffType()
        {
            Register = new HashSet<Register>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<Register> Register { get; set; }
    }
}
