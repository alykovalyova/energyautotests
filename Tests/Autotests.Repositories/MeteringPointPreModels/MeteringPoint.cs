﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointPreModels
{
    public partial class MeteringPoint
    {
        public MeteringPoint()
        {
            Register = new HashSet<Register>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public bool? AdministrativeStatusSmartMeter { get; set; }
        public short? GridAreaId { get; set; }
        public string LocationDescription { get; set; }
        public short? MarketSegmentId { get; set; }
        public short ProductTypeId { get; set; }
        public short? GridOperatorId { get; set; }
        public string MeterEdsnId { get; set; }
        public short? EnergyMeterTypeId { get; set; }
        public short? CommunicationStatusId { get; set; }
        public byte? NrOfRegisters { get; set; }
        public bool? TemperatureCorrection { get; set; }
        public short? CapTarCodeId { get; set; }
        public int? EacoffPeak { get; set; }
        public int? Eacpeak { get; set; }
        public short? EnergyFlowDirectionId { get; set; }
        public short? MeteringMethodId { get; set; }
        public short? PhysicalStatusId { get; set; }
        public short? ProfileCategoryId { get; set; }
        public short? ProfileCategotyId { get; set; }
        public string DossierId { get; set; }
        public string ExternalReference { get; set; }
        public DateTime MutationDate { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Zipcode { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public string BagId { get; set; }
        public string BagBuildingId { get; set; }
        public string Papean { get; set; }
        public string Sapeans { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime ValidDate { get; set; }

        public virtual CapTarCode CapTarCode { get; set; }
        public virtual EnumCommunicationStatus CommunicationStatus { get; set; }
        public virtual EnumEnergyFlowDirection EnergyFlowDirection { get; set; }
        public virtual EnumEnergyMeterType EnergyMeterType { get; set; }
        public virtual GridArea GridArea { get; set; }
        public virtual MarketParty GridOperator { get; set; }
        public virtual EnumMarketSegment MarketSegment { get; set; }
        public virtual EnumEnergyMeteringMethod MeteringMethod { get; set; }
        public virtual EnumEnergyConnectionPhysicalStatus PhysicalStatus { get; set; }
        public virtual EnumEnergyProductType ProductType { get; set; }
        public virtual EnumEnergyUsageProfileCategoty ProfileCategoty { get; set; }
        public virtual ICollection<Register> Register { get; set; }
    }
}
