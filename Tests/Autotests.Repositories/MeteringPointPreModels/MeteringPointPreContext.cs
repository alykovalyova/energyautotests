﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.MeteringPointPreModels
{
    public partial class MeteringPointPreContext : DbContext
    {
        private readonly string _connectionString;

        public MeteringPointPreContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public MeteringPointPreContext(DbContextOptions<MeteringPointPreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CapTarCode> CapTarCode { get; set; }
        public virtual DbSet<EnumCommunicationStatus> EnumCommunicationStatus { get; set; }
        public virtual DbSet<EnumEnergyConnectionPhysicalStatus> EnumEnergyConnectionPhysicalStatus { get; set; }
        public virtual DbSet<EnumEnergyFlowDirection> EnumEnergyFlowDirection { get; set; }
        public virtual DbSet<EnumEnergyMeterType> EnumEnergyMeterType { get; set; }
        public virtual DbSet<EnumEnergyMeteringMethod> EnumEnergyMeteringMethod { get; set; }
        public virtual DbSet<EnumEnergyProductType> EnumEnergyProductType { get; set; }
        public virtual DbSet<EnumEnergyTariffType> EnumEnergyTariffType { get; set; }
        public virtual DbSet<EnumEnergyUsageProfileCategoty> EnumEnergyUsageProfileCategoty { get; set; }
        public virtual DbSet<EnumMarketSegment> EnumMarketSegment { get; set; }
        public virtual DbSet<GridArea> GridArea { get; set; }
        public virtual DbSet<MarketParty> MarketParty { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoint { get; set; }
        public virtual DbSet<Register> Register { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CapTarCode>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumCommunicationStatus>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyConnectionPhysicalStatus>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyFlowDirection>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeterType>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyMeteringMethod>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyProductType>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyTariffType>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumEnergyUsageProfileCategoty>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EnumMarketSegment>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<GridArea>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MarketParty>(entity =>
            {
                entity.HasIndex(e => e.Identifier)
                    .IsUnique();

                entity.Property(e => e.Identifier)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.HasIndex(e => e.CapTarCodeId);

                entity.HasIndex(e => e.CommunicationStatusId);

                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique()
                    .HasFilter("([EanId] IS NOT NULL)");

                entity.HasIndex(e => e.EnergyFlowDirectionId);

                entity.HasIndex(e => e.EnergyMeterTypeId);

                entity.HasIndex(e => e.GridAreaId);

                entity.HasIndex(e => e.GridOperatorId);

                entity.HasIndex(e => e.MarketSegmentId);

                entity.HasIndex(e => e.MeteringMethodId);

                entity.HasIndex(e => e.PhysicalStatusId);

                entity.HasIndex(e => e.ProductTypeId);

                entity.HasIndex(e => e.ProfileCategotyId);

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(20);

                entity.Property(e => e.EacoffPeak).HasColumnName("EACOffPeak");

                entity.Property(e => e.Eacpeak).HasColumnName("EACPeak");

                entity.Property(e => e.EanId).HasMaxLength(18);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.LocationDescription).HasMaxLength(200);

                entity.Property(e => e.MeterEdsnId).HasMaxLength(50);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.Property(e => e.Papean).HasColumnName("PAPEan");

                entity.Property(e => e.Sapeans).HasColumnName("SAPEans");

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.ValidDate).HasColumnType("date");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.CapTarCode)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.CapTarCodeId);

                entity.HasOne(d => d.CommunicationStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.CommunicationStatusId);

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.EnergyMeterType)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.EnergyMeterTypeId);

                entity.HasOne(d => d.GridArea)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.GridAreaId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.GridOperatorId);

                entity.HasOne(d => d.MarketSegment)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.MarketSegmentId);

                entity.HasOne(d => d.MeteringMethod)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.MeteringMethodId);

                entity.HasOne(d => d.PhysicalStatus)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.PhysicalStatusId);

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ProfileCategoty)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProfileCategotyId);
            });

            modelBuilder.Entity<Register>(entity =>
            {
                entity.HasIndex(e => e.MeteringDirectionId);

                entity.HasIndex(e => e.MeteringPointId);

                entity.HasIndex(e => e.TariffTypeId);

                entity.Property(e => e.EdsnId).HasMaxLength(20);

                entity.Property(e => e.MultiplicationFactor).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeteringDirection)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringDirectionId);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.TariffType)
                    .WithMany(p => p.Register)
                    .HasForeignKey(d => d.TariffTypeId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
