﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.OfferBundleModels
{
    public partial class DictionaryClientType
    {
        public DictionaryClientType()
        {
            OfferBundles = new HashSet<OfferBundle>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<OfferBundle> OfferBundles { get; set; }
    }
}
