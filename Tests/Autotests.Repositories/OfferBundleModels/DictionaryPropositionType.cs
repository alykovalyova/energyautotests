﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.OfferBundleModels
{
    public partial class DictionaryPropositionType
    {
        public DictionaryPropositionType()
        {
            PropositionTypes = new HashSet<PropositionType>();
        }

        public short Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PropositionType> PropositionTypes { get; set; }
    }
}
