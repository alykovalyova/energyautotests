﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.OfferBundleModels
{
    public partial class OfferBundle
    {
        public OfferBundle()
        {
            OfferPropositions = new HashSet<OfferProposition>();
            PropositionTypes = new HashSet<PropositionType>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short LabelCode { get; set; }
        public short ClientTypeId { get; set; }
        public short CustomerProductTypeId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual DictionaryClientType ClientType { get; set; }
        public virtual DictionaryCustomerProductType CustomerProductType { get; set; }
        public virtual ICollection<OfferProposition> OfferPropositions { get; set; }
        public virtual ICollection<PropositionType> PropositionTypes { get; set; }
    }
}
