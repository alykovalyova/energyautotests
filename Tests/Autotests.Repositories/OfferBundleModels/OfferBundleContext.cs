﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Autotests.Repositories.OfferBundleModels
{
    public partial class OfferBundleContext : DbContext
    {
        private readonly string _connectionString;

        public OfferBundleContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public OfferBundleContext(DbContextOptions<OfferBundleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryClientType> DictionaryClientTypes { get; set; }
        public virtual DbSet<DictionaryCustomerProductType> DictionaryCustomerProductTypes { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryPropositionType> DictionaryPropositionTypes { get; set; }
        public virtual DbSet<OfferBundle> OfferBundles { get; set; }
        public virtual DbSet<OfferProposition> OfferPropositions { get; set; }
        public virtual DbSet<PropositionType> PropositionTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<DictionaryClientType>(entity =>
            {
                entity.ToTable("Dictionary_ClientType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryCustomerProductType>(entity =>
            {
                entity.ToTable("Dictionary_CustomerProductType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryPropositionType>(entity =>
            {
                entity.ToTable("Dictionary_PropositionType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<OfferBundle>(entity =>
            {
                entity.ToTable("OfferBundle");

                entity.HasIndex(e => e.ClientTypeId, "IX_OfferBundle_ClientTypeId");

                entity.HasIndex(e => e.CustomerProductTypeId, "IX_OfferBundle_CustomerProductTypeId");

                entity.HasOne(d => d.ClientType)
                    .WithMany(p => p.OfferBundles)
                    .HasForeignKey(d => d.ClientTypeId);

                entity.HasOne(d => d.CustomerProductType)
                    .WithMany(p => p.OfferBundles)
                    .HasForeignKey(d => d.CustomerProductTypeId);
            });

            modelBuilder.Entity<OfferProposition>(entity =>
            {
                entity.ToTable("OfferProposition");

                entity.HasIndex(e => e.OfferBundleId, "IX_OfferProposition_OfferBundleId");

                entity.Property(e => e.Cashback).HasColumnType("decimal(6, 2)");

                entity.HasOne(d => d.OfferBundle)
                    .WithMany(p => p.OfferPropositions)
                    .HasForeignKey(d => d.OfferBundleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PropositionType>(entity =>
            {
                entity.ToTable("PropositionType");

                entity.HasIndex(e => e.OfferBundleId, "IX_PropositionType_OfferBundleId");

                entity.HasIndex(e => e.PropositionTypeId, "IX_PropositionType_PropositionTypeId");

                entity.HasOne(d => d.OfferBundle)
                    .WithMany(p => p.PropositionTypes)
                    .HasForeignKey(d => d.OfferBundleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PropositionTypeNavigation)
                    .WithMany(p => p.PropositionTypes)
                    .HasForeignKey(d => d.PropositionTypeId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
