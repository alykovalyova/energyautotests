﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.OfferBundleModels
{
    public partial class OfferProposition
    {
        public int Id { get; set; }
        public int OfferBundleId { get; set; }
        public Guid PropositionId { get; set; }
        public int ContractDurationInMonths { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime ValidFrom { get; set; }
        public decimal? Cashback { get; set; }

        public virtual OfferBundle OfferBundle { get; set; }
    }
}
