﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.OfferBundleModels
{
    public partial class PropositionType
    {
        public int Id { get; set; }
        public int OfferBundleId { get; set; }
        public short PropositionTypeId { get; set; }

        public virtual OfferBundle OfferBundle { get; set; }
        public virtual DictionaryPropositionType PropositionTypeNavigation { get; set; }
    }
}
