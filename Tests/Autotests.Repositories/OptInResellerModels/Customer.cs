﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.OptInResellerModels
{
    public partial class Customer
    {
        public Customer()
        {
            OptIn = new HashSet<OptIn>();
        }

        public int Id { get; set; }
        public int? NutsHomeCustomerNumber { get; set; }
        public string Initials { get; set; }
        public string SurnamePrefix { get; set; }
        public string Surname { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberExtension { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual ICollection<OptIn> OptIn { get; set; }
    }
}
