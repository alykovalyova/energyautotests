﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.OptInResellerModels
{
    public partial class OnlineLog
    {
        public OnlineLog()
        {
            OptIn = new HashSet<OptIn>();
        }

        public int Id { get; set; }
        public string OnlineExternalReference { get; set; }
        public string IpAddress { get; set; }
        public string CephReference { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual ICollection<OptIn> OptIn { get; set; }
    }
}
