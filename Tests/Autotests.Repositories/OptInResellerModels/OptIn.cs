﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.OptInResellerModels
{
    public partial class OptIn
    {
        public OptIn()
        {
            VoiceLog = new HashSet<VoiceLog>();
        }

        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public DateTime OptInDate { get; set; }
        public int SalesChannelId { get; set; }
        public int CustomerId { get; set; }
        public int? OnlineLogId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual OnlineLog OnlineLog { get; set; }
        public virtual ICollection<VoiceLog> VoiceLog { get; set; }
    }
}
