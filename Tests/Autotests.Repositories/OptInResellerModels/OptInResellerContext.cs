﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.OptInResellerModels
{
    public partial class OptInResellerContext : DbContext
    {
        private readonly string _connectionString;

        public OptInResellerContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public OptInResellerContext(DbContextOptions<OptInResellerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<DictionaryAudioType> DictionaryAudioType { get; set; }
        public virtual DbSet<DictionaryVoiceLogStatus> DictionaryVoiceLogStatus { get; set; }
        public virtual DbSet<OnlineLog> OnlineLog { get; set; }
        public virtual DbSet<OptIn> OptIn { get; set; }
        public virtual DbSet<VoiceLog> VoiceLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.NutsHomeCustomerNumber);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.HouseNumberExtension).HasMaxLength(10);

                entity.Property(e => e.Initials).HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.PostalCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Street).HasMaxLength(100);

                entity.Property(e => e.Surname).HasMaxLength(100);

                entity.Property(e => e.SurnamePrefix).HasMaxLength(10);
            });

            modelBuilder.Entity<DictionaryAudioType>(entity =>
            {
                entity.ToTable("Dictionary_AudioType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryVoiceLogStatus>(entity =>
            {
                entity.ToTable("Dictionary_VoiceLogStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<OnlineLog>(entity =>
            {
                entity.Property(e => e.CephReference)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasDefaultValueSql("(N'')");

                entity.Property(e => e.IpAddress).HasMaxLength(45);

                entity.Property(e => e.OnlineExternalReference)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<OptIn>(entity =>
            {
                entity.HasIndex(e => e.CustomerId);

                entity.HasIndex(e => e.ExternalReference)
                    .IsUnique();

                entity.HasIndex(e => e.OnlineLogId);

                entity.HasIndex(e => new { e.ExternalReference, e.SalesChannelId });

                entity.Property(e => e.ExternalReference)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.OptIn)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.OnlineLog)
                    .WithMany(p => p.OptIn)
                    .HasForeignKey(d => d.OnlineLogId);
            });

            modelBuilder.Entity<VoiceLog>(entity =>
            {
                entity.HasIndex(e => e.AudioTypeId);

                entity.HasIndex(e => e.OptInId);

                entity.HasIndex(e => e.StatusId);

                entity.Property(e => e.StatusId).HasDefaultValueSql("(CONVERT([smallint],(0)))");

                entity.Property(e => e.VoiceExternalReference)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.AudioType)
                    .WithMany(p => p.VoiceLog)
                    .HasForeignKey(d => d.AudioTypeId);

                entity.HasOne(d => d.OptIn)
                    .WithMany(p => p.VoiceLog)
                    .HasForeignKey(d => d.OptInId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.VoiceLog)
                    .HasForeignKey(d => d.StatusId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
