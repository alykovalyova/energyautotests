﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.OptInResellerModels
{
    public partial class VoiceLog
    {
        public int Id { get; set; }
        public string VoiceExternalReference { get; set; }
        public short AudioTypeId { get; set; }
        public string CephReference { get; set; }
        public int OptInId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public short StatusId { get; set; }

        public virtual DictionaryAudioType AudioType { get; set; }
        public virtual OptIn OptIn { get; set; }
        public virtual DictionaryVoiceLogStatus Status { get; set; }
    }
}
