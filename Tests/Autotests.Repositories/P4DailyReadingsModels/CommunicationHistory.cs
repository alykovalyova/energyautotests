﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class CommunicationHistory
    {
        public CommunicationHistory()
        {
            StatusHistory = new HashSet<StatusHistory>();
        }

        public int Id { get; set; }
        public string ExternalReference { get; set; }
        public DateTime ReadingDate { get; set; }
        public int MeteringPointId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual MeteringPoint MeteringPoint { get; set; }
        public virtual ICollection<StatusHistory> StatusHistory { get; set; }
    }
}
