﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class EdsnRejection
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public short CodeId { get; set; }
        public short? RejectionLevelId { get; set; }
        public int StatusHistoryId { get; set; }

        public virtual StatusHistory StatusHistory { get; set; }
    }
}
