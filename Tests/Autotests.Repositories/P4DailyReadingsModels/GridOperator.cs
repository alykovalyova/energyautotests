﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class GridOperator
    {
        public GridOperator()
        {
            MeteringPoint = new HashSet<MeteringPoint>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
    }
}
