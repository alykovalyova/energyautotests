﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class MandateHistory
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public short MandateTypeId { get; set; }
        public int MeteringPointId { get; set; }

        public virtual DictionaryMandateType MandateType { get; set; }
        public virtual MeteringPoint MeteringPoint { get; set; }
    }
}
