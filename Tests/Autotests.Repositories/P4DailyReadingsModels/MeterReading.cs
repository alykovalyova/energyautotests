﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class MeterReading
    {
        public MeterReading()
        {
            RegisterReading = new HashSet<RegisterReading>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public short ProductTypeId { get; set; }
        public string ExternalReference { get; set; }
        public DateTime ReadingDate { get; set; }
        public string EdsnMeterId { get; set; }
        public short NrOfRegisters { get; set; }
        public short StatusId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<RegisterReading> RegisterReading { get; set; }
    }
}
