﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class MeteringPoint
    {
        public MeteringPoint()
        {
            CommunicationHistory = new HashSet<CommunicationHistory>();
            MandateHistory = new HashSet<MandateHistory>();
            MeteringPointStatusHistory = new HashSet<MeteringPointStatusHistory>();
        }

        public int Id { get; set; }
        public string EanId { get; set; }
        public int GridOperatorId { get; set; }
        public short ProductTypeId { get; set; }
        public short StatusId { get; set; }
        public string BalanceSupplier { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Allocation { get; set; }
        public bool Mandate { get; set; }
        public bool CallHistoricalData { get; set; }

        public virtual GridOperator GridOperator { get; set; }
        public virtual ICollection<CommunicationHistory> CommunicationHistory { get; set; }
        public virtual ICollection<MandateHistory> MandateHistory { get; set; }
        public virtual ICollection<MeteringPointStatusHistory> MeteringPointStatusHistory { get; set; }
    }
}
