﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class MeteringPointStatusHistory
    {
        public int Id { get; set; }
        public short StatusId { get; set; }
        public string Comment { get; set; }
        public int MeteringPointId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual MeteringPoint MeteringPoint { get; set; }
    }
}
