﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class P4DailyReadingsContext : DbContext
    {
        private readonly string _connectionString;

        public P4DailyReadingsContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public P4DailyReadingsContext(DbContextOptions<P4DailyReadingsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CommunicationHistory> CommunicationHistory { get; set; }
        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevel { get; set; }
        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCode { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCode { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabel { get; set; }
        public virtual DbSet<DictionaryMandateType> DictionaryMandateType { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCode { get; set; }
        public virtual DbSet<DictionaryMeterReadingStatus> DictionaryMeterReadingStatus { get; set; }
        public virtual DbSet<DictionaryMeteringPointStatus> DictionaryMeteringPointStatus { get; set; }
        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatus { get; set; }
        public virtual DbSet<DictionaryRejectionCode> DictionaryRejectionCode { get; set; }
        public virtual DbSet<EdsnRejection> EdsnRejection { get; set; }
        public virtual DbSet<GridOperator> GridOperator { get; set; }
        public virtual DbSet<MandateHistory> MandateHistory { get; set; }
        public virtual DbSet<MeterReading> MeterReading { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoint { get; set; }
        public virtual DbSet<MeteringPointStatusHistory> MeteringPointStatusHistory { get; set; }
        public virtual DbSet<RegisterReading> RegisterReading { get; set; }
        public virtual DbSet<StatusHistory> StatusHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CommunicationHistory>(entity =>
            {
                entity.HasIndex(e => e.ExternalReference)
                    .HasName("IX_ExternalReference");

                entity.HasIndex(e => new { e.MeteringPointId, e.ReadingDate })
                    .HasName("IX_MeteringPointId_ReadingDate");

                entity.Property(e => e.ReadingDate).HasColumnType("date");

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.CommunicationHistory)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMandateType>(entity =>
            {
                entity.ToTable("Dictionary_MandateType");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeterReadingStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeterReadingStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeteringPointStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeteringPointStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryRejectionCode>(entity =>
            {
                entity.ToTable("Dictionary_RejectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.HasIndex(e => e.StatusHistoryId);

                entity.HasOne(d => d.StatusHistory)
                    .WithMany(p => p.EdsnRejection)
                    .HasForeignKey(d => d.StatusHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<GridOperator>(entity =>
            {
                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();
            });

            modelBuilder.Entity<MandateHistory>(entity =>
            {
                entity.HasIndex(e => e.CreatedOn)
                    .HasName("IX_CreatedOn");

                entity.HasIndex(e => e.MandateTypeId);

                entity.HasIndex(e => e.MeteringPointId);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.MandateType)
                    .WithMany(p => p.MandateHistory)
                    .HasForeignKey(d => d.MandateTypeId);

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.MandateHistory)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MeterReading>(entity =>
            {
                entity.HasIndex(e => new { e.EanId, e.EdsnMeterId })
                    .HasName("IX_EanId_EdsnMeterId");

                entity.HasIndex(e => new { e.EanId, e.ReadingDate })
                    .HasName("IX_EanId_ReadingDate");

                entity.Property(e => e.ReadingDate).HasColumnType("date");
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.HasIndex(e => e.EanId)
                    .HasName("IX_EanId")
                    .IsUnique();

                entity.HasIndex(e => e.GridOperatorId);

                entity.HasOne(d => d.GridOperator)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.GridOperatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MeteringPointStatusHistory>(entity =>
            {
                entity.HasIndex(e => new { e.MeteringPointId, e.CreatedOn })
                    .HasName("IX_MeteringPointId_CreatedOn");

                entity.HasOne(d => d.MeteringPoint)
                    .WithMany(p => p.MeteringPointStatusHistory)
                    .HasForeignKey(d => d.MeteringPointId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RegisterReading>(entity =>
            {
                entity.HasIndex(e => e.MeterReadingId);

                entity.HasIndex(e => new { e.TariffTypeId, e.MeteringDirectionId, e.MeterReadingId })
                    .HasName("IX_TariffType_MeteringDirection")
                    .IsUnique();

                entity.HasOne(d => d.MeterReading)
                    .WithMany(p => p.RegisterReading)
                    .HasForeignKey(d => d.MeterReadingId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.HasIndex(e => new { e.CommunicationHistoryId, e.CreatedOn })
                    .HasName("IX_CommunicationHistoryId_CreatedOn");

                entity.HasOne(d => d.CommunicationHistory)
                    .WithMany(p => p.StatusHistory)
                    .HasForeignKey(d => d.CommunicationHistoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
