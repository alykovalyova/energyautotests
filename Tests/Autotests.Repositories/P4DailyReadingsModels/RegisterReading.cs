﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class RegisterReading
    {
        public int Id { get; set; }
        public string EdsnId { get; set; }
        public int Value { get; set; }
        public short TariffTypeId { get; set; }
        public short MeteringDirectionId { get; set; }
        public short MeasureUnitId { get; set; }
        public int MeterReadingId { get; set; }

        public virtual MeterReading MeterReading { get; set; }
    }
}
