﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.P4DailyReadingsModels
{
    public partial class StatusHistory
    {
        public StatusHistory()
        {
            EdsnRejection = new HashSet<EdsnRejection>();
        }

        public int Id { get; set; }
        public Guid? XmlMessageId { get; set; }
        public DateTime? XmlCreationTs { get; set; }
        public short StatusId { get; set; }
        public int CommunicationHistoryId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual CommunicationHistory CommunicationHistory { get; set; }
        public virtual ICollection<EdsnRejection> EdsnRejection { get; set; }
    }
}
