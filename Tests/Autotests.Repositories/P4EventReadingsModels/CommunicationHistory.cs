﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class CommunicationHistory
    {
        public int Id { get; set; }

        public string ExternalReference { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReadingDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime RequestDate { get; set; }

        public int MeteringPointId { get; set; }

        public int BalanceSupplierId { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<StatusHistory> StatusHistories { get; set; }

        public virtual ICollection<Source> Sources { get; set; }
    }
}
