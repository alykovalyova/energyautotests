﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class DictionarySource
    {
        public short Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
