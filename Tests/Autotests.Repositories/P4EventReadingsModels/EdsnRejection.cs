﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class EdsnRejection
    {
        public int Id { get; set; }

        public string Message { get; set; }

        public short CodeId { get; set; }

        public short? RejectionLevelId { get; set; }

        public int StatusHistoryId { get; set; }
    }
}
