﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class GridOperator
    {
        public int Id { get; set; }

        public string EanId { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoints { get; set; }
    }
}
