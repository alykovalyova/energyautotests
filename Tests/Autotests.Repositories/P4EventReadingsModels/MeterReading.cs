﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class MeterReading
    {
        public int Id { get; set; }

        public string EanId { get; set; }

        public short ProductTypeId { get; set; }

        public string ExternalReference { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReadingDate { get; set; }

        public string EdsnMeterId { get; set; }

        public short NrOfRegisters { get; set; }

        public short StatusId { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<RegisterReading> RegisterReadings { get; set; }
    }
}
