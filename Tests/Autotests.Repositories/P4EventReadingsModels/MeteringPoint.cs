﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class MeteringPoint
    {
        public int Id { get; set; }

        public string EanId { get; set; }

        public short ProductTypeId { get; set; }

        public int GridOperatorId { get; set; }

        public short StatusId { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<CommunicationHistory> CommunicationHistories { get; set; }
    }
}
