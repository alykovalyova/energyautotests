﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public partial class P4EventReadingsContext : DbContext
    {
        private readonly string _connectionString;

        public P4EventReadingsContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public P4EventReadingsContext(DbContextOptions<P4EventReadingsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CommunicationHistory> CommunicationHistories { get; set; }

        public virtual DbSet<GridOperator> GridOperators { get; set; }

        public virtual DbSet<StatusHistory> StatusHistories { get; set; }

        public virtual DbSet<EdsnRejection> EdsnRejections { get; set; }

        public virtual DbSet<MeterReading> MeterReadings { get; set; }

        public virtual DbSet<MeteringPoint> MeteringPoints { get; set; }

        public virtual DbSet<RegisterReading> RegisterReadings { get; set; }

        public virtual DbSet<BalanceSupplier> BalanceSuppliers { get; set; }

        public virtual DbSet<Source> Sources { get; set; }

        public virtual DbSet<DictionaryEdsnRejectionLevel> DictionaryEdsnRejectionLevels { get; set; }

        public virtual DbSet<DictionaryEnergyFlowDirectionCode> DictionaryEnergyFlowDirectionCodes { get; set; }

        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; }

        public virtual DbSet<DictionaryEnergyTariffTypeCode> DictionaryEnergyTariffTypeCodes { get; set; }

        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }

        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCodes { get; set; }

        public virtual DbSet<DictionaryMeteringPointStatus> DictionaryMeteringPointStatuses { get; set; }

        public virtual DbSet<DictionaryMeterReadingStatus> DictionaryMeterReadingStatuses { get; set; }

        public virtual DbSet<DictionaryProcessStatus> DictionaryProcessStatuses { get; set; }

        public virtual DbSet<DictionaryRejectionCode> DictionaryRejectionCodes { get; set; }

        public virtual DbSet<DictionarySource> DictionarySources { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // removing PluralizingTableNameConvention
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.SetTableName(entity.DisplayName());
            }

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<MeterReading>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => new { x.EanId, x.ReadingDate }).HasName("IX_EanId_ReadingDate");
                entity.HasIndex(x => new { x.EanId, x.EdsnMeterId }).HasName("IX_EanId_EdsnMeterId");
            });

            modelBuilder.Entity<RegisterReading>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => new { x.TariffTypeId, x.MeteringDirectionId, x.MeterReadingId }).HasName("IX_TariffType_MeteringDirection").IsUnique();
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => x.EanId).HasName("IX_EanId").IsUnique();
            });

            modelBuilder.Entity<CommunicationHistory>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => new { x.MeteringPointId, x.ReadingDate }).HasName("IX_MeteringPointId_ReadingDate");
                entity.HasIndex(x => x.ExternalReference).HasName("IX_ExternalReference");
            });

            modelBuilder.Entity<StatusHistory>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => new { x.CommunicationHistoryId, x.CreatedOn }).HasName("IX_CommunicationHistoryId_CreatedOn");
            });

            modelBuilder.Entity<EdsnRejection>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
            });

            modelBuilder.Entity<Source>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
            });

            modelBuilder.Entity<BalanceSupplier>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => x.EanId).HasName("IX_EanId").IsUnique();
                entity.HasData(new BalanceSupplier { Id = 1, EanId = "8714252018141" }, new BalanceSupplier { Id = 2, EanId = "8712423016965" });
            });

            modelBuilder.Entity<GridOperator>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).UseIdentityColumn();
                entity.HasIndex(x => x.EanId).HasName("IX_EanId").IsUnique();
            });

            modelBuilder.Entity<DictionaryEdsnRejectionLevel>(entity =>
            {
                entity.ToTable("Dictionary_EdsnRejectionLevel");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyFlowDirectionCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyFlowDirectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryEnergyTariffTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyTariffTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Label");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeterReadingStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeterReadingStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryMeteringPointStatus>(entity =>
            {
                entity.ToTable("Dictionary_MeteringPointStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryProcessStatus>(entity =>
            {
                entity.ToTable("Dictionary_ProcessStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionaryRejectionCode>(entity =>
            {
                entity.ToTable("Dictionary_RejectionCode");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<DictionarySource>(entity => 
            {
                entity.ToTable("Dictionary_Source");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
