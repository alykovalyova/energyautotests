﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class RegisterReading
    {
        public int Id { get; set; }

        public string EdsnId { get; set; }

        public int Value { get; set; }

        public short TariffTypeId { get; set; }

        public short MeteringDirectionId { get; set; }

        public short MeasureUnitId { get; set; }

        public int MeterReadingId { get; set; }
    }
}
