﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class Source
    {
        public int Id { get; set; }

        public int CommunicationHistoryId { get; set; }

        public short SourceId { get; set; }
    }
}
