﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autotests.Repositories.P4EventReadingsModels
{
    public class StatusHistory
    {
        public int Id { get; set; }

        public Guid? XmlMessageId { get; set; }

        public DateTime? XmlCreationTs { get; set; }

        public short StatusId { get; set; }

        public int CommunicationHistoryId { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual ICollection<EdsnRejection> EdsnRejections { get; set; }
    }
}
