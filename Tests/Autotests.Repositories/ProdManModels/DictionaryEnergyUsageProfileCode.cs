﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class DictionaryEnergyUsageProfileCode
    {
        public DictionaryEnergyUsageProfileCode()
        {
            ElectricityProfileCategory = new HashSet<ElectricityProfileCategory>();
            GasProfileCategory = new HashSet<GasProfileCategory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityProfileCategory> ElectricityProfileCategory { get; set; }
        public virtual ICollection<GasProfileCategory> GasProfileCategory { get; set; }
    }
}
