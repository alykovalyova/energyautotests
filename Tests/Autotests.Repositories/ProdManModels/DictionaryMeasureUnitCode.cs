﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class DictionaryMeasureUnitCode
    {
        public DictionaryMeasureUnitCode()
        {
            ElectricityPrice = new HashSet<ElectricityPrice>();
            GasPrice = new HashSet<GasPrice>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityPrice> ElectricityPrice { get; set; }
        public virtual ICollection<GasPrice> GasPrice { get; set; }
    }
}
