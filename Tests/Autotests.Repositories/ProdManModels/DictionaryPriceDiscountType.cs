﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class DictionaryPriceDiscountType
    {
        public DictionaryPriceDiscountType()
        {
            ElectricityTariff = new HashSet<ElectricityTariff>();
            GasTariff = new HashSet<GasTariff>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityTariff> ElectricityTariff { get; set; }
        public virtual ICollection<GasTariff> GasTariff { get; set; }
    }
}
