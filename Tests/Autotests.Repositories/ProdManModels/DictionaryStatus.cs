﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class DictionaryStatus
    {
        public DictionaryStatus()
        {
            ElectricityPrice = new HashSet<ElectricityPrice>();
            ElectricityPriceStatusHistory = new HashSet<ElectricityPriceStatusHistory>();
            GasPrice = new HashSet<GasPrice>();
            GasPriceStatusHistory = new HashSet<GasPriceStatusHistory>();
            Proposition = new HashSet<Proposition>();
            PropositionStatusHistory = new HashSet<PropositionStatusHistory>();
        }

        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ElectricityPrice> ElectricityPrice { get; set; }
        public virtual ICollection<ElectricityPriceStatusHistory> ElectricityPriceStatusHistory { get; set; }
        public virtual ICollection<GasPrice> GasPrice { get; set; }
        public virtual ICollection<GasPriceStatusHistory> GasPriceStatusHistory { get; set; }
        public virtual ICollection<Proposition> Proposition { get; set; }
        public virtual ICollection<PropositionStatusHistory> PropositionStatusHistory { get; set; }
    }
}
