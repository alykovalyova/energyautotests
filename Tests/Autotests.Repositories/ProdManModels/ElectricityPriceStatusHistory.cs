﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class ElectricityPriceStatusHistory
    {
        public Guid Id { get; set; }
        public Guid ElectricityPriceId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusModifiedOn { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }

        public virtual ElectricityPrice ElectricityPrice { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
