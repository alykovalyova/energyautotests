﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class ElectricityTariff
    {
        public ElectricityTariff()
        {
            ElectricityProfileCategory = new HashSet<ElectricityProfileCategory>();
        }

        public Guid Id { get; set; }
        public byte ElektraTariffTypeId { get; set; }
        public decimal ProductionPrice { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal LoyalCustomerDiscount { get; set; }
        public byte? PriceDiscountTypeId { get; set; }
        public decimal? PriceDiscountValue { get; set; }
        public Guid ElectricityPriceId { get; set; }

        public virtual ElectricityPrice ElectricityPrice { get; set; }
        public virtual DictionaryElektraTariffType ElektraTariffType { get; set; }
        public virtual DictionaryPriceDiscountType PriceDiscountType { get; set; }
        public virtual ICollection<ElectricityProfileCategory> ElectricityProfileCategory { get; set; }
    }
}
