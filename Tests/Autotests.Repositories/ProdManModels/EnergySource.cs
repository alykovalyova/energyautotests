﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class EnergySource
    {
        public Guid Id { get; set; }
        public Guid PropositionId { get; set; }
        public byte EnergySourceId { get; set; }

        public virtual DictionaryEnergySource EnergySourceNavigation { get; set; }
        public virtual Proposition Proposition { get; set; }
    }
}
