﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class GasPrice
    {
        public GasPrice()
        {
            GasPriceStatusHistory = new HashSet<GasPriceStatusHistory>();
            GasTariff = new HashSet<GasTariff>();
        }

        public Guid Id { get; set; }
        public Guid PropositionId { get; set; }
        public DateTime MarketFrom { get; set; }
        public byte StatusId { get; set; }
        public string Code { get; set; }
        public decimal StandingCharge { get; set; }
        public decimal CashBack { get; set; }
        public decimal NpvPerCustomer { get; set; }
        public byte MeasureUnitCodeId { get; set; }

        public virtual DictionaryMeasureUnitCode MeasureUnitCode { get; set; }
        public virtual Proposition Proposition { get; set; }
        public virtual DictionaryStatus Status { get; set; }
        public virtual ICollection<GasPriceStatusHistory> GasPriceStatusHistory { get; set; }
        public virtual ICollection<GasTariff> GasTariff { get; set; }
    }
}
