﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class GasPriceStatusHistory
    {
        public Guid Id { get; set; }
        public Guid GasPriceId { get; set; }
        public byte StatusId { get; set; }
        public DateTime StatusModifiedOn { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }

        public virtual GasPrice GasPrice { get; set; }
        public virtual DictionaryStatus Status { get; set; }
    }
}
