﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class GasProfileCategory
    {
        public Guid Id { get; set; }
        public byte EnergyUsageProfileCodeId { get; set; }
        public Guid GasTariffId { get; set; }

        public virtual DictionaryEnergyUsageProfileCode EnergyUsageProfileCode { get; set; }
        public virtual GasTariff GasTariff { get; set; }
    }
}
