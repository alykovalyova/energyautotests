﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class GasTariff
    {
        public GasTariff()
        {
            GasProfileCategory = new HashSet<GasProfileCategory>();
        }

        public Guid Id { get; set; }
        public byte GasRegionId { get; set; }
        public decimal RegionalSurcharge { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal LoyalCustomerDiscount { get; set; }
        public byte? PriceDiscountTypeId { get; set; }
        public decimal? PriceDiscountValue { get; set; }
        public Guid GasPriceId { get; set; }

        public virtual GasPrice GasPrice { get; set; }
        public virtual DictionaryGasRegion GasRegion { get; set; }
        public virtual DictionaryPriceDiscountType PriceDiscountType { get; set; }
        public virtual ICollection<GasProfileCategory> GasProfileCategory { get; set; }
    }
}
