﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class Incentive
    {
        public Incentive()
        {
            IncentivePayment = new HashSet<IncentivePayment>();
        }

        public Guid Id { get; set; }
        public byte IncentiveTypeId { get; set; }
        public decimal MaxCashback { get; set; }
        public decimal VoucherAmount { get; set; }
        public string GiftName { get; set; }
        public Guid PropositionId { get; set; }

        public virtual DictionaryIncentiveType IncentiveType { get; set; }
        public virtual Proposition Proposition { get; set; }
        public virtual ICollection<IncentivePayment> IncentivePayment { get; set; }
    }
}
