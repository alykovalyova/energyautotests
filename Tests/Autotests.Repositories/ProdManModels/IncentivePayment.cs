﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class IncentivePayment
    {
        public Guid Id { get; set; }
        public byte IncentivePaymentTypeId { get; set; }
        public Guid IncentiveId { get; set; }

        public virtual Incentive Incentive { get; set; }
        public virtual DictionaryIncentivePaymentType IncentivePaymentType { get; set; }
    }
}
