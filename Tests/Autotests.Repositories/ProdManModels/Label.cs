﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class Label
    {
        public byte LabelCode { get; set; }
        public string BalanceSupplierElektricityEan { get; set; }
        public string BalanceSupplierGasEan { get; set; }
        public string BalanceResponsiblePartyElektricityEan { get; set; }
        public string BalanceResponsiblePartyGasEan { get; set; }
        public string BalanceSupplierLargeElektricityEan { get; set; }
        public string BalanceSupplierLargeGasEan { get; set; }
        public string BalanceResponsiblePartyLargeElektricityEan { get; set; }
        public string BalanceResponsiblePartyLargeGasEan { get; set; }
        public bool IsActive { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Zipcode { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public string Iban { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }

        public virtual DictionaryLabels LabelCodeNavigation { get; set; }
    }
}
