﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class ProdManContext : DbContext
    {
        private readonly string _connectionString;

        public ProdManContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ProdManContext(DbContextOptions<ProdManContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClientType> ClientType { get; set; }
        public virtual DbSet<DictionaryElektraTariffType> DictionaryElektraTariffType { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergySource> DictionaryEnergySource { get; set; }
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCode { get; set; }
        public virtual DbSet<DictionaryGasRegion> DictionaryGasRegion { get; set; }
        public virtual DbSet<DictionaryIncentivePaymentType> DictionaryIncentivePaymentType { get; set; }
        public virtual DbSet<DictionaryIncentiveType> DictionaryIncentiveType { get; set; }
        public virtual DbSet<DictionaryLabels> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryMargeTypeN> DictionaryMargeTypeN { get; set; }
        public virtual DbSet<DictionaryMargeTypeNnle> DictionaryMargeTypeNnle { get; set; }
        public virtual DbSet<DictionaryMargeTypeR> DictionaryMargeTypeR { get; set; }
        public virtual DbSet<DictionaryMargeTypeRnle> DictionaryMargeTypeRnle { get; set; }
        public virtual DbSet<DictionaryMeasureUnitCode> DictionaryMeasureUnitCode { get; set; }
        public virtual DbSet<DictionaryPriceDiscountType> DictionaryPriceDiscountType { get; set; }
        public virtual DbSet<DictionaryStandingChargePeriodType> DictionaryStandingChargePeriodType { get; set; }
        public virtual DbSet<DictionaryStatus> DictionaryStatus { get; set; }
        public virtual DbSet<DictionaryTypeOfClient> DictionaryTypeOfClient { get; set; }
        public virtual DbSet<ElectricityPrice> ElectricityPrice { get; set; }
        public virtual DbSet<ElectricityPriceStatusHistory> ElectricityPriceStatusHistory { get; set; }
        public virtual DbSet<ElectricityProfileCategory> ElectricityProfileCategory { get; set; }
        public virtual DbSet<ElectricityTariff> ElectricityTariff { get; set; }
        public virtual DbSet<EnergySource> EnergySource { get; set; }
        public virtual DbSet<GasPrice> GasPrice { get; set; }
        public virtual DbSet<GasPriceStatusHistory> GasPriceStatusHistory { get; set; }
        public virtual DbSet<GasProfileCategory> GasProfileCategory { get; set; }
        public virtual DbSet<GasTariff> GasTariff { get; set; }
        public virtual DbSet<Incentive> Incentive { get; set; }
        public virtual DbSet<IncentivePayment> IncentivePayment { get; set; }
        public virtual DbSet<Label> Label { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<Proposition> Proposition { get; set; }
        public virtual DbSet<PropositionStatusHistory> PropositionStatusHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientType>(entity =>
            {
                entity.ToTable("ClientType", "dbo");

                entity.HasIndex(e => e.PropositionId)
                    .HasName("IX_PropositionId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.ClientType)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.ClientType_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.TypeOfClient)
                    .WithMany(p => p.ClientType)
                    .HasForeignKey(d => d.TypeOfClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ClientType_dbo.Dictionary_TypeOfClient_TypeOfClientId");
            });

            modelBuilder.Entity<DictionaryElektraTariffType>(entity =>
            {
                entity.ToTable("Dictionary_ElektraTariffType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergySource>(entity =>
            {
                entity.ToTable("Dictionary_EnergySource", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryGasRegion>(entity =>
            {
                entity.ToTable("Dictionary_GasRegion", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryIncentivePaymentType>(entity =>
            {
                entity.ToTable("Dictionary_IncentivePaymentType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryIncentiveType>(entity =>
            {
                entity.ToTable("Dictionary_IncentiveType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryLabels>(entity =>
            {
                entity.ToTable("Dictionary_Labels", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeN>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeN", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeNnle>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeNNLE", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeR>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeR", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeRnle>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeRNLE", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMeasureUnitCode>(entity =>
            {
                entity.ToTable("Dictionary_MeasureUnitCode", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryPriceDiscountType>(entity =>
            {
                entity.ToTable("Dictionary_PriceDiscountType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStandingChargePeriodType>(entity =>
            {
                entity.ToTable("Dictionary_StandingChargePeriodType", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryStatus>(entity =>
            {
                entity.ToTable("Dictionary_Status", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryTypeOfClient>(entity =>
            {
                entity.ToTable("Dictionary_TypeOfClient", "dbo");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<ElectricityPrice>(entity =>
            {
                entity.ToTable("ElectricityPrice", "dbo");

                entity.HasIndex(e => e.PropositionId)
                    .HasName("IX_PropositionId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CashBack).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MarketFrom).HasColumnType("date");

                entity.Property(e => e.NpvPerCustomer).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StandingCharge).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeasureUnitCode)
                    .WithMany(p => p.ElectricityPrice)
                    .HasForeignKey(d => d.MeasureUnitCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityPrice_dbo.Dictionary_MeasureUnitCode_MeasureUnitCodeId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.ElectricityPrice)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.ElectricityPrice_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ElectricityPrice)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityPrice_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<ElectricityPriceStatusHistory>(entity =>
            {
                entity.ToTable("ElectricityPriceStatusHistory", "dbo");

                entity.HasIndex(e => e.ElectricityPriceId)
                    .HasName("IX_ElectricityPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ElectricityPrice)
                    .WithMany(p => p.ElectricityPriceStatusHistory)
                    .HasForeignKey(d => d.ElectricityPriceId)
                    .HasConstraintName("FK_dbo.ElectricityPriceStatusHistory_dbo.ElectricityPrice_ElectricityPriceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ElectricityPriceStatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityPriceStatusHistory_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<ElectricityProfileCategory>(entity =>
            {
                entity.ToTable("ElectricityProfileCategory", "dbo");

                entity.HasIndex(e => e.ElectricityTariffId)
                    .HasName("IX_ElectricityTariffId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.ElectricityTariff)
                    .WithMany(p => p.ElectricityProfileCategory)
                    .HasForeignKey(d => d.ElectricityTariffId)
                    .HasConstraintName("FK_dbo.ElectricityProfileCategory_dbo.ElectricityTariff_ElectricityTariffId");

                entity.HasOne(d => d.EnergyUsageProfileCode)
                    .WithMany(p => p.ElectricityProfileCategory)
                    .HasForeignKey(d => d.EnergyUsageProfileCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityProfileCategory_dbo.Dictionary_EnergyUsageProfileCode_EnergyUsageProfileCodeId");
            });

            modelBuilder.Entity<ElectricityTariff>(entity =>
            {
                entity.ToTable("ElectricityTariff", "dbo");

                entity.HasIndex(e => e.ElectricityPriceId)
                    .HasName("IX_ElectricityPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.LoyalCustomerDiscount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.PriceDiscountValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductionPrice).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RetailPrice).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.ElectricityPrice)
                    .WithMany(p => p.ElectricityTariff)
                    .HasForeignKey(d => d.ElectricityPriceId)
                    .HasConstraintName("FK_dbo.ElectricityTariff_dbo.ElectricityPrice_ElectricityPriceId");

                entity.HasOne(d => d.ElektraTariffType)
                    .WithMany(p => p.ElectricityTariff)
                    .HasForeignKey(d => d.ElektraTariffTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ElectricityTariff_dbo.Dictionary_ElektraTariffType_ElektraTariffTypeId");

                entity.HasOne(d => d.PriceDiscountType)
                    .WithMany(p => p.ElectricityTariff)
                    .HasForeignKey(d => d.PriceDiscountTypeId)
                    .HasConstraintName("FK_dbo.ElectricityTariff_dbo.Dictionary_PriceDiscountType_PriceDiscountTypeId");
            });

            modelBuilder.Entity<EnergySource>(entity =>
            {
                entity.ToTable("EnergySource", "dbo");

                entity.HasIndex(e => e.PropositionId)
                    .HasName("IX_PropositionId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.EnergySourceNavigation)
                    .WithMany(p => p.EnergySource)
                    .HasForeignKey(d => d.EnergySourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.EnergySource_dbo.Dictionary_EnergySource_EnergySourceId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.EnergySource)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.EnergySource_dbo.Proposition_PropositionId");
            });

            modelBuilder.Entity<GasPrice>(entity =>
            {
                entity.ToTable("GasPrice", "dbo");

                entity.HasIndex(e => e.PropositionId)
                    .HasName("IX_PropositionId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CashBack).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MarketFrom).HasColumnType("date");

                entity.Property(e => e.NpvPerCustomer).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StandingCharge).HasColumnType("decimal(18, 5)");

                entity.HasOne(d => d.MeasureUnitCode)
                    .WithMany(p => p.GasPrice)
                    .HasForeignKey(d => d.MeasureUnitCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasPrice_dbo.Dictionary_MeasureUnitCode_MeasureUnitCodeId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.GasPrice)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.GasPrice_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GasPrice)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasPrice_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<GasPriceStatusHistory>(entity =>
            {
                entity.ToTable("GasPriceStatusHistory", "dbo");

                entity.HasIndex(e => e.GasPriceId)
                    .HasName("IX_GasPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.GasPrice)
                    .WithMany(p => p.GasPriceStatusHistory)
                    .HasForeignKey(d => d.GasPriceId)
                    .HasConstraintName("FK_dbo.GasPriceStatusHistory_dbo.GasPrice_GasPriceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.GasPriceStatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasPriceStatusHistory_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<GasProfileCategory>(entity =>
            {
                entity.ToTable("GasProfileCategory", "dbo");

                entity.HasIndex(e => e.GasTariffId)
                    .HasName("IX_GasTariffId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.EnergyUsageProfileCode)
                    .WithMany(p => p.GasProfileCategory)
                    .HasForeignKey(d => d.EnergyUsageProfileCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasProfileCategory_dbo.Dictionary_EnergyUsageProfileCode_EnergyUsageProfileCodeId");

                entity.HasOne(d => d.GasTariff)
                    .WithMany(p => p.GasProfileCategory)
                    .HasForeignKey(d => d.GasTariffId)
                    .HasConstraintName("FK_dbo.GasProfileCategory_dbo.GasTariff_GasTariffId");
            });

            modelBuilder.Entity<GasTariff>(entity =>
            {
                entity.ToTable("GasTariff", "dbo");

                entity.HasIndex(e => e.GasPriceId)
                    .HasName("IX_GasPriceId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.LoyalCustomerDiscount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.PriceDiscountValue).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.RegionalSurcharge).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.RetailPrice).HasColumnType("decimal(18, 6)");

                entity.HasOne(d => d.GasPrice)
                    .WithMany(p => p.GasTariff)
                    .HasForeignKey(d => d.GasPriceId)
                    .HasConstraintName("FK_dbo.GasTariff_dbo.GasPrice_GasPriceId");

                entity.HasOne(d => d.GasRegion)
                    .WithMany(p => p.GasTariff)
                    .HasForeignKey(d => d.GasRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.GasTariff_dbo.Dictionary_GasRegion_GasRegionId");

                entity.HasOne(d => d.PriceDiscountType)
                    .WithMany(p => p.GasTariff)
                    .HasForeignKey(d => d.PriceDiscountTypeId)
                    .HasConstraintName("FK_dbo.GasTariff_dbo.Dictionary_PriceDiscountType_PriceDiscountTypeId");
            });

            modelBuilder.Entity<Incentive>(entity =>
            {
                entity.ToTable("Incentive", "dbo");

                entity.HasIndex(e => e.PropositionId)
                    .HasName("IX_PropositionId");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.MaxCashback).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.VoucherAmount).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.IncentiveType)
                    .WithMany(p => p.Incentive)
                    .HasForeignKey(d => d.IncentiveTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Incentive_dbo.Dictionary_IncentiveType_IncentiveTypeId");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.Incentive)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.Incentive_dbo.Proposition_PropositionId");
            });

            modelBuilder.Entity<IncentivePayment>(entity =>
            {
                entity.ToTable("IncentivePayment", "dbo");

                entity.HasIndex(e => e.IncentiveId)
                    .HasName("IX_IncentiveId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.Incentive)
                    .WithMany(p => p.IncentivePayment)
                    .HasForeignKey(d => d.IncentiveId)
                    .HasConstraintName("FK_dbo.IncentivePayment_dbo.Incentive_IncentiveId");

                entity.HasOne(d => d.IncentivePaymentType)
                    .WithMany(p => p.IncentivePayment)
                    .HasForeignKey(d => d.IncentivePaymentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.IncentivePayment_dbo.Dictionary_IncentivePaymentType_IncentivePaymentTypeId");
            });

            modelBuilder.Entity<Label>(entity =>
            {
                entity.HasKey(e => e.LabelCode)
                    .HasName("PK_dbo.Label");

                entity.ToTable("Label", "dbo");

                entity.HasIndex(e => e.IsActive)
                    .HasName("IX_IsActive");

                entity.Property(e => e.BalanceResponsiblePartyElektricityEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceResponsiblePartyGasEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceResponsiblePartyLargeElektricityEan).HasMaxLength(13);

                entity.Property(e => e.BalanceResponsiblePartyLargeGasEan).HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierElektricityEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierGasEan)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierLargeElektricityEan).HasMaxLength(13);

                entity.Property(e => e.BalanceSupplierLargeGasEan).HasMaxLength(13);

                entity.Property(e => e.CityName).HasMaxLength(50);

                entity.Property(e => e.CompanyName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(50);

                entity.Property(e => e.Iban).HasMaxLength(35);

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.Property(e => e.StreetName).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(50);

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithOne(p => p.Label)
                    .HasForeignKey<Label>(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Label_dbo.Dictionary_Labels_LabelCode");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Proposition>(entity =>
            {
                entity.ToTable("Proposition", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Bna)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MargeTypeNnle).HasColumnName("MargeTypeNNLE");

                entity.Property(e => e.MargeTypeRnle).HasColumnName("MargeTypeRNLE");

                entity.Property(e => e.MarketFrom).HasColumnType("date");

                entity.Property(e => e.MarketTo).HasColumnType("date");

                entity.Property(e => e.ReimburseFine).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StandingChargePeriodId).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_Labels_LabelCode");

                entity.HasOne(d => d.MargeTypeNNavigation)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.MargeTypeN)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeN_MargeTypeN");

                entity.HasOne(d => d.MargeTypeNnleNavigation)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.MargeTypeNnle)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeNNLE_MargeTypeNNLE");

                entity.HasOne(d => d.MargeTypeRNavigation)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.MargeTypeR)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeR_MargeTypeR");

                entity.HasOne(d => d.MargeTypeRnleNavigation)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.MargeTypeRnle)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_MargeTypeRNLE_MargeTypeRNLE");

                entity.HasOne(d => d.StandingChargePeriod)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.StandingChargePeriodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_StandingChargePeriodType_StandingChargePeriodId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Proposition)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Proposition_dbo.Dictionary_Status_StatusId");
            });

            modelBuilder.Entity<PropositionStatusHistory>(entity =>
            {
                entity.ToTable("PropositionStatusHistory", "dbo");

                entity.HasIndex(e => e.PropositionId)
                    .HasName("IX_PropositionId");

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.StatusModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Proposition)
                    .WithMany(p => p.PropositionStatusHistory)
                    .HasForeignKey(d => d.PropositionId)
                    .HasConstraintName("FK_dbo.PropositionStatusHistory_dbo.Proposition_PropositionId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PropositionStatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PropositionStatusHistory_dbo.Dictionary_Status_StatusId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
