﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProdManModels
{
    public partial class Proposition
    {
        public Proposition()
        {
            ClientType = new HashSet<ClientType>();
            ElectricityPrice = new HashSet<ElectricityPrice>();
            EnergySource = new HashSet<EnergySource>();
            GasPrice = new HashSet<GasPrice>();
            Incentive = new HashSet<Incentive>();
            PropositionStatusHistory = new HashSet<PropositionStatusHistory>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime MarketFrom { get; set; }
        public DateTime? MarketTo { get; set; }
        public bool IsEligibleAsRenewal { get; set; }
        public bool IsSalesEligible { get; set; }
        public int Duration { get; set; }
        public bool IsFineApplicable { get; set; }
        public int CoolDownPeriod { get; set; }
        public decimal ReimburseFine { get; set; }
        public int? ContractStartWindow { get; set; }
        public byte StatusId { get; set; }
        public bool IsFixedPriceCombination { get; set; }
        public string Description { get; set; }
        public byte LabelCode { get; set; }
        public byte StandingChargePeriodId { get; set; }
        public byte? MargeTypeN { get; set; }
        public byte? MargeTypeR { get; set; }
        public byte? MargeTypeNnle { get; set; }
        public byte? MargeTypeRnle { get; set; }
        public bool ReclaimPenaltyOneYear { get; set; }
        public string GeneralInformation { get; set; }
        public bool? Bna { get; set; }
        public int? PresentValue { get; set; }
        public int? CostPerOrder { get; set; }

        public virtual DictionaryLabels LabelCodeNavigation { get; set; }
        public virtual DictionaryMargeTypeN MargeTypeNNavigation { get; set; }
        public virtual DictionaryMargeTypeNnle MargeTypeNnleNavigation { get; set; }
        public virtual DictionaryMargeTypeR MargeTypeRNavigation { get; set; }
        public virtual DictionaryMargeTypeRnle MargeTypeRnleNavigation { get; set; }
        public virtual DictionaryStandingChargePeriodType StandingChargePeriod { get; set; }
        public virtual DictionaryStatus Status { get; set; }
        public virtual ICollection<ClientType> ClientType { get; set; }
        public virtual ICollection<ElectricityPrice> ElectricityPrice { get; set; }
        public virtual ICollection<EnergySource> EnergySource { get; set; }
        public virtual ICollection<GasPrice> GasPrice { get; set; }
        public virtual ICollection<Incentive> Incentive { get; set; }
        public virtual ICollection<PropositionStatusHistory> PropositionStatusHistory { get; set; }
    }
}
