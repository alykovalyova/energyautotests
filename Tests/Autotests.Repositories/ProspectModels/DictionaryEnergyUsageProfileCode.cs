﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProspectModels
{
    public partial class DictionaryEnergyUsageProfileCode
    {
        public DictionaryEnergyUsageProfileCode()
        {
            MeteringPoint = new HashSet<MeteringPoint>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
    }
}
