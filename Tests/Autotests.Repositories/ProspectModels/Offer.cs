﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProspectModels
{
    public partial class Offer
    {
        public Offer()
        {
            MeteringPoint = new HashSet<MeteringPoint>();
            OfferStatusHistory = new HashSet<OfferStatusHistory>();
        }

        public int Id { get; set; }
        public int ProspectId { get; set; }
        public Guid PropositionId { get; set; }
        public string PropositionName { get; set; }
        public string SalesAgentName { get; set; }
        public decimal? Cashback { get; set; }
        public byte LastStatusId { get; set; }
        public string LastComment { get; set; }
        public Guid ExternalReference { get; set; }
        public string CalculationDetailsSnapshot { get; set; }
        public string Zipcode { get; set; }
        public int BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public DateTime? ElkContractEndDate { get; set; }
        public DateTime? GasContractEndDate { get; set; }
        public DateTime? GetEndDateRequestDate { get; set; }
        public bool IsCalculationsBasedOnEdsn { get; set; }

        public virtual Prospect Prospect { get; set; }
        public virtual ICollection<MeteringPoint> MeteringPoint { get; set; }
        public virtual ICollection<OfferStatusHistory> OfferStatusHistory { get; set; }
    }
}
