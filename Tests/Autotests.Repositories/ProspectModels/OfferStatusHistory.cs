﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProspectModels
{
    public partial class OfferStatusHistory
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int StatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Comment { get; set; }
        public string User { get; set; }

        public virtual Offer Offer { get; set; }
        public virtual DictionaryOfferStatusCode Status { get; set; }
    }
}
