﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProspectModels
{
    public partial class Prospect
    {
        public Prospect()
        {
            Offer = new HashSet<Offer>();
        }

        public int Id { get; set; }
        public int LabelId { get; set; }
        public int ClientTypeId { get; set; }
        public int GenderId { get; set; }
        public int ProspectStatusId { get; set; }
        public string Initials { get; set; }
        public string Prefix { get; set; }
        public string Surname { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCocNumber { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumberHome { get; set; }
        public string PhoneNumberMobile { get; set; }
        public int SourceId { get; set; }
        public string Zipcode { get; set; }
        public int? BuildingNr { get; set; }
        public string ExBuildingNr { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Iban { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual DictionaryClientType ClientType { get; set; }
        public virtual DictionaryGender Gender { get; set; }
        public virtual DictionaryLabels Label { get; set; }
        public virtual DictionaryProspectStatusCode ProspectStatus { get; set; }
        public virtual ICollection<Offer> Offer { get; set; }
    }
}
