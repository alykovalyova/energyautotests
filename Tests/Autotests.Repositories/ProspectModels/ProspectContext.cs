﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ProspectModels
{
    public partial class ProspectContext : DbContext
    {
        private readonly string _connectionString;

        public ProspectContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ProspectContext(DbContextOptions<ProspectContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DictionaryClientType> DictionaryClientType { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCode { get; set; }
        public virtual DbSet<DictionaryEnergyUsageProfileCode> DictionaryEnergyUsageProfileCode { get; set; }
        public virtual DbSet<DictionaryGender> DictionaryGender { get; set; }
        public virtual DbSet<DictionaryLabels> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryOfferStatusCode> DictionaryOfferStatusCode { get; set; }
        public virtual DbSet<DictionaryProspectStatusCode> DictionaryProspectStatusCode { get; set; }
        public virtual DbSet<MeteringPoint> MeteringPoint { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<Offer> Offer { get; set; }
        public virtual DbSet<OfferStatusHistory> OfferStatusHistory { get; set; }
        public virtual DbSet<Prospect> Prospect { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DictionaryClientType>(entity =>
            {
                entity.ToTable("Dictionary_ClientType", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyUsageProfileCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyUsageProfileCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryGender>(entity =>
            {
                entity.ToTable("Dictionary_Gender", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryLabels>(entity =>
            {
                entity.ToTable("Dictionary_Labels", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryOfferStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_OfferStatusCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryProspectStatusCode>(entity =>
            {
                entity.ToTable("Dictionary_ProspectStatusCode", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<MeteringPoint>(entity =>
            {
                entity.ToTable("MeteringPoint", "dbo");

                entity.HasIndex(e => e.OfferId)
                    .HasName("IX_OfferId");

                entity.Property(e => e.CaptarCode).HasMaxLength(13);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.GridArea).HasMaxLength(20);

                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.OfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Offer_OfferId");

                entity.HasOne(d => d.ProductType)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.ProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyProductTypeCode_ProductTypeId");

                entity.HasOne(d => d.UsageProfile)
                    .WithMany(p => p.MeteringPoint)
                    .HasForeignKey(d => d.UsageProfileId)
                    .HasConstraintName("FK_dbo.MeteringPoint_dbo.Dictionary_EnergyUsageProfileCode_UsageProfileId");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory", "dbo");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.ToTable("Offer", "dbo");

                entity.HasIndex(e => e.ExternalReference)
                    .HasName("IX_ExternalReference");

                entity.HasIndex(e => e.ProspectId)
                    .HasName("IX_ProspectId");

                entity.Property(e => e.Cashback).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.ElkContractEndDate).HasColumnType("datetime");

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.GasContractEndDate).HasColumnType("datetime");

                entity.Property(e => e.GetEndDateRequestDate).HasColumnType("datetime");

                entity.Property(e => e.LastComment).HasMaxLength(255);

                entity.Property(e => e.PropositionName).IsRequired();

                entity.Property(e => e.SalesAgentName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Street).HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .IsRequired()
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.Prospect)
                    .WithMany(p => p.Offer)
                    .HasForeignKey(d => d.ProspectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Offer_dbo.Prospect_ProspectId");
            });

            modelBuilder.Entity<OfferStatusHistory>(entity =>
            {
                entity.ToTable("OfferStatusHistory", "dbo");

                entity.HasIndex(e => e.OfferId)
                    .HasName("IX_OfferId");

                entity.Property(e => e.Comment).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.User).HasMaxLength(50);

                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.OfferStatusHistory)
                    .HasForeignKey(d => d.OfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.OfferStatusHistory_dbo.Offer_OfferId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.OfferStatusHistory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.OfferStatusHistory_dbo.Dictionary_OfferStatusCode_StatusId");
            });

            modelBuilder.Entity<Prospect>(entity =>
            {
                entity.ToTable("Prospect", "dbo");

                entity.Property(e => e.Birthday).HasColumnType("datetime");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CompanyCocNumber).HasMaxLength(20);

                entity.Property(e => e.CompanyName).HasMaxLength(50);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EmailAddress).HasMaxLength(255);

                entity.Property(e => e.ExBuildingNr).HasMaxLength(10);

                entity.Property(e => e.Iban).HasMaxLength(35);

                entity.Property(e => e.Initials).HasMaxLength(20);

                entity.Property(e => e.PhoneNumberHome).HasMaxLength(20);

                entity.Property(e => e.PhoneNumberMobile).HasMaxLength(20);

                entity.Property(e => e.Prefix).HasMaxLength(20);

                entity.Property(e => e.Street).HasMaxLength(50);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCode")
                    .HasMaxLength(10);

                entity.HasOne(d => d.ClientType)
                    .WithMany(p => p.Prospect)
                    .HasForeignKey(d => d.ClientTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Prospect_dbo.Dictionary_ClientType_ClientTypeId");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.Prospect)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Prospect_dbo.Dictionary_Gender_GenderId");

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.Prospect)
                    .HasForeignKey(d => d.LabelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Prospect_dbo.Dictionary_Labels_LabelId");

                entity.HasOne(d => d.ProspectStatus)
                    .WithMany(p => p.Prospect)
                    .HasForeignKey(d => d.ProspectStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Prospect_dbo.Dictionary_ProspectStatusCode_ProspectStatusId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
