﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class CustomerInformationEan
    {
        public string Ean { get; set; }
        public short LabelCodeId { get; set; }
        public int ContractId { get; set; }

        public virtual EnumLabel LabelCode { get; set; }
    }
}
