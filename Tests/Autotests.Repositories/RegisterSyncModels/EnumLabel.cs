﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class EnumLabel
    {
        public EnumLabel()
        {
            CustomerInformationEan = new HashSet<CustomerInformationEan>();
            EanMismatchEdsnLabelCode = new HashSet<EanMismatch>();
            EanMismatchEnerfreeLabelCode = new HashSet<EanMismatch>();
            EanMismatchIcarLabelCode = new HashSet<EanMismatch>();
            EdsnEan = new HashSet<EdsnEan>();
            IcarEan = new HashSet<IcarEan>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<CustomerInformationEan> CustomerInformationEan { get; set; }
        public virtual ICollection<EanMismatch> EanMismatchEdsnLabelCode { get; set; }
        public virtual ICollection<EanMismatch> EanMismatchEnerfreeLabelCode { get; set; }
        public virtual ICollection<EanMismatch> EanMismatchIcarLabelCode { get; set; }
        public virtual ICollection<EdsnEan> EdsnEan { get; set; }
        public virtual ICollection<IcarEan> IcarEan { get; set; }
    }
}
