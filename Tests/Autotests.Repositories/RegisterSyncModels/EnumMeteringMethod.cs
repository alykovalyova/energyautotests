﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class EnumMeteringMethod
    {
        public EnumMeteringMethod()
        {
            EanMismatch = new HashSet<EanMismatch>();
            EdsnEan = new HashSet<EdsnEan>();
            IcarEan = new HashSet<IcarEan>();
        }

        public short Id { get; set; }
        public string Identifier { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual ICollection<EanMismatch> EanMismatch { get; set; }
        public virtual ICollection<EdsnEan> EdsnEan { get; set; }
        public virtual ICollection<IcarEan> IcarEan { get; set; }
    }
}
