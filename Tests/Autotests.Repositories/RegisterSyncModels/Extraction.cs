﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class Extraction
    {
        public int Id { get; set; }
        public DateTime ExtractionDate { get; set; }
        public int TotalMessages { get; set; }
        public int CountMessages { get; set; }
        public short SourceTypeId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public virtual EnumSourceType SourceType { get; set; }
    }
}
