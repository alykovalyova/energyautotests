﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class IcarEan
    {
        public string Ean { get; set; }
        public string BalanceSupplier { get; set; }
        public short LabelCodeId { get; set; }
        public DateTime MutationDate { get; set; }
        public short MeteringMethodCodeId { get; set; }
        public short PhysicalStatusMeterCodeId { get; set; }
        public short? EnergyFlowDirectionId { get; set; }

        public virtual EnumEnergyFlowDirection EnergyFlowDirection { get; set; }
        public virtual EnumLabel LabelCode { get; set; }
        public virtual EnumMeteringMethod MeteringMethodCode { get; set; }
        public virtual EnumPhysicalStatusMeter PhysicalStatusMeterCode { get; set; }
    }
}
