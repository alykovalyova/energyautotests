﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class RegisterSyncContext : DbContext
    {
        private readonly string _connectionString;

        public RegisterSyncContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public RegisterSyncContext(DbContextOptions<RegisterSyncContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CustomerInformationEan> CustomerInformationEan { get; set; }
        public virtual DbSet<EanMismatch> EanMismatch { get; set; }
        public virtual DbSet<EdsnEan> EdsnEan { get; set; }
        public virtual DbSet<EnumEnergyFlowDirection> EnumEnergyFlowDirection { get; set; }
        public virtual DbSet<EnumLabel> EnumLabel { get; set; }
        public virtual DbSet<EnumMeteringMethod> EnumMeteringMethod { get; set; }
        public virtual DbSet<EnumMismatchStatus> EnumMismatchStatus { get; set; }
        public virtual DbSet<EnumPhysicalStatusMeter> EnumPhysicalStatusMeter { get; set; }
        public virtual DbSet<EnumSourceType> EnumSourceType { get; set; }
        public virtual DbSet<Extraction> Extraction { get; set; }
        public virtual DbSet<IcarEan> IcarEan { get; set; }
        public virtual DbSet<Report> Report { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerInformationEan>(entity =>
            {
                entity.HasKey(e => e.Ean);

                entity.ToTable("CustomerInformationEan", "dbo");

                entity.HasIndex(e => e.LabelCodeId);

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.HasOne(d => d.LabelCode)
                    .WithMany(p => p.CustomerInformationEan)
                    .HasForeignKey(d => d.LabelCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EanMismatch>(entity =>
            {
                entity.ToTable("EanMismatch", "dbo");

                entity.HasIndex(e => e.Ean)
                    .HasName("IX_Ean");

                entity.HasIndex(e => e.EdsnLabelCodeId);

                entity.HasIndex(e => e.EnerfreeLabelCodeId);

                entity.HasIndex(e => e.IcarLabelCodeId);

                entity.HasIndex(e => e.MeteringMethodCodeId);

                entity.HasIndex(e => e.PhysicalStatusMeterCodeId);

                entity.HasIndex(e => e.ReportId);

                entity.HasIndex(e => e.StatusId);

                entity.Property(e => e.AssignedTo).HasMaxLength(150);

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.EdsnLabelCode)
                    .WithMany(p => p.EanMismatchEdsnLabelCode)
                    .HasForeignKey(d => d.EdsnLabelCodeId);

                entity.HasOne(d => d.EnerfreeLabelCode)
                    .WithMany(p => p.EanMismatchEnerfreeLabelCode)
                    .HasForeignKey(d => d.EnerfreeLabelCodeId);

                entity.HasOne(d => d.IcarLabelCode)
                    .WithMany(p => p.EanMismatchIcarLabelCode)
                    .HasForeignKey(d => d.IcarLabelCodeId);

                entity.HasOne(d => d.MeteringMethodCode)
                    .WithMany(p => p.EanMismatch)
                    .HasForeignKey(d => d.MeteringMethodCodeId);

                entity.HasOne(d => d.PhysicalStatusMeterCode)
                    .WithMany(p => p.EanMismatch)
                    .HasForeignKey(d => d.PhysicalStatusMeterCodeId);

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.EanMismatch)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.EanMismatch)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EdsnEan>(entity =>
            {
                entity.HasKey(e => e.Ean);

                entity.ToTable("EdsnEan", "dbo");

                entity.HasIndex(e => e.LabelCodeId);

                entity.HasIndex(e => e.MeteringMethodCodeId);

                entity.HasIndex(e => e.PhysicalStatusMeterCodeId);

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.Property(e => e.BalanceSupplier)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.HasOne(d => d.LabelCode)
                    .WithMany(p => p.EdsnEan)
                    .HasForeignKey(d => d.LabelCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethodCode)
                    .WithMany(p => p.EdsnEan)
                    .HasForeignKey(d => d.MeteringMethodCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PhysicalStatusMeterCode)
                    .WithMany(p => p.EdsnEan)
                    .HasForeignKey(d => d.PhysicalStatusMeterCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EnumEnergyFlowDirection>(entity =>
            {
                entity.ToTable("EnumEnergyFlowDirection", "dbo");
            });

            modelBuilder.Entity<EnumLabel>(entity =>
            {
                entity.ToTable("EnumLabel", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();
            });

            modelBuilder.Entity<EnumMeteringMethod>(entity =>
            {
                entity.ToTable("EnumMeteringMethod", "dbo");
            });

            modelBuilder.Entity<EnumMismatchStatus>(entity =>
            {
                entity.ToTable("EnumMismatchStatus", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();
            });

            modelBuilder.Entity<EnumPhysicalStatusMeter>(entity =>
            {
                entity.ToTable("EnumPhysicalStatusMeter", "dbo");
            });

            modelBuilder.Entity<EnumSourceType>(entity =>
            {
                entity.ToTable("EnumSourceType", "dbo");

                entity.HasIndex(e => e.Identifier)
                    .IsUnique();
            });

            modelBuilder.Entity<Extraction>(entity =>
            {
                entity.ToTable("Extraction", "dbo");

                entity.HasIndex(e => e.ExtractionDate)
                    .HasName("IX_ExtractionDate");

                entity.HasIndex(e => e.SourceTypeId);

                entity.Property(e => e.ExtractionDate).HasColumnType("date");

                entity.HasOne(d => d.SourceType)
                    .WithMany(p => p.Extraction)
                    .HasForeignKey(d => d.SourceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<IcarEan>(entity =>
            {
                entity.HasKey(e => e.Ean);

                entity.ToTable("IcarEan", "dbo");

                entity.HasIndex(e => e.EnergyFlowDirectionId);

                entity.HasIndex(e => e.LabelCodeId);

                entity.HasIndex(e => e.MeteringMethodCodeId);

                entity.HasIndex(e => e.PhysicalStatusMeterCodeId);

                entity.Property(e => e.Ean).HasMaxLength(18);

                entity.Property(e => e.BalanceSupplier)
                    .IsRequired()
                    .HasMaxLength(13);

                entity.Property(e => e.MutationDate).HasColumnType("date");

                entity.HasOne(d => d.EnergyFlowDirection)
                    .WithMany(p => p.IcarEan)
                    .HasForeignKey(d => d.EnergyFlowDirectionId);

                entity.HasOne(d => d.LabelCode)
                    .WithMany(p => p.IcarEan)
                    .HasForeignKey(d => d.LabelCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MeteringMethodCode)
                    .WithMany(p => p.IcarEan)
                    .HasForeignKey(d => d.MeteringMethodCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.PhysicalStatusMeterCode)
                    .WithMany(p => p.IcarEan)
                    .HasForeignKey(d => d.PhysicalStatusMeterCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.ToTable("Report", "dbo");

                entity.HasIndex(e => e.ExtractionDate)
                    .HasName("IX_ExtractionDate");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.ExtractionDate).HasColumnType("date");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
