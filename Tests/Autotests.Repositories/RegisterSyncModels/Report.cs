﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.RegisterSyncModels
{
    public partial class Report
    {
        public Report()
        {
            EanMismatch = new HashSet<EanMismatch>();
        }

        public int Id { get; set; }
        public DateTime ExtractionDate { get; set; }
        public bool CreatedManually { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Labels { get; set; }
        public int MismatchesCount { get; set; }
        public bool Completed { get; set; }

        public virtual ICollection<EanMismatch> EanMismatch { get; set; }
    }
}
