﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class BalanceSupplier
    {
        public BalanceSupplier()
        {
            RenewableContracts = new HashSet<RenewableContract>();
        }

        public int Id { get; set; }
        public string Ean { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RenewableContract> RenewableContracts { get; set; }
    }
}
