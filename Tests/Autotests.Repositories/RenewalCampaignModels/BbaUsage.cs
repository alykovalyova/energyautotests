﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class BbaUsage
    {
        public int Id { get; set; }
        public int BbaUsageChangeId { get; set; }
        public int ContractRegelId { get; set; }
        public string Ean { get; set; }
        public bool? IsResidential { get; set; }
        public int UsageEacPeak { get; set; }
        public int? UsageEacOffPeak { get; set; }
        public int ProductTypeId { get; set; }
        public int? UsageTypeId { get; set; }
        public int? UsageEapPeak { get; set; }
        public int? UsageEapOffPeak { get; set; }

        public virtual BbaUsageChange BbaUsageChange { get; set; }
        public virtual DictionaryUsageType UsageType { get; set; }
    }
}
