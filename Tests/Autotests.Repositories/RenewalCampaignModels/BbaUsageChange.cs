﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class BbaUsageChange
    {
        public BbaUsageChange()
        {
            BbaUsages = new HashSet<BbaUsage>();
            CustomOffers = new HashSet<CustomOffer>();
        }

        public int Id { get; set; }
        public int RenewableContractId { get; set; }
        public DateTime ValidFrom { get; set; }
        public bool IsCustomerSource { get; set; }

        public virtual RenewableContract RenewableContract { get; set; }
        public virtual ICollection<BbaUsage> BbaUsages { get; set; }
        public virtual ICollection<CustomOffer> CustomOffers { get; set; }
    }
}
