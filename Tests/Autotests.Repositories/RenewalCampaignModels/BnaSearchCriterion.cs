﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class BnaSearchCriterion
    {
        public int Id { get; set; }
        public int BnaSettingId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int OperatorId { get; set; }

        public virtual BnaSetting BnaSetting { get; set; }
    }
}
