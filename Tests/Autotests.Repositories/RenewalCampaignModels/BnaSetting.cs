﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class BnaSetting
    {
        public BnaSetting()
        {
            BnaSearchCriteria = new HashSet<BnaSearchCriterion>();
        }

        public int Id { get; set; }
        public double Percentage { get; set; }

        public virtual ICollection<BnaSearchCriterion> BnaSearchCriteria { get; set; }
    }
}
