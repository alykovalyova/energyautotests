﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class Campaign
    {
        public Campaign()
        {
            CampaignContracts = new HashSet<CampaignContract>();
            CampaignStatusHistories = new HashSet<CampaignStatusHistory>();
            ContractSelections = new HashSet<ContractSelection>();
            PlannedEvents = new HashSet<PlannedEvent>();
        }

        public int Id { get; set; }
        public string CampaignName { get; set; }
        public int CampaignTypeId { get; set; }
        public int CampaignStatusId { get; set; }
        public int LabelCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? RenewalPageIdFromOnline { get; set; }
        public int? Priority { get; set; }
        public int CustomerTypeId { get; set; }
        public int? SelectedContractCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public int SalesChannelId { get; set; }
        public double? Probability { get; set; }
        public int CustomerProductTypeId { get; set; }

        public virtual DictionaryCampaignStatus CampaignStatus { get; set; }
        public virtual DictionaryCampaignType CampaignType { get; set; }
        public virtual DictionaryCustomerProductType CustomerProductType { get; set; }
        public virtual DictionaryRelationCategory CustomerType { get; set; }
        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual ICollection<CampaignContract> CampaignContracts { get; set; }
        public virtual ICollection<CampaignStatusHistory> CampaignStatusHistories { get; set; }
        public virtual ICollection<ContractSelection> ContractSelections { get; set; }
        public virtual ICollection<PlannedEvent> PlannedEvents { get; set; }
    }
}
