﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CampaignContract
    {
        public CampaignContract()
        {
            CampaignContractStatusHistories = new HashSet<CampaignContractStatusHistory>();
            CustomerOfferAcknowledgmentActions = new HashSet<CustomerOfferAcknowledgmentAction>();
            ExecutedContractEvents = new HashSet<ExecutedContractEvent>();
        }

        public int Id { get; set; }
        public int RenewableContractId { get; set; }
        public int CampaignId { get; set; }
        public int CampaignContractStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string AcceptedBy { get; set; }
        public int? AcceptedOfferPropositionId { get; set; }
        public string CustomerIpAddress { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual DictionaryCampaignContractStatus CampaignContractStatus { get; set; }
        public virtual RenewableContract RenewableContract { get; set; }
        public virtual ICollection<CampaignContractStatusHistory> CampaignContractStatusHistories { get; set; }
        public virtual ICollection<CustomerOfferAcknowledgmentAction> CustomerOfferAcknowledgmentActions { get; set; }
        public virtual ICollection<ExecutedContractEvent> ExecutedContractEvents { get; set; }
    }
}
