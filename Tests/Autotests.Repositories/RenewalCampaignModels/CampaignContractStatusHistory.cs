﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CampaignContractStatusHistory
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CampaignContractId { get; set; }
        public int CampaignContractStatusId { get; set; }

        public virtual CampaignContract CampaignContract { get; set; }
        public virtual DictionaryCampaignContractStatus CampaignContractStatus { get; set; }
    }
}
