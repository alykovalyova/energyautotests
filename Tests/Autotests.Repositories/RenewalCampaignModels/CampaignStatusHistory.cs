﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CampaignStatusHistory
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ResponsibleUser { get; set; }
        public int CampaignId { get; set; }
        public int CampaignStatusId { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual DictionaryCampaignStatus CampaignStatus { get; set; }
    }
}
