﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class ContractSelection
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int OperatorId { get; set; }
        public int CampaignId { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual DictionaryOperator Operator { get; set; }
    }
}
