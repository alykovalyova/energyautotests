﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CustomOffer
    {
        public CustomOffer()
        {
            CustomOfferPropositions = new HashSet<CustomOfferProposition>();
            CustomOfferStatusHistories = new HashSet<CustomOfferStatusHistory>();
        }

        public int Id { get; set; }
        public int RenewableContractId { get; set; }
        public int CustomOfferStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ExpiredOn { get; set; }
        public int? BbaUsageChangeId { get; set; }
        public Guid ExternalReference { get; set; }
        public string LastStatusChangedBy { get; set; }
        public int EmailSendStatusId { get; set; }
        public int? AcceptedOfferPropositionId { get; set; }
        public int? OfferBundleId { get; set; }
        public string CustomerIpAddress { get; set; }

        public virtual BbaUsageChange BbaUsageChange { get; set; }
        public virtual DictionaryCustomOfferStatus CustomOfferStatus { get; set; }
        public virtual DictionaryEmailSendStatus EmailSendStatus { get; set; }
        public virtual RenewableContract RenewableContract { get; set; }
        public virtual ICollection<CustomOfferProposition> CustomOfferPropositions { get; set; }
        public virtual ICollection<CustomOfferStatusHistory> CustomOfferStatusHistories { get; set; }
    }
}
