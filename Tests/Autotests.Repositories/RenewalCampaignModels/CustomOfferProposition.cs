﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CustomOfferProposition
    {
        public int Id { get; set; }
        public int CustomOfferId { get; set; }
        public int? OfferPropositionId { get; set; }

        public virtual CustomOffer CustomOffer { get; set; }
    }
}
