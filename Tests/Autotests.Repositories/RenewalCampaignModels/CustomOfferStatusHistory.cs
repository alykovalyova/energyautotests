﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CustomOfferStatusHistory
    {
        public int Id { get; set; }
        public int? CustomOfferOldStatusId { get; set; }
        public int CustomOfferNewStatusId { get; set; }
        public int CustomOfferId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string StatusChangedBy { get; set; }

        public virtual CustomOffer CustomOffer { get; set; }
    }
}
