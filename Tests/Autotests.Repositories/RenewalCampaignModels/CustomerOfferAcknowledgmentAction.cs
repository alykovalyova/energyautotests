﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class CustomerOfferAcknowledgmentAction
    {
        public long Id { get; set; }
        public int ActionId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? CampaignContractId { get; set; }

        public virtual CampaignContract CampaignContract { get; set; }
    }
}
