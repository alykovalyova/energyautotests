﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryCampaignContractStatus
    {
        public DictionaryCampaignContractStatus()
        {
            CampaignContractStatusHistories = new HashSet<CampaignContractStatusHistory>();
            CampaignContracts = new HashSet<CampaignContract>();
            RenewableContracts = new HashSet<RenewableContract>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CampaignContractStatusHistory> CampaignContractStatusHistories { get; set; }
        public virtual ICollection<CampaignContract> CampaignContracts { get; set; }
        public virtual ICollection<RenewableContract> RenewableContracts { get; set; }
    }
}
