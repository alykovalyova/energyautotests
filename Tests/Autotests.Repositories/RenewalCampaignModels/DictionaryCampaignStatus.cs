﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryCampaignStatus
    {
        public DictionaryCampaignStatus()
        {
            CampaignStatusHistories = new HashSet<CampaignStatusHistory>();
            Campaigns = new HashSet<Campaign>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CampaignStatusHistory> CampaignStatusHistories { get; set; }
        public virtual ICollection<Campaign> Campaigns { get; set; }
    }
}
