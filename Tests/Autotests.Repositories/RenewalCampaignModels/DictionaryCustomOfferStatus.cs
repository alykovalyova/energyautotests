﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryCustomOfferStatus
    {
        public DictionaryCustomOfferStatus()
        {
            CustomOffers = new HashSet<CustomOffer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomOffer> CustomOffers { get; set; }
    }
}
