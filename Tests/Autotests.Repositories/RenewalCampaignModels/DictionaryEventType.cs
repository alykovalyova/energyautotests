﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryEventType
    {
        public DictionaryEventType()
        {
            PlannedEvents = new HashSet<PlannedEvent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PlannedEvent> PlannedEvents { get; set; }
    }
}
