﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryLabel
    {
        public DictionaryLabel()
        {
            Campaigns = new HashSet<Campaign>();
            EmailTemplates = new HashSet<EmailTemplate>();
            RenewableContracts = new HashSet<RenewableContract>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Campaign> Campaigns { get; set; }
        public virtual ICollection<EmailTemplate> EmailTemplates { get; set; }
        public virtual ICollection<RenewableContract> RenewableContracts { get; set; }
    }
}
