﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryMargeTypeR
    {
        public DictionaryMargeTypeR()
        {
            RenewableContracts = new HashSet<RenewableContract>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<RenewableContract> RenewableContracts { get; set; }
    }
}
