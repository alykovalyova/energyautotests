﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryOperator
    {
        public DictionaryOperator()
        {
            ContractSelections = new HashSet<ContractSelection>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ContractSelection> ContractSelections { get; set; }
    }
}
