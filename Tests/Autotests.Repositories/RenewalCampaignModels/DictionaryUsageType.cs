﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class DictionaryUsageType
    {
        public DictionaryUsageType()
        {
            BbaUsages = new HashSet<BbaUsage>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<BbaUsage> BbaUsages { get; set; }
    }
}
