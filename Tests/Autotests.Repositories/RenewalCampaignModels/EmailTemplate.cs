﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class EmailTemplate
    {
        public EmailTemplate()
        {
            PlannedEvents = new HashSet<PlannedEvent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int LabelCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid TemplateId { get; set; }

        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual ICollection<PlannedEvent> PlannedEvents { get; set; }
    }
}
