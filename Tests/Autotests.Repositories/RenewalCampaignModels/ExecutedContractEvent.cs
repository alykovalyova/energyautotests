﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class ExecutedContractEvent
    {
        public int Id { get; set; }
        public int ExecutedEventId { get; set; }
        public int CampaignContractId { get; set; }
        public Guid ExecutedContractEventCode { get; set; }

        public virtual CampaignContract CampaignContract { get; set; }
        public virtual ExecutedEvent ExecutedEvent { get; set; }
    }
}
