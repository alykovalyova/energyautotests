﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class ExecutedEvent
    {
        public ExecutedEvent()
        {
            ExecutedContractEvents = new HashSet<ExecutedContractEvent>();
        }

        public int Id { get; set; }
        public int PlannedEventId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual PlannedEvent PlannedEvent { get; set; }
        public virtual ICollection<ExecutedContractEvent> ExecutedContractEvents { get; set; }
    }
}
