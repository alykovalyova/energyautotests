﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class OfferAcknowledgmentOption
    {
        public OfferAcknowledgmentOption()
        {
            PlannedEvents = new HashSet<PlannedEvent>();
        }

        public int Id { get; set; }
        public int ActionId { get; set; }
        public bool Value { get; set; }

        public virtual ICollection<PlannedEvent> PlannedEvents { get; set; }
    }
}
