﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class PlannedEvent
    {
        public PlannedEvent()
        {
            ExecutedEvents = new HashSet<ExecutedEvent>();
        }

        public int Id { get; set; }
        public DateTime? EventDateTime { get; set; }
        public int? ContractEndDateOffsetInDays { get; set; }
        public int? EmailTemplateId { get; set; }
        public int CampaignId { get; set; }
        public int EventTypeId { get; set; }
        public int? PropositionBundleId { get; set; }
        public double? Probability { get; set; }
        public string BnaId { get; set; }
        public int? OfferBundleId { get; set; }
        public int? OfferAcknowledgmentOptionId { get; set; }

        public virtual Campaign Campaign { get; set; }
        public virtual EmailTemplate EmailTemplate { get; set; }
        public virtual DictionaryEventType EventType { get; set; }
        public virtual OfferAcknowledgmentOption OfferAcknowledgmentOption { get; set; }
        public virtual ICollection<ExecutedEvent> ExecutedEvents { get; set; }
    }
}
