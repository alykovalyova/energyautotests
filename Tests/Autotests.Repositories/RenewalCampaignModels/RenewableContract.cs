﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class RenewableContract
    {
        public RenewableContract()
        {
            BbaUsageChanges = new HashSet<BbaUsageChange>();
            CampaignContracts = new HashSet<CampaignContract>();
            CustomOffers = new HashSet<CustomOffer>();
        }

        public int Id { get; set; }
        public long ContractId { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int? InitialSalesChannelId { get; set; }
        public Guid ContractPropositionId { get; set; }
        public int? PreviousSupplierId { get; set; }
        public bool IsRenewal { get; set; }
        public int InitialSwitchMessageId { get; set; }
        public int ContractDurationInMonths { get; set; }
        public bool? HasElectricity { get; set; }
        public bool? HasGas { get; set; }
        public DateTime? ExpectedCancellationDate { get; set; }
        public DateTime? ExpectedCerCancellationDate { get; set; }
        public int CustomerTypeId { get; set; }
        public int GenderId { get; set; }
        public DateTime? CustomerDateOfBirth { get; set; }
        public bool HasCustomerUnsubscribedForEmails { get; set; }
        public bool HasCustomerBadPaymentHistory { get; set; }
        public int LabelCode { get; set; }
        public int? SalesChannelId { get; set; }
        public bool IsActive { get; set; }
        public DateTime SignUpDate { get; set; }
        public int? HouseNumber { get; set; }
        public string HouseNumberExt { get; set; }
        public long ProductCustomerId { get; set; }
        public string CustomerMobilePhoneNumber { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerSurname { get; set; }
        public string ZipCode { get; set; }
        public int? CampaignContractStatusId { get; set; }
        public int? MargeTypeNid { get; set; }
        public int? MargeTypeRid { get; set; }
        public string StreetName { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsUnsubscribed { get; set; }
        public bool IsEligibleForBna { get; set; }
        public DateTime? LastCheckUsage { get; set; }
        public int CustomerProductTypeId { get; set; }
        public int? NutsHomeCustomerNumber { get; set; }
        public int RenewableContractType { get; set; }
        public bool? HasFiber { get; set; }
        public bool? HasTv { get; set; }
        public bool? HasVoip { get; set; }
        public long? ServiceAccountId { get; set; }
        public int? OfferBundleId { get; set; }
        public DateTime? LastUpdateUsage { get; set; }

        public virtual DictionaryCampaignContractStatus CampaignContractStatus { get; set; }
        public virtual DictionaryCustomerProductType CustomerProductType { get; set; }
        public virtual DictionaryRelationCategory CustomerType { get; set; }
        public virtual DictionaryGender Gender { get; set; }
        public virtual SalesChannel InitialSalesChannel { get; set; }
        public virtual DictionarySwitchMethod InitialSwitchMessage { get; set; }
        public virtual DictionaryLabel LabelCodeNavigation { get; set; }
        public virtual DictionaryMargeTypeN MargeTypeN { get; set; }
        public virtual DictionaryMargeTypeR MargeTypeR { get; set; }
        public virtual BalanceSupplier PreviousSupplier { get; set; }
        public virtual DictionaryRenewableContractType RenewableContractTypeNavigation { get; set; }
        public virtual SalesChannel SalesChannel { get; set; }
        public virtual ICollection<BbaUsageChange> BbaUsageChanges { get; set; }
        public virtual ICollection<CampaignContract> CampaignContracts { get; set; }
        public virtual ICollection<CustomOffer> CustomOffers { get; set; }
    }
}
