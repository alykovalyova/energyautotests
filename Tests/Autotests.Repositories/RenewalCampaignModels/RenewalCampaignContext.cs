﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class RenewalCampaignContext : DbContext
    {
        private readonly string _connectionString;

        public RenewalCampaignContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public RenewalCampaignContext(DbContextOptions<RenewalCampaignContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BalanceSupplier> BalanceSuppliers { get; set; }
        public virtual DbSet<BbaUsage> BbaUsages { get; set; }
        public virtual DbSet<BbaUsageChange> BbaUsageChanges { get; set; }
        public virtual DbSet<BnaSearchCriterion> BnaSearchCriteria { get; set; }
        public virtual DbSet<BnaSetting> BnaSettings { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<CampaignContract> CampaignContracts { get; set; }
        public virtual DbSet<CampaignContractStatusHistory> CampaignContractStatusHistories { get; set; }
        public virtual DbSet<CampaignStatusHistory> CampaignStatusHistories { get; set; }
        public virtual DbSet<ContractSelection> ContractSelections { get; set; }
        public virtual DbSet<CustomOffer> CustomOffers { get; set; }
        public virtual DbSet<CustomOfferProposition> CustomOfferPropositions { get; set; }
        public virtual DbSet<CustomOfferStatusHistory> CustomOfferStatusHistories { get; set; }
        public virtual DbSet<CustomerOfferAcknowledgmentAction> CustomerOfferAcknowledgmentActions { get; set; }
        public virtual DbSet<DictionaryCampaignContractStatus> DictionaryCampaignContractStatuses { get; set; }
        public virtual DbSet<DictionaryCampaignStatus> DictionaryCampaignStatuses { get; set; }
        public virtual DbSet<DictionaryCampaignType> DictionaryCampaignTypes { get; set; }
        public virtual DbSet<DictionaryCustomOfferStatus> DictionaryCustomOfferStatuses { get; set; }
        public virtual DbSet<DictionaryCustomerOfferAcknowledgmentAction> DictionaryCustomerOfferAcknowledgmentActions { get; set; }
        public virtual DbSet<DictionaryCustomerProductType> DictionaryCustomerProductTypes { get; set; }
        public virtual DbSet<DictionaryEmailSendStatus> DictionaryEmailSendStatuses { get; set; }
        public virtual DbSet<DictionaryEnergyProductTypeCode> DictionaryEnergyProductTypeCodes { get; set; }
        public virtual DbSet<DictionaryEventType> DictionaryEventTypes { get; set; }
        public virtual DbSet<DictionaryGender> DictionaryGenders { get; set; }
        public virtual DbSet<DictionaryIncentivePaymentType> DictionaryIncentivePaymentTypes { get; set; }
        public virtual DbSet<DictionaryLabel> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryMargeTypeN> DictionaryMargeTypeNs { get; set; }
        public virtual DbSet<DictionaryMargeTypeR> DictionaryMargeTypeRs { get; set; }
        public virtual DbSet<DictionaryOperator> DictionaryOperators { get; set; }
        public virtual DbSet<DictionaryRelationCategory> DictionaryRelationCategories { get; set; }
        public virtual DbSet<DictionaryRenewableContractType> DictionaryRenewableContractTypes { get; set; }
        public virtual DbSet<DictionarySwitchMethod> DictionarySwitchMethods { get; set; }
        public virtual DbSet<DictionaryUsageType> DictionaryUsageTypes { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<ExecutedContractEvent> ExecutedContractEvents { get; set; }
        public virtual DbSet<ExecutedEvent> ExecutedEvents { get; set; }
        public virtual DbSet<OfferAcknowledgmentOption> OfferAcknowledgmentOptions { get; set; }
        public virtual DbSet<PlannedEvent> PlannedEvents { get; set; }
        public virtual DbSet<RenewableContract> RenewableContracts { get; set; }
        public virtual DbSet<SalesChannel> SalesChannels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("Latin1_General_CI_AI");

            modelBuilder.Entity<BalanceSupplier>(entity =>
            {
                entity.ToTable("BalanceSupplier");

                entity.HasIndex(e => e.Ean, "IX_Ean")
                    .IsUnique();

                entity.Property(e => e.Ean).HasMaxLength(13);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<BbaUsage>(entity =>
            {
                entity.ToTable("BbaUsage");

                entity.HasIndex(e => e.BbaUsageChangeId, "IX_BbaUsage_BbaUsageChangeId");

                entity.Property(e => e.Ean)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.HasOne(d => d.BbaUsageChange)
                    .WithMany(p => p.BbaUsages)
                    .HasForeignKey(d => d.BbaUsageChangeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.UsageType)
                    .WithMany(p => p.BbaUsages)
                    .HasForeignKey(d => d.UsageTypeId)
                    .HasConstraintName("FK_BbaUsage_Dictionary_UsageType_Id");
            });

            modelBuilder.Entity<BbaUsageChange>(entity =>
            {
                entity.ToTable("BbaUsageChange");

                entity.HasIndex(e => e.RenewableContractId, "IX_BbaUsageChange_RenewableContractId");

                entity.HasOne(d => d.RenewableContract)
                    .WithMany(p => p.BbaUsageChanges)
                    .HasForeignKey(d => d.RenewableContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<BnaSearchCriterion>(entity =>
            {
                entity.HasIndex(e => e.BnaSettingId, "IX_BnaSearchCriteria_BnaSettingId");

                entity.HasOne(d => d.BnaSetting)
                    .WithMany(p => p.BnaSearchCriteria)
                    .HasForeignKey(d => d.BnaSettingId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<BnaSetting>(entity =>
            {
                entity.ToTable("BnaSetting");
            });

            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.ToTable("Campaign");

                entity.Property(e => e.CampaignName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerProductTypeId).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CampaignStatus)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => d.CampaignStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CampaignType)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => d.CampaignTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CustomerProductType)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => d.CustomerProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Campaign_Dictionary_CustomerProductType_Id");

                entity.HasOne(d => d.CustomerType)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => d.CustomerTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Campaign_Dictionary_RelationCategory_RelationTypeId");

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CampaignContract>(entity =>
            {
                entity.ToTable("CampaignContract");

                entity.HasIndex(e => e.CampaignId, "IX_CampaignContract_CampaignId");

                entity.HasIndex(e => e.RenewableContractId, "IX_CampaignContract_RenewableContractId");

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.CampaignContractStatus)
                    .WithMany(p => p.CampaignContracts)
                    .HasForeignKey(d => d.CampaignContractStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.CampaignContracts)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.RenewableContract)
                    .WithMany(p => p.CampaignContracts)
                    .HasForeignKey(d => d.RenewableContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CampaignContractStatusHistory>(entity =>
            {
                entity.ToTable("CampaignContractStatusHistory");

                entity.HasIndex(e => e.CampaignContractId, "IX_CampaignContractStatusHistory_CampaignContractId");

                entity.HasOne(d => d.CampaignContract)
                    .WithMany(p => p.CampaignContractStatusHistories)
                    .HasForeignKey(d => d.CampaignContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CampaignContractStatus)
                    .WithMany(p => p.CampaignContractStatusHistories)
                    .HasForeignKey(d => d.CampaignContractStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CampaignStatusHistory>(entity =>
            {
                entity.ToTable("CampaignStatusHistory");

                entity.HasIndex(e => e.CampaignId, "IX_CampaignStatusHistory_CampaignId");

                entity.Property(e => e.ResponsibleUser).HasMaxLength(255);

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.CampaignStatusHistories)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CampaignStatus)
                    .WithMany(p => p.CampaignStatusHistories)
                    .HasForeignKey(d => d.CampaignStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ContractSelection>(entity =>
            {
                entity.ToTable("ContractSelection");

                entity.HasIndex(e => e.CampaignId, "IX_ContractSelection_CampaignId");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.ContractSelections)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Operator)
                    .WithMany(p => p.ContractSelections)
                    .HasForeignKey(d => d.OperatorId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CustomOffer>(entity =>
            {
                entity.ToTable("CustomOffer");

                entity.HasIndex(e => e.BbaUsageChangeId, "IX_CustomOffer_BbaUsageChangeId");

                entity.HasIndex(e => e.EmailSendStatusId, "IX_CustomOffer_EmailSendStatusId");

                entity.HasIndex(e => e.RenewableContractId, "IX_CustomOffer_RenewableContractId");

                entity.Property(e => e.EmailSendStatusId).HasDefaultValueSql("((1))");

                entity.Property(e => e.ExpiredOn).HasColumnType("date");

                entity.Property(e => e.ExternalReference).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.BbaUsageChange)
                    .WithMany(p => p.CustomOffers)
                    .HasForeignKey(d => d.BbaUsageChangeId);

                entity.HasOne(d => d.CustomOfferStatus)
                    .WithMany(p => p.CustomOffers)
                    .HasForeignKey(d => d.CustomOfferStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EmailSendStatus)
                    .WithMany(p => p.CustomOffers)
                    .HasForeignKey(d => d.EmailSendStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.RenewableContract)
                    .WithMany(p => p.CustomOffers)
                    .HasForeignKey(d => d.RenewableContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CustomOfferProposition>(entity =>
            {
                entity.ToTable("CustomOfferProposition");

                entity.HasIndex(e => e.CustomOfferId, "IX_CustomOfferProposition_CustomOfferId");

                entity.HasOne(d => d.CustomOffer)
                    .WithMany(p => p.CustomOfferPropositions)
                    .HasForeignKey(d => d.CustomOfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CustomOfferStatusHistory>(entity =>
            {
                entity.ToTable("CustomOfferStatusHistory");

                entity.HasIndex(e => e.CustomOfferId, "IX_CustomOfferStatusHistory_CustomOfferId");

                entity.HasOne(d => d.CustomOffer)
                    .WithMany(p => p.CustomOfferStatusHistories)
                    .HasForeignKey(d => d.CustomOfferId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<CustomerOfferAcknowledgmentAction>(entity =>
            {
                entity.ToTable("CustomerOfferAcknowledgmentAction");

                entity.HasIndex(e => e.CampaignContractId, "IX_CustomerOfferAcknowledgmentAction_CampaignContractId");

                entity.HasOne(d => d.CampaignContract)
                    .WithMany(p => p.CustomerOfferAcknowledgmentActions)
                    .HasForeignKey(d => d.CampaignContractId);
            });

            modelBuilder.Entity<DictionaryCampaignContractStatus>(entity =>
            {
                entity.ToTable("Dictionary_CampaignContractStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryCampaignStatus>(entity =>
            {
                entity.ToTable("Dictionary_CampaignStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryCampaignType>(entity =>
            {
                entity.ToTable("Dictionary_CampaignType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryCustomOfferStatus>(entity =>
            {
                entity.ToTable("Dictionary_CustomOfferStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryCustomerOfferAcknowledgmentAction>(entity =>
            {
                entity.ToTable("Dictionary_CustomerOfferAcknowledgmentAction");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryCustomerProductType>(entity =>
            {
                entity.ToTable("Dictionary_CustomerProductType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEmailSendStatus>(entity =>
            {
                entity.ToTable("Dictionary_EmailSendStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEnergyProductTypeCode>(entity =>
            {
                entity.ToTable("Dictionary_EnergyProductTypeCode");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryEventType>(entity =>
            {
                entity.ToTable("Dictionary_EventType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryGender>(entity =>
            {
                entity.ToTable("Dictionary_Gender");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryIncentivePaymentType>(entity =>
            {
                entity.ToTable("Dictionary_IncentivePaymentType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryLabel>(entity =>
            {
                entity.ToTable("Dictionary_Labels");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeN>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeN");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryMargeTypeR>(entity =>
            {
                entity.ToTable("Dictionary_MargeTypeR");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryOperator>(entity =>
            {
                entity.ToTable("Dictionary_Operator");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryRelationCategory>(entity =>
            {
                entity.ToTable("Dictionary_RelationCategory");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryRenewableContractType>(entity =>
            {
                entity.ToTable("Dictionary_RenewableContractType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionarySwitchMethod>(entity =>
            {
                entity.ToTable("Dictionary_SwitchMethod");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryUsageType>(entity =>
            {
                entity.ToTable("Dictionary_UsageType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.ToTable("EmailTemplate");

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name).IsRequired();

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.EmailTemplates)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ExecutedContractEvent>(entity =>
            {
                entity.ToTable("ExecutedContractEvent");

                entity.HasIndex(e => e.ExecutedContractEventCode, "IX_ExecutedContractEvent_ExecutedContractEventCode");

                entity.HasIndex(e => e.ExecutedEventId, "IX_ExecutedContractEvent_ExecutedEventId");

                entity.HasOne(d => d.CampaignContract)
                    .WithMany(p => p.ExecutedContractEvents)
                    .HasForeignKey(d => d.CampaignContractId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.ExecutedEvent)
                    .WithMany(p => p.ExecutedContractEvents)
                    .HasForeignKey(d => d.ExecutedEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ExecutedEvent>(entity =>
            {
                entity.ToTable("ExecutedEvent");

                entity.HasIndex(e => e.PlannedEventId, "IX_ExecutedEvent_PlannedEventId");

                entity.HasOne(d => d.PlannedEvent)
                    .WithMany(p => p.ExecutedEvents)
                    .HasForeignKey(d => d.PlannedEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<OfferAcknowledgmentOption>(entity =>
            {
                entity.ToTable("OfferAcknowledgmentOption");
            });

            modelBuilder.Entity<PlannedEvent>(entity =>
            {
                entity.ToTable("PlannedEvent");

                entity.HasIndex(e => e.CampaignId, "IX_PlannedEvent_CampaignId");

                entity.HasIndex(e => e.EmailTemplateId, "IX_PlannedEvent_EmailTemplateId");

                entity.HasIndex(e => e.OfferAcknowledgmentOptionId, "IX_PlannedEvent_OfferAcknowledgmentOptionId");

                entity.Property(e => e.BnaId).HasMaxLength(50);

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.PlannedEvents)
                    .HasForeignKey(d => d.CampaignId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.EmailTemplate)
                    .WithMany(p => p.PlannedEvents)
                    .HasForeignKey(d => d.EmailTemplateId);

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.PlannedEvents)
                    .HasForeignKey(d => d.EventTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlannedEvent_Dictionary_EventType_EventTypeIdId");

                entity.HasOne(d => d.OfferAcknowledgmentOption)
                    .WithMany(p => p.PlannedEvents)
                    .HasForeignKey(d => d.OfferAcknowledgmentOptionId);
            });

            modelBuilder.Entity<RenewableContract>(entity =>
            {
                entity.ToTable("RenewableContract");

                entity.HasIndex(e => e.CampaignContractStatusId, "IX_RenewableContract_CampaignContractStatusId");

                entity.HasIndex(e => new { e.ContractId, e.CustomerProductTypeId }, "IX_RenewableContract_ContractId_CustomerProductTypeId");

                entity.HasIndex(e => e.InitialSalesChannelId, "IX_RenewableContract_InitialSalesChannelId");

                entity.HasIndex(e => e.NutsHomeCustomerNumber, "IX_RenewableContract_NutsHomeCustomerNumber");

                entity.HasIndex(e => e.PreviousSupplierId, "IX_RenewableContract_PreviousSupplierId");

                entity.HasIndex(e => e.ProductCustomerId, "IX_RenewableContract_ProductCustomerId");

                entity.HasIndex(e => e.SalesChannelId, "IX_RenewableContract_SalesChannelId");

                entity.HasIndex(e => e.ZipCode, "IX_RenewableContract_ZipCode");

                entity.Property(e => e.ContractEndDate).HasColumnType("date");

                entity.Property(e => e.ContractStartDate).HasColumnType("date");

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerDateOfBirth).HasColumnType("date");

                entity.Property(e => e.CustomerProductTypeId).HasDefaultValueSql("((1))");

                entity.Property(e => e.LastCheckUsage).HasColumnType("date");

                entity.Property(e => e.MargeTypeNid).HasColumnName("MargeTypeNId");

                entity.Property(e => e.MargeTypeRid).HasColumnName("MargeTypeRId");

                entity.Property(e => e.RenewableContractType).HasDefaultValueSql("((1))");

                entity.Property(e => e.SignUpDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("('2018-10-03T11:31:53.0160224+02:00')");

                entity.HasOne(d => d.CampaignContractStatus)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.CampaignContractStatusId);

                entity.HasOne(d => d.CustomerProductType)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.CustomerProductTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RenewableContract_Dictionary_CustomerProductType_Id");

                entity.HasOne(d => d.CustomerType)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.CustomerTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RenewableContract_Dictionary_RelationCategory_RelationCategoryId");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.GenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RenewableContract_Dictionary_Gender_RelationGenderId");

                entity.HasOne(d => d.InitialSalesChannel)
                    .WithMany(p => p.RenewableContractInitialSalesChannels)
                    .HasForeignKey(d => d.InitialSalesChannelId);

                entity.HasOne(d => d.InitialSwitchMessage)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.InitialSwitchMessageId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.LabelCodeNavigation)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.LabelCode)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.MargeTypeN)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.MargeTypeNid)
                    .HasConstraintName("FK_RenewableContract_Dictionary_MargeTypeN_Id");

                entity.HasOne(d => d.MargeTypeR)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.MargeTypeRid)
                    .HasConstraintName("FK_RenewableContract_Dictionary_MargeTypeR_Id");

                entity.HasOne(d => d.PreviousSupplier)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.PreviousSupplierId);

                entity.HasOne(d => d.RenewableContractTypeNavigation)
                    .WithMany(p => p.RenewableContracts)
                    .HasForeignKey(d => d.RenewableContractType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RenewableContract_Dictionary_RenewableContractType_Id");

                entity.HasOne(d => d.SalesChannel)
                    .WithMany(p => p.RenewableContractSalesChannels)
                    .HasForeignKey(d => d.SalesChannelId);
            });

            modelBuilder.Entity<SalesChannel>(entity =>
            {
                entity.ToTable("SalesChannel");

                entity.Property(e => e.SalesChannelId).ValueGeneratedNever();

                entity.Property(e => e.ChannelName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PartnerName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
