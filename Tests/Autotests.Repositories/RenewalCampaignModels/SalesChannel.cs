﻿using System;
using System.Collections.Generic;

namespace Autotests.Repositories.RenewalCampaignModels
{
    public partial class SalesChannel
    {
        public SalesChannel()
        {
            RenewableContractInitialSalesChannels = new HashSet<RenewableContract>();
            RenewableContractSalesChannels = new HashSet<RenewableContract>();
        }

        public int SalesChannelId { get; set; }
        public string ChannelName { get; set; }
        public string PartnerName { get; set; }

        public virtual ICollection<RenewableContract> RenewableContractInitialSalesChannels { get; set; }
        public virtual ICollection<RenewableContract> RenewableContractSalesChannels { get; set; }
    }
}
