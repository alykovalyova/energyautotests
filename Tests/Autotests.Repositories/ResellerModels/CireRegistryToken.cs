﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ResellerModels
{
    public partial class CireRegistryToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public string TokenType { get; set; }
        public DateTime AcquiredOn { get; set; }
        public DateTime ExpiresOn { get; set; }
    }
}
