﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ResellerModels
{
    public partial class OfferBundle
    {
        public int Id { get; set; }
        public int SalesChannelId { get; set; }
        public Guid OfferBundleId { get; set; }
        public string Name { get; set; }
        public int? NewOfferBundleId { get; set; }

        public virtual SalesChannel SalesChannel { get; set; }
    }
}
