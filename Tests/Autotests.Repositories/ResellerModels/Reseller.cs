﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ResellerModels
{
    public partial class Reseller
    {
        public Reseller()
        {
            SalesChannel = new HashSet<SalesChannel>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CireId { get; set; }
        public short? CireStatusId { get; set; }

        public virtual CireStatus CireStatus { get; set; }
        public virtual ICollection<SalesChannel> SalesChannel { get; set; }
    }
}
