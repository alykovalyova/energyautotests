﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ResellerModels
{
    public partial class ResellerContext : DbContext
    {
        private readonly string _connectionString;

        public ResellerContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ResellerContext(DbContextOptions<ResellerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CireRegistryToken> CireRegistryToken { get; set; }
        public virtual DbSet<CireStatus> CireStatus { get; set; }
        public virtual DbSet<OfferBundle> OfferBundle { get; set; }
        public virtual DbSet<Reseller> Reseller { get; set; }
        public virtual DbSet<SalesChannel> SalesChannel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CireRegistryToken>(entity =>
            {
                entity.ToTable("CireRegistryToken", "dbo");

                entity.Property(e => e.Token).IsRequired();

                entity.Property(e => e.TokenType).IsRequired();
            });

            modelBuilder.Entity<CireStatus>(entity =>
            {
                entity.ToTable("CireStatus", "dbo");

                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<OfferBundle>(entity =>
            {
                entity.ToTable("OfferBundle", "dbo");

                entity.HasIndex(e => e.Name);

                entity.HasIndex(e => e.SalesChannelId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.SalesChannel)
                    .WithMany(p => p.OfferBundle)
                    .HasForeignKey(d => d.SalesChannelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Reseller>(entity =>
            {
                entity.ToTable("Reseller", "dbo");

                entity.HasIndex(e => e.CireStatusId);

                entity.HasIndex(e => new { e.IsActive, e.Name, e.Id });

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CireStatus)
                    .WithMany(p => p.Reseller)
                    .HasForeignKey(d => d.CireStatusId);
            });

            modelBuilder.Entity<SalesChannel>(entity =>
            {
                entity.ToTable("SalesChannel", "dbo");

                entity.HasIndex(e => e.ResellerId);

                entity.Property(e => e.SalesChannelId).ValueGeneratedNever();

                entity.Property(e => e.ChannelName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.ResellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
