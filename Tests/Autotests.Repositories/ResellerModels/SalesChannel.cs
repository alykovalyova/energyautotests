﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.ResellerModels
{
    public partial class SalesChannel
    {
        public SalesChannel()
        {
            OfferBundle = new HashSet<OfferBundle>();
        }

        public int SalesChannelId { get; set; }
        public string ChannelName { get; set; }
        public string Label { get; set; }
        public Guid ResellerId { get; set; }

        public virtual Reseller Reseller { get; set; }
        public virtual ICollection<OfferBundle> OfferBundle { get; set; }
    }
}
