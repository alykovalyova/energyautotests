﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.SalesChannelsModels
{
    public partial class Partner
    {
        public Partner()
        {
            PartnerEmail = new HashSet<PartnerEmail>();
            SalesChannel = new HashSet<SalesChannel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PartnerEmail> PartnerEmail { get; set; }
        public virtual ICollection<SalesChannel> SalesChannel { get; set; }
    }
}
