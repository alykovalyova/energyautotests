﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.SalesChannelsModels
{
    public partial class PartnerEmail
    {
        public int Id { get; set; }
        public int PartnerId { get; set; }
        public string Address { get; set; }

        public virtual Partner Partner { get; set; }
    }
}
