﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.SalesChannelsModels
{
    public partial class SalesChannelEmail
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public int SalesChannelId { get; set; }
        public int RecipientTypeId { get; set; }

        public virtual DictionaryRecipientType RecipientType { get; set; }
        public virtual SalesChannel SalesChannel { get; set; }
    }
}
