﻿using Microsoft.EntityFrameworkCore;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Autotests.Repositories.SalesChannelsModels
{
    public partial class SalesChannelsContext : DbContext
    {
        private readonly string _connectionString;

        public SalesChannelsContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SalesChannelsContext(DbContextOptions<SalesChannelsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Campaign> Campaign { get; set; }
        public virtual DbSet<Channel> Channel { get; set; }
        public virtual DbSet<CoolDownPeriod> CoolDownPeriod { get; set; }
        public virtual DbSet<DictionaryLabels> DictionaryLabels { get; set; }
        public virtual DbSet<DictionaryRecipientType> DictionaryRecipientType { get; set; }
        public virtual DbSet<DictionarySalesChannelStatus> DictionarySalesChannelStatus { get; set; }
        public virtual DbSet<Partner> Partner { get; set; }
        public virtual DbSet<PartnerEmail> PartnerEmail { get; set; }
        public virtual DbSet<PredefinedEmail> PredefinedEmail { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<SalesChannel> SalesChannel { get; set; }
        public virtual DbSet<SalesChannelEmail> SalesChannelEmail { get; set; }
        public virtual DbSet<Store> Store { get; set; }
        public virtual DbSet<SwitchWindow> SwitchWindow { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campaign>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Channel>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DictionaryLabels>(entity =>
            {
                entity.ToTable("Dictionary_Labels");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionaryRecipientType>(entity =>
            {
                entity.ToTable("Dictionary_RecipientType");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DictionarySalesChannelStatus>(entity =>
            {
                entity.ToTable("Dictionary_SalesChannelStatus");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Partner>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PartnerEmail>(entity =>
            {
                entity.HasIndex(e => e.PartnerId);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.PartnerEmail)
                    .HasForeignKey(d => d.PartnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PredefinedEmail>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SalesChannel>(entity =>
            {
                entity.HasIndex(e => e.CampaignId);

                entity.HasIndex(e => e.ChannelId);

                entity.HasIndex(e => e.CoolDownPeriodId);

                entity.HasIndex(e => e.LabelId);

                entity.HasIndex(e => e.PartnerId);

                entity.HasIndex(e => e.RegionId);

                entity.HasIndex(e => e.StatusId);

                entity.HasIndex(e => e.StoreId);

                entity.HasIndex(e => e.SwitchWindowId);

                entity.Property(e => e.OldReferenceId).HasMaxLength(50);

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.CampaignId);

                entity.HasOne(d => d.Channel)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.ChannelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CoolDownPeriod)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.CoolDownPeriodId);

                entity.HasOne(d => d.Label)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.LabelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Partner)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.PartnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.RegionId);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.StoreId);

                entity.HasOne(d => d.SwitchWindow)
                    .WithMany(p => p.SalesChannel)
                    .HasForeignKey(d => d.SwitchWindowId);
            });

            modelBuilder.Entity<SalesChannelEmail>(entity =>
            {
                entity.HasIndex(e => e.SalesChannelId);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.RecipientType)
                    .WithMany(p => p.SalesChannelEmail)
                    .HasForeignKey(d => d.RecipientTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.SalesChannel)
                    .WithMany(p => p.SalesChannelEmail)
                    .HasForeignKey(d => d.SalesChannelId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
