﻿using Autotests.Clients;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Repositories.ProdManModels;
using Autotests.Repositories.ResellerModels;
using NUnit.Framework;
using Nuts.Reseller.Contract;
using Autotests.Repositories.OfferBundleModels;
using OB = Autotests.Repositories.ResellerModels.OfferBundle;
using R = Autotests.Repositories.ResellerModels.Reseller;
using SC = Autotests.Repositories.ResellerModels.SalesChannel;
using Autotests.Repositories.SalesChannelsModels;
using Autotests.Core;
using Allure.Commons;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Builders.SalesChannel;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Reseller.Builders;
using Autotests.Reseller.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract.AddSalesChannel;
using Autotests.Helper.Enums.SalesChannel;

namespace Autotests.Reseller.Base
{
    [SetUpFixture, Category(Categories.Reseller)]
    internal abstract class BaseReseller : AllureReport
    {
        protected NutsHttpClient SalesChannelClient;
        protected NutsHttpClient ResellerClient;
        protected NutsHttpClient OfferBundleClient;
        protected static BuildDirector BuildDirector;

        protected OfferBundleProvider OfferBundleProvider;

        protected DbHandler<ResellerContext> ResellerRepository;
        protected DbHandler<ProdManContext> ProdManDb;
        protected DbHandler<OfferBundleContext> OfferBundleDb;
        protected DbHandler<SalesChannelsContext> SalesChannelDb;

        protected const string ResellerName = "Autotest_Reseller_Name";
        protected const string SalesChannelName = "Autotest_Channel_Name";
        protected const string OfferBundleName = "Autotest_Bundle_Name";
        protected const string Label = "Autotest_Label";

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            SalesChannelClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.SalesChannel));
            ResellerClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.Reseller));
            OfferBundleClient = new NutsHttpClient(new BaseHttpClient(ConfigHandler.Instance.ApiUrls.OfferBundle));
            BuildDirector = new BuildDirector();
            OfferBundleProvider = new OfferBundleProvider();
            ResellerRepository = new DbHandler<ResellerContext>(ConfigHandler.Instance.GetConnectionString(DbConnectionName.ResellerDatabase.ToString()));
            ProdManDb = new DbHandler<ProdManContext>(ConfigHandler.Instance.GetConnectionString(DbConnectionName.ProdManDatabase.ToString()));
            OfferBundleDb = new DbHandler<OfferBundleContext>(ConfigHandler.Instance.GetConnectionString(DbConnectionName.OfferBundleDatabase.ToString()));
            SalesChannelDb = new DbHandler<SalesChannelsContext>(ConfigHandler.Instance.GetConnectionString(DbConnectionName.SalesChannelsDatabase.ToString()));
        }

        [SetUp]
        public virtual void SetUp()
        {
            var resellers =
                ResellerRepository.GetEntitiesByCondition<R>(r =>
                    r.Name.Contains(ResellerName));
            foreach (var reseller in resellers)
            {
                var salesChannels = ResellerRepository.GetEntitiesByCondition<SC>(sc => sc.ResellerId == reseller.Id);
                foreach (var salesChannel in salesChannels)
                {
                    ResellerRepository.DeleteEntitiesByCondition<OB>(ob => ob.SalesChannelId == salesChannel.SalesChannelId);
                }
                ResellerRepository.DeleteEntities(salesChannels);
            }
            ResellerRepository.DeleteEntities(resellers);
        }

        protected static UpsertResellerRequest CreateUpsertResellerRequest(
            string resellerName, bool? isActive, Nuts.Reseller.Contract.SalesChannel salesChannel)
        {
            return new UpsertResellerRequest
            {
                Reseller = BuildDirector.Get<ResellerBuilder>()
                    .SetName(resellerName)
                    .SetIsActive(isActive ?? false).SetSalesChannels(salesChannel).Build()
            };
        }

        protected static SearchResellersRequest CreateSearchResellersRequest(
            bool? isActive = null, string bundleName = null, string resellerId = null, string resellerName = null)
        {
            return new SearchResellersRequest
            {
                SearchCriteria = BuildDirector.Get<SearchResellersCriteriaBuilder>()
                    .SetIsActive(isActive).SetOfferBundleName(bundleName)
                    .SetResellerId(resellerId).SetResellerName(resellerName).Build()
            };
        }

        protected AddSalesChannelRequest GenerateAddSalesChannelRequest(Channels channel = Channels.Retail,
            bool valid = true)
        {
            const int id = 1;
            var request = new AddSalesChannelRequest
            {
                SalesChannel = BuildDirector.Get<AddSalesChannelDetailsBuilder>()
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(TestConstants.salesChannel_ExternalReference).SetCampaign(id)
                    .SetChannel((int)channel, channel.ToString()).SetPartner(id)
                    .SetCoolDownPeriod(1, 14).SetSwitchWindow(id, 244).SetLabel(Labels.BudgetEnergie)
                    .InCase(valid && channel.Equals(Channels.Retail), b => b.SetRegion(id).SetStore(id))
                    .InCase(!valid && !channel.Equals(Channels.Retail), b => b.SetRegion(id).SetStore(id))
                    .Build(),
                UserName = TestConstants.SalesChannel_UserName
            };
            return request;
        }

        protected string GetNewOfferBundleId() => Guid.NewGuid().ToString();
    }
}