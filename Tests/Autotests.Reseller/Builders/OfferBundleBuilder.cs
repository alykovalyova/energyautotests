﻿using Autotests.Core.Helpers;
using Contract_OfferBundle = Nuts.Reseller.Contract.OfferBundle;

namespace Autotests.Reseller.Builders
{
    internal class OfferBundleBuilder : MainBuilder<Contract_OfferBundle>
    {
        internal OfferBundleBuilder SetOfferBundleId(string id)
        {
            Instance.OfferBundleId = id;
            return this;
        }

        internal OfferBundleBuilder SetName(string name)
        {
            Instance.Name = name;
            return this;
        }
    }
}