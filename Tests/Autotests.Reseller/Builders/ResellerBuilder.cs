﻿using Autotests.Core.Helpers;

namespace Autotests.Reseller.Builders
{
    internal class ResellerBuilder : MainBuilder<Nuts.Reseller.Contract.Reseller>
    {
        internal ResellerBuilder SetName(string name)
        {
            Instance.Name = name;
            return this;
        }

        internal ResellerBuilder SetIsActive(bool isActive)
        {
            Instance.IsActive = isActive;
            return this;
        }

        internal ResellerBuilder SetSalesChannels(params Nuts.Reseller.Contract.SalesChannel[] salesChannels)
        {
            Instance.SalesChannels = salesChannels.ToList();
            return this;
        }
    }
}