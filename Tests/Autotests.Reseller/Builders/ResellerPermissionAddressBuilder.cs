﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using Autotests.Core.Helpers;

namespace Autotests.Reseller.Builders
{
    internal class ResellerPermissionAddressBuilder : MainBuilder<Address>
    {
        internal ResellerPermissionAddressBuilder SetDefaults()
        {
            Instance.City = "Kharkiv";
            Instance.HouseNumber = 71;
            Instance.HouseNumberSuffix = "B";
            Instance.Street = "Peremoga";
            return this;
        }

        internal ResellerPermissionAddressBuilder SetCity(string city)
        {
            Instance.City = city;
            return this;
        }

        internal ResellerPermissionAddressBuilder SetHouseNumber(int hsNr)
        {
            Instance.HouseNumber = hsNr;
            return this;
        }

        internal ResellerPermissionAddressBuilder SetHouseNumberSuffix(string exHsNr)
        {
            Instance.HouseNumberSuffix = exHsNr;
            return this;
        }

        internal ResellerPermissionAddressBuilder SetPostalCode(string postalCode)
        {
            Instance.PostalCode = postalCode;
            return this;
        }

        internal ResellerPermissionAddressBuilder SetStreet(string street)
        {
            Instance.Street = street;
            return this;
        }
    }
}
