﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using Autotests.Core.Helpers;

namespace Autotests.Reseller.Builders
{
    internal class ResellerPermissionCustomerBuilder : MainBuilder<Customer>
    {
        internal ResellerPermissionCustomerBuilder SetDefaults()
        {
            Instance.Initials = "J.J";
            Instance.LastName = "Wick";
            return this;
        }

        internal ResellerPermissionCustomerBuilder SetGender(string gender)
        {
            Instance.Gender = gender;
            return this;
        }

        internal ResellerPermissionCustomerBuilder SetInitials(string initials)
        {
            Instance.Initials = initials;
            return this;
        }

        internal ResellerPermissionCustomerBuilder SetLastName(string lastName)
        {
            Instance.LastName = lastName;
            return this;
        }
    }
}
