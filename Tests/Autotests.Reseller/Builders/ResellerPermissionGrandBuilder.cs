﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using Autotests.Core.Helpers;

namespace Autotests.Reseller.Builders
{
    internal class ResellerPermissionGrandBuilder : MainBuilder<PermissionGrand>
    {
        internal ResellerPermissionGrandBuilder SetPermissionInfo(PermissionInfo permissionInfo)
        {
            Instance.PermissionInfo = permissionInfo;
            return this;
        }

        internal ResellerPermissionGrandBuilder SetBirthDayKey(string birthDayKey)
        {
            Instance.BirthDayKey = birthDayKey;
            return this;
        }

        internal ResellerPermissionGrandBuilder SetIbanKey(string ibanKey)
        {
            Instance.IBANKey = ibanKey;
            return this;
        }
    }
}
