﻿using Autotests.Clients.Rest.Models.ResellerApi.GetMpOfferPhase;
using Autotests.Core.Helpers;

namespace Autotests.Reseller.Builders
{
    internal class ResellerPermissionInfoBuilder : MainBuilder<PermissionInfo>
    {
        internal ResellerPermissionInfoBuilder SetPermissionAddress(Address address)
        {
            Instance.Address = address;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetPermissionCustomer(Customer customer)
        {
            Instance.Customer = customer;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetHasOptedInByCheckbox(bool hasOptedInByCheckbox)
        {
            Instance.HasOptedInByCheckbox = hasOptedInByCheckbox;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetPermissionDateTime(DateTime permissionDateTime)
        {
            Instance.PermissionDateTime = permissionDateTime;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetPermissionPurpose(string permissionPurpose)
        {
            Instance.PermissionPurpose = permissionPurpose;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetPermissionType(string permissionType)
        {
            Instance.PermissionType = permissionType;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetRequestOrigin(string requestOrigin = "Origin")
        {
            Instance.RequestOrigin = requestOrigin;
            return this;
        }

        internal ResellerPermissionInfoBuilder SetResponsibleUser(string responsibleUser = "autotest_responsible_user")
        {
            Instance.ResponsibleUser = responsibleUser;
            return this;
        }
    }
}