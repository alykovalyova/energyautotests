﻿using Autotests.Core.Helpers;
using Contract_OfferBundle = Nuts.Reseller.Contract.OfferBundle;

namespace Autotests.Reseller.Builders
{
    internal class SalesChannelBuilder : MainBuilder<Nuts.Reseller.Contract.SalesChannel>
    {
        internal SalesChannelBuilder SetSalesChannelId(int id)
        {
            Instance.SalesChannelId = id;
            return this;
        }

        internal SalesChannelBuilder SetChannelName(string name)
        {
            Instance.ChannelName = name;
            return this;
        }

        internal SalesChannelBuilder SetLabel(string label)
        {
            Instance.Label = label;
            return this;
        }

        internal SalesChannelBuilder SetOfferBundles(params Contract_OfferBundle[] offerBundles)
        {
            Instance.OfferBundles = offerBundles.ToList();
            return this;
        }
    }
}