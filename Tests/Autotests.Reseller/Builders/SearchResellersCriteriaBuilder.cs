﻿using Autotests.Core.Helpers;
using Nuts.Reseller.Contract;

namespace Autotests.Reseller.Builders
{
    internal class SearchResellersCriteriaBuilder : MainBuilder<SearchResellersCriteria>
    {
        internal SearchResellersCriteriaBuilder SetIsActive(bool? isActive)
        {
            Instance.IsActive = isActive;
            return this;
        }

        internal SearchResellersCriteriaBuilder SetOfferBundleName(string name)
        {
            Instance.OfferBundleName = name;
            return this;
        }

        internal SearchResellersCriteriaBuilder SetResellerId(string resellerId)
        {
            Instance.ResellerId = resellerId;
            return this;
        }

        internal SearchResellersCriteriaBuilder SetResellerName(string name)
        {
            Instance.ResellerName = name;
            return this;
        }
    }
}