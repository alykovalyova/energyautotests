﻿using Autotests.Clients.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Builders.OfferBundle;
using Autotests.Repositories.OfferBundleModels;
using Nuts.InterDom.Models.Enums;
using Nuts.OfferBundle.Contract.Rest.Transport;
using Nuts.ProdMan.Model.Contract;
using OfferProposition = Nuts.OfferBundle.Contract.Rest.Model.CreateOfferBundle.OfferProposition;

namespace Autotests.Reseller.Helpers
{
    public class OfferBundleProvider
    {
        private readonly BuildDirector _buildDirector = new BuildDirector();
        private readonly ProdmanServiceClient _prodManService = new ProdmanServiceClient();
        private readonly DbHandler<OfferBundleContext> _offerBundleDb =
            new DbHandler<OfferBundleContext>(ConfigHandler.Instance.GetConnectionString(DbConnectionName.OfferBundleDatabase.ToString()));
        public readonly string TestMarker = "AutoT_OB";
        public DateTime MarketFrom = DateTime.Today;
        public DateTime MarketTo = DateTime.Today.AddDays(5);

        public string GetOfferName()
        {
            return $"{TestMarker}_{ RandomDataProvider.GetRandomNumbersString(5)}_{DateTime.Now.Millisecond}";
        }

        public OfferProposition CreateOfferProposition(Labels label, DateTime? marketFrom = null,
            DateTime? marketTo = null, int duration = 12, CashBack cashBack = null, bool isPropositionActive = false)
        {
            var proposition = _buildDirector.Get<OfferBundlePropositionBuilder>()
                .GetBaseProposition(marketFrom ?? MarketFrom, duration)
                .WithMarketTo(marketTo ?? MarketTo)
                .WithLabel(label)
                .WithCashBackTest(cashBack)
                .Build();

            return isPropositionActive
                ? _prodManService.CreateActiveProposition(proposition).To<OfferProposition>()
                : _prodManService.CreateProposition(proposition).To<OfferProposition>();
        }

        public CreateOfferBundleRequest CreateOffer(OfferProposition[] offerPropositions, Labels label,
            RelationCategory category, string name = null, string productType = "Energy", string propositionTypes = "NewCustomer")
        {
            return _buildDirector.Get<CreateOfferBundleRequestBuilder>().SetDefaults(name)
                .SetClientType(category.ToString())
                .SetLabel(label)
                .SetCustomerProductType(productType)
                .SetOfferPropositions(offerPropositions)
                .SetPropositionTypes(propositionTypes).Build();
        }

        public int GetCreatedOfferBundleId(string offerBundleName) =>
            _offerBundleDb.GetSingleEntityByCondition<Repositories.OfferBundleModels.OfferBundle>(ob => ob.Name == offerBundleName).Id;
        public Repositories.OfferBundleModels.OfferBundle GetCreatedOfferBundle(string offerBundleName) =>
            _offerBundleDb.GetSingleEntityByCondition<Repositories.OfferBundleModels.OfferBundle>(ob => ob.Name == offerBundleName);

    }
}
