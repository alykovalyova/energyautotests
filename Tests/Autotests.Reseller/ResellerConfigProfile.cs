﻿using AutoMapper;

namespace Autotests.Framework.MapProfiles
{
    public class ResellerConfigProfile : Profile
    {
        public ResellerConfigProfile()
        {
            CreateMap<Repositories.OfferBundleModels.OfferBundle, Nuts.Reseller.Contract.OfferBundle>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.OfferBundleId, opt => opt.MapFrom(src => src.Id));
        }
    }
}
