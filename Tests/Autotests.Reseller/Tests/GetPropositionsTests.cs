﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums;
using Autotests.Reseller.Base;
using Autotests.Reseller.Builders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.OfferBundle.Contract.Rest.Model.CreateOfferBundle;
using Nuts.Reseller.Contract;

namespace Autotests.Reseller.Tests
{
    [TestFixture]
    internal class GetPropositionsTests : BaseReseller
    {
        [Test]
        public void GetPropositions_DefaultValidTest_Returns200()
        {
            var label = Labels.BudgetEnergie;
            var productType = CustomerProductType.Energy;
            var clientType = RelationCategory.Private;
            var obName = OfferBundleProvider.GetOfferName();

            var offerBundleResponse = OfferBundleProvider.CreateOffer(
                 new OfferProposition[] { OfferBundleProvider.CreateOfferProposition(label) },
                 label, clientType, obName, productType.ToString()).CallWith(OfferBundleClient);
            var offerBundle = OfferBundleProvider.GetCreatedOfferBundle(obName).To<Nuts.Reseller.Contract.OfferBundle>();

            var salesChannelRequest = GenerateAddSalesChannelRequest();
            salesChannelRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == salesChannelRequest.SalesChannel.ExternalReference).Id;

            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(salesChannelId).SetChannelName(salesChannelRequest.SalesChannel.Channel.Name)
                .SetLabel(label.ToString()).SetOfferBundles(offerBundle).Build();
            
           var upsertResellerResponse = CreateUpsertResellerRequest(ResellerName, true, salesChannel)
                .CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            upsertResellerResponse.Header.StatusCode
                .Should().BeEquivalentTo(HttpStatusCode.OK, upsertResellerResponse.Header.Message);

            var response = new GetPropositionsRequest { ResellerCode = upsertResellerResponse.Data.ResellerCode }
                .CallWith<GetPropositionsRequest, GetPropositionsResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels[0].Propositions.Should().HaveCountGreaterOrEqualTo(0);
        }

        [Test]
        public void GetPropositions_WithInactiveReseller_Returns200()
        {
            var label = Labels.BudgetEnergie;
            var productType = CustomerProductType.Energy;
            var clientType = RelationCategory.Private;
            var obName = OfferBundleProvider.GetOfferName();

            var offerBundleResponse = OfferBundleProvider.CreateOffer(
                 new OfferProposition[] { OfferBundleProvider.CreateOfferProposition(label) },
                 label, clientType, obName, productType.ToString()).CallWith(OfferBundleClient);
            var offerBundle = OfferBundleProvider.GetCreatedOfferBundle(obName).To<Nuts.Reseller.Contract.OfferBundle>();

            var salesChannelRequest = GenerateAddSalesChannelRequest();
            salesChannelRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == salesChannelRequest.SalesChannel.ExternalReference).Id;

            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(salesChannelId).SetChannelName(salesChannelRequest.SalesChannel.Channel.Name)
                .SetLabel(label.ToString()).SetOfferBundles(offerBundle).Build();

            var upsertResellerResponse = CreateUpsertResellerRequest(ResellerName, false, salesChannel)
                .CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            upsertResellerResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, upsertResellerResponse.Header.Message);

            var response = new GetPropositionsRequest { ResellerCode = upsertResellerResponse.Data.ResellerCode }
                .CallWith<GetPropositionsRequest, GetPropositionsResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels[0].Propositions.Should().HaveCountGreaterOrEqualTo(0);
            response.Data.Reseller.IsActive.Should().BeFalse();
        }

        [Test]
        public void GetPropositions_WithInvalidResellerCode_Returns404()
        {
            var resellerCode = Guid.Empty;
            var response = new GetPropositionsRequest { ResellerCode = resellerCode }
                .CallWith<GetPropositionsRequest, GetPropositionsResponse>(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var patternMessage = string.Format(PatternMessages.ResellerWasNotFound, resellerCode);
            response.Header.Message.Should().BeEquivalentTo(patternMessage);
        }
    }
}