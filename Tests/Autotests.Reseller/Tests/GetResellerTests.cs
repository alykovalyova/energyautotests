﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Reseller.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Reseller.Contract;

namespace Autotests.Reseller.Tests
{
    [TestFixture]
    internal class GetResellerTests : BaseReseller
    {
        [Test]
        public void GetReseller_WithInvalidResellerCode_Returns404()
        {
            var resellerCode = Guid.Empty;
            var response = new GetResellerRequest { ResellerCode = resellerCode }
                .CallWith<GetResellerRequest, GetResellerResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var patternMessage = string.Format(PatternMessages.ResellerWasNotFound, resellerCode);
            response.Header.Message.Should().BeEquivalentTo(patternMessage);
        }
    }
}