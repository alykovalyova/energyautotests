﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Reseller.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Reseller.Contract;
using ResellerOfferBundle = Autotests.Repositories.ResellerModels.OfferBundle;

namespace Autotests.Reseller.Tests
{
    [TestFixture]
    internal class GetSalesChannelPropositionsTests : BaseReseller
    {
        [Test]
        public void GetSalesChannelPropositions_DefaultValidCase_Returns200()
        {
            var salesChannelId = ResellerRepository.GetEntitiesByCondition<ResellerOfferBundle>(ob => true).RandomItem()
                .SalesChannelId;

            new GetSalesChannelPropositionsRequest { SalesChannelId = salesChannelId }
                .CallWith<GetSalesChannelPropositionsRequest, GetSalesChannelPropositionsResponse>(ResellerClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
        }

        [Test]
        public void GetSalesChannelPropositions_WithNotExistedSalesChannelId_Returns404()
        {
            const int salesChannelId = int.MaxValue - 1;

            var response = new GetSalesChannelPropositionsRequest { SalesChannelId = salesChannelId }
                .CallWith<GetSalesChannelPropositionsRequest, GetSalesChannelPropositionsResponse>(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            var patternMessage = string.Format(PatternMessages.OfferBundlesAreNotFound, salesChannelId);
            response.Header.Message.Should().BeEquivalentTo(patternMessage);
        }
    }
}