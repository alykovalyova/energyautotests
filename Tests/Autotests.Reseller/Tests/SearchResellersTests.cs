﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Reseller.Base;
using Autotests.Reseller.Builders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Reseller.Contract;
using ResellerDbSalesChannel = Autotests.Repositories.ResellerModels.SalesChannel;

namespace Autotests.Reseller.Tests
{
    [TestFixture]
    internal class SearchResellersTests : BaseReseller
    {
        [Test]
        public void SearchResellers_DefaultValidTest_Returns200()
        {
            const bool isActive = true;
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();

            var upsertResellerRequest = CreateUpsertResellerRequest(ResellerName, isActive, salesChannel);
            var upsertResellerResponse = upsertResellerRequest.CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);


            upsertResellerResponse.Header.StatusCode
                .Should().BeEquivalentTo(HttpStatusCode.OK, upsertResellerResponse.Header.Message);

            var response = CreateSearchResellersRequest(isActive, offerBundle.Name, upsertResellerRequest.Reseller.Code, ResellerName)
                .CallWith<SearchResellersRequest, SearchResellersResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            Assert.Multiple(() =>
            {
                response.Data.Resellers.Should().OnlyContain(r => r.IsActive.Equals(isActive));
                response.Data.Resellers.Should().OnlyContain(r => r.Name.Equals(ResellerName));
            });
        }

        [Test]
        [TestCase(null, "Invalid_Bundle_Name", null, null, TestName = "SearchResellers_WithInvalidBundleName_ResellersListIsEmpty")]
        [TestCase(null, null, "3297F0F2-35D3-4231-919D-1CFCF4035975", null, TestName = "SearchResellers_WithInvalidCode_ResellersListIsEmpty")]
        [TestCase(null, null, null, "Invalid_Reseller_Name", TestName = "SearchResellers_WithInvalidResellerName_ResellersListIsEmpty")]
        public void SearchResellers_InvalidSearchCriteria_ResellersListIsEmpty(bool? isActive, string bundleName, string code, string resellerName)
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();
            var upsertResellerRequest = CreateUpsertResellerRequest(ResellerName, isActive, salesChannel);
            var upsertResellerResponse = upsertResellerRequest.CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            upsertResellerResponse.Header.StatusCode
                .Should().BeEquivalentTo(HttpStatusCode.OK, upsertResellerResponse.Header.Message);

            var response = CreateSearchResellersRequest(
                isActive ?? true, bundleName ?? offerBundle.Name,
                code ?? upsertResellerRequest.Reseller.Code, resellerName ?? ResellerName)
                .CallWith<SearchResellersRequest, SearchResellersResponse>(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Resellers.Should().BeEmpty();
        }

        [Test]
        public void SearchResellers_InvalidResellerIdValue_Returns400()
        {
            var resellerId = Guid.Empty;
            var response = CreateSearchResellersRequest(resellerId: resellerId.ToString())
                .CallWith<SearchResellersRequest, SearchResellersResponse>(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.InvalidResellerIdValue);
        }
    }
}
