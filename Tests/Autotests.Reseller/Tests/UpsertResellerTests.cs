﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Reseller.Base;
using Autotests.Reseller.Builders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Reseller.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;
using ResellerDbSalesChannel = Autotests.Repositories.ResellerModels.SalesChannel;

namespace Autotests.Reseller.Tests
{
    [TestFixture]
    internal class UpsertResellerTests : BaseReseller
    {
        [Test]
        public void UpsertReseller_DefaultValidTest_CreatesReseller()
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();
            
            var request = CreateUpsertResellerRequest(ResellerName, true, salesChannel);
            var response = request.CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            
            var getResellerResponse = new GetResellerRequest{ResellerCode = response.Data.ResellerCode}
                .CallWith<GetResellerRequest, GetResellerResponse>(ResellerClient);
            getResellerResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, getResellerResponse.Header.Message);
            ObjectComparator.ComparePropsOfTypes(getResellerResponse.Data.Reseller, request.Reseller).Should().BeTrue();
        }

        [Test]
        public void UpsertReseller_WithResellerIsInactive_CreatesReseller()
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();
            
            var request = CreateUpsertResellerRequest(ResellerName, false, salesChannel);
            var response = request.CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);

            var getResellerResponse = new GetResellerRequest { ResellerCode = response.Data.ResellerCode }
                .CallWith<GetResellerRequest, GetResellerResponse>(ResellerClient);
            getResellerResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, getResellerResponse.Header.Message);
            ObjectComparator.ComparePropsOfTypes(getResellerResponse.Data.Reseller, request.Reseller).Should().BeTrue();
        }

        [Test]
        public void UpsertReseller_WithAlreadyExistedSalesChannelId_Returns400()
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();
            
            var response = CreateUpsertResellerRequest(ResellerName, true, salesChannel)
                .CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.SalesChannelAlreadyExists);
        }

        [Test]
        public void UpsertReseller_WithAlreadyExistedResellerName_Returns400()
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();

            var request = CreateUpsertResellerRequest(ResellerName, true, salesChannel);
            request.CallWith(ResellerClient).Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            request.Reseller.SalesChannels[0].ChannelName += "_Changed";
            request.Reseller.SalesChannels[0].SalesChannelId = ++request.Reseller.SalesChannels[0].SalesChannelId;
            var response2 = request.CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            response2.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response2.Header.Message.Should().BeEquivalentTo(PatternMessages.ResellerAlreadyExists);
        }

        [Test]
        [TestCase("", TestName = "UpsertReseller_WithEmptyResellerName_Returns400")]
        [TestCase(null, TestName = "UpsertReseller_WithResellerNameIsNull_Returns400")]
        public void UpsertReseller_WithInvalidResellerName_Returns400(string resellerName)
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();
            
            var response = CreateUpsertResellerRequest(resellerName, true, salesChannel).CallWith(ResellerClient);
            
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.FieldIsRequired, "Name"));
        }

        [Test]
        public void UpsertReseller_WithInvalidSalesChannelId_Returns400([Values(0, null)] int id)
        {
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(id).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();
            
            var response = CreateUpsertResellerRequest(ResellerName, true, salesChannel).CallWith(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(PatternMessages.InvalidSalesChannelIdRange);
        }

        [Test]
        [TestCase("", TestName = "UpsertReseller_WithEmptyChannelName_Returns400")]
        [TestCase(null, TestName = "UpsertReseller_WithChannelNameIsNull_Returns400")]
        public void UpsertReseller_WithInvalidChannelName_Returns400(string channelName)
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(channelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();

            var response = CreateUpsertResellerRequest(ResellerName, true, salesChannel).CallWith(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.FieldIsRequired, "ChannelName"));
        }

        [Test]
        [TestCase("", TestName = "UpsertReseller_WithEmptyLabel_Returns400")]
        [TestCase(null, TestName = "UpsertReseller_WithLabelIsNull_Returns400")]
        public void UpsertReseller_WithInvalidLabel_Returns400(string labelName)
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(OfferBundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(labelName).SetOfferBundles(offerBundle).Build();

            var response = CreateUpsertResellerRequest(ResellerName, true, salesChannel).CallWith(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.FieldIsRequired, "Label"));
        }

        [Test]
        [TestCase("", TestName = "UpsertReseller_WithEmptyOfferBundleName_Returns400")]
        [TestCase(null, TestName = "UpsertReseller_WithOfferBundleNameIsNull_Returns400")]
        public void UpsertReseller_WithInvalidOfferBundleName_Returns400(string bundleName)
        {
            var lastSalesChannelId = ResellerRepository.GetMaxEntity<ResellerDbSalesChannel, int>(s => s.SalesChannelId);
            var offerBundle = BuildDirector.Get<OfferBundleBuilder>()
                .SetOfferBundleId(GetNewOfferBundleId()).SetName(bundleName).Build();
            var salesChannel = BuildDirector.Get<SalesChannelBuilder>()
                .SetSalesChannelId(++lastSalesChannelId).SetChannelName(SalesChannelName)
                .SetLabel(Label).SetOfferBundles(offerBundle).Build();

            var response = CreateUpsertResellerRequest(ResellerName, true, salesChannel).CallWith(ResellerClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.ErrorDetails.Should().Contain(string.Format(PatternMessages.FieldIsRequired, "Name"));
        }

        [Test, Ignore("some issue with builder - can not parse SalesChannel = null into correct json format")]
        public void UpsertReseller_WithoutSalesChannel_Return400()
        {
            var request = CreateUpsertResellerRequest(ResellerName, false, null);
            var response = request.CallWith<UpsertResellerRequest, UpsertResellerResponse>(ResellerClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);

            var getResellerResponse = new GetResellerRequest { ResellerCode = response.Data.ResellerCode }
                .CallWith<GetResellerRequest, GetResellerResponse>(ResellerClient);
            getResellerResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest, getResellerResponse.Header.Message);
            ObjectComparator.ComparePropsOfTypes(getResellerResponse.Data.Reseller, request.Reseller).Should().BeTrue();
        }
    }
}