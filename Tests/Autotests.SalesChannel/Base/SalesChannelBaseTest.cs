﻿using System.Net;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Repositories.SalesChannelsModels;
using Autotests.SalesChannel.Factory;
using NUnit.Framework;
using Nuts.ApiClient;
using Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus;
using Nuts.SalesChannels.Model.Core.Enums;

namespace Autotests.SalesChannel.Base
{
    [SetUpFixture, System.ComponentModel.Category(Categories.SalesChannel)]
    internal abstract class SalesChannelBaseTest : AllureReport
    {
        //clients declaration
        protected NutsHttpClient SalesChannelClient;

        //databases declaration
        protected DbHandler<SalesChannelsContext> SalesChannelDb;

        //variables initialization
        protected const string TestMarker = "Autotest_";
        private ConfigHandler _configHelper = new ConfigHandler();
        public string UserName => $"Test_{Guid.NewGuid().ToString().Remove(4)}";

        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            SalesChannelClient = new NutsHttpClient(new BaseHttpClient(_configHelper.ApiUrls.SalesChannel));
            SalesChannelDb = new DbHandler<SalesChannelsContext>(_configHelper.GetConnectionString(DbConnectionName.SalesChannelsDatabase.ToString()));
        }

        [OneTimeTearDown]
        public void AfterAllTests()
        {
            SalesChannelDb.DeleteEntitiesByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId.StartsWith(TestMarker) || sc.UserName.StartsWith(TestMarker));
        }

        public void CreateSalesChannel(SalesChannelStatus status)
        {
            const string errorMessage = "SalesChannelWith '{0}' status hasn't been created.\n{1}";

            var addRequest = SalesChannelFactory.GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;

            if (status == SalesChannelStatus.New) return;
            ApiResponse response;
            if (status == SalesChannelStatus.Inactive)
            {
                response = new UpdateSalesChannelStatusRequest
                { SalesChannelId = salesChannelId, NewStatus = SalesChannelStatus.Active }
                    .CallWith(SalesChannelClient);
                if (response.Header.StatusCode != HttpStatusCode.OK)
                    throw new AssertionException(string.Format(errorMessage, status, response.Header.Message));
            }

            response = new UpdateSalesChannelStatusRequest { SalesChannelId = salesChannelId, NewStatus = status }
                .CallWith(SalesChannelClient);
            if (response.Header.StatusCode != HttpStatusCode.OK)
                throw new AssertionException(string.Format(errorMessage, status, response.Header.Message));
        }
    }
}