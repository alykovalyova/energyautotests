﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract;

namespace Autotests.SalesChannel.Builders
{
    internal class AddSalesChannelFinancialDetailsBuilder : MainBuilder<AddSalesChannelFinancialDetails>
    {
        internal AddSalesChannelFinancialDetailsBuilder SetSkipPaidCreditCheck(bool skipPaidCreditCheck)
        {
            Instance.SkipPaidCreditCheck = skipPaidCreditCheck;
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetIsConsumerEligible(bool? isConsumerEligible)
        {
            Instance.IsConsumerEligible = isConsumerEligible;
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetIsBusinessEligible(bool? isBusinessEligible)
        {
            Instance.IsBusinessEligible = isBusinessEligible;
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetIsContractProvider(bool isContractProvider)
        {
            Instance.IsContractProvider = isContractProvider;
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetExternalReference(string exRef)
        {
            Instance.ExternalReference = exRef;
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetCampaign(int id, string name = "Test")
        {
            Instance.Campaign = new Campaign { Id = id, Name = name };
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetChannel(int id, string name)
        {
            Instance.Channel = new Channel { Id = id, Name = name };
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetPartner(int id, string name = "5-steps")
        {
            Instance.Partner = new Partner { Id = id, Name = name };
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetCoolDownPeriod(int id, int value)
        {
            Instance.CoolDownPeriod = new CoolDownPeriod { Id = id, Value = value };
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetSwitchWindow(int id, int value)
        {
            Instance.SwitchWindow = new SwitchWindow { Id = id, Value = value };
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetLabel(Labels label)
        {
            Instance.Label = label;
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetRegion(int id, string name = "Regio 1")
        {
            Instance.Region = new Region { Id = id, Name = name };
            return this;
        }

        internal AddSalesChannelFinancialDetailsBuilder SetStore(int id, string name = "Alkmaar")
        {
            Instance.Store = new Store { Id = id, Name = name };
            return this;
        }
    }
}