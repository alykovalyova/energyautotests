﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract;
using Nuts.SalesChannels.Model.Contract.EditSalesChannel;

namespace Autotests.SalesChannel.Builders
{
    internal class EditNewSalesChannelFinancialModelBuilder : MainBuilder<EditNewSalesChannelFinancialModel>
    {
        internal EditNewSalesChannelFinancialModelBuilder SetSkipPaidCreditCheck(bool skipPaidCreditCheck)
        {
            Instance.SkipPaidCreditCheck = skipPaidCreditCheck;
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetLabel(Labels label)
        {
            Instance.Label = label;
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetCoolDownPeriod(int id, int value)
        {
            Instance.CoolDownPeriod = new CoolDownPeriod { Id = id, Value = value };
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetSwitchWindow(int id, int value)
        {
            Instance.SwitchWindow = new SwitchWindow { Id = id, Value = value };
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetCampaign(int id, string name)
        {
            Instance.Campaign = new Campaign { Id = id, Name = name };
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetChannel(int id, string name)
        {
            Instance.Channel = new Channel { Id = id, Name = name };
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetPartner(int id, string name)
        {
            Instance.Partner = new Partner { Id = id, Name = name };
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetIsConsumerEligible(bool isConsumerEligible)
        {
            Instance.IsConsumerEligible = isConsumerEligible;
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetIsBusinessEligible(bool isBusinessEligible)
        {
            Instance.IsBusinessEligible = isBusinessEligible;
            return this;
        }

        internal EditNewSalesChannelFinancialModelBuilder SetIsContractProvider(bool isContractProvider)
        {
            Instance.IsContractProvider = isContractProvider;
            return this;
        }
    }
}