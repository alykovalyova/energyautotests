﻿using Autotests.Core.Helpers;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract;
using Nuts.SalesChannels.Model.Contract.EditSalesChannel;

namespace Autotests.SalesChannel.Builders
{
    internal class EditNewSalesChannelModelBuilder : MainBuilder<EditNewSalesChannelModel>
    {
        internal EditNewSalesChannelModelBuilder SetLabel(Labels label)
        {
            Instance.Label = label;
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetCoolDownPeriod(int id, int value)
        {
            Instance.CoolDownPeriod = new CoolDownPeriod{Id = id, Value = value};
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetSwitchWindow(int id, int value)
        {
            Instance.SwitchWindow = new SwitchWindow{Id = id, Value = value};
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetCampaign(int id, string name)
        {
            Instance.Campaign = new Campaign{Id = id, Name = name};
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetChannel(int id, string name)
        {
            Instance.Channel = new Channel{Id = id, Name = name};
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetPartner(int id, string name)
        {
            Instance.Partner = new Partner{Id = id, Name = name};
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetIsConsumerEligible(bool isConsumerEligible)
        {
            Instance.IsConsumerEligible = isConsumerEligible;
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetIsBusinessEligible(bool isBusinessEligible)
        {
            Instance.IsBusinessEligible = isBusinessEligible;
            return this;
        }

        internal EditNewSalesChannelModelBuilder SetIsContractProvider(bool isContractProvider)
        {
            Instance.IsContractProvider = isContractProvider;
            return this;
        }
    }
}