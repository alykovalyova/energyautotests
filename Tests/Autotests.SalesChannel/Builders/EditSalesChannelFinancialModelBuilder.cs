﻿using Autotests.Core.Helpers;
using Nuts.SalesChannels.Model.Contract;
using Nuts.SalesChannels.Model.Contract.EditSalesChannel;

namespace Autotests.SalesChannel.Builders
{
    internal class EditSalesChannelFinancialModelBuilder : MainBuilder<EditSalesChannelFinancialModel>
    {
        internal EditSalesChannelFinancialModelBuilder SetSkipPaidCreditCheck(bool skipPaidCreditCheck)
        {
            Instance.SkipPaidCreditCheck = skipPaidCreditCheck;
            return this;
        }

        internal EditSalesChannelFinancialModelBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal EditSalesChannelFinancialModelBuilder SetIsConsumerEligible(bool isConsumerEligible)
        {
            Instance.IsConsumerEligible = isConsumerEligible;
            return this;
        }

        internal EditSalesChannelFinancialModelBuilder SetIsBusinessEligible(bool isBusinessEligible)
        {
            Instance.IsBusinessEligible = isBusinessEligible;
            return this;
        }

        internal EditSalesChannelFinancialModelBuilder SetIsContractProvider(bool isContractProvider)
        {
            Instance.IsContractProvider = isContractProvider;
            return this;
        }

        internal EditSalesChannelFinancialModelBuilder SetChannel(int id, string name)
        {
            Instance.Channel = new Channel { Id = id, Name = name };
            return this;
        }

        internal EditSalesChannelFinancialModelBuilder SetPartner(int id, string name)
        {
            Instance.Partner = new Partner { Id = id, Name = name };
            return this;
        }
    }
}