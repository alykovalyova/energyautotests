﻿using Autotests.Core.Helpers;
using Nuts.SalesChannels.Model.Contract;
using Nuts.SalesChannels.Model.Contract.EditSalesChannel;

namespace Autotests.SalesChannel.Builders
{
    internal class EditSalesChannelModelBuilder : MainBuilder<EditSalesChannelModel>
    {
        internal EditSalesChannelModelBuilder SetId(int id)
        {
            Instance.Id = id;
            return this;
        }

        internal EditSalesChannelModelBuilder SetIsConsumerEligible(bool isConsumerEligible)
        {
            Instance.IsConsumerEligible = isConsumerEligible;
            return this;
        }

        internal EditSalesChannelModelBuilder SetIsBusinessEligible(bool isBusinessEligible)
        {
            Instance.IsBusinessEligible = isBusinessEligible;
            return this;
        }

        internal EditSalesChannelModelBuilder SetIsContractProvider(bool isContractProvider)
        {
            Instance.IsContractProvider = isContractProvider;
            return this;
        }

        internal EditSalesChannelModelBuilder SetChannel(int id, string name)
        {
            Instance.Channel = new Channel{Id = id, Name = name};
            return this;
        }

        internal EditSalesChannelModelBuilder SetPartner(int id, string name)
        {
            Instance.Partner = new Partner{Id = id, Name = name};
            return this;
        }
    }
}