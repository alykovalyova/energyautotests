﻿using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Builders.SalesChannel;
using Autotests.Helper.DataProviders.FakeEdsn;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Builders;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract;
using Nuts.SalesChannels.Model.Contract.AddSalesChannel;
using Nuts.SalesChannels.Model.Contract.EditSalesChannel;

namespace Autotests.SalesChannel.Factory
{
    internal static class SalesChannelFactory
    {
        private const string TestMarker = "Autotest_";
        private static readonly BuildDirector _buildDirector = new BuildDirector();
        internal static string UserName => TestConstants.SalesChannel_UserName;

        internal static string ExReference => TestConstants.salesChannel_ExternalReference;

        internal static AddSalesChannelRequest GenerateAddSalesChannelRequest(Channels channel = Channels.Retail,
            bool valid = true)
        {
            const int id = 1;
            var request = new AddSalesChannelRequest
            {
                SalesChannel = _buildDirector.Get<AddSalesChannelDetailsBuilder>()
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(ExReference).SetCampaign(id)
                    .SetChannel((int) channel, channel.ToString()).SetPartner(id)
                    .SetCoolDownPeriod(1, 14).SetSwitchWindow(id, 244).SetLabel(Labels.BudgetEnergie)
                    .InCase(valid && channel.Equals(Channels.Retail), b => b.SetRegion(id).SetStore(id))
                    .InCase(!valid && !channel.Equals(Channels.Retail), b => b.SetRegion(id).SetStore(id))
                    .Build(),
                UserName = UserName
            };
            return request;
        }

        internal static AddSalesChannelRequest GenerateAddSalesChannelRequestWithoutRegionAndStore(Channels channel)
        {
            const int id = 1;
            return new AddSalesChannelRequest
            {
                SalesChannel = _buildDirector.Get<AddSalesChannelDetailsBuilder>()
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(ExReference).SetCampaign(id)
                    .SetChannel((int) channel, channel.ToString()).SetPartner(id)
                    .SetCoolDownPeriod(1, 14).SetSwitchWindow(id, 244).SetLabel(Labels.BudgetEnergie)
                    .Build(),
                UserName = UserName
            };
        }

        internal static AddSalesChannelRequest GenerateCustomAddSalesChannelRequest(
            KeyValuePair<int, string> campaignValues,
            KeyValuePair<int, string> partnerValues,
            KeyValuePair<int, string> regionValues,
            KeyValuePair<int, string> storeValues,
            KeyValuePair<int, int> coolDownPeriodValues,
            KeyValuePair<int, int> switchWindowValues)
        {
            return new AddSalesChannelRequest
            {
                SalesChannel = _buildDirector.Get<AddSalesChannelDetailsBuilder>()
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(ExReference).SetCampaign(campaignValues.Key, campaignValues.Value)
                    .SetChannel((int) Channels.Retail, Channels.Retail.ToString())
                    .SetPartner(partnerValues.Key, partnerValues.Value)
                    .SetRegion(regionValues.Key, regionValues.Value)
                    .SetStore(storeValues.Key, storeValues.Value)
                    .SetCoolDownPeriod(coolDownPeriodValues.Key, coolDownPeriodValues.Value)
                    .SetSwitchWindow(switchWindowValues.Key, switchWindowValues.Value).SetLabel(Labels.BudgetEnergie)
                    .Build(),
                UserName = UserName
            };
        }

        internal static AddSalesChannelFinancialRequest GenerateAddSalesChannelFinancialRequest(
            Channels channel = Channels.Retail, bool valid = true, bool skipPaidCreditCheck = true)
        {
            const int id = 1;
            var request = new AddSalesChannelFinancialRequest
            {
                SalesChannel = _buildDirector.Get<AddSalesChannelFinancialDetailsBuilder>()
                    .SetSkipPaidCreditCheck(skipPaidCreditCheck)
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(ExReference).SetCampaign(id)
                    .SetChannel((int) channel, channel.ToString()).SetPartner(id)
                    .SetCoolDownPeriod(1, 14).SetSwitchWindow(id, 244).SetLabel(Labels.BudgetEnergie)
                    .InCase(valid && channel.Equals(Channels.Retail), b => b.SetRegion(id).SetStore(id))
                    .InCase(!valid && !channel.Equals(Channels.Retail), b => b.SetRegion(id).SetStore(id))
                    .Build(),
                UserName = UserName
            };

            return request;
        }

        internal static AddSalesChannelFinancialRequest GenerateAddSalesChannelFinancialRequestWithoutRegionAndStore(
            Channels channel, bool skipPaidCreditCheck = true)
        {
            const int id = 1;
            return new AddSalesChannelFinancialRequest
            {
                SalesChannel = _buildDirector.Get<AddSalesChannelFinancialDetailsBuilder>()
                    .SetSkipPaidCreditCheck(skipPaidCreditCheck)
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(ExReference).SetCampaign(id)
                    .SetChannel((int) channel, channel.ToString()).SetPartner(id)
                    .SetCoolDownPeriod(1, 14).SetSwitchWindow(id, 244).SetLabel(Labels.BudgetEnergie)
                    .Build(),
                UserName = UserName
            };
        }

        internal static AddSalesChannelFinancialRequest GenerateCustomAddSalesChannelFinancialRequest(
            KeyValuePair<int, string> campaignValues, KeyValuePair<int, string> partnerValues,
            KeyValuePair<int, string> regionValues, KeyValuePair<int, string> storeValues,
            KeyValuePair<int, int> coolDownPeriodValues, KeyValuePair<int, int> switchWindowValues)
        {
            return new AddSalesChannelFinancialRequest
            {
                SalesChannel = _buildDirector.Get<AddSalesChannelFinancialDetailsBuilder>()
                    .SetSkipPaidCreditCheck(true)
                    .SetIsConsumerEligible(true).SetIsBusinessEligible(true).SetIsContractProvider(true)
                    .SetExternalReference(ExReference).SetCampaign(campaignValues.Key, campaignValues.Value)
                    .SetChannel((int) Channels.Retail, Channels.Retail.ToString())
                    .SetPartner(partnerValues.Key, partnerValues.Value)
                    .SetRegion(regionValues.Key, regionValues.Value)
                    .SetStore(storeValues.Key, storeValues.Value)
                    .SetCoolDownPeriod(coolDownPeriodValues.Key, coolDownPeriodValues.Value)
                    .SetSwitchWindow(switchWindowValues.Key, switchWindowValues.Value).SetLabel(Labels.BudgetEnergie)
                    .Build(),
                UserName = UserName
            };
        }

        internal static EditSalesChannelRequest GenerateEditSalesChannelRequest(
            int salesChannelId, AddSalesChannelRequest addSalesChannelRequest,
            bool editInnerObjects = false, bool isValidForRetail = true)
        {
            var request = new EditSalesChannelRequest
            {
                SalesChannel = SetEditSalesChannelModel(salesChannelId, addSalesChannelRequest),
                UserName = addSalesChannelRequest.UserName
            };
            if (addSalesChannelRequest.SalesChannel.Region != null &&
                addSalesChannelRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region = new Region
                {
                    Id = addSalesChannelRequest.SalesChannel.Region.Id,
                    Name = addSalesChannelRequest.SalesChannel.Region.Name
                };
                request.SalesChannel.Store = new Store
                {
                    Id = addSalesChannelRequest.SalesChannel.Store.Id,
                    Name = addSalesChannelRequest.SalesChannel.Store.Name
                };
            }

            if (!editInnerObjects) return request;
            ResetInnerObjectsValues(request, addSalesChannelRequest, isValidForRetail);

            return request;
        }

        internal static EditNewSalesChannelRequest GenerateEditNewSalesChannelRequest(
            int salesChannelId, AddSalesChannelRequest addSalesChannelRequest,
            bool editInnerObjects = false, bool isValidForRetail = true)
        {
            var request = new EditNewSalesChannelRequest
            {
                SalesChannel = SetEditNewSalesChannelModel(salesChannelId, addSalesChannelRequest),
                UserName = addSalesChannelRequest.UserName
            };
            if (addSalesChannelRequest.SalesChannel.Region != null &&
                addSalesChannelRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region = new Region
                {
                    Id = addSalesChannelRequest.SalesChannel.Region.Id,
                    Name = addSalesChannelRequest.SalesChannel.Region.Name
                };
                request.SalesChannel.Store = new Store
                {
                    Id = addSalesChannelRequest.SalesChannel.Store.Id,
                    Name = addSalesChannelRequest.SalesChannel.Store.Name
                };
            }

            if (!editInnerObjects) return request;

            ResetInnerObjectsValues(request, addSalesChannelRequest, isValidForRetail);
            return request;
        }

        internal static EditSalesChannelFinancialRequest GenerateEditSalesChannelFinancialRequest(
            int salesChannelId, AddSalesChannelFinancialRequest addSalesChannelFinancialRequest,
            bool editInnerObjects = false, bool isValidForRetail = true)
        {
            var request = new EditSalesChannelFinancialRequest
            {
                SalesChannel = SetEditSalesChannelModel(salesChannelId, addSalesChannelFinancialRequest),
                UserName = addSalesChannelFinancialRequest.UserName
            };
            if (addSalesChannelFinancialRequest.SalesChannel.Region != null &&
                addSalesChannelFinancialRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region = new Region
                {
                    Id = addSalesChannelFinancialRequest.SalesChannel.Region.Id,
                    Name = addSalesChannelFinancialRequest.SalesChannel.Region.Name
                };
                request.SalesChannel.Store = new Store
                {
                    Id = addSalesChannelFinancialRequest.SalesChannel.Store.Id,
                    Name = addSalesChannelFinancialRequest.SalesChannel.Store.Name
                };
            }

            if (!editInnerObjects) return request;

            ResetInnerObjectsValues(request, addSalesChannelFinancialRequest, isValidForRetail);
            return request;
        }

        internal static EditNewSalesChannelFinancialRequest GenerateEditNewSalesChannelFinancialRequest(
            int salesChannelId, AddSalesChannelFinancialRequest addSalesChannelFinancialRequest,
            bool editInnerObjects = false, bool isValidForRetail = true)
        {
            var request = new EditNewSalesChannelFinancialRequest
            {
                SalesChannel = SetEditNewSalesChannelModel(salesChannelId, addSalesChannelFinancialRequest),
                UserName = addSalesChannelFinancialRequest.UserName
            };
            if (addSalesChannelFinancialRequest.SalesChannel.Region != null &&
                addSalesChannelFinancialRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region = new Region
                {
                    Id = addSalesChannelFinancialRequest.SalesChannel.Region.Id,
                    Name = addSalesChannelFinancialRequest.SalesChannel.Region.Name
                };
                request.SalesChannel.Store = new Store
                {
                    Id = addSalesChannelFinancialRequest.SalesChannel.Store.Id,
                    Name = addSalesChannelFinancialRequest.SalesChannel.Store.Name
                };
            }

            if (!editInnerObjects) return request;

            ResetInnerObjectsValues(request, addSalesChannelFinancialRequest, isValidForRetail);
            return request;
        }

        private static EditSalesChannelModel SetEditSalesChannelModel(int salesChannelId,
            AddSalesChannelRequest addSalesChannelRequest)
        {
            var salesChannel = addSalesChannelRequest.SalesChannel;
            return _buildDirector.Get<EditSalesChannelModelBuilder>()
                .SetId(salesChannelId)
                .SetIsConsumerEligible(salesChannel.IsConsumerEligible.GetValueOrDefault())
                .SetIsBusinessEligible(salesChannel.IsBusinessEligible.GetValueOrDefault())
                .SetIsContractProvider(salesChannel.IsContractProvider)
                .SetChannel(salesChannel.Channel.Id, salesChannel.Channel.Name)
                .SetPartner(salesChannel.Partner.Id, salesChannel.Partner.Name)
                .Build();
        }

        private static EditSalesChannelFinancialModel SetEditSalesChannelModel(
            int salesChannelId,
            AddSalesChannelFinancialRequest addSalesChannelRequest,
            bool skipPaidCreditCheck = true)
        {
            var salesChannel = addSalesChannelRequest.SalesChannel;
            return _buildDirector.Get<EditSalesChannelFinancialModelBuilder>()
                .SetSkipPaidCreditCheck(skipPaidCreditCheck).SetId(salesChannelId)
                .SetIsConsumerEligible(salesChannel.IsConsumerEligible.GetValueOrDefault())
                .SetIsBusinessEligible(salesChannel.IsBusinessEligible.GetValueOrDefault())
                .SetIsContractProvider(salesChannel.IsContractProvider)
                .SetChannel(salesChannel.Channel.Id, salesChannel.Channel.Name)
                .SetPartner(salesChannel.Partner.Id, salesChannel.Partner.Name)
                .Build();
        }

        private static EditNewSalesChannelModel SetEditNewSalesChannelModel(int salesChannelId,
            AddSalesChannelRequest addSalesChannelRequest)
        {
            var salesChannel = addSalesChannelRequest.SalesChannel;
            return _buildDirector.Get<EditNewSalesChannelModelBuilder>()
                .SetId(salesChannelId).SetLabel(salesChannel.Label)
                .SetIsConsumerEligible(salesChannel.IsConsumerEligible.GetValueOrDefault())
                .SetIsBusinessEligible(salesChannel.IsBusinessEligible.GetValueOrDefault())
                .SetIsContractProvider(salesChannel.IsContractProvider)
                .SetCoolDownPeriod(salesChannel.CoolDownPeriod.Id, salesChannel.CoolDownPeriod.Value)
                .SetSwitchWindow(salesChannel.SwitchWindow.Id, salesChannel.SwitchWindow.Value)
                .SetCampaign(salesChannel.Campaign.Id, salesChannel.Campaign.Name)
                .SetChannel(salesChannel.Channel.Id, salesChannel.Channel.Name)
                .SetPartner(salesChannel.Partner.Id, salesChannel.Partner.Name)
                .Build();
        }

        private static EditNewSalesChannelFinancialModel SetEditNewSalesChannelModel(
            int salesChannelId,
            AddSalesChannelFinancialRequest addSalesChannelRequest,
            bool skipPaidCreditCheck = true)
        {
            var salesChannel = addSalesChannelRequest.SalesChannel;
            return _buildDirector.Get<EditNewSalesChannelFinancialModelBuilder>()
                .SetSkipPaidCreditCheck(skipPaidCreditCheck).SetId(salesChannelId).SetLabel(salesChannel.Label)
                .SetIsConsumerEligible(salesChannel.IsConsumerEligible.GetValueOrDefault())
                .SetIsBusinessEligible(salesChannel.IsBusinessEligible.GetValueOrDefault())
                .SetIsContractProvider(salesChannel.IsContractProvider)
                .SetCoolDownPeriod(salesChannel.CoolDownPeriod.Id, salesChannel.CoolDownPeriod.Value)
                .SetSwitchWindow(salesChannel.SwitchWindow.Id, salesChannel.SwitchWindow.Value)
                .SetCampaign(salesChannel.Campaign.Id, salesChannel.Campaign.Name)
                .SetChannel(salesChannel.Channel.Id, salesChannel.Channel.Name)
                .SetPartner(salesChannel.Partner.Id, salesChannel.Partner.Name)
                .Build();
        }

        private static void ResetInnerObjectsValues(
            EditSalesChannelRequest request, AddSalesChannelRequest addSalesChannelRequest, bool isValidForRetail)
        {
            request.SalesChannel.Channel.Id = (int) Channels.Retail;
            request.SalesChannel.Channel.Name = Channels.Retail.ToString();
            request.SalesChannel.Partner.Id = 2;
            request.SalesChannel.Partner.Name = "Actis Wonen";
            if (addSalesChannelRequest.SalesChannel.Region != null && addSalesChannelRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region.Id = 2;
                request.SalesChannel.Region.Name = "Regio 2";
                request.SalesChannel.Store.Id = 2;
                request.SalesChannel.Store.Name = "Almere";
            }

            if (request.SalesChannel.Channel.Name == Channels.Retail.ToString() && isValidForRetail)
            {
                request.SalesChannel.Region = new Region {Id = 2, Name = "Regio 2"};
                request.SalesChannel.Store = new Store {Id = 2, Name = "Almere"};
            }
        }

        private static void ResetInnerObjectsValues(
            EditNewSalesChannelRequest request, AddSalesChannelRequest addSalesChannelRequest, bool isValidForRetail)
        {
            request.SalesChannel.Label = Labels.NLE;
            request.SalesChannel.SwitchWindow.Id = 2;
            request.SalesChannel.SwitchWindow.Value = 365;
            request.SalesChannel.Campaign.Id = 2;
            request.SalesChannel.Campaign.Name = "API";
            request.SalesChannel.Channel.Id = (int) Channels.Retail;
            request.SalesChannel.Channel.Name = Channels.Retail.ToString();
            request.SalesChannel.Partner.Id = 2;
            request.SalesChannel.Partner.Name = "Actis Wonen";
            if (addSalesChannelRequest.SalesChannel.Region != null && addSalesChannelRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region.Id = 2;
                request.SalesChannel.Region.Name = "Regio 2";
                request.SalesChannel.Store.Id = 2;
                request.SalesChannel.Store.Name = "Almere";
            }

            if (request.SalesChannel.Channel.Name == Channels.Retail.ToString() && isValidForRetail)
            {
                request.SalesChannel.Region = new Region {Id = 2, Name = "Regio 2"};
                request.SalesChannel.Store = new Store {Id = 2, Name = "Almere"};
            }
        }

        private static void ResetInnerObjectsValues(
            EditSalesChannelFinancialRequest request, AddSalesChannelFinancialRequest addSalesChannelRequest,
            bool isValidForRetail)
        {
            request.SalesChannel.Channel.Id = (int) Channels.Retail;
            request.SalesChannel.Channel.Name = Channels.Retail.ToString();
            request.SalesChannel.Partner.Id = 2;
            request.SalesChannel.Partner.Name = "Actis Wonen";
            if (addSalesChannelRequest.SalesChannel.Region != null && addSalesChannelRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region.Id = 2;
                request.SalesChannel.Region.Name = "Regio 2";
                request.SalesChannel.Store.Id = 2;
                request.SalesChannel.Store.Name = "Almere";
            }

            if (request.SalesChannel.Channel.Name == Channels.Retail.ToString() && isValidForRetail)
            {
                request.SalesChannel.Region = new Region {Id = 2, Name = "Regio 2"};
                request.SalesChannel.Store = new Store {Id = 2, Name = "Almere"};
            }
        }

        private static void ResetInnerObjectsValues(
            EditNewSalesChannelFinancialRequest request, AddSalesChannelFinancialRequest addSalesChannelRequest,
            bool isValidForRetail)
        {
            request.SalesChannel.Label = Labels.NLE;
            request.SalesChannel.SwitchWindow.Id = 2;
            request.SalesChannel.SwitchWindow.Value = 365;
            request.SalesChannel.Campaign.Id = 2;
            request.SalesChannel.Campaign.Name = "API";
            request.SalesChannel.Channel.Id = (int) Channels.Retail;
            request.SalesChannel.Channel.Name = Channels.Retail.ToString();
            request.SalesChannel.Partner.Id = 2;
            request.SalesChannel.Partner.Name = "Actis Wonen";
            if (addSalesChannelRequest.SalesChannel.Region != null && addSalesChannelRequest.SalesChannel.Store != null)
            {
                request.SalesChannel.Region.Id = 2;
                request.SalesChannel.Region.Name = "Regio 2";
                request.SalesChannel.Store.Id = 2;
                request.SalesChannel.Store.Name = "Almere";
            }

            if (request.SalesChannel.Channel.Name == Channels.Retail.ToString() && isValidForRetail)
            {
                request.SalesChannel.Region = new Region {Id = 2, Name = "Regio 2"};
                request.SalesChannel.Store = new Store {Id = 2, Name = "Almere"};
            }
        }
    }
}