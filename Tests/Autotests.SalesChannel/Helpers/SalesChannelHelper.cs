﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.Repositories.SalesChannelsModels;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract;
using Nuts.SalesChannels.Model.Contract.AddSalesChannel;
using Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus;
using Nuts.SalesChannels.Model.Core.Enums;
using Campaign = Nuts.SalesChannels.Model.Contract.Campaign;
using Channel = Nuts.SalesChannels.Model.Contract.Channel;
using CoolDownPeriod = Nuts.SalesChannels.Model.Contract.CoolDownPeriod;
using Partner = Nuts.SalesChannels.Model.Contract.Partner;
using SwitchWindow = Nuts.SalesChannels.Model.Contract.SwitchWindow;

namespace Autotests.SalesChannel.Helpers
{
    internal static class SalesChannelHelper
    {
        internal static string ExReference => $"Test_{Guid.NewGuid().ToString().Remove(5)}";

        public static HttpClient SalesChannelHttpClient = new BaseHttpClient(ConfigHandler.Instance.ApiUrls.SalesChannel);
        private static readonly NutsHttpClient _salesChannelApiClient = new NutsHttpClient(SalesChannelHttpClient);

        private static readonly DbHandler<SalesChannelsContext> _salesChannelDb =
            new DbHandler<SalesChannelsContext>(
                ConfigHandler.Instance.GetConnectionString(DbConnectionName.SalesChannelsDatabase.ToString()));

        private static int CreateSalesChannel(Labels label, Channels channel = Channels.D2D)
        {
            var request = new AddSalesChannelRequest()
            {
                SalesChannel = new AddSalesChannelDetails()
                {
                    IsConsumerEligible = true,
                    IsBusinessEligible = true,
                    IsContractProvider = true,
                    ExternalReference = ExReference,
                    Campaign = new Campaign { Id = 1, Name = "Test" },
                    Channel = new Channel { Id = (int)channel, Name = channel.ToString() },
                    Partner = new Partner { Id = 1, Name = "5-steps" },
                    CoolDownPeriod = new CoolDownPeriod { Id = 1, Value = 14 },
                    SwitchWindow = new SwitchWindow { Id = 1, Value = 244 },
                    Label = label
                },
                UserName = "AutoTest"
            };
            var response = request.CallWith(_salesChannelApiClient);
            Assert.That(response.Header.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            return _salesChannelDb
                .GetLastEntity<Repositories.SalesChannelsModels.SalesChannel, DateTime>(sc => sc.CreatedOn).Id;
        }

        private static void UpdateSalesChannelStatus(SalesChannelStatus status, int salesChannelId)
        {
            var activateChannelRequest = new UpdateSalesChannelStatusRequest
            {
                SalesChannelId = salesChannelId,
                NewStatus = status
            };
            var activatedResponse = activateChannelRequest.CallWith(_salesChannelApiClient);
            Assert.That(activatedResponse.Header.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        public static int GetSalesChannelForTest(Labels label)
        {
            var salesChannelId = CreateSalesChannel(label);
            UpdateSalesChannelStatus(SalesChannelStatus.Active, salesChannelId);
            return salesChannelId;
        }
    }
}