﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class AddSalesChannelFinancialTests : SalesChannelBaseTest
    {
        [Test]
        public void AddSalesChannelFinancial_WithRetailChannel_Returns200()
        {
            var request = GenerateAddSalesChannelFinancialRequest();
            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test]
        [TestCase(Channels.D2D, TestName = "AddSalesChannelFinancial_D2dChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Intern,
            TestName = "AddSalesChannelFinancial_InternChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Onbekend,
            TestName = "AddSalesChannelFinancial_OnbekendChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Prijsvergelijker,
            TestName = "AddSalesChannelFinancial_PrijsvergelijkerChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Veiling,
            TestName = "AddSalesChannelFinancial_VeilingChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Website,
            TestName = "AddSalesChannelFinancial_WebsiteChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Wederverkoper,
            TestName = "AddSalesChannelFinancial_WederverkoperChannelsWithoutRegionAndStore_Returns200")]
        public void AddSalesChannelFinancial_ValidChannelData_Returns200(Channels channel)
        {
            var request = GenerateAddSalesChannelFinancialRequestWithoutRegionAndStore(channel);
            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test]
        [TestCase(Channels.D2D, TestName = "AddSalesChannelFinancial_D2dChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Intern,
            TestName = "AddSalesChannelFinancial_InternChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Onbekend,
            TestName = "AddSalesChannelFinancial_OnbekendChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Prijsvergelijker,
            TestName = "AddSalesChannelFinancial_PrijsvergelijkerChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Veiling,
            TestName = "AddSalesChannelFinancial_VeilingChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Website,
            TestName = "AddSalesChannelFinancial_WebsiteChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Wederverkoper,
            TestName = "AddSalesChannelFinancial_WederverkoperChannelsWithRegionAndStore_Returns400Error")]
        public void AddSalesChannelFinancial_InvalidChannelData_Returns400Error(Channels channel)
        {
            var response = GenerateAddSalesChannelFinancialRequest(channel, false)
                .CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.RegionAndStoreShouldBeEmpty);
        }

        [Test, Pairwise]
        public void AddSalesChannelFinancial_CheckEligibleAndProviderProps_Returns200(
            [Values(true, false)] bool isConsumerEligible,
            [Values(true, false)] bool? isBusinessEligible,
            [Values(true, false)] bool isContractProvider)
        {
            var request = GenerateAddSalesChannelFinancialRequest();
            request.SalesChannel.IsConsumerEligible = isConsumerEligible;
            request.SalesChannel.IsBusinessEligible = isBusinessEligible;
            request.SalesChannel.IsContractProvider = isContractProvider;

            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test, Pairwise]
        public void AddSalesChannelFinancial_CheckInvalidNamedParamsExceptChannel_Returns400Or404Error(
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetCampaignTestData))]
            KeyValuePair<int, string> campaignValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetPartnerTestData))]
            KeyValuePair<int, string> partnerValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetRegionTestData))]
            KeyValuePair<int, string> regionValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetStoreTestData))]
            KeyValuePair<int, string> storeValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetCoolDownPeriodTestData))]
            KeyValuePair<int, int> coolDownPeriodValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetSwitchWindowTestData))]
            KeyValuePair<int, int> switchWindowValues)
        {
            var response = GenerateCustomAddSalesChannelFinancialRequest(campaignValues, partnerValues, regionValues,
                storeValues, coolDownPeriodValues, switchWindowValues)
                .CallWith(SalesChannelClient);

            (response.Header.StatusCode.Equals(HttpStatusCode.BadRequest) || response.Header.StatusCode.Equals(HttpStatusCode.NotFound))
                .Should().BeTrue();
            response.Header.Should().NotBeNull();
        }

        [Test]
        [TestCase(Labels.BudgetEnergie, TestName = "AddSalesChannelFinancial_WithBELabel_Returns200")]
        [TestCase(Labels.NLE, TestName = "AddSalesChannelFinancial_WithNLELabel_Returns200")]
        public void AddSalesChannelFinancial_CheckWithValidLabel(Labels label)
        {
            var request = GenerateAddSalesChannelFinancialRequest();
            request.SalesChannel.Label = label;

            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }
    }
}