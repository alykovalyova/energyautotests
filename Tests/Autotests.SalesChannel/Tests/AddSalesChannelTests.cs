﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class AddSalesChannelTests : SalesChannelBaseTest
    {
        [Test]
        public void AddSalesChannel_WithRetailChannel_Returns200()
        {
            var request = GenerateAddSalesChannelRequest();
            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test]
        [TestCase(Channels.D2D, TestName = "AddSalesChannel_D2dChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Intern, TestName = "AddSalesChannel_InternChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Onbekend, TestName = "AddSalesChannel_OnbekendChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Prijsvergelijker,
            TestName = "AddSalesChannel_PrijsvergelijkerChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Veiling, TestName = "AddSalesChannel_VeilingChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Website, TestName = "AddSalesChannel_WebsiteChannelsWithoutRegionAndStore_Returns200")]
        [TestCase(Channels.Wederverkoper,
            TestName = "AddSalesChannel_WederverkoperChannelsWithoutRegionAndStore_Returns200")]
        public void AddSalesChannel_ValidChannelData_Returns200(Channels channel)
        {
            var request = GenerateAddSalesChannelRequestWithoutRegionAndStore(channel);
            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test]
        [TestCase(Channels.D2D, TestName = "AddSalesChannel_D2dChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Intern, TestName = "AddSalesChannel_InternChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Onbekend, TestName = "AddSalesChannel_OnbekendChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Prijsvergelijker,
            TestName = "AddSalesChannel_PrijsvergelijkerChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Veiling, TestName = "AddSalesChannel_VeilingChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Website, TestName = "AddSalesChannel_WebsiteChannelsWithRegionAndStore_Returns400Error")]
        [TestCase(Channels.Wederverkoper,
            TestName = "AddSalesChannel_WederverkoperChannelsWithRegionAndStore_Returns400Error")]
        public void AddSalesChannel_InvalidChannelData_Returns400Error(Channels channel)
        {
            var response = GenerateAddSalesChannelRequest(channel, false)
                .CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.RegionAndStoreShouldBeEmpty);
        }

        [Test, Pairwise]
        public void AddSalesChannel_CheckEligibleAndProviderProps_Returns200(
            [Values(true, false)] bool isConsumerEligible,
            [Values(true, false)] bool? isBusinessEligible,
            [Values(true, false)] bool isContractProvider)
        {
            var request = GenerateAddSalesChannelRequest();
            request.SalesChannel.IsConsumerEligible = isConsumerEligible;
            request.SalesChannel.IsBusinessEligible = isBusinessEligible;
            request.SalesChannel.IsContractProvider = isContractProvider;

            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }

        [Test, Pairwise]
        public void AddSalesChannel_CheckInvalidNamedParamsExceptChannel_Returns400Or404Error(
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetCampaignTestData))]
            KeyValuePair<int, string> campaignValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetPartnerTestData))]
            KeyValuePair<int, string> partnerValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetRegionTestData))]
            KeyValuePair<int, string> regionValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetStoreTestData))]
            KeyValuePair<int, string> storeValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetCoolDownPeriodTestData))]
            KeyValuePair<int, int> coolDownPeriodValues,
            [ValueSource(typeof(DataProvider), nameof(DataProvider.GetSwitchWindowTestData))]
            KeyValuePair<int, int> switchWindowValues)
        {
            var response = GenerateCustomAddSalesChannelRequest(campaignValues, partnerValues, regionValues,
                    storeValues, coolDownPeriodValues, switchWindowValues)
                .CallWith(SalesChannelClient);

            (response.Header.StatusCode.Equals(HttpStatusCode.BadRequest)
             || response.Header.StatusCode.Equals(HttpStatusCode.NotFound)).Should().BeTrue();
            response.Header.Message.Should().NotBeNullOrEmpty();
        }

        [Test]
        [TestCase(Labels.BudgetEnergie, TestName = "AddSalesChannel_WithBELabel_Returns200")]
        [TestCase(Labels.NLE, TestName = "AddSalesChannel_WithNLELabel_Returns200")]
        public void AddSalesChannel_CheckWithValidLabel(Labels label)
        {
            var request = GenerateAddSalesChannelRequest();
            request.SalesChannel.Label = label;

            var response = request.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
        }
    }
}