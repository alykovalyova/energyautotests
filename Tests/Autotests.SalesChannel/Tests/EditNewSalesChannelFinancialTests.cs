﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannel;
using Nuts.SalesChannels.Model.Core.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class EditNewSalesChannelFinancialTests : SalesChannelBaseTest
    {
        [Test]
        [TestCase(SalesChannelStatus.Active,
            TestName = "EditNewSalesChannelFinancial_EditUserNameWithActiveStatus_Returns409Error")]
        [TestCase(SalesChannelStatus.Inactive,
            TestName = "EditNewSalesChannelFinancial_EditUserNameWithInactiveStatus_Returns409Error")]
        public void EditNewSalesChannelFinancial_EditUserName_Returns409Error(SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.UserName = UserName;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewFinancialWithActiveStatus
                : PatternMessages.EditNewFinancialWithInactiveStatus);
        }

        [Test]
        public void EditNewSalesChannelFinancial_EditUserNameWithNewStatus_Returns200()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.UserName = UserName;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);
            var userName = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc => sc.Id == salesChannelId).UserName;
            addRequest.UserName.Should().NotBeEquivalentTo(userName);
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditIsConsumerEligibleWithNotNewStatus_Returns409Error(
            [Values(true, false)] bool isConsumerEligible,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsConsumerEligible = isConsumerEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewFinancialWithActiveStatus
                : PatternMessages.EditNewFinancialWithInactiveStatus);
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditIsConsumerEligibleWithNewStatus_Returns200(
            [Values(true, false)] bool isConsumerEligible)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsConsumerEligible = isConsumerEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getSalesChannelByIdResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getSalesChannelByIdResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, getSalesChannelByIdResponse.Header.Message);
            getSalesChannelByIdResponse.Data.SalesChannel.IsConsumerEligible.Equals(isConsumerEligible)
                .Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditIsBusinessEligibleWithNotNewStatus_Returns409Error(
            [Values(true, false)] bool isBusinessEligible,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
           new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
           { NewStatus = status, SalesChannelId = salesChannelId }
           .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsBusinessEligible = isBusinessEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewFinancialWithActiveStatus
                : PatternMessages.EditNewFinancialWithInactiveStatus);
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditIsBusinessEligibleWithNewStatus_Returns200(
            [Values(true, false)] bool isBusinessEligible)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsBusinessEligible = isBusinessEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getSalesChannelByIdResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getSalesChannelByIdResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, getSalesChannelByIdResponse.Header.Message);
            getSalesChannelByIdResponse.Data.SalesChannel.IsBusinessEligible.Equals(isBusinessEligible)
                .Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditIsContractProviderWithNotNewStatus_Returns409Error(
            [Values(true, false)] bool isContractProvider,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                 .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsContractProvider = isContractProvider != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewFinancialWithActiveStatus
                : PatternMessages.EditNewFinancialWithInactiveStatus);
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditIsContractProviderWithNewStatus_Returns200(
            [Values(true, false)] bool isContractProvider)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsContractProvider = isContractProvider != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getSalesChannelByIdResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getSalesChannelByIdResponse.Header.StatusCode.Should()
                .BeEquivalentTo(HttpStatusCode.OK, getSalesChannelByIdResponse.Header.Message);
            getSalesChannelByIdResponse.Data.SalesChannel.IsContractProvider.Equals(isContractProvider)
                .Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditNewSalesChannelFinancial_EditNamedParamsWithNotNewStatus_Returns409Error(
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest, true);
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewFinancialWithActiveStatus
                : PatternMessages.EditNewFinancialWithInactiveStatus);
        }

        [Test]
        public void EditNewSalesChannelFinancial_EditNamedParamsWithNewStatus_Returns200()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest, true)
                .CallWith(SalesChannelClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var response = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, response.Header.Message);
            response.Data.SalesChannel.Label.Should().NotBeEquivalentTo(addRequest.SalesChannel.Label);
            response.Data.SalesChannel.SwitchWindowValue.Should().NotBe(addRequest.SalesChannel.SwitchWindow.Value);
            response.Data.SalesChannel.CampaignName.Should().NotBeEquivalentTo(addRequest.SalesChannel.Campaign.Name);
            response.Data.SalesChannel.ChannelName.Should().NotBeEquivalentTo(addRequest.SalesChannel.Channel.Name);
            response.Data.SalesChannel.PartnerName.Should().NotBeEquivalentTo(addRequest.SalesChannel.Partner.Name);
        }

        [Test]
        public void EditNewSalesChannelFinancial_EditNamedParamsWithNewStatusAndInvalidRegionAndStore_Returns400()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editResponse = GenerateEditNewSalesChannelFinancialRequest(salesChannelId, addRequest, true, false)
                .CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.RegionIsRequiredForRetail);
        }
    }
}