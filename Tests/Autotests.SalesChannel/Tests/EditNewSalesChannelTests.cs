﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannel;
using Nuts.SalesChannels.Model.Core.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class EditNewSalesChannelTests : SalesChannelBaseTest
    {
        [Test]
        [TestCase(SalesChannelStatus.Active, TestName =
            "EditNewSalesChannel_EditUserNameWithActiveStatus_Returns409Error")]
        [TestCase(SalesChannelStatus.Inactive, TestName =
            "EditNewSalesChannel_EditUserNameWithInactiveStatus_Returns409Error")]
        public void EditNewSalesChannel_EditUserName_Returns409Error(SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                    .CallWith(SalesChannelClient);
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = status, SalesChannelId = salesChannelId }
                    .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.UserName = UserName;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewWithActiveStatus
                : PatternMessages.EditNewWithInactiveStatus);
        }

        [Test]
        public void EditNewSalesChannel_EditUserNameWithNewStatus_Returns200()
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId =
                SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.UserName = UserName;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);
            var userName = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.Id == salesChannelId).UserName;
            userName.Should().NotBeEquivalentTo(addRequest.UserName);
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditIsConsumerEligibleWithNotNewStatus_Returns409Error(
            [Values(true, false)] bool isConsumerEligible,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsConsumerEligible = isConsumerEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewWithActiveStatus
                : PatternMessages.EditNewWithInactiveStatus);
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditIsConsumerEligibleWithNewStatus_Returns200(
            [Values(true, false)] bool isConsumerEligible)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsConsumerEligible = isConsumerEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, getResponse.Header.Message);
            (getResponse.Data.SalesChannel.IsConsumerEligible.Equals(isConsumerEligible)).Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditIsBusinessEligibleWithNotNewStatus_Returns409Error(
            [Values(true, false)] bool isBusinessEligible,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = status, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsBusinessEligible = isBusinessEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewWithActiveStatus
                : PatternMessages.EditNewWithInactiveStatus);
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditIsBusinessEligibleWithNewStatus_Returns200(
            [Values(true, false)] bool isBusinessEligible)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsBusinessEligible = isBusinessEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, getResponse.Header.Message);
            (getResponse.Data.SalesChannel.IsBusinessEligible.Equals(isBusinessEligible)).Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditIsContractProviderWithNotNewStatus_Returns409Error(
            [Values(true, false)] bool isContractProvider,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = status, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);

            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsContractProvider = isContractProvider != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewWithActiveStatus
                : PatternMessages.EditNewWithInactiveStatus);
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditIsContractProviderWithNewStatus_Returns200(
            [Values(true, false)] bool isContractProvider)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsContractProvider = isContractProvider != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, getResponse.Header.Message);
            (getResponse.Data.SalesChannel.IsContractProvider.Equals(isContractProvider)).Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditNewSalesChannel_EditNamedParamsWithNotNewStatus_Returns409Error(
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editResponse = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest, true)
                .CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(status == SalesChannelStatus.Active
                ? PatternMessages.EditNewWithActiveStatus
                : PatternMessages.EditNewWithInactiveStatus);
        }

        [Test]
        public void EditNewSalesChannel_EditNamedParamsWithNewStatus_Returns200()
        {
            var addRequest = GenerateAddSalesChannelRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editResponse = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest, true)
                .CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, getResponse.Header.Message);
            var patternSalesChannel = addRequest.SalesChannel;
            var createdSalesChannel = getResponse.Data.SalesChannel;
            patternSalesChannel.Label.Should().NotBeEquivalentTo(createdSalesChannel.Label);
            patternSalesChannel.SwitchWindow.Value.Should().NotBe(createdSalesChannel.SwitchWindowValue);
            patternSalesChannel.Campaign.Name.Should().NotBeEquivalentTo(createdSalesChannel.CampaignName);
            patternSalesChannel.Channel.Name.Should().NotBeEquivalentTo(createdSalesChannel.ChannelName);
            patternSalesChannel.Partner.Name.Should().NotBeEquivalentTo(createdSalesChannel.PartnerName);
        }

        [Test]
        public void EditNewSalesChannel_EditNamedParamsWithNewStatusAndInvalidRegionAndStore_Returns400()
        {
            var addRequest = GenerateAddSalesChannelRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editResponse = GenerateEditNewSalesChannelRequest(salesChannelId, addRequest, true, false)
                .CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.RegionIsRequiredForRetail);
        }
    }
}