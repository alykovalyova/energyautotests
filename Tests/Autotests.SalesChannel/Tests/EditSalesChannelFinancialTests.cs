﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannel;
using Nuts.SalesChannels.Model.Core.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class EditSalesChannelFinancialTests : SalesChannelBaseTest
    {
        [Test]
        [TestCase(SalesChannelStatus.Active,
            TestName = "EditSalesChannelFinancial_EditUserNameWithActiveStatus_Returns200")]
        [TestCase(SalesChannelStatus.Inactive,
            TestName = "EditSalesChannelFinancial_EditUserNameWithInactiveStatus_Returns200")]
        public void EditSalesChannelFinancial_EditUserName_Returns200(SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.UserName = UserName;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK, editResponse.Header.Message);
            var userName = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.Id == salesChannelId).UserName;
            addRequest.UserName.Should().NotBeEquivalentTo(userName);
        }

        [Test]
        public void EditSalesChannelFinancial_EditUserNameWithNewStatus_Returns409Error()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.UserName = UserName;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.EditFinancialWithWrongStatus);
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditIsConsumerEligibleWithNotNewStatus_Returns200(
            [Values(true, false)] bool isConsumerEligible,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsConsumerEligible = isConsumerEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            (getResponse.Data.SalesChannel.IsConsumerEligible.Equals(isConsumerEligible)).Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditIsConsumerEligibleWithNewStatus_Returns409Error(
            [Values(true, false)] bool isConsumerEligible)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsConsumerEligible = isConsumerEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.EditFinancialWithWrongStatus);
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditIsBusinessEligibleWithNotNewStatus_Returns200(
            [Values(true, false)] bool isBusinessEligible,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsBusinessEligible = isBusinessEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            (getResponse.Data.SalesChannel.IsBusinessEligible.Equals(isBusinessEligible)).Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditIsBusinessEligibleWithNewStatus_Returns409Error(
            [Values(true, false)] bool isBusinessEligible)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsBusinessEligible = isBusinessEligible != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.EditFinancialWithWrongStatus);
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditIsContractProviderWithNotNewStatus_Returns200(
            [Values(true, false)] bool isContractProvider,
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsContractProvider = isContractProvider != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            (getResponse.Data.SalesChannel.IsContractProvider.Equals(isContractProvider)).Should().BeFalse();
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditIsContractProviderWithNewStatus_Returns409Error(
            [Values(true, false)] bool isContractProvider)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editRequest = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest);
            editRequest.SalesChannel.IsContractProvider = isContractProvider != true;
            var editResponse = editRequest.CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.EditFinancialWithWrongStatus);
        }

        [Test, Pairwise]
        public void EditSalesChannelFinancial_EditNamedParamsWithNotNewStatus_Returns200(
            [Values(SalesChannelStatus.Active, SalesChannelStatus.Inactive)]
            SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
            { NewStatus = status, SalesChannelId = salesChannelId }
            .CallWith(SalesChannelClient);

            var editResponse = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest, true)
                .CallWith(SalesChannelClient);
            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            addRequest.SalesChannel.Channel.Name.Should().NotBeEquivalentTo(getResponse.Data.SalesChannel.ChannelName);
            addRequest.SalesChannel.Partner.Name.Should().NotBeEquivalentTo(getResponse.Data.SalesChannel.PartnerName);
        }

        [Test]
        public void EditSalesChannelFinancial_EditNamedParamsWithNewStatus_Returns409()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest(Channels.Intern);
            addRequest.CallWith(SalesChannelClient);

            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            var editResponse = GenerateEditSalesChannelFinancialRequest(salesChannelId, addRequest, true)
                .CallWith(SalesChannelClient);

            editResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.Conflict);
            editResponse.Header.Message.Should().BeEquivalentTo(PatternMessages.EditFinancialWithWrongStatus);
        }
    }
}