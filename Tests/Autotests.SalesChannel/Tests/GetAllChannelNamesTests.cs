﻿using System.Net;
using Autotests.Clients;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetAllChannelNames;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class GetAllChannelNamesTests : SalesChannelBaseTest
    {
        [Test]
        public void GetAllChannelNames_Valid_ApiCountMatchesDbCount()
        {
            var response = new GetAllChannelNamesRequest()
                .CallWith<GetAllChannelNamesRequest, GetAllChannelNamesResponse>(SalesChannelClient);
            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);

            var salesChannelCount = SalesChannelDb.GetEntitiesByCondition<Repositories.SalesChannelsModels.SalesChannel>(x => true).Count;
            response.Data.ChannelNames.Should().HaveCount(salesChannelCount);
        }
    }
}