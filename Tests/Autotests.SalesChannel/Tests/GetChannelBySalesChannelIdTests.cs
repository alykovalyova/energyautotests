﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetChannelBySalesChannelId;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class GetChannelBySalesChannelIdTests : SalesChannelBaseTest
    {
        [Test]
        public void GetChannelBySalesChannelId_WithValidSalesChannelId_Returns200()
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId =
                SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc => sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;

            var response = new GetChannelBySalesChannelIdRequest { SalesChannelId = salesChannelId }
                .CallWith<GetChannelBySalesChannelIdRequest, GetChannelBySalesChannelIdResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.Channel.Id.Should().Be(addRequest.SalesChannel.Channel.Id);
            response.Data.Channel.Name.Should().BeEquivalentTo(addRequest.SalesChannel.Channel.Name);
        }

        [Test]
        public void GetChannelBySalesChannelId_WithInvalidSalesChannelId_Returns400Error()
        {
            var response = new GetChannelBySalesChannelIdRequest { SalesChannelId = 0 }
                .CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidRangeOfSalesChannelId);
        }

        [Test]
        public void GetChannelBySalesChannelId_WithInExistentSalesChannelId_Returns404Error()
        {
            new GetChannelBySalesChannelIdRequest { SalesChannelId = 30000 }
                .CallWith(SalesChannelClient)
                .Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
        }
    }
}