﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetDictionaryItems;
using Nuts.SalesChannels.Model.Core.Enums;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class GetDictionaryItemsTests : SalesChannelBaseTest
    {
        [Test]
        public void GetDictionaryItems_ByDictionaryNameOnly_Returns200([Values(
            SalesChannelDictionary.Channel, SalesChannelDictionary.Campaign,
            SalesChannelDictionary.CooldownPeriod, SalesChannelDictionary.Partner,
            SalesChannelDictionary.Region, SalesChannelDictionary.Store,
            SalesChannelDictionary.SwitchWindow)] SalesChannelDictionary dictionary)
        {
            var response = new GetDictionaryItemsRequest { DictionaryName = dictionary }
                .CallWith<GetDictionaryItemsRequest, GetDictionaryItemsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannelAuxiliaries.Should().NotBeEmpty();
        }

        [Test]
        public void GetDictionaryItems_WithoutAnyParams_Returns400Error()
        {
            var response = new GetDictionaryItemsRequest()
                .CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.WrongDictionaryNameValue);
        }

        [Test]
        public void GetDictionaryItems_ByDictionaryNameAndSearchText_Returns200()
        {
            var response = new GetDictionaryItemsRequest
            {
                DictionaryName = SalesChannelDictionary.Channel,
                SearchText = "e"
            }.CallWith<GetDictionaryItemsRequest, GetDictionaryItemsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannelAuxiliaries.Should().NotBeEmpty();
        }

        [Test]
        public void GetDictionaryItems_ByDictionaryNameAndSearchTextWithLimit_Returns200([Values(0, 2)] int limit)
        {
            var response = new GetDictionaryItemsRequest
            {
                DictionaryName = SalesChannelDictionary.Channel,
                SearchText = "e",
                MaxReturnValues = limit
            }.CallWith<GetDictionaryItemsRequest, GetDictionaryItemsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            limit.Equals(2).IfTrue(() => response.Data.SalesChannelAuxiliaries.Should().HaveCountLessOrEqualTo(2));
            limit.Equals(0).IfTrue(() => response.Data.SalesChannelAuxiliaries.Should().HaveCount(0));
        }
    }
}