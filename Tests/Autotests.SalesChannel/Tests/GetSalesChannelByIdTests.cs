﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannel;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class GetSalesChannelByIdTests : SalesChannelBaseTest
    {
        [Test]
        public void GetSalesChannelById_GetByNewAddedSalesChannelId_ReturnsAddedSalesChannel()
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);

            var response = new GetSalesChannelByIdRequest
            {
                SalesChannelId = SalesChannelDb.GetMaxEntity<Repositories.SalesChannelsModels.SalesChannel, int>(sc => sc.Id)
            }.CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            var expectedSalesChannel = addRequest.SalesChannel;
            var actualSalesChannel = response.Data.SalesChannel;
            expectedSalesChannel.ExternalReference.Should().BeEquivalentTo(actualSalesChannel.ExternalReference);
            expectedSalesChannel.Campaign.Name.Should().BeEquivalentTo(actualSalesChannel.CampaignName);
            expectedSalesChannel.Channel.Name.Should().BeEquivalentTo(actualSalesChannel.ChannelName);
            expectedSalesChannel.Label.Should().BeEquivalentTo(actualSalesChannel.Label);
            expectedSalesChannel.Partner.Name.Should().BeEquivalentTo(actualSalesChannel.PartnerName);
            expectedSalesChannel.Region.Name.Should().BeEquivalentTo(actualSalesChannel.RegionName);
            expectedSalesChannel.Store.Name.Should().BeEquivalentTo(actualSalesChannel.StoreName);
        }

        [Test]
        public void GetSalesChannelById_WithInvalidSalesChannelId_Returns400Error([Values(-1, 0)] int salesChannelId)
        {
            var response = salesChannelId == -1
                ? new GetSalesChannelByIdRequest().CallWith(SalesChannelClient)
                : new GetSalesChannelByIdRequest { SalesChannelId = 0 }.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidSalesChannelId);
        }
    }
}