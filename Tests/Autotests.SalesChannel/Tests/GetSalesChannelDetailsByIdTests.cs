﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannelDetails;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class GetSalesChannelDetailsByIdTests : SalesChannelBaseTest
    {
        [Test]
        public void GetSalesChannelDetailsById_WithValidSalesChannelId_Returns200()
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;

            var response = new GetSalesChannelDetailsByIdRequest { SalesChannelId = salesChannelId }
                .CallWith<GetSalesChannelDetailsByIdRequest, GetSalesChannelDetailsByIdResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannelDetails.Should().NotBeNull();
        }

        [Test]
        public void GetSalesChannelDetailsById_WithInvalidSalesChannelId_Returns400Error([Values(-1, 0)] int salesChannelId)
        {
            var response = salesChannelId == -1
                ? new GetSalesChannelDetailsByIdRequest().CallWith(SalesChannelClient)
                : new GetSalesChannelDetailsByIdRequest { SalesChannelId = 0 }.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidSalesChannelId);
        }
    }
}