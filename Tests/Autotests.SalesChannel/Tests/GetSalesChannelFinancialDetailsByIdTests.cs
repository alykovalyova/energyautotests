﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannelDetails;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class GetSalesChannelFinancialDetailsByIdTests : SalesChannelBaseTest
    {
        [Test]
        public void GetSalesChannelFinancialDetailsById_WithValidSalesChannelId_Returns200()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;

            var response = new GetSalesChannelFinancialDetailsByIdRequest { SalesChannelId = salesChannelId }
                .CallWith<GetSalesChannelFinancialDetailsByIdRequest, GetSalesChannelFinancialDetailsByIdResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannelDetails.Should().NotBeNull();
        }

        [Test]
        public void GetSalesChannelFinancialDetailsById_WithInvalidSalesChannelId_Returns400Error()
        {
            var response = new GetSalesChannelFinancialDetailsByIdRequest()
                .CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidSalesChannelIdFinancial);
        }
    }
}