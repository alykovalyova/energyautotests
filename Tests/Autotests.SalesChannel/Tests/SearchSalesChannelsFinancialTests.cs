﻿using System.Net;
using Autotests.Clients;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract.SearchSalesChannel;
using Nuts.SalesChannels.Model.Core.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class SearchSalesChannelsFinancialTests : SalesChannelBaseTest
    {
        [Test]
        public void SearchSalesChannelsFinancial_EmptyRequest_ReturnedArrayIsNotEmpty()
        {
            var response = new SearchSalesChannelsFinancialRequest { SkipPaidCreditCheck = true }
                .CallWith<SearchSalesChannelsFinancialRequest, SearchSalesChannelsFinancialResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
        }

        [Test]
        public void SearchSalesChannelsFinancial_BySalesChannelIdOnly_ReturnsOneEntity()
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsFinancialRequest
            {
                SalesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id,
                SkipPaidCreditCheck = true
            }.CallWith<SearchSalesChannelsFinancialRequest, SearchSalesChannelsFinancialResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().HaveCount(1);
            response.Data.SalesChannels.All(sc => sc.SkipPaidCreditCheck.Equals(true)).Should().BeTrue();
        }

        [Test]
        [TestCase(Channels.Retail, TestName = "SearchSalesChannelsFinancial_WithRetailChannel_ReturnsManyEntities")]
        [TestCase(Channels.D2D, TestName = "SearchSalesChannelsFinancial_WithD2DChannel_ReturnsManyEntities")]
        [TestCase(Channels.Intern, TestName = "SearchSalesChannelsFinancial_WithInternChannel_ReturnsManyEntities")]
        [TestCase(Channels.Onbekend, TestName = "SearchSalesChannelsFinancial_WithOnbekendChannel_ReturnsManyEntities")]
        [TestCase(Channels.Prijsvergelijker,
            TestName = "SearchSalesChannelsFinancial_WithPrijsvergelijkerChannel_ReturnsManyEntities")]
        [TestCase(Channels.Veiling, TestName = "SearchSalesChannelsFinancial_WithVeilingChannel_ReturnsManyEntities")]
        [TestCase(Channels.Website, TestName = "SearchSalesChannelsFinancial_WithWebsiteChannel_ReturnsManyEntities")]
        [TestCase(Channels.Wederverkoper,
            TestName = "SearchSalesChannelsFinancial_WithWederverkoperChannel_ReturnsManyEntities")]
        public void SearchSalesChannelsFinancial_ByChannelNameOnly_ReturnsManyEntities(Channels channel)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest(channel);
            if (channel != Channels.Retail)
            {
                addRequest.SalesChannel.Region = null;
                addRequest.SalesChannel.Store = null;
            }

            addRequest.CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsFinancialRequest
            {
                ChannelName = channel.ToString(),
                SkipPaidCreditCheck = true
            }.CallWith<SearchSalesChannelsFinancialRequest, SearchSalesChannelsFinancialResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels.Should().Contain(sc => sc.ChannelName.Equals(channel.ToString()));
            response.Data.SalesChannels.Should().Contain(sc => sc.SkipPaidCreditCheck.Equals(true));
        }

        [Test]
        [TestCase(SalesChannelStatus.New, TestName = "SearchSalesChannelsFinancial_WithStatusNew_ReturnsManyEntities")]
        [TestCase(SalesChannelStatus.Active,
            TestName = "SearchSalesChannelsFinancial_WithStatusActive_ReturnsManyEntities")]
        [TestCase(SalesChannelStatus.Inactive,
            TestName = "SearchSalesChannelsFinancial_WithStatusInactive_ReturnsManyEntities")]
        public void SearchSalesChannelsFinancial_ByStatusOnly_ReturnsManyEntities(SalesChannelStatus status)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;
            if (status == SalesChannelStatus.Inactive)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = SalesChannelStatus.Active, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);
            if (status != SalesChannelStatus.New)
                new Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus.UpdateSalesChannelStatusRequest()
                { NewStatus = status, SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsFinancialRequest
            {
                Status = status,
                SkipPaidCreditCheck = true
            }.CallWith<SearchSalesChannelsFinancialRequest, SearchSalesChannelsFinancialResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels.Should().Contain(sc => sc.Status.Equals(status));
            response.Data.SalesChannels.Should().Contain(sc => sc.SkipPaidCreditCheck.Equals(true));
        }

        [Test]
        public void SearchSalesChannelsFinancial_WithBoolParams_ReturnsManyEntities(
            [Values(true, false)] bool isConsumerEligible,
            [Values(true, false)] bool isBusinessEligible)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsFinancialRequest
            {
                IsConsumerEligible = isConsumerEligible,
                IsBusinessEligible = isBusinessEligible,
                SkipPaidCreditCheck = true
            }.CallWith<SearchSalesChannelsFinancialRequest, SearchSalesChannelsFinancialResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels.All(sc =>
                    sc.IsConsumerEligible.Equals(isConsumerEligible) &&
                    sc.IsBusinessEligible.Equals(isBusinessEligible))
                .Should().BeTrue();
            response.Data.SalesChannels.All(sc => sc.SkipPaidCreditCheck.Equals(true)).Should().BeTrue();
        }

        [Test]
        [TestCase(Labels.BudgetEnergie, TestName = "SearchSalesChannelsFinancial_WithBeLabel_ReturnsManyEntities")]
        [TestCase(Labels.NLE, TestName = "SearchSalesChannelsFinancial_WithNleLabel_ReturnsManyEntities")]
        public void SearchSalesChannelsFinancial_ByLabelOnly_ReturnsManyEntities(Labels label)
        {
            var addRequest = GenerateAddSalesChannelFinancialRequest();
            addRequest.SalesChannel.Label = label;
            addRequest.CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsFinancialRequest
            {
                LabelCode = label,
                SkipPaidCreditCheck = true
            }.CallWith<SearchSalesChannelsFinancialRequest, SearchSalesChannelsFinancialResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels.Should().Contain(sc => sc.Label.Equals(label));
            response.Data.SalesChannels.Should().Contain(sc => sc.SkipPaidCreditCheck.Equals(true));
        }
    }
}