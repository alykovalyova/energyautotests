﻿using System.Net;
using Autotests.Clients;
using Autotests.Helper.Enums.SalesChannel;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.SalesChannels.Model.Contract.SearchSalesChannel;
using Nuts.SalesChannels.Model.Core.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class SearchSalesChannelsTests : SalesChannelBaseTest
    {
        [Test]
        public void SearchSalesChannels_EmptyRequest_ReturnedArrayIsNotEmpty()
        {
            var response = new SearchSalesChannelsRequest()
                .CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
        }

        [Test]
        public void SearchSalesChannels_BySalesChannelIdOnly_ReturnsOneEntity()
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsRequest
            {
                SalesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id
            }.CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().HaveCount(1);
        }

        [Test]
        [TestCase(Channels.Retail, TestName = "SearchSalesChannels_WithRetailChannel_ReturnsManyEntities")]
        [TestCase(Channels.D2D, TestName = "SearchSalesChannels_WithD2DChannel_ReturnsManyEntities")]
        [TestCase(Channels.Intern, TestName = "SearchSalesChannels_WithInternChannel_ReturnsManyEntities")]
        [TestCase(Channels.Onbekend, TestName = "SearchSalesChannels_WithOnbekendChannel_ReturnsManyEntities")]
        [TestCase(Channels.Prijsvergelijker, TestName = "SearchSalesChannels_WithPrijsvergelijkerChannel_ReturnsManyEntities")]
        [TestCase(Channels.Veiling, TestName = "SearchSalesChannels_WithVeilingChannel_ReturnsManyEntities")]
        [TestCase(Channels.Website, TestName = "SearchSalesChannels_WithWebsiteChannel_ReturnsManyEntities")]
        [TestCase(Channels.Wederverkoper, TestName = "SearchSalesChannels_WithWederverkoperChannel_ReturnsManyEntities")]
        public void SearchSalesChannels_ByChannelNameOnly_ReturnsManyEntities(Channels channel)
        {
            var response = new SearchSalesChannelsRequest { ChannelName = channel.ToString() }
                .CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().HaveCountGreaterThan(1);
            response.Data.SalesChannels.All(sc => sc.ChannelName.Equals(channel.ToString())).Should().BeTrue();
        }

        [Test]
        [TestCase(SalesChannelStatus.New, TestName = "SearchSalesChannels_WithStatusNew_ReturnsManyEntities")]
        [TestCase(SalesChannelStatus.Active, TestName = "SearchSalesChannels_WithStatusActive_ReturnsManyEntities")]
        [TestCase(SalesChannelStatus.Inactive, TestName = "SearchSalesChannels_WithStatusInactive_ReturnsManyEntities")]
        public void SearchSalesChannels_ByStatusOnly_ReturnsManyEntities(SalesChannelStatus status)
        {
            CreateSalesChannel(status);
            var response = new SearchSalesChannelsRequest { Status = status }
                .CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels.All(sc => sc.Status.Equals(status)).Should().BeTrue();
        }

        [Test]
        public void SearchSalesChannels_WithBoolParams_ReturnsManyEntities(
            [Values(true, false)] bool isConsumerEligible,
            [Values(true, false)] bool isBusinessEligible,
            [Values(true, false)] bool isContractProvider)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.SalesChannel.IsBusinessEligible = isBusinessEligible;
            addRequest.SalesChannel.IsConsumerEligible = isConsumerEligible;
            addRequest.SalesChannel.IsContractProvider = isContractProvider;
            addRequest.CallWith(SalesChannelClient);

            var response = new SearchSalesChannelsRequest
            {
                IsConsumerEligible = isConsumerEligible,
                IsBusinessEligible = isBusinessEligible,
                IsContractProvider = isContractProvider
            }.CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().NotBeEmpty();
            response.Data.SalesChannels
                .All(sc =>
                    sc.IsConsumerEligible.Equals(isConsumerEligible) &&
                    sc.IsBusinessEligible.Equals(isBusinessEligible) &&
                    sc.IsContractProvider.Equals(isContractProvider)).Should().BeTrue();
        }

        [Test]
        [TestCase(Labels.BudgetEnergie, TestName = "SearchSalesChannels_WithBeLabel_ReturnsManyEntities")]
        [TestCase(Labels.NLE, TestName = "SearchSalesChannels_WithNleLabel_ReturnsManyEntities")]
        public void SearchSalesChannels_ByLabelOnly_ReturnsManyEntities(Labels label)
        {
            var response = new SearchSalesChannelsRequest { LabelCode = label }
                .CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            response.Data.SalesChannels.Should().HaveCountGreaterThan(1);
            response.Data.SalesChannels.All(sc => sc.Label.Equals(label)).Should().BeTrue();
        }

        [Test]
        public void SearchSalesChannels_WithRetailAndActiveStatus_ApiCountMatchesDbCount()
        {
            var response = new SearchSalesChannelsRequest
            {
                Status = SalesChannelStatus.Active,
                ChannelName = "Retail"
            }.CallWith<SearchSalesChannelsRequest, SearchSalesChannelsResponse>(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            var dbSalesChannel = SalesChannelDb.GetEntitiesByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.StatusId == (int)SalesChannelStatus.Active && sc.Channel.Name == "Retail");
            response.Data.SalesChannels.Should().HaveCount(dbSalesChannel.Count);
        }
    }
}