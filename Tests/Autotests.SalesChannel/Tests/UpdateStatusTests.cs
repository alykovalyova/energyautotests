﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.SalesChannel.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.SalesChannels.Model.Contract.GetSalesChannel;
using Nuts.SalesChannels.Model.Contract.UpdateSalesChannelStatus;
using Nuts.SalesChannels.Model.Core.Enums;
using static Autotests.SalesChannel.Factory.SalesChannelFactory;

namespace Autotests.SalesChannel.Tests
{
    [TestFixture]
    internal class UpdateStatusTests : SalesChannelBaseTest
    {
        [Test]
        [TestCase(SalesChannelStatus.Active, HttpStatusCode.OK,
            TestName = "UpdateStatus_UpdateStatusFromNewToActive_Returns200")]
        [TestCase(SalesChannelStatus.Inactive, HttpStatusCode.Conflict,
            TestName = "UpdateStatus_UpdateStatusFromNewToInactive_Returns409")]
        public void UpdateStatus_UpdateStatus_Returns200(SalesChannelStatus status, HttpStatusCode statusCode)
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb
                .GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                    sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;

            var response = new UpdateSalesChannelStatusRequest
            {
                SalesChannelId = salesChannelId,
                NewStatus = status
            }.CallWith(SalesChannelClient);
            response.Header.StatusCode.Should().BeEquivalentTo(statusCode);

            if (statusCode != HttpStatusCode.OK) return;
            var getResponse = new GetSalesChannelByIdRequest { SalesChannelId = salesChannelId }
            .CallWith<GetSalesChannelByIdRequest, GetSalesChannelByIdResponse>(SalesChannelClient);
            getResponse.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.OK);
            getResponse.Data.SalesChannel.Status.Should().BeEquivalentTo(status);
        }

        [Test]
        public void UpdateStatus_WithInvalidSalesChannelId_Returns404Error()
        {
            var response = new UpdateSalesChannelStatusRequest
            {
                SalesChannelId = 0,
                NewStatus = SalesChannelStatus.Active
            }.CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.NotFound);
            response.Header.Message.Should().BeEquivalentTo(string.Format(PatternMessages.SalesChannelNotFound, 0));
        }

        [Test]
        public void UpdateStatus_WithoutStatus_Returns400Error()
        {
            var addRequest = GenerateAddSalesChannelRequest();
            addRequest.CallWith(SalesChannelClient);
            var salesChannelId = SalesChannelDb.GetEntityByCondition<Repositories.SalesChannelsModels.SalesChannel>(sc =>
                sc.OldReferenceId == addRequest.SalesChannel.ExternalReference).Id;

            var response = new UpdateSalesChannelStatusRequest { SalesChannelId = salesChannelId }
                .CallWith(SalesChannelClient);

            response.Header.StatusCode.Should().BeEquivalentTo(HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.WrongNewStatusFieldValue);
        }
    }
}