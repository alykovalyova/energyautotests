﻿using System.Linq.Expressions;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Clients.Enums.TimeLine;
using Autotests.Clients.Rest.Models.MongoDb;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.Sql;
using Autotests.Core.Sql.Entities;
using Autotests.Core.TestDataConfiguration;
using Autotests.Repositories.EnerFreeDBModels;
using Autotests.Repositories.MeteringPointModels;
using Autotests.Repositories.P4DailyReadingsModels;
using Autotests.UsageCalc.Models;
using NUnit.Framework;
using Nuts.InterDom.Model.Core.Enums;
using Nuts.UsageCalculation.Contract.Rest.Transport;
using Mp = Autotests.Repositories.MeteringPointModels.MeteringPoint;
using MeteringPoint = Nuts.P4DailyReadings.Contract.MeteringPoint;
using MeterReading = Autotests.Repositories.P4DailyReadingsModels.MeterReading;
using RegisterReading = Autotests.Repositories.P4DailyReadingsModels.RegisterReading;
using MongoDB.Driver;
using Nuts.P4DailyReadings.Contract;
using EdsnFraction = Autotests.Repositories.EnerFreeDBModels.EdsnFraction;

namespace Autotests.UsageCalc.Base
{
    [SetUpFixture, Category(Categories.UsageCalculation)]
    internal abstract class BaseUsageCalculation : AllureReport
    {
        //clients declaration
        protected NutsHttpClient UsageCalculationClient;
        protected BaseHttpClient UsageCalculationHttpClient;
        protected NutsHttpClient P4DailyClient;
        protected MongoClient MongoClient;

        //databases declaration
        protected DbHandler<EnerFreeDBContext> EnerFreeDb;
        protected DbHandler<MeteringPointContext> MeteringPointDb;
        protected DbHandler<P4DailyReadingsContext> P4DailyReadingsDb;

        //variables declaration
        protected int ContractId;
        protected string ElkEanId;
        protected string GasEanId;
        protected static IMongoDatabase UsageCalculationDb;
        protected static string DbName;
        protected DateTime Today = DateTime.Today;
        protected ConfigurationService<Core.TestDataConfiguration.UsageCalculation> TestVariablesConfig;
        private ConfigHandler _configHandler = new ConfigHandler();
        readonly Expression<Func<MeterReading, int>> _outerKey = mr => mr.Id;
        readonly Expression<Func<RegisterReading, int>> _innerKey = rr => rr.MeterReadingId;
        readonly Expression<Func<MeterReading, RegisterReading, JoinSelector<MeterReading, RegisterReading>>> _resultSelector
            = (o, i) => new JoinSelector<MeterReading, RegisterReading> { Outer = o, Inner = i };

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            //UsageCalculationHttpClient = new BaseHttpClient(_configHandler.ApiUrls.UsageCalculation);
            UsageCalculationClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.UsageCalculation));
            P4DailyClient = new NutsHttpClient(new BaseHttpClient(_configHandler.ApiUrls.P4Daily));
            DbName = _configHandler.MongoDbSettings.DbName;
            MongoClient = new MongoClient(_configHandler.MongoDbSettings.ConnectionString);
            UsageCalculationDb = MongoClient.GetDatabase(DbName);
            EnerFreeDb = new DbHandler<EnerFreeDBContext>(_configHandler.GetConnectionString(DbConnectionName.EnerFreeDBDatabase.ToString()));
            MeteringPointDb = new DbHandler<MeteringPointContext>(_configHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
            P4DailyReadingsDb = new DbHandler<P4DailyReadingsContext>(_configHandler.GetConnectionString(DbConnectionName.P4DailyReadingsDatabase.ToString()));

            TestVariablesConfig = new ConfigurationService<Core.TestDataConfiguration.UsageCalculation>();
            ContractId = TestVariablesConfig.GetDefaultValues(Section.UsageCalculation.ToString()).ContractId;
            ElkEanId = SqlQueryExecutor.Instance.ConnectTo("EnerFreeDBDatabase").
                ExecuteSqlQuery<EnerFreeEan>("GetEanByContractId", condition: ContractId.ToString()).FirstOrDefault(ean => ean.ProductType == (int)EnerFreeProductType.Elk)
                ?.EanId;
            GasEanId = SqlQueryExecutor.Instance.ConnectTo("EnerFreeDBDatabase").
                ExecuteSqlQuery<EnerFreeEan>("GetEanByContractId", condition: ContractId.ToString()).FirstOrDefault(ean => ean.ProductType == (int)EnerFreeProductType.Gas)
                ?.EanId;
            UpdateMandate();
            new GetBalanceRequest { ContractId = ContractId }.CallWith<GetBalanceRequest, GetBalanceResponse>(UsageCalculationClient);
        }
        
        protected void UpdateMandate()
        {
            var eans = new List<string> { GasEanId, ElkEanId };
            foreach (var ean in eans)
            {
                var mp = P4DailyReadingsDb.GetEntityByCondition<Repositories.P4DailyReadingsModels.MeteringPoint>(m =>
                m.EanId == ean, "GridOperator");

                var request = new UpsertMandateRequest()
                {
                    MandateEnabled = true,
                    MeteringPoints = new List<MeteringPoint>()
                    {
                        new MeteringPoint()
                        {
                            EanId = mp.EanId,
                            GridOperator = mp.GridOperator.EanId,
                            BalanceSupplier = mp.BalanceSupplier,
                            ProductType = ((ProductType) mp.ProductTypeId).ToString()
                        }
                    }
                };

                var response = P4DailyClient.MakeRequest(request);
            }
        }

    protected TariffPeriods GetActualTariff(DateTime dateFrom)
        {
            var currentTariffs = UsageCalculationDb.GetCollection<Balance>("Balance").AsQueryable();

            var tariff = currentTariffs.FirstOrDefault(c => c.ContractId == ContractId)
                    ?.CalculationData.ConnectionCosts.FirstOrDefault(ean =>
                ean.EanId == ElkEanId)?.TariffPeriods.FirstOrDefault(tp => tp.From <= dateFrom && tp.To >= dateFrom);

            return tariff ?? new TariffPeriods { PriceUsagePerUnit = 0, PriceNonUsagePerDay = 0 };
        }

        protected TariffPeriods GetLastTariff()
        {
            var currentTariffs = UsageCalculationDb.GetCollection<Balance>("Balance").AsQueryable();

            var tariff = currentTariffs.FirstOrDefault(c => c.ContractId == ContractId)
                ?.CalculationData.ConnectionCosts.First(ean =>
                    ean.EanId == ElkEanId)?.TariffPeriods.OrderByDescending(o => o.To).FirstOrDefault();
            
            return tariff ?? new TariffPeriods { PriceUsagePerUnit = 0, PriceNonUsagePerDay = 0 };
        }

        protected FilterDefinition<Models.EdsnFraction> GetDbQueryFiltersForConsumption(EnergyUsageProfileCode profileCodeId, bool isProduction, DateTime fromDate,
           DateTime? toDate = null)
        {
            var filterDefinition = new FilterDefinitionBuilder<Models.EdsnFraction>();

            if (toDate.Equals(null))
            {
                return filterDefinition.Where(x => x.IsProduction == isProduction & x.ProfileCode == profileCodeId.ToString()
                & x.FractionDate == fromDate);              
            }

            return filterDefinition.Where(x => x.IsProduction == isProduction & x.ProfileCode == profileCodeId.ToString()
                & x.FractionDate >= fromDate & x.FractionDate <= toDate);
        }

        protected FilterDefinition<Models.EdsnFraction> GetDbQueryFiltersForProduction(bool isProduction, DateTime fromDate,
           DateTime? toDate = null)
        {
            var filterDefinition = new FilterDefinitionBuilder<Models.EdsnFraction>();

            if (toDate.Equals(null))
            {
                return filterDefinition.Where(x => x.IsProduction == isProduction & x.FractionDate == fromDate);
            }

            return filterDefinition.Where(x => x.IsProduction == isProduction & x.FractionDate >= fromDate & x.FractionDate <= toDate);
        }

        protected Dictionary<DateTime, decimal> GetFractionValues(FilterDefinition<Models.EdsnFraction> filters)
        {
            var fractionValues = new Dictionary<DateTime, decimal>();
            List<Models.EdsnFraction> edsnFractionBsonDocs;
            var edsnFractions = UsageCalculationDb.GetCollection<Models.EdsnFraction>("EdsnFraction");
           
            edsnFractionBsonDocs = edsnFractions.Find(filters).ToList();

            foreach (var fract in edsnFractionBsonDocs)
            {
                fractionValues.Add(fract.FractionDate.ToLocalTime(),
                    fract.FractionValue);
            }

            return fractionValues;
        }

        protected decimal GetEstimatedCosts(Mp meteringPoint,
             decimal priceNonUsagePerDay, decimal priceUsagePerUnit, DateTime fromDate, DateTime? toDate = null)
        {
            var consFilters = GetDbQueryFiltersForConsumption((EnergyUsageProfileCode)meteringPoint.ProfileCategoryId, false,
                fromDate, toDate);
            var prodFilters = GetDbQueryFiltersForProduction(true, fromDate, toDate);
            var consFractions = GetFractionValues(consFilters);
            var prodFractions = GetFractionValues(prodFilters);

            var consUsage = new Dictionary<DateTime, decimal>();
            var prodUsage = new Dictionary<DateTime, decimal>();
            var totalUsagePerPeriod = new Dictionary<DateTime, decimal>();
            var costPerDay = new Dictionary<DateTime, decimal>();

            foreach (var p in prodFractions)
            {
                prodUsage.Add(p.Key, decimal.Round(p.Value * (meteringPoint.EapPeak.GetValueOrDefault() +
                                                              meteringPoint.EapOffPeak.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero));
            }

            foreach (var c in consFractions)
            {
                consUsage.Add(c.Key, decimal.Round(c.Value * (meteringPoint.EacPeak.GetValueOrDefault() +
                                                              meteringPoint.EacOffPeak.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero));
            }

            foreach (var y in consUsage)
            {
                var value = prodUsage.Where(k => k.Key == y.Key).Select(s => s.Value).FirstOrDefault();
                totalUsagePerPeriod.Add(y.Key, y.Value - value);
            }

            foreach (var total in totalUsagePerPeriod)
            {
                costPerDay.Add(total.Key, (decimal.Round(((total.Value * priceUsagePerUnit) + priceNonUsagePerDay), 2, MidpointRounding.AwayFromZero)));
            }

            return costPerDay.Sum(k => k.Value);
        }

        protected int CalculateActualUsage(Mp meteringPoint, DateTime fromDate, DateTime toDate)
        {
            var initialRegisterReading = P4DailyReadingsDb.GetEntitiesByConditionWithJoin(_outerKey, _innerKey,
                    _resultSelector, join => join.Outer.EanId == meteringPoint.EanId && join.Outer.ReadingDate == fromDate)
                .OrderByDescending(x => x.Outer.ReadingDate)
                .Select(s => s.Inner).Take(4).ToList();
            var finalRegisterReading = P4DailyReadingsDb.GetEntitiesByConditionWithJoin(_outerKey, _innerKey,
                    _resultSelector, join => join.Outer.EanId == meteringPoint.EanId && join.Outer.ReadingDate == toDate)
                .OrderByDescending(x => x.Outer.ReadingDate)
                .Select(s => s.Inner).Take(4).ToList();

            var usagePerRegister = new Dictionary<string, int>();
            var consUsage = new List<int>();
            var prodUsage = new List<int>();

            foreach (var initReg in initialRegisterReading)
            {
                var resultUsage = 0;
                if (initialRegisterReading.Count == 0 || finalRegisterReading.Count == 0)
                    resultUsage = 0;
                else
                {
                    var usage = finalRegisterReading.Where(fr =>
                        fr.MeteringDirectionId == initReg.MeteringDirectionId &&
                        fr.TariffTypeId == initReg.TariffTypeId).Select(fr => fr.Value).FirstOrDefault();
                    resultUsage = usage - initReg.Value;
                }

                usagePerRegister.Add($"{initReg.MeteringDirectionId}{initReg.TariffTypeId}", resultUsage);
            }

            foreach (var usage in usagePerRegister)
            {
                if (usage.Key.Contains("3"))
                    prodUsage.Add(usage.Value);
                else
                    consUsage.Add(usage.Value);
            }
            var totalUsage = consUsage.Sum() - prodUsage.Sum();

            return totalUsage;
        }

        public IEnumerable<DateTime> DateInARange(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }
    }
}