﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Autotests.UsageCalc.Models
{
    public class EdsnFraction
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime FractionDate { get; set; }
        public bool IsProduction { get; set; }
        public decimal FractionValue { get; set; }
        public string ProfileCode { get; set; }
    }
}
