﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Repositories.EnerFreeDBModels;
using Autotests.UsageCalc.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.UsageCalculation.Contract.Rest.Transport;

namespace Autotests.UsageCalc.Tests
{
    [TestFixture]
    internal class GetCustomerBalanceTests : BaseUsageCalculation
    {
        [Test]
        public void GetCustomerBalance_DefaultValidCase_Returns200()
        {
            var contractId = EnerFreeDb.GetLastEntity<ContractRegels, DateTime?>(c => c.ContractStart, c => c.Actief)
                .ContractId;

            var response = new GetBalanceRequest { ContractId = contractId }
                .CallWith<GetBalanceRequest, GetBalanceResponse>(UsageCalculationClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.CustomerBalance.MonthlyAmount.Should().NotBe(0);
        }

        [Test]
        public void GetCustomerBalance_WithNotExistedContractId_Returns400()
        {
            const int contractId = 123456;
            const int relationId = 3148038;

            var response = new GetBalanceRequest { ContractId = contractId }
                .CallWith<GetBalanceRequest, GetBalanceResponse>(UsageCalculationClient);

            var errorMessage = string.Format(PatternMessages.NoContractFound2, relationId);
            response.Header.StatusCodeShouldBe(HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void GetCustomerBalance_WithContractWithoutInfo_Returns404()
        {
            const int contractId = 3129044;

            var response = new GetBalanceRequest { ContractId = contractId }
                .CallWith<GetBalanceRequest, GetBalanceResponse>(UsageCalculationClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.NotFound);
            response.Header.Message.Should()
                .BeEquivalentTo(string.Format(PatternMessages.ContractInfoNotFound, contractId));
        }
    }
}