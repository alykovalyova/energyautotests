﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.UsageCalc.Base;
using NUnit.Framework;
using Nuts.UsageCalculation.Contract.Rest.Enums;
using Nuts.UsageCalculation.Contract.Rest.Transport;

namespace Autotests.UsageCalc.Tests
{
    [TestFixture]
    internal class GetUsageTests : BaseUsageCalculation
    {
        [Test]
        public void GetUsage_PerDay()
        {
            var response = new GetUsageRequest
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Day.ToString()
            }.CallWith<GetUsageRequest, GetUsageResponse>(UsageCalculationClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
        }
    }
}