﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataConfiguration;
using Autotests.UsageCalc.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.UsageCalculation.Contract.Rest.Enums;
using Nuts.UsageCalculation.Contract.Rest.Transport;
using SqlMeteringPoint = Autotests.Repositories.MeteringPointModels.MeteringPoint;

namespace Autotests.UsageCalc.Tests
{
    [TestFixture, Category(Categories.UsageCalculation)]
    internal class UsageCalculationTests : BaseUsageCalculation
    {
        [Test]
        public void GetCosts_EstimatedCostsPerDay()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Day.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(mp => mp.EanId == ElkEanId);
            var date = DateTime.Today.AddDays(-30);
            var tariffs = GetLastTariff();
            var priceUsagePerUnit = tariffs.PriceUsagePerUnit;
            var priceNonUsagePerDay = tariffs.PriceNonUsagePerDay;

            var actualCostsSum = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods
                .Where(cp => cp.Date == date).Select(c => c.Estimated).FirstOrDefault();
            var expectedCostsSum = GetEstimatedCosts(meteringPoint, priceNonUsagePerDay,
                priceUsagePerUnit.GetValueOrDefault(), date);
            expectedCostsSum.Should().Be(actualCostsSum);
        }

        [Test]//ean should not have readings in P4Daily and should be called mandate method in P4Daily
        public void GetCosts_NoTariffsWithMandate_ReturnsOnlyZeroCosts()
        {
            var contractId = TestVariablesConfig.GetDefaultValues(Section.UsageCalculation.ToString()).ContractWithMandateAndNoTariffs;

            var response = new GetCostRequest
            {
                ContractId = contractId,
                Period = CalculationPeriod.Day.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);
            response.Data.CustomerCost.EnergyConnections.Should()
                .OnlyContain(c => c.CostPeriods.All(cp => cp.Actual == 0 && cp.Estimated == 0));
        }

        [Test]//ean should not have readings in P4Daily and should NOT be called mandate method in P4Daily
        public void GetCosts_NoTariffsWithoutMandate_Returns412()
        {
            var contractId = TestVariablesConfig.GetDefaultValues(Section.UsageCalculation.ToString()).ContractWithoutTariffsAndMandate;

            var response = new GetCostRequest
            {
                ContractId = contractId,
                Period = CalculationPeriod.Day.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);

            response.Header.StatusCodeShouldBe(HttpStatusCode.PreconditionFailed);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.MandateHasRevoked);
        }

        [Test]
        public void GetCosts_ActualCostsPerDay()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Day.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<SqlMeteringPoint>(mp => mp.EanId == ElkEanId);
            var dateFrom = DateTime.Today.AddDays(-20);
            var dateTo = DateTime.Today.AddDays(-19);
            var tariffs = GetActualTariff(dateFrom);
            var priceUsagePerUnit = tariffs.PriceUsagePerUnit;
            var priceNonUsagePerDay = tariffs.PriceNonUsagePerDay;

            var totalUsage = CalculateActualUsage(meteringPoint, dateFrom, dateTo);
            var actualCosts = decimal.Round(((totalUsage * priceUsagePerUnit) + priceNonUsagePerDay).GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);

            var actualCostsSum = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods
                .Where(cp => cp.Date == dateFrom).Select(c => c.Actual).FirstOrDefault();

            actualCostsSum.Should().Be(actualCosts);
        }

        [Test]
        public void GetCosts_EstimatedCostsPerWeek()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Week.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<SqlMeteringPoint>(mp => mp.EanId == ElkEanId);
            var tariffs = GetLastTariff();
            var priceUsagePerUnit = tariffs.PriceUsagePerUnit;
            var priceNonUsagePerDay = tariffs.PriceNonUsagePerDay;
            var baseDate = DateTime.Today;
            var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddDays(1);
            var dateFrom = thisWeekStart.AddDays(-7);
            var dateTo = thisWeekStart.AddSeconds(-1);

            var actualCostsSum = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods
                .Where(cp => cp.Date == dateFrom).Select(c => c.Estimated).FirstOrDefault();
            var expectedCostsSum = GetEstimatedCosts(meteringPoint, priceNonUsagePerDay,
                priceUsagePerUnit.GetValueOrDefault(), dateFrom, dateTo);
            expectedCostsSum.Should().Be(actualCostsSum);
        }


        /// <summary>
        /// Notes:
        /// 18.06.2021 - Changed calculation of "dateFrom" and "dateTo".
        /// </summary>
        [Test]
        public void GetCosts_ActualCostsPerWeek()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Week.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<SqlMeteringPoint>(mp => mp.EanId == ElkEanId);

            var baseDate = DateTime.Today;
            var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek).AddDays(1);
            var dateFrom = thisWeekStart;
            var dateTo = dateFrom.AddDays(6);
            var costs = new List<decimal>();

            foreach (DateTime day in DateInARange(dateFrom, dateTo))
            {
                var usagePerDay = CalculateActualUsage(meteringPoint, day, day.AddDays(1));
                var tariffPerDay = GetActualTariff(day);
                var costPerDay = (usagePerDay * tariffPerDay.PriceUsagePerUnit) + tariffPerDay.PriceNonUsagePerDay;
                costs.Add(decimal.Round(costPerDay.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero));
            }

            var actualCosts = costs.Sum();
            var actualCostsSum = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods
                .Where(cp => cp.Date == dateFrom).Select(c => c.Actual).FirstOrDefault();

            actualCostsSum.Should().Be(actualCosts);
        }

        [Test]
        public void GetCosts_EstimatedCostsPerMonth()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Month.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var date = DateTime.Today.AddMonths(-2);
            var dateFrom = new DateTime(date.Year, date.Month, 1);
            var dateTo = dateFrom.AddMonths(1).AddDays(-1);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<SqlMeteringPoint>(mp => mp.EanId == ElkEanId);
            var tariffs = GetLastTariff();
            var priceUsagePerUnit = tariffs.PriceUsagePerUnit;
            var priceNonUsagePerDay = tariffs.PriceNonUsagePerDay;

            var actualCostsSum = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods
                .Where(cp => cp.Date == dateFrom).Select(c => c.Estimated).FirstOrDefault();
            var expectedCosts = GetEstimatedCosts(meteringPoint, priceNonUsagePerDay,
                priceUsagePerUnit.GetValueOrDefault(), dateFrom, dateTo);
            expectedCosts.Should().Be(actualCostsSum);
        }

        [Test]
        public void GetCosts_ActualCostsPerMonth()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Month.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<SqlMeteringPoint>(mp => mp.EanId == ElkEanId);
            var date = DateTime.Today.AddMonths(-2);
            var dateFrom = new DateTime(date.Year, date.Month, 1);
            var dateTo = dateFrom.AddMonths(1).AddDays(-1);
            var costs = new List<decimal>();

            foreach (DateTime day in DateInARange(dateFrom, dateTo))
            {
                var usagePerDay = CalculateActualUsage(meteringPoint, day, day.AddDays(1));
                var tariffPerDay = GetActualTariff(day);
                var costPerDay = (usagePerDay * tariffPerDay.PriceUsagePerUnit) + tariffPerDay.PriceNonUsagePerDay;
                costs.Add(decimal.Round(costPerDay.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero));
            }

            var actualCosts = costs.Sum();
            var actualCostsSum = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods
                .Where(cp => cp.Date == dateFrom).Select(c => c.Actual).FirstOrDefault();

            actualCostsSum.Should().Be(actualCosts);
        }

        [Test, Ignore("bug ticket https://budget.atlassian.net/browse/UT-4736")]
        public void GetCosts_ForTheFuture()
        {
            var response = new GetCostRequest()
            {
                ContractId = ContractId,
                Period = CalculationPeriod.Day.ToString()
            }.CallWith<GetCostRequest, GetCostResponse>(UsageCalculationClient);
            response.Header.StatusCodeShouldBe(HttpStatusCode.OK);

            var meteringPoint = MeteringPointDb.GetEntityByCondition<SqlMeteringPoint>(mp => mp.EanId == ElkEanId);
            var date = DateTime.Today.AddDays(1);
            var tariffs = GetLastTariff();
            var priceUsagePerUnit = tariffs.PriceUsagePerUnit;
            var priceNonUsagePerDay = tariffs.PriceNonUsagePerDay;

            var costs = response.Data.CustomerCost.EnergyConnections.First(ec => ec.EanId == ElkEanId).CostPeriods.FirstOrDefault(cp => cp.Date == date);

            var estimatedCostsSum = GetEstimatedCosts(meteringPoint, priceNonUsagePerDay,
                priceUsagePerUnit.GetValueOrDefault(), date, date);
            estimatedCostsSum.Should().Be(costs.Estimated);
            costs.Actual.Should().BeNull();
        }
    }
}
