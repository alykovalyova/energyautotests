﻿using System.Linq.Expressions;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Dispatchers;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Repositories.MMVDBModels;
using Autotests.Repositories.ProdManModels;
using Autotests.Voucher.Builders;
using NUnit.Framework;
using Contract = Nuts.MMVoucher.Model.Contract;
using SftpManager = Autotests.Voucher.SFTP.SftpManager;

namespace Autotests.Voucher.Base
{
    [SetUpFixture, Category(Core.Helpers.Categories.Voucher)]
    internal abstract class BaseVoucherTest : AllureReport
    {
        //clients declaration
        protected CephUploader CephUploader;
        protected SftpManager SftpManager;
        protected Dispatcher Dispatcher;
        protected RestSchedulerClient Scheduler;
        protected ServiceClient<Contract.IMMVoucherService> VoucherClient;
        private const string VoucherFileName = "AO_668343_001_001_200-00_0067641867330.PDF";
        private readonly string _voucherFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
            $"Framework/Files/Voucher/{VoucherFileName}");

        protected Repositories.ProdManModels.Incentive GeneralIncentive;
        protected List<string> AddedCsvRequests;
        protected List<string> AddedCsvErrorResponses;
        protected List<string> AddedCsvRestValueSuccessResponses;
        protected BuildDirector BuildDirector;

        //databases declaration
        protected DbHandler<MMVDBContext> VoucherDb;
        protected DbHandler<ProdManContext> ProdManDb;

        // SQL Join query components
        protected ConfigHandler Config = new ConfigHandler();
        protected Random Random = new Random();
        protected Expression<Func<Proposition, Guid>> OuterKey => p => p.Id;
        protected Expression<Func<Repositories.ProdManModels.Incentive, Guid>> InnerKey => i => i.PropositionId;
        protected Expression<Func<Proposition, Repositories.ProdManModels.Incentive, JoinSelector<Proposition, Repositories.ProdManModels.Incentive>>> ResultSelector =>
            (o, i) => new JoinSelector<Proposition, Repositories.ProdManModels.Incentive> {Outer = o, Inner = i};
        protected Expression<Func<JoinSelector<Proposition, Repositories.ProdManModels.Incentive>, bool>> Condition =>
            res => res.Outer.StatusId == 3 && res.Outer.LabelCode == (int) Helper.Enums.Label.BudgetEnergie &&
                   res.Inner.IncentiveTypeId == 3 && res.Inner.VoucherAmount > 0;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            CephUploader = new CephUploader();
            VoucherClient = new ServiceClient<Contract.IMMVoucherService>(Config.ServicesEndpoints.MMVoucherService);
            SftpManager = new SftpManager();
            Scheduler = new RestSchedulerClient();

            VoucherDb = new DbHandler<MMVDBContext>(Config.GetConnectionString(DbConnectionName.MMVDBDatabase.ToString()));
            ProdManDb = new DbHandler<ProdManContext>(Config.GetConnectionString(DbConnectionName.ProdManDatabase.ToString()));
            Dispatcher = new Dispatcher(DispatcherName.CustomerInfo, Config.QueueSettings.HostNameQueue);

            CephUploader.UploadCephFile($"PDF/{DateTime.Now:yyyy-MM-dd}", VoucherFileName, _voucherFilePath)
                .GetAwaiter().GetResult();
            AddedCsvErrorResponses = new List<string>();
            AddedCsvRequests = new List<string>();
            AddedCsvRestValueSuccessResponses = new List<string>();
            BuildDirector = new BuildDirector();
            if (TestContext.CurrentContext.Test.Name == "DistributeVoucherTests") ReserveThenDistributeSetUp();

            SftpInitialize();
        }

        [TearDown]
        public void TearDown()
        {
            if (!TestContext.CurrentContext.Test.Name.Equals("CancelVoucher_FromMediaMarket_VoucherCanceled")) return;

            foreach (var csvRequest in AddedCsvRequests)
                SftpManager.RemoveFile(csvRequest);
            foreach (var csvErrorResponse in AddedCsvErrorResponses)
                SftpManager.RemoveFile(csvErrorResponse);
            foreach (var csvRestValueSuccess in AddedCsvRestValueSuccessResponses)
                SftpManager.RemoveFile(csvRestValueSuccess);
        }

        private void SftpInitialize()
        {
            // checking error folder and clean if errors are present
            var errorFiles = new List<string>();
            errorFiles.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpErrorCsvPath, "*.csv"));
            if (AddedCsvErrorResponses.Count <= 0) return;
            TestContext.Out.WriteLine($"Output folder has {AddedCsvErrorResponses.Count} errors, cleaning it");
            foreach (var errorFile in errorFiles)
            {
                SftpManager.RemoveFile(errorFile);
            }
        }

        protected int AddFreeVoucherToDb(decimal voucherAmount = 500)
        {
            var freeVoucher = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().SetAmount(voucherAmount).Build();
            return AddVoucher(freeVoucher);
        }

        public int AddVoucher(Contract.Voucher voucher)
        {
            var voucherToAdd = voucher.To<Repositories.MMVDBModels.Voucher>();
            voucherToAdd.CephKey = CephUploader.CephKey;
            var dbId = VoucherDb.AddEntityToDb(voucherToAdd).Id;
            voucher.VoucherId = dbId;
            return dbId;
        }

        protected Contract.Customer GetCustomer()
        {
            var count = 3;
            Contract.Customer customer = null;
            while (count > 0)
            {
                var customerName = RandomDataProvider.GetRandomNumbersString(25);
                var initials = GenerateGuidString(Random.Next(2, 10));
                var customerBirthday = GetRandomBirthDate();
                var bankAccountNumber = GetRandomIbanNumber();
                const int houseNr = 25;
                var postalCode = GetRandomZipcode();
                var extHouseNr = RandomDataProvider.GetRandomNumbersString(1);
                customer = BuildDirector.Get<CustomerBuilder>()
                    .SetPersonalDetails(customerName, initials, bankAccountNumber, customerBirthday)
                    .SetAddress(postalCode, houseNr, extHouseNr).Build();
                var checkVoucher = new Contract.VoucherCheckRequest {Customer = customer}
                    .CallWith(VoucherClient.Proxy.VoucherCheck);
                if (!checkVoucher.CanReserveVoucher)
                {
                    count--;
                    continue;
                }

                break;
            }

            return customer;
        }

        public int AddVoucher(Repositories.MMVDBModels.Voucher voucher)
        {
            var dbId = VoucherDb.AddEntityToDb(voucher).Id;
            voucher.Id = dbId;
            return dbId;
        }

        public Contract.Voucher GetVoucher(int voucherId) => VoucherDb
            .GetEntityByCondition<Repositories.MMVDBModels.Voucher>(v => v.Id == voucherId).To<Contract.Voucher>();

        protected string GetRandomIbanNumber() => "NL91ABNA" + Random.Next(0, 9) + Random.Next(100000000, 999999999);

        protected string GetRandomZipcode() => RandomDataProvider.GetRandomNumbersString(4) + "GR";

        protected string GenerateGuidString(int length) => Guid.NewGuid().ToString("N").Remove(length);

        protected DateTime GetRandomBirthDate() =>
            DateTime.Today.AddYears(-Random.Next(20, 90)).AddDays(-Random.Next(1, 365));

        private void ReserveThenDistributeSetUp()
        {
            GeneralIncentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition)
                .Inner;
            for (var i = 0; i < 15; i++)
                AddFreeVoucherToDb(GeneralIncentive.VoucherAmount);
        }
    }
}