﻿using Autotests.Core.Helpers;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Builders
{
    internal class CustomerBuilder : MainBuilder<Customer>
    {
        internal CustomerBuilder SetPersonalDetails(string surname, string initials = "J.J.", string iban = "123", DateTime? birthday = null)
        {
            Instance.PersonalDetails = new PersonalDetails
            {
                Surname = surname,
                Initials = initials,
                BirthDate = birthday ?? DateTime.Now.AddYears(-50),
                Iban = iban
            };
            return this;
        }

        internal CustomerBuilder SetAddress(string zipcode, int buildingNr, string exBuildingNr = null)
        {
            Instance.Address = new Address
            {
                ZIPCode = zipcode,
                BuildingNr = buildingNr,
                ExBuildingNr = exBuildingNr
            };
            return this;
        }
    }
}