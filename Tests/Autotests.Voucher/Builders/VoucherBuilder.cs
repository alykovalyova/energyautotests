﻿using System.Reflection;
using Autotests.Core.Helpers;
using Autotests.Voucher.Enums;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;
using ContractVoucher = Nuts.MMVoucher.Model.Contract.Voucher;

namespace Autotests.Voucher.Builders
{
    internal class VoucherBuilder : MainBuilder<ContractVoucher>
    {
        private readonly Random _random = new Random();

        internal VoucherBuilder GetBaseVoucher()
        {
            const string baseFileName = @"test\xx_new_voucher";
            var uniqueCode = _random.Next(10000000, 99999999);
            Instance.CreatedOn = DateTime.Now;
            Instance.FtpFileName = $"{baseFileName}_{uniqueCode}";
            Instance.VoucherCode = uniqueCode.ToString();
            Instance.VoucherAmount = 100;
            return this;
        }

        internal VoucherBuilder SetDateProperty(VoucherDateProps propertyName, DateTime propertyValue)
        {
            var property = Instance.GetType().GetProperty(propertyName.ToString(), BindingFlags.Public | BindingFlags.Instance);
            if (property != null && property.CanWrite)
            {
                property.SetValue(Instance, propertyValue);
            }
            else
            {
                Assert.Fail("Property name of Voucher object is incorrect in VoucherBuilder");
            }
            return this;
        }

        internal VoucherBuilder SetDateProperties(KeyValuePair<Enums.VoucherDateProps, DateTime> canceledOn, params KeyValuePair<VoucherDateProps, DateTime>[] values)
        {
            foreach (var keyValuePair in values)
            {
                var prop = Instance.GetType().GetProperty(keyValuePair.Key.ToString(), BindingFlags.Public | BindingFlags.Instance);
                if (prop != null && prop.CanWrite)
                {
                    prop.SetValue(Instance, keyValuePair.Value);
                }
                else
                {
                    Assert.Fail("Property name of Voucher object is incorrect in VoucherBuilder");
                }
            }
            return this;
        }

        internal VoucherBuilder SetAmount(decimal amount)
        {
            Instance.VoucherAmount = amount;
            return this;
        }

        internal VoucherBuilder WithCustomer()
        {
            Instance.Customer = new Customer();
            return this;
        }

        internal VoucherBuilder SetPersonalDetails(string surname, string initials, string iban, DateTime birthday)
        {
            Assert.NotNull(Instance.Customer, "First need to add customer property using WithCustomer method");
            Instance.Customer.PersonalDetails = new PersonalDetails
            {
                Surname = surname,
                Initials = initials,
                BirthDate = birthday,
                Iban = iban
            };
            return this;
        }

        internal VoucherBuilder SetAddress(string zipcode, int buildingNr, string exBuildingNr = null)
        {
            Assert.NotNull(Instance.Customer, "First need to add customer property using WithCustomer method");
            Instance.Customer.Address = new Address
            {
                ZIPCode = zipcode,
                BuildingNr = buildingNr,
                ExBuildingNr = exBuildingNr
            };
            return this;
        }
    }
}