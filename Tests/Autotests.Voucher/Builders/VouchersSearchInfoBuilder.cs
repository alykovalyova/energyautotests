﻿using Autotests.Core.Helpers;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Builders
{
    internal class VouchersSearchInfoBuilder : MainBuilder<VouchersSearchInfo>
    {
        internal VouchersSearchInfoBuilder SetDefaults(string surname, string iban, string voucherCode)
        {
            Instance.CustomerSurname = surname;
            Instance.Iban = iban;
            Instance.VoucherCode = voucherCode;
            return this;
        }

        internal VouchersSearchInfoBuilder SetZipcode(string zipcode)
        {
            Instance.ZIPCode = zipcode;
            return this;
        }
    }
}