﻿namespace Autotests.Voucher.Enums
{
    internal enum VoucherDateProps
    {
        CreatedOn,
        ReservedOn,
        DistributedOn,
        ClosedOn,
        CanceledOn,
        ContractCreatedOn,
        SentToMediaMarketOn
    }
}