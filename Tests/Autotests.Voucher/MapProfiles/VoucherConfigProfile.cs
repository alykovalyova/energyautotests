﻿using AutoMapper;
using Autotests.Core;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.MapProfiles
{
    public class VoucherConfigProfile : Profile
    {
        public VoucherConfigProfile()
        {
            CreateMap<Repositories.MMVDBModels.Voucher, Nuts.MMVoucher.Model.Contract.Voucher>()
                .ForMember(dest => dest.VoucherId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Customer, opt => opt.Ignore());
            CreateMap<Repositories.MMVDBModels.Voucher, PersonalDetails>()
                .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.CustomerSurname))
                .ForMember(dest => dest.Initials, opt => opt.MapFrom(src => src.CustomerInitials))
                .ForMember(dest => dest.Iban, opt => opt.MapFrom(src => src.CustomerIban))
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.CustomerBirthDate ?? DateTime.MinValue));
            CreateMap<Repositories.MMVDBModels.Voucher, Address>()
                .ForMember(dest => dest.BuildingNr, opt => opt.MapFrom(src => src.ContractBuildingNr ?? 0))
                .ForMember(dest => dest.ZIPCode, opt => opt.MapFrom(src => src.ContractZipcode))
                .ForMember(dest => dest.ExBuildingNr, opt => opt.MapFrom(src => src.ContractExBuildingNr));
            CreateMap<Repositories.MMVDBModels.Voucher, Customer>()
                .ForMember(dest => dest.PersonalDetails, opt => opt.Ignore())
                .ForMember(dest => dest.Address, opt => opt.Ignore())
                .AfterMap((opt, dest) =>
                {
                    if (opt.CustomerBirthDate.HasValue ||
                        opt.CustomerSurname != null ||
                        opt.CustomerInitials != null ||
                        opt.CustomerIban != null)
                    {
                        dest.PersonalDetails = opt.To<PersonalDetails>();
                    }
                    if (opt.ContractBuildingNr.HasValue ||
                        opt.ContractZipcode != null ||
                        opt.ContractExBuildingNr != null)
                    {
                        dest.Address = opt.To<Address>();
                    }
                });
            CreateMap<Nuts.MMVoucher.Model.Contract.Voucher, Repositories.MMVDBModels.Voucher>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.VoucherId))
                .ForMember(dest => dest.CephKey, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerSurname, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerInitials, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerBirthDate, opt => opt.Ignore())
                .ForMember(dest => dest.CustomerIban, opt => opt.Ignore())
                .ForMember(dest => dest.ContractBuildingNr, opt => opt.Ignore())
                .ForMember(dest => dest.ContractExBuildingNr, opt => opt.Ignore())
                .ForMember(dest => dest.ContractZipcode, opt => opt.Ignore())
                .ForMember(dest => dest.RestValueFileName, opt => opt.Ignore())
                .ForMember(dest => dest.ReceivedRestValueCephKey, opt => opt.Ignore())
                .ForMember(dest => dest.CanceledManually, opt => opt.Ignore())
                .ForMember(dest => dest.SentRestValueCephKey, opt => opt.Ignore())
                .AfterMap((opt, dest) =>
                {
                    if (opt.Customer != null)
                    {
                        dest.CustomerBirthDate = opt.Customer.PersonalDetails.BirthDate;
                        dest.CustomerSurname = opt.Customer.PersonalDetails.Surname;
                        dest.CustomerInitials = opt.Customer.PersonalDetails.Initials;
                        dest.CustomerIban = opt.Customer.PersonalDetails.Iban;
                        dest.ContractBuildingNr = opt.Customer.Address.BuildingNr;
                        dest.ContractExBuildingNr = opt.Customer.Address.ExBuildingNr;
                        dest.ContractZipcode = opt.Customer.Address.ZIPCode;
                    }
                });
            CreateMap<Repositories.MMVDBModels.Voucher, VoucherAmountInfo>()
                .ForMember(dest => dest.CanceledOn, opt => opt.MapFrom(src => src.CanceledOn))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => src.CreatedOn))
                .ForMember(dest => dest.DistributedOn, opt => opt.MapFrom(src => src.DistributedOn))
                .ForMember(dest => dest.ReservedOn, opt => opt.MapFrom(src => src.ReservedOn))
                .ForMember(dest => dest.RestValue, opt => opt.MapFrom(src => src.RestValue))
                .ForMember(dest => dest.VoucherAmount, opt => opt.MapFrom(src => src.VoucherAmount))
                .ForMember(dest => dest.VoucherId, opt => opt.MapFrom(src => src.Id));
        }
    }
}