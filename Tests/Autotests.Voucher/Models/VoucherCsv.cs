﻿namespace Autotests.Voucher.Models
{
    public class VoucherCsv
    {
        public int ID { get; set; }
        public string BRAND { get; set; }
        public string VOUCHER { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal? CORRECTED_TOTAL_AMOUNT { get; set; }
        public decimal? AMOUNT_USED { get; set; }
        public decimal? AMOUNT_BLOCKED { get; set; }
    }
}
