﻿namespace Autotests.Voucher.SFTP
{
    public interface ISftpManager
    {
        string[] GetFiles(string sftpDictionary, string mask);
        byte[] DownloadFile(string sftpPath);
        void UploadFile(byte[] content, string sftpPath);
        void RemoveFile(string sftpPath);
        bool FileExists(string sftpPath);
    }
}