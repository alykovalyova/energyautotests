﻿using Autotests.Core.Handlers;
using WinSCP;
using EnumerationOptions = WinSCP.EnumerationOptions;

namespace Autotests.Voucher.SFTP
{
    public class SftpManager : ISftpManager
    {
        private readonly ConfigHandler _config = ConfigHandler.Instance;
        private readonly TransferOptions _transferOptions = new TransferOptions { TransferMode = TransferMode.Binary };
        private readonly SessionOptions _sessionOptions = new SessionOptions();

        public SftpManager()
        {
            _sessionOptions.Protocol = Protocol.Sftp;
            _sessionOptions.HostName = _config.SftpSettings.SFTPHost;
            _sessionOptions.UserName = _config.SftpSettings.SFTPUser;
            _sessionOptions.Password = _config.SftpSettings.SFTPPass;
            _sessionOptions.GiveUpSecurityAndAcceptAnySshHostKey = true;
        }

        public string[] GetFiles(string sftpDictionary, string mask)
        {
            using (var session = new Session())
            {
                session.Open(_sessionOptions);
                var files = session.EnumerateRemoteFiles(sftpDictionary, mask, new EnumerationOptions())
                    .Select(f => f.FullName).ToArray();
                return files;
            }
        }

        public byte[] DownloadFile(string sftpPath)
        {
            using (var session = new Session())
            {
                session.Open(_sessionOptions);
                var localPath = Path.GetTempFileName();
                try
                {
                    session.GetFiles(sftpPath, localPath, false, _transferOptions).Check();
                    if (File.Exists(localPath))
                    {
                        return File.ReadAllBytes(localPath);
                    }
                }
                finally
                {
                    if (!string.IsNullOrEmpty(localPath) && File.Exists(localPath))
                    {
                        File.Delete(localPath);
                    }
                }

                throw new FileNotFoundException("Downloaded ftp file was not found in local storage.");
            }
        }

        public void UploadFile(byte[] content, string sftpPath)
        {
            using (var session = new Session())
            {
                session.Open(_sessionOptions);
                var localPath = Path.GetTempFileName();
                if (string.IsNullOrEmpty(sftpPath))
                    throw new ArgumentException("SFTP File name can't be empty");

                File.WriteAllBytes(localPath, content);
                session.PutFiles(localPath, sftpPath, true, _transferOptions).Check();
            }
        }

        public void RemoveFile(string sftpPath)
        {
            using (var session = new Session())
            {
                session.Open(_sessionOptions);
                session.RemoveFiles(sftpPath).Check();
            }
        }

        public bool FileExists(string sftpPath)
        {
            using (var session = new Session())
            {
                session.Open(_sessionOptions);
                return session.FileExists(sftpPath);
            }
        }
    }
}