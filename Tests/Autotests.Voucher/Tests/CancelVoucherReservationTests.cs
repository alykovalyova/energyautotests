﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class CancelVoucherReservationTests : BaseVoucherTest
    {
        [Test]
        public void CancelVoucherReservation_DefaultValidCase_ReservedVoucherCanceled()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);

            // Reserve voucher
            var newCustomer = GetCustomer();
            var reserveResponse = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            reserveResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, reserveResponse.Header.Message);
            GetVoucher(reserveResponse.VoucherId).ReservedOn.Should().HaveValue("ReservedOn is empty");

            // Cancel voucher
            var cancelResponse = new CancelVoucherReservationRequest { VoucherId = reserveResponse.VoucherId }
                .CallWith(VoucherClient.Proxy.CancelVoucherReservation);

            // Assertion
            cancelResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, cancelResponse.Header.Message);
            var voucherInDb = GetVoucher(reserveResponse.VoucherId);
            voucherInDb.ReservedOn.Should().BeNull();
            voucherInDb.Customer.Should().BeNull();
        }

        [Test]
        public void CancelVoucherReservation_WithNotExistedVoucherId_Returns500()
        {
            const int voucherId = int.MaxValue - 1;

            var response = new CancelVoucherReservationRequest { VoucherId = voucherId }
                .CallWith(VoucherClient.Proxy.CancelVoucherReservation);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError, response.Header.Message);
            var message = string.Format(PatternMessages.CannotFindVoucherToCancelReserve, voucherId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CancelVoucherReservation_WithNotReservedVoucherId_Returns500()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition)
                .Inner;
            var voucherId = AddFreeVoucherToDb(incentive.VoucherAmount);

            var response = new CancelVoucherReservationRequest { VoucherId = voucherId }
                .CallWith(VoucherClient.Proxy.CancelVoucherReservation);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError, response.Header.Message);
            var message = string.Format(PatternMessages.CannotFindVoucherToCancelReserve, voucherId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}