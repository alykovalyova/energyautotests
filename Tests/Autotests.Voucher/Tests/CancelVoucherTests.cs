﻿using System.Net;
using Autotests.Clients;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class CancelVoucherTests : BaseVoucherTest
    {
        [Test]
        public void CancelVoucher_DefaultValidCase_Returns200()
        {
            // Arrange
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);
            var customer = GetCustomer();

            // Reserve free voucher
            var reservationResponse = new ReserveFreeVoucherRequest
            {
                Customer = customer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            reservationResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            // Distribute reserved voucher
            var distributeResponse = new DistributeVoucherRequest { VoucherId = reservationResponse.VoucherId }
                .CallWith(VoucherClient.Proxy.DistributeVoucher);
            distributeResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, distributeResponse.Header.Message);
            GetVoucher(reservationResponse.VoucherId).DistributedOn.GetValueOrDefault().Date.Should().Be(DateTime.Now.Date);

            // Cancel voucher
            var response = new CancelVoucherRequest { VoucherId = distributeResponse.VoucherId }
                .CallWith(VoucherClient.Proxy.CancelVoucher);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            // Assertion
            GetVoucher(distributeResponse.VoucherId).CanceledOn
                .GetValueOrDefault().Date.Should().Be(DateTime.Now.Date);
        }
    }
}
