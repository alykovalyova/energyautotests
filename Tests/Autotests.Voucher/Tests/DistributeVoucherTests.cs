﻿using System.Net;
using System.ServiceModel;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using Autotests.Voucher.Builders;
using Autotests.Voucher.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class DistributeVoucherTests : BaseVoucherTest
    {
        [Test]
        public void DistributeVoucher_DefaultValidCase_Returns200()
        {
            var voucherWithTestCephKey = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().SetDateProperty(VoucherDateProps.ReservedOn, DateTime.Now)
                .Build().To<Repositories.MMVDBModels.Voucher>();
            voucherWithTestCephKey.CephKey = CephUploader.CephKey;
            var voucherId = AddVoucher(voucherWithTestCephKey);

            var response = new DistributeVoucherRequest { VoucherId = voucherId }
                .CallWith(VoucherClient.Proxy.DistributeVoucher);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.VoucherId.Should().Be(voucherId);
            response.VoucherContentPdf.Should().BeOfType(typeof(byte[]));
            GetVoucher(voucherId).DistributedOn.GetValueOrDefault().Date.Should().Be(DateTime.Now.Date);
        }

        [Test]
        public void DistributeVoucher_AfterReservation_VoucherDistributed()
        {
            // Reserve voucher
            var newCustomer = GetCustomer();

            var reserveResponse = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = GeneralIncentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            reserveResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, reserveResponse.Header.Message);

            // Distribute reserved voucher
            var response = new DistributeVoucherRequest { VoucherId = reserveResponse.VoucherId }
                .CallWith(VoucherClient.Proxy.DistributeVoucher);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            GetVoucher(reserveResponse.VoucherId).DistributedOn.GetValueOrDefault().Date.Should().Be(DateTime.Now.Date);
        }

        [Test]
        public void DistributeVoucher_WithNotExistedVoucherId_ThrowsException()
        {
            const int voucherId = int.MaxValue - 1;
            Action action = () =>
            {
                new DistributeVoucherRequest { VoucherId = voucherId }
                    .CallWith(VoucherClient.Proxy.DistributeVoucher);
            };
            action.Should().ThrowExactly<FaultException<ExceptionDetail>>()
                .And.Message.Should().BeEquivalentTo(string.Format(PatternMessages.CannotFindVoucherToDistribute, voucherId));
        }
    }
}