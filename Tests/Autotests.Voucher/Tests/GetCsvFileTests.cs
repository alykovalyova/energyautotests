﻿using System.Net;
using Autotests.Clients;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class GetCsvFileTests : BaseVoucherTest
    {
        [Test]
        public void GetCsvFile_DefaultValidCase_Returns200()
        {
            var response = new GetCsvFileRequest
            {
                FileName = string.Empty
            }.CallWith(VoucherClient.Proxy.GetCsvFile);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }
    }
}