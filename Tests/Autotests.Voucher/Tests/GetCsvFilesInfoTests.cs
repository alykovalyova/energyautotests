﻿using System.Net;
using Autotests.Clients;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class GetCsvFilesInfoTests : BaseVoucherTest
    {
        [Test]
        public void GetCsvFilesInfo_DefaultValidCase_Returns200()
        {
            var response = new GetCsvFilesInfoRequest().CallWith(VoucherClient.Proxy.GetCsvFilesInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.CsvFilesInfo.Should().NotBeEmpty();
        }
    }
}