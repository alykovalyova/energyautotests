﻿using System.Net;
using Autotests.Clients;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class GetFreeVouchersTests : BaseVoucherTest
    {
        [Test]
        public void GetFreeVouchers_DefaultValidCase_Returns200()
        {
            var response = new GetFreeVouchersRequest().CallWith(VoucherClient.Proxy.GetFreeVouchers);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.FreeVoucherInfo.Should().NotBeEmpty();
        }
    }
}