﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class GetVoucherAmountByIdTests : BaseVoucherTest
    {
        [Test]
        public void GetVoucherAmountById_DefaultValidCase_Returns200()
        {
            var voucherFromDb = VoucherDb.GetEntityByCondition<Repositories.MMVDBModels.Voucher>(v => !v.VoucherAmount.Equals(0));

            var response = new GetVoucherAmountByIdRequest { VoucherId = voucherFromDb.Id }
                .CallWith(VoucherClient.Proxy.GetVoucherAmountById);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            ObjectComparator.ComparePropsOfTypes(response.Voucher, voucherFromDb.To<VoucherAmountInfo>())
                .Should().BeTrue();
        }

        [Test]
        public void GetVoucherAmountById_WithNotExistedVoucherId_Returns500()
        {
            const int voucherId = int.MaxValue - 1;

            var response = new GetVoucherAmountByIdRequest { VoucherId = voucherId }
                .CallWith(VoucherClient.Proxy.GetVoucherAmountById);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            var message = string.Format(PatternMessages.VoucherIsNotFound, voucherId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}
