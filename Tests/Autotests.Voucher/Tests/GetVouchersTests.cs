﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using Autotests.Voucher.Builders;
using Autotests.Voucher.Enums;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class GetVouchersTests : BaseVoucherTest
    {
        [Test]
        public void GetVouchers_DefaultValidCase_Returns200()
        {
            var voucherFromDb = VoucherDb.GetEntitiesByCondition<Repositories.MMVDBModels.Voucher>(
                v => v.CustomerSurname != null
                     && v.CustomerIban != null
                     && v.VoucherCode != null
                     && v.ContractZipcode != null).RandomItem();
            var response = new GetVouchersRequest
            {
                SearchCriteria = BuildDirector.Get<VouchersSearchInfoBuilder>()
                    .SetDefaults(voucherFromDb.CustomerSurname, voucherFromDb.CustomerIban, voucherFromDb.VoucherCode)
                    .SetZipcode(voucherFromDb.ContractZipcode).Build()
            }.CallWith(VoucherClient.Proxy.GetVouchers);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        public void GetVouchers_AllVouchers_Returns200()
        {
            var response = new GetVouchersRequest
            {
                SearchCriteria = new VouchersSearchInfo()
            }.CallWith(VoucherClient.Proxy.GetVouchers);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Test]
        public void GetVouchers_WithoutSearchCriteria_Returns500()
        {
            var response = new GetVouchersRequest().CallWith(VoucherClient.Proxy.GetVouchers);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo("The SearchCriteria field is required.");
        }

        [Test]
        public void GetVouchers_WithInvalidZipcodeRegex_Returns500([Values("111QQ", "11111Q", "QQQQQQ", "111111")] string zipcode)
        {
            var response = new GetVouchersRequest
            {
                SearchCriteria = BuildDirector.Get<VouchersSearchInfoBuilder>().SetZipcode(zipcode).Build()
            }.CallWith(VoucherClient.Proxy.GetVouchers);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidZipCodeRegex);
        }

        [Test]
        public void GetVouchers_SearchCanceledVoucher_Returns200()
        {
            var customerName = GenerateGuidString(17);
            var initials = GenerateGuidString(6);
            var customerBirthday = GetRandomBirthDate();
            var bankAccountNumber = GetRandomIbanNumber();
            var canceledOn = new KeyValuePair<VoucherDateProps, DateTime>(VoucherDateProps.CanceledOn, new DateTime(2017, 10, 25));

            var voucher = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().SetDateProperties(canceledOn)
                .WithCustomer().SetPersonalDetails(customerName, initials, bankAccountNumber, customerBirthday)
                .Build();
            var dbVoucherId = AddVoucher(voucher);

            var response = new GetVouchersRequest
            {
                SearchCriteria = new VouchersSearchInfo
                {
                    CustomerSurname = customerName,
                    Iban = bankAccountNumber
                }
            }.CallWith(VoucherClient.Proxy.GetVouchers);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Vouchers.Should().HaveCount(1)
                .And.Subject.First().VoucherId.Should().Be(dbVoucherId);
        }
    }
}