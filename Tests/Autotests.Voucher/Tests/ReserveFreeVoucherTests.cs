﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core.CsvTool;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using Autotests.Voucher.Builders;
using Autotests.Voucher.Enums;
using Autotests.Voucher.Models;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class ReserveFreeVoucherTests : BaseVoucherTest
    {
        [Test]
        public void ReserveFreeVoucher_WhenNewCustomerInfoSupplied_ReturnedOnIsNotEmpty()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);
            var newCustomer = GetCustomer();

            // Reserve voucher
            var response = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            // Assertion
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            GetVoucher(response.VoucherId).ReservedOn.Should().HaveValue("ReservedOn is empty");
            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_CancelVoucherReservationJob);
            Waiter.Wait(() =>
                VoucherDb.GetEntitiesByCondition<Repositories.MMVDBModels.Voucher>(v => v.CustomerIban == newCustomer.PersonalDetails.Iban).Count == 0);
        }

        [Test]
        public void ReserveFreeVoucher_WhenFullNameAndIbanMatchExistingCustomer_Returns412()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);
            var newCustomer = GetCustomer();

            // Reserve voucher
            var request = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = incentive.PropositionId
            };

            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_CancelVoucherReservationJob);
            Waiter.Wait(() =>
                VoucherDb.GetEntitiesByCondition<Repositories.MMVDBModels.Voucher>(v => v.CustomerIban == newCustomer.PersonalDetails.Iban).Count == 0);
            var response = request.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            // Reserve voucher for existing customer
            response = request.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            // Assertion
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.PreconditionFailed, response.Header.Message);
            var errorMessage = string.Format(
                PatternMessages.VoucherReceived, newCustomer.PersonalDetails.Initials,
                newCustomer.PersonalDetails.Surname, newCustomer.PersonalDetails.BirthDate.ToString("dd-MM-yyyy"), newCustomer.PersonalDetails.Iban);
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void ReserveFreeVoucher_WhenAddressMatchExistingCustomer_Returns412()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;

            var customer = GetCustomer();
            var voucher = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().WithCustomer().SetPersonalDetails(
                    customer.PersonalDetails.Surname, customer.PersonalDetails.Initials,
                    customer.PersonalDetails.Iban, customer.PersonalDetails.BirthDate)
                .SetAddress(customer.Address.ZIPCode, customer.Address.BuildingNr, customer.Address.ExBuildingNr)
                .SetAmount(incentive.VoucherAmount).Build();
            AddVoucher(voucher);

            // Reserve voucher
            var request = new ReserveFreeVoucherRequest
            {
                Customer = customer,
                PropositionId = incentive.PropositionId
            };
            var response = request.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            // Reserve voucher for existing customer
            response = request.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            // Assertion
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.PreconditionFailed, response.Header.Message);
            var errorMessage = string.Format(
                PatternMessages.VoucherReceived, customer.PersonalDetails.Initials,
                customer.PersonalDetails.Surname, customer.PersonalDetails.BirthDate.ToString("dd-MM-yyyy"),
                customer.PersonalDetails.Iban);
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void ReserveFreeVoucher_AfterItWasCanceled_RestValueEqualToVoucherAmount()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            var customer = GetCustomer();

            // Add canceled voucher to DB
            var canceledVoucher = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().WithCustomer().SetPersonalDetails(
                    customer.PersonalDetails.Surname, customer.PersonalDetails.Initials,
                    customer.PersonalDetails.Iban, customer.PersonalDetails.BirthDate)
                .SetAddress(customer.Address.ZIPCode, customer.Address.BuildingNr, customer.Address.ExBuildingNr)
                .SetAmount(incentive.VoucherAmount)
                .SetDateProperty(VoucherDateProps.ReservedOn, DateTime.Now)
                .SetDateProperty(VoucherDateProps.DistributedOn, DateTime.Now)
                .SetDateProperty(VoucherDateProps.CanceledOn, DateTime.Now)
                .Build();
            var canceledVoucherId = AddVoucher(canceledVoucher);
            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_RequestRestValueJob);
            Waiter.Wait(() => VoucherDb.EntityIsInDb<Repositories.MMVDBModels.Voucher>(
                v => v.RestValueFileName != null && v.SentRestValueCephKey != null));
            var voucher = GetVoucher(canceledVoucherId);

            // Prepare csv for Media Market
            var voucherCsv = new List<VoucherCsv>
            {
                new VoucherCsv
                {
                    ID = canceledVoucherId,
                    BRAND = "Budget",
                    VOUCHER = voucher.VoucherCode,
                    TOTAL_AMOUNT = voucher.VoucherAmount,
                    CORRECTED_TOTAL_AMOUNT = voucher.VoucherAmount,
                    AMOUNT_BLOCKED = voucher.VoucherAmount,
                    AMOUNT_USED = 0
                }
            };
            var csv = new CsvCreator<VoucherCsv>(voucherCsv, ";", true).ToBytes();
            var path = $"{Config.SftpSettings.SftpResponseCsvPath}/Budget_{DateTime.Now:yyyyMMddHHmmss}.csv";
            SftpManager.UploadFile(csv, path);

            // Process response csv from Media Market
            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_SftpListenerJob);
            Waiter.Wait(() => VoucherDb.GetEntityByCondition<Repositories.MMVDBModels.Voucher>(
                v => v.Id == canceledVoucherId && v.RestValue.HasValue && v.ClosedOn.HasValue));
            voucher = GetVoucher(canceledVoucherId);
            voucher.RestValue.Should().Be(voucher.VoucherAmount);
            voucher.CanceledOn.Should().NotBeNull();

            AddedCsvErrorResponses.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpErrorCsvPath, "*.csv"));
            AddedCsvRequests.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpRequestCsvPath, "*.csv"));
            AddedCsvRestValueSuccessResponses.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpRestValueSuccessCsvPath, "*.csv"));

            // Reserve voucher
            var response = new ReserveFreeVoucherRequest
            {
                Customer = customer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            // Assertion
            VoucherDb.GetEntityByCondition<Repositories.MMVDBModels.Voucher>(v => v.Id == response.VoucherId)
                .ReservedOn.Should().HaveValue();
        }

        [Test]
        public void ReserveFreeVoucher_WithoutCustomerField_Returns500()
        {
            var response = new ReserveFreeVoucherRequest
            {
                Customer = null,
                PropositionId = Guid.NewGuid()
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError, response.Header.Message);
            var message = string.Format(PatternMessages.FieldIsRequired, "Customer");
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void ReserveFreeVoucher_WithNotExistedPropositionId_Returns500()
        {
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);
            var newCustomer = GetCustomer();
            var propositionId = Guid.NewGuid();

            var response = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = propositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError, response.Header.Message);
            var message = string.Format(PatternMessages.EmptyProdManResponse, propositionId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void ReserveVoucher_LessThanTwoYearsPeriod_Returns412()
        {
            var date = DateTime.Now.AddYears(-1);
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            var customer = VoucherDb.GetEntitiesByCondition<Repositories.MMVDBModels.Voucher>(v =>
                v.DistributedOn > date && v.ContractZipcode != null && v.CustomerSurname != null && v.CustomerIban != null).First();
            var newCustomer = BuildDirector.Get<CustomerBuilder>()
                .SetPersonalDetails(customer.CustomerSurname, customer.CustomerInitials, customer.CustomerIban, customer.CustomerBirthDate)
                .SetAddress(customer.ContractZipcode, customer.ContractBuildingNr.GetValueOrDefault(), customer.ContractExBuildingNr).Build();
            var propositionId = incentive.PropositionId;

            var response = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = propositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.PreconditionFailed, response.Header.Message);
            var errorMessage = string.Format(PatternMessages.VoucherReceived, customer.CustomerInitials, customer.CustomerSurname,
                customer.CustomerBirthDate.GetValueOrDefault().ToString("dd-MM-yyyy"), customer.CustomerIban);
            response.Header.Message.Should().BeEquivalentTo(errorMessage);
        }

        [Test]
        public void ReserveVoucher_MoreThanTwoYearsPeriod_Returns200()
        {
            var date = DateTime.Now.AddYears(-2);
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            var customer = VoucherDb.GetEntitiesByCondition<Repositories.MMVDBModels.Voucher>(v =>
                v.DistributedOn < date && v.ContractZipcode != null && v.CustomerSurname != null && v.CustomerIban != null).First();
            var newCustomer = BuildDirector.Get<CustomerBuilder>()
                .SetPersonalDetails(customer.CustomerSurname, customer.CustomerInitials, customer.CustomerIban, customer.CustomerBirthDate)
                .SetAddress(customer.ContractZipcode, customer.ContractBuildingNr.GetValueOrDefault(), customer.ContractExBuildingNr).Build();
            var propositionId = incentive.PropositionId;

            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_CancelVoucherReservationJob);
            Waiter.Wait(() =>
                VoucherDb.GetEntitiesByCondition<Repositories.MMVDBModels.Voucher>(v => v.CustomerIban == customer.CustomerIban).Count == 1);

            var response = new ReserveFreeVoucherRequest
            {
                Customer = newCustomer,
                PropositionId = propositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
        }
    }
}