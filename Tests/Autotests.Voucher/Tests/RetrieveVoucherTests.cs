﻿using System.Net;
using System.ServiceModel;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using Autotests.Voucher.Builders;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class RetrieveVoucherTests : BaseVoucherTest
    {
        [Test]
        public void RetrieveVoucher_DefaultValidCase_Returns200()
        {
            var surname = $"AutoTests_{GenerateGuidString(17)}";
            var initials = GenerateGuidString(6);
            var customerBirthday = GetRandomBirthDate();
            var iban = GetRandomIbanNumber();

            var voucherWithTestCephKey = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().WithCustomer()
                .SetPersonalDetails(surname, initials, iban, customerBirthday).Build().To<Repositories.MMVDBModels.Voucher>();
            voucherWithTestCephKey.CephKey = CephUploader.CephKey;
            var dbId = AddVoucher(voucherWithTestCephKey);

            var response = new RetrieveVoucherRequest { VoucherId = dbId }
                .CallWith(VoucherClient.Proxy.RetrieveVoucher);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.VoucherContentPdf.Should().BeOfType<byte[]>();
        }

        [Test]
        public void RetrieveVoucher_WithInvalidVoucherId_Returns500()
        {
            Action action = () =>
            {
                new RetrieveVoucherRequest().CallWith(VoucherClient.Proxy.RetrieveVoucher);
            };
            action.Should().ThrowExactly<FaultException<ExceptionDetail>>()
                .And.Message.Should().BeEquivalentTo(string.Format(PatternMessages.VoucherIsNotFound, 0));
        }
    }
}
