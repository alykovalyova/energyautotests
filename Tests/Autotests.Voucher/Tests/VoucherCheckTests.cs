﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class VoucherCheckTests : BaseVoucherTest
    {
        [Test]
        public void VoucherCheck_WithNewCustomer_CanReserveVoucherIsTrue()
        {
            var response = new VoucherCheckRequest { Customer = GetCustomer() }
                .CallWith(VoucherClient.Proxy.VoucherCheck);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.CanReserveVoucher.Should().BeTrue();
        }

        [Test]
        public void VoucherCheck_WithTheSameCustomer_CanReserveVoucherIsFalse()
        {
            // Arrange
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);
            var customer = GetCustomer();

            // Reserve free voucher
            var reserveFreeVoucherResponse = new ReserveFreeVoucherRequest
            {
                Customer = customer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            reserveFreeVoucherResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            // Same client but with different IBAN
            var newIban = $"NL91ABNA{Random.Next(0, 9)}{Random.Next(111111111, 999999999)}";
            customer.PersonalDetails.Iban = newIban;

            // Check voucher is false
            var response = new VoucherCheckRequest { Customer = customer }
                .CallWith(VoucherClient.Proxy.VoucherCheck);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);
            response.CanReserveVoucher.Should().BeFalse();
        }

        [Test]
        public void VoucherCheck_WithoutCustomerField_Returns500()
        {
            var response = new VoucherCheckRequest().CallWith(VoucherClient.Proxy.VoucherCheck);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
            response.CanReserveVoucher.Should().BeFalse();
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(VoucherCheckRequest.Customer));
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}