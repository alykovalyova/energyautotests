﻿using System.Net;
using Autotests.Clients;
using Autotests.Clients.Enums;
using Autotests.Core.CsvTool;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using Autotests.Voucher.Builders;
using Autotests.Voucher.Enums;
using Autotests.Voucher.Models;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class VoucherJobsTest : BaseVoucherTest
    {
        [Test]
        public void CancelVoucher_FromMediaMarket_VoucherCanceled()
        {
            // Arrange
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            var customer = GetCustomer();

            // Add canceled voucher          
            var canceledVoucher = BuildDirector.Get<VoucherBuilder>()
                .GetBaseVoucher().WithCustomer().SetPersonalDetails(
                    customer.PersonalDetails.Surname, customer.PersonalDetails.Initials,
                    customer.PersonalDetails.Iban, customer.PersonalDetails.BirthDate)
                .SetAddress(customer.Address.ZIPCode, customer.Address.BuildingNr, customer.Address.ExBuildingNr)
                .SetAmount(incentive.VoucherAmount)
                .SetDateProperty(VoucherDateProps.ReservedOn, DateTime.Now)
                .SetDateProperty(VoucherDateProps.DistributedOn, DateTime.Now)
                .SetDateProperty(VoucherDateProps.CanceledOn, DateTime.Now)
                .Build();
            var voucherId = AddVoucher(canceledVoucher);

            // Prepare csv for Media Market
            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_RequestRestValueJob);
            Waiter.Wait(() =>
                VoucherDb.EntityIsInDb<Repositories.MMVDBModels.Voucher>(v =>
                    v.RestValueFileName != null && v.SentRestValueCephKey != null));
            var voucher = GetVoucher(voucherId);

            // Prepare response csv from Media Market
            var voucherCsv = new List<VoucherCsv>
            {
                new VoucherCsv
                {
                    ID = voucherId,
                    BRAND = "Budget",
                    VOUCHER = voucher.VoucherCode,
                    TOTAL_AMOUNT = voucher.VoucherAmount ,
                    CORRECTED_TOTAL_AMOUNT = voucher.VoucherAmount,
                    AMOUNT_BLOCKED = voucher.VoucherAmount / 2,
                    AMOUNT_USED = voucher.VoucherAmount / 2
                }
            };
            var csv = new CsvCreator<VoucherCsv>(voucherCsv, ";", true).ToBytes();
            var path = $"{Config.SftpSettings.SftpResponseCsvPath}/Budget_{DateTime.Now:yyyyMMddHHmmss}.csv";
            SftpManager.UploadFile(csv, path);

            // Process response csv from Media Market
            Scheduler.TriggerJob(QuartzJobName.Energy_MMVoucher_SftpListenerJob);
            Waiter.Wait(() => VoucherDb.GetEntityByCondition<Repositories.MMVDBModels.Voucher>(
                v => v.Id == voucherId && v.RestValue.HasValue && v.ClosedOn.HasValue));
            voucher = GetVoucher(voucherId);
            voucher.ClosedOn.Should().HaveValue("ClosedOn has NO value");
            voucher.RestValue.Should().HaveValue("RestValue has NO value");

            AddedCsvErrorResponses.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpErrorCsvPath, "*.csv"));
            AddedCsvRequests.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpRequestCsvPath, "*.csv"));
            AddedCsvRestValueSuccessResponses.AddRange(SftpManager.GetFiles(Config.SftpSettings.SftpRestValueSuccessCsvPath, "*.csv"));

            // Csv assertion
            var today = DateTime.Now.ToString("yyyyMMdd");
            AddedCsvErrorResponses.Any(error => error.Contains(today)).Should()
                .BeFalse("Import has failed, check restValueError folder");
            AddedCsvRestValueSuccessResponses.Any(error => error.Contains(today)).Should()
                .BeTrue("There is NO report for success upload, check restValueSuccess folder");

            // Reserve voucher
            var responseReserveVoucher = new ReserveFreeVoucherRequest
            {
                Customer = customer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);

            // Assert that reservation has failed
            var errorMessage = string.Format(
                PatternMessages.VoucherReceived, customer.PersonalDetails.Initials,
                customer.PersonalDetails.Surname, customer.PersonalDetails.BirthDate.ToString("dd-MM-yyyy"),
                customer.PersonalDetails.Iban);
            responseReserveVoucher.Header.StatusCode.Should().Be((int)HttpStatusCode.PreconditionFailed);
            responseReserveVoucher.Header.Message.Should().BeEquivalentTo(errorMessage);
        }
    }
}