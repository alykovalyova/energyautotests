﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Voucher.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.MMVoucher.Model.Contract;

namespace Autotests.Voucher.Tests
{
    [TestFixture]
    internal class VoucherQueueExchangeTests : BaseVoucherTest
    {
        [Test]
        public void CancelVoucher_ViaQueue_VoucherWasCanceled()
        {
            // Arrange
            var incentive = ProdManDb.GetEntityByConditionWithJoin(OuterKey, InnerKey, ResultSelector, Condition).Inner;
            AddFreeVoucherToDb(incentive.VoucherAmount);

            // Reserve free voucher
            var customer = GetCustomer();
            var response = new ReserveFreeVoucherRequest
            {
                Customer = customer,
                PropositionId = incentive.PropositionId
            }.CallWith(VoucherClient.Proxy.ReserveFreeVoucher);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            var reservedVoucherId = response.VoucherId;

            // Distribute reserved voucher
            new DistributeVoucherRequest { VoucherId = reservedVoucherId }
                .CallWith(VoucherClient.Proxy.DistributeVoucher)
                .Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            // Cancel voucher
            var cancelRequest = new Nuts.MMVoucher.Contract.Queue.CancelVoucherRequest{VoucherId = reservedVoucherId};
            Dispatcher.DispatchMessage(cancelRequest);

            // Assertion
            Waiter.Wait(() => VoucherDb.GetEntityByCondition<Repositories.MMVDBModels.Voucher>(
                v => v.Id == reservedVoucherId && v.CanceledOn != null), 20)
                .CanceledOn.Should().NotBeNull();
        }
    }
}