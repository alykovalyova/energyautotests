﻿using System.Net;
using System.Text;
using Allure.Commons;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.DbInfrastructure;
using Autotests.Core.Handlers;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Repositories.MeteringPointModels;
using Autotests.Repositories.ProspectModels;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.ProdMan.Model.Contract.Transport;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract.Transport;
using ProdManContract = Nuts.ProdMan.Model.Contract;
using ProspectContract = Nuts.Prospect.Model.Contract;
using ProspectToEdit = Nuts.Prospect.Model.Contract.Prospect;

namespace Autotests.Prospect.Base
{
    [SetUpFixture, Category(Categories.Prospect)]
    internal abstract class BaseProspectTest : AllureReport
    {
        //clients declaration
        protected Autotests.Clients.ServiceClient<ProspectContract.IProspectService> ProspectClient;
        protected Autotests.Clients.ServiceClient<ProdManContract.IProdManService> ProdManClient;
        protected RestSchedulerClient Scheduler;

        //databases declaration
        protected DbHandler<ProspectContext> ProspectDb;
        protected DbHandler<MeteringPointContext> MeteringPointDb;

        //variables initialization
        private ConfigHandler _configHandler = new ConfigHandler();
        private const string DefaultCompanyCocNumber = "12345678";
        private const string DefaultInitials = "Default";
        private const string DefaultPrefix = "pre";
        protected const Labels Label = Labels.BudgetEnergie;
        protected const ClientType ClientType = Nuts.Prospect.Core.Enums.ClientType.Business;
        protected const string PhoneHomeNumber = "+23456789012";
        protected const string PhoneMobileNumber = "+13459789012";
        protected const string Email = "test@be.nl";
        protected const string Surname = "Mask";
        protected const string CompanyName = "BudgetEnergie";
        protected const string SalesAgentName = "TestAgent";
        protected const string PostOfficeUser = "Post Office";

        protected string[] Includes => new[] { "Address", "CapTarCode", "GridArea", "ProductType", "ProfileCategory" };

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ProspectClient = new Autotests.Clients.ServiceClient<ProspectContract.IProspectService>(_configHandler.ServicesEndpoints.ProspectService);
            ProdManClient = new Autotests.Clients.ServiceClient<ProdManContract.IProdManService>(_configHandler.ServicesEndpoints.ProdManService);
            Scheduler = new RestSchedulerClient();
            ProspectDb = new DbHandler<ProspectContext>(_configHandler.GetConnectionString(DbConnectionName.ProspectDatabase.ToString()));
            MeteringPointDb = new DbHandler<MeteringPointContext>(_configHandler.GetConnectionString(DbConnectionName.MeteringPointDatabase.ToString()));
        }

        protected Tuple<string, Guid> GetProposition(Status status, TypeOfClient typeOfClient, Labels label)
        {
            var prodManServiceResponse = new SearchPropositionInfosRequest
            {
                SearchInfo = new ProdManContract.SearchCriteria
                {
                    Status = status,
                    ClientType = typeOfClient,
                    LabelCode = label
                }
            }.CallWith(ProdManClient.Proxy.SearchPropositionInfos);
            var proposition = prodManServiceResponse.PropositionInfos.RandomItem();

            return Tuple.Create(proposition.Name, proposition.Id);
        }

        protected ProspectContract.AddOffer GetAddOffer(
            int prospectId, Tuple<string, Guid> proposition,
            string zipCode, EanInfo ean)
        {
            var (propositionName, propositionId) = proposition;
            return new ProspectContract.AddOffer
            {
                ProspectId = prospectId,
                PropositionName = propositionName,
                PropositionId = propositionId,
                SalesAgentName = "SI",
                SupplyAddress = new ProspectContract.AddOfferSupplyAddress
                {
                    ZIPCode = zipCode,
                    BuildingNr = 12
                },
                MeteringPoints = new List<ProspectContract.AddOfferMeteringPoint>
                {
                    new ProspectContract.AddOfferMeteringPoint
                    {
                        ProductType = ean.ProductType,
                        Ean = ean.EanId,
                        UsageEacOffPeak = ean.EacOffPeak,
                        UsageEacPeak = ean.EacPeak
                    }
                },
            };
        }

        protected EditProspectRequest FillAllFieldsToEdit(AddProspectRequest addProspectRequest, int prospectId)
        {
            const Labels newLabel = Labels.BudgetEnergie;
            const Gender newGender = Gender.Female;
            const string newInitials = "J.W.";
            const string newPrefix = "erp";
            const string newSurname = "Wick";
            const string newCompanyName = "NewCompanyName";
            const string newCompanyCocNr = "87654321";
            const string newEmail = "edited@email.com";
            const string newPhoneNrHome = "+45678914756";
            const string newPhoneNrMobile = "+98756421894";
            const string newZipCode = "2222QQ";
            const int newBuildNr = 77;
            const string newExBuildNr = "CC";
            const string newCity = "New Kharkiv";
            const string newStreet = "New Peremoga str.";

            var request = new EditProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToEdit>(false)
            };
            request.Prospect.ProspectId = prospectId;
            request.Prospect.Label = newLabel;
            request.Prospect.Gender = newGender;
            request.Prospect.Initials = newInitials;
            request.Prospect.Prefix = newPrefix;
            request.Prospect.Surname = newSurname;
            request.Prospect.CompanyName = newCompanyName;
            request.Prospect.CompanyCocNumber = newCompanyCocNr;
            request.Prospect.EmailAddress = newEmail;
            request.Prospect.PhoneNumberHome = newPhoneNrHome;
            request.Prospect.PhoneNumberMobile = newPhoneNrMobile;
            request.Prospect.ZIPCode = newZipCode;
            request.Prospect.BuildingNr = newBuildNr;
            request.Prospect.ExBuildingNr = newExBuildNr;
            request.Prospect.City = newCity;
            request.Prospect.Street = newStreet;

            return request;
        }

        protected string AddDots(string str)
        {
            var sb = new StringBuilder(str);
            foreach (var c in str) sb.Replace($"{c}", $"{c}.");

            return sb.ToString();
        }

        protected AddOfferRequest GetAddOfferRequest(
            int prospectId,
            ProspectContract.AddOfferMeteringPoint addOfferMeteringPoint,
            ProspectContract.AddOfferSupplyAddress address)
        {
            var (propositionName, propositionId) = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            return new AddOfferRequest
            {
                Offer = new ProspectContract.AddOffer
                {
                    ProspectId = prospectId,
                    PropositionId = propositionId,
                    PropositionName = propositionName,
                    SalesAgentName = "Autotest_Agent",
                    MeteringPoints = new List<ProspectContract.AddOfferMeteringPoint>
                    {
                        addOfferMeteringPoint
                    },
                    SupplyAddress = address,
                    Cashback = 10
                }
            };
        }

        protected GetProspectResponse CreateDefaultProspect()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            // add new prospect
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            if (!addProspectResponse.Header.StatusCode.Equals((int)HttpStatusCode.OK))
                throw new Exception(addProspectResponse.Header.Message);

            // add offer to new prospect
            var addOfferResponse = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<ProspectContract.AddOfferMeteringPoint>(),
                mpTestData.Address.To<ProspectContract.AddOfferSupplyAddress>())
                .CallWith(ProspectClient.Proxy.AddOffer);
            if (!addOfferResponse.Header.StatusCode.Equals((int)HttpStatusCode.OK))
                throw new Exception(addOfferResponse.Header.Message);

            // get resulted prospect
            var resultedProspect = new GetProspectRequest { Id = addProspectResponse.ProspectId }
                .CallWith(ProspectClient.Proxy.GetProspect);
            if (!resultedProspect.Header.StatusCode.Equals((int)HttpStatusCode.OK))
                throw new Exception(resultedProspect.Header.Message);

            return resultedProspect;
        }

        protected static UpdateOfferStatusByExternalReferenceRequest GetUpdateOfferStatusByExternalReferenceRequest(
            OfferStatusCode statusCode, string comment = null,
            string exRef = null, string salesAgentName = null)
        {
            return new UpdateOfferStatusByExternalReferenceRequest
            {
                Comment = comment,
                ExternalReference = exRef,
                NewStatus = statusCode,
                SalesAgentName = salesAgentName
            };
        }

        protected AddProspectRequest GetDefaultAddProspectRequest(
            ClientType clientType = ClientType,
            Labels label = Label,
            string phoneHomeNumber = PhoneHomeNumber,
            string phoneMobileNumber = PhoneMobileNumber,
            string email = Email,
            string surname = Surname,
            string companyName = CompanyName
        )
        {
            return new AddProspectRequest
            {
                Prospect = GetDefaultProspect<ProspectContract.ProspectToSave>(clientType, label, phoneHomeNumber,
                    phoneMobileNumber, email, surname, companyName)
            };
        }

        protected AddOfferRequest GetAddOfferRequest(string zipCode, EanInfo ean, int prospectId, Tuple<string, Guid> proposition)
        {
            var (propositionName, propositionId) = proposition;
            var offer = new ProspectContract.AddOffer
            {
                Cashback = 100,
                MeteringPoints = new List<ProspectContract.AddOfferMeteringPoint>
                {
                    new ProspectContract.AddOfferMeteringPoint
                    {
                        CaptarCode = "8760077102114",
                        Ean = ean.EanId,
                        GridArea = "08206702603426",
                        IsResidential = true,
                        ProductType = ean.ProductType,
                        UsageEacOffPeak = ean.EacOffPeak,
                        UsageEacPeak = ean.EacPeak,
                        UsageProfile = ean.ProfileCategory
                    }
                },
                PropositionId = propositionId,
                ProspectId = prospectId,
                SalesAgentName = SalesAgentName,
                PropositionName = propositionName,
                SupplyAddress = new ProspectContract.AddOfferSupplyAddress
                {
                    BuildingNr = 11,
                    ZIPCode = zipCode
                },
                IsCalculationsBasedOnEdsn = true
            };
            return new AddOfferRequest
            {
                Offer = offer
            };
        }

        private static T GetDefaultProspect<T>(
            ClientType clientType, Labels label, string phoneHomeNumber, string phoneMobileNumber, string email,
            string surname, string companyName) where T : ProspectContract.ProspectBase, new()
        {
            return new T
            {
                ClientType = clientType,
                CompanyCocNumber = clientType == ClientType.Business ? DefaultCompanyCocNumber : null,
                CompanyName = clientType == ClientType.Business ? companyName : null,
                EmailAddress = email,
                Gender = Gender.Male,
                Initials = DefaultInitials,
                Label = label,
                PhoneNumberHome = phoneHomeNumber,
                PhoneNumberMobile = phoneMobileNumber,
                Prefix = DefaultPrefix,
                SourceId = 1,
                Surname = surname
            };
        }
    }
}