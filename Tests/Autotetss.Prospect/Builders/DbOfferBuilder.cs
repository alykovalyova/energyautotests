﻿using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Repositories.ProspectModels;
using Nuts.Prospect.Core.Enums;

namespace Autotests.Prospect.Builders
{
    internal class DbOfferBuilder : MainBuilder<Offer>
    {
        private static readonly string _offerName = "Offer created by Autotest " + DateTime.Now;

        internal DbOfferBuilder GetBaseOffer()
        {
            //Instance.Id = int.Parse(RandomDataProvider.GetRandomNumbersString(2));
            Instance.PropositionId = Guid.NewGuid();
            Instance.Cashback = 10;
            Instance.SalesAgentName = Guid.NewGuid().ToString();
            Instance.ExternalReference = Guid.NewGuid();
            Instance.LastComment = _offerName;
            Instance.LastStatusId = (byte) OfferStatusCode.New;
            return this;
        }

        internal DbOfferBuilder SetZipcode(string Zipcode)
        {
            Instance.Zipcode = Zipcode;
            return this;
        }

        internal DbOfferBuilder SetBuildingNr(int buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal DbOfferBuilder SetExBuildingNr(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }

        internal DbOfferBuilder SetMeteringPoint(EanInfo ean)
        {
            Instance.MeteringPoint = new List<MeteringPoint>
            {
                new MeteringPoint
                {
                    Ean = ean.EanId,
                    Offer = Instance,
                    UsageEacOffPeak = ean.EacOffPeak,
                    UsageEacPeak = ean.EacPeak,
                    Id = new int(),
                    OfferId = Instance.Id,
                    ProductTypeId = (int) ean.ProductType
                }
            };
            return this;
        }

        internal DbOfferBuilder SetLastComment(string comment)
        {
            Instance.LastComment = comment;
            return this;
        }

        internal DbOfferBuilder SetPrepositionName(string preposition)
        {
            Instance.PropositionName = preposition;
            return this;
        }

        internal DbOfferBuilder SetOfferStatusHistory(OfferStatusHistory item)
        {
            Instance.OfferStatusHistory.Add(item);
            return this;
        }

        internal DbOfferBuilder SetCity(string city)
        {
            Instance.City = city;
            return this;
        }

        internal DbOfferBuilder SetStreet(string street)
        {
            Instance.Street = street;
            return this;
        }
    }
}