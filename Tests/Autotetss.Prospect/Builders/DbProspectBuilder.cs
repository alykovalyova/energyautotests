﻿using Autotests.Core.Helpers;
using Autotests.Repositories.ProspectModels;
using Nuts.InterDom.Models.Enums;
using Nuts.Prospect.Core.Enums;

namespace Autotests.Prospect.Builders
{
    internal class DbProspectBuilder : MainBuilder<Repositories.ProspectModels.Prospect>
    {
        internal DbProspectBuilder GetBaseProspect(Labels label = Labels.BudgetEnergie,
            ClientType clientType = ClientType.Private, Gender gender = Gender.Male)
        {
            Instance.LabelId = (int) label;
            Instance.ClientTypeId = (int) clientType;
            Instance.GenderId = (int) gender;
            Instance.Offer = new List<Offer>();
            Instance.ProspectStatusId = 1;
            Instance.CreatedOn = DateTime.Now;
            return this;
        }

        internal DbProspectBuilder SetSurname(string surname)
        {
            Instance.Surname = surname;
            return this;
        }

        internal DbProspectBuilder SetPrefix(string prefix)
        {
            Instance.Prefix = prefix;
            return this;
        }

        internal DbProspectBuilder SetInitials(string initials)
        {
            Instance.Initials = initials;
            return this;
        }

        internal DbProspectBuilder SetEmail(string emailAddress)
        {
            Instance.EmailAddress = emailAddress;
            return this;
        }

        internal DbProspectBuilder SetPhoneNumberMobile(string phoneNumberMobile)
        {
            Instance.PhoneNumberMobile = phoneNumberMobile;
            return this;
        }

        public DbProspectBuilder SetPhoneNumberHome(string phoneNumberHome)
        {
            Instance.PhoneNumberHome = phoneNumberHome;
            return this;
        }

        internal DbProspectBuilder SetCompanyName(string companyName)
        {
            Instance.CompanyName = companyName;
            return this;
        }

        internal DbProspectBuilder SetCompanyCocNumber(string companyCocNumber)
        {
            Instance.CompanyCocNumber = companyCocNumber;
            return this;
        }

        internal DbProspectBuilder SetOffer(Offer offer)
        {
            Instance.Offer.Add(offer);
            return this;
        }

        internal DbProspectBuilder SetCreationOnDate(DateTime date)
        {
            Instance.CreatedOn = date;
            return this;
        }

        internal DbProspectBuilder SetZipcode(string Zipcode)
        {
            Instance.Zipcode = Zipcode;
            return this;
        }

        internal DbProspectBuilder SetBuildingNr(int buildingNr)
        {
            Instance.BuildingNr = buildingNr;
            return this;
        }

        internal DbProspectBuilder SetExBuildingNr(string exBuildingNr)
        {
            Instance.ExBuildingNr = exBuildingNr;
            return this;
        }

        internal DbProspectBuilder SetStreet(string street)
        {
            Instance.Street = street;
            return this;
        }

        internal DbProspectBuilder SetCity(string city)
        {
            Instance.City = city;
            return this;
        }
    }
}