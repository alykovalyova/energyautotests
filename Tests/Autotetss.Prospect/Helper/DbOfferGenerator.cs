﻿using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Helper;
using Autotests.Prospect.Builders;
using Autotests.Repositories.ProspectModels;
using Nuts.Prospect.Core.Enums;

namespace Autotests.Prospect.Helper
{
    public static class DbOfferGenerator
    {
        private readonly static BuildDirector BuildDirector = new BuildDirector();
        public static Offer GetDeepBelatedOffer(OfferStatusCode status)
        {
            var deepBelatedDate = DateTime.Now.AddMonths(-new Random().Next(4, 6));
            return GetOffer(status, deepBelatedDate);
        }

        public static Offer GetOffer(OfferStatusCode status, bool isActive)
        {
            var activeDate = DateTime.Now.AddMonths(-3).AddDays(1).AddHours(3);
            var inactiveDate = DateTime.Now.AddMonths(-3).AddHours(-1);
            return isActive ? GetOffer(status, activeDate) : GetOffer(status, inactiveDate);
        }

        private static Offer GetOffer(OfferStatusCode status, DateTime createdOn)
        {
            return BuildDirector.Get<DbOfferBuilder>()
                .GetBaseOffer().SetMeteringPoint(TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg])
                .SetBuildingNr(1).SetExBuildingNr(RandomDataProvider.GetRandomNumbersString(1))
                .SetZipcode("1111AA").SetLastComment("Last Comment")
                .SetPrepositionName(RandomDataProvider.GetRandomLettersString(10))
                .SetCity(RandomDataProvider.GetRandomLettersString(10))
                .SetStreet(RandomDataProvider.GetRandomLettersString(10))
                .SetOfferStatusHistory(new OfferStatusHistory
                {
                    StatusId = (int)status,
                    CreatedOn = createdOn,
                    Comment = "test comment"
                }).Build();
        }

        public static List<Offer> MakeOneAddress(List<Offer> offers)
        {
            var commonBuildingNr = offers[0].BuildingNr;
            var commonExBuildingNr = offers[0].ExBuildingNr;
            var commonZipcode = offers[0].Zipcode;
            offers.ForEach(item =>
            {
                item.BuildingNr = commonBuildingNr;
                item.ExBuildingNr = commonExBuildingNr;
                item.Zipcode = commonZipcode;
            });

            return offers;
        }
    }
}