﻿using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Helper;
using Autotests.Prospect.Builders;
using Nuts.Prospect.Core.Enums;

namespace Autotests.Prospect.Helper
{
    public static class DbProspectGenerator
    {
        private static readonly BuildDirector BuildDirector = new BuildDirector();
        public static Repositories.ProspectModels.Prospect GetProspectWithOffers(bool isActive, OfferStatusCode status, int count = 1)
        {
            var prospect = GetProspect(isActive);
            for (var i = 0; i < count; i++)
            {
                prospect.Offer.Add(DbOfferGenerator.GetOffer(status, isActive));
            }
            return prospect;
        }

        public static Repositories.ProspectModels.Prospect GetProspect(bool isActive)
        {
            var prospect = BuildDirector.Get<DbProspectBuilder>()
                .GetBaseProspect()
                .SetInitials(RandomDataProvider.GetRandomLettersString(10))
                .SetPrefix(RandomDataProvider.GetRandomLettersString(10))
                .SetSurname(RandomDataProvider.GetRandomLettersString(10))
                .SetCompanyName(RandomDataProvider.GetRandomLettersString(10))
                .SetCompanyCocNumber(RandomDataProvider.GetRandomLettersString(10))
                .SetEmail(RandomDataProvider.GetRandomEmailString(10))
                .SetPhoneNumberHome(RandomDataProvider.GetRandomNumbersString(10))
                .SetBuildingNr(new Random().Next(1, 9))
                .SetCity(RandomDataProvider.GetRandomLettersString(10))
                .SetExBuildingNr(RandomDataProvider.GetRandomNumbersString(1))
                .SetStreet(RandomDataProvider.GetRandomLettersString(10))
                .SetZipcode("1111AA")
                .SetPhoneNumberMobile(RandomDataProvider.GetRandomNumbersString(10))
                .SetCreationOnDate(isActive
                    ? DateTime.Now.AddMonths(-3).AddDays(1).AddHours(3)
                    : DateTime.Now.AddMonths(-3).AddHours(-1)).Build();

            return prospect;
        }
    }
}