﻿using AutoMapper;
using Autotests.Repositories.MeteringPointModels;
using Nuts.Prospect.Model.Contract;
using ProspectToEdit = Nuts.Prospect.Model.Contract.Prospect;

namespace Autotests.Framework.MapProfiles
{
    public class ProspectConfigProfile : Profile
    {
        public ProspectConfigProfile()
        {
            CreateMap<Address, AddOfferSupplyAddress>()
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.CityName))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.StreetName))
                .ForMember(dest => dest.ContractInfo, opt => opt.Ignore());
            CreateMap<MeteringPoint, AddOfferMeteringPoint>()
                .ForMember(dest => dest.UsageEacPeak, opt => opt.MapFrom(src => src.EacPeak))
                .ForMember(dest => dest.UsageEacOffPeak, opt => opt.MapFrom(src => src.EacOffPeak))
                .ForMember(dest => dest.UsageEapPeak, opt => opt.MapFrom(src => src.EapPeak))
                .ForMember(dest => dest.UsageEapOffPeak, opt => opt.MapFrom(src => src.EapOffPeak))
                .ForMember(dest => dest.Ean, opt => opt.MapFrom(src => src.EanId))
                .ForMember(dest => dest.CaptarCode, opt => opt.MapFrom(src => src.CapTarCode.Ean))
                .ForMember(dest => dest.GridArea, opt => opt.MapFrom(src => src.GridArea.Ean))
                .ForMember(dest => dest.IsResidential, opt => opt.Ignore())
                .ForMember(dest => dest.ProductType, opt => opt.MapFrom(src => src.ProductType.Identifier))
                .ForMember(dest => dest.UsageProfile, opt => opt.MapFrom(src => src.ProfileCategory.Identifier));
            CreateMap<AddOffer, EditOffer>()
                .ForMember(dest => dest.OfferId, opt => opt.Ignore());
            CreateMap<AddOfferMeteringPoint, EditOfferMeteringPoint>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<AddOfferSupplyAddress, EditOfferSupplyAddress>();
            CreateMap<AddContractInfo, EditContractInfo>();
            CreateMap<ProspectToSave, ProspectToCheck>()
                .ForMember(dest => dest.ProspectId, opt => opt.Ignore());
            CreateMap<ProspectToSave, ProspectToEdit>()
                .ForMember(dest => dest.ProspectId, opt => opt.Ignore());
        }
    }
}