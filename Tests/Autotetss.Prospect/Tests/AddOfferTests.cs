﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class AddOfferTests : BaseProspectTest
    {
        [Test]
        public void AddOffer_DefaultValidCase_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var response = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>())
                .CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferId.Should().NotBe(0);
            ProspectDb.GetEntityByCondition<Offer>(o => o.Id == response.OfferId)
                .Should().NotBeNull("Created Offer should be in DB.");
        }

        [Test]
        public void AddOffer_WithInvalidProspectId_Returns400()
        {
            const int prospectId = 0;
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);

            var response = GetAddOfferRequest(prospectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>())
                .CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidProspectIdForOffer);
        }

        [Test]
        public void AddOffer_WithEmptyPropositionId_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.PropositionId = Guid.Empty;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PropositionIdIsRequired);
        }

        [Test]
        public void AddOffer_WithEmptyPropositionName_Returns400([Values("", null)] string propositionName)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.PropositionName = propositionName;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Offer.PropositionName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithEmptySalesAgentName_Returns400([Values("", null)] string salesAgentName)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SalesAgentName = salesAgentName;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Offer.SalesAgentName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithOverMaxSalesAgentNameLength_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SalesAgentName = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldOverMaxLength, nameof(request.Offer.SalesAgentName), 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithMeteringPointListIsNull_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.MeteringPoints = null;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Offer.MeteringPoints));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithMeteringPointListIsEmpty_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.MeteringPoints = new List<AddOfferMeteringPoint>();
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.AddOfferMpsIsEmpty);
        }

        [Test]
        public void AddOffer_WithMeteringPointWithEmptyEan_Returns400([Values("", null)] string ean)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.MeteringPoints[0].Ean = ean;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, "Ean");
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithMeteringPointWithInvalidEanRegex_Returns400(
            [Values("12345678912345678", "1234567891234567891", "ffffffffffffffffff", "!@#$%^&*()_+!@#$%")] string ean)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.MeteringPoints[0].Ean = ean;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.AddOfferEanInvalidRegex);
        }

        [Test]
        public void AddOffer_WithEmptySupplyAddress_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SupplyAddress = null;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Offer.SupplyAddress));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithSupplyAddressWithInvalidZipcodeRegex_Returns400([Values("111QQ", "1111Q", "1111", "QQQQ", "1111QQQ")] string Zipcode)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6, Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SupplyAddress.ZIPCode = Zipcode;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ZipCodeFieldRegexIsWrong);
        }

        [Test]
        public void AddOffer_WithSupplyAddressWithInvalidBuildingNr_Returns400([Values(0, 100000)] int buildingNr)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SupplyAddress.BuildingNr = buildingNr;
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidBuildingNr);
        }

        [Test]
        public void AddOffer_WithSupplyAddressWithOverMaxExBuildingNr_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SupplyAddress.ExBuildingNr = new string('a', 7);
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, "ExBuilding number", 6);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithSupplyAddressWithOverMaxCity_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SupplyAddress.City = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, "City name", 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddOffer_WithSupplyAddressWithOverMaxStreet_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            request.Offer.SupplyAddress.Street = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.AddOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, "Street name", 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}