﻿using System.Net;
using Autotests.Clients;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract.Transport;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class AddProspectTests : BaseProspectTest
    {
        [Test, Pairwise]
        public void AddProspect_ForLabels_Returns200(
            [Values(Labels.BudgetEnergie, Labels.NLE)] Labels label,
            [Values(ClientType.Business, ClientType.Private)] ClientType clientType)
        {
            const string surname = "AutotestSurname";
            
            var response = GetDefaultAddProspectRequest(clientType, label, surname: surname)
                .CallWith(ProspectClient.Proxy.AddProspect);
            var prospectResponse = new GetProspectRequest{Id = response.ProspectId}
                .CallWith(ProspectClient.Proxy.GetProspect);

            prospectResponse.Header.StatusCode.Should().Be((int) HttpStatusCode.OK, prospectResponse.Header.Message);
            prospectResponse.GetProspect.ClientType.Should().BeEquivalentTo(clientType);
            prospectResponse.GetProspect.Label.Should().BeEquivalentTo(label);
            prospectResponse.GetProspect.Surname.Should().BeEquivalentTo(surname);
        }

        [Test,Category(Autotests.Core.Helpers.Categories.Prospect)]
        public void AddProspect_PrivateProspectWithCompanyName_Returns400()
        {
            const ClientType defaultClientType = ClientType.Private;
            var request = GetDefaultAddProspectRequest(defaultClientType);
            request.Prospect.CompanyName = "AddPrivateProspectWithCompanyName";

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Autotests.Core.Helpers.PatternMessages.PrivateClientWithCompanyName);
        }

        [Test]
        public void AddProspect_AddBusinessProspectWithNoCompanyName_Returns400()
        {
            var request = GetDefaultAddProspectRequest();
            request.Prospect.CompanyName = string.Empty;
            request.Prospect.CompanyCocNumber = string.Empty;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Autotests.Core.Helpers.PatternMessages.BusinessClientWithoutCompanyName);
        }

        [Test]
        public void AddProspect_WithInvalidEmailAddress_Returns400()
        {
            var request = GetDefaultAddProspectRequest();
            request.Prospect.EmailAddress = "test@";

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidEmail);
        }

        [Test]
        public void AddProspect_WithoutEmailAddress_Returns400()
        {
            var request = GetDefaultAddProspectRequest();
            request.Prospect.EmailAddress = string.Empty;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Prospect.EmailAddress));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddProspect_WithNullEmailAddress_Returns400()
        {
            var request = GetDefaultAddProspectRequest();
            request.Prospect.EmailAddress = null;
            
            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Prospect.EmailAddress));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddProspect_WithWrongPhoneHomeNumber_Returns400()
        {
            const string wrongPhone = "76412";
            var request = GetDefaultAddProspectRequest();
            request.Prospect.PhoneNumberHome = wrongPhone;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidHomePhoneNr, wrongPhone);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddProspect_WithWrongPhoneMobileNumber_Returns400()
        {
            const string wrongPhone = "76412";
            var request = GetDefaultAddProspectRequest();
            request.Prospect.PhoneNumberMobile = wrongPhone;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidMobilePhoneNr, wrongPhone);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void AddProspect_WithInitialsWithDot_Returns200()
        {
            const string newInitials = "IT.";
            const string expectedInitials = "I.T.";
            var request = GetDefaultAddProspectRequest();
            request.Prospect.Initials = newInitials;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            var initialsRecordedInDb = ProspectDb.GetEntityByCondition<Autotests.Repositories.ProspectModels.Prospect>(p => p.Id == response.ProspectId).Initials;
            initialsRecordedInDb.Should().BeEquivalentTo(expectedInitials);
        }
        
        [Test]
        public void AddProspect_WithInitialsLengthMoreTenSymbols_Returns400()
        {
            var newInitials = new string('a', 11);
            var request = GetDefaultAddProspectRequest();
            request.Prospect.Initials = newInitials;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InitialsFieldOverMaxLength);
        }

        [Test]
        public void AddProspect_WithInitialsStartingWithDot_Returns200()
        {
            const string newInitials = ".IT";
            const string expectedInitials = "I.T.";
            var request = GetDefaultAddProspectRequest();
            request.Prospect.Initials = newInitials;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            var initialsRecordedInDb = ProspectDb.GetEntityByCondition<Autotests.Repositories.ProspectModels.Prospect>(p => p.Id == response.ProspectId).Initials;
            initialsRecordedInDb.Should().BeEquivalentTo(expectedInitials);
        }

        [Test]
        public void AddProspect_WithSurnameIsEmpty_Returns400([Values("", null)] string surname)
        {
            var request = GetDefaultAddProspectRequest();
            request.Prospect.Surname = surname;

            var response = request.CallWith(ProspectClient.Proxy.AddProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Prospect.Surname));
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}