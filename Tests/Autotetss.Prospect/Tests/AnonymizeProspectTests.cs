﻿using Autotests.Clients.Enums;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Prospect.Helper;
using NUnit.Framework;
using Nuts.Prospect.Core.Enums;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class AnonymizeProspectTests : BaseProspectTest
    {
        [Test]
        public void AnonymizeProspectWithoutOffers()
        {
            const string s = "*";
            const int time = 20;
            var newActiveProspectId = ProspectDb.AddEntityToDb(DbProspectGenerator.GetProspect(false)).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);

            Assert.DoesNotThrow(() => Waiter.Wait(() => ProspectDb.EntityIsInDb<Repositories.ProspectModels.Prospect>(
                p => p.Id == newActiveProspectId && p.Initials == s && p.Prefix == s
                     && p.Surname == s && p.CompanyName == s && p.CompanyCocNumber == s
                     && p.EmailAddress == s && p.PhoneNumberHome == s && p.PhoneNumberMobile == s), time),
                $"Prospect hasn't been found by condition in {time} sec.");
        }

        [Test]
        public void AnonymizeOneInactiveOffer([Values(
                OfferStatusCode.Canceled, OfferStatusCode.TooLate, OfferStatusCode.Approved,
                OfferStatusCode.Accepted, OfferStatusCode.Rejected)] OfferStatusCode status)
        {
            const string s = "*";
            var newActiveProspectId =
                ProspectDb.AddEntityToDb(DbProspectGenerator.GetProspectWithOffers(false, status)).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);
            Assert.DoesNotThrow(() =>
            {
                Waiter.Wait(() => ProspectDb.EntityIsInDb<Repositories.ProspectModels.Prospect>(
                    p => p.Id == newActiveProspectId && p.Initials == s && p.Prefix == s && p.Surname == s && p.CompanyName == s
                         && p.CompanyCocNumber == s && p.EmailAddress == s && p.PhoneNumberHome == s && p.PhoneNumberMobile == s
                         && p.ExBuildingNr == s && p.Zipcode == s
                         && p.Offer.ToList().All(o => o.ExBuildingNr == s && o.LastComment == s && o.BuildingNr == 0
                                                      && o.OfferStatusHistory.ToList().All(os => os.Comment == s)
                                                      && o.MeteringPoint.ToList().All(mp => mp.Ean == s))));
            });
        }

        [Test]
        public void AnonymizeOneActiveOffer(
            [Values(OfferStatusCode.Canceled, OfferStatusCode.TooLate, OfferStatusCode.Approved,
                OfferStatusCode.Accepted,
                OfferStatusCode.Rejected)]
            OfferStatusCode status)
        {
            var newActiveProspect = DbProspectGenerator.GetProspectWithOffers(isActive: true, status: status);
            var newActiveProspectId = ProspectDb.AddEntityToDb(newActiveProspect).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);
            Thread.Sleep(3000);

            var actualProspect = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.Prospect>(p => p.Id == newActiveProspectId, "Offer.OfferStatusHistory");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(newActiveProspect.Initials, actualProspect.Initials);
                Assert.AreEqual(newActiveProspect.Prefix, actualProspect.Prefix);
                Assert.AreEqual(newActiveProspect.Surname, actualProspect.Surname);
                Assert.AreEqual(newActiveProspect.CompanyName, actualProspect.CompanyName);
                Assert.AreEqual(newActiveProspect.CompanyCocNumber, actualProspect.CompanyCocNumber);
                Assert.AreEqual(newActiveProspect.EmailAddress, actualProspect.EmailAddress);
                Assert.AreEqual(newActiveProspect.PhoneNumberHome, actualProspect.PhoneNumberHome);
                Assert.AreEqual(newActiveProspect.PhoneNumberMobile, actualProspect.PhoneNumberMobile);
                Assert.AreEqual(newActiveProspect.ExBuildingNr, actualProspect.ExBuildingNr);
                Assert.AreEqual(newActiveProspect.Zipcode, actualProspect.Zipcode);

                var expectedOffer = newActiveProspect.Offer.ToList()[0];
                var offer = actualProspect.Offer.ToList()[0];
                Assert.AreEqual(expectedOffer.ExBuildingNr, offer.ExBuildingNr);
                Assert.AreEqual(expectedOffer.LastComment, offer.LastComment);
                Assert.AreEqual(expectedOffer.BuildingNr, offer.BuildingNr);

                var expectedOfferHistory = expectedOffer.OfferStatusHistory.ToList()[0];
                var offerHistory = offer.OfferStatusHistory.ToList()[0];
                Assert.AreEqual(expectedOfferHistory.Comment, offerHistory.Comment);

                var expectedMeteringPoint = expectedOffer.MeteringPoint.ToList()[0];
                var meteringPoint = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.MeteringPoint>(m => m.OfferId == offer.Id);
                Assert.AreEqual(expectedMeteringPoint.Ean, meteringPoint.Ean);
            });
        }

        [Test]
        public void AnonymizeTwoInactiveOffersOneAddress()
        {
            const string s = "*";
            var newActiveProspect = DbProspectGenerator.GetProspect(false);
            newActiveProspect.Offer.Add(DbOfferGenerator.GetOffer(OfferStatusCode.Approved, false));
            newActiveProspect.Offer.Add(DbOfferGenerator.GetDeepBelatedOffer(OfferStatusCode.Rejected));
            newActiveProspect.Offer = DbOfferGenerator.MakeOneAddress(newActiveProspect.Offer.ToList());

            var newActiveProspectId = ProspectDb.AddEntityToDb(newActiveProspect).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);
            Assert.DoesNotThrow(() =>
            {
                Waiter.Wait(() => ProspectDb.EntityIsInDb<Repositories.ProspectModels.Prospect>(
                    p => p.Id == newActiveProspectId && p.Initials == s && p.Prefix == s && p.Surname == s && p.CompanyName == s
                         && p.CompanyCocNumber == s && p.EmailAddress == s && p.PhoneNumberHome == s && p.PhoneNumberMobile == s
                         && p.ExBuildingNr == s && p.Zipcode == s
                         && p.Offer.ToList().All(o => o.ExBuildingNr == s && o.LastComment == s && o.BuildingNr == 0
                                                      && o.OfferStatusHistory.ToList().All(os => os.Comment == s)
                                                      && o.MeteringPoint.ToList().All(mp => mp.Ean == s))));
            });
        }

        [Test]
        public void AnonymizeTwoOffersOneActiveOneAddress()
        {
            var newActiveProspect = DbProspectGenerator.GetProspect(true);
            newActiveProspect.Offer.Add(DbOfferGenerator.GetOffer(OfferStatusCode.Accepted, true));
            newActiveProspect.Offer.Add(DbOfferGenerator.GetDeepBelatedOffer(OfferStatusCode.Canceled));
            newActiveProspect.Offer = DbOfferGenerator.MakeOneAddress(newActiveProspect.Offer.ToList());
            var newActiveProspectId = ProspectDb.AddEntityToDb(newActiveProspect).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);
            Thread.Sleep(1000);

            var actualProspect = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.Prospect>(p => p.Id == newActiveProspectId, "Offer.OfferStatusHistory");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(newActiveProspect.Initials, actualProspect.Initials);
                Assert.AreEqual(newActiveProspect.Prefix, actualProspect.Prefix);
                Assert.AreEqual(newActiveProspect.Surname, actualProspect.Surname);
                Assert.AreEqual(newActiveProspect.CompanyName, actualProspect.CompanyName);
                Assert.AreEqual(newActiveProspect.CompanyCocNumber, actualProspect.CompanyCocNumber);
                Assert.AreEqual(newActiveProspect.EmailAddress, actualProspect.EmailAddress);
                Assert.AreEqual(newActiveProspect.PhoneNumberHome, actualProspect.PhoneNumberHome);
                Assert.AreEqual(newActiveProspect.PhoneNumberMobile, actualProspect.PhoneNumberMobile);
                Assert.AreEqual(newActiveProspect.ExBuildingNr, actualProspect.ExBuildingNr);
                Assert.AreEqual(newActiveProspect.Zipcode, actualProspect.Zipcode);

                for (var i = 0; i < actualProspect.Offer.Count; i++)
                {
                    var expectedOffer = newActiveProspect.Offer.ToList()[i];
                    var offer = actualProspect.Offer.ToList()[i];
                    Assert.AreEqual(expectedOffer.ExBuildingNr, offer.ExBuildingNr);
                    Assert.AreEqual(expectedOffer.LastComment, offer.LastComment);
                    Assert.AreEqual(expectedOffer.BuildingNr, offer.BuildingNr);

                    var expectedOfferHistory = expectedOffer.OfferStatusHistory.ToList()[0];
                    var offerHistory = offer.OfferStatusHistory.ToList()[0];
                    Assert.AreEqual(expectedOfferHistory.Comment, offerHistory.Comment);

                    var expectedMeteringPoint = expectedOffer.MeteringPoint.ToList()[0];
                    var meteringPoint = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.MeteringPoint>(m => m.OfferId == offer.Id);
                    Assert.AreEqual(expectedMeteringPoint.Ean, meteringPoint.Ean);
                }
            });
        }

        [Test]
        public void AnonymizeTwoInactiveOffersDifferentAddresses()
        {
            var newActiveProspect = DbProspectGenerator.GetProspect(false);
            newActiveProspect.Offer.Add(DbOfferGenerator.GetOffer(OfferStatusCode.Approved, false));
            newActiveProspect.Offer.Add(DbOfferGenerator.GetDeepBelatedOffer(OfferStatusCode.Canceled));
            newActiveProspect.Offer = DbOfferGenerator.MakeOneAddress(newActiveProspect.Offer.ToList());

            var newActiveProspectId = ProspectDb.AddEntityToDb(newActiveProspect).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);
            const string s = "*";
            Assert.DoesNotThrow(() =>
            {
                Waiter.Wait(() => ProspectDb.EntityIsInDb<Repositories.ProspectModels.Prospect>(
                    p => p.Id == newActiveProspectId && p.Initials == s && p.Prefix == s && p.Surname == s && p.CompanyName == s
                         && p.CompanyCocNumber == s && p.EmailAddress == s && p.PhoneNumberHome == s && p.PhoneNumberMobile == s
                         && p.ExBuildingNr == s && p.Zipcode == s
                         && p.Offer.ToList().All(o => o.ExBuildingNr == s && o.LastComment == s && o.BuildingNr == 0
                                                      && o.OfferStatusHistory.ToList().All(os => os.Comment == s)
                                                      && o.MeteringPoint.ToList().All(mp => mp.Ean == s))));
            });
        }

        [Test]
        public void AnonymizeOffersForDifferentAddressesOneActive()
        {
            var newActiveProspect = DbProspectGenerator.GetProspect(true);
            newActiveProspect.Offer.Add(DbOfferGenerator.GetOffer(OfferStatusCode.Approved, true));
            newActiveProspect.Offer.Add(DbOfferGenerator.GetDeepBelatedOffer(OfferStatusCode.Canceled));
            newActiveProspect.Offer = DbOfferGenerator.MakeOneAddress(newActiveProspect.Offer.ToList());
            var newActiveProspectId = ProspectDb.AddEntityToDb(newActiveProspect).Id;

            Scheduler.TriggerJob(QuartzJobName.Energy_Prospect_DailyCheckJob);
            Thread.Sleep(1000);

            var actualProspect = ProspectDb
                .GetEntityByCondition<Repositories.ProspectModels.Prospect>(p => p.Id == newActiveProspectId, "Offer.OfferStatusHistory");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(newActiveProspect.Initials, actualProspect.Initials);
                Assert.AreEqual(newActiveProspect.Prefix, actualProspect.Prefix);
                Assert.AreEqual(newActiveProspect.Surname, actualProspect.Surname);
                Assert.AreEqual(newActiveProspect.CompanyName, actualProspect.CompanyName);
                Assert.AreEqual(newActiveProspect.CompanyCocNumber, actualProspect.CompanyCocNumber);
                Assert.AreEqual(newActiveProspect.EmailAddress, actualProspect.EmailAddress);
                Assert.AreEqual(newActiveProspect.PhoneNumberHome, actualProspect.PhoneNumberHome);
                Assert.AreEqual(newActiveProspect.PhoneNumberMobile, actualProspect.PhoneNumberMobile);
                Assert.AreEqual(newActiveProspect.ExBuildingNr, actualProspect.ExBuildingNr);
                Assert.AreEqual(newActiveProspect.Zipcode, actualProspect.Zipcode);

                for (int i = 0; i < actualProspect.Offer.Count; i++)
                {
                    var expectedOffer = newActiveProspect.Offer.ToList()[i];
                    var offer = actualProspect.Offer.ToList()[i];
                    Assert.AreEqual(expectedOffer.ExBuildingNr, offer.ExBuildingNr);
                    Assert.AreEqual(expectedOffer.LastComment, offer.LastComment);
                    Assert.AreEqual(expectedOffer.BuildingNr, offer.BuildingNr);

                    var expectedOfferHistory = expectedOffer.OfferStatusHistory.ToList()[0];
                    var offerHistory = offer.OfferStatusHistory.ToList()[0];
                    Assert.AreEqual(expectedOfferHistory.Comment, offerHistory.Comment);

                    var expectedMeteringPoint = expectedOffer.MeteringPoint.ToList()[0];
                    var meteringPoint = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.MeteringPoint>(m => m.OfferId == offer.Id);
                    Assert.AreEqual(expectedMeteringPoint.Ean, meteringPoint.Ean);
                }
            });
        }
    }
}