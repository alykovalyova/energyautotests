﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;
using static System.String;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class CheckProspectTests : BaseProspectTest
    {
        [Test]
        public void CheckProspect_DefaultValidCase_Returns200()
        {
            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var response = new CheckProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToCheck>(false)
            }.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            var prospectIds = response.Prospects.Select(p => p.ProspectId).ToList();
            prospectIds.Should().Contain(addProspectResponse.ProspectId);
        }

        [Test]
        public void CheckProspect_OverMaxLengthOfInitials_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Initials = new string('a', 11);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InitialsFieldOverMaxLength);
        }

        [Test]
        public void CheckProspect_InvalidInitials_Returns400([Values("123", "!@#$%^&*()_+")] string initials)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Initials = initials;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.WrongFormatInitials, initials));
        }

        [Test]
        public void CheckProspect_OverMaxLengthOfPrefix_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Prefix = new string('a', 21);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.FieldOverMaxLength, "Prefix", 20));
        }

        [Test]
        public void CheckProspect_InvalidPrefix_Returns400([Values("123", "!@#$%^&*()_+")] string prefix)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Prefix = prefix;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.WrongFormatPrefix, prefix));
        }

        [Test]
        public void CheckProspect_OverMaxLengthOfSurname_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Surname = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.FieldOverMaxLength, "Surname", 50));
        }

        [Test]
        public void CheckProspect_InvalidSurname_Returns400([Values("123", "!@#$%^&*()_+")] string surname)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Surname = surname;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.WrongFormatSurname, surname));
        }

        [Test]
        public void CheckProspect_LengthOfSurnameLessThan2_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Surname = "a";
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.FieldLessThan2, nameof(CheckProspectRequest.Prospect.Surname)));
        }

        [Test]
        public void CheckProspect_SurnameIsEmpty_Returns400([Values("", null)] string surname)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Surname = surname;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.FieldIsRequired, nameof(CheckProspectRequest.Prospect.Surname)));
        }

        [Test]
        public void CheckProspect_PrivateProspectWithCompanyName_Returns400()
        {
            const ClientType clientType = ClientType.Private;
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest(clientType).Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.CompanyName = "AddPrivateProspectWithCompanyName";
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.PrivateClientWithCompanyName));
        }

        [Test]
        public void CheckProspect_BusinessProspectWithoutCompanyName_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.CompanyName = Empty;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.BusinessClientWithoutCompanyName));
        }

        [Test]
        public void CheckProspect_OverMaxLengthOfCompanyName_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.CompanyName = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldOverMaxLength, nameof(CheckProspectRequest.Prospect.CompanyName), 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_OverMaxLengthOfCompanyCocNumber_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.CompanyCocNumber = new string('a', 21);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldOverMaxLength, nameof(CheckProspectRequest.Prospect.CompanyCocNumber), 20);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_InvalidCompanyCocNumberFormat_Returns400(
            [Values("123", "!@#$%^&*()_+")] string companyCocNr)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.CompanyCocNumber = companyCocNr;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.WrongFormatCocNr, companyCocNr));
        }

        [Test]
        public void CheckProspect_PrivateProspectWithCompanyCocNr_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest(ClientType.Private).Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.CompanyCocNumber = "aaa";
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PrivateClientWithCompanyCocNr);
        }

        [Test]
        public void CheckProspect_InvalidEmailAddress_Returns400([Values("aaaa", "aaa@", "@aaa.com", "aaa@aaa")]
            string email)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.EmailAddress = email;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidEmail);
        }

        [Test]
        public void CheckProspect_WithoutEmailAddress_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.EmailAddress = Empty;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldIsRequired, nameof(request.Prospect.EmailAddress));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_OverMaxPhoneNrHome_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.PhoneNumberHome = new string('a', 21);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldOverMaxLength, nameof(CheckProspectRequest.Prospect.PhoneNumberHome), 20);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_OverMaxPhoneNrMobile_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.PhoneNumberMobile = new string('a', 21);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.FieldOverMaxLength, nameof(CheckProspectRequest.Prospect.PhoneNumberMobile), 20);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_InvalidIban_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Iban = "aaa";
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.ProspectInvalidIban, request.Prospect.Iban));
        }

        [Test]
        public void CheckProspect_BirthdayIsLessThan18Years_Returns400()
        {
            var birthday = DateTime.Now.AddYears(-17);
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Birthday = birthday;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.BirthdayLessThan18, birthday));
        }

        [Test]
        public void CheckProspect_BirthdayIsMoreThan125Years_Returns400()
        {
            var birthday = DateTime.Now.AddYears(-126);
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Birthday = birthday;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(Format(PatternMessages.BirthdayMoreThan125, birthday));
        }

        [Test]
        public void CheckProspect_InvalidZipcode_Returns400([Values("111QQ", "1111Q", "1111", "QQQQ", "1111QQQ")]
            string zipcode)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.ZIPCode = zipcode;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ZipCodeFieldRegexIsWrong);
        }

        [Test]
        public void CheckProspect_InvalidBuildingNr_Returns400([Values(0, 100000)] int buildingNr)
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.BuildingNr = buildingNr;
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidBuildingNr);
        }

        [Test]
        public void CheckProspect_OverMaxExBuildingNrLength_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.ExBuildingNr = new string('a', 7);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.InvalidFieldLength, "ExBuilding number", 6);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_OverMaxCityLength_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.City = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.InvalidFieldLength, "City name", 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void CheckProspect_OverMaxStreetLength_Returns400()
        {
            var request = new CheckProspectRequest
            {
                Prospect = GetDefaultAddProspectRequest().Prospect
                    .To<ProspectToCheck>()
            };
            request.Prospect.Street = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.CheckProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = Format(PatternMessages.InvalidFieldLength, "Street name", 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}