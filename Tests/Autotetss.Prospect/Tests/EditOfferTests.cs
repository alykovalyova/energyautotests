﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class EditOfferTests : BaseProspectTest
    {
        [Test]
        public void EditOffer_PropositionDataValidCase_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);
            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.MeteringPoints[0].Id = mpTestData.Id;
            var (propositionName, propositionId) = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            request.Offer.PropositionId = propositionId;
            request.Offer.PropositionName = propositionName;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferId.Should().NotBe(0);
            var editedOffer = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);
            editedOffer.PropositionId.Should().Be(propositionId);
            editedOffer.PropositionName.Should().BeEquivalentTo(propositionName);
        }

        [Test]
        public void EditOffer_SalesAgentNameValidCase_Returns200()
        {
            const string newAgentName = "New_Prospect_Agent_Name";
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.MeteringPoints[0].Id = mpTestData.Id;
            request.Offer.SalesAgentName = newAgentName;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferId.Should().NotBe(0);
            ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId)
                .SalesAgentName.Should().BeEquivalentTo(newAgentName);
        }

        [Test]
        public void EditOffer_CashBackValidCase_Returns200()
        {
            const decimal newCashBack = 15;
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.MeteringPoints[0].Id = mpTestData.Id;
            request.Offer.Cashback = newCashBack;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferId.Should().NotBe(0);
            ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId)
                .Cashback.Should().Be(newCashBack);
        }

        [Test]
        public void EditOffer_MeteringPointsValidCase_Returns200()
        {
            const int newUsagePeak = 4500;
            const int newUsageOffPeak = 999;
            const string testZipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);
            var prospectId = addProspectResponse.ProspectId;
            var addOffer = GetAddOffer(prospectId, activeProposition, testZipcode, ean);

            var addOfferResponse = new AddOfferRequest { Offer = addOffer }
                .CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);
            var offerId = addOfferResponse.OfferId;
            var meteringPointId = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.MeteringPoint>(m => m.OfferId == offerId).Id;

            var request = new EditOfferRequest { Offer = addOffer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.MeteringPoints[0].Id = meteringPointId;
            request.Offer.MeteringPoints[0].Ean = ean.EanId;
            request.Offer.MeteringPoints[0].UsageEacPeak = newUsagePeak;
            request.Offer.MeteringPoints[0].UsageEacOffPeak = newUsageOffPeak;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferId.Should().NotBe(0);
            var mp = ProspectDb.GetEntityByCondition<Repositories.ProspectModels.MeteringPoint>(m => m.OfferId == offerId);
            mp.UsageEacPeak.Should().Be(newUsagePeak);
            mp.UsageEacOffPeak.Should().Be(newUsageOffPeak);
        }

        [Test]
        public void EditOffer_WithInvalidOfferId_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = 0;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.IdIsNotBetweenValues, nameof(request.Offer.OfferId));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void EditOffer_WithEmptyPropositionId_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.PropositionId = Guid.Empty;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PropositionIdIsRequired);
        }

        [Test]
        public void EditOffer_WithEmptyPropositionName_Returns400([Values("", null)] string propositionName)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.PropositionName = propositionName;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Offer.PropositionName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void EditOffer_WithEmptySalesAgentName_Returns400([Values("", null)] string salesAgentName)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.SalesAgentName = salesAgentName;
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(request.Offer.SalesAgentName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void EditOffer_WithOverMaxSalesAgentNameLength_Returns400()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var request = new EditOfferRequest { Offer = addOfferRequest.Offer.To<EditOffer>() };
            request.Offer.OfferId = addOfferResponse.OfferId;
            request.Offer.SalesAgentName = new string('a', 51);
            var response = request.CallWith(ProspectClient.Proxy.EditOffer);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldOverMaxLength, nameof(request.Offer.SalesAgentName), 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}