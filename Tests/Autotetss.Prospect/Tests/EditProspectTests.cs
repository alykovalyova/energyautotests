﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract.Transport;
using ProspectToEdit = Nuts.Prospect.Model.Contract.Prospect;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class EditProspectTests : BaseProspectTest
    {
        [Test]
        public void EditProspect_ClientTypeToPrivateValidCase_Returns200()
        {
            const ClientType clientType = ClientType.Private;
            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = new EditProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToEdit>()
            };
            request.Prospect.ProspectId = addProspectResponse.ProspectId;
            request.Prospect.ClientType = clientType;
            request.Prospect.CompanyName = null;
            request.Prospect.CompanyCocNumber = null;
            var response = request.CallWith(ProspectClient.Proxy.EditProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            var editedProspect = new GetProspectRequest { Id = addProspectResponse.ProspectId }
                .CallWith(ProspectClient.Proxy.GetProspect);
            editedProspect.GetProspect.ClientType.Should().BeEquivalentTo(clientType);
        }

        [Test]
        public void EditProspect_ClientTypeToPrivateLeaveCompanyName_Returns400()
        {
            const ClientType clientType = ClientType.Private;
            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = new EditProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToEdit>()
            };
            request.Prospect.ProspectId = addProspectResponse.ProspectId;
            request.Prospect.ClientType = clientType;
            var response = request.CallWith(ProspectClient.Proxy.EditProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PrivateClientWithCompanyName);
        }

        [Test]
        public void EditProspect_ClientTypeToPrivateLeaveCompanyCocNr_Returns400()
        {
            const ClientType clientType = ClientType.Private;
            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = new EditProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToEdit>()
            };
            request.Prospect.ProspectId = addProspectResponse.ProspectId;
            request.Prospect.ClientType = clientType;
            request.Prospect.CompanyName = null;
            var response = request.CallWith(ProspectClient.Proxy.EditProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.PrivateClientWithCompanyCocNr);
        }

        [Test]
        public void EditProspect_ClientTypeToBusinessValidCase_Returns200()
        {
            const ClientType clientType = ClientType.Business;
            var addProspectRequest = GetDefaultAddProspectRequest(ClientType.Private);
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = new EditProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToEdit>()
            };
            request.Prospect.ProspectId = addProspectResponse.ProspectId;
            request.Prospect.ClientType = clientType;
            request.Prospect.CompanyName = "CompanyName";
            request.Prospect.CompanyCocNumber = "12345678";
            var response = request.CallWith(ProspectClient.Proxy.EditProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            var editedProspect = new GetProspectRequest { Id = addProspectResponse.ProspectId }
                .CallWith(ProspectClient.Proxy.GetProspect);
            editedProspect.GetProspect.ClientType.Should().BeEquivalentTo(clientType);
        }

        [Test]
        public void EditProspect_ClientTypeToBusinessWithoutCompanyName_Returns400()
        {
            const ClientType clientType = ClientType.Business;
            var addProspectRequest = GetDefaultAddProspectRequest(ClientType.Private);
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var request = new EditProspectRequest
            {
                Prospect = addProspectRequest.Prospect.To<ProspectToEdit>()
            };
            request.Prospect.ProspectId = addProspectResponse.ProspectId;
            request.Prospect.ClientType = clientType;
            var response = request.CallWith(ProspectClient.Proxy.EditProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.BusinessClientWithoutCompanyName);
        }

        [Test]
        public void EditProspect_DefaultValidCase_Returns200()
        {
            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var response = FillAllFieldsToEdit(addProspectRequest, addProspectResponse.ProspectId)
                .CallWith(ProspectClient.Proxy.EditProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            var editedProspect = new GetProspectRequest { Id = addProspectResponse.ProspectId }
                .CallWith(ProspectClient.Proxy.GetProspect);
            var check = ObjectComparator.ComparePropsOfTypes(editedProspect, false);
            check.Should().BeTrue();
        }
    }
}