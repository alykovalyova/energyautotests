﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOfferAllowedTransitionsTests : BaseProspectTest
    {
        [Test]
        public void GetOfferAllowedTransitions_DefaultValidCase_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var response = new GetOfferAllowedTransitionsRequest { OfferId = addOfferResponse.OfferId }
                .CallWith(ProspectClient.Proxy.GetOfferAllowedTransitions);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.AllowedTransitions.Should().NotBeEmpty();
        }

        [Test]
        public void GetOfferAllowedTransitions_WithNotExistedOfferId_Returns404()
        {
            const int offerId = int.MaxValue - 1;
            var response = new GetOfferAllowedTransitionsRequest { OfferId = offerId }
                .CallWith(ProspectClient.Proxy.GetOfferAllowedTransitions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.ObjectWasNotFoundById, "Offer", offerId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetOfferAllowedTransitions_WithInvalidOfferId_Returns400()
        {
            var response = new GetOfferAllowedTransitionsRequest { OfferId = 0 }
                .CallWith(ProspectClient.Proxy.GetOfferAllowedTransitions);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.IdIsNotBetweenValues, nameof(GetOfferAllowedTransitionsRequest.OfferId));
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}