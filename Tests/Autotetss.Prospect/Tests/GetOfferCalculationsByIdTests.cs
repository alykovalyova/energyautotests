﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOfferCalculationsByIdTests : BaseProspectTest
    {
        [Test]
        public void GetOfferCalculationsById_DefaultValidCase_Returns200()
        {
            var offer = ProspectDb.GetEntityByCondition<Offer>(o => o.CalculationDetailsSnapshot != null);

            var response = new GetOfferCalculationsByIdRequest { OfferId = offer.Id }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferCalculationSnapshot.Should().Contain(offer.CalculationDetailsSnapshot);
        }

        [Test]
        public void GetOfferCalculationsById_WithNotExistedOfferId_Returns404()
        {
            const int offerId = int.MaxValue - 1;
            var response = new GetOfferCalculationsByIdRequest { OfferId = offerId }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.ObjectWasNotFoundById, "Offer", offerId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetOfferCalculationsById_WithInvalidOfferId_Returns400()
        {
            var response = new GetOfferCalculationsByIdRequest { OfferId = 0 }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.IdIsNotBetweenValues, nameof(GetOfferCalculationsByIdRequest.OfferId));
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}