﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.TestDataProviders;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOfferCalculationsTests : BaseProspectTest
    {
        [Test]
        public void GetOfferCalculations_DefaultValidCase_Returns200()
        {
            const string testZipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var prospectId = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect).ProspectId;
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];

            var addOfferRequest = GetAddOfferRequest(testZipcode, ean, prospectId, activeProposition);
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK);

            var response = new GetOfferCalculationsRequest { Offer = addOfferRequest.Offer }
                .CallWith(ProspectClient.Proxy.GetOfferCalculations);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferCalculationSnapshot.Should().Contain(activeProposition.Item2.ToString());
        }

        [Test]
        public void GetOfferCalculations_WithNotExistedOfferId_Returns404()
        {
            const int fakeOfferId = -1;
            const string testZipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];

            var createOfferInProposition = GetAddOffer(fakeOfferId, activeProposition, testZipcode, ean);
            var response = new GetOfferCalculationsRequest { Offer = createOfferInProposition }
                .CallWith(ProspectClient.Proxy.GetOfferCalculations);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Test]
        public void GetOfferCalculations_InvalidOfferId_Returns400()
        {
            const int fakeOfferId = 0;
            const string testZipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];

            var createOfferInProposition = GetAddOffer(fakeOfferId, activeProposition, testZipcode, ean);
            var response = new GetOfferCalculationsRequest { Offer = createOfferInProposition }
                .CallWith(ProspectClient.Proxy.GetOfferCalculations);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }
    }
}