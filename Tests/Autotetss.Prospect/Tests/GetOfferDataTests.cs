﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOfferDataTests : BaseProspectTest
    {
        [Test]
        public void GetOfferData_DefaultValidCase_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var offerFromDb = ProspectDb.GetEntitiesByCondition<Offer>(o => o.Id == addOfferResponse.OfferId, "Prospect").RandomItem();
            var response = new GetOfferDataRequest { ExternalReference = offerFromDb.ExternalReference.ToString() }
                .CallWith(ProspectClient.Proxy.GetOfferData);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.SupplyAddress.Should().NotBeNull();
            Assert.Multiple(() =>
            {
                var patternAddress = addOfferRequest.Offer.SupplyAddress;
                response.SupplyAddress.BuildingNr.Should().Be(patternAddress.BuildingNr);
                response.SupplyAddress.ExBuildingNr.Should().BeEquivalentTo(patternAddress.ExBuildingNr);
                response.SupplyAddress.ZIPCode.Should().BeEquivalentTo(patternAddress.ZIPCode);
            });
        }

        [Test]
        public void GetOfferData_WithEmptyExRef_Returns400([Values("", null)] string exRef)
        {
            var response = new GetOfferDataRequest { ExternalReference = exRef }
                .CallWith(ProspectClient.Proxy.GetOfferData);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(GetOfferDataRequest.ExternalReference));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetOfferData_WithNotExistedExRef_Returns404()
        {
            var notExistedExRef = Guid.NewGuid().ToString();

            var response = new GetOfferDataRequest { ExternalReference = notExistedExRef }
                .CallWith(ProspectClient.Proxy.GetOfferData);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.ObjectWasNotFoundByExRef, "Prospect", notExistedExRef);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}