﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOfferEmailInfoTests : BaseProspectTest
    {
        [Test]
        public void GetOfferEmailInfo_DefaultValidCase_Returns200()
        {
            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(
                o => o.OfferStatusHistory.Any(osh => osh.StatusId == (int)OfferStatusCode.Sent));

            var response = new GetOfferEmailInfoRequest { ExternalReference = offerFromDb.ExternalReference.ToString() }
                .CallWith(ProspectClient.Proxy.GetOfferEmailInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferEmailInfo.Should().NotBeNull();
        }

        [Test]
        public void GetOfferEmailInfo_WithEmptyExRef_Returns400([Values("", null)] string exRef)
        {
            var response = new GetOfferEmailInfoRequest { ExternalReference = exRef }
                .CallWith(ProspectClient.Proxy.GetOfferEmailInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(GetOfferEmailInfoRequest.ExternalReference));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetOfferEmailInfo_WithNotExistedExRef_Returns404()
        {
            var notExistedExRef = Guid.NewGuid().ToString();

            var response = new GetOfferEmailInfoRequest { ExternalReference = notExistedExRef }
                .CallWith(ProspectClient.Proxy.GetOfferEmailInfo);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.ObjectWasNotFoundByExRef, "Prospect", notExistedExRef);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}