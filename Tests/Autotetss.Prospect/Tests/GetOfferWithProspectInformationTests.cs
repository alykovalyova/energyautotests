﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOfferWithProspectInformationTests : BaseProspectTest
    {
        [Test]
        public void GetOfferWithProspectInformation_DefaultValidCase_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferResponse = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>())
                .CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var response = new GetOfferWithProspectInfoRequest
            {
                Label = Labels.BudgetEnergie,
                OfferStatus = OfferStatusCode.New,
                SalesAgentName = SalesAgentName
            }.CallWith(ProspectClient.Proxy.GetOfferWithProspectInformation);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferWithProspectInfo.Should().NotBeEmpty();
        }

        [Test]
        public void GetOfferWithProspectInformation_WithEmptySalesAgentName_Returns400([Values("", null)] string salesAgentName)
        {
            var response = new GetOfferWithProspectInfoRequest
            {
                Label = Labels.BudgetEnergie,
                OfferStatus = OfferStatusCode.New,
                SalesAgentName = salesAgentName
            }.CallWith(ProspectClient.Proxy.GetOfferWithProspectInformation);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(GetOfferWithProspectInfoRequest.SalesAgentName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetOfferWithProspectInformation_WithNotExistedSalesAgentName_Returns200()
        {
            var notExistedName = Guid.NewGuid().ToString();

            var response = new GetOfferWithProspectInfoRequest
            {
                Label = Labels.BudgetEnergie,
                OfferStatus = OfferStatusCode.New,
                SalesAgentName = notExistedName
            }.CallWith(ProspectClient.Proxy.GetOfferWithProspectInformation);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OfferWithProspectInfo.Should().BeEmpty();
        }
    }
}