﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetOffersBySalesAgentNameTests : BaseProspectTest
    {
        [Test]
        public void GetOffersBySalesAgentName_DefaultValidCase_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferRequest = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>());
            var addOfferResponse = addOfferRequest.CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);

            var response = new GetOffersBySalesAgentNameRequest { SalesAgentName = addOfferRequest.Offer.SalesAgentName }
                .CallWith(ProspectClient.Proxy.GetOffersBySalesAgentName);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OffersInfo.Should().NotBeEmpty();
        }

        [Test]
        public void GetOffersBySalesAgentName_WithEmptySalesAgentName_Returns400([Values("", null)] string salesAgentName)
        {
            var response = new GetOffersBySalesAgentNameRequest { SalesAgentName = salesAgentName }
                .CallWith(ProspectClient.Proxy.GetOffersBySalesAgentName);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(GetOffersBySalesAgentNameRequest.SalesAgentName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetOffersBySalesAgentName_WithNotExistedSalesAgentName_Returns200()
        {
            var notExistedName = Guid.NewGuid().ToString();

            var response = new GetOffersBySalesAgentNameRequest { SalesAgentName = notExistedName }
                .CallWith(ProspectClient.Proxy.GetOffersBySalesAgentName);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.OffersInfo.Should().BeEmpty();
        }
    }
}