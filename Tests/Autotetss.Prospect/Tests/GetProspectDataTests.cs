﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract.Transport;
using static Autotests.Core.Helpers.ObjectComparator;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetProspectDataTests : BaseProspectTest
    {
        [Test]
        public void GetProspectData_DefaultValidCase_Returns200()
        {
            var offerFromDb = ProspectDb.GetEntitiesByCondition<Offer>(o => o.ExternalReference != null, "Prospect").RandomItem();

            var response = new GetProspectDataRequest { ExternalReference = offerFromDb.ExternalReference.ToString() }
                .CallWith(ProspectClient.Proxy.GetProspectData);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospect.Should().NotBeNull();
            ComparePropsOfTypes(response.Prospect, offerFromDb.Prospect).Should().BeTrue();
        }

        [Test]
        public void GetProspectData_WithEmptyExRef_Returns400([Values("", null)] string exRef)
        {
            var response = new GetProspectDataRequest { ExternalReference = exRef }
                .CallWith(ProspectClient.Proxy.GetProspectData);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(GetProspectDataRequest.ExternalReference));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetProspectData_WithNotExistedExRef_Returns404()
        {
            var notExistedExRef = Guid.NewGuid().ToString();

            var response = new GetProspectDataRequest { ExternalReference = notExistedExRef }
                .CallWith(ProspectClient.Proxy.GetProspectData);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.ObjectWasNotFoundByExRef, nameof(response.Prospect), notExistedExRef);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}