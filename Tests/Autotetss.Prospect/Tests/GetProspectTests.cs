﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Model.Contract.Transport;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class GetProspectTests : BaseProspectTest
    {
        [Test]
        public void GetProspect_DefaultValidCase_Returns200()
        {
            var addProspectRequest = GetDefaultAddProspectRequest();
            var addProspectResponse = addProspectRequest.CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var response = new GetProspectRequest { Id = addProspectResponse.ProspectId }
                .CallWith(ProspectClient.Proxy.GetProspect);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            addProspectRequest.Prospect.Initials = AddDots(addProspectRequest.Prospect.Initials).ToUpper();
            ObjectComparator.ComparePropsOfTypes(response.GetProspect, addProspectRequest.Prospect, false).Should().BeTrue();
            response.GetProspect.IsAnonymized.Should().BeFalse();
        }

        [Test]
        public void GetProspect_WithNotExistedProspectId_Returns404()
        {
            const int id = int.MaxValue - 1;
            var response = new GetProspectRequest { Id = id }
                .CallWith(ProspectClient.Proxy.GetProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.ObjectWasNotFoundById, "Prospect", id);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void GetProspect_WithInvalidProspectId_Returns400()
        {
            const int id = 0;
            var response = new GetProspectRequest { Id = id }
                .CallWith(ProspectClient.Proxy.GetProspect);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.IdIsNotBetweenValues, "ProspectId");
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}