﻿using System.Net;
using Autotests.Clients;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class SearchProspectTests : BaseProspectTest
    {
        [Test]
        public void SearchProspect_BySurnameOnly_Returns200()
        {
            var prospect = CreateDefaultProspect().GetProspect;

            var response = new SearchProspectsRequest { Surname = prospect.Surname, Label = prospect.Label }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            response.Prospects.Should().Contain(p => p.ProspectId.Equals(prospect.ProspectId));
        }

        [Test]
        public void SearchProspect_ByZipcodeOnly_Returns200()
        {
            var prospect = CreateDefaultProspect().GetProspect;

            var response = new SearchProspectsRequest { ZipCode = prospect.ZIPCode, Label = prospect.Label }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            response.Prospects.Should().Contain(p => p.ProspectId.Equals(prospect.ProspectId));
        }

        [Test]
        public void SearchProspect_ByPhoneNrHomeOnly_Returns200()
        {
            var prospect = CreateDefaultProspect().GetProspect;

            var response = new SearchProspectsRequest { PhoneNumberHome = prospect.PhoneNumberHome, Label = prospect.Label }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            response.Prospects.Should().Contain(p => p.ProspectId.Equals(prospect.ProspectId));
        }

        [Test]
        public void SearchProspect_ByPhoneNrMobileOnly_Returns200()
        {
            var prospect = CreateDefaultProspect().GetProspect;

            var response = new SearchProspectsRequest { PhoneNumberMobile = prospect.PhoneNumberMobile, Label = prospect.Label }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            response.Prospects.Should().Contain(p => p.ProspectId.Equals(prospect.ProspectId));
        }

        [Test]
        public void SearchProspect_ByEmailOnly_Returns200()
        {
            var prospect = CreateDefaultProspect().GetProspect;

            var response = new SearchProspectsRequest { Email = prospect.EmailAddress, Label = prospect.Label }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            response.Prospects.Should().Contain(p => p.ProspectId.Equals(prospect.ProspectId));
        }

        [Test]
        public void SearchProspect_ByCompanyNameOnly_Returns200()
        {
            var prospect = CreateDefaultProspect().GetProspect;

            var response = new SearchProspectsRequest { CompanyName = prospect.CompanyName, Label = prospect.Label }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            response.Prospects.Should().NotBeEmpty();
            response.Prospects.Should().Contain(p => p.ProspectId.Equals(prospect.ProspectId));
        }

        [Test]
        public void SearchProspect_WithoutAnyOfRequiredFields_Returns400()
        {
            var response = new SearchProspectsRequest { Label = Labels.BudgetEnergie }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.SearchWithoutRequiredFields);
        }

        [Test]
        public void SearchProspect_BuildNrIsNullWhenZipcodeAndExBuildNrAreSet_Returns400()
        {
            var response = new SearchProspectsRequest { Label = Labels.BudgetEnergie, ZipCode = "1111QQ", ExBuildingNr = "B" }
                .CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.SearchWithEmptyBuildNr);
        }

        [Test]
        public void SearchProspect_SetBuildNrAndExBuildNrWithoutZipcode_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                ExBuildingNr = "B",
                BuildingNr = 11,
                Email = "Email@tochka.com"
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.RestrictedError);
        }

        [Test]
        public void SearchProspect_InvalidPhoneNrHomeFormat_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                Email = "Email@tochka.com",
                PhoneNumberHome = "+123"
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidPhoneFormat, "Home");
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SearchProspect_InvalidPhoneNrMobileFormat_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                Email = "Email@tochka.com",
                PhoneNumberMobile = "+123"
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidPhoneFormat, "Mobile");
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SearchProspect_OverMaxSurnameLength_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                Email = "Email@tochka.com",
                Surname = new string('a', 51)
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, $"{nameof(SearchProspectsRequest.Surname)}", 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SearchProspect_InvalidZipcode_Returns400([Values("111QQ", "1111Q", "1111", "QQQQ", "1111QQQ")] string zipcode)
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                ZipCode = zipcode
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.ZipCodeFieldRegexIsWrong);
        }

        [Test]
        public void SearchProspect_InvalidBuildingNr_Returns400([Values(0, 100000)] int buildingNr)
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                ZipCode = "1111QQ",
                BuildingNr = buildingNr
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            response.Header.Message.Should().BeEquivalentTo(PatternMessages.InvalidBuildingNr);
        }

        [Test]
        public void SearchProspect_OverMaxExBuildingNrLength_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                ZipCode = "1111QQ",
                ExBuildingNr = new string('a', 7)
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, $"{nameof(SearchProspectsRequest.ExBuildingNr)}", 6);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SearchProspect_OverMaxCompanyNameLength_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                ZipCode = "1111QQ",
                CompanyName = new string('a', 51)
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, $"{nameof(SearchProspectsRequest.CompanyName)}", 50);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SearchProspect_OverMaxEmailLength_Returns400()
        {
            var response = new SearchProspectsRequest
            {
                Label = Labels.BudgetEnergie,
                ZipCode = "1111QQ",
                Email = $"{new string('a', 250)}@aaa.com"
            }.CallWith(ProspectClient.Proxy.SearchProspects);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.InvalidFieldLength, $"{nameof(SearchProspectsRequest.Email)}", 255);
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}