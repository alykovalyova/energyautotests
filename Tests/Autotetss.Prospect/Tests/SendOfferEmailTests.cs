﻿using Autotests.Core.Helpers;
using Autotests.Core.TestDataProviders;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.InterDom.Models.Enums;
using Nuts.ProdMan.Core.Enums;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;
using System.Net;
using Autotests.Clients;
using Autotests.Prospect.Base;
using PatternMessages = Autotests.Core.Helpers.PatternMessages;

namespace Autotests.Tests.Prospect
{
    [TestFixture]
    internal class SendOfferEmailTests : BaseProspectTest
    {
        [Test]
        public void SendOfferEmail_DefaultValidCase_Returns200()
        {
            const string zipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var prospectId = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect).ProspectId;

            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];

            var addOfferResponse = GetAddOfferRequest(zipcode, ean, prospectId, activeProposition)
                .CallWith(ProspectClient.Proxy.AddOffer);
            var offerCalculations = new GetOfferCalculationsByIdRequest { OfferId = addOfferResponse.OfferId }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById).OfferCalculationSnapshot;

            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);
            var response = new SendOfferEmailRequest
            {
                SendOfferInfo = new SendOfferInfo
                {
                    CalcutionSnapshotJson = offerCalculations,
                    ExternalReference = offerFromDb.ExternalReference.ToString(),
                    SalesAgentName = SalesAgentName
                }
            }.CallWith(ProspectClient.Proxy.SendOfferEmail);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            offerFromDb =
                ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId, "OfferStatusHistory");
            offerFromDb.OfferStatusHistory.Any(osh => osh.StatusId == (int)OfferStatusCode.QueuedForSending)
                .Should().BeTrue();
            Assert.DoesNotThrow(() =>
                    Waiter.Wait(() =>
                        ProspectDb.EntityIsInDb<OfferStatusHistory>(
                            osh => osh.OfferId == addOfferResponse.OfferId && osh.StatusId == (int)OfferStatusCode.Sent), 5),
                $"Offer with Id: {addOfferResponse.OfferId} hasn't been sent.");
        }

        [Test]
        public void SendOfferEmail_WithEmptyExRef_Returns400([Values("", null)] string exRef)
        {
            const string zipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var prospectId = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect).ProspectId;
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];
            var addOfferResponse = GetAddOfferRequest(zipcode, ean, prospectId, activeProposition)
                .CallWith(ProspectClient.Proxy.AddOffer);
            var offerCalculations = new GetOfferCalculationsByIdRequest { OfferId = addOfferResponse.OfferId }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById).OfferCalculationSnapshot;

            var response = new SendOfferEmailRequest
            {
                SendOfferInfo = new SendOfferInfo
                {
                    CalcutionSnapshotJson = offerCalculations,
                    ExternalReference = exRef,
                    SalesAgentName = SalesAgentName
                }
            }.CallWith(ProspectClient.Proxy.SendOfferEmail);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(SendOfferEmailRequest.SendOfferInfo.ExternalReference));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SendOfferEmail_WithNotExistedExRef_Returns404()
        {
            var notExistedExRef = Guid.NewGuid().ToString();
            const string zipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var prospectId = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect).ProspectId;
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];
            var addOfferResponse = GetAddOfferRequest(zipcode, ean, prospectId, activeProposition)
                .CallWith(ProspectClient.Proxy.AddOffer);
            var offerCalculations = new GetOfferCalculationsByIdRequest { OfferId = addOfferResponse.OfferId }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById).OfferCalculationSnapshot;
            if (offerCalculations == null)
                throw new IgnoreException($"{nameof(offerCalculations)} is null.");

            var response = new SendOfferEmailRequest
            {
                SendOfferInfo = new SendOfferInfo
                {
                    CalcutionSnapshotJson = offerCalculations,
                    ExternalReference = notExistedExRef,
                    SalesAgentName = SalesAgentName
                }
            }.CallWith(ProspectClient.Proxy.SendOfferEmail);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.OfferWasNotFoundByExFef, notExistedExRef);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SendOfferEmail_WithEmptyCalculationSnapshotJson_Returns400([Values("", null)] string calcJson)
        {
            const string zipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var prospectId = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect).ProspectId;
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];
            var addOfferResponse = GetAddOfferRequest(zipcode, ean, prospectId, activeProposition)
                .CallWith(ProspectClient.Proxy.AddOffer);
            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);

            var response = new SendOfferEmailRequest
            {
                SendOfferInfo = new SendOfferInfo
                {
                    CalcutionSnapshotJson = calcJson,
                    ExternalReference = offerFromDb.ExternalReference.ToString(),
                    SalesAgentName = SalesAgentName
                }
            }.CallWith(ProspectClient.Proxy.SendOfferEmail);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired,
                nameof(SendOfferEmailRequest.SendOfferInfo.CalcutionSnapshotJson));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SendOfferEmail_WithEmptySalesAgentName_Returns400([Values("", null)] string salesAgentName)
        {
            const string zipcode = "1111AA";
            var activeProposition = GetProposition(Status.Active, TypeOfClient.Private, Labels.BudgetEnergie);
            var prospectId = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect).ProspectId;
            var ean = TestDataPreparation.TestEans[TestDataPreparation.MeterType.ElkEanTfSignalValid2Reg];
            var addOfferResponse = GetAddOfferRequest(zipcode, ean, prospectId, activeProposition)
                .CallWith(ProspectClient.Proxy.AddOffer);
            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);
            var offerCalculations = new GetOfferCalculationsByIdRequest { OfferId = addOfferResponse.OfferId }
                .CallWith(ProspectClient.Proxy.GetOfferCalculationsById).OfferCalculationSnapshot;

            var response = new SendOfferEmailRequest
            {
                SendOfferInfo = new SendOfferInfo
                {
                    CalcutionSnapshotJson = offerCalculations,
                    ExternalReference = offerFromDb.ExternalReference.ToString(),
                    SalesAgentName = salesAgentName
                }
            }.CallWith(ProspectClient.Proxy.SendOfferEmail);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired,
                nameof(SendOfferEmailRequest.SendOfferInfo.SalesAgentName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void SendOfferEmail_WithNullSendOfferInfo_Returns400()
        {
            var response = new SendOfferEmailRequest
            {
                SendOfferInfo = null
            }.CallWith(ProspectClient.Proxy.SendOfferEmail);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired, nameof(SendOfferEmailRequest.SendOfferInfo));
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}