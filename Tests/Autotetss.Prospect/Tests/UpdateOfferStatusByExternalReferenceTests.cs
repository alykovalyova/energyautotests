﻿using System.Net;
using Autotests.Clients;
using Autotests.Core;
using Autotests.Core.Helpers;
using Autotests.Prospect.Base;
using Autotests.Repositories.ProspectModels;
using FluentAssertions;
using NUnit.Framework;
using Nuts.Prospect.Core.Enums;
using Nuts.Prospect.Model.Contract;
using Nuts.Prospect.Model.Contract.Transport;

namespace Autotests.Prospect.Tests
{
    [TestFixture]
    internal class UpdateOfferStatusByExternalReferenceTests : BaseProspectTest
    {
        [Test]
        public void UpdateOfferStatusByExternalReference_FromNewStatusToValidStatuses_Returns200([Values(
            OfferStatusCode.Approved, OfferStatusCode.Canceled,
            OfferStatusCode.QueuedForSending, OfferStatusCode.TooLate)] OfferStatusCode newStatus)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferResponse = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>()).CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);
            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);

            var response = GetUpdateOfferStatusByExternalReferenceRequest(
                newStatus, "Autotest_Comment", offerFromDb.ExternalReference.ToString(), "Autotest_User")
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);
            offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId, "OfferStatusHistory");
            offerFromDb.OfferStatusHistory.Should().Contain(e => e.StatusId.Equals((int)newStatus));
        }

        [Test]
        public void UpdateOfferStatusByExternalReference_FromApprovedToAcceptedStatus_Returns200()
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferResponse = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>())
                .CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);
            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);

            var response = GetUpdateOfferStatusByExternalReferenceRequest(
                OfferStatusCode.Approved, "Autotest_Comment", offerFromDb.ExternalReference.ToString(), "Autotest_User")
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            response = GetUpdateOfferStatusByExternalReferenceRequest(
                OfferStatusCode.Accepted, "Autotest_Comment", offerFromDb.ExternalReference.ToString(), "Autotest_User")
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);
            response.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, response.Header.Message);

            offerFromDb = ProspectDb.GetEntityByCondition<Offer>(
                o => o.Id == addOfferResponse.OfferId, "OfferStatusHistory");
            offerFromDb.OfferStatusHistory.Should().Contain(e => e.StatusId.Equals((int)OfferStatusCode.Accepted));
        }

        [Test]
        public void UpdateOfferStatusByExternalReference_FromNewStatusToInvalidStatuses_Returns400([Values(
            OfferStatusCode.Accepted, OfferStatusCode.Rejected, OfferStatusCode.Sent)] OfferStatusCode newStatus)
        {
            var mpTestData = MeteringPointDb.GetEntityByCondition<Autotests.Repositories.MeteringPointModels.MeteringPoint>(
                m => m.Address.Zipcode.Length == 6 && !m.Address.Zipcode.StartsWith("0"), Includes);

            var addProspectResponse = GetDefaultAddProspectRequest()
                .CallWith(ProspectClient.Proxy.AddProspect);
            addProspectResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addProspectResponse.Header.Message);

            var addOfferResponse = GetAddOfferRequest(addProspectResponse.ProspectId,
                mpTestData.To<AddOfferMeteringPoint>(),
                mpTestData.Address.To<AddOfferSupplyAddress>())
                .CallWith(ProspectClient.Proxy.AddOffer);
            addOfferResponse.Header.StatusCode.Should().Be((int)HttpStatusCode.OK, addOfferResponse.Header.Message);
            var offerFromDb = ProspectDb.GetEntityByCondition<Offer>(o => o.Id == addOfferResponse.OfferId);

            var response = GetUpdateOfferStatusByExternalReferenceRequest(
                newStatus, "Autotest_Comment", offerFromDb.ExternalReference.ToString(), "Autotest_User")
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.TransitionUnavailable, OfferStatusCode.New, newStatus, addOfferResponse.OfferId);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdateOfferStatusByExternalReference_WithEmptyExRef_Returns400([Values("", null)] string exRef)
        {
            var response = GetUpdateOfferStatusByExternalReferenceRequest(
                OfferStatusCode.Approved, "Autotest_Comment", exRef, "Autotest_User")
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired,
                nameof(UpdateOfferStatusByExternalReferenceRequest.ExternalReference));
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdateOfferStatusByExternalReference_WithNotExistedExRef_Returns404()
        {
            var notExistedExRef = Guid.NewGuid().ToString();

            var response = GetUpdateOfferStatusByExternalReferenceRequest(
                OfferStatusCode.Approved, "Autotest_Comment", notExistedExRef, "Autotest_User")
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
            var message = string.Format(PatternMessages.OfferWasNotFoundByExFef, notExistedExRef);
            response.Header.Message.Should().BeEquivalentTo(message);
        }

        [Test]
        public void UpdateOfferStatusByExternalReference_WithEmptySalesAgentName_Returns400([Values("", null)] string salesAgentName)
        {
            var response = GetUpdateOfferStatusByExternalReferenceRequest(
                OfferStatusCode.Approved, "Autotest_Comment", Guid.NewGuid().ToString(), salesAgentName)
                .CallWith(ProspectClient.Proxy.UpdateOfferStatusByExternalReference);

            response.Header.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
            var message = string.Format(PatternMessages.FieldIsRequired,
                nameof(UpdateOfferStatusByExternalReferenceRequest.SalesAgentName));
            response.Header.Message.Should().BeEquivalentTo(message);
        }
    }
}