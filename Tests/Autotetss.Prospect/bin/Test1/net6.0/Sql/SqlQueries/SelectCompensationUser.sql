﻿select DISTINCT TOP 1 c.ContractId, cpk.ContactPersonNumber
from [EnerfreeDB].[dbo].Contracten c
inner join [EnerFreeDB].[dbo].Relaties r on r.RelatieID = c.ContractantID
inner join [EnerFreeDB].[dbo].ContractRegels cr on cr.ContractID = c.ContractID
join [Customer].[dbo].[ProductCustomer] pc on pc.ProductCustomerId = c.ContractantID
join [Customer].[dbo].[CustomerContactPerson] ccp on ccp.NutsHomeCustomerId = pc.NutsHomeCustomerId
join [Customer].[dbo].[ContactPersonKey] cpk on cpk.Id = ccp.ContactPersonKeyId
inner join [EnerFreeDB].[dbo].ContractRegelAgreements cra on cra.ContractRegelId = cr.ContractRegelID
inner join [EnerFreeDB].[dbo].ContractRegelStatusHistory crh on crh.ContractRegelId = cr.ContractRegelID
inner join [ProdMan].[dbo].Proposition p on p.Id = cra.ProdmanPropositionId
WHERE NOT EXISTS (
	SELECT * FROM [CustomerInformation].[dbo].ReimbursePenaltyRequest rpr WHERE rpr.ContractId = c.ContractID
) 
AND crh.StatusId = 2
AND crh.FromDate > DATEADD(month, -4, GETDATE())
AND cr.Actief = 1 
AND p.ReimburseFine > 0
order by c.ContractID desc